Install:

1.Ubuntu16.04

2.Install ROS kinetic Desktop Full

3.Depend physics package、rosplane_msgs package

4.Install fastRTPS1.9
  （1）sudo apt install libasio-dev libtinyxml2-dev
  （2）Fast CDR：
      $ git clone https://github.com/eProsima/Fast-CDR.git
      $ mkdir Fast-CDR/build && cd Fast-CDR/build
      $ cmake ..
      $ sudo  cmake --build . --target install
  （3）Foonathan memory 
      $ git clone https://github.com/eProsima/foonathan_memory_vendor.git
      $ cd foonathan_memory_vendor
      $ mkdir build && cd build
      $ cmake ..
      $ sudo cmake --build . --target install
  （4）Fast RTPS。
      $ git clone https://github.com/eProsima/Fast-RTPS.git
      $ mkdir Fast-RTPS/build && cd Fast-RTPS/build
      $ cmake ..
      $ sudo cmake --build . --target install
  （5）sudo ldconfig

5.Install sqlite json expand：
  （1）下载sqlite3并解压缩（压缩包在third_party文件夹里）
  （2）将src中的sqlite3ext.h文件拷贝到/ext/misc下
      执行：gcc -fPIC -shared json1.c sqlite3ext.h -o libjson.so 即生成  libjson.so库
  （3）将生成的json库拷贝到/usr/local/lib下

6.Install geodesy
   sudo apt-get install ros-kinetic-geodesy

7.适配程序软件安装说明
需要安装的压缩包全部在third_party文件夹里
需要安装的库有：
mqtt对应的库：mosquitto-1.4.10.tar.gz
mqtt依赖的库：libuuid-1.0.3.tar.gz c-ares-1.14.0.tar.gz openssl-1.0.2e.tar.gz
protobuf库：protobuf-all-3.5.0.tar.gz
(1)mosquitto-1.4.10安装方法
命令行安装：sudo apt-add-repository ppa:mosquitto-dev/mosquitto-ppa
     sudo apt-get update
(2)libuuid-1.0.3安装方法
源码安装：
  ./configure
  make
  sudo make install
(3)c-ares-1.14.0安装方法
源码安装:
  ./configure
  make
  sudo make install
(4)openssl-1.0.2e安装方法
源码安装：
  ./config
  make
  make test
  sudo make install
(5)protobuf-all-3.5.0安装方法
源码安装：
  ./configure --prefix=/usr/local/protobuf/
  make
  make check
  sudo make install
