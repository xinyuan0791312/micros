/*=================================================================
* Copyright (c) 2016, micROS Group, NIIDT, TAIIC, HPCL. 
* All rights reserved. 
* 
* Redistribution and use in source and binary forms, with or without modification, are permitted 
* provided that the following conditions are met:  
* 
* 1. Redistributions of source code must retain the above copyright notice, this list of conditions
*   and the following disclaimer. 
* 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
*   and the following disclaimer in the documentation and/or other materials provided with the distribution.
* 3. All advertising materials mentioning features or use of this software must display the following 
*   acknowledgement: This product includes software developed by the micROS Group and its contributors. 
* 4. Neither the name of the Group nor the names of contributors may be used to endorse or promote 
*   products derived from this software without specific prior written permission. 
* 
* THIS SOFTWARE IS PROVIDED BY MICROS GROUP AND CONTRIBUTORS ''AS IS''AND ANY EXPRESS OR IMPLIED WARRANTIES,
* INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR 
* PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE MICROS, GROUP OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
* GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION). HOWEVER CAUSED AND ON ANY THEORY 
* OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
* WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 
===================================================================
* Author: Yantai Yang. 
*/ 
#ifndef LOG_REC_STORE_H
#define LOG_REC_STORE_H

#include <ros/ros.h>
#include <ros/time.h>
#include <rosgraph_msgs/Log.h>
#include <sys/time.h>
#include <time.h>
#include <iostream>
#include <fstream>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/unistd.h>
#include <vector>
#include "micros_log/MicRosLog.h"

/**
 * @brief handle micros log information \n
 * ROS log and micROs log
 */
namespace micros_log
{
    std::string levelNumToStr(int num);

    
    /**
     * @brief handle micros log information \n
     * obtain topic name /rosout ,storage log and pub topic /robot_i/logs
     */
    class LogRecStore
    {
        public:
            LogRecStore();
            ~LogRecStore();
            /**
             * @brief receive topic callback function
             * receive topic name /rosout and pub /robot_i/logs topic name
             * @param msg msg data
             */
            void rosoutRecCallback(const rosgraph_msgs::Log::ConstPtr& msg);
            /**
             * @brief log information store into file \n
             * 
             */
            void logStoreFile();
            /**
             * @brief Create a File Mkdir object \n
             * 
             * @param aFileDir file directory name
             * rerurn
             */
            void createFileMkdir(std::string aFileDir);

            /**
             * @brief init Log
             * 
             */
            void initLog();

            /**
             * @brief Set the Logs Pub object
             * 
             * @param pub object
             */
            void setLogsPub(ros::Publisher pub)
            {
                this->_logsPub = pub;
            }

            /**
             * @brief Get the Logs Pub object
             * 
             * @return ros::Publisher 
             */
            ros::Publisher getLogsPub()
            {
                return this->_logsPub;
            }
        
        private:
            ros::NodeHandle _nh;
            ros::NodeHandle _nhPrivate;
            ros::Subscriber _rosoutSub;
            ros::Publisher _logsPub;
            micros_log::MicRosLog _microslogMsg;
            std::ofstream _fileOut;
            std::string _namespace;
            std::string _pubTopicName;
            std::string _fileName;
            std::string _fileDir;
            int _robotId;
            int _count;
            struct tm _tm;
            /**
             * @brief the current time chars
             * 
             */
            char _nowChar[64];
            double _currentTime;
            std::vector<std::string> _logTypes;

    };
}//end of namespace
#endif