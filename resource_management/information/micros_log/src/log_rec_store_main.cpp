#include "micros_log/log_rec_store.h"
int main(int argc, char **argv)
{
  	ros::init(argc, argv, "micros_log_rec_store");
	micros_log::LogRecStore log_instance;	
    log_instance.initLog();      
	ros::spin();

  	return 0;
}