#include "micros_log/log_rec_store.h"

namespace micros_log
{
    using std::string;
    string levelNumToStr(int num)
    {
        string result="";
        switch(num) {
            case 1:
                result="debug";
                break;
            case 2:
                result="info";
                break;
            case 4:
                result="warn";
                break;
            case 8:
                result="error";
                break;
            case 16:
                result="fatal";
                break;
            default:
                result="wrong";
        }
        return result;
    }
   
    LogRecStore::LogRecStore()
    {
       _nh= ros::NodeHandle(); 
       _nhPrivate= ros::NodeHandle("~");
     
       _nhPrivate.param<std::string>("namespace",_namespace,"robot_0");
       //_nhPrivate.param<std::string>("file_dir",_fileDir,"/home/micros/log");

       _nhPrivate.param<int>("robot_id",_robotId,0);
       _nhPrivate.getParam("logs_type",_logTypes);
       _currentTime = ros::Time::now().toSec();
    } 
    LogRecStore::~LogRecStore()
    {
        if(!_fileOut.is_open())
        {
            _fileOut.close();
        } 
    }

    void LogRecStore::initLog()
    {
       _fileName = "robot_"+ std::to_string(_robotId)+".log";
       
       _pubTopicName = "/"+_namespace+"/logs";
       _logsPub = _nhPrivate.advertise<micros_log::MicRosLog>(_pubTopicName,10);
       _rosoutSub = _nh.subscribe<rosgraph_msgs::Log>("/rosout",10,&LogRecStore::rosoutRecCallback,this);
       _count = 0;
       
       std::string home = getenv("HOME");
       _fileDir = home + "/informationLog/";
       std::string fileAll = _fileDir + "/" + _fileName;
       createFileMkdir(_fileDir);
       _fileOut.open(fileAll.c_str(),std::ios::out);
       if(!_fileOut.is_open()){
           ROS_ERROR("open file=%s is failed,ple check!",fileAll.c_str());
           return;
       }
    }

    void LogRecStore::rosoutRecCallback(const rosgraph_msgs::Log::ConstPtr& msg)
    {
        double stampTime = ros::Time::now().toSec();
        time_t tick = (time_t)stampTime;
        _tm = *localtime(&tick);
        strftime(_nowChar,64,"%Y-%m-%d %H:%M:%S",&_tm);
        //_microslogMsg.datetime = std::to_string(ros::Time::now().toSec());
        _microslogMsg.datetime = _nowChar;
        _microslogMsg.level = levelNumToStr(msg->level);
        _microslogMsg.name = msg->name;
        _microslogMsg.msg = msg->msg;
        //msg contain log types 
        if(_logTypes.size()>0)
        {
            for(int i=0; i<_logTypes.size(); i++)
            {
                if(_microslogMsg.msg.find(_logTypes[i]) != std::string::npos){
                    _logsPub.publish(_microslogMsg);
                    logStoreFile();
                }
            }

        }else{
          _logsPub.publish(_microslogMsg);
          logStoreFile();
        }

    }

    void LogRecStore::logStoreFile()
    {
        _count++;
        _fileOut<<"[";
        _fileOut<<_microslogMsg.datetime<<"] ";
        _fileOut<<"[";
        _fileOut<<_microslogMsg.level<<"] ";
        _fileOut<<"[";
        _fileOut<<_microslogMsg.name<<"] ";
        _fileOut<<"[";
        _fileOut<<_microslogMsg.msg<<"]";
        _fileOut<<std::endl;
        //mod：add timeout mechanism 
        double currentTmpTime = ros::Time::now().toSec();
        if(_count%10 == 0 || currentTmpTime >_currentTime+10){
            _fileOut.flush();
        }
        _currentTime = currentTmpTime;

    }
    
    void LogRecStore::createFileMkdir(std::string fileDir)
    {
         ROS_INFO("The will create fileName = %s",fileDir.c_str());
         while(access(fileDir.c_str(), F_OK) == -1)
         {
            mode_t mode = 0755;
            int flagCreate = mkdir(fileDir.c_str(), mode);
            if(flagCreate == 0)
            {
                ROS_INFO("create file dir =%s,is success.",fileDir.c_str());
            }
            else
            {
                size_t indexFile =fileDir.find_last_of("/");
                std::string parentFileDir = fileDir.substr(0,indexFile);
                ROS_INFO("The will create parent file dir = %s",parentFileDir.c_str());
                createFileMkdir(parentFileDir);
            }
        }
    }
   
}//end of namespace

// int main(int argc, char **argv)
// {
//   	ros::init(argc, argv, "micros_log_rec_store");
// 	micros_log::LogRecStore log_instance;	
//     log_instance.initLog();        
// 	ros::spin();

//   	return 0;
// }
