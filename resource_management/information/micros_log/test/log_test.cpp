#include <gtest/gtest.h>
#include "micros_log/log_rec_store.h"

void testCallback(const micros_log::MicRosLog msg);

/**
 * @brief Construct a new TEST object
 *  测试用例1-普通函数
 *  断言：单元测试函数返回值与给定的值是否相同
 * @param LogRecStore 测试用例名
 * @param test_levelNumToStr 测试名称
 */
TEST(LogRecStore,test_levelNumToStr)
{
    EXPECT_EQ("info",micros_log::levelNumToStr(2));
}

/**
 * @brief Construct a new TEST object
 * 测试用例2-类中成员函数
 * 断言：因函数是void类型，用异常的方式断言是否正确
 * @param LogRecStore 测试用例名
 * @param test_levelNumToStr 测试名称
 */
TEST(LogRecStore,test_CreateFileMkdir)
{
    micros_log::LogRecStore log_instance;
    EXPECT_NO_THROW(log_instance.createFileMkdir("/home/yyt/micros_log/"));
}

/**
 * @brief Construct a new TEST object
 *  测试用例3-类中成员函数并且为消息sub的回调函数
 *  断言：构造发布msg数据通过类中方法发布后再构造接收信息判断信息数据是否一致
 * @param LogRecStore 测试用例名
 * @param test_levelNumToStr 测试名称
 */
TEST(LogRecStore,test_callback)
{
  //1.生成pub对象并注册到类对象中
  micros_log::LogRecStore log_instance;
  ros::NodeHandle nh;
  ros::Publisher pub = nh.advertise<micros_log::MicRosLog>("test",10);
  log_instance.setLogsPub(pub);
  //2.构造智能指针数据并调用待测试的函数信息
  //boost::shared_ptr<rosgraph_msgs::Log> ConstPtr;//智能指针未实例化会报错
  boost::shared_ptr<rosgraph_msgs::Log> ConstPtr(new rosgraph_msgs::Log());//智能指针实例化
  ConstPtr->name = "YYT";
  log_instance.rosoutRecCallback(ConstPtr);
  //3.构造sub对象接收待测回调函数中pub的数据信息
  ros::Subscriber sub = nh.subscribe<micros_log::MicRosLog>("test",1,&testCallback);
}
/**
 * @brief 上面测试用例中调用的回调函数
 * 
 * @param msg 
 */
void testCallback(const micros_log::MicRosLog msg){
    std::string result_name = msg.name;
    std::string expected_str = "YYT";
    EXPECT_STREQ(expected_str.c_str(),result_name.c_str());
}


/**
 * @brief 主函数
 * 
 * @param argc 
 * @param argv 
 * @return int 
 */
int main(int argc, char **argv){
    testing::InitGoogleTest(&argc, argv);
    ros::init(argc, argv, "test_micros_log");
    ros::NodeHandle nh;
    return RUN_ALL_TESTS();
}