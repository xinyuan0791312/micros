/*=================================================================
* Copyright (c) 2016, micROS Group, NIIDT, TAIIC, HPCL.
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without modification, are permitted
* provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice, this list of conditions
*   and the following disclaimer.
* 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions
*   and the following disclaimer in the documentation and/or other materials provided with the distribution.
* 3. All advertising materials mentioning features or use of this software must display the following
*   acknowledgement: This product includes software developed by the micROS Group and its contributors.
* 4. Neither the name of the Group nor the names of contributors may be used to endorse or promote
*   products derived from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY MICROS GROUP AND CONTRIBUTORS ''AS IS''AND ANY EXPRESS OR IMPLIED WARRANTIES,
* INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
* PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE MICROS, GROUP OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
* GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION). HOWEVER CAUSED AND ON ANY THEORY
* OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
* WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
===================================================================
* Author: Bin Lin, Jun Cai, Songchang Jin, Jie Yan.
*/

#ifndef SRC_INFORMATION_VIRTUAL_SCENE_SYSTEM_INCLUDE_VIRTUAL_SCENE_SYSTEM_VIRTUALSCENESYSTEM_H_
#define SRC_INFORMATION_VIRTUAL_SCENE_SYSTEM_INCLUDE_VIRTUAL_SCENE_SYSTEM_VIRTUALSCENESYSTEM_H_

#include <pluginlib/class_loader.h>
#include "virtual_scene_system/SceneBase.h"
#include <ros/ros.h>

namespace micros_scene
{

/**
 * @brief The VirtualSceneSystem class, mount a scene, similar to the virtual file system(VFS)
 */
class VirtualSceneSystem
{
public:
	VirtualSceneSystem();
  ~VirtualSceneSystem();

  /**
   * @brief sMount, mout a scene, the scene type is set by the param "scene_type" in the virtual_scene_system_daemon_node.launch file
   */
  void sMount();

  /**
   * @brief sInit, init the mouted scene
   */
  void sInit();

  /**
   * @brief sRead, start reading data customized by the opened scene xml.
   */
  void sRead();

  /**
   * @brief sClose, stop reading data customized by the opened scene xml.
   */
  void sClose();

  /**
   * @brief sOpen, open a scene xml.
   * @param aXmlPath, the name and path of target scene xml.
   * @param RECORD, whether to record the scene result to a rosbag.
   * @param aRecordPath, the name and path of the rosbag to record scene result.
   */
  void sOpen(std::string aXmlPath, bool RECORD, std::string aRecordPath);

  /**
   * @brief sWrite, write a ros msg to the scene pool(database, file, etc.).
   * @param aMsg, the ros msg to be write.
   * @param isEvent, whether the msg is an event msg.
   * @param saveEvent, whether to save the event msg to the scene pool.
   * @return
   */
  template<typename T>
  int32_t sWrite(T aMsg, bool isEvent=false, bool saveEvent=true)
  {
	  if(_isAorB)
		  _sceneLoaderPtrA->sWrite(_sceneLoaderPtrA->msgConvert(aMsg), isEvent, saveEvent);
	  else
		  _sceneLoaderPtrB->sWrite(_sceneLoaderPtrB->msgConvert(aMsg), isEvent, saveEvent);
  }


private:
	pluginlib::ClassLoader<micros_scene::SceneBase> *_sceneLoader;
	bool _isAorB;

  /**
   * @brief _sceneLoaderPtrA, Pointer to loaded class loaded by pluginlib::ClassLoader
   */
  boost::shared_ptr<micros_scene::SceneBase>  _sceneLoaderPtrA;
  /**
   * @brief _sceneLoaderPtrB, Pointer to loaded class loaded by class_loader::class_loader_private::FactoryMap
   */
  micros_scene::SceneBase  *_sceneLoaderPtrB;
};

}  // namespace micros_scene

#endif /* SRC_INFORMATION_VIRTUAL_SCENE_SYSTEM_INCLUDE_VIRTUAL_SCENE_SYSTEM_VIRTUALSCENESYSTEM_H_ */
