/*=================================================================
* Copyright (c) 2016, micROS Group, NIIDT, TAIIC, HPCL.
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without modification, are permitted
* provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice, this list of conditions
*   and the following disclaimer.
* 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions
*   and the following disclaimer in the documentation and/or other materials provided with the distribution.
* 3. All advertising materials mentioning features or use of this software must display the following
*   acknowledgement: This product includes software developed by the micROS Group and its contributors.
* 4. Neither the name of the Group nor the names of contributors may be used to endorse or promote
*   products derived from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY MICROS GROUP AND CONTRIBUTORS ''AS IS''AND ANY EXPRESS OR IMPLIED WARRANTIES,
* INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
* PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE MICROS, GROUP OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
* GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION). HOWEVER CAUSED AND ON ANY THEORY
* OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
* WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
===================================================================
* Author: Bin Lin, Jun Cai, Songchang Jin, Jie Yan.
*/

#ifndef SRC_INFORMATION_VIRTUAL_SCENE_SYSTEM_INCLUDE_VIRTUAL_SCENE_SYSTEM_SCENEBASE_H_
#define SRC_INFORMATION_VIRTUAL_SCENE_SYSTEM_INCLUDE_VIRTUAL_SCENE_SYSTEM_SCENEBASE_H_

#include <string>
#include <topic_tools/shape_shifter.h>
#include <ros/message_operations.h>
#include <boost/regex.hpp>

namespace micros_scene
{
using std::string;
using std::vector;

/**
 * @brief The CommonMsg struct
 */
struct CommonMsg
{
  /**
   * @brief data, serialize data of a ros msg
   */
	vector<uint8_t> data;
  /**
   * @brief dataType, data type of a ros msg
   */
	string dataType;
  /**
   * @brief contentTag, json content of a ros msg
   */
	string contentTag;
};

/**
 * @brief The SceneBase class, the base class of all kinds of scenes
 */
class SceneBase
{
public:
	SceneBase(){}

  /**
   * @brief sInit, init a scene
   */
  virtual void sInit(){}
  /**
   * @brief sOpen, open a scene xml
   * @param aXmlPath, the name and path of target scene xml
   * @param RECORD, whether to record the scene result to a rosbag
   * @param aRecordPath, the name and path of the rosbag to record scene result
   * @return
   */
  virtual int32_t sOpen(std::string aXmlPath, bool RECORD, std::string aRecordPath){return 0;}

  /**
   * @brief sRead, start reading data customized by the opened scene xml
   */
  virtual void sRead(){}

  /**
   * @brief sClose, start reading data customized by the opened scene xml
   */
  virtual void sClose(){}

  /**
   * @brief sWrite, write a msg to the scene pool(database, file, etc.).
   * @param aCommonMsg, a three tuple format, all kinds of ros msg can convert to this format
   * @param isEvent, whether the msg is an event msg
   * @param saveEvent, whether to save the event to  the scene pool
   * @return
   */
  virtual int32_t sWrite(CommonMsg aCommonMsg, bool isEvent=false, bool saveEvent=true){return 0;}

  virtual ~SceneBase(){}

  /**
   * @brief msgConvert, convert a ros msg to struct CommonMsg
   * @param aMsg, the ros msg to be converted
   * @return
   */
  template<typename T>
  struct CommonMsg  msgConvert(T aMsg)
  {

	  struct CommonMsg tmp;
	  vector<uint8_t> vec=serializeRos(aMsg);
	  tmp.data=vec;

	  ros::message_traits::DataType<T> msgType;
	  tmp.dataType = msgType.value();

	  tmp.contentTag = toJSONSchema(aMsg);

	  return tmp;
  }

  /**
   * @brief serializeRos, to serialize a ros msg to vector<uint8_t>.
   * @param aMsg, the ros msg to be serialized.
   * @return
   */
  template<typename T>
  std::vector<uint8_t> serializeRos(T aMsg)
  {
    std::vector<uint8_t> vec;
    uint32_t serialSize = ros::serialization::serializationLength(aMsg);
    boost::shared_array<uint8_t> buffer(new uint8_t[serialSize]);
    ros::serialization::OStream ostream(buffer.get(), serialSize);
    ros::serialization::serialize(ostream, aMsg);
    vec.resize(serialSize);
    std::copy(buffer.get(), buffer.get() + serialSize, vec.begin());
    return vec;
  }

  /**
   * @brief toJSONSchema, extract the content of a ros msg to JSON schema
   * @param aMsg
   * @return
   */
  template<typename T>
	std::string toJSONSchema(T aMsg)
	{
	  std::string msg2json = "{ ";
	  bool hasContent = 0;

	  ros::message_operations::Printer<T> p;
	  std::ostringstream ss;
	  p.stream(ss, "", aMsg);
	  std::string data = ss.str();

	  std::vector<std::string> pathHeader;

	  boost::regex reg("\n");
	  boost::sregex_token_iterator it(data.begin(), data.end(), reg, -1);
	  boost::sregex_token_iterator end;
	  while(it != end)
	  {
		std::string t = *it;

		if((t.find("[") == t.npos) && (t.find("]") == t.npos))
		{
		  if(t.find(": ") == (t.length()-2))
		  {
			t = t.substr(0, t.find(":"));

			int32_t num = 0;
			if(t.find(" ") != t.npos)
			  num = (t.find_last_of(" ") + 1)/2;

			if(t.find(" ") != t.npos)
			  t = t.substr(t.find_last_of(" ")+1, t.length()-1);

			if(num == pathHeader.size())
			  pathHeader.push_back(t);
			else
			{
			  int32_t popNum = pathHeader.size() - num;
			  for(int i = 0;i<popNum;i++)
				pathHeader.pop_back();

			  pathHeader.push_back(t);
			}
		  }
		  else
		  {
			std::string type = t.substr(0, t.find(": "));
			std::string value = t.substr(t.find(": ")+2, t.length()-1);

			int32_t num = 0;
			if(type.find(" ") != t.npos)
			  num = (type.find_last_of(" ") + 1)/2;

			if(num == pathHeader.size())
			{
			  if(num > 0)
			  {
				std::string path = pathHeader[0] + "/";
				for(int32_t i = 1;i<pathHeader.size();i++)
				  path = path + pathHeader[i] + "/";
				type = path + type.substr(type.find_last_of(" ")+1, type.length()-1);
			  }
			}
			else
			{
			  int32_t j = pathHeader.size() - num;
			  for(int32_t i = 0;i<j;i++)
				pathHeader.pop_back();

			  if(pathHeader.size() > 0)
			  {
				std::string path = pathHeader[0] + "/";
				for(int32_t i = 1;i<pathHeader.size();i++)
				  path = path + pathHeader[i] + "/";
				type = path + type.substr(type.find_last_of(" ")+1, type.length()-1);
			  }
			}

			if(hasContent)
			  msg2json = msg2json + ", " + "\"" + type.c_str() + "\"" + ": " + "\"" + value.c_str() + "\"";
			else
			  msg2json = msg2json + + "\"" + type.c_str() + "\"" + ": " + "\"" + value.c_str() + "\"";

			hasContent = 1;
		  }

		  it++;
		}
		else
		{
		  it++;
		}
	  }

	  msg2json = msg2json + " }";

	  return msg2json;
	}

  /**
   * @brief setSceneType, set the type of a scene
   * @param aSceneType
   */
  void setSceneType(std::string aSceneType)
  {
   _sceneType=aSceneType;
  }

  /**
   * @brief getSceneType, get the type of a scene
   * @return
   */
  std::string getSceneType() const
  {
    return _sceneType;
  }


protected:
  std::string _sceneType;
};

}  // namespace micros_scene

#endif /* SRC_INFORMATION_VIRTUAL_SCENE_SYSTEM_INCLUDE_VIRTUAL_SCENE_SYSTEM_SCENEBASE_H_ */
