#include <signal.h>
#include <ros/ros.h>
#include <boost/thread/thread.hpp>
#include <string>
#include "virtual_scene_system/VirtualSceneSystem.h"

#include <fastrtps/participant/Participant.h>
#include <fastrtps/attributes/ParticipantAttributes.h>
#include <fastrtps/Domain.h>
#include "fastrtps_pubsub/Fastrtps_PacketPublisher.h"

using namespace eprosima::fastrtps;
using namespace eprosima::fastrtps::rtps;

/**
 * @brief mp_participant, a fastrtps participant shared by multi cpp files
 */
extern eprosima::fastrtps::Participant *mp_participant;
