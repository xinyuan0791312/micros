/*=================================================================
* Copyright (c) 2019, micROS Group, NIIDT, TAIIC, HPCL.
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without modification, are permitted
* provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice, this list of conditions and
* the following disclaimer.
* 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions
* and the following disclaimer in the documentation and/or other materials provided with the
* distribution.
* 3. All advertising materials mentioning features or use of this software must display the following
* acknowledgement: This product includes software developed by the micROS Group and its
* contributors.
* 4. Neither the name of the Group nor the names of contributors may be used to endorse or promote
* products derived from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY MICROS GROUP AND CONTRIBUTORS ''AS IS''AND
* ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
* PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE MICROS, GROUP OR
* CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
* EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
* PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
* PROFITS; OR BUSINESS INTERRUPTION). HOWEVER CAUSED AND ON ANY THEORY
* OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
* NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
* SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
===================================================================
* Author: JunCai.
*/
#include "virtual_scene_system/VirtualSceneSystem.h"
#include <class_loader/multi_library_class_loader.h>
#include <class_loader/class_loader_core.hpp>

namespace micros_scene
{


VirtualSceneSystem::VirtualSceneSystem (){
	  _sceneLoader = new pluginlib::ClassLoader<micros_scene::SceneBase>("virtual_scene_system", "micros_scene::SceneBase");
}

VirtualSceneSystem::~VirtualSceneSystem(){
	delete _sceneLoader;
}

void VirtualSceneSystem::sInit()
{
	  if(_isAorB)
		  _sceneLoaderPtrA->sInit();
	  else
		  _sceneLoaderPtrB->sInit();

	ROS_INFO("[scene][VirtualSceneSystem] Scene resource init success!!!");
}

void VirtualSceneSystem::sOpen(std::string aXmlPath, bool RECORD, std::string aRecordPath)
{
	 if(_isAorB)
		 _sceneLoaderPtrA->sOpen(aXmlPath, RECORD, aRecordPath);
	 else
		 _sceneLoaderPtrB->sOpen(aXmlPath, RECORD, aRecordPath);
}

void VirtualSceneSystem::sRead()
{
	if(_isAorB)
		_sceneLoaderPtrA->sRead();
	else
		_sceneLoaderPtrB->sRead();
}

void VirtualSceneSystem::sClose()
{
	if(_isAorB)
		_sceneLoaderPtrA->sClose();
	else
		_sceneLoaderPtrB->sClose();
}

void VirtualSceneSystem::sMount()
{
	  ros::NodeHandle nh;
	  std::string sceneType;
	  bool param_ok = nh.getParam("/scene_type", sceneType);
	  if(!param_ok) {
	      ROS_WARN("[scene][sMount] Could not get parameter scene_type, use default scene type.");
	      sceneType = "pioneer_scene/PioneerScene";
	  }

    try
    {
        std::string baseClassName = _sceneLoader->getBaseClassType();
        std::string className = _sceneLoader->getClassType(sceneType);

       class_loader::class_loader_private::AbstractMetaObject<micros_scene::SceneBase> * factory = NULL;

       class_loader::class_loader_private::FactoryMap m_factory = class_loader::class_loader_private::getFactoryMapForBaseClass<micros_scene::SceneBase>();
        if (m_factory.find(className) != m_factory.end()) {
        	ROS_INFO("[scene][sMount] Using class_loader to mount a scene.");
			factory = dynamic_cast<class_loader::class_loader_private::AbstractMetaObject<micros_scene::SceneBase> *>(m_factory[className]);
			micros_scene::SceneBase * obj = NULL;
			obj = factory->create();
			_sceneLoaderPtrB = obj;
			_isAorB = false;
			_sceneLoaderPtrB->setSceneType(sceneType);
        } else {
            ROS_INFO("[scene][sMount] Using pluginlib to mount a scene.");
        	  _isAorB = true;
        	  _sceneLoaderPtrA = _sceneLoader->createInstance(sceneType);
		      _sceneLoaderPtrA->setSceneType(sceneType);
          }

    }
    catch(pluginlib::PluginlibException& ex)
    {
      ROS_ERROR("[scene][sMount] Failed to load scene for some reason. Error: %s", ex.what());
    }

    if(_isAorB)
    	 ROS_INFO("[scene][VirtualSceneSystem] Mount scene %s  success!!!!!!" , _sceneLoaderPtrA->getSceneType().c_str());
    else
    	 ROS_INFO("[scene][VirtualSceneSystem] Mount scene %s  success!!!!!!" , _sceneLoaderPtrB->getSceneType().c_str());
}

}  // namespace micros_scene



