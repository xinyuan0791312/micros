/*=================================================================
* Copyright (c) 2019, micROS Group, NIIDT, TAIIC, HPCL.
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without modification, are permitted
* provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice, this list of conditions and
* the following disclaimer.
* 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions
* and the following disclaimer in the documentation and/or other materials provided with the
* distribution.
* 3. All advertising materials mentioning features or use of this software must display the following
* acknowledgement: This product includes software developed by the micROS Group and its
* contributors.
* 4. Neither the name of the Group nor the names of contributors may be used to endorse or promote
* products derived from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY MICROS GROUP AND CONTRIBUTORS ''AS IS''AND
* ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
* PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE MICROS, GROUP OR
* CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
* EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
* PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
* PROFITS; OR BUSINESS INTERRUPTION). HOWEVER CAUSED AND ON ANY THEORY
* OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
* NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
* SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
===================================================================
* Author: Cai Jun.
*/

#include "virtual_scene_system/VirtualSceneSystemDaemonNode.h"
#include <ros/package.h>

eprosima::fastrtps::Participant *mp_participant = NULL;


void virtualSceneSystemDestroySigintHandler(int sig)
{
	Domain::removeParticipant(mp_participant);
  ros::shutdown();
}

int main(int argc, char** argv)
{
  ros::init(argc, argv, "virtual_scene_system_daemon_node");

  Fastrtps_PacketPublisher RTPS_Publisher;
  std::string path = ros::package::getPath("virtual_scene_system");
  std::string file = path + "/src/informationRTPS.xml";
  RTPS_Publisher.createRTPSParticipant(mp_participant, 50, "Participant_information", file.c_str());
  Fastrtps_PacketPubSubType myType;
  Domain::registerType(mp_participant, static_cast<TopicDataType*>(&myType));

  micros_scene::VirtualSceneSystem virtualSceneSystem;
  virtualSceneSystem.sMount();
  virtualSceneSystem.sInit();

  // Override the default ros sigint handler.
  // This must be set after the first NodeHandle is created.
  // SIGINT：输入CTRL+C
  signal(SIGINT, virtualSceneSystemDestroySigintHandler);

  boost::thread t = boost::thread(boost::bind(&ros::spin));
  t.join();
  return 0;
}



