cmake_minimum_required(VERSION 2.8.3)
project(virtual_scene_system)

## Compile as C++11, supported in ROS Kinetic and newer
add_compile_options(-std=c++11)

find_package(catkin REQUIRED COMPONENTS
  roscpp
  rospy
  std_msgs
  pluginlib
  fastrtps_pubsub
)

catkin_package(
    INCLUDE_DIRS include
    CATKIN_DEPENDS roscpp rospy std_msgs  pluginlib
)

###########
## Build ##
###########

include_directories(
  include
  ${catkin_INCLUDE_DIRS}
)

## Declare a C++ library
add_library(${PROJECT_NAME}
   src/VirtualSceneSystem.cpp
   src/VirtualSceneSystemDaemonNode.cpp
 )

target_link_libraries(${PROJECT_NAME}
   ${catkin_LIBRARIES}
 )

add_executable(VirtualSceneSystemDaemonNode 
    src/VirtualSceneSystem.cpp
    src/VirtualSceneSystemDaemonNode.cpp
)

target_link_libraries(VirtualSceneSystemDaemonNode
   ${catkin_LIBRARIES}
 )
