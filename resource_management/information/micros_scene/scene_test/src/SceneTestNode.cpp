/*=================================================================
* Copyright (c) 2019, micROS Group, NIIDT, TAIIC, HPCL.
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without modification, are permitted
* provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice, this list of conditions and
* the following disclaimer.
* 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions
* and the following disclaimer in the documentation and/or other materials provided with the
* distribution.
* 3. All advertising materials mentioning features or use of this software must display the following
* acknowledgement: This product includes software developed by the micROS Group and its
* contributors.
* 4. Neither the name of the Group nor the names of contributors may be used to endorse or promote
* products derived from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY MICROS GROUP AND CONTRIBUTORS ''AS IS''AND
* ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
* PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE MICROS, GROUP OR
* CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
* EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
* PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
* PROFITS; OR BUSINESS INTERRUPTION). HOWEVER CAUSED AND ON ANY THEORY
* OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
* NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
* SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
===================================================================
* Author: JunCai.
*/
#include <signal.h>
#include <ros/ros.h>
#include <boost/thread/thread.hpp>
#include <string>
#include "virtual_scene_system/VirtualSceneSystem.h"

#include <abstract_msgs/QueryResult.h>
#include "std_msgs/String.h"

micros_scene::VirtualSceneSystem virtualSceneSystem;

template<typename T>
T deserializeRos(std::vector<uint8_t> &aVec)
{
  T t;
  uint32_t serialSize = aVec.size();
  std::vector<uint8_t> buffer(serialSize);
  std::copy(aVec.begin(), aVec.begin() + serialSize, buffer.begin());
  ros::serialization::IStream istream(buffer.data(), serialSize);
  ros::serialization::Serializer<T>::read(istream, t);
  return t;
}

void scene_result_process(const abstract_msgs::QueryResult &msg)
{

	abstract_msgs::UnifiedData  data;
	data = msg.data;

	std::cout << "\n\nreceive data from actor  " << data.actorName << ", whose robot ID is " << data.robotID << std::endl;
	//std::cout<<"receive data size is  "<<sizeof(msg)<<std::endl;
	std::cout << "scene name: " << msg.sceneName << ", layer name: " << msg.layerName << std::endl;
	std::cout << "msg number: " << msg.count << std::endl;
	std::cout << "robot position: x=" << data.positionX << ", y=" << data.positionY << ", z=" << data.positionZ << std::endl;

	std::cout << "data source: " << data.dataType << std::endl;

	if(data.dataType == "monitor_string")
	{
		std_msgs::String str;
		str = deserializeRos<std_msgs::String>(data.data);
		std::cout << "error code: " << str.data.c_str() << std::endl;
	}
}

void sceneStopSigintHandler(int sig)
{
    // Do some custom action.
    // For example, publish a stop message to some other nodes.
    // All the default sigint handler does is call shutdown()
	virtualSceneSystem.sClose();
	sleep(1);
    ros::shutdown();
}


int main(int argc, char** argv)
{
	ros::init(argc, argv, "scene_test_node");
	ros::NodeHandle nh;

	virtualSceneSystem.sMount();
	virtualSceneSystem.sOpen( "/home/cj/scene.xml",false,"/home/cj/test.bag");

	ros::Subscriber scene_result_sub = nh.subscribe("/GroudStationScene",100, scene_result_process);

	virtualSceneSystem.sRead();

	//SIGINT：输入CTRL+C
    signal(SIGINT, sceneStopSigintHandler);

	//ros::spin();
    boost::thread t = boost::thread(boost::bind(&ros::spin));
    t.join();

	return 0;
}






