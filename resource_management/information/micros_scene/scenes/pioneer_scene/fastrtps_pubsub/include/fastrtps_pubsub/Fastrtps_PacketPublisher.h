// Copyright 2016 Proyectos y Sistemas de Mantenimiento SL (eProsima).
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/*!
 * @file Fastrtps_PacketPublisher.h
 * This header file contains the declaration of the publisher functions.
 *
 * This file was generated by the tool fastcdrgen.
 */


#ifndef _FASTRTPS_PACKET_PUBLISHER_H_
#define _FASTRTPS_PACKET_PUBLISHER_H_

#include <fastrtps/fastrtps_fwd.h>
#include <fastrtps/publisher/PublisherListener.h>

#include "Fastrtps_PacketPubSubTypes.h"

/**
 * @brief The Fastrtps_PacketPublisher class
 */
class Fastrtps_PacketPublisher
{
public:
	Fastrtps_PacketPublisher();
	virtual ~Fastrtps_PacketPublisher();
  /**
   * @brief run, using for test
   */
	void run();

  /**
   * @brief getParticipant, get the Participant user created by using createRTPSParticipant
   * @param p_participant, the target participant
   * @return
   */
	bool getParticipant(eprosima::fastrtps::Participant *p_participant);

  /**
   * @brief init, init the RTPS publisher using static configure
   * @param m_topic, the name of the topic to publish data
   * @param aUserDefinedID, the id of writer defined by user
   * @return
   */
	bool init(std::string m_topic, uint8_t aUserDefinedID);


	  /**
	   * @brief init, init the RTPS publisher using dynamic configure
	   * @param m_topic, the name of the topic to publish data
	   * @return
	   */
		bool init(std::string m_topic);

	  /**
	   * @brief init, also used to init the RTPS publisher using dynamic configure, users can use this function to set the reliability QoS policy
	   * @param m_topic, the name of the topic to publish data
	   * @param isSetQos, whether to set QoS
	   * @param isReliable, if this param is true, the reliability QoS policy will be set to RELIABLE_RELIABILITY_QOS,
	   * otherwise, the reliability QoS policy will be set to BEST_EFFORT_RELIABILITY_QOS
	   * @return
	   */
		bool init(std::string m_topic, bool isSetQoS, bool isReliable);

  /**
   * @brief DDS_pub, pub data to the RTPS topic
   * @param msg_data, the data to be publish
   * @return
   */
	bool DDS_pub(const std::vector<uint8_t>& msg_data);

  /**
   * @brief createRTPSParticipant, create an RTPS Participant using static configure
   * @param aParticipant, target Participant
   * @param aDomainId, the ID of domain the target participant will join
   * @param aParticipantName, the name of target Participant
   */
	void createRTPSParticipant(eprosima::fastrtps::Participant* &aParticipant, uint32_t aDomainId, std::string aParticipantName, std::string aConfigFile);

	/**
	 * @brief createRTPSParticipant, create an RTPS Participant using dynamic configure
	 * @param aParticipant, target Participant
	 * @param aDomainId, the ID of domain the target participant will join
	 * @param aParticipantName, the name of target Participant
	 */
      void createRTPSParticipant(eprosima::fastrtps::Participant* &aParticipant, uint32_t aDomainId, std::string aParticipantName);

private:
	eprosima::fastrtps::Participant *mp_participant;
	eprosima::fastrtps::Publisher *mp_publisher;

	class PubListener : public eprosima::fastrtps::PublisherListener
	{
	public:
		PubListener() : n_matched(0){};
		~PubListener(){};
		void onPublicationMatched(eprosima::fastrtps::Publisher* pub,eprosima::fastrtps::rtps::MatchingInfo& info);
		int n_matched;
	} m_listener;
	Fastrtps_PacketPubSubType myType;
};

#endif // _FASTRTPS_PACKET_PUBLISHER_H_
