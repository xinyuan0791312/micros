// Copyright 2016 Proyectos y Sistemas de Mantenimiento SL (eProsima).
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/*!
 * @file Fastrtps_PacketPublisher.cpp
 * This file contains the implementation of the publisher functions.
 *
 * This file was generated by the tool fastcdrgen.
 */


#include <fastrtps/participant/Participant.h>
#include <fastrtps/attributes/ParticipantAttributes.h>
#include <fastrtps/publisher/Publisher.h>
#include <fastrtps/attributes/PublisherAttributes.h>

#include <fastrtps/Domain.h>

#include <fastrtps/utils/eClock.h>

#include "fastrtps_pubsub/Fastrtps_PacketPublisher.h"

using namespace eprosima::fastrtps;
using namespace eprosima::fastrtps::rtps;

Fastrtps_PacketPublisher::Fastrtps_PacketPublisher() : mp_publisher(nullptr) {}
//Fastrtps_PacketPublisher::Fastrtps_PacketPublisher() : mp_participant(nullptr), mp_publisher(nullptr) {}

Fastrtps_PacketPublisher::~Fastrtps_PacketPublisher() {}
//Fastrtps_PacketPublisher::~Fastrtps_PacketPublisher() {	Domain::removeParticipant(mp_participant);}

/*bool Fastrtps_PacketPublisher::init()
{
    // Create RTPSParticipant

    ParticipantAttributes PParam;
    PParam.rtps.builtin.domainId = 0;
    PParam.rtps.builtin.leaseDuration = c_TimeInfinite;
    PParam.rtps.setName("Participant_publisher");  //You can put here the name you want
    mp_participant = Domain::createParticipant(PParam);
    if(mp_participant == nullptr)
    {
        return false;
    }

    //Register the type

    Domain::registerType(mp_participant, static_cast<TopicDataType*>(&myType));

    // Create Publisher

    PublisherAttributes Wparam;
    Wparam.topic.topicKind = NO_KEY;
    Wparam.topic.topicDataType = myType.getName();  //This type MUST be registered
    Wparam.topic.topicName = "Fastrtps_PacketPubSubTopic";

    mp_publisher = Domain::createPublisher(mp_participant,Wparam,static_cast<PublisherListener*>(&m_listener));

    if(mp_publisher == nullptr)
    {
        return false;
    }

    std::cout << "Publisher created, waiting for Subscribers." << std::endl;
    return true;
}*/

bool Fastrtps_PacketPublisher::getParticipant(eprosima::fastrtps::Participant *p_participant)
{
	if(p_participant==nullptr)
		return false;
	else
		mp_participant = p_participant;

	return true;
}

//static configure
void Fastrtps_PacketPublisher::createRTPSParticipant(eprosima::fastrtps::Participant* &aParticipant, uint32_t aDomainId, std::string aParticipantName, std::string aConfigFile)
{
	ParticipantAttributes PParam;
	//PParam.rtps.builtin.discovery_config.discoveryProtocol = DiscoveryProtocol_t::SIMPLE;
	//PParam.rtps.builtin.discovery_config.use_SIMPLE_EndpointDiscoveryProtocol = true;
	//PParam.rtps.builtin.discovery_config.m_simpleEDP.use_PublicationReaderANDSubscriptionWriter = true;
	//PParam.rtps.builtin.discovery_config.m_simpleEDP.use_PublicationWriterANDSubscriptionReader = true;

	PParam.rtps.builtin.discovery_config.use_SIMPLE_EndpointDiscoveryProtocol = false;
	PParam.rtps.builtin.discovery_config.use_STATIC_EndpointDiscoveryProtocol = true;
	PParam.rtps.builtin.discovery_config.setStaticEndpointXMLFilename(aConfigFile.c_str());

	PParam.rtps.builtin.domainId = aDomainId;
	//PParam.rtps.builtin.discovery_config.leaseDuration = c_TimeInfinite;
	//PParam.rtps.setName("Participant_information");
	PParam.rtps.setName(aParticipantName.c_str());
	aParticipant = Domain::createParticipant(PParam);
    if(aParticipant != nullptr)
    {
    	std::cout<<"creating RTPSParticipant " << aParticipantName.c_str() << " success!!!"<<std::endl;
    }
	return;
}

//static configure
bool Fastrtps_PacketPublisher::init(std::string m_topic, uint8_t aUserDefinedID)
{
	// Create RTPSParticipant
	/*
    ParticipantAttributes PParam;
    PParam.rtps.builtin.discovery_config.discoveryProtocol = DiscoveryProtocol_t::SIMPLE;
    PParam.rtps.builtin.discovery_config.use_SIMPLE_EndpointDiscoveryProtocol = true;
    PParam.rtps.builtin.discovery_config.m_simpleEDP.use_PublicationReaderANDSubscriptionWriter = true;
    PParam.rtps.builtin.discovery_config.m_simpleEDP.use_PublicationWriterANDSubscriptionReader = true;

    PParam.rtps.builtin.domainId = domainId;
    PParam.rtps.builtin.discovery_config.leaseDuration = c_TimeInfinite;
    PParam.rtps.setName("Participant_pub");
    mp_participant = Domain::createParticipant(PParam);
    */

    if(mp_participant==nullptr)
    {
    	std::cout<<"RTPSParticipant has not created!!!"<<std::endl;
        return false;
    }

	//Register the type

    //Register the type

    //Domain::registerType(mp_participant, static_cast<TopicDataType*>(&myType));

    //CREATE THE PUBLISHER

    PublisherAttributes Wparam;
    Wparam.topic.topicKind = NO_KEY;
    Wparam.topic.topicDataType = myType.getName();
    std::cout<<myType.getName()<<std::endl;
    Wparam.topic.topicName = m_topic;
    //Wparam.topic.historyQos.kind = KEEP_LAST_HISTORY_QOS;
    //Wparam.topic.historyQos.depth = 30;
    //Wparam.topic.resourceLimitsQos.max_samples = 50;
    //Wparam.topic.resourceLimitsQos.allocated_samples = 20;
    //Wparam.times.heartbeatPeriod.seconds = 2;
    //Wparam.times.heartbeatPeriod.nanosec = 200*1000*1000;
    //Wparam.qos.m_reliability.kind = RELIABLE_RELIABILITY_QOS;
    Wparam.qos.m_reliability.kind = BEST_EFFORT_RELIABILITY_QOS;
    std::cout<<"user defined id: "<<int(aUserDefinedID)<<std::endl;
    Wparam.setUserDefinedID(aUserDefinedID);
    Wparam.qos.m_publishMode.kind = ASYNCHRONOUS_PUBLISH_MODE;
    Wparam.historyMemoryPolicy = PREALLOCATED_WITH_REALLOC_MEMORY_MODE;
    mp_publisher = Domain::createPublisher(mp_participant,Wparam,(PublisherListener*)&m_listener);
    if(mp_publisher == nullptr)
        return false;

    std::cout << "Publisher created, waiting for Subscribers." << std::endl;
    return true;
}


//dynamic configure
void Fastrtps_PacketPublisher::createRTPSParticipant(eprosima::fastrtps::Participant* &aParticipant, uint32_t aDomainId, std::string aParticipantName)
{
	ParticipantAttributes PParam;
	PParam.rtps.builtin.discovery_config.discoveryProtocol = DiscoveryProtocol_t::SIMPLE;
	PParam.rtps.builtin.discovery_config.use_SIMPLE_EndpointDiscoveryProtocol = true;
	PParam.rtps.builtin.discovery_config.m_simpleEDP.use_PublicationReaderANDSubscriptionWriter = true;
	PParam.rtps.builtin.discovery_config.m_simpleEDP.use_PublicationWriterANDSubscriptionReader = true;

	PParam.rtps.builtin.domainId = aDomainId;
	PParam.rtps.builtin.discovery_config.leaseDuration = c_TimeInfinite;
	//PParam.rtps.setName("Participant_information");
	PParam.rtps.setName(aParticipantName.c_str());
	aParticipant = Domain::createParticipant(PParam);
	return;
}

//dynamic configure
//bool Fastrtps_PacketPublisher::init(uint32_t  domainId, std::string m_topic)
bool Fastrtps_PacketPublisher::init(std::string m_topic)
{
	// Create RTPSParticipant
	/*
    ParticipantAttributes PParam;
    PParam.rtps.builtin.discovery_config.discoveryProtocol = DiscoveryProtocol_t::SIMPLE;
    PParam.rtps.builtin.discovery_config.use_SIMPLE_EndpointDiscoveryProtocol = true;
    PParam.rtps.builtin.discovery_config.m_simpleEDP.use_PublicationReaderANDSubscriptionWriter = true;
    PParam.rtps.builtin.discovery_config.m_simpleEDP.use_PublicationWriterANDSubscriptionReader = true;

    PParam.rtps.builtin.domainId = domainId;
    PParam.rtps.builtin.discovery_config.leaseDuration = c_TimeInfinite;
    PParam.rtps.setName("Participant_pub");
    mp_participant = Domain::createParticipant(PParam);
    */

    if(mp_participant==nullptr)
    {
    	std::cout<<"RTPSParticipant has not created!!!"<<std::endl;
        return false;
    }

	//Register the type

    //Register the type

    //Domain::registerType(mp_participant, static_cast<TopicDataType*>(&myType));

    //CREATE THE PUBLISHER

    PublisherAttributes Wparam;
    Wparam.topic.topicKind = NO_KEY;
    Wparam.topic.topicDataType = myType.getName();
    Wparam.topic.topicName = m_topic;
    Wparam.topic.historyQos.kind = KEEP_LAST_HISTORY_QOS;
    Wparam.topic.historyQos.depth = 30;
    Wparam.topic.resourceLimitsQos.max_samples = 50;
    Wparam.topic.resourceLimitsQos.allocated_samples = 20;
    Wparam.times.heartbeatPeriod.seconds = 2;
    Wparam.times.heartbeatPeriod.nanosec = 200*1000*1000;
    //Wparam.qos.m_reliability.kind = RELIABLE_RELIABILITY_QOS;
    Wparam.qos.m_reliability.kind = BEST_EFFORT_RELIABILITY_QOS;
    Wparam.qos.m_publishMode.kind = ASYNCHRONOUS_PUBLISH_MODE;
    Wparam.historyMemoryPolicy = PREALLOCATED_WITH_REALLOC_MEMORY_MODE;
    mp_publisher = Domain::createPublisher(mp_participant,Wparam,(PublisherListener*)&m_listener);
    if(mp_publisher == nullptr)
        return false;

    std::cout << "Publisher created, waiting for Subscribers." << std::endl;
    return true;
}

//dynamic configure
bool Fastrtps_PacketPublisher::init(std::string m_topic, bool isSetQoS, bool isReliable)
{
	// Create RTPSParticipant
	/*
    ParticipantAttributes PParam;
    PParam.rtps.builtin.discovery_config.discoveryProtocol = DiscoveryProtocol_t::SIMPLE;
    PParam.rtps.builtin.discovery_config.use_SIMPLE_EndpointDiscoveryProtocol = true;
    PParam.rtps.builtin.discovery_config.m_simpleEDP.use_PublicationReaderANDSubscriptionWriter = true;
    PParam.rtps.builtin.discovery_config.m_simpleEDP.use_PublicationWriterANDSubscriptionReader = true;

    PParam.rtps.builtin.domainId = domainId;
    PParam.rtps.builtin.discovery_config.leaseDuration = c_TimeInfinite;
    PParam.rtps.setName("Participant_pub");
    mp_participant = Domain::createParticipant(PParam);
    */

    if(mp_participant==nullptr)
    {
    	std::cout<<"RTPSParticipant has not created!!!"<<std::endl;
        return false;
    }

	//Register the type

    //Register the type

    //Domain::registerType(mp_participant, static_cast<TopicDataType*>(&myType));

    //CREATE THE PUBLISHER

    PublisherAttributes Wparam;
    Wparam.topic.topicKind = NO_KEY;
    Wparam.topic.topicDataType = myType.getName();
    Wparam.topic.topicName = m_topic;
    Wparam.topic.historyQos.kind = KEEP_LAST_HISTORY_QOS;
    Wparam.topic.historyQos.depth = 30;
    Wparam.topic.resourceLimitsQos.max_samples = 50;
    Wparam.topic.resourceLimitsQos.allocated_samples = 20;
    Wparam.times.heartbeatPeriod.seconds = 2;
    Wparam.times.heartbeatPeriod.nanosec = 200*1000*1000;
    if(isSetQoS&&isReliable)
    	Wparam.qos.m_reliability.kind = RELIABLE_RELIABILITY_QOS;
    else
    	Wparam.qos.m_reliability.kind = BEST_EFFORT_RELIABILITY_QOS;
    Wparam.qos.m_publishMode.kind = ASYNCHRONOUS_PUBLISH_MODE;
    Wparam.historyMemoryPolicy = PREALLOCATED_WITH_REALLOC_MEMORY_MODE;
    mp_publisher = Domain::createPublisher(mp_participant,Wparam,(PublisherListener*)&m_listener);
    if(mp_publisher == nullptr)
        return false;

    std::cout << "Publisher created, waiting for Subscribers." << std::endl;
    return true;
}

void Fastrtps_PacketPublisher::PubListener::onPublicationMatched(Publisher* pub,MatchingInfo& info)
{
    (void)pub;
    std::string remoteIP = std::to_string(info.remoteEndpointGuid.guidPrefix.value[2]) + "." + std::to_string(info.remoteEndpointGuid.guidPrefix.value[3]);
    if (info.status == MATCHED_MATCHING)
    {
        n_matched++;
        //std::cout << "Publisher matched" << std::endl;
        std::cout << "[" << pub->getAttributes().topic.topicName<<"]["<<remoteIP<<"] Publisher matched"<< std::endl;
    }
    else
    {
        n_matched--;
        //std::cout << "Publisher unmatched" << std::endl;
        std::cout << "[" << pub->getAttributes().topic.topicName<<"]["<<remoteIP<<"] Publisher unmatched"<< std::endl;
    }
}

bool Fastrtps_PacketPublisher::DDS_pub(const std::vector<uint8_t>& msg_data)
{
	Fastrtps_Packet st;
	st.data(msg_data);
    if(m_listener.n_matched > 0)
    {
    	mp_publisher->write(&st);
        return true;
    }
    return false;
}


void Fastrtps_PacketPublisher::run()
{
    while(m_listener.n_matched == 0)
    {
        eClock::my_sleep(250); // Sleep 250 ms
    }

    // Publication code

    Fastrtps_Packet st;

    /* Initialize your structure here */

    int msgsent = 0;
    char ch = 'y';
    do
    {
        if(ch == 'y')
        {
            mp_publisher->write(&st);  ++msgsent;
            std::cout << "Sending sample, count=" << msgsent << ", send another sample?(y-yes,n-stop): ";
        }
        else if(ch == 'n')
        {
            std::cout << "Stopping execution " << std::endl;
            break;
        }
        else
        {
            std::cout << "Command " << ch << " not recognized, please enter \"y/n\":";
        }
    } while(std::cin >> ch);
}
