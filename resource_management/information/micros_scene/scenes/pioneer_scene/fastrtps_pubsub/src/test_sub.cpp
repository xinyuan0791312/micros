/*=================================================================
* Copyright (c) 2016, micROS Group, NIIDT, TAIIC, HPCL.
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without modification, are permitted
* provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice, this list of conditions and
*       the following disclaimer.
* 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions
*      and the following disclaimer in the documentation and/or other materials provided with the
*      distribution.
* 3. All advertising materials mentioning features or use of this software must display the following
*      acknowledgement: This product includes software developed by the micROS Group and its
*      contributors.
* 4. Neither the name of the Group nor the names of contributors may be used to endorse or promote
*      products derived from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY MICROS GROUP AND CONTRIBUTORS ''AS IS''AND
* ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
* PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE MICROS, GROUP OR
* CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
* EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
* PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
* PROFITS; OR BUSINESS INTERRUPTION). HOWEVER CAUSED AND ON ANY THEORY
* OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
* NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
* SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
===================================================================
* Author: Jun Cai
*/

#include <iostream>
#include <ros/ros.h>

#include <fastrtps/Domain.h>
#include <fastrtps/participant/Participant.h>
#include <fastrtps/attributes/ParticipantAttributes.h>
#include <fastrtps/utils/eClock.h>
#include <fastrtps/log/Log.h>

#include <sensor_msgs/Image.h>
#include <sensor_msgs/CompressedImage.h>
#include <std_msgs/String.h>

#include "fastrtps_pubsub/Fastrtps_PacketSubscriber.h"
#include "fastrtps_pubsub/Fastrtps_PacketPublisher.h"
#include <ros/package.h>

using namespace eprosima;
using namespace eprosima::fastrtps;

ros::Publisher pub;
//sensor_msgs::Image image;
sensor_msgs::CompressedImage image;

// deserializer using ROS.
template<class T>
T deserialize_ros(const std::vector<uint8_t>& vec)
{
    T t;
    uint32_t serial_size = vec.size();
    std::vector<uint8_t> buffer(serial_size);
    std::copy(vec.begin(), vec.begin() + serial_size, buffer.begin());
    ros::serialization::IStream istream(buffer.data(), serial_size);
    ros::serialization::Serializer<T>::read(istream, t);
    return t;
}
/*void RTPS_callback(std::vector<char> msg)
{
    std::string str_temp;
    str_temp = std::string(reinterpret_cast<char*>(&msg[0]),msg.size());
    image = deserialize_ros<sensor_msgs::Image>(str_temp);
    //image = deserialize_ros<sensor_msgs::CompressedImage>(str_temp);
    std::cout << "length:::" << str_temp.size() << std::endl;
    if(pub == NULL)
    {
        ros::NodeHandle nh;
        pub = nh.advertise<sensor_msgs::Image>("/ImagequeryResult",1000);
        //pub = nh.advertise<sensor_msgs::CompressedImage>("/ImagequeryResult",1000);
        std::cout << "test advertise" << std::endl;
    }
    // cv_bridge::CvImage(std_msgs::Header(), "bgr8", imgRes).toImageMsg();
    pub.publish(image);
}*/

void RTPS_callback(std::vector<uint8_t> msg)
{
    //std::string str_temp;
    //str_temp = std::string(reinterpret_cast<char*>(&msg[0]),msg.size());
    //image = deserialize_ros<sensor_msgs::Image>(msg);
    image = deserialize_ros<sensor_msgs::CompressedImage>(msg);
    std::cout << "length:::" << msg.size() << std::endl;
    if(pub == NULL)
    {
        ros::NodeHandle nh;
        //pub = nh.advertise<sensor_msgs::Image>("/ImagequeryResult",1000);
        pub = nh.advertise<sensor_msgs::CompressedImage>("/ImagequeryResult",1000);
        std::cout << "test advertise" << std::endl;
    }
    // cv_bridge::CvImage(std_msgs::Header(), "bgr8", imgRes).toImageMsg();
    pub.publish(image);
}

void callback(std::vector<uint8_t> msg)
{
	std::cout<<"dds receive a msg!!!!!!"<<std::endl;
	std_msgs::String str = deserialize_ros<std_msgs::String>(msg);
    //for(int i = 0; i < msg.size(); i++) {
       //std::cout<<(char)msg[i];
    //}
    std::cout<<str.data<<std::endl;
}

int main(int argc, char **argv)
{
    ros::init(argc, argv, "Receiver");
    std::cout << "sub started" <<std::endl;

    eprosima::fastrtps::Participant *mp_participant;
    Fastrtps_PacketPublisher RTPS_Publisher;
    std::string path = ros::package::getPath("fastrtps_pubsub");
    std::string file = path + "/src/test.xml";
    RTPS_Publisher.createRTPSParticipant(mp_participant, 50, "Participant_test", file.c_str());
    Fastrtps_PacketPubSubType myType;
    Domain::registerType(mp_participant, static_cast<TopicDataType*>(&myType));
    Fastrtps_PacketSubscriber RTPS_Subscriber;
    //if(RTPS_Subscriber.init())
    //if(RTPS_Subscriber.init(300, "fastrtps_image_topic"))
    RTPS_Subscriber.getParticipant(mp_participant);
    if(RTPS_Subscriber.init("fastrtps_hello_topic", 2))
    {
    	RTPS_Subscriber.receive(&callback);//回调函数
    	/*while(ros::ok())
        {
    		RTPS_Subscriber.receive(&RTPS_callback);//回调函数
        }*/
    }
    ros::spin();
    Domain::stopAll();
    Log::Reset();
    return 0;
}
