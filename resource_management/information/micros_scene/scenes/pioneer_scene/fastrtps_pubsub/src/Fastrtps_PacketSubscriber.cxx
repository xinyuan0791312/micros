// Copyright 2016 Proyectos y Sistemas de Mantenimiento SL (eProsima).
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/*!
 * @file Fastrtps_PacketSubscriber.cpp
 * This file contains the implementation of the subscriber functions.
 *
 * This file was generated by the tool fastcdrgen.
 */

#include <fastrtps/participant/Participant.h>
#include <fastrtps/attributes/ParticipantAttributes.h>
#include <fastrtps/subscriber/Subscriber.h>
#include <fastrtps/attributes/SubscriberAttributes.h>

#include <fastrtps/Domain.h>

#include "fastrtps_pubsub/Fastrtps_PacketSubscriber.h"

using namespace eprosima::fastrtps;
using namespace eprosima::fastrtps::rtps;

Fastrtps_PacketSubscriber::Fastrtps_PacketSubscriber() : mp_subscriber(nullptr) {}
//Fastrtps_PacketSubscriber::Fastrtps_PacketSubscriber() : mp_participant(nullptr), mp_subscriber(nullptr) {}

Fastrtps_PacketSubscriber::~Fastrtps_PacketSubscriber() {	}
//Fastrtps_PacketSubscriber::~Fastrtps_PacketSubscriber() {	Domain::removeParticipant(mp_participant);}

/*bool Fastrtps_PacketSubscriber::init()
{
    // Create RTPSParticipant

    ParticipantAttributes PParam;
    PParam.rtps.builtin.domainId = 0; //MUST BE THE SAME AS IN THE PUBLISHER
    PParam.rtps.builtin.leaseDuration = c_TimeInfinite;
    PParam.rtps.setName("Participant_subscriber"); //You can put the name you want
    mp_participant = Domain::createParticipant(PParam);
    if(mp_participant == nullptr)
    {
        return false;
    }

    //Register the type

    Domain::registerType(mp_participant, static_cast<TopicDataType*>(&myType));

    // Create Subscriber

    SubscriberAttributes Rparam;
    Rparam.topic.topicKind = NO_KEY;
    Rparam.topic.topicDataType = myType.getName(); //Must be registered before the creation of the subscriber
    Rparam.topic.topicName = "Fastrtps_PacketPubSubTopic";
    mp_subscriber = Domain::createSubscriber(mp_participant,Rparam, static_cast<SubscriberListener*>(&m_listener));
    if(mp_subscriber == nullptr)
    {
        return false;
    }
    return true;
}*/

bool Fastrtps_PacketSubscriber::getParticipant(eprosima::fastrtps::Participant *p_participant)
{
	if(p_participant==nullptr)
		return false;
	else
		mp_participant = p_participant;

	return true;
}


//using static configure
bool Fastrtps_PacketSubscriber::init(std::string m_topic, uint8_t aUserDefinedID)
{
	// Create RTPSParticipant

	/*
    ParticipantAttributes PParam;
    PParam.rtps.builtin.discovery_config.discoveryProtocol = DiscoveryProtocol_t::SIMPLE;
    PParam.rtps.builtin.discovery_config.use_SIMPLE_EndpointDiscoveryProtocol = true;
    PParam.rtps.builtin.discovery_config.m_simpleEDP.use_PublicationReaderANDSubscriptionWriter = true;
    PParam.rtps.builtin.discovery_config.m_simpleEDP.use_PublicationWriterANDSubscriptionReader = true;
    PParam.rtps.builtin.domainId = domainId;
    PParam.rtps.builtin.discovery_config.leaseDuration = c_TimeInfinite;
    PParam.rtps.setName("Participant_sub");
    mp_participant = Domain::createParticipant(PParam);
    */
    if(mp_participant==nullptr)
    {
    	std::cout<<"RTPSParticipant has not created!!!"<<std::endl;
        return false;
    }


	//Register the type

    //Domain::registerType(mp_participant, static_cast<TopicDataType*>(&myType));

	// Create Subscriber

    SubscriberAttributes Rparam;
    Rparam.topic.topicKind = NO_KEY;
    Rparam.topic.topicDataType = myType.getName();
    Rparam.topic.topicName = m_topic;
    //Rparam.topic.historyQos.kind = KEEP_LAST_HISTORY_QOS;
    //Rparam.topic.historyQos.depth = 30;
    //Rparam.topic.resourceLimitsQos.max_samples = 50;
    //Rparam.topic.resourceLimitsQos.allocated_samples = 20;
    //Rparam.qos.m_reliability.kind = RELIABLE_RELIABILITY_QOS;
    Rparam.qos.m_reliability.kind = BEST_EFFORT_RELIABILITY_QOS;
    std::cout<<"user defined id: "<<int(aUserDefinedID)<<std::endl;
    Rparam.setUserDefinedID(aUserDefinedID);
    Rparam.qos.m_durability.kind = TRANSIENT_LOCAL_DURABILITY_QOS;
    Rparam.historyMemoryPolicy = PREALLOCATED_WITH_REALLOC_MEMORY_MODE;
    mp_subscriber = Domain::createSubscriber(mp_participant,Rparam,(SubscriberListener*)&m_listener);

	if(mp_subscriber == nullptr)
		return false;
	std::cout << "Subscriber created, waiting for Publishers." << std::endl;
	return true;
}


//using dynamic configure
//bool Fastrtps_PacketSubscriber::init(uint32_t  domainId, std::string m_topic)
bool Fastrtps_PacketSubscriber::init(std::string m_topic)
{
	// Create RTPSParticipant

	/*
    ParticipantAttributes PParam;
    PParam.rtps.builtin.discovery_config.discoveryProtocol = DiscoveryProtocol_t::SIMPLE;
    PParam.rtps.builtin.discovery_config.use_SIMPLE_EndpointDiscoveryProtocol = true;
    PParam.rtps.builtin.discovery_config.m_simpleEDP.use_PublicationReaderANDSubscriptionWriter = true;
    PParam.rtps.builtin.discovery_config.m_simpleEDP.use_PublicationWriterANDSubscriptionReader = true;
    PParam.rtps.builtin.domainId = domainId;
    PParam.rtps.builtin.discovery_config.leaseDuration = c_TimeInfinite;
    PParam.rtps.setName("Participant_sub");
    mp_participant = Domain::createParticipant(PParam);
    */
    if(mp_participant==nullptr)
    {
    	std::cout<<"RTPSParticipant has not created!!!"<<std::endl;
        return false;
    }


	//Register the type

    //Domain::registerType(mp_participant, static_cast<TopicDataType*>(&myType));

	// Create Subscriber

    SubscriberAttributes Rparam;
    Rparam.topic.topicKind = NO_KEY;
    Rparam.topic.topicDataType = myType.getName();
    Rparam.topic.topicName = m_topic;
    Rparam.topic.historyQos.kind = KEEP_LAST_HISTORY_QOS;
    Rparam.topic.historyQos.depth = 30;
    Rparam.topic.resourceLimitsQos.max_samples = 50;
    Rparam.topic.resourceLimitsQos.allocated_samples = 20;
    //Rparam.qos.m_reliability.kind = RELIABLE_RELIABILITY_QOS;
    Rparam.qos.m_reliability.kind = BEST_EFFORT_RELIABILITY_QOS;
    Rparam.qos.m_durability.kind = TRANSIENT_LOCAL_DURABILITY_QOS;
    Rparam.historyMemoryPolicy = PREALLOCATED_WITH_REALLOC_MEMORY_MODE;
    mp_subscriber = Domain::createSubscriber(mp_participant,Rparam,(SubscriberListener*)&m_listener);

	if(mp_subscriber == nullptr)
		return false;
	std::cout << "Subscriber created, waiting for Publishers." << std::endl;
	return true;
}


//using dynamic configure
bool Fastrtps_PacketSubscriber::init(std::string m_topic, bool isSetQos, bool isReliable)
{
	// Create RTPSParticipant

	/*
    ParticipantAttributes PParam;
    PParam.rtps.builtin.discovery_config.discoveryProtocol = DiscoveryProtocol_t::SIMPLE;
    PParam.rtps.builtin.discovery_config.use_SIMPLE_EndpointDiscoveryProtocol = true;
    PParam.rtps.builtin.discovery_config.m_simpleEDP.use_PublicationReaderANDSubscriptionWriter = true;
    PParam.rtps.builtin.discovery_config.m_simpleEDP.use_PublicationWriterANDSubscriptionReader = true;
    PParam.rtps.builtin.domainId = domainId;
    PParam.rtps.builtin.discovery_config.leaseDuration = c_TimeInfinite;
    PParam.rtps.setName("Participant_sub");
    mp_participant = Domain::createParticipant(PParam);
    */
    if(mp_participant==nullptr)
    {
    	std::cout<<"RTPSParticipant has not created!!!"<<std::endl;
        return false;
    }


	//Register the type

    //Domain::registerType(mp_participant, static_cast<TopicDataType*>(&myType));

	// Create Subscriber

    SubscriberAttributes Rparam;
    Rparam.topic.topicKind = NO_KEY;
    Rparam.topic.topicDataType = myType.getName();
    Rparam.topic.topicName = m_topic;
    Rparam.topic.historyQos.kind = KEEP_LAST_HISTORY_QOS;
    Rparam.topic.historyQos.depth = 30;
    Rparam.topic.resourceLimitsQos.max_samples = 50;
    Rparam.topic.resourceLimitsQos.allocated_samples = 20;
    if(isSetQos&&isReliable)
    	Rparam.qos.m_reliability.kind = RELIABLE_RELIABILITY_QOS;
    else
    	Rparam.qos.m_reliability.kind = BEST_EFFORT_RELIABILITY_QOS;
    Rparam.qos.m_durability.kind = TRANSIENT_LOCAL_DURABILITY_QOS;
    Rparam.historyMemoryPolicy = PREALLOCATED_WITH_REALLOC_MEMORY_MODE;
    mp_subscriber = Domain::createSubscriber(mp_participant,Rparam,(SubscriberListener*)&m_listener);

	if(mp_subscriber == nullptr)
		return false;
	std::cout << "Subscriber created, waiting for Publishers." << std::endl;
	return true;
}


void Fastrtps_PacketSubscriber::SubListener::onSubscriptionMatched(Subscriber* sub,MatchingInfo& info)
{
    (void)sub;
    std::string remoteIP = std::to_string(info.remoteEndpointGuid.guidPrefix.value[2]) + "." + std::to_string(info.remoteEndpointGuid.guidPrefix.value[3]);
    if (info.status == MATCHED_MATCHING)
    {
        n_matched++;
        //std::cout << "Subscriber matched" << std::endl;
        std::cout << "[" << sub->getAttributes().topic.topicName<<"]["<<remoteIP<<"] Subscriber matched"<< std::endl;
    }
    else
    {
        n_matched--;
        //std::cout << "Subscriber unmatched" << std::endl;
        std::cout << "[" << sub->getAttributes().topic.topicName<<"]["<<remoteIP<<"] Subscriber unmatched"<< std::endl;
    }
}

void Fastrtps_PacketSubscriber::SubListener::onNewDataMessage(Subscriber* sub)
{
		// Take data
		Fastrtps_Packet st;

		if(sub->takeNextData(&st, &m_info))
		{
			if(m_info.sampleKind == ALIVE)
			{
				callback_(st.data());
				// Print your structure data here.
				//++n_msg;
				//std::cout << "Sample received, count=" << n_msg << std::endl;
			}
		}
}

void Fastrtps_PacketSubscriber::receive(boost::function<void(const std::vector<uint8_t>&)> callBack)
{
   m_listener.callback_ = callBack;
}

void Fastrtps_PacketSubscriber::run()
{
    std::cout << "Waiting for Data, press Enter to stop the Subscriber. "<<std::endl;
    std::cin.ignore();
    std::cout << "Shutting down the Subscriber." << std::endl;
}

