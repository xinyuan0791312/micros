/*=================================================================
* Copyright (c) 2016, micROS Group, NIIDT, TAIIC, HPCL.
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without modification, are permitted
* provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice, this list of conditions and
*       the following disclaimer.
* 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions
*      and the following disclaimer in the documentation and/or other materials provided with the
*      distribution.
* 3. All advertising materials mentioning features or use of this software must display the following
*      acknowledgement: This product includes software developed by the micROS Group and its
*      contributors.
* 4. Neither the name of the Group nor the names of contributors may be used to endorse or promote
*      products derived from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY MICROS GROUP AND CONTRIBUTORS ''AS IS''AND
* ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
* PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE MICROS, GROUP OR
* CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
* EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
* PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
* PROFITS; OR BUSINESS INTERRUPTION). HOWEVER CAUSED AND ON ANY THEORY
* OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
* NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
* SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
===================================================================
* Author: Jun Cai
*/

#include <iostream>
#include <ros/ros.h>

#include "fastrtps_pubsub/Fastrtps_PacketPublisher.h"

#include <fastrtps/participant/Participant.h>
#include <fastrtps/attributes/ParticipantAttributes.h>
#include <fastrtps/Domain.h>
#include <fastrtps/utils/eClock.h>
#include <fastrtps/log/Log.h>

#include <sensor_msgs/Image.h>
#include <sensor_msgs/CompressedImage.h>
#include <std_msgs/String.h>
#include <ros/package.h>

using namespace eprosima;
using namespace eprosima::fastrtps;

Fastrtps_PacketPublisher RTPS_Publisher;

template<class T>
std::vector<uint8_t> serialize_ros(T t)
{
    std::vector<uint8_t> vec;
    uint32_t serial_size = ros::serialization::serializationLength(t);
    boost::shared_array<uint8_t> buffer(new uint8_t[serial_size]);
    ros::serialization::OStream ostream(buffer.get(), serial_size);
    ros::serialization::serialize(ostream, t);
    vec.resize(serial_size);
    std::copy(buffer.get(), buffer.get() + serial_size, vec.begin());
    return vec;
}
/*void pub_callback(const sensor_msgs::Image::ConstPtr& msg)
{
    cv_bridge::CvImagePtr cv_ptr;
    try
    {
        cv_ptr = cv_bridge::toCvCopy(msg,sensor_msgs::image_encodings::RGB8);
    }   
    catch (cv_bridge::Exception& e)
    {
        ROS_ERROR("cv_bridge exception: %s", e.what());
        return;
    }
    std::string encoding;
    encoding = "jpg";
    cv::imencode("."+encoding,cv_ptr->image,RTPS_Publisher.buf);
    std::cout << "length:::" << RTPS_Publisher.buf.size() << std::endl;
    if(!RTPS_Publisher.RTPS_send())
    eClock::my_sleep(25);
}
*/

/*void pub_callback(const sensor_msgs::Image::ConstPtr& msg)
{
	std::cout<<"success subscribing image_raw!!!!!"<<std::endl;
    sensor_msgs::Image image;
    image.header = msg->header;
    image.height = msg->height;
    image.width =  msg->width;
    image.encoding = msg->encoding;
    image.is_bigendian = msg->is_bigendian;
    image.step = msg->step;
    image.data = msg->data;
    std::string str1;
    str1 = serialize_ros(image);//ros序列化
    std::cout << str1.size() << std::endl;
    std::vector<char> vec_data(str1.c_str(), str1.c_str()+str1.size());
    //将通过ros订阅的消息类型转换成Fast-RTPS所能发送的vector<uchar>
    //for(auto e : vec_data)
    //{
    //    RTPS_Publisher.buf.push_back(static_cast<uchar>(e));
    //}
    //std::cout << "length:::" << RTPS_Publisher.buf.size() << std::endl;
    if(!RTPS_Publisher.DDS_pub(vec_data))//发送消息
    eClock::my_sleep(25);
    vec_data.clear();
    str1.clear();
}*/

//void pub_callback(const sensor_msgs::Image::ConstPtr& msg)
void pub_callback(const sensor_msgs::CompressedImage::ConstPtr& msg)
{
	static int count=0;
	if(count==300)
	{
		std::cout<<"success subscribing image_raw!!!!!"<<std::endl;

		std::vector<uint8_t> vec_data;

		vec_data = serialize_ros(*msg);

		RTPS_Publisher.DDS_pub(vec_data);

		//if(!RTPS_Publisher.DDS_pub(vec_data))//发送消息
			//eClock::my_sleep(25);
		vec_data.clear();
		count=0;
	}
    count++;
    //str1.clear();
}

std::string anylengthstr(int length)
{
	std::string str="";
	for(int i=0;i<length;i++)
	{
		str+="A";
	}
	return str;
}


int main(int argc, char **argv)
{
    std::cout << "pub started" << std::endl;
    ros::init(argc,argv,"sub_pub_camera_img_new");

    eprosima::fastrtps::Participant *mp_participant;

    std::string path = ros::package::getPath("fastrtps_pubsub");
    std::string file = path + "/src/test.xml";
    std::cout << "file: " << file << std::endl;
    RTPS_Publisher.createRTPSParticipant(mp_participant, 50, "Participant_test", file.c_str());
  	Fastrtps_PacketPubSubType myType;
  	Domain::registerType(mp_participant, static_cast<TopicDataType*>(&myType));

    //RTPS_Publisher.init();
    //RTPS_Publisher.init(300,"fastrtps_image_topic");
    RTPS_Publisher.getParticipant(mp_participant);
    RTPS_Publisher.init("fastrtps_hello_topic", 1);
    ros::NodeHandle nh;

    std_msgs::String str;
    //str.data="hello world";
    //str.data=anylengthstr(96);
    str.data=anylengthstr(150);


    //std::string data = "hello world";
	std::vector<uint8_t> vec_data;
	vec_data = serialize_ros(str);

    while(ros::ok()) {
    	std::cout<<"dds pub a msg!!!!!!"<<std::endl;
        RTPS_Publisher.DDS_pub(vec_data);
        sleep (1);
    }


    //ros::Subscriber sub = nh.subscribe("/uav0/image_raw",1,pub_callback);
    //ros::Subscriber sub = nh.subscribe("/mytopic",1,pub_callback);
    //ros::Subscriber sub = nh.subscribe("/usb_cam/image_raw",1,pub_callback);
    /*ros::Subscriber sub = nh.subscribe("/usb_cam/image_raw/compressed",1,pub_callback);
    //ros::Subscriber sub = nh.subscribe("/camera/rgb/image_raw/compressed",1,pub_callback);
    //ros::Subscriber sub = nh.subscribe("/camera/rgb/image_raw",1,pub_callback);
    ros::spin();
    Domain::stopAll();
    Log::Reset();*/
    return 0;
}

