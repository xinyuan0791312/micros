#ifndef __ACTOR_DATA_SUB_PLUGIN__
#define __ACTOR_DATA_SUB_PLUGIN__

#include "general_plugin/general_plugin.h"
#include "std_msgs/String.h"

#include "rosplane_msgs/State.h"

#include "abstract_msgs/ActiveActors.h"
#include "abstract_msgs/SwarmActiveActors.h"
//#include "pioneer_scene/PioneerScene.h"
#include "virtual_scene_system/VirtualSceneSystem.h"

namespace general_bus {

/**
 * @brief The ActorDataSubPlugin class, this plugin is used to get current active actors,
 * and pub them to topic "/active_actors" and "/swarm_active_actors"
 */
class ActorDataSubPlugin: public GeneralPlugin{

public:
  /**
   * @brief start,inherited from GeneralPlugin
   */
  virtual void start();

  std::string _actorName;

  /**
   * @brief _actorDataSub,subscriber used to subscribe topic "truth"
   */
  ros::Subscriber _actorDataSub;

  //micros_scene::PioneerScene _scene;
  /**
   * @brief _virtualSceneSystem, use to call "sMount sWrite" etc.
   */
  boost::shared_ptr<micros_scene::VirtualSceneSystem> _virtualSceneSystem;

	ros::Publisher _activeActorsPub;
	ros::Publisher _swarmActiveActorsPub;

  /**
   * @brief truthCallback,"truth" topic subscribe callback, used for test
   * @param input
   */
  void truthCallback(const rosplane_msgs::State::ConstPtr &input);

};

}

#endif
