/*=================================================================
* Copyright (c) 2019, micROS Group, NIIDT, TAIIC, HPCL.
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without modification, are permitted
* provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice, this list of conditions and
* the following disclaimer.
* 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions
* and the following disclaimer in the documentation and/or other materials provided with the
* distribution.
* 3. All advertising materials mentioning features or use of this software must display the following
* acknowledgement: This product includes software developed by the micROS Group and its
* contributors.
* 4. Neither the name of the Group nor the names of contributors may be used to endorse or promote
* products derived from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY MICROS GROUP AND CONTRIBUTORS ''AS IS''AND
* ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
* PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE MICROS, GROUP OR
* CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
* EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
* PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
* PROFITS; OR BUSINESS INTERRUPTION). HOWEVER CAUSED AND ON ANY THEORY
* OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
* NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
* SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
===================================================================
* Author: Cai Jun.
*/
#include <pluginlib/class_list_macros.h>
#include "actor_data_sub_plugin/actor_data_sub_plugin.h"

PLUGINLIB_EXPORT_CLASS(general_bus::ActorDataSubPlugin, general_bus::GeneralPlugin)

namespace general_bus
{

  void ActorDataSubPlugin::truthCallback(const rosplane_msgs::State::ConstPtr &input)
  {
    rosplane_msgs::State tmp = *input;
    static int count = 0;
    if(count == 10)
    {
    	_virtualSceneSystem->sWrite(tmp,true,true);
    	count=0;
    }
    count++;
  }

  void ActorDataSubPlugin::start()
  {
		getActorName(_actorID, _actorName);

		_virtualSceneSystem.reset(new micros_scene::VirtualSceneSystem());
		_virtualSceneSystem->sMount();

		//sMount();

		int robotID = -1;
		ros::NodeHandle privNh("~");
		if(privNh.hasParam("robot_id"))
		  privNh.getParam("robot_id",robotID);
		else
		  ROS_WARN("[Daemon Node]cannot find robot ID in the parameter server, return..");

		abstract_msgs::ActiveActors tmp;
		tmp.robotID = robotID;
		tmp.activeActorNames.push_back(_actorName);

		abstract_msgs::SwarmActiveActors tmp2;
    tmp2.swarmName = "QYFK";
		tmp2.swarmActiveActors.push_back(tmp);

		_activeActorsPub = pluginAdvertise<abstract_msgs::ActiveActors>("/active_actors", 10000);
		_swarmActiveActorsPub = pluginAdvertise<abstract_msgs::SwarmActiveActors>("/swarm_active_actors", 10000);
		//_actorDataSub = pluginSubscribe("truth", 10000, &ActorDataSubPlugin::truthCallback, this);

		while(ros::ok())
		{
				GOON_OR_RETURN;

				_activeActorsPub.publish(tmp);
				_swarmActiveActorsPub.publish(tmp2);
				usleep(100000);
			}
		}

}// end namespece



