/*=================================================================
* Copyright (c) 2016, micROS Group, NIIDT, TAIIC, HPCL.
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without modification, are permitted
* provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice, this list of conditions
*   and the following disclaimer.
* 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions
*   and the following disclaimer in the documentation and/or other materials provided with the distribution.
* 3. All advertising materials mentioning features or use of this software must display the following
*   acknowledgement: This product includes software developed by the micROS Group and its contributors.
* 4. Neither the name of the Group nor the names of contributors may be used to endorse or promote
*   products derived from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY MICROS GROUP AND CONTRIBUTORS ''AS IS''AND ANY EXPRESS OR IMPLIED WARRANTIES,
* INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
* PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE MICROS, GROUP OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
* GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION). HOWEVER CAUSED AND ON ANY THEORY
* OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
* WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
===================================================================
* Author: Bin Lin, Jun Cai, Songchang Jin, Jie Yan.
*/

#ifndef SCENETOOLS_H
#define SCENETOOLS_H
#include <topic_tools/shape_shifter.h>
#include "rosplane_msgs/State.h"
#include "pioneer_scene/GlobalVariable.h"
#include <ros/message_operations.h>
#include "virtual_scene_system/SceneBase.h"

namespace micros_scene
{
  using std::string;
  using std::vector;

  /**
   * @brief The SceneTools class, provide some useful common functions
   */
  class SceneTools
  {
    public:
      SceneTools();
      ~SceneTools();

      /**
       * @brief activeActorsSubCallback, callback to process data from ros topic "/active_actors",
       * use to get current active actors
       * @param aMsg
       */
      void activeActorsSubCallback(const abstract_msgs::ActiveActors::ConstPtr aMsg);

      /**
       * @brief swarmActiveActorsSubCallback, callback to process data from ros topic "/swarm_active_actors",
       * use to get current swarm name.
       * @param aMsg
       */
      void swarmActiveActorsSubCallback(const abstract_msgs::SwarmActiveActors::ConstPtr aMsg);

      /**
       * @brief robotPositionSubCallback, callback to process data from ros topic "truth", use to get the robot position
       * @param aMsg
       */
      void robotPositionSubCallback(const rosplane_msgs::State::ConstPtr aMsg);

      /**
       * @brief sWrite, use to write a ros msg as an event msg or to database.
       * @param aCommonMsg
       * @param isEvent
       * @param isSave
       * @return 
       */
      int32_t sWrite(CommonMsg aCommonMsg, bool isEvent, bool isSave);
    private:

      micros_storage::Tools _storageTool;

      ros::NodeHandle _nh;
      ros::Publisher _dataPublisher;
      ros::Publisher _eventPublisher;

      int32_t _selfID;

      ros::Subscriber _dataSub;
      ros::Subscriber _activeActorsSub;
      ros::Subscriber _swarmActiveActorsSub;
      ros::Subscriber _robotPositionSub;
  };

}//end of namespace micros_scene
#endif
