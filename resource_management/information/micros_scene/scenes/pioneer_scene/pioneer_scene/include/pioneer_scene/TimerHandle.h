/*=================================================================
* Copyright (c) 2016, micROS Group, NIIDT, TAIIC, HPCL. 
* All rights reserved. 
* 
* Redistribution and use in source and binary forms, with or without modification, are permitted 
* provided that the following conditions are met:  
* 
* 1. Redistributions of source code must retain the above copyright notice, this list of conditions
*   and the following disclaimer. 
* 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
*   and the following disclaimer in the documentation and/or other materials provided with the distribution.
* 3. All advertising materials mentioning features or use of this software must display the following 
*   acknowledgement: This product includes software developed by the micROS Group and its contributors. 
* 4. Neither the name of the Group nor the names of contributors may be used to endorse or promote 
*   products derived from this software without specific prior written permission. 
* 
* THIS SOFTWARE IS PROVIDED BY MICROS GROUP AND CONTRIBUTORS ''AS IS''AND ANY EXPRESS OR IMPLIED WARRANTIES,
* INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR 
* PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE MICROS, GROUP OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
* GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION). HOWEVER CAUSED AND ON ANY THEORY 
* OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
* WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 
===================================================================
* Author: Bin Lin, Jun Cai, Songchang Jin. 
*/ 

#ifndef TIMERHANDLE_H
#define TIMERHANDLE_H
#include <storage/tools.h>
#include <abstract_msgs/Query.h>
#include <abstract_msgs/LayerQuery.h>
#include <abstract_msgs/QueryResult.h>
#include <fastrtps_pubsub/Fastrtps_PacketPublisher.h>

#include <boost/any.hpp>
#include <algorithm>
#include "std_msgs/String.h"
#include <math.h>

#include "virtual_scene_system/VirtualSceneSystem.h"
#include "pioneer_scene/SceneTools.h"

namespace micros_scene
{
  using std::string;
  using std::vector;
  using std::map;

  struct ActorPosition
  {
	  float x;
	  float y;
	  float z;
	  int count=0;
  };


  /**
   * @brief The TimerHandle class, create a ros timer, periodically transmit scene result data
   */
  class TimerHandle
  {
    public:

      /**
       * @brief TimerHandle, Constructor function
       * @param aSenderID, the id of local robot
       * @param aReceiverID, the id of the robot who send the scene query
       * @param aSceneName
       * @param aPeriodLayerQuery
       */
      TimerHandle(int32_t aSenderID, int32_t aReceiverID, string aSceneName, abstract_msgs::LayerQuery aPeriodLayerQuery);
      ~TimerHandle();

      /**
       * @brief setSenderID, set the robot id who send scene result
       * @param ID
       */
      void setSenderID(int32_t ID);

      /**
       * @brief setReceiverID, set the robot id who receive scene result
       * @param ID
       */
      void setReceiverID(int32_t ID);

      /**
       * @brief setSceneName, set the name of scene query
       * @param name
       */
      void setSceneName(string name);

      /**
       * @brief setLayerName, set the name of layer quey
       * @param name
       */
      void setLayerName(string name);

      /**
       * @brief setQueryMsg, set input period layer query
       * @param aLayerQuery
       */
      void setQueryMsg(abstract_msgs::LayerQuery aLayerQuery);

      /**
       * @brief getSenderID, get the sender id
       * @return
       */
      int32_t getSenderID();

      /**
       * @brief getReceiverID, get the receiver id
       * @return
       */
      int32_t getReceiverID();

      /**
       * @brief getSceneName, get the scene name
       * @return
       */
      string getSceneName();

      /**
       * @brief getLayerName, get the layer name
       * @return
       */
      string getLayerName();

      /**
       * @brief getQueryMsg, get the input period layer query
       * @return
       */
      abstract_msgs::LayerQuery getQueryMsg();


      /**
       * @brief createTimerHandle, create a ros timer to periodically transmit scene result data
       * @param aFlag, if this param is 0, the ros timer callback will set to be "localPublishCallback", else,
       * the ros timer callback will set to be "remotePublishCallback".
       */
      void createTimerHandle(int32_t aFlag);

      /**
      * @brief callback function to process local scene query
      */

      void localPublishCallback(const ros::TimerEvent&);

      /**
      * @brief callback function to process remote scene query
      */
      void remotePublishCallback(const ros::TimerEvent&);

      /**
       * @brief commonDataSubCallback
       * @param aMsg
       * @param topic_name
       */
      void commonDataSubCallback(const topic_tools::ShapeShifter::ConstPtr& aMsg, const std::string &topic_name);

      /**
       * @brief start, start the timer
       */
      void start();

      /**
      * @brief stop timer, stop the timer
      */
      void stop();

    private:

      ros::Timer _timer;

      /**
       * @brief _actorQueries, a set of queries parsed from a period layer query
       */
      vector<abstract_msgs::Query>  _actorQueries;

      string _sceneName;

      string _layerName;

      int32_t _senderID;

      int32_t _receiverID;

      micros_storage::Tools _storageTool;

      ros::NodeHandle _nh;

      /**
       * @brief _sceneResultLocalPub, ros publisher to pub local scene query result
       */
      ros::Publisher _sceneResultLocalPub;

      /**
       * @brief _sceneResultRemotePub, ros publisher to pub remote scene query result
       */
      ros::Publisher _sceneResultRemotePub;

      /**
       * @brief _actorDataSubs, ros Subscribers for different data types, string is the topic name of data type.
       */
      map<string, ros::Subscriber> _actorDataSubs;

      /**
       * @brief _actorDataBuffers, buffers for different data types, each data type has a buffer, string
       * is the topic name of data type
       */
      map<string, vector<abstract_msgs::UnifiedData> > _actorDataBuffers;


      abstract_msgs::LayerQuery _periodLayerQuery;
      int32_t _count;

      map<string, ActorPosition> _lastActorData;
  };
}

#endif
