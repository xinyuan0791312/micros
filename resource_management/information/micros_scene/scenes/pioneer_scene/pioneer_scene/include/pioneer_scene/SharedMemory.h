/*=================================================================
* Copyright (c) 2016, micROS Group, NIIDT, TAIIC, HPCL.
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without modification, are permitted
* provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice, this list of conditions
*   and the following disclaimer.
* 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions
*   and the following disclaimer in the documentation and/or other materials provided with the distribution.
* 3. All advertising materials mentioning features or use of this software must display the following
*   acknowledgement: This product includes software developed by the micROS Group and its contributors.
* 4. Neither the name of the Group nor the names of contributors may be used to endorse or promote
*   products derived from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY MICROS GROUP AND CONTRIBUTORS ''AS IS''AND ANY EXPRESS OR IMPLIED WARRANTIES,
* INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
* PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE MICROS, GROUP OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
* GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION). HOWEVER CAUSED AND ON ANY THEORY
* OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
* WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
===================================================================
* Author: Bin Lin, Jun Cai, Songchang Jin, Jie Yan.
*/

#ifndef SRC_INFORMATION_MICROS_SCENE_INCLUDE_MICROS_SCENE_SHAREDMEMORY_H_
#define SRC_INFORMATION_MICROS_SCENE_INCLUDE_MICROS_SCENE_SHAREDMEMORY_H_

#include<sys/shm.h>
#include<sys/ipc.h>
#include<sys/types.h>
#include "abstract_msgs/ActiveActors.h"
#include "abstract_msgs/SwarmActiveActors.h"

namespace micros_scene
{

	class SharedMemory
	{
		public:
      SharedMemory();
			~SharedMemory();

      /**
       * @brief createSharedMemory, create a block of shared memory
       * @return
       */
			int createSharedMemory();

      /**
       * @brief getSharedMemoryID, get the ID of created shared memory
       * @return
       */
			int getSharedMemoryID();

      /**
       * @brief write, write a SwarmActiveActors msg to shared memory
       * @param msg
       */
			void write(abstract_msgs::SwarmActiveActors msg);

      /**
       * @brief read, read a SwarmActiveActors msg from shared memory
       * @param msg
       */
			void read(abstract_msgs::SwarmActiveActors &msg);

      /**
       * @brief deleteSharedMemory, delete created shared memory
       */
			void deleteSharedMemory();

		private:
			size_t _size;
	};
}

#endif /* SRC_INFORMATION_MICROS_SCENE_INCLUDE_MICROS_SCENE_SHAREDMEMORY_H_ */
