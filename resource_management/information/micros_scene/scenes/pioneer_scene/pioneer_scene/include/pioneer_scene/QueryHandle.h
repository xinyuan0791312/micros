/*=================================================================
* Copyright (c) 2016, micROS Group, NIIDT, TAIIC, HPCL. 
* All rights reserved. 
* 
* Redistribution and use in source and binary forms, with or without modification, are permitted 
* provided that the following conditions are met:  
* 
* 1. Redistributions of source code must retain the above copyright notice, this list of conditions
*   and the following disclaimer. 
* 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
*   and the following disclaimer in the documentation and/or other materials provided with the distribution.
* 3. All advertising materials mentioning features or use of this software must display the following 
*   acknowledgement: This product includes software developed by the micROS Group and its contributors. 
* 4. Neither the name of the Group nor the names of contributors may be used to endorse or promote 
*   products derived from this software without specific prior written permission. 
* 
* THIS SOFTWARE IS PROVIDED BY MICROS GROUP AND CONTRIBUTORS ''AS IS''AND ANY EXPRESS OR IMPLIED WARRANTIES,
* INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR 
* PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE MICROS, GROUP OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
* GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION). HOWEVER CAUSED AND ON ANY THEORY 
* OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
* WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 
===================================================================
* Author: Bin Lin, Jun Cai, Songchang Jin. 
*/ 

#ifndef QUERYHANDLE_H
#define QUERYHANDLE_H
#include <storage/tools.h>
#include <abstract_msgs/SceneQuery.h>
#include <abstract_msgs/LayerQuery.h>
#include <abstract_msgs/EventSceneMap.h>
#include <abstract_msgs/EventQuery.h>
#include <pioneer_scene/TimerHandle.h>
#include <fastrtps_pubsub/Fastrtps_PacketPublisher.h>

namespace micros_scene
{
  using std::string;
  using std::vector;

  /**
  * @brief process a scene query
  */

  /**
   * @brief The QueryHandle class, used to process a scene query msg
   */
  class QueryHandle
  {
    public:
      QueryHandle();
      ~QueryHandle();

      /**
       * @brief setID. set the robot ID
       * @param aSelfID
       */
      void setID(int32_t aSelfID);


      /**
       * @brief getID, get the robot ID
       * @return
       */
      int32_t getID();

      /**
       * @brief setInput, set input scene query
       * @param anInput
       */
      void setInput(abstract_msgs::SceneQuery anInput);

      /**
       * @brief getInput, get input scene query
       * @return
       */
      abstract_msgs::SceneQuery getInput();

      /**
       * @brief sha256, hash fuction
       * @param aBuf
       * @param aBufSize
       * @return
       */
      string sha256(char* aBuf, uint64_t aBufSize);


      /**
       * @brief sha256Input, using the hash function to create a hash string for input msg
       * @param anInput
       * @return
       */
      template<typename T>
      string sha256Input(T anInput)
      {
        uint64_t serialSize = ros::serialization::serializationLength<T>(anInput);
        boost::shared_array<uint8_t> buffer(new uint8_t[serialSize]);
        ros::serialization::OStream ostream(buffer.get(), serialSize);
        ros::serialization::serialize<T>(ostream, anInput);
        string hashResult = sha256((char*)buffer.get(), serialSize);

        return hashResult;
      }

      /**
       * @brief calculateHash, calculate the hash code of input scene query
       */
      void calculateHash();

      /**
       * @brief getHash, get the hash code of input scene query
       * @return
       */
      string getHash();

      /**
       * @brief parsePeriodLayerQuery, process a period layer query
       * @param aPeriodLayerQuery
       */
      void parsePeriodLayerQuery(abstract_msgs::LayerQuery aPeriodLayerQuery);

      /**
       * @brief parseHistoryLayerQuery, process a history layer query
       * @param aHistoryLayerQuery
       */
      void parseHistoryLayerQuery(abstract_msgs::LayerQuery aHistoryLayerQuery);

      /**
       * @brief parseEventLayerQuery, process an event layer query
       * @param anEventLayerQuery
       * @param anEventList
       */
      void parseEventLayerQuery(abstract_msgs::LayerQuery anEventLayerQuery, vector<abstract_msgs::EventSceneMap> &anEventList);

      /**
       * @brief startTimers, start all timehandle's timers
       */
      void startTimers();

      /**
       * @brief stopTimers, stop all timehandle's timers
       */
      void stopTimers();

    private:

      abstract_msgs::SceneQuery _query;


      int32_t _selfID;

      /**
       * @brief _timerVec, store TimerHandles
       */
      vector<boost::shared_ptr<micros_scene::TimerHandle>> _timerVec;

      string _hashCode;
      
      ros::NodeHandle _nh;
      ros::Publisher _historyResultLocalPub;
      ros::Publisher _historyResultRemotePub;

      string _filePath;
      string _databasePath;
      micros_storage::Tools _storageTool;
  };
}
#endif
