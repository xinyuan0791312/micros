/*=================================================================
* Copyright (c) 2016, micROS Group, NIIDT, TAIIC, HPCL. 
* All rights reserved. 
* 
* Redistribution and use in source and binary forms, with or without modification, are permitted 
* provided that the following conditions are met:  
* 
* 1. Redistributions of source code must retain the above copyright notice, this list of conditions
*   and the following disclaimer. 
* 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
*   and the following disclaimer in the documentation and/or other materials provided with the distribution.
* 3. All advertising materials mentioning features or use of this software must display the following 
*   acknowledgement: This product includes software developed by the micROS Group and its contributors. 
* 4. Neither the name of the Group nor the names of contributors may be used to endorse or promote 
*   products derived from this software without specific prior written permission. 
* 
* THIS SOFTWARE IS PROVIDED BY MICROS GROUP AND CONTRIBUTORS ''AS IS''AND ANY EXPRESS OR IMPLIED WARRANTIES,
* INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR 
* PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE MICROS, GROUP OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
* GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION). HOWEVER CAUSED AND ON ANY THEORY 
* OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
* WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 
===================================================================
* Author: Bin Lin, Jun Cai, Songchang Jin, Jie Yan.
*/

#ifndef DATAPUBSUB_H
#define DATAPUBSUB_H
#include <storage/tools.h>
#include <storage/memdbdao.h>
#include <pioneer_scene/QueryHandle.h>
#include <boost/function.hpp>
#include <boost/bind.hpp>
#include <fastrtps_pubsub/Fastrtps_PacketSubscriber.h>
#include <abstract_msgs/EventSceneMap.h>
#include <abstract_msgs/EventQuery.h>
#include <abstract_msgs/EventResult.h>

#include "virtual_scene_system/VirtualSceneSystemDaemonNode.h"


#define USING_COMPRESS_MSG

namespace micros_scene
{
  using std::string;
  using std::vector;

  /**
   * @brief The DataPubSub class, is an data transmitter, using ros pub/sub to transmit local data, using dds pub/sub to transmit remote data
   */
  class DataPubSub
  {
    public:
	    DataPubSub();
      ~DataPubSub();

      /**
       * @brief rtpsSceneQueryCallback, callback for the DDS Subscriber to process a scene query msg.
       * @param aMsg
       */
      void rtpsSceneQueryCallback(vector<uint8_t> aMsg);

      /**
       * @brief rtpsSceneQueryResultCallback, callback for the DDS Subscriber to process a scene query result msg.
       * @param aMsg
       */
      void rtpsSceneQueryResultCallback(vector<uint8_t> aMsg);

      /**
       * @brief rtpsEventResultCallback, callback for the DDS Subscriber to process an event result msg.
       * @param aMsg
       */
      void rtpsEventResultCallback(vector<uint8_t> aMsg);

      /**_eventPublisher
       * @brief eventCallback, call back for the ROS subscriber to process an event
       * @param aMsg
       */
      void eventCallback(const abstract_msgs::UnifiedData aMsg);

      /**
       * @brief sceneQueryCallback, call back for the ROS subscriber to process an scene query msg
       * @param aMsg
       */
      void sceneQueryCallback(const abstract_msgs::SceneQuery aMsg);

      /**
       * @brief sceneQueryResultCallback, call back for the ROS subscriber to process an scene query result msg
       * @param aMsg
       */
      void sceneQueryResultCallback(const abstract_msgs::QueryResult aMsg);

    private:     

      /**
      * @brief store QueryHandles for the scene queries, each scene query a QueryHandle
      */
      vector<boost::shared_ptr<micros_scene::QueryHandle>> _queryHandleVec;


      micros_storage::Tools _storageTool;

      ros::NodeHandle _nh;
      ros::Subscriber _eventSub;
      ros::Subscriber _sceneQuerySub;
      ros::Subscriber _sceneQueryResultSub;
      ros::Publisher _eventResultPub;
      //ros::Publisher _eventPublisher;

      vector<abstract_msgs::EventSceneMap> _eventList;

  	  Fastrtps_PacketPublisher _rtpsSceneQueryPublisher;
  	  Fastrtps_PacketPublisher _rtpsSceneQueryResultPublisher;
  	  Fastrtps_PacketPublisher _rtpsEventResultPublisher;

  	  Fastrtps_PacketSubscriber _rtpsSceneQuerySubscriber;
  	  Fastrtps_PacketSubscriber _rtpsSceneQueryResultSubscriber;
  	  Fastrtps_PacketSubscriber _rtpsEventResultSubscriber;

      int32_t _selfID;
      int32_t _eventCount;

      map<string, ros::Publisher> _sceneQueryResultPubs;


      ros::Subscriber _activeActorsSub;
      ros::Subscriber _swarmActiveActorsSub;
      ros::Subscriber _robotPositionSub;
  };

}//end of namespace micros_scene
#endif
