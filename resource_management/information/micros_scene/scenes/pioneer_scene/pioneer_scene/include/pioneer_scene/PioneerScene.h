/*=================================================================
* Copyright (c) 2016, micROS Group, NIIDT, TAIIC, HPCL.
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without modification, are permitted
* provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice, this list of conditions
*   and the following disclaimer.
* 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions
*   and the following disclaimer in the documentation and/or other materials provided with the distribution.
* 3. All advertising materials mentioning features or use of this software must display the following
*   acknowledgement: This product includes software developed by the micROS Group and its contributors.
* 4. Neither the name of the Group nor the names of contributors may be used to endorse or promote
*   products derived from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY MICROS GROUP AND CONTRIBUTORS ''AS IS''AND ANY EXPRESS OR IMPLIED WARRANTIES,
* INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
* PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE MICROS, GROUP OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
* GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION). HOWEVER CAUSED AND ON ANY THEORY
* OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
* WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
===================================================================
* Author: Bin Lin, Jun Cai, Songchang Jin, Jie Yan.
*/

#ifndef SRC_INFORMATION_SCENES_PIONEER_SCENE_INCLUDE_PIONEER_SCENE_PIONEERSCENE_H_
#define SRC_INFORMATION_SCENES_PIONEER_SCENE_INCLUDE_PIONEER_SCENE_PIONEERSCENE_H_


#include <storage/tools.h>
#include <abstract_msgs/SceneQuery.h>
#include <fastrtps_pubsub/Fastrtps_PacketSubscriber.h>
#include <fastrtps_pubsub/Fastrtps_PacketPublisher.h>
#include <abstract_msgs/EventResult.h>

#include <abstract_msgs/QueryResult.h>
#include <pioneer_scene/tinyxml2.h>
#include <unistd.h>
#include <stdlib.h>
#include <cstring>
#include <ros/ros.h>
#include <rosbag/bag.h>
#include <ros/package.h>

#include <openssl/sha.h>
#include "pioneer_scene/DataPubSub.h"
#include <pluginlib/class_list_macros.h>

//#define PUB_HEARTBEAT
#define USING_COMPRESS_MSG

namespace micros_scene
{

using std::string;
using std::vector;

/**
 * @brief The PioneerScene class, the first type of scene
 */
class PioneerScene: public SceneBase
{

public:

  /**
   * @brief sInit, inherited from SceneBase, used to init a scene
   */
  virtual void sInit();

  /**
   * @brief sOpen, inherited from SceneBase, used to open and parse a scene xml, an example xml "scene.xml" is given in the "src" folder.
   * @param aXmlPath, the path and name of the xml to be open
   * @param RECORD, if this param is true, the scene result will be recorded to a rosbag, otherwise not to be recorded
   * @param aRecordPath, the path of the rosbag to record the scene result
   * @return
   */
  virtual int32_t sOpen(std::string aXmlPath, bool RECORD, std::string aRecordPath);

  /**
   * @brief sRead, inherited from SceneBase, used to read data customized by the opened scene xml
   */
  virtual void sRead();

  /**
   * @brief sClose, inherited from SceneBase, used to stop reading data customized by the opened scene xml
   */
  virtual void sClose();

  /**
   * @brief sWrite, inherited from SceneBase, used to write a msg as an event or to database
   * @param aCommonMsg, a three tuple contains the serialize data, the data type and the json content of a msg
   * @param isEvent, if this param is true, the msg will be published as an event, othewise, the msg will be write to the
   * database and not publish as an event
   * @param saveEvent, if this param is true, the event msg will saved to database
   * @return
   */
  virtual int32_t sWrite(CommonMsg aCommonMsg, bool isEvent=false, bool saveEvent=true);


  PioneerScene();
	~PioneerScene();

  /**
   * @brief sceneQueryResultCallback, used to store the scene result to a rosbag when needed.
   * @param aMsg
   */
  void sceneQueryResultCallback(const abstract_msgs::QueryResult aMsg);

  /**
   * @brief getSceneName, get the name of opened scene
   * @return
   */
	string getSceneName();

  /**
   * @brief setTargetRobotId, set the target robot to receive data from
   * @param anID
   */
	void setTargetRobotId(int anID);

  /**
   * @brief abort, exception handling
   */
	int32_t abort();

  /**
   * @brief sendSceneQuery, a ros timer callback, use to send the scene query msg out periodically
   */
	void sendSceneQuery(const ros::TimerEvent&);

  /**
   * @brief sceneQueryStart, begin to send the scene query msg out.
   */
	void sceneQueryStart();

  /**
   * @brief sceneQueryStop, send a stop signal out
   */
	void sceneQueryStop();

  /**
   * @brief layerQueryStop, stop a layer singly
   * @param alayerName
   */
	void layerQueryStop(string alayerName);

  /**
   * @brief layerQueryRestart, restart a layer
   * @param alayerName
   */
	void layerQueryRestart(string alayerName);

  /**
   * @brief convertDateToTimestamp, convert data such as "2019-10-21 10:50:00" to timestamp
   * @param aDateTime
   * @return
   */
	time_t convertDateToTimestamp(char * aDateTime);

  /**
   * @brief createSceneQuery, fuction to create a scene query msg
   * @param aSenderID
   * @param aSceneName
   */
	void createSceneQuery(int32_t aSenderID, string aSceneName);

  /**
   * @brief deleteScene, fuction to delete a scene query msg
   * @param aSenderID
   * @param aSceneName
   */
	void deleteScene(int32_t aSenderID, string aSceneName);

  /**
   * @brief createPeriodLayer, funtion to delete a period layer.
   * @param aLayerName
   * @param frequency
   * @param aIsSpaceConstraint
   * @param aXMin
   * @param aXMax
   * @param aYMin
   * @param aYMax
   * @param aZMin
   * @param aZMax
   * @return
   */
	abstract_msgs::LayerQuery createPeriodLayer(string aLayerName, float frequency, bool aIsSpaceConstraint, float aXMin, float aXMax, float aYMin, float aYMax,float aZMin, float aZMax);

  /**
   * @brief createHistoryLayer, fuction to create a history layer.
   * @param aLayerName
   * @param aTimeStart
   * @param aTimeEnd
   * @param aIsSpaceConstraint
   * @param aXMin
   * @param aXMax
   * @param aYMin
   * @param aYMax
   * @param aZMin
   * @param aZMax
   * @return
   */
  abstract_msgs::LayerQuery createHistoryLayer(string aLayerName, char * aTimeStart, char * aTimeEnd, bool aIsSpaceConstraint, float aXMin, float aXMax, float aYMin, float aYMax,float aZMin, float aZMax);

  /**
   * @brief createEventLayer, function to create an event layer.
   * @param aLayerName
   * @param aIsSpaceConstraint
   * @param aXMin
   * @param aXMax
   * @param aYMin
   * @param aYMax
   * @param aZMin
   * @param aZMax
   * @return
   */
  abstract_msgs::LayerQuery createEventLayer(string aLayerName,  bool aIsSpaceConstraint, float aXMin, float aXMax, float aYMin, float aYMax,float aZMin, float aZMax);

  /**
   * @brief deleteLayer, fuction to delete a layer.
   * @param aLayerName
   */
  void deleteLayer(string aLayerName);

  /**
   * @brief addOneActorToLayer, add one actor to a layer
   * @param layerQuery
   * @param aActorName
   * @param aDataTypeVec
   */
  void addOneActorToLayer(abstract_msgs::LayerQuery &layerQuery, string aActorName, std::vector<string> aDataTypeVec);

  /**
   * @brief addOneEventToLayer, add one event to a layer
   * @param layerQuery
   * @param aEventName
   */
  void addOneEventToLayer(abstract_msgs::LayerQuery &layerQuery, string aEventName);

  /**
   * @brief deleteOneActorFromLayer, delete one actor from a layer.
   * @param aLayerName
   * @param aActorName
   */
  void deleteOneActorFromLayer(string aLayerName, string aActorName);

  /**
   * @brief addOneLayerToScene, add one layer to a scene query msg
   * @param aLayerQuery
   */
  void addOneLayerToScene(abstract_msgs::LayerQuery aLayerQuery);

  /**
   * @brief deleteOneEventFromLayer, delete one event from an event layer
   * @param aLayerName
   * @param aEventName
   */
  void deleteOneEventFromLayer(string aLayerName, string aEventName);

private:
	ros::NodeHandle _nh;

  ros::Subscriber _sceneQueryResultSub;

	string _sceneName;
  abstract_msgs::SceneQuery _sceneQuery;

  ros::Publisher _sceneQueryPublisher;

	micros_storage::Tools _storageTool;
	int32_t _selfID;

	bool _isStorage;
	rosbag::Bag _sceneBag;

	ros::Timer _timer;
	vector<uint8_t> _vec;

  ros::Subscriber _sub;
  ros::Subscriber _actorDataSub;

  int32_t _bufSize;

  boost::shared_ptr<micros_scene::DataPubSub> _dataPubSub;
  boost::shared_ptr<micros_scene::SceneTools> _sceneTools;
};

}//end of namespace micros_scene




#endif /* SRC_INFORMATION_SCENES_PIONEER_SCENE_INCLUDE_PIONEER_SCENE_PIONEERSCENE_H_ */
