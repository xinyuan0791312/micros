/*=================================================================
* Copyright (c) 2019, micROS Group, NIIDT, TAIIC, HPCL.
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without modification, are permitted
* provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice, this list of conditions and
* the following disclaimer.
* 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions
* and the following disclaimer in the documentation and/or other materials provided with the
* distribution.
* 3. All advertising materials mentioning features or use of this software must display the following
* acknowledgement: This product includes software developed by the micROS Group and its
* contributors.
* 4. Neither the name of the Group nor the names of contributors may be used to endorse or promote
* products derived from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY MICROS GROUP AND CONTRIBUTORS ''AS IS''AND
* ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
* PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE MICROS, GROUP OR
* CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
* EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
* PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
* PROFITS; OR BUSINESS INTERRUPTION). HOWEVER CAUSED AND ON ANY THEORY
* OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
* NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
* SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
===================================================================
* Author: Jun Cai.
*/
#include <pioneer_scene/DataPubSub.h>
/*
 * @brief PubandSub msg
 *
*/
namespace micros_scene
{
  using std::string;
  using std::vector;

  DataPubSub::DataPubSub()// NOLINT
  {
    if(_nh.hasParam("robotID"))
      _nh.param("robotID", _selfID, -1);

    if(_nh.hasParam("/robot_id"))
      _nh.param("/robot_id", _selfID, -1);

    if(_selfID == -1)
    {
    	ROS_ERROR("[scene][DataPubSub] Robot ID not set, please set robot ID on the parameter server. ");
    }

    if(_selfID == 0)
    {
		_rtpsSceneQueryPublisher.getParticipant(mp_participant);
		while(true)
		{
		  if(_rtpsSceneQueryPublisher.init("SceneQuery", 1))
		  {
			ROS_INFO("[scene][DataPubSub] RtpSceneQueryPublisher initialized!!!");
			break;
		  }
		  usleep(100000);
		}

		  _rtpsSceneQueryResultSubscriber.getParticipant(mp_participant);
		  while(true)
		  {
			  if(_rtpsSceneQueryResultSubscriber.init("SceneQueryResult", 4))
			  {
				  ROS_INFO("[scene][DataPubSub] RtpsSceneQueryResultSubscriber initialized!!! ");
				  boost::function<void(const vector<uint8_t>&)> func = boost::bind(&DataPubSub::rtpsSceneQueryResultCallback, this, _1);
				  _rtpsSceneQueryResultSubscriber.receive(func);
				  break;
			  }
			  usleep(100000);
		  }

		  _rtpsEventResultSubscriber.getParticipant(mp_participant);
		  while(true)
		  {
			  if(_rtpsEventResultSubscriber.init("Event", 6))
			  {
				  ROS_INFO("[scene][DataPubSub] RtpsEventResultSubscriber initialized!!! ");
				  boost::function<void(const vector<uint8_t>&)> func = boost::bind(&DataPubSub::rtpsEventResultCallback, this, _1);
				  _rtpsEventResultSubscriber.receive(func);
				  break;
			  }
			  usleep(100000);
		  }
    }
    else
    {
  	  _rtpsSceneQuerySubscriber.getParticipant(mp_participant);
  	  while(true)
  	  {
  		  if(_rtpsSceneQuerySubscriber.init("SceneQuery", 2))
  		  {
  			  ROS_INFO("[scene][DataPubSub] RtpsSceneQuerySubscriber initialized!!! ");
  			  boost::function<void(const vector<uint8_t>&)> func = boost::bind(&DataPubSub::rtpsSceneQueryCallback, this, _1);
  			  _rtpsSceneQuerySubscriber.receive(func);
  			  break;
  		  }
  		  usleep(100000);
  	  }


  	  _rtpsSceneQueryResultPublisher.getParticipant(mp_participant);
  	  while(true)
  	  {
  		  if( _rtpsSceneQueryResultPublisher.init("SceneQueryResult", 3))
  		  {
  			  ROS_INFO("[scene][DataPubSub] RtpsSceneQueryResultPublisher initialized!!!");
  			  break;
  		  }
  		  usleep(100000);
  	  }


  	  //_rtpsEventResultPublisher.reset(new Fastrtps_PacketPublisher());
  	  _rtpsEventResultPublisher.getParticipant(mp_participant);
  	  while(true)
  	  {
  		  if(_rtpsEventResultPublisher.init("Event", 5))
  		  {
  			  ROS_INFO("[scene][DataPubSub] RtpsEventResultPublisher initialized!!!", _selfID);
  			  break;
  		  }
  		  usleep(100000);
  	  }
    }

		_eventSub = _nh.subscribe("/eventData", 100, &DataPubSub::eventCallback, this);
    ROS_INFO("[scene][DataPubSub] Local event listener initialized!!!");
		_sceneQuerySub = _nh.subscribe("/SceneQuery", 100, &DataPubSub::sceneQueryCallback, this);
    ROS_INFO("[scene][DataPubSub] Local SceneQuery listener initialized!!!");
		_sceneQueryResultSub = _nh.subscribe("/SceneQueryResult", 100, &DataPubSub::sceneQueryResultCallback, this);
    ROS_INFO("[scene][DataPubSub] Remote SceneQueryResult listener initialized!!!"); 
     //   _eventPublisher = _nh.advertise<abstract_msgs::UnifiedData>("/eventData", 100);
        _eventCount = 0;
  }

  DataPubSub::~DataPubSub(){}//NOLINT

  void DataPubSub::sceneQueryCallback(const abstract_msgs::SceneQuery aMsg) //NOLINT
  {
		vector<uint8_t> vec;
#ifdef USING_COMPRESS_MSG
		_storageTool.compressMsg<abstract_msgs::SceneQuery>(aMsg, vec);
#else
		vec = _stor.serializeRos<abstract_msgs::SceneQuery>(aMsg);
#endif
		_rtpsSceneQueryPublisher.DDS_pub(vec);
    ROS_DEBUG("[scene][DataPubSub] DDS publish a scene query: sceneName-%s, size-%d",aMsg.sceneName.c_str(), vec.size());
  }

  void DataPubSub::sceneQueryResultCallback(const abstract_msgs::QueryResult aMsg)//NOLINT
  {
		vector<uint8_t> vec;
#ifdef USING_COMPRESS_MSG
                _storageTool.compressMsg<abstract_msgs::QueryResult>(aMsg, vec);
#else
                vec = _stor.serializeRos<abstract_msgs::QueryResult>(aMsg);
#endif
		_rtpsSceneQueryResultPublisher.DDS_pub(vec);
    ROS_DEBUG("[scene][DataPubSub] DDS publish a scene result: sceneName-%s, size-%d",aMsg.sceneName.c_str(), vec.size());
  }

  void DataPubSub::eventCallback(const abstract_msgs::UnifiedData aMsg)//NOLINT
  {
    string dataType = aMsg.dataType;
    ROS_DEBUG("[scene][DataPubSub] Receive an event %s, ready to process.",dataType.c_str());
    bool hasEvent = false;
    int32_t count = 0;
    for(int32_t i = 0;i < _eventList.size();i++)
    {
      if(dataType == _eventList[i].event)
      {
        hasEvent = true;
        for(int k = 0;k < _eventList[i].eventVec.size();k++)
        {

          if(_eventList[i].eventVec[k].spaceConstraint)
          {
            if(aMsg.positionX < _eventList[i].eventVec[k].xMin || aMsg.positionX > _eventList[i].eventVec[k].xMax \
                 || aMsg.positionY < _eventList[i].eventVec[k].yMin || aMsg.positionY > _eventList[i].eventVec[k].yMax \
                 || aMsg.positionZ < _eventList[i].eventVec[k].zMin || aMsg.positionZ > _eventList[i].eventVec[k].zMax)
            {
             // ROS_DEBUG("[scene][DataPubSub] Event %s matched but space constraint for %d not satisfied!!!",dataType.c_str(), _eventList[i].eventVec[k].receiverID);
                continue;
            }
            else{
              ROS_DEBUG("[scene][DataPubSub] Event %s matched and space constraint for <sceneName-%s,layerName-%s,robot ID-%d> satisfied!!!",dataType.c_str(), _eventList[i].eventVec[k].sceneName.c_str(), _eventList[i].eventVec[k].layerName.c_str(), _eventList[i].eventVec[k].receiverID);
            }
          }
          else{
            ROS_DEBUG("[scene][DataPubSub] Event %s matched for <sceneName-%s,layerName-%s,robot ID-%d> satisfied!!!",dataType.c_str(), _eventList[i].eventVec[k].sceneName.c_str(), _eventList[i].eventVec[k].layerName.c_str(), _eventList[i].eventVec[k].receiverID);

          }

          if(_eventList[i].eventVec[k].isStop){
           ROS_DEBUG("[scene][DataPubSub] Event %s matched for but <sceneName-%s,layerName-%s,robot ID-%d> sceneQuery in stop state!!!",dataType.c_str(), _eventList[i].eventVec[k].sceneName.c_str(), _eventList[i].eventVec[k].layerName.c_str(), _eventList[i].eventVec[k].receiverID);

           continue;
          }
            
          count++;
          _eventCount++;
          if(_eventCount == 100000000)
            _eventCount = 0;

          abstract_msgs::EventResult msg;
          msg.senderID = _selfID;
          msg.receiverID = _eventList[i].eventVec[k].receiverID;
          msg.layerName = _eventList[i].eventVec[k].layerName;
          msg.sceneName = _eventList[i].eventVec[k].sceneName;
          msg.data = aMsg;
          msg.eventCount = _eventCount;

          vector<uint8_t> vec;
#ifdef USING_COMPRESS_MSG
          _storageTool.compressMsg<abstract_msgs::EventResult>(msg, vec);
#else
          vec = _stor.serializeRos<abstract_msgs::EventResult>(msg);
#endif
           _rtpsEventResultPublisher.DDS_pub(vec);
              ROS_DEBUG("[scene][DataPubSub] DDS publish an event message to <sceneName-%s,layerName-%s,robot ID-%d>!!!", _eventList[i].eventVec[k].sceneName.c_str(), _eventList[i].eventVec[k].layerName.c_str(), _eventList[i].eventVec[k].receiverID);
        }
        break;
      }

    }
    if(!hasEvent){
      ROS_DEBUG("[scene][DataPubSub] Event-%s not exist in the event map list",dataType.c_str());
    }
    
    ROS_DEBUG("[scene][DataPubSub] Process event %s, send message to %d event subscribers.",dataType.c_str(), count);
  }

        void DataPubSub::rtpsSceneQueryResultCallback(vector<uint8_t> aMsg)//NOLINT
	{
#ifdef USING_COMPRESS_MSG
		std::vector<uint8_t> uncompressedData;
		_storageTool.uncompressMsg(aMsg, uncompressedData);
		abstract_msgs::QueryResult result = _storageTool.deserializeRos<abstract_msgs::QueryResult>(uncompressedData);
#else
		abstract_msgs::QueryResult result = _stor.deserializeRos<abstract_msgs::QueryResult>(aMsg);
#endif
    if(result.receiverID == _selfID){
                if(_sceneQueryResultPubs[result.sceneName] == NULL)
		{
      ros::Publisher tmpPub;
      _sceneQueryResultPubs[result.sceneName] = tmpPub;
      _sceneQueryResultPubs[result.sceneName] = _nh.advertise<abstract_msgs::QueryResult>(result.sceneName, 100);
		}
		_sceneQueryResultPubs[result.sceneName].publish(result);
    ROS_DEBUG("[scene][DataPubSub] Reveive a sceneResult for %s: compressed size-%d, uncompressed size-%d.",result.sceneName, aMsg.size(), uncompressedData.size());
    }
    else{
      ROS_DEBUG("[scene][DataPubSub] Reveive a sceneResult but not to me: compressed size-%d, uncompressed size-%d.",aMsg.size(), uncompressedData.size());
    }
	}

        void DataPubSub::rtpsEventResultCallback(vector<uint8_t> aMsg)//NOLINT
	{

		
#ifdef USING_COMPRESS_MSG
		std::vector<uint8_t> uncompressedData;
		_storageTool.uncompressMsg(aMsg, uncompressedData);
		abstract_msgs::EventResult event = _storageTool.deserializeRos<abstract_msgs::EventResult>(uncompressedData);
#else
		abstract_msgs::EventResult event = _stor.deserializeRos<abstract_msgs::EventResult>(aMsg);;
#endif

		if(event.receiverID == _selfID)
		{
			abstract_msgs::QueryResult result;
			result.senderID = event.senderID;
			result.receiverID = event.receiverID;
			result.layerName = event.layerName;
			result.sceneName = event.sceneName;
			result.data = event.data;
			result.count = event.eventCount;
                        if(_sceneQueryResultPubs[result.sceneName] == NULL)
			{
        ros::Publisher tmpPub;
        _sceneQueryResultPubs[result.sceneName] = tmpPub;
        _sceneQueryResultPubs[result.sceneName] = _nh.advertise<abstract_msgs::QueryResult>(result.sceneName, 100);
			}
			_sceneQueryResultPubs[result.sceneName].publish(result);
      ROS_DEBUG("[scene][DataPubSub] Reveive a eventResult for %s: compressed size-%d, uncompressed size-%d.",result.sceneName, aMsg.size(), uncompressedData.size());
		}
     else{
      ROS_DEBUG("[scene][DataPubSub] Reveive a eventResult but not to me: compressed size-%d, uncompressed size-%d.",aMsg.size(), uncompressedData.size());
    }
	}

  void DataPubSub::rtpsSceneQueryCallback(vector<uint8_t> aMsg)// NOLINT
  {
	  //  ROS_INFO("the size of scene query is %d",aMsg.size());
#ifdef USING_COMPRESS_MSG
		std::vector<uint8_t> uncompressedData;
		_storageTool.uncompressMsg(aMsg, uncompressedData);
//		ROS_INFO("the size of uncompressed scene query is %d",uncompressedData.size());
		abstract_msgs::SceneQuery query = _storageTool.deserializeRos<abstract_msgs::SceneQuery>(uncompressedData);
#else
		abstract_msgs::SceneQuery query = _stor.deserializeRos<abstract_msgs::SceneQuery>(aMsg);
#endif

    if((query.targetRobotID != _selfID) && (query.targetRobotID != -1))
    {
      ROS_DEBUG("[scene][DataPubSub] SceneQuery targetRobotID is %d but my id is %d, ignore this sceneQuery!!!", query.targetRobotID, _selfID);
      return;
    }
      

	  if(query.isHeartbeat)
		  ROS_DEBUG("[scene][DataPubSub] Receive a heatbeat sceneQuery %s from robot %d !!!",query.sceneName.c_str(), query.senderID);
	  else
		  ROS_DEBUG("[scene][DataPubSub] Receive a sceneQuery %s from robot %d !!!",query.sceneName.c_str(), query.senderID);

	  if(query.isStop)
	  {
      for(vector <boost::shared_ptr<micros_scene::QueryHandle>> ::iterator it = _queryHandleVec.begin();it != _queryHandleVec.end();it++)
		  {
        if(query.sceneName == (*it)->getInput().sceneName && query.senderID == (*it)->getInput().senderID)
        {
          ROS_DEBUG("[scene][DataPubSub] The sceneQuery %s from robot %d is a stop signal!!!",query.sceneName.c_str(), query.senderID);

          abstract_msgs::SceneQuery tmpSceneQuery = (*it)->getInput();
          tmpSceneQuery.isStop = true;
          (*it)->setInput(tmpSceneQuery);
          (*it)->calculateHash();
          (*it)->stopTimers();
          ROS_DEBUG("[scene][DataPubSub] Stop the period layers of the sceneQuery %s.",query.sceneName.c_str());

          for(int j = 0;j < (*it)->getInput().layerVec.size();j++)
          {
            if((*it)->getInput().layerVec[j].layerType == 2)
            {
              for(int32_t i = 0;i < _eventList.size();i++)
              {
                for(int32_t k = 0;k < (*it)->getInput().layerVec[j].eventMsgVec.size();k++)
                {
                  if((*it)->getInput().layerVec[j].eventMsgVec[k] == _eventList[i].event)
                  {
                    for(vector <abstract_msgs::EventQuery >::iterator it2 = _eventList[i].eventVec.begin();it2 != _eventList[i].eventVec.end();it2++)
                    {
                      if(it2->sceneName == (*it)->getInput().sceneName&& \
                          it2->receiverID == (*it)->getInput().senderID&& \
                        it2->layerName == (*it)->getInput().layerVec[j].layerName)
                      {
                        
                        ROS_DEBUG("[scene][DataPubSub] Erase event <sceneName-%s,layerName-%s,robot ID-%d> from local event map list.", it2->sceneName.c_str(), it2->layerName.c_str(), it2->receiverID);
                        it2 = _eventList[i].eventVec.erase(it2);
                        break;
                      }
                    }
                  }
                }
              }
            }
          }

          break;
        }
		  }
		  return;
	  }

	  boost::shared_ptr<micros_scene::QueryHandle> tmp;
  	  tmp.reset(new micros_scene::QueryHandle());
	  tmp->setInput(query);
	  tmp->calculateHash();
	  string hash = tmp->getHash();

          bool hasScene = false;
          int pos = -1;
          for(pos = 0;pos < _queryHandleVec.size();pos++)
	  {
                  if(query.sceneName == _queryHandleVec[pos]->getInput().sceneName&&query.senderID == _queryHandleVec[pos]->getInput().senderID)
                  {
                          hasScene = true;
			  break;
		  }
	  }

	  if(!hasScene)
	  {
      ROS_DEBUG("[scene][DataPubSub] Receive a new sceneQuery %s from robot %d and process.",query.sceneName.c_str(), query.senderID);
		  _queryHandleVec.push_back(tmp);
      
                  for(int j = 0;j < tmp->getInput().layerVec.size();j++)
		  {
				switch(tmp->getInput().layerVec[j].layerType)
				{
					case 0:
						tmp->parsePeriodLayerQuery(tmp->getInput().layerVec[j]);
						break;
					case 1:
						tmp->parseHistoryLayerQuery(tmp->getInput().layerVec[j]);
						break;
					case 2:
						tmp->parseEventLayerQuery(tmp->getInput().layerVec[j], _eventList);
						break;
					default:
						break;
				}
		  }
      
	  }

	  else
	  {
		  if(query.isHeartbeat)
			  return;

      if(hash == _queryHandleVec[pos]->getHash())
      {
        for(int j = 0;j < _queryHandleVec[pos]->getInput().layerVec.size();j++)
        {
          if(_queryHandleVec[pos]->getInput().layerVec[j].layerType == 1)
            tmp->parseHistoryLayerQuery(_queryHandleVec[pos]->getInput().layerVec[j]);
        }
        return;
      }
      else
      {//update query
        ROS_DEBUG("[scene][DataPubSub] SceneQuery %s is updating!!!!", query.sceneName.c_str());

        _queryHandleVec[pos].swap(tmp);
        tmp->stopTimers();

        for(int j = 0;j < _queryHandleVec[pos]->getInput().layerVec.size();j++)
        {
          switch(_queryHandleVec[pos]->getInput().layerVec[j].layerType)
          {
            case 0:
              _queryHandleVec[pos]->parsePeriodLayerQuery(_queryHandleVec[pos]->getInput().layerVec[j]);
              break;
            case 1:
              _queryHandleVec[pos]->parseHistoryLayerQuery(_queryHandleVec[pos]->getInput().layerVec[j]);
              break;
            case 2:
              _queryHandleVec[pos]->parseEventLayerQuery(_queryHandleVec[pos]->getInput().layerVec[j], _eventList);
              break;
            default:
              break;
          }
        }
        return;
      }
    }
  }

}//end of namaspace
