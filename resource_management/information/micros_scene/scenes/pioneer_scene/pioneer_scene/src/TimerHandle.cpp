/*=================================================================
* Copyright (c) 2019, micROS Group, NIIDT, TAIIC, HPCL.
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without modification, are permitted
* provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice, this list of conditions and
* the following disclaimer.
* 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions
* and the following disclaimer in the documentation and/or other materials provided with the
* distribution.
* 3. All advertising materials mentioning features or use of this software must display the following
* acknowledgement: This product includes software developed by the micROS Group and its
* contributors.
* 4. Neither the name of the Group nor the names of contributors may be used to endorse or promote
* products derived from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY MICROS GROUP AND CONTRIBUTORS ''AS IS''AND
* ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
* PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE MICROS, GROUP OR
* CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
* EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
* PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
* PROFITS; OR BUSINESS INTERRUPTION). HOWEVER CAUSED AND ON ANY THEORY
* OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
* NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
* SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
===================================================================
* Author: Jun Cai.
*/
#include <pioneer_scene/TimerHandle.h>

namespace micros_scene
{
  using std::string;
  using std::vector;

  TimerHandle::TimerHandle(int32_t aSenderID, int32_t aReceiverID, string aSceneName, abstract_msgs::LayerQuery aPeriodLayerQuery)
  {
	  _senderID = aSenderID;
    _receiverID = aReceiverID;
    _sceneName = aSceneName;
    _layerName = aPeriodLayerQuery.layerName;
    _periodLayerQuery = aPeriodLayerQuery;

	  _sceneResultLocalPub = _nh.advertise<abstract_msgs::QueryResult>(_sceneName, 100);
	  _sceneResultRemotePub = _nh.advertise<abstract_msgs::QueryResult>("/SceneQueryResult", 100);

	  int32_t actorNum = aPeriodLayerQuery.actorVec.size();
	  int32_t j;
      for(j = 0;j < actorNum;j++)
	  {
		  abstract_msgs::Query query;

		  query.xMin = aPeriodLayerQuery.xMin;
		  query.xMax = aPeriodLayerQuery.xMax;
		  query.yMin = aPeriodLayerQuery.yMin;
		  query.yMax = aPeriodLayerQuery.yMax;
		  query.zMin = aPeriodLayerQuery.zMin;
		  query.zMax = aPeriodLayerQuery.zMax;

		  query.frequency = aPeriodLayerQuery.frequency;

		  query.swarmID = aPeriodLayerQuery.actorVec[j].swarmID;
		  query.actorName = aPeriodLayerQuery.actorVec[j].actorName;
		  query.robotID = aPeriodLayerQuery.actorVec[j].robotID;
		  query.dataTypeVec = aPeriodLayerQuery.actorVec[j].dataTypeVec;
          query.taskName = aPeriodLayerQuery.actorVec[j].taskName;

		  _actorQueries.push_back(query);
                  _count = 0;
	  }

    vector<string> dataTypeVec;
    for(int i = 0;i < _actorQueries.size();i++)
    {
        for(int j = 0;j < _actorQueries[i].dataTypeVec.size();j++)
    		dataTypeVec.push_back(_actorQueries[i].dataTypeVec[j]);
    }

    sort(dataTypeVec.begin(), dataTypeVec.end());
    dataTypeVec.erase(unique(dataTypeVec.begin(), dataTypeVec.end()), dataTypeVec.end());

		ROS_DEBUG("[scene][TimerHandle] Create a PeriodLayerQuery process envirment <sceneName-%s,layerName-%s>",_sceneName.c_str(),_layerName.c_str());

    for(int k = 0;k < dataTypeVec.size();k++)
    {
    	string topicName = "";
        if(_senderID >= 0)
                topicName += "/robot_"+std::to_string(_senderID) + "/" + dataTypeVec[k];
    	else
    	{
    		ROS_ERROR("[scene][TimerHandle] Robot id is wrong, whose value is %d, <sceneName-%s,layerName-%s>", _senderID,_sceneName.c_str(),_layerName.c_str());
    		topicName += dataTypeVec[k];
    	}
    	

    	ros::Subscriber tmpSub;
    	_actorDataSubs[dataTypeVec[k]] = tmpSub;

        _actorDataSubs[dataTypeVec[k]] = _nh.subscribe<topic_tools::ShapeShifter>(topicName, 10000, boost::bind(&TimerHandle::commonDataSubCallback,this,_1,dataTypeVec[k]));
			
			ROS_DEBUG("[scene][TimerHandle] Create a topic_tools::ShapeShifter callback for <sceneName-%s,layerName-%s, topicName-%s>",_sceneName.c_str(),_layerName.c_str(), topicName.c_str());	
    }
  }

  TimerHandle::~TimerHandle(){}

  void TimerHandle::setSenderID(int32_t ID)
  {
	  _senderID = ID;
  }

  void TimerHandle::setReceiverID(int32_t ID)
  {
	  _receiverID = ID;
  }

  void TimerHandle::setSceneName(string name)
  {
	  _sceneName = name;
  }

  void TimerHandle::setLayerName(string name)
  {
	  _layerName = name;
  }

  void TimerHandle::setQueryMsg(abstract_msgs::LayerQuery aLayerQuery)
  {
	  _periodLayerQuery = aLayerQuery;
  }

  int32_t TimerHandle::getSenderID()
  {
    return _senderID;
  }

  int32_t TimerHandle::getReceiverID()
  {
    return _receiverID;
  }

  string TimerHandle::getSceneName()
  {
    return _sceneName;
  }

  string TimerHandle::getLayerName()
  {
    return _layerName;
  }

  abstract_msgs::LayerQuery TimerHandle::getQueryMsg()
  {
    return _periodLayerQuery;
  }


	void TimerHandle::commonDataSubCallback(const topic_tools::ShapeShifter::ConstPtr& aMsg, const std::string &aTopicName)
	{

		if(_activeActors.activeActorNames.size() == 0){
      ROS_DEBUG("[scene][TimerHandle] ActiveActors num is zero, the msg callback of <sceneName-%s,layerName-%s, topicName-%s> return.",_sceneName.c_str(),_layerName.c_str(), aTopicName.c_str());
			return;
		}
		else
		{
			abstract_msgs::UnifiedData actorData;

			std::vector<uint8_t> buffer;
			// copy raw memory into the buffer
			buffer.resize( aMsg->size() );
			ros::serialization::OStream stream(buffer.data(), buffer.size());
			aMsg->write(stream);
            actorData.data = buffer;

			_actorDataBuffers[aTopicName].clear();

			actorData.timeStamp = std::to_string(_storageTool.getCurrentTime());
			actorData.robotID = _senderID;
			actorData.swarmID = -1;

			actorData.positionX = _robotPosition.x;
			actorData.positionY = _robotPosition.y;
			actorData.positionZ = _robotPosition.z;

			actorData.dataType = aTopicName;
			actorData.taskName = _swarmActiveActors.swarmName;

		  //write data to mem vec
		  for(int32_t i = 0;i < _activeActors.activeActorNames.size();i++)
		 {
			  actorData.actorName = _activeActors.activeActorNames[i];
			  _actorDataBuffers[aTopicName].push_back(actorData);
				ROS_DEBUG("[scene][TimerHandle] Insert a UnifiedData< topicName-%s,actorName-%s,> to actorDataBuffers for <sceneName-%s,layerName-%s>",aTopicName.c_str(),actorData.actorName.c_str(),_sceneName.c_str(),_layerName.c_str());
		}
	}
}

  void TimerHandle::createTimerHandle(int32_t aFlag)
  {
	  if(aFlag == 0)
	  {
		  _timer = _nh.createTimer(ros::Duration(_actorQueries[0].frequency), &TimerHandle::localPublishCallback, this);
		  _sceneResultLocalPub = _nh.advertise<abstract_msgs::QueryResult>(_sceneName, 100);
			ROS_INFO("[scene][TimerHandle] Create a local timerHandle for <sceneName-%s,layerName-%s>, publish sceneQueryResult to local.",_sceneName.c_str(),_layerName.c_str());
	  }
	  else
	  {
		  _timer = _nh.createTimer(ros::Duration(_actorQueries[0].frequency), &TimerHandle::remotePublishCallback, this);
		  _sceneResultRemotePub = _nh.advertise<abstract_msgs::QueryResult>("/SceneQueryResult", 100);
			ROS_INFO("[scene][TimerHandle] Create a remote timerHandle for <sceneName-%s,layerName-%s>,publish sceneQueryResult to data transfer engine.",_sceneName.c_str(),_layerName.c_str());
	  }
  }

  void TimerHandle::localPublishCallback(const ros::TimerEvent&)
  {  
		
	  ROS_DEBUG("[scene][TimerHandle] this is localPublish timerCallback of <sceneName-%s,layerName-%s>.",_sceneName.c_str(), _layerName.c_str());

  for(int32_t i = 0;i < _actorQueries.size();i++)
	{
    for(int32_t k = 0;k < _activeActors.activeActorNames.size();k++)
	  {
		  if(_actorQueries[i].actorName == _activeActors.activeActorNames[k])
	     {
			  for(int32_t j = 0;j < _actorQueries[i].dataTypeVec.size();j++)
			  {
				  if(_actorQueries[i].dataTypeVec[j] == "null")
					  {
							abstract_msgs::QueryResult msg;
							msg.senderID = _senderID;
							msg.receiverID = _receiverID;
							msg.layerName = _layerName;
							msg.sceneName = _sceneName;

							abstract_msgs::UnifiedData actorData;
							actorData.actorName = _actorQueries[i].actorName;
							actorData.robotID = _senderID;

							actorData.swarmID = -1;

							actorData.positionX = _robotPosition.x;
							actorData.positionY = _robotPosition.y;
							actorData.positionZ = _robotPosition.z;

							actorData.taskName = _swarmActiveActors.swarmName;

							msg.data = actorData;

							_count++;
							if(_count == 100000000)
									_count = 0;

							msg.count = _count;
							_sceneResultLocalPub.publish(msg);
							ROS_DEBUG("[scene][TimerHandle] DataTypeVec of <actorname: %s,sceneName-%s,layerName-%s> is null, publish robotPosition info to local .",actorData.actorName.c_str(),_sceneName.c_str(),_layerName.c_str());
					  }
				  }
			  }
		  }
	  }

    for(map<string, vector<abstract_msgs::UnifiedData>>::iterator it = _actorDataBuffers.begin();it != _actorDataBuffers.end();it++)
	  {
		  if(it->second.size() == 0)
		  {
			  ROS_DEBUG("[scene][TimerHandle] buffer for data type %s has no data of <sceneName-%s,layerName-%s>.", it->first.c_str(),_sceneName.c_str(),_layerName.c_str());
			  continue;
		  }
		  else
		  {
				abstract_msgs::QueryResult msg;
				msg.senderID = _senderID;
				msg.receiverID = _receiverID;
				msg.layerName = _layerName;
				msg.sceneName = _sceneName;

        for(int32_t i = 0;i < it->second.size();i++)
        {
          for(int32_t j = 0;j < _actorQueries.size();j++)
					{
            if(_actorQueries[j].actorName.compare(it->second[i].actorName) == 0)
						{
							ROS_DEBUG("[scene][TimerHandle] Prepare data for %s of <sceneName-%s,layerName-%s,actorname-%s>.", it->first.c_str(),_sceneName.c_str(),_layerName.c_str(),_actorQueries[j].actorName.c_str());
							msg.data = it->second[i];
							float x = msg.data.positionX;
							float y = msg.data.positionY;
							float z = msg.data.positionZ;

							ActorPosition tmp;
							tmp.x = x;
							tmp.y = y;
							tmp.z = z;

							if(_periodLayerQuery.spaceConstraint)
							{
                if(x < _periodLayerQuery.xMin || x > _periodLayerQuery.xMax || y < _periodLayerQuery.yMin || y > _periodLayerQuery.yMax || z<_periodLayerQuery.zMin || z > _periodLayerQuery.zMax)
								{
									ROS_INFO("[scene][TimerHandle] Space constraint of <sceneName-%s,layerName-%s,actorname-%s,dataType-%s> not satisfied.",_sceneName.c_str(),_layerName.c_str(),_actorQueries[j].actorName.c_str(), it->first.c_str());
									break;
								}
							}

							if(_periodLayerQuery.isOptimized)
							{
                if(_lastActorData[_actorQueries[j].actorName].count == 0 || _lastActorData[_actorQueries[j].actorName].count == _periodLayerQuery.updateThreshold)
								{
									_count++;
									if(_count == 100000000)
											_count = 0;

									msg.count = _count;
									vector<uint8_t> vec;
									vec = _storageTool.serializeRos<abstract_msgs::QueryResult>(msg);

									tmp.count = 1;
									_lastActorData[_actorQueries[j].actorName] = tmp;
									_sceneResultLocalPub.publish(msg);
									ROS_DEBUG("[scene][TimerHandle] PeriodLayerQuery is position optimized for  <sceneName-%s,layerName-%s>, updateThreshold %d is reached, publish data of <actorname-%s,dataType-%s> to local.",_sceneName.c_str(),_layerName.c_str(),_periodLayerQuery.updateThreshold,_actorQueries[j].actorName.c_str(), it->first.c_str());
								}
                else if(_lastActorData[_actorQueries[j].actorName].count > 0 && _lastActorData[_actorQueries[j].actorName].count < _periodLayerQuery.updateThreshold)
								{
									float x1 = _lastActorData[_actorQueries[j].actorName].x;
									float y1 = _lastActorData[_actorQueries[j].actorName].y;
									float z1 = _lastActorData[_actorQueries[j].actorName].z;

									float distance = sqrt(pow((x - x1),2) + pow((y - y1),2) + pow((z - z1),2));
                  if(distance <= _periodLayerQuery.distanceThreshold)
									{
										_lastActorData[_actorQueries[j].actorName].count++;
										break;
									}
									else
									{
										_count++;
										if(_count == 100000000)
												_count = 0;

										msg.count = _count;
										vector<uint8_t> vec;
										vec = _storageTool.serializeRos<abstract_msgs::QueryResult>(msg);
										tmp.count = 1;
										_lastActorData[_actorQueries[j].actorName] = tmp;
										_sceneResultLocalPub.publish(msg);
										ROS_DEBUG("[scene][TimerHandle] PeriodLayerQuery is position optimized for  <sceneName-%s,layerName-%s>, count > 0, distanceThreshold %f is reached, publish data of <actorname-%s,dataType-%s> to local.",_sceneName.c_str(),_layerName.c_str(),_periodLayerQuery.distanceThreshold, _actorQueries[j].actorName.c_str(), it->first.c_str());
									}
								}
							}
							else
							{
								_count++;
								if(_count == 100000000)
										_count = 0;

								msg.count = _count;
								vector<uint8_t> vec;
								vec = _storageTool.serializeRos<abstract_msgs::QueryResult>(msg);
								_sceneResultLocalPub.publish(msg);
								ROS_DEBUG("[scene][TimerHandle] Publish msg to local of <sceneName-%s,layerName-%s,actorname-%s,dataType-%s>",_sceneName.c_str(),_layerName.c_str(),_actorQueries[j].actorName.c_str(), it->first.c_str());
							}
						}
					}
				}
		  }
	  }
  }

  void TimerHandle::remotePublishCallback(const ros::TimerEvent&)
  {
	  ROS_DEBUG("[scene][TimerHandle] this is localPublish timerCallback of <sceneName-%s,layerName-%s>.",_sceneName.c_str(),_layerName.c_str());

    if((_senderID != _periodLayerQuery.targetRobotID) && (_periodLayerQuery.targetRobotID != -1))
	  {
		  ROS_ERROR("[scene][TimerHandle] The <sceneName-%s,layerName-%s> of targetRobotID is %d, but the RobotID is %d", _sceneName.c_str(),_layerName.c_str(),_periodLayerQuery.targetRobotID, _senderID);
		  return;
	  }


       for(int i = 0;i < _actorQueries.size();i++)
	  {
           for(int k = 0;k < _activeActors.activeActorNames.size();k++)
		  {
               if(_actorQueries[i].actorName == _activeActors.activeActorNames[k])
			  {
				   for(int j = 0;j < _actorQueries[i].dataTypeVec.size();j++)
				  {
					   if(_actorQueries[i].dataTypeVec[j] == "null")
					  {
							abstract_msgs::QueryResult msg;
							msg.senderID = _senderID;
							msg.receiverID = _receiverID;
							msg.layerName = _layerName;
							msg.sceneName = _sceneName;

							abstract_msgs::UnifiedData actorData;
							actorData.actorName = _actorQueries[i].actorName;
							actorData.robotID = _senderID;

							actorData.swarmID = -1;

							actorData.positionX = _robotPosition.x;
							actorData.positionY = _robotPosition.y;
							actorData.positionZ = _robotPosition.z;

							actorData.taskName = _swarmActiveActors.swarmName;

							msg.data =actorData;

							_count++;
							if(_count == 100000000)
									_count = 0;

							msg.count = _count;

								ROS_DEBUG("[scene][TimerHandle] DataTypeVec of <actorname-%s,sceneName-%s,layerName-%s> is null, publish robotPosition info to data transfer engine.",actorData.actorName.c_str(), _sceneName.c_str(),_layerName.c_str());
							_sceneResultRemotePub.publish(msg);
					  }
				  }
			  }
		  }
	  }

          for(map<string, vector<abstract_msgs::UnifiedData> >::iterator it = _actorDataBuffers.begin();it != _actorDataBuffers.end();it++)
	  {
		  if(it->second.size() == 0)
		  {
			   ROS_DEBUG("[scene][TimerHandle] buffer for data type %s has no data of <sceneName-%s,layerName-%s>.", it->first.c_str(),_sceneName.c_str(),_layerName.c_str());
			  continue;
		  }
		  else
		  {
				abstract_msgs::QueryResult msg;
				msg.senderID = _senderID;
				msg.receiverID = _receiverID;
				msg.layerName = _layerName;
				msg.sceneName = _sceneName;

				for(int i = 0;i < it->second.size();i++)
				{
					for(int j = 0;j < _actorQueries.size();j++)
					{
						if(_actorQueries[j].actorName.compare(it->second[i].actorName) == 0)
						{
							msg.data = it->second[i];
							float x = msg.data.positionX;
							float y = msg.data.positionY;
							float z = msg.data.positionZ;

							ActorPosition tmp;
							tmp.x = x;
							tmp.y = y;
							tmp.z = z;

							if(_periodLayerQuery.spaceConstraint)
							{
								if(x < _periodLayerQuery.xMin || x > _periodLayerQuery.xMax || y < _periodLayerQuery.yMin || y > _periodLayerQuery.yMax || z < _periodLayerQuery.zMin || z > _periodLayerQuery.zMax)
								{
										ROS_INFO("[scene][TimerHandle] Space constraint of <sceneName-%s,layerName-%s,actorname-%s,dataType-%s> not satisfied.",_sceneName.c_str(),_layerName.c_str(),_actorQueries[j].actorName.c_str(), it->first.c_str());
									break;
								}
							}

							if(_periodLayerQuery.isOptimized)
							{
								if(_lastActorData[_actorQueries[j].actorName].count == 0 || _lastActorData[_actorQueries[j].actorName].count == _periodLayerQuery.updateThreshold)
								{
									_count++;
									if(_count == 100000000)
											_count = 0;

									msg.count = _count;
									tmp.count = 1;
									_lastActorData[_actorQueries[j].actorName] = tmp;
									_sceneResultRemotePub.publish(msg);
									ROS_DEBUG("[scene][TimerHandle] PeriodLayerQuery is position optimized for  <sceneName-%s,layerName-%s>, updateThreshold %d is reached, publish data of <actorname-%s,dataType-%s> to data transfer engine.",_sceneName.c_str(),_layerName.c_str(),_periodLayerQuery.updateThreshold,_actorQueries[j].actorName.c_str(), it->first.c_str());
								}
								else if(_lastActorData[_actorQueries[j].actorName].count > 0 && _lastActorData[_actorQueries[j].actorName].count < _periodLayerQuery.updateThreshold)
								{
									float x1 = _lastActorData[_actorQueries[j].actorName].x;
									float y1 = _lastActorData[_actorQueries[j].actorName].y;
									float z1 = _lastActorData[_actorQueries[j].actorName].z;

									float distance = sqrt(pow((x - x1),2)+pow((y-y1),2)+pow((z-z1),2));
									
									if(distance <= _periodLayerQuery.distanceThreshold)
									{
										_lastActorData[_actorQueries[j].actorName].count++;
										break;
									}
									else
									{
										_count++;
										if(_count == 100000000)
											_count=0;

										msg.count = _count;

										tmp.count = 1;
										_lastActorData[_actorQueries[j].actorName] = tmp;
										_sceneResultRemotePub.publish(msg);
										ROS_DEBUG("[scene][TimerHandle] PeriodLayerQuery is position optimized for  <sceneName-%s,layerName-%s>, count > 0, distanceThreshold %f is reached, publish data of <actorname-%s,dataType-%s> to data transfer engine.",_sceneName.c_str(),_layerName.c_str(),_periodLayerQuery.distanceThreshold, _actorQueries[j].actorName.c_str(), it->first.c_str());
									}
								}
							}
							else
							{
								_count++;
								if(_count == 100000000)
										_count = 0;
								msg.count = _count;

								_sceneResultRemotePub.publish(msg);
								ROS_DEBUG("[scene][TimerHandle] Publish msg to data transfer engine of <sceneName-%s,layerName-%s,actorname-%s,dataType-%s>",_sceneName.c_str(),_layerName.c_str(),_actorQueries[j].actorName.c_str(), it->first.c_str());
							}
						}
					}
				}
		  }
	  }
  }

  void TimerHandle::start()
  {
    _timer.start();
		ROS_INFO("[scene][TimerHandle] The timerHandle of period <sceneName-%d,layerName-%d> start.",_sceneName.c_str(),_layerName.c_str());
  }
      
  void TimerHandle::stop()
  {
    _timer.stop();
		ROS_INFO("[scene][TimerHandle] The timerHandle of period <sceneName-%d,layerName-%d> stop.",_sceneName.c_str(),_layerName.c_str());
  }

}
