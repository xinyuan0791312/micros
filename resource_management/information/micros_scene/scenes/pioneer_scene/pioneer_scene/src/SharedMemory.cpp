/*=================================================================
* Copyright (c) 2019, micROS Group, NIIDT, TAIIC, HPCL.
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without modification, are permitted
* provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice, this list of conditions and
* the following disclaimer.
* 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions
* and the following disclaimer in the documentation and/or other materials provided with the
* distribution.
* 3. All advertising materials mentioning features or use of this software must display the following
* acknowledgement: This product includes software developed by the micROS Group and its
* contributors.
* 4. Neither the name of the Group nor the names of contributors may be used to endorse or promote
* products derived from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY MICROS GROUP AND CONTRIBUTORS ''AS IS''AND
* ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
* PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE MICROS, GROUP OR
* CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
* EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
* PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
* PROFITS; OR BUSINESS INTERRUPTION). HOWEVER CAUSED AND ON ANY THEORY
* OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
* NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
* SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
===================================================================
* Author: Jun Cai.
*/
#include <pioneer_scene/SharedMemory.h>


namespace micros_scene
{
    SharedMemory::SharedMemory( )
    {
        _size = 4096;
    }

    SharedMemory::~SharedMemory()
    {

    }

    int SharedMemory::createSharedMemory()
    {
    	int id;
        key_t key = ftok("/home",100);
    	if(key < 0)
    	{
    		ROS_DEBUG("[scene][sharedMemory] Get key error");
    		return -1;
    	}

    	id = shmget(key,_size,IPC_CREAT|IPC_EXCL|0666);
    	if(id < 0)
    	{
				ROS_DEBUG("[scene][sharedMemory] Create shared memory error!!!");
    		return -1;
    	}
    	return id;
	}

    int SharedMemory::getSharedMemoryID()
    {
    	int id;
        key_t key = ftok("/home",100);
    	if(key < 0)
    	{
				ROS_DEBUG("[scene][sharedMemory] Get key error.");
    		return -1;
    	}

    	id = shmget(key,_size,IPC_CREAT|0666);
    	if(id < 0)
    	{
				ROS_DEBUG("[scene][sharedMemory] Get shared memory error.");
    		return -1;
    	}
    	return id;
    }

    void SharedMemory::write(abstract_msgs::SwarmActiveActors aMsg)
    {

		int id = getSharedMemoryID();
		if(id < 0)
		{
				ROS_DEBUG("[scene][sharedMemory] Write shared memory error.");
			return;
		}

    	abstract_msgs::SwarmActiveActors *p;
		p = (abstract_msgs::SwarmActiveActors *)shmat(id,NULL,0);
		if(p < 0)
		{
			ROS_DEBUG("[scene][sharedMemory] Get shared memory addr error.");
			p = NULL;
			return;
		}

                p->swarmName = aMsg.swarmName;
                p->swarmActiveActors = aMsg.swarmActiveActors;

    }

    void SharedMemory::read(abstract_msgs::SwarmActiveActors &aMsg)
    {

		int id = getSharedMemoryID();
		if(id < 0)
		{
				ROS_DEBUG("[scene][sharedMemory] Get shared memory memory error.");
			return;
		}

    	abstract_msgs::SwarmActiveActors *p;
		p = (abstract_msgs::SwarmActiveActors *)shmat(id,NULL,0);
		if(p < 0)
		{
		  	ROS_DEBUG("[scene][sharedMemory] Get shared memory addr error.");
	
			p = NULL;
			return;
		}

                aMsg.swarmName = p->swarmName;
                aMsg.swarmActiveActors = p->swarmActiveActors;

    }

    void SharedMemory::deleteSharedMemory()
    {
        int id = getSharedMemoryID();
    	int ret = shmctl(id,IPC_RMID,NULL);

                if(ret < 0)
		{
				ROS_DEBUG("[scene][sharedMemory] Shared memory destroy error.");
		}
	}



}//end of namespace micros_scene


