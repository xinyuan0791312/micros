/**=================================================================
* Copyright (c) 2019, micROS Group, NIIDT, TAIIC, HPCL.
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without modification, are permitted
* provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice, this list of conditions and
* the following disclaimer.
* 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions
* and the following disclaimer in the documentation and/or other materials provided with the
* distribution.
* 3. All advertising materials mentioning features or use of this software must display the following
* acknowledgement: This product includes software developed by the micROS Group and its
* contributors.
* 4. Neither the name of the Group nor the names of contributors may be used to endorse or promote
* products derived from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY MICROS GROUP AND CONTRIBUTORS ''AS IS''AND
* ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
* PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE MICROS, GROUP OR
* CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
* EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
* PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
* PROFITS; OR BUSINESS INTERRUPTION). HOWEVER CAUSED AND ON ANY THEORY
* OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
* NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
* SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
===================================================================
* Author: Jun Cai.
*/
#include <pioneer_scene/QueryHandle.h>
#include <openssl/sha.h>
/**
 *@brief scene namespace
 */
namespace micros_scene
{
  using std::string;
  using std::vector;

  QueryHandle::QueryHandle()
  {
		if(_nh.hasParam("robotID"))
			_nh.param("robotID", _selfID, -1);

    if (_nh.hasParam("/robot_id"))
      _nh.param("/robot_id", _selfID, -1);
  }

  QueryHandle::~QueryHandle(){}

  void QueryHandle::setID(int32_t aSelfID)
  {
      _selfID = aSelfID;
  }

  int32_t QueryHandle::getID()
  {
    return _selfID;
  }

  void QueryHandle::setInput(abstract_msgs::SceneQuery anInput)
  {
    _query = anInput;
  }

  abstract_msgs::SceneQuery QueryHandle::getInput()
  {
    return  _query;
  }

  string QueryHandle::sha256(char* aBuf, uint64_t aBufSize)
  {
    char tmp[SHA256_DIGEST_LENGTH];
    //char* tmp;
    unsigned char hash[SHA256_DIGEST_LENGTH];
    SHA256_CTX sha256;
    SHA256_Init(&sha256);
    SHA256_Update(&sha256, aBuf, aBufSize);
    SHA256_Final(hash, &sha256);
    string newString = "";
    for(int i = 0; i < SHA256_DIGEST_LENGTH; i++)
    {
        sprintf(tmp,"%02x",hash[i]);
        newString = newString + tmp;
    }
	  return newString;
  }

  void QueryHandle::calculateHash()
  {
    _hashCode = sha256Input<abstract_msgs::SceneQuery>(_query);
  }

  string QueryHandle::getHash()
  {
    return _hashCode;
  }

  void QueryHandle::parsePeriodLayerQuery(abstract_msgs::LayerQuery aPeriodLayerQuery)
  {
	  ROS_DEBUG("[scene][QueryHandle] Parse period layerQuery <sceneName-%s, layerName-%s>!!!", _query.sceneName.c_str(),aPeriodLayerQuery.layerName.c_str());

	  if(aPeriodLayerQuery.isStop)
	  {
		  stopTimers();
      ROS_DEBUG("[scene][QueryHandle] Receive period layerQuery stop signal of <sceneName-%s, layerName-%s> and stopTimers!!!",_query.sceneName.c_str(), aPeriodLayerQuery.layerName.c_str());
		  return;
	  }

	  boost::shared_ptr<micros_scene::TimerHandle> timerHandle;
	  timerHandle.reset(new micros_scene::TimerHandle(_selfID, _query.senderID, _query.sceneName, aPeriodLayerQuery));
		if(_query.senderID == _selfID)
		{
			timerHandle->createTimerHandle(0);
			_timerVec.push_back(timerHandle);
    	ROS_DEBUG("[scene][QueryHandle] Receive local period layerQuery of <sceneName-%s, layerName-%s> and create data handle!!!", _query.sceneName.c_str(), aPeriodLayerQuery.layerName.c_str());
		}
		else
		{
			timerHandle->createTimerHandle(1);
			_timerVec.push_back(timerHandle);
      ROS_DEBUG("[scene][QueryHandle] Receive remote period layerQuery of <sceneName-%s, layerName-%s> and create data handle!!!",_query.sceneName.c_str(), aPeriodLayerQuery.layerName.c_str());
		}
    startTimers();
    ROS_DEBUG("[scene][QueryHandle] Start data process period layerQuery of <sceneName-%s, layerName-%s>!!!",_query.sceneName.c_str(), aPeriodLayerQuery.layerName.c_str());
  }

  void QueryHandle::parseHistoryLayerQuery(abstract_msgs::LayerQuery aHistoryLayerQuery)
  {
	  ROS_DEBUG("[scene][QueryHandle] Parse history layerQuery <sceneName-%s, layerName-%s>!!!",_query.sceneName.c_str(), aHistoryLayerQuery.layerName.c_str());
	  /*if(_selfID != aHistoryLayerQuery.targetRobotID && aHistoryLayerQuery.targetRobotID != -1)
	  {
		  ROS_INFO("[QueryHandle] targetRobotID is %d, but my id is %d!!!", aHistoryLayerQuery.targetRobotID, _selfID);
		  return;
	  }*/

	  if(aHistoryLayerQuery.isStop)
	  {
		  ROS_DEBUG("[scene][QueryHandle] Receive history layerQuery stop signal of <sceneName-%s, layerName-%s> and do nothing.",_query.sceneName.c_str(), aHistoryLayerQuery.layerName.c_str());
		  return;
	  }

    _historyResultLocalPub = _nh.advertise<abstract_msgs::QueryResult>(_query.sceneName, 100);
    _historyResultRemotePub = _nh.advertise<abstract_msgs::QueryResult>("/SceneQueryResult", 100);

	  abstract_msgs::Query query;
	  int32_t actorNum = aHistoryLayerQuery.actorVec.size();
	  int32_t j;
    for(j = 0;j < actorNum;j++)
	  {
		  abstract_msgs::Query query;

		  query.xMin = aHistoryLayerQuery.xMin;
		  query.xMax = aHistoryLayerQuery.xMax;
		  query.yMin = aHistoryLayerQuery.yMin;
		  query.yMax = aHistoryLayerQuery.yMax;
		  query.zMin = aHistoryLayerQuery.zMin;
		  query.zMax = aHistoryLayerQuery.zMax;

		  query.timeStart = aHistoryLayerQuery.timeStart;
		  query.timeEnd = aHistoryLayerQuery.timeEnd;

		  query.swarmID = aHistoryLayerQuery.actorVec[j].swarmID;
		  query.actorName = aHistoryLayerQuery.actorVec[j].actorName;
		  query.robotID = _selfID;
		  query.dataTypeVec = aHistoryLayerQuery.actorVec[j].dataTypeVec;
		  query.taskName = aHistoryLayerQuery.actorVec[j].taskName;

		  vector<abstract_msgs::UnifiedData> results;
		  _stor->querySqliteDB(results, query);
		  ROS_DEBUG("[scene][QueryHandle] Query actor-%s data from database for <sceneName-%s, layerName-%s> and result size is %d", query.actorName.c_str(), _query.sceneName.c_str(), aHistoryLayerQuery.layerName.c_str(), results.size());

      if(_query.senderID == _selfID)
      {
        for(int32_t i = 0;i < results.size();i++)
        {
          abstract_msgs::QueryResult msg;
          msg.senderID = _selfID;
          msg.receiverID = _query.senderID;
          msg.layerName = aHistoryLayerQuery.layerName;
          msg.sceneName = _query.sceneName;

          msg.data = results[i];
          _historyResultLocalPub.publish(msg);
        }
        ROS_DEBUG("[scene][QueryHandle] Publish historyQuery results to local !!!!!");
      }
      else
      {
        for(int32_t i = 0;i < results.size();i++)
        {
          abstract_msgs::QueryResult msg;
          msg.senderID = _selfID;
          msg.receiverID = _query.senderID;
          msg.layerName = aHistoryLayerQuery.layerName;
          msg.sceneName = _query.sceneName;

          msg.data = results[i];
          _historyResultRemotePub.publish(msg);
        }
        ROS_DEBUG("[scene][QueryHandle] Publish historyQuery results to remote !!!!!");
      }
	  }
  }

  void QueryHandle::parseEventLayerQuery(abstract_msgs::LayerQuery anEventLayerQuery, vector<abstract_msgs::EventSceneMap> &anEventList)
  {
	  ROS_DEBUG("[scene][QueryHandle] Parse event layerQuery <senderID-%d,layerName-%s,sceneName-%s>",_query.senderID, anEventLayerQuery.layerName.c_str(), _query.sceneName.c_str());

	  string layerName = anEventLayerQuery.layerName;
	  string sceneName = _query.sceneName;
	  int32_t  receiverID = _query.senderID;
    int32_t eventMsgNum = anEventLayerQuery.eventMsgVec.size();

    for(int32_t i = 0;i < eventMsgNum;i++)
    {
      abstract_msgs::EventQuery eventQuery;
      eventQuery.event = anEventLayerQuery.eventMsgVec[i];
      eventQuery.sceneName = sceneName;
      eventQuery.layerName = layerName;
      eventQuery.receiverID = receiverID;

      eventQuery.spaceConstraint = anEventLayerQuery.spaceConstraint;

      eventQuery.xMin = anEventLayerQuery.xMin;
      eventQuery.xMax = anEventLayerQuery.xMax;
      eventQuery.yMin = anEventLayerQuery.yMin;
      eventQuery.yMax = anEventLayerQuery.xMax;
      eventQuery.zMin = anEventLayerQuery.zMin;
      eventQuery.zMax = anEventLayerQuery.xMax;

      eventQuery.isStop = anEventLayerQuery.isStop;

      int32_t hasEvent = 0;
      for(int32_t j = 0;j < anEventList.size();j++)
      {
        int32_t eventFound = 0;
        if(anEventList[j].event == anEventLayerQuery.eventMsgVec[i])
        {
          hasEvent = 1;
          for(int k = 0;k < anEventList[j].eventVec.size();k++)
          {
            if(anEventList[j].eventVec[k].sceneName == sceneName&&anEventList[j].eventVec[k].receiverID == receiverID&&anEventList[j].eventVec[k].layerName == layerName)
            {
              eventFound = 1;

              if(anEventLayerQuery.isStop)
              {
                anEventList[j].eventVec[k].isStop = true;
                ROS_DEBUG("[scene][QueryHandle] Receive event layerQuery stop signal of <senderID-%d,  layerName-%s, sceneName-%s> and stop spefic event transfer to %d.",_query.senderID,  anEventLayerQuery.layerName.c_str(), _query.sceneName.c_str(), _query.senderID);
                return;
              }

              if(anEventList[j].eventVec[k].spaceConstraint == anEventLayerQuery.spaceConstraint \
                  &&anEventList[j].eventVec[k].isStop == anEventLayerQuery.isStop \
                  &&anEventList[j].eventVec[k].xMin == anEventLayerQuery.xMin && anEventList[j].eventVec[k].xMax == anEventLayerQuery.xMax \
                  &&anEventList[j].eventVec[k].yMin == anEventLayerQuery.yMin&&anEventList[j].eventVec[k].yMax == anEventLayerQuery.yMax \
                  &&anEventList[j].eventVec[k].zMin == anEventLayerQuery.zMin&&anEventList[j].eventVec[k].zMax == anEventLayerQuery.zMax)
              {
                ROS_DEBUG("[scene][QueryHandle] Already has an event of <eventMsg-%s, receiverID-%d,  layerName-%s, sceneName-%s>.", eventQuery.event.c_str(), eventQuery.receiverID, eventQuery.layerName.c_str(),eventQuery.sceneName.c_str());
                
              }
              else
              {
                anEventList[j].eventVec[k] = eventQuery;
                ROS_DEBUG("[scene][QueryHandle] Only the event of <eventMsg-%s, receiverID-%d,  layerName-%s, sceneName-%s> is same.", eventQuery.event.c_str(), eventQuery.receiverID, eventQuery.layerName.c_str(),eventQuery.sceneName.c_str());
                break;
              }
            }
          }

          if(eventFound == 0)
          {
            anEventList[j].eventVec.push_back(eventQuery);
            ROS_DEBUG("[scene][QueryHandle] The event <eventMsg-%s, receiverID-%d,  layerName-%s, sceneName-%s> is new and insert to event map lists.", eventQuery.event.c_str(), eventQuery.receiverID, eventQuery.layerName.c_str(),eventQuery.sceneName.c_str());
            break;
          }
           
        }
      }

      if(hasEvent == 0)
      {
        ROS_DEBUG("[scene][QueryHandle] The event <eventMsg-%s, receiverID-%d,  layerName-%s, sceneName-%s> don't find and it will be register.", eventQuery.event.c_str(), eventQuery.receiverID, eventQuery.layerName.c_str(),eventQuery.sceneName.c_str());
        abstract_msgs::EventSceneMap eventSceneMap;
        eventSceneMap.event = anEventLayerQuery.eventMsgVec[i];
        eventSceneMap.eventVec.push_back(eventQuery);
        anEventList.push_back(eventSceneMap);
      }
    }
  }

  void QueryHandle::startTimers()
  {
	  int32_t i;
    for(i = 0;i < _timerVec.size();i++)
		  _timerVec[i]->start(); 
  }

  void QueryHandle::stopTimers()
  {
	  int32_t i;
    for(i = 0;i < _timerVec.size();i++)
		  _timerVec[i]->stop();
  }
  
}
