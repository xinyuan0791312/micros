/*=================================================================
* Copyright (c) 2019, micROS Group, NIIDT, TAIIC, HPCL.
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without modification, are permitted
* provided that the following conditions are met:
* 1. Redistributions of source code must retain the above copyright notice, this list of conditions and
* the following disclaimer.
* 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions
* and the following disclaimer in the documentation and/or other materials provided with the
* distribution.
* 3. All advertising materials mentioning features or use of this software must display the following
* acknowledgement: This product includes software developed by the micROS Group and its contributors.
* 4. Neither the name of the Group nor the names of contributors may be used to endorse or promote
* products derived from this software without specific prior written permission.
* THIS SOFTWARE IS PROVIDED BY MICROS GROUP AND CONTRIBUTORS ''AS IS''AND
* ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
* PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE MICROS, GROUP OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,* INDIRECT, INCIDENTAL, SPECIAL,
* EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
* PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
* PROFITS; OR BUSINESS INTERRUPTION). HOWEVER CAUSED AND ON ANY THEORY
* OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
* NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
* SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
===================================================================
* Author: Jun Cai, Jie Yan.
*/
#include "pioneer_scene/PioneerScene.h"

PLUGINLIB_EXPORT_CLASS(micros_scene::PioneerScene, micros_scene::SceneBase)
/*
 * @brief parase xml
 * Analysis scene.xml and pub msg
 *
*/
namespace micros_scene
{
        using namespace tinyxml2; // NOLINT
	using std::string;
	using std::vector;

	PioneerScene::PioneerScene()
	{
		if(_nh.hasParam("robotID"))
			_nh.param("robotID", _selfID, -1);

		ros::NodeHandle privNh("~");
		if (privNh.hasParam("robot_id"))
		  privNh.getParam("robot_id",_selfID);
	
	if(_selfID==-1){
			ROS_ERROR("[scene][PioneerScene] Robot ID not set, please set robot ID on the parameter server.");
		}
		_sceneTools.reset(new micros_scene::SceneTools());
	
	}

  PioneerScene::~PioneerScene()
  {
                if(_isStorage) // NOLINT
			_sceneBag.close();
  }

	void PioneerScene::sInit()
	{
    _nh.param("bufSize", _bufSize, 20);
    _stor.reset(new micros_storage::Storage());
    _stor->initialize(_bufSize);
		ROS_INFO("[scene][PioneerScene] Storage resource init.");

    _dataPubSub.reset(new micros_scene::DataPubSub());
		ROS_INFO("[scene][PioneerScene] Network transfer handle init.");
	}

  void PioneerScene::sRead()
  {
    sceneQueryStart();
  }

  void PioneerScene::sClose()
  {
    sceneQueryStop();// NOLINT
  }


  int32_t PioneerScene::sOpen(std::string aXmlPath, bool RECORD, std::string aRecordPath)
	{
		XMLDocument doc;
		if(doc.LoadFile(aXmlPath.c_str()) != 0)
		{
			ROS_ERROR("[scene][sOpen] Load xml file failed, the file not exist.");
			doc.PrintError();
			return -1;
		}

		_sceneQuery.senderID = _selfID;

		ROS_DEBUG("[scene][sOpen] senderID : %d",  _selfID);

		XMLElement* sceneEle = doc.RootElement();

                if(sceneEle->Attribute("name") != 0)
		{
			_sceneQuery.sceneName = string(sceneEle->Attribute("name"));
			_sceneName = _sceneQuery.sceneName;
			ROS_DEBUG("[scene][sOpen] _sceneName : %s", _sceneName.c_str());
		}
		else
		{
			ROS_ERROR("[scene][sOpen] Scene XML File Parse Error: scene name not defined!");
			return abort();
		}

    _isStorage = RECORD;

		if(_isStorage)
		{
			ROS_INFO("[scene][sOpen] Scene result is set to be storaged!!!");

			_sceneBag.open(aRecordPath.c_str(), rosbag::bagmode::Write);

			_sceneQueryResultSub = _nh.subscribe(_sceneName, 100, &PioneerScene::sceneQueryResultCallback, this);
		}

    _sceneQuery.targetRobotID = sceneEle->IntAttribute("targetRobotId", -1);
		ROS_DEBUG("[scene][sOpen] targetRobotID  : %d",  _sceneQuery.targetRobotID);

		XMLElement* layerObj = sceneEle->FirstChildElement("Layer");
                while(layerObj)
		{
			abstract_msgs::LayerQuery layerQuery;
                        layerQuery.targetRobotID = _sceneQuery.targetRobotID;
			if(layerObj->Attribute("name") != 0)
			{
				layerQuery.layerName = string(layerObj->Attribute("name"));
			}
			else
			{
				ROS_ERROR("[scene][sOpen] Scene XML File Parse Error: layer name not defined!");
                               return abort();
			}

                        layerQuery.spaceConstraint = layerObj->BoolAttribute("spaceConstraint", false);
                        if(layerObj->Attribute("spaceConstraint") != 0)
			{
                                string str = string(layerObj->Attribute("spaceConstraint"));
                                if(str.compare("true") != 0&&str.compare("false") != 0)
				{
					ROS_ERROR("[scene][sOpen] Scene XML File Parse Error: spaceConstraint value error!");
					return abort();
				}
			}

			if(layerQuery.spaceConstraint)
			{
				if((layerQuery.xMin = layerObj->FloatAttribute("xMin", -1)) == -1)
                                {
					ROS_ERROR("[scene][sOpen] Scene XML File Parse Error: xMin value error or not-defined!");
					return abort();
				}
				if((layerQuery.xMax = layerObj->FloatAttribute("xMax", -1)) == -1)
				{
					ROS_ERROR("[scene][sOpen] Scene XML File Parse Error: xMax value error or not-defined!");
					return abort();
				}
				if((layerQuery.yMin = layerObj->FloatAttribute("yMin", -1)) == -1)
				{
					ROS_ERROR("[scene][sOpen] Scene XML File Parse Error: yMin value error or not-defined!");
					return abort();
				}
				if((layerQuery.yMax = layerObj->FloatAttribute("yMax", -1)) == -1)
				{
					ROS_ERROR("[scene][sOpen] Scene XML File Parse Error: yMax value error or not-defined!");
					return abort();
				}
				if((layerQuery.zMin = layerObj->FloatAttribute("zMin", -1)) == -1)
				{
					ROS_ERROR("[scene][sOpen] Scene XML File Parse Error: zMin value error or not-defined!");
					return abort();
				}
				if((layerQuery.zMax = layerObj->FloatAttribute("zMax", -1)) == -1)
				{
					ROS_ERROR("[scene][sOpen] Scene XML File Parse Error: zMax value error or not-defined!");
					return abort();
				}
			}
			else
			{
				layerQuery.xMin = -1;
				layerQuery.xMax = -1;
				layerQuery.yMin = -1;
				layerQuery.yMax = -1;
				layerQuery.zMin = -1;
				layerQuery.zMax = -1;
			}

			if((layerQuery.layerType = layerObj->IntAttribute("layerType", -1)) == -1)
			{
				ROS_ERROR("[scene][sOpen] Scene XML File Parse Error: layerType value error or not defined!");
				return abort();
			}
			else
			{
				switch(layerQuery.layerType)
				{
					case 0:
					{
						if((layerQuery.frequency = layerObj->FloatAttribute("frequency", 0)) == 0)
						{
							ROS_ERROR("[scene][sOpen] Scene XML File Parse Error: frequency value error or not defined!");
							return abort();
						}

						layerQuery.isOptimized = layerObj->BoolAttribute("isOptimized", false);
                                                if(layerObj->Attribute("isOptimized") != 0)
						{
                                                        string str = string(layerObj->Attribute("isOptimized"));
                                                        if(str.compare("true") != 0&&str.compare("false") != 0)
							{
								ROS_ERROR("[scene][sOpen] Scene XML File Parse Error: isOptimized value error!");
							  return	abort();
							}
                                        	}

						if(layerQuery.isOptimized)
						{
							ROS_DEBUG("[scene][sOpen] Layer parameter isOptimized is set true!!!");
							if((layerQuery.distanceThreshold = layerObj->FloatAttribute("distanceThreshold", -1)) == -1)
							{
								ROS_ERROR("[scene][sOpen] Scene XML File Parse Error: distanceThreshold value error or not-defined!");
								return abort();
							}
							if((layerQuery.updateThreshold = layerObj->IntAttribute("updateThreshold", -1)) == -1)
							{
								ROS_ERROR("[scene][sOpen] Scene XML File Parse Error: updateThreshold value error or not-defined!");
								return abort();
							}
						}

						XMLElement* actorObj = layerObj->FirstChildElement("Actor");
						while(actorObj)
						{
							abstract_msgs::ActorQuery actorQuery;
							if(actorObj->Attribute("name") != 0)
							{
                                                                actorQuery.actorName = string(actorObj->Attribute("name"));
							}
							else
							{
								ROS_ERROR("[scene][sOpen] Scene XML File Parse Error: Actor name not defined!");
								return abort();
							}

							XMLElement* dataType = actorObj->FirstChildElement("Data");
							while(dataType)
							{
                                                                if(dataType->Attribute("source") != 0)
								{
									string tmp = string(dataType->Attribute("source"));
									actorQuery.dataTypeVec.push_back(tmp);
									dataType = dataType->NextSiblingElement("Data");
								}
								else
								{
									ROS_ERROR("[scene][sOpen] Scene XML File Parse Error: Actor data source not defined!");
								  return	abort();
								}
							}
                                                        actorQuery.swarmID = -1;
							layerQuery.actorVec.push_back(actorQuery);
							actorObj = actorObj->NextSiblingElement("Actor");
						}
						break;
					}
					case 1:
					{
						if(layerObj->Attribute("timeStart") != 0)
						//modified by Jie Yan
						{
							time_t lIntStartTime = convertDateToTimestamp((char *)layerObj->Attribute("timeStart"));
							layerQuery.timeStart = std::to_string(lIntStartTime*1000000);
						}
						else
							layerQuery.timeStart = "0";

						if(layerObj->Attribute("timeEnd") != 0)
						//modified by Jie Yan
						{
							time_t lIntEndTime = convertDateToTimestamp((char *)layerObj->Attribute("timeEnd"));
							layerQuery.timeEnd = std::to_string(lIntEndTime*1000000);
						}
						else
							layerQuery.timeEnd = "0";

						int64_t min = atol(layerQuery.timeStart.c_str());
						int64_t max = atol(layerQuery.timeEnd.c_str());
						if((min != 0) && (max != 0) && (min > max))
						{
							ROS_ERROR("[scene][sOpen] Scene XML File Parse Error: timeStart or timeEnd value error!");
							return abort();
						}

						XMLElement* actorObj = layerObj->FirstChildElement("Actor");
						while(actorObj)
						{
							abstract_msgs::ActorQuery actorQuery;
							if(actorObj->Attribute("name") != 0)
							{
								actorQuery.actorName = string(actorObj->Attribute("name"));
							}
							else
							{
								ROS_ERROR("[scene][sOpen] Scene XML File Parse Error: Actor name not defined!");
								return abort();
							}

							XMLElement* dataType = actorObj->FirstChildElement("Data");
							while(dataType)
							{
                                                                if(dataType->Attribute("type") != 0)
								{
									string tmp = string(dataType->Attribute("type"));
									actorQuery.dataTypeVec.push_back(tmp);
									dataType = dataType->NextSiblingElement("Data");
								}
								else
								{
									ROS_ERROR("[scene][sOpen] Scene XML File Parse Error: Actor data type not defined!");
									return abort();
								}

							}
                                                        actorQuery.swarmID = -1;
							layerQuery.actorVec.push_back(actorQuery);
							actorObj = actorObj->NextSiblingElement("Actor");
						}
						break;
					}
					case 2:
					{
						XMLElement* eventObj = layerObj->FirstChildElement("Event");
						while(eventObj)
						{
                                                        if(eventObj->Attribute("type") != 0)
							{
								string tmp = string(eventObj->Attribute("type"));
								layerQuery.eventMsgVec.push_back(tmp);
								eventObj = eventObj->NextSiblingElement("Event");
							}
							else
							{
								ROS_ERROR("[scene][sOpen] Scene XML File Parse Error: event data type not defined!");
								return abort();
							}
						}
						break;
					}

					default:
						ROS_ERROR("[scene][sOpen] Scene XML File Parse Error: layerType value error or not defined!");
						
						return abort();
						break;
				}
			}

                        layerQuery.isStop = false;

			_sceneQuery.layerVec.push_back(layerQuery);
			layerObj = layerObj->NextSiblingElement("Layer");
		}

                _sceneQuery.isHeartbeat = false;
                _sceneQuery.isStop = false;

                _sceneQueryPublisher = _nh.advertise<abstract_msgs::SceneQuery>("/SceneQuery",10);

		return 0;
	}

  int32_t PioneerScene::sWrite(CommonMsg aCommonMsg, bool isEvent, bool saveEvent)
  {
	  return _sceneTools->sWrite(aCommonMsg, isEvent, saveEvent);
  }

	int32_t PioneerScene::abort()
	{
    ROS_INFO("[scene][sOpen] Please correct your scene xml!!!");
    return -1;
	}
  	void PioneerScene::sceneQueryResultCallback(const abstract_msgs::QueryResult aMsg)
	{
    _sceneBag.write(_sceneName, ros::Time::now(),aMsg);
	}
	string PioneerScene::getSceneName()
	{
		return _sceneName;
	}

	void PioneerScene::setTargetRobotId(int anID)
	{
                _sceneQuery.targetRobotID = anID;
                for(int i = 0;i < _sceneQuery.layerVec.size();i++)
		{
                        _sceneQuery.layerVec[i].targetRobotID = anID;
		}
	}

	void PioneerScene::sendSceneQuery(const ros::TimerEvent&)
	{
		_sceneQueryPublisher.publish(_sceneQuery);
	}

	void PioneerScene::sceneQueryStart()
	{
		
#ifdef PUB_HEARTBEAT
		_timer.stop();
#endif

		_sceneQueryPublisher.publish(_sceneQuery);
		sleep(1);
		_sceneQueryPublisher.publish(_sceneQuery);
    ROS_DEBUG("[scene] Send %s read message.",_sceneQuery.sceneName.c_str());
#ifdef PUB_HEARTBEAT
                _sceneQuery.isHeartbeat = true;
		_vec = _stor.serializeRos<abstract_msgs::SceneQuery>(_sceneQuery);
		_timer = _nh.createTimer(ros::Duration(60), &PioneerScene::sendSceneQuery, this);
		_timer.start();
                _sceneQuery.isHeartbeat = false;
#endif
	}

	void PioneerScene::sceneQueryStop()
	{		
#ifdef PUB_HEARTBEAT
		 _timer.stop();
#endif
                _sceneQuery.isStop = true;

		_sceneQueryPublisher.publish(_sceneQuery);
		sleep(1);
		_sceneQueryPublisher.publish(_sceneQuery);
                _sceneQuery.isStop = false;
  ROS_INFO("[scene] Send %s stop message.",_sceneQuery.sceneName.c_str());
	}

	void PioneerScene::layerQueryStop(string alayerName)
	{
                for(int i = 0;i < _sceneQuery.layerVec.size();i++)
		{
                        if(alayerName == _sceneQuery.layerVec[i].layerName)
			{
                                _sceneQuery.layerVec[i].isStop = true;
				sceneQueryStart();
				break;
			}
		}
	}

	void PioneerScene::layerQueryRestart(string alayerName)
	{
                for(int i = 0;i < _sceneQuery.layerVec.size();i++)
		{
                        if(alayerName == _sceneQuery.layerVec[i].layerName)
			{
                                _sceneQuery.layerVec[i].isStop = false;
				sceneQueryStart();
				break;
			}
		}
	}

  //modified by Yan Jie
  time_t PioneerScene::convertDateToTimestamp(char *aDateTime)
  {
    struct tm tt;
    memset(&tt,0,sizeof(tt));
    tt.tm_year = atoi(aDateTime) - 1900;
    tt.tm_mon = atoi(aDateTime + 5) - 1;
    tt.tm_mday = atoi(aDateTime + 8);
    tt.tm_hour = atoi(aDateTime + 11);
    tt.tm_min = atoi(aDateTime + 14);
    tt.tm_sec = atoi(aDateTime + 17);
    return mktime(&tt);
  }

  void PioneerScene::createSceneQuery(int32_t aSenderID, string aSceneName)
  {
      _sceneQuery.senderID = aSenderID;
      _sceneQuery.sceneName = aSceneName;
  }

  void PioneerScene::deleteScene(int32_t aSenderID, string aSceneName)
  {
      _sceneQuery.layerVec.clear();
  }

  abstract_msgs::LayerQuery PioneerScene::createPeriodLayer(string aLayerName, float frequency, bool aIsSpaceConstraint, float aXMin, float aXMax, float aYMin, float aYMax,float aZMin, float aZMax) //NOLINT
  {
    abstract_msgs::LayerQuery layerQuery;
    layerQuery.layerName = aLayerName;
    layerQuery.layerType = 0;
    layerQuery.frequency = frequency;
    layerQuery.spaceConstraint = aIsSpaceConstraint;
    if(layerQuery.spaceConstraint != true && layerQuery.spaceConstraint != false)
    {
      ROS_INFO("Scene::createPeriodLayer(string aLayerName, float frequency, bool aIsSpaceConstraint, float aXMin, float64 aXMax, float aYMin, float aYMax,float aZMin, float aZMax) function Error: spaceConstraint value error!\n");
      abort();
    }
    if(layerQuery.spaceConstraint == true)
    {
      layerQuery.xMin = aXMin;
      layerQuery.xMax = aXMax;
      layerQuery.yMin = aYMin;
      layerQuery.yMax = aYMax;
      layerQuery.zMin = aZMin;
      layerQuery.zMax = aZMax;
    }
    if(layerQuery.spaceConstraint == false)
    {
      layerQuery.xMin = -1;
      layerQuery.xMax = -1;
      layerQuery.yMin = -1;
      layerQuery.yMax = -1;
      layerQuery.zMin = -1;
      layerQuery.zMax = -1;
    }
    layerQuery.isStop = false;
    return layerQuery;
  }

  //modified by Yan Jie
  abstract_msgs::LayerQuery PioneerScene::createHistoryLayer(string aLayerName, char * aTimeStart, char * aTimeEnd, bool aIsSpaceConstraint, float aXMin, float aXMax, float aYMin, float aYMax,float aZMin, float aZMax)
  {
    abstract_msgs::LayerQuery layerQuery;
    layerQuery.layerName = aLayerName;
    layerQuery.layerType = 1;
    time_t lIntStartTime = convertDateToTimestamp(aTimeStart);
    time_t lIntEndTime = convertDateToTimestamp(aTimeEnd);
    layerQuery.timeStart = std::to_string(lIntStartTime*1000000);
    layerQuery.timeEnd = std::to_string(lIntEndTime*1000000);
    ROS_INFO("create HistoryLayer, startTime = %s \n",layerQuery.timeStart.c_str());
    ROS_INFO("create HistoryLayer, endTime = %s \n",layerQuery.timeEnd.c_str());
    layerQuery.spaceConstraint = aIsSpaceConstraint;
    if(layerQuery.spaceConstraint != true && layerQuery.spaceConstraint != false)
    {
      ROS_INFO("Scene::createHistoryLayer(string aLayerName, float frequency, bool aIsSpaceConstraint, float aXMin, float64 aXMax, float aYMin, float aYMax,float aZMin, float aZMax) function Error: spaceConstraint value error!\n");
      abort();
    }
    if(layerQuery.spaceConstraint == true)
    {
      layerQuery.xMin = aXMin;
      layerQuery.xMax = aXMax;
      layerQuery.yMin = aYMin;
      layerQuery.yMax = aYMax;
      layerQuery.zMin = aZMin;
      layerQuery.zMax = aZMax;
    }
    if(layerQuery.spaceConstraint == false)
    {
      layerQuery.xMin = -1;
      layerQuery.xMax = -1;
      layerQuery.yMin = -1;
      layerQuery.yMax = -1;
      layerQuery.zMin = -1;
      layerQuery.zMax = -1;
    }
    layerQuery.isStop = false;
    return layerQuery;
  }

  abstract_msgs::LayerQuery PioneerScene::createEventLayer(string aLayerName,  bool aIsSpaceConstraint, float aXMin, float aXMax, float aYMin, float aYMax,float aZMin, float aZMax)
  {
    abstract_msgs::LayerQuery layerQuery;
    layerQuery.layerName = aLayerName;
    layerQuery.layerType = 2;
    layerQuery.spaceConstraint = aIsSpaceConstraint;
    if(layerQuery.spaceConstraint != true && layerQuery.spaceConstraint != false)
    {
      ROS_INFO("Scene::createEventLayer(string aLayerName, bool aIsSpaceConstraint, float aXMin, float64 aXMax, float aYMin, float aYMax,float aZMin, float aZMax) function Error: spaceConstraint value error!\n");
      abort();
    }
    if(layerQuery.spaceConstraint == true)
    {
      layerQuery.xMin = aXMin;
      layerQuery.xMax = aXMax;
      layerQuery.yMin = aYMin;
      layerQuery.yMax = aYMax;
      layerQuery.zMin = aZMin;
      layerQuery.zMax = aZMax;
    }
    if(layerQuery.spaceConstraint == false)
    {
      layerQuery.xMin = -1;
      layerQuery.xMax = -1;
      layerQuery.yMin = -1;
      layerQuery.yMax = -1;
      layerQuery.zMin = -1;
      layerQuery.zMax = -1;
    }
    layerQuery.isStop = false;
    return layerQuery;
  }

  void PioneerScene::deleteLayer(string aLayerName)
  {
    vector<abstract_msgs::LayerQuery>::iterator it;
    for(it = _sceneQuery.layerVec.begin();it != _sceneQuery.layerVec.end();)
    {
      if((*it).layerName == aLayerName.c_str())
      {
        it = _sceneQuery.layerVec.erase(it);
        break;
      }
      else
        ++it;
    }
  }

  void PioneerScene::addOneActorToLayer(abstract_msgs::LayerQuery &layerQuery, string aActorName, std::vector<string> aDataTypeVec)
  {
      abstract_msgs::ActorQuery actorQuery;
      actorQuery.actorName = aActorName;
      for(int32_t i = 0; i < aDataTypeVec.size(); i++)
        actorQuery.dataTypeVec.push_back(aDataTypeVec[i]);

      layerQuery.actorVec.push_back(actorQuery);
  }

	//added by Yan Jie
	void PioneerScene::addOneEventToLayer(abstract_msgs::LayerQuery &layerQuery, string aEventName)
  {
    layerQuery.eventMsgVec.push_back(aEventName);
  }

  void PioneerScene::deleteOneActorFromLayer(string aLayerName, string aActorName)
  {
    vector<abstract_msgs::LayerQuery>::iterator it;
    for(it = _sceneQuery.layerVec.begin();it != _sceneQuery.layerVec.end();)
    {
      if((*it).layerName == aLayerName.c_str())
      {
        vector<abstract_msgs::ActorQuery>::iterator itActorQuery;
        for(itActorQuery = (*it).actorVec.begin();itActorQuery != (*it).actorVec.end();)
        {
          if((*itActorQuery).actorName == aActorName.c_str())
          {
            itActorQuery = (*it).actorVec.erase(itActorQuery);
            break;
          }
          else
            ++itActorQuery;
        }
        break;
      }
      else
        ++it;
    }
  }

	void PioneerScene::deleteOneEventFromLayer(string aLayerName, string aEventName)
	{
		vector<abstract_msgs::LayerQuery>::iterator it;
    for(it=_sceneQuery.layerVec.begin();it != _sceneQuery.layerVec.end();)
    {
      if((*it).layerName == aLayerName.c_str())
      {
        vector<string>::iterator itString;
        for(itString = (*it).eventMsgVec.begin();itString != (*it).eventMsgVec.end();)
        {
          if(*itString == aEventName.c_str())
          {
            itString = (*it).eventMsgVec.erase(itString);
            break;
          }
          else
            ++itString;
        }
        break;
      }
      else
        ++it;
    }
	}

  void PioneerScene::addOneLayerToScene(abstract_msgs::LayerQuery aLayerQuery)
  {
    _sceneQuery.layerVec.push_back(aLayerQuery);
  }

}//end of namespace micros_scene



