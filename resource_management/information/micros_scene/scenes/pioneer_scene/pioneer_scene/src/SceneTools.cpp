/*=================================================================
* Copyright (c) 2019, micROS Group, NIIDT, TAIIC, HPCL.
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without modification, are permitted
* provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice, this list of conditions and
* the following disclaimer.
* 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions
* and the following disclaimer in the documentation and/or other materials provided with the
* distribution.
* 3. All advertising materials mentioning features or use of this software must display the following
* acknowledgement: This product includes software developed by the micROS Group and its
* contributors.
* 4. Neither the name of the Group nor the names of contributors may be used to endorse or promote
* products derived from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY MICROS GROUP AND CONTRIBUTORS ''AS IS''AND
* ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
* PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE MICROS, GROUP OR
* CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
* EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
* PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
* PROFITS; OR BUSINESS INTERRUPTION). HOWEVER CAUSED AND ON ANY THEORY
* OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
* NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
* SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
===================================================================
* Author: Jun Cai.
*/
#include "pioneer_scene/SceneTools.h"

RobotPosition _robotPosition = {};
abstract_msgs::ActiveActors _activeActors = {};
abstract_msgs::SwarmActiveActors _swarmActiveActors = {};
boost::shared_ptr<micros_storage::Storage> _stor = NULL;

namespace micros_scene
{
  using std::string;
  using std::vector;

  SceneTools::SceneTools()
  {
		if(_nh.hasParam("robotID"))
			_nh.param("robotID", _selfID, -1);

		if(_nh.hasParam("/robot_id"))
		  _nh.param("/robot_id", _selfID, -1);

		_eventPublisher = _nh.advertise<abstract_msgs::UnifiedData>("/eventData", 100);
		_dataPublisher= _nh.advertise<abstract_msgs::TmpMSG>("/data", 100);
		_activeActorsSub = _nh.subscribe("/active_actors",10000, &SceneTools::activeActorsSubCallback, this);
		_swarmActiveActorsSub = _nh.subscribe("/swarm_active_actors",10000, &SceneTools::swarmActiveActorsSubCallback, this);

		string robotPositionTopic = "";
		robotPositionTopic += "/robot_"+std::to_string(_selfID)+"/truth";
		_robotPositionSub = _nh.subscribe(robotPositionTopic,10000, &SceneTools::robotPositionSubCallback, this);
    ROS_INFO("[scene] Init func to obtain robot's actor and position info.");
  }

	void SceneTools::activeActorsSubCallback(const abstract_msgs::ActiveActors::ConstPtr aMsg)
	{
		_activeActors = *aMsg;
	}

	void SceneTools::swarmActiveActorsSubCallback(const abstract_msgs::SwarmActiveActors::ConstPtr aMsg)
	{
		_swarmActiveActors = *aMsg;
	}

	void SceneTools::robotPositionSubCallback(const rosplane_msgs::State::ConstPtr aMsg)
	{
		_robotPosition.x = (*aMsg).position[0];
		_robotPosition.y = (*aMsg).position[1];
		_robotPosition.z = (*aMsg).position[2];
	}

	 int32_t SceneTools::sWrite(CommonMsg aCommonMsg, bool isEvent, bool isSave)
	 {

		 //construct a memdbStruct object with the params
		 struct micros_storage::MemDBStruct mdbs;

		 mdbs.swarmID = -1;
		 mdbs.taskName = _swarmActiveActors.swarmName;
		 mdbs.robotID = _selfID;
		 mdbs.timeStamp = std::to_string(_storageTool.getCurrentTime());
		 mdbs.dataType = aCommonMsg.dataType;

		 mdbs.positionX = _robotPosition.x;
		 mdbs.positionY = _robotPosition.y;
		 mdbs.positionZ = _robotPosition.z;

		 for(int i = 0;i < _activeActors.activeActorNames.size();i++)
		 {
		   mdbs.actorName = _activeActors.activeActorNames[i];
		   mdbs.content = aCommonMsg.contentTag;
		   mdbs.data = aCommonMsg.data;

		   abstract_msgs::TmpMSG tmpMsg;

		   if(isEvent)
		   {
			 abstract_msgs::UnifiedData ud;
			 _storageTool.memDBStruct2UnifiedData(ud,mdbs);
			 _eventPublisher.publish(ud);
			 ROS_DEBUG("[scene][sWrite] Publish an event data: type-%s, size-%d",ud.dataType.c_str(),ud.data.size());
			 }

			 if(isSave)
			 {
			   _storageTool.memDBStruct2TmpMSG(tmpMsg,mdbs);
			   _dataPublisher.publish(tmpMsg);
				  ROS_DEBUG("[scene][sWrite] Store data: type-%s, size-%d",tmpMsg.dataType.c_str(),tmpMsg.data.size());
			 } 
		 }
		 return 0;
	 }

	SceneTools::~SceneTools(){}



}//end of namaspace


