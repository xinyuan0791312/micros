#ifndef OBSERVESTURCT_H
#define OBSERVESTURCT_H

#include <string>
#include <sstream>
/**
 *the name space of the storage module.
 */
namespace micros_storage
{
  using std::string;
  /** @brief structure of table 'observe' in the disk db */
  struct ObserveStruct
  {
    int32_t id;///row id of a record
    int32_t swarmID;///the id of the swarm
    int32_t robotID;///the id of the platform
    string actorName;///role name
    string taskName;///task name
    string timeStamp;///generation time of the msg
    string dataType;///data type of the msg
    string content;/// a json format of the main content of real msg
    double positionX = 0.0;///the longitude of the position of the uav
    double positionY = 0.0;///the latitude of the postion of the uav
    double positionZ = 0.0;///the altitude of the position of the uav

    uint64_t offset;///the bytes number of the msg's serialized data offset the start position of the file @see fileName
    uint64_t size;///the bytes number of the msg's serialized data
    string fileName;///the name of the file storing the msg's serialized data
    char* data;/// the serialized data of the msg
    /**
     * @brief toString collects all the field names and the corresponding content, connects all of them to get a string.
     * @return a string contains all the variables of the struct
     */
    string toString()
    {
      return std::to_string(id) + ":" + std::to_string(swarmID) + ":" + taskName + ":" + std::to_string(robotID) + ":" + actorName + ":" + timeStamp + ":" + dataType + ":" + content + ":("+std::to_string(positionX) + "," + std::to_string(positionY) + "," + std::to_string(positionZ) + "):" + fileName + ":" + std::to_string(offset) + ":" + std::to_string(size);
    }
  };
}
#endif // OBSERVESTURCT_H
