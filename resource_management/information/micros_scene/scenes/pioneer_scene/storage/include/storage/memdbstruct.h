#ifndef MEMDBSTRUCT_H
#define MEMDBSTRUCT_H
#include <iostream>

namespace micros_storage
{
  using namespace std;
/**
   * @brief The MemDBStruct struct declares the structure of the records in the memDB and Ringbuffer
   */
  struct MemDBStruct
  {
    int32_t id;/// row id of the record
    int32_t swarmID;///the id of the swarm
    int32_t robotID;///the id of the platform
    string actorName;///role name
    string taskName;///task name
    string timeStamp;///generation time of the msg
    string dataType;///data type of the msg
    string content;/// a json format of the main content of real msg
    double positionX = 0.0;///the longitude of the position of the uav
    double positionY = 0.0;///the latitude of the postion of the uav
    double positionZ = 0.0;///the altitude of the position of the uav
    std::vector<uint8_t> data; ///the serialized data of the msg
    /**
     * @brief toString collects all the field names and the corresponding content, connects all of them to get a string.
     * @return a string contains all the variables of the struct
     */
    string toString()
    {
      return "swarmID =" + std::to_string(swarmID) + ", taskName =" + taskName + ", actorName=" + actorName + ", robotID=" + std::to_string(robotID) + ", timeStamp=" + timeStamp + ", dataType=" + dataType + " content =" + content + ", robotPosition =(" + std::to_string(positionX) + "," + std::to_string(positionY) + "," + std::to_string(positionZ) + ")";
    }
  };
} // namespace micros_storage
#endif // MEMDBSTRUCT_H
