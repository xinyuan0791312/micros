#ifndef MEMDBDAO_H
#define MEMDBDAO_H

#include <sqlite3.h>
#include <ros/ros.h>
#include <stdio.h>
#include <abstract_msgs/UnifiedData.h>
#include <storage/observesturct.h>
#include <signal.h>
#include <iostream>
#include <boost/regex.hpp>
#include <storage/tools.h>
#include <storage/memdbstruct.h>
#include <cstring>

namespace micros_storage
{
  using std::string;
  using std::vector;

  class MemDBDao
  {
    public:
      //create and open a memdb in the construct function
      MemDBDao(const char *sql_create_data = aCreateMemDBSql);
      ~MemDBDao();
      
      //when closeMemDB, we will fetch all the records in the memDB, then the user can do something on the records
      int32_t closeMemDB(std::vector<struct MemDBStruct> &rsts);
      
      //close the memdb and ignore all the records in the memdb
      int32_t closeMemDB();
      
      int32_t insertRecord(struct MemDBStruct ud);
      
      int32_t insertDB(struct MemDBStruct &replaced, struct MemDBStruct mdbs);
      
      int32_t searchDB(std::vector<struct MemDBStruct> &rsts, std::string queryCondition); //Return TmpMSG  
      int32_t searchDB(std::vector<abstract_msgs::UnifiedData> &rsts, std::string queryCondition);

      int32_t updateDB(struct MemDBStruct mdbs, int rowID);
      int32_t updateDB(abstract_msgs::UnifiedData ud, std::string content, int rowID);

      //get all records from the in-memory db
      int32_t getAllRecords(std::vector<struct MemDBStruct> &rsts);
      //get the num of records in the memdb
      int32_t getRecordNum();
      int32_t getMemDBCompacity();

    private:
      int32_t createAndOpenDbOnMemery(const char *aCreateDBSql = aCreateMemDBSql);

      constexpr static const char *aCreateMemDBSql = "CREATE TABLE IF NOT EXISTS observe(id INTEGER PRIMARY KEY AUTOINCREMENT, swarmID INTERGER NOT NULL, taskName TEXT NOT NULL, actorName TEXT NOT NULL, robotID INTEGER NOT NULL, timeStamp TEXT NOT NULL,dataType TEXT NOT NULL, content TEXT, positionX REAL NOT NULL, positionY REAL NOT NULL, positionZ REAL NOT NULL, data BLOB);";

      sqlite3 *_memdb;
      int32_t RECORD_NUM = 0;
      int32_t MAX_RECORD_NUM = 1000;
      Tools _tools;
  };
} // namespace micros_storage

#endif // MEMDBDAO_H
