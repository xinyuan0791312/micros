#ifndef TOOLS_H
#define TOOLS_H
#include <stdlib.h>
#include <stdio.h>
#include <vector>
#include <ros/ros.h>
#include <ros/message_operations.h>
#include <stdio.h>
#include <ros/serialization.h>
#include <sys/time.h>
#include <ctime>
#include <signal.h>
#include <iostream>
#include <boost/regex.hpp>
#include <abstract_msgs/UnifiedData.h>
#include <abstract_msgs/TmpMSG.h>
#include <storage/memdbstruct.h>
#include <unistd.h>
#include <dirent.h>
#include <zlib.h>

namespace micros_storage
{
  using std::string;
  using std::vector;

/**
   * @brief The Tools class is a collection of common functions
   */
  class Tools
  {
    public:
      Tools();
      ~Tools();

      /**
       * serialization function of the ros msgs
       */
      template<typename T>
      std::vector<uint8_t> serializeRos(T aMsg)
      {
        std::vector<uint8_t> vec;
        uint32_t serialSize = ros::serialization::serializationLength(aMsg);
        boost::shared_array<uint8_t> buffer(new uint8_t[serialSize]);
        ros::serialization::OStream ostream(buffer.get(), serialSize);
        ros::serialization::serialize(ostream, aMsg);
        vec.resize(serialSize);
        std::copy(buffer.get(), buffer.get() + serialSize, vec.begin());
        return vec;
      }

      /**
       * deserialization function of the ros msgs
       */
      template<typename T>
      T deserializeRos(std::vector<uint8_t> &aVec)
      {
        T t;
        uint32_t serialSize = aVec.size();
        std::vector<uint8_t> buffer(serialSize);
        std::copy(aVec.begin(), aVec.begin() + serialSize, buffer.begin());
        ros::serialization::IStream istream(buffer.data(), serialSize);
        ros::serialization::Serializer<T>::read(istream, t);
        return t;
      }

      /**
       * compression function of the ros msgs. The compressed data is pushed into @param data.
       */
      template<typename T>
      uint64_t compressMsg(T aMsg, std::vector<uint8_t> &data)
      {
        uint64_t serial_size = ros::serialization::serializationLength(aMsg);
        //std::cout<<"serial_size: "<<serial_size<<std::endl;
        boost::shared_array<uint8_t> tmpBuffer(new uint8_t[serial_size]);
        ros::serialization::OStream stream(tmpBuffer.get(), serial_size);
        ros::serialization::serialize(stream, aMsg);

        uLong comprLen = compressBound(serial_size);
        Byte *compr = new Byte[comprLen];
        uint32_t err;
        err = compress(compr, &comprLen, (const Bytef*)(tmpBuffer.get()), serial_size);
        if(err != Z_OK)
        {
          printf("compress message failed !\n");
          return -1;
        }

        char* buffer = new char[serial_size + sizeof(uint64_t)];

        char temp_l2c[sizeof(uint64_t)];
        sprintf(temp_l2c, "%ld", serial_size);
        memcpy(&buffer[0], temp_l2c, sizeof(uint64_t));
        memcpy(&buffer[sizeof(uint64_t)], compr, comprLen);

        uint64_t size = comprLen + sizeof(uint64_t);

        for(uint64_t i=0;i<size;i++)
          data.push_back(buffer[i]);

        delete []compr;
        delete []buffer;

        //std::cout<<"compress size: "<<size<<std::endl;
        return size;
      }

      /**
       * uncompression function of the ros msgs. The uncompressed data is pushed into @param uncompressedData.
       */
      int32_t uncompressMsg(std::vector<uint8_t> &compressedData, std::vector<uint8_t> &uncompressedData)
      {
      //std::cout<<"msg size: "<<compressedData.size()<<std::endl;
        char* buf = new char[compressedData.size()];
        for(uint64_t i=0;i<compressedData.size();i++)
          buf[i] = compressedData[i];

        char ll[sizeof(uint64_t)];
        memcpy(ll, buf, sizeof(uint64_t));
        uint64_t len = atol(ll);
        uLong uncomprLen = len;
        //std::cout<<"uncomprLen: "<<len<<std::endl;

        boost::shared_array<uint8_t> uncompr(new uint8_t[len]);
        uint32_t err;
        err = uncompress(uncompr.get(), &uncomprLen, (const Bytef*)&buf[sizeof(uint64_t)], (compressedData.size() - sizeof(uint64_t)));
        if(err != Z_OK)
        {
          printf("uncompress message failed !\n");
          return -1;
        }

        uncompressedData.resize(uncomprLen);
        std::copy(uncompr.get(), uncompr.get() + uncomprLen, uncompressedData.begin());

        delete []buf;

        return 0;
      }

      /**
       * transfer the ros msg @param aMsg into json format (ignore the arrayes in the @param aMsg)
       */
      template<typename T>
      std::string toJSONSchema(T aMsg)
      {
        std::string msg2json = "{ ";
        bool hasContent = 0;

        ros::message_operations::Printer<T> p;
        std::ostringstream ss;
        p.stream(ss, "", aMsg);
        std::string data = ss.str();

        std::vector<std::string> pathHeader;

        boost::regex reg("\n");
        boost::sregex_token_iterator it(data.begin(), data.end(), reg, -1);
        boost::sregex_token_iterator end;
        while(it != end)
        {
          std::string t = *it;

          if((t.find("[") == t.npos) && (t.find("]") == t.npos))
          {
            if(t.find(": ") == (t.length()-2))
            {
              t = t.substr(0, t.find(":"));

              int32_t num = 0;
              if(t.find(" ") != t.npos)
                num = (t.find_last_of(" ") + 1)/2;

              if(t.find(" ") != t.npos)
                t = t.substr(t.find_last_of(" ")+1, t.length()-1);

              if(num == pathHeader.size())
                pathHeader.push_back(t);
              else
              {
                int32_t popNum = pathHeader.size() - num;
                for(int i = 0;i<popNum;i++)
                  pathHeader.pop_back();

                pathHeader.push_back(t);
              }
            }
            else
            {
              std::string type = t.substr(0, t.find(": "));
              std::string value = t.substr(t.find(": ")+2, t.length()-1);

              int32_t num = 0;
              if(type.find(" ") != t.npos)
                num = (type.find_last_of(" ") + 1)/2;

              if(num == pathHeader.size())
              {
                if(num > 0)
                {
                  std::string path = pathHeader[0] + "/";
                  for(int32_t i = 1;i<pathHeader.size();i++)
                    path = path + pathHeader[i] + "/";
                  type = path + type.substr(type.find_last_of(" ")+1, type.length()-1);
                }
              }
              else
              {
                int32_t j = pathHeader.size() - num;
                for(int32_t i = 0;i<j;i++)
                  pathHeader.pop_back();

                if(pathHeader.size() > 0)
                {
                  std::string path = pathHeader[0] + "/";
                  for(int32_t i = 1;i<pathHeader.size();i++)
                    path = path + pathHeader[i] + "/";
                  type = path + type.substr(type.find_last_of(" ")+1, type.length()-1);
                }
              }

              if(hasContent)
                msg2json = msg2json + ", " + "\"" + type.c_str() + "\"" + ": " + "\"" + value.c_str() + "\"";
              else
                msg2json = msg2json + + "\"" + type.c_str() + "\"" + ": " + "\"" + value.c_str() + "\"";

              hasContent = 1;
            }

            it++;
          }
          else
          {
            it++;
          }
        }

        msg2json = msg2json + " }";

        return msg2json;
      }

      /**
       * @brief tmpMSG2UnifiedData converts the TmpMSG format into UnifiedData
       * @param ud an instance of the UnifiedData
       * @param aTmp an instance of the TmpMSG
       * @return whether succeed
       *
       */
      int32_t tmpMSG2UnifiedData(abstract_msgs::UnifiedData &ud, abstract_msgs::TmpMSG aTmp);

      /**
       * @brief memDBStruct2UnifiedData converts the MemDBStruct format into UnifiedData
       * @param ud a UnifiedData instance
       * @param aTmp a TmpMSG instance
       * @return whether succeed
       */
      int32_t memDBStruct2UnifiedData(abstract_msgs::UnifiedData &ud, struct MemDBStruct aTmp);

      /**
       * @brief memDBStruct2TmpMSG converts the MemDBStruct format into TmpMSG
       * @param aMsg a TmpMSG instance
       * @param aTmp a MemDBStruct instance
       * @return whether succeed
       */
      int32_t memDBStruct2TmpMSG(abstract_msgs::TmpMSG &aMsg,  struct MemDBStruct aTmp);
      /**
       * @brief getCurrentTime get current time
       * @return the usecond from the 1970-1-1 00:00:00
       */
      uint64_t getCurrentTime();
  };
}
#endif // TOOLS_H
