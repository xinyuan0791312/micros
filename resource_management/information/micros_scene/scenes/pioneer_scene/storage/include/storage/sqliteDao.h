/*=================================================================
* Copyright (c) 2016, micROS Group, NIIDT, TAIIC, HPCL. 
* All rights reserved. 
* 
* Redistribution and use in source and binary forms, with or without modification, are permitted 
* provided that the following conditions are met:  
* 
* 1. Redistributions of source code must retain the above copyright notice, this list of conditions
*  and the following disclaimer. 
* 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
*  and the following disclaimer in the documentation and/or other materials provided with the distribution.
* 3. All advertising materials mentioning features or use of this software must display the following 
*  acknowledgement: This product includes software developed by the micROS Group and its contributors. 
* 4. Neither the name of the Group nor the names of contributors may be used to endorse or promote 
*  products derived from this software without specific prior written permission. 
* 
* THIS SOFTWARE IS PROVIDED BY MICROS GROUP AND CONTRIBUTORS ''AS IS''AND ANY EXPRESS OR IMPLIED WARRANTIES,
* INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR 
* PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE MICROS, GROUP OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
* GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION). HOWEVER CAUSED AND ON ANY THEORY 
* OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
* WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 
===================================================================
* Author: Bin Lin, Jun Cai, Songchang Jin. 
*/ 

#ifndef SqliteDao_H
#define SqliteDao_H

#include <memory>
#include <vector>
#include <algorithm>
#include <map>
#include <fstream>
#include <iostream>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/time.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include "zlib.h"
#include <abstract_msgs/UnifiedData.h>
#include <sqlite3.h>
#include <storage/DBResultStruct.h>
#include <abstract_msgs/TmpMSG.h>
#include "ros/ros.h"
#include <storage/tools.h>

/**
 * the name space for the storage module
 */
namespace micros_storage
{
  using std::string;
  using std::vector;

  /**
    * @brief A MICROS DB Interface for meta data operation
    * SqliteDao is the entrance for the database operations, such as create database, insert data into database, query data from database, et al.
  */

  class SqliteDao {
  public:

    /**
      * @brief default constructor of the class SqliteDao
      * used to reset the flag _isOpen to 0, declears that the database is closed and can be open now.
    */
    SqliteDao();

    /**
      * @brief constructor of the class SqliteDao
      * use a db name to initialize the database, then open the database to get a handler _handle.
      * @param aDBName  the database name used to initialize the member variable _dbName.
    */
    SqliteDao(std::string aDBName);

    /**
      * @brief default destructor of the class SqliteDao
      * close the datbabase and destory the handler _handle.
      * /
    */
    ~SqliteDao();

    /**
      * @brief createDB create db with sql sentence @param aCreateTableSentence, and create Spatial index with @param  aCreateSpaIndexSentence
      * @param aDataBaseName is the database file name
      * @param aCreateTableSentence is a sql sentence to create a database
      * @return whether create  db successfully
      * @retval 0, succeced
      * @retval -1, error occures
    */
    int32_t createDB(std::string aDataBaseName, std::string aCreateTableSentence);

    /**
      * @brief insertDB processes metadata of the msgs from RingBuffer into sqlite3 in batches
      * @param pOffset an array maintains the offsets of serialized msg data from the start point of the file @param aFileName
      * @param pSize an array maintains the sizes of serialized msg data in the file @param aFileName
      * @param aFileName is the file name storing the serialized msg data. a file may maitains several serialized msgs
      * @param aBuffer the buffer maintains the TmpMSG msgs
      * @param aBegin the start position of the msgs in the @param aBuffer to be written into database and files
      * @param aWriteSize the size to be written into database and files
      * @param aMaxSize the capacity of the buffer maintaining the TmpMSG msgs.
      * @return whether successfully
      * @retval 0, succeed
      * @retval -1, error occures
    */
    int32_t insertDB(uint64_t* pOffset, uint64_t* pSize, std::string aFileName, boost::shared_array<abstract_msgs::TmpMSG> aBuffer,  uint32_t aBegin, uint32_t aWriteSize, uint32_t aMaxSize);

    /**
     * @brief searchDB a query entrance for searching records in the table 'observe' in the disk db (supports json extension)
     * @param aVec [out] contains the search results
     * @param aSwarmID the swarm ID
     * @param anActorName the role name of the platform plays when the msg generating
     * @param aRobotID robot id
     * @param aDataTypeVec the data types of the msg
     * @param jsonCondition the query condition in the content field in the table
     * @param anXMinToFind the min longitude value of the space to search
     * @param anXMaxToFind the max longitude value of the space to search
     * @param aYMinToFind the min latitude value of the space to search
     * @param aYMaxToFind the max latitude value of the space to search
     * @param aZMinToFind the min altitude value of the space to search
     * @param aZMaxToFind the max altitude value of the space to search
     * @param aTimeBegin the start point of the time period to search
     * @param aTimeEnd the end point of the time period to search
     * @return the size of the @param aVec
     * @retval -1, error occured
     * @retval others, @param aVec.size()
     *
     */
    int32_t searchDB(vector<struct DBSearchResultStructure> &aVec, int32_t aSwarmID, std::string anActorName, int32_t aRobotID, std::vector<std::string> aDataTypeVec, std::string jsonCondition, double anXMinToFind, double anXMaxToFind, double aYMinToFind, double aYMaxToFind, double aZMinToFind, double aZMaxToFind, std::string aTimeBegin, std::string aTimeEnd);

    /**
     * @brief searchDB a common query entrance for searching in the table 'observe' in the disk db
     * @param aVec the vetor maintains the search results
     * @param aCondition query condition
     * @return the result num
     * @retval -1, error occures
     * @retval others, @param aVec.size()
     */
    int32_t searchDB(std::vector<struct DBSearchResultStructure> &aVec, std::string aCondition);

    /**
       * @brief searchDB searches in the db with constraints, such as space/time, swarm id, robot id, actor name and data type, and reconsturct the results in the DBSearchResultStructure format into a vector
       * @param[out] aVec constains the search results from the db, consists of 3 segments: file name, offset and size
       * @param aSwarmID is the swarm id
       * @param anActorName is an actor name
       * @param aRobotID is a robot id
       * @param aDataTypeList is a list contains the data types to search
       * @param anXMinToFind declears the min value of the x-aixes range of the space to search
       * @param anXMaxToFind declears the max value of the x-aixes range of the space to search
       * @param aYMinToFind declears the min value of the y-aixes range of the space to search
       * @param aYMaxToFind declears the max value of the y-aixes range of the space to search
       * @param aZMinToFind declears the min value of the z-aixes range of the space to search
       * @param aZMaxToFind declears the max value of the z-aixes range of the space to search
       * @param aTimeBegin is the start point of the time range to search
       * @param aTimeEnd is the end point of the time range to search
       * @return the number of records in the @param aVec
       * @retval -1, error occured
       * @retval others, aVec.size()
    */
    int32_t searchDB(std::vector<struct DBSearchResultStructure> &aVec, int32_t aSwarmID, std::string anActorName, int32_t aRobotID, std::vector<std::string> aDataTypeVec, double anXMinToFind, double anXMaxToFind, double aYMinToFind, double aYMaxToFind, double aZMinToFind, double aZMaxToFind, std::string aTimeBegin, std::string aTimeEnd);

    /**
       * @brief searchDBWithMultiActors searches in the db with constraints, such as space/time, swarm id, robot id, actor names and data types, and reconsturct the results in the DBSearchResultStructure format into a vector
       * @param[out] aVec constains the search results from the db, consists of 3 segments: file name, offset and size
       * @param aSwarmID is the swarm id
       * @param anActorNameList is a list contains serveral actor names
       * @param aRobotID is a robot id
       * @param aDataTypeList is a list contains the data types to search
       * @param anXMinToFind declears the min value of the x-aixes range of the space to search
       * @param anXMaxToFind declears the max value of the x-aixes range of the space to search
       * @param aYMinToFind declears the min value of the y-aixes range of the space to search
       * @param aYMaxToFind declears the max value of the y-aixes range of the space to search
       * @param aZMinToFind declears the min value of the z-aixes range of the space to search
       * @param aZMaxToFind declears the max value of the z-aixes range of the space to search
       * @param aTimeBegin is the start point of the time range to search
       * @param aTimeEnd is the end point of the time range to search
       * @return the number of records in the @param aVec
       * @retval -1, error occured
       * @retval others, aVec.size()
    */
    int32_t searchDBWithMultiActors(std::vector<struct DBSearchResultStructure> &aVec, int32_t aSwarmID, std::vector<std::string> anActorNameList, std::string aRobotID, std::vector<std::string> aDataTypeList, double anXMinToFind, double anXMaxToFind, double aYMinToFind, double aYMaxToFind, double aZMinToFind, double aZMaxToFind, std::string aTimeBegin, std::string aTimeEnd);
   
    /**
       * @brief searchDBWithMultiActors searches in the db with constraints (json extension support), such as space/time, swarm id, robot id, actor names and data types, and reconsturct the results in the DBSearchResultStructure format into a vector
       * @param[out] aVec constains the search results from the db, consists of 3 segments: file name, offset and size
       * @param aSwarmID is the swarm id
       * @param anActorNameList is a list contains serveral actor names
       * @param aRobotID is a robot id
       * @param aDataTypeList is a list contains the data types to search
       * @param aJsonCondition the query condition in the content field in the table 'observe'
       * @param anXMinToFind declears the min value of the x-aixes range of the space to search
       * @param anXMaxToFind declears the max value of the x-aixes range of the space to search
       * @param aYMinToFind declears the min value of the y-aixes range of the space to search
       * @param aYMaxToFind declears the max value of the y-aixes range of the space to search
       * @param aZMinToFind declears the min value of the z-aixes range of the space to search
       * @param aZMaxToFind declears the max value of the z-aixes range of the space to search
       * @param aTimeBegin is the start point of the time range to search
       * @param aTimeEnd is the end point of the time range to search
       * @return the number of records in the @param aVec
       * @retval -1, error occured
       * @retval others, aVec.size()
     */
    int32_t searchDBWithMultiActors(std::vector<struct DBSearchResultStructure> &aVec, int32_t aSwarmID, std::vector<std::string> anActorNameList, std::string aRobotID, std::vector<std::string> aDataTypeList, std::string aJsonCondition, double anXMinToFind, double anXMaxToFind, double aYMinToFind, double aYMaxToFind, double aZMinToFind, double aZMaxToFind, std::string aTimeBegin, std::string aTimeEnd);

    /**
       * @brief getFirstRec get the file name corresponding to the first record in the db
       * get the first record, then fetch the "file_name" column to get the file name
       * @return the file name in the 1st record in the table 'observe'
    */
    std::string getFirstRec();

    /**
       * @brief getFileSize search the "file_name" column in the db to find the records contains the @param aFileName, then iterate the records to fetch the file_size column to get filesize, and cast them up to return.
       * @param aFileName is the file name to search, which may contains several sub files
       * @return the size of the file @param aFileName
       * @retval -1, error occures
       * @retval others, the size of the file
    */
    int64_t getFileSize(std::string aFileName);

    /**
     * @brief close closes the connection to the disk db
     * @return whether close successfully
     * @retval -1, error occures
     * @retval 0, succeed
     */
    int32_t close();

     /**
       * @brief deleteDB delete records contains the @param aFileName in the column "fileName" from db
       * @param aFileName is the file name to be searched and deleted from the db
       * @return whether delete successfully
       * @retval -1, error occures
       * @retval 0, succeed
     */
    int64_t deleteDB(std::string aFileName);

  private:
    sqlite3* _dbHandle;///the handler of the operations on the sqltite3 database.
    std::string _dbName;///the name (with the path) of the database file
    bool _isOpen;///flag to mark the database's state, declearing it is open or close
  };
}

#endif
