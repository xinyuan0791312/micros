#ifndef RINGBUFFER_H
#define RINGBUFFER_H
#include <stdio.h>
#include <abstract_msgs/TmpMSG.h>
#include <storage/sqliteDao.h>
#include <ros/ros.h>
#include <semaphore.h>
#include <storage/atomic.h>
#include <boost/thread/condition.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/thread/thread.hpp>
#include <boost/thread/shared_mutex.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <unistd.h>
#include <stdlib.h>
#include <dirent.h>
#include <storage/tools.h>
#include <memory>
#include <vector>
#include <algorithm>
#include <map>
#include <fstream>
#include <iostream>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/time.h>
#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>
#include <aio.h>
#include <zlib.h>
/**
 *the name space of the storage module
 */
namespace micros_storage
{
  using std::string;

  /** @brief async write buffer */
  class RingBuffer
  {
    public:
    /**
       * @brief RingBuffer an empty constructor
       */
      RingBuffer();

      /**
         * @brief ~RingBuffer an empty destructor
         */
      ~RingBuffer();

      /**
       * @brief initialize initialize the  @var _ringBuffer with the params @param aBufferSize, @param aSerializeDataPath and @param aDatabaseName
       * @param aBufferSize the capacity of the RingBuffer instance
       * @param aSerielizeDataPath the folder path storing the serialized data on the disk
       * @param aDatabaseName the database name containing the talbe 'observe' which storing the metadata of the msgs
       */
      void initialize(uint32_t aBufferSize, std::string aSerielizeDataPath, std::string aDatabaseName);

      /** @brief flush the  @var _ringBuffer, then delete  @var _ringBuffer */
      void destroy();

      /** @brief write data when RingBuffer size reach the threshold size or timeout */
      void writeHandle();

      /**
       * @brief dataWrite writes the msg @param aMsg to the  @var _ringBuffer
       * @param aMsg is a TmpMSG instance to be written into  @var _ringBuffer
       */
      void dataWrite(const abstract_msgs::TmpMSG aMsg);

      /** @brief  */
      /**
       * @brief flush writes @param aWriteSize msgs of  @var _ringBuffer from the @param aBegin into file and database
       * @param aBegin
       * @param aWriteSize
       * @return whether write succesed
       *     @retval 0, succesed
       *     @retval -1, failed or error occured
       */
      int32_t flush(uint32_t aBegin, uint32_t aWriteSize);

      /**
       * @brief getStorageUsed gets total file size used in storage engine
       * @return @var _currStorageUsed
       */
      uint64_t getStorageUsed();

      /**
       * @brief checkWriteReady check whether the  @var _ringBuffer size reach the write threshold
       * @return the status whether ready for writing
       * @retval true, is ready
       * @retval false, not ready
       */
      bool checkWriteReady();

      /** @brief check whether write_handle is flushing data
       *  @return the status of the write_handle
       *  @retval true, is flusing data
       *  @retval false, is not flushing data
       */
      bool checkIsWriting();

      /**
       * @brief checkBufferIsFull checks whether @var _ringBuffer is full that the writing msg will be ignored
       * @return whether the buffer is full or not
       * @retval true, the buffer is full
       * @retval false, the buffer is not full yet
       */
      bool checkBufferIsFull();

      /**
       * @brief getBufSize gets the capacity of the  @var _ringBuffer.
       * @return the max number of TmpMSG type msgs can be stored in the ringbuffer instance.
       */
      int32_t getBufSize();

      /**
       * @brief getData get the @variable anIndex -th msg in the @var _ringBuffer
       * @param anIndex the index of the msg to get
       * @return the @param anIndex-th msg in the @var _ringBuffer
       */
      abstract_msgs::TmpMSG getData(uint32_t anIndex);

      /**
       * @brief getDataSize gets total binary file size in @param aDir
       * @param aDir the directory storing the serialized msgs
       * @return the total file size in the @param aDir
       */
      uint64_t getDataSize(const char* aDir);

    private:

      uint32_t _ringBufferCapacity;///capacity of the @var _ringBuffer
      uint32_t _writeThreshold; ///threshold for writting. Msg number reaches the threshold will triggers a write operation

      string _serielizeDataDir;///the directory storing the serialized msg data
      string _databaseName;///the database name storing the metadata

      boost::shared_array<abstract_msgs::TmpMSG> _ringBuffer;///a buffer caching the TmpMSG msgs

      boost::mutex _writeLock;///lock for write operation

      atomic_t _cursorH; ///cursor head of ringbuffer
      atomic_t _cursorT; ///cursor tail of ringbuffer
      atomic_t _isWriting; ///flag of is writing to file

      boost::shared_ptr<boost::thread> _writeThread; /// thread handle, callback is write_handle()
      boost::condition_variable _cond;///a condition variable
      boost::mutex _mutex;
      int32_t _threadRun; ///weather write_thread_ should run, when destroy the thread, set thread_run_ to 0

      uint64_t _currStorageUsed;///disk space used by the storage module in bytes

      boost::shared_ptr<SqliteDao> _dbDao;/// the handler of the diskDB operation

      Tools _tools;/// the instance of the Tools for some common functions.
  };
}
#endif // RINGBUFFER_H
