#ifndef STORAGE_H
#define STORAGE_H

#include <stdio.h>
#include <storage/RingBuffer.h>
#include <unistd.h>
#include <stdlib.h>
#include <dirent.h>
#include <ctime>
#include <abstract_msgs/Query.h>
#include <storage/sqliteDao.h>
#include <storage/memdbdao.h>
#include <storage/tools.h>
#include <storage/memdbstruct.h>
#include <abstract_msgs/TmpMSG.h>

/**
 *name space of the storage module
 */
namespace micros_storage
{
  using std::string;
  using std::vector;
/**
   * @brief The Storage class is the entrance of data processing of the scence system.
   */
  class Storage
  {
    public:
    /**
       * @brief Storage an empty constructor
       */
      Storage();
      //Storage(int32_t aBufferSize, std::string aSerializeDataPath, std::string aDatabasePath);
      /**
         * @brief ~Storage an empty destructor
         */
      ~Storage();

      //void initialize(int32_t aBufferSize, string aSerielizeDataPath, string aDatabasePath);
      /**
       * @brief initialize initial the database, ringbuffer
       * @param aBufferSize _bufferSize
       */
      void initialize(int32_t aBufferSize);

      /**
       * @brief dataSubCallback a callback function of the @see initialize()
       * @param anInput a TmpMSG msg to be procceed in the callback
       */
      void dataSubCallback(const abstract_msgs::TmpMSG anInput);

      /**
      * @brief gcHandle is the capacity gc handle
      */
      void gcHandle();
      
      /**
       * create mem db
       */
      void createMemDB();

      /**
       * @brief insertMemDB insert a MemDBSturct instance @param aMdbs into memDB
       * @param aReplaced the instance to insert into memDB
       * @param aMdbs the instance replaced by the @param aMdbs
       * @return the replaced record in MemDBStruct format, @param aReplaced
       */
      int insertMemDB(struct MemDBStruct &aReplaced, struct MemDBStruct aMdbs);

      /**
       * @brief setQueryEnv sets the read environment for data read from file system and database
       * @param aSerielizeDataPath is the directory path stores the serialized data on the file system
       * @param aDatabasePath is the database path on the file system
       */
      void setQueryEnv(string aSerielizeDataPath, string aDatabasePath);
      /**
       * @brief setWriteEnv sets the write environment
       */
      void setWriteEnv();

      /**
      * @brief destroy the storage uint32_terface, delete RingBuffer
      */
      void destroy();

      /**
       * @brief saveData2RingBuffer save a TmpMSG into the @var _buffer
       * @param aMsg an instance of TmpMSG
       * @return 0
       */
      int32_t saveData2RingBuffer(abstract_msgs::TmpMSG aMsg);

      /**
      * @brief readFile read binary data according to @param aFileName, @param anOffset and @param aSize, then uncompress and deserialize the binary data to a msg in Data.msg format
      * @param aResult is the result of the read operation
      * @param aFileName is the file name to be read
      * @param anOffset is the start postion of the read operation in the file
      * @param aSize is the number of the bytes to be read
      * @return 0
      */
      int32_t readFile(abstract_msgs::UnifiedData &aResult, std::string aFileName, uint64_t anOffset, uint64_t aSize);

      /**
      * @brief delete metadata in database and corresponding bianry data in file
      * return 0
      */
      int32_t deleteData();

      /** @brief entrance of the query function for using topic mechanism called by the ground station**/
      /**
      * @brief queryDB search in the db with the constraint in @param aQuery, and return the results in the @param aResults
      * @param[out] aResults is used to store the return results
      * @param aQuery is the query condition
      * @return 0
      */
      int32_t querySqliteDB(std::vector<abstract_msgs::UnifiedData> &aResults, abstract_msgs::Query aQuery);

      /**
       * @brief queryMemDB query the memDB with the condition @param aCondition.
       * @param aResults the results
       * @param aCondition query condition
       * @return
       * @retval -1, error occurs
       * @retval others, aResults.size()
       */
      int32_t queryMemDB(std::vector<abstract_msgs::UnifiedData> &aResults, std::string aCondition);

      /** @todo
       * @brief query search in both memdb and disk db to get the records
       * @param aRsltVec the results
       * @param aQueryCondition query condition
       * @return
       * @retval -1, error occurs
       * @retval others, aRsltVec.size()
       */
      int32_t query(std::vector<abstract_msgs::UnifiedData> &aRsltVec, std::string aQueryCondition);


      /** @todo
      * @brief query will search in sqlite and fastdb to get the msgs
      * @param aResults
      * @param aQuery
      * @return
      */
      int32_t query(vector<abstract_msgs::UnifiedData> &aResults, abstract_msgs::Query aQuery);

      /**
       * @brief getAllMemDBRecords gets all the records in the memdb
       * @param aResults the results
       * @return
       * @retval -1, error occurs
       * @retval others, aResults.size()
       */
      int32_t getAllMemDBRecords(vector<abstract_msgs::UnifiedData> &aResults);

    private:

      ros::NodeHandle _nodeHandle;/// nodehandle

      std::string _serializeDataPath;/// file path to serialized data storage in the disk
      std::string _databaseName;/// database name
      std::string _databasePath;/// database path

      uint64_t _totalSize;
      int32_t _gcRun;

      /**
      * @brief _buffer is a ringbuffer
      */
      boost::shared_ptr<micros_storage::RingBuffer> _buffer;///instance of the RingBuffer @see RingBuffer

     /**
     * @brief _bufferSize is the size of the ringbuffer @variable _buffer
     */
     uint32_t _bufferSize;

     boost::shared_ptr<boost::thread> _gcThread;
     boost::condition_variable _gcCondition;
     boost::mutex _gcMutex;

     /**
     * @brief _publisher is a publisher used to publish data in ROS framework
     */
     ros::Publisher _publisher;

     /**
     * @brief _dataSub is a subscriber used to get data in ROS framework
     */
     ros::Subscriber _dataSub;

     //ros::Publisher _eventPublisher;

     /**
     * @brief _dbDao is an instance to accece the sqlite db
     */
     boost::shared_ptr<SqliteDao> _dbDao;

     Tools _tools;

     /**
      * @brief _memdb an instance to access the mem db
      */
     boost::shared_ptr<MemDBDao> _memdb;

     /**
      * @brief _createMemDBTableSentence the sql sentence to create the table in the mem db
      */
     string _createMemDBTableSentence  = "CREATE TABLE IF NOT EXISTS observe(id INTEGER PRIMARY KEY AUTOINCREMENT, swarmID INTERGER NOT NULL, taskName TEXT NOT NULL, actorName TEXT NOT NULL, robotID INTEGER NOT NULL, timeStamp TEXT NOT NULL, dataType TEXT NOT NULL, content TEXT, positionX REAL NOT NULL, positionY REAL NOT NULL, positionZ REAL NOT NULL, data BLOB);";
     
     /**
      * @brief _createSqlite3DBTableSentence the sql sentence to create the table in the disk db
      */
     std::string _createSqlite3DBTableSentence = "CREATE TABLE IF NOT EXISTS observe(id INTEGER PRIMARY KEY AUTOINCREMENT, swarmID INTERGER NOT NULL, taskName TEXT NOT NULL, actorName TEXT NOT NULL, robotID INTEGER NOT NULL, timeStamp TEXT NOT NULL, dataType TEXT NOT NULL, content TEXT, offset INTERGER NOT NULL, size INTERGER NOT NULL, fileName TEXT NOT NULL, positionX REAL NOT NULL, positionY REAL NOT NULL, positionZ REAL NOT NULL)";
  };
}
#endif // STORAGE_H
