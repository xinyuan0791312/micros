/*=================================================================
* Copyright (c) 2016, micROS Group, NIIDT, TAIIC, HPCL. 
* All rights reserved. 
* 
* Redistribution and use in source and binary forms, with or without modification, are permitted 
* provided that the following conditions are met:  
* 
* 1. Redistributions of source code must retain the above copyright notice, this list of conditions
*   and the following disclaimer. 
* 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
*   and the following disclaimer in the documentation and/or other materials provided with the distribution.
* 3. All advertising materials mentioning features or use of this software must display the following 
*   acknowledgement: This product includes software developed by the micROS Group and its contributors. 
* 4. Neither the name of the Group nor the names of contributors may be used to endorse or promote 
*   products derived from this software without specific prior written permission. 
* 
* THIS SOFTWARE IS PROVIDED BY MICROS GROUP AND CONTRIBUTORS ''AS IS''AND ANY EXPRESS OR IMPLIED WARRANTIES,
* INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR 
* PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE MICROS, GROUP OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
* GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION). HOWEVER CAUSED AND ON ANY THEORY 
* OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
* WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 
===================================================================
* Author: Bin Lin, Jun Cai, Songchang Jin. 
*/ 

#ifndef DBRESULTSTRUCT_H
#define DBRESULTSTRUCT_H
#include <string>
#include <sstream>

/**
 *the name space of the storage module
 */
namespace micros_storage
{
	using std::string;
	/** @brief structure of db search result */
	struct DBSearchResultStructure
	{
    uint64_t offset;///the number of bytes from the start position of the file @see fileName
    uint64_t size;/// the number of bytes taken up by current msg in the file @see fileName
    string fileName;///the file name storing current msg
    /**
     * @brief toString transfers all the variables to string and collects them in a string
     * @return a string contains all the variables sperated by semicolons.
     */
		string toString()
		{
			return fileName + ":" + std::to_string(offset) + ":" + std::to_string(size);
		}
	};
}
#endif //DBRESULTSTRUCT_H

