/*=================================================================
* Copyright (c) 2019, micROS Group, NIIDT, TAIIC, HPCL.
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without modification, are permitted
* provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice, this list of conditions and
* the following disclaimer.
* 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions
* and the following disclaimer in the documentation and/or other materials provided with the
* distribution.
* 3. All advertising materials mentioning features or use of this software must display the following
* acknowledgement: This product includes software developed by the micROS Group and its
* contributors.
* 4. Neither the name of the Group nor the names of contributors may be used to endorse or promote
* products derived from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY MICROS GROUP AND CONTRIBUTORS ''AS IS''AND
* ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
* PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE MICROS, GROUP OR
* CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
* EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
* PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
* PROFITS; OR BUSINESS INTERRUPTION). HOWEVER CAUSED AND ON ANY THEORY
* OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
* NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
* SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
===================================================================
* Author: SongChang Jin.
*/
#include <storage/storage.h>
namespace micros_storage
{
  using std::string;

  Storage::Storage(){}
  Storage::~Storage(){}

  void Storage::initialize(int32_t aBufferSize)
  {
    if(aBufferSize < 10)
    {
      ROS_ERROR("[scene][storge] RingBuffer size must be larger than 10.");
      return;
    }

    _bufferSize = aBufferSize;

    std::string home = getenv("HOME");
    _serializeDataPath = home + "/file/";
    _databasePath=home + "/database/";
    _databaseName = _databasePath+"MicrosDB"; // MicrosDB can be modified by user

    _totalSize = 21474836480;//20G

    // mkdir of file and database path
    mode_t mode = 0755;
    mkdir(_databasePath.c_str(), mode);
    mkdir(_serializeDataPath.c_str(), mode);
    ROS_INFO("[scene][storge] Create storge data path.");

    // if database is not exist, then createDB
    if(access(_databaseName.c_str(), F_OK) == -1)
    {
      ROS_INFO("[scene][storge] StorageInterface initializing, since database is not exist, using DBDao to create DB.");
      SqliteDao dbDaoTmp;
      dbDaoTmp.createDB(_databaseName, _createSqlite3DBTableSentence);
    }

    _buffer.reset(new RingBuffer());
    _buffer->initialize(_bufferSize, _serializeDataPath, _databaseName);

    _dbDao.reset(new SqliteDao(_databaseName));

    _gcRun = 1;

    _dataSub = _nodeHandle.subscribe("/data", 100, &Storage::dataSubCallback, this);
    ROS_INFO("[scene][storge] Storge engine initialize finished.");
  }

  void Storage::gcHandle()
  {
    while(_gcRun == 1)
    {
      // wait util write threshold is reached or timeout
      boost::mutex::scoped_lock lock(_gcMutex);
      _gcCondition.timed_wait(lock, boost::get_system_time() + boost::posix_time::seconds(60));

      if(_gcRun == 0)
        return;
      // flush is not performing, or else wait
      if((_buffer -> getStorageUsed()) > (_totalSize * 0.9))
        deleteData();
    }
  }

  void Storage::createMemDB()
  {
    _memdb.reset(new MemDBDao(_createMemDBTableSentence.c_str()));
  }

  int Storage::insertMemDB(struct MemDBStruct &aReplaced, struct MemDBStruct aMdbs)
  {
    return _memdb->insertDB(aReplaced,aMdbs);
  }

  void Storage::destroy()
  {
    _memdb->closeMemDB();
    _buffer->destroy();
  }

  void Storage::dataSubCallback(const abstract_msgs::TmpMSG anInput)
    {
  	  saveData2RingBuffer(anInput);
    }

  int32_t Storage::saveData2RingBuffer(const abstract_msgs::TmpMSG aMsg)
  {
    _buffer->dataWrite(aMsg);
    return 0;
  }

  int32_t Storage::readFile(abstract_msgs::UnifiedData &aResult, string aFileName, uint64_t anOffset, uint64_t aSize)
  {
    std::ifstream rs(aFileName.c_str(), std::ios::binary);
    rs.seekg(anOffset, std::ios_base::beg);
    unsigned char* ubuf = new unsigned char[aSize];
    rs.read((char*)(&ubuf[0]), aSize);

    ros::serialization::IStream stream(ubuf, aSize);
    ros::serialization::Serializer<abstract_msgs::UnifiedData>::read(stream, aResult);

    delete []ubuf;

    rs.close();

    return 0;
  }

  int32_t Storage::deleteData()
  {
    int64_t delSize = _totalSize * 0.05;
    int64_t size = 0;

    SqliteDao dbDaoTmp(_databaseName);

    while(size < delSize)
    {
      string name = dbDaoTmp.getFirstRec();
      size += dbDaoTmp.getFileSize(name);
      dbDaoTmp.deleteDB(name);
      std::remove(name.c_str());
    }

   return 0;
  }

  int32_t Storage::querySqliteDB(vector<abstract_msgs::UnifiedData> &aResults, abstract_msgs::Query aQuery)
  {
    vector<struct DBSearchResultStructure> vec;

    _dbDao->searchDB(vec, aQuery.swarmID, aQuery.actorName, aQuery.robotID, aQuery.dataTypeVec, aQuery.xMin, aQuery.xMax, aQuery.yMin, aQuery.yMax, aQuery.zMin, aQuery.zMax, aQuery.timeStart, aQuery.timeEnd);

    int32_t count = vec.size();
    ROS_DEBUG("[scene][storge] QuerySqliteDB result size: %d.",count);
    for(int32_t i=0;i<count;i++)
    {
      abstract_msgs::UnifiedData result;
	  readFile(result, vec[i].fileName, vec[i].offset, vec[i].size);
      aResults.push_back(result);
    }

    return 0;
  }

  int32_t Storage::queryMemDB(vector<abstract_msgs::UnifiedData> &aResults, string aCondition)
  {
   return _memdb->searchDB(aResults,aCondition);
  }

  int32_t Storage::query(vector<abstract_msgs::UnifiedData> &aResults, abstract_msgs::Query aQuery)
  {
    return 0;
  }

  int32_t Storage::query(std::vector<abstract_msgs::UnifiedData> &aRsltVec, std::string aQueryCondition)
  {
    int num = 0;
    std::vector<struct DBSearchResultStructure> aVec;
    abstract_msgs::UnifiedData ud;

    num = _dbDao->searchDB(aVec, aQueryCondition);
    //read file to get the UnifiedData object for sqlite3db
    ROS_DEBUG("[scene][storge] QuerySqliteDB result size: %d.",num);
    if(num > 0)
    {
      for(int i=0;i<num;i++)
      {
        readFile(ud,aVec[i].fileName,aVec[i].offset,aVec[i].size);
        aRsltVec.push_back(ud);
      }
    }
    return num;
  }

  int32_t Storage::getAllMemDBRecords(vector<abstract_msgs::UnifiedData> &aResults)
  {
    std::vector<MemDBStruct> rsts;
    int num = _memdb->getAllRecords(rsts);
    for(int i=0;i<num;i++)
    {
      abstract_msgs::UnifiedData ud;
      _tools.memDBStruct2UnifiedData(ud,rsts[i]);
      aResults.push_back(ud);
    }
    return num;
  }

}
