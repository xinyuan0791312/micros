/*=================================================================
* Copyright (c) 2019, micROS Group, NIIDT, TAIIC, HPCL.
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without modification, are permitted
* provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice, this list of conditions and
* the following disclaimer.
* 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions
* and the following disclaimer in the documentation and/or other materials provided with the
* distribution.
* 3. All advertising materials mentioning features or use of this software must display the following
* acknowledgement: This product includes software developed by the micROS Group and its
* contributors.
* 4. Neither the name of the Group nor the names of contributors may be used to endorse or promote
* products derived from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY MICROS GROUP AND CONTRIBUTORS ''AS IS''AND
* ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
* PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE MICROS, GROUP OR
2
* CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
* EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
* PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
* PROFITS; OR BUSINESS INTERRUPTION). HOWEVER CAUSED AND ON ANY THEORY
* OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
* NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
* SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
===================================================================
* Author: SongChang Jin.
*/
#include <storage/tools.h>

namespace micros_storage
{
  using std::string;
  using std::vector;

  Tools::Tools(){}
  Tools::~Tools(){}

  int32_t Tools::tmpMSG2UnifiedData(abstract_msgs::UnifiedData &ud,  abstract_msgs::TmpMSG aTmp)
  {
    ud.actorName = aTmp.actorName;
    ud.data = aTmp.data;
    ud.dataType = aTmp.dataType;
    ud.robotID = aTmp.robotID;

    ud.positionX = aTmp.positionX;
    ud.positionY = aTmp.positionY;
    ud.positionZ= aTmp.positionZ;

    ud.swarmID = aTmp.swarmID;
    ud.taskName = aTmp.taskName;
    ud.timeStamp = aTmp.timeStamp;

    return 0;
  }

  int32_t Tools::memDBStruct2UnifiedData(abstract_msgs::UnifiedData &ud,  struct MemDBStruct aTmp)
  {
    ud.swarmID = aTmp.swarmID;
    ud.taskName = aTmp.taskName;
    ud.actorName = aTmp.actorName;
    ud.robotID = aTmp.robotID;
    ud.timeStamp = aTmp.timeStamp;
    ud.dataType = aTmp.dataType;

    ud.positionX = aTmp.positionX;
    ud.positionY = aTmp.positionY;
    ud.positionZ = aTmp.positionZ;

    for(int i=0;i<aTmp.data.size();i++)
      ud.data.push_back(aTmp.data[i]);

    return 0;
  }

  int32_t Tools::memDBStruct2TmpMSG( abstract_msgs::TmpMSG &ud,  struct MemDBStruct aTmp)
  {
    ud.swarmID = aTmp.swarmID;
    ud.taskName = aTmp.taskName;
    ud.actorName = aTmp.actorName;
    ud.robotID = aTmp.robotID;
    ud.timeStamp = aTmp.timeStamp;
    ud.dataType = aTmp.dataType;
    ud.content = aTmp.content;

    ud.positionX = aTmp.positionX;
    ud.positionY = aTmp.positionY;
    ud.positionZ = aTmp.positionZ;

    for(int i=0;i<aTmp.data.size();i++)
      ud.data.push_back(aTmp.data[i]);

    return 0;
  }

  uint64_t Tools::getCurrentTime()
  {
    struct timeval tv;
    gettimeofday(&tv,NULL);
    return tv.tv_sec*1000000 + tv.tv_usec;
  }
}
