/*=================================================================
* Copyright (c) 2019, micROS Group, NIIDT, TAIIC, HPCL.
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without modification, are permitted
* provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice, this list of conditions and
* the following disclaimer.
* 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions
* and the following disclaimer in the documentation and/or other materials provided with the
* distribution.
* 3. All advertising materials mentioning features or use of this software must display the following
* acknowledgement: This product includes software developed by the micROS Group and its
* contributors.
* 4. Neither the name of the Group nor the names of contributors may be used to endorse or promote
* products derived from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY MICROS GROUP AND CONTRIBUTORS ''AS IS''AND
* ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
* PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE MICROS, GROUP 
* CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
* EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
* PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
* PROFITS; OR BUSINESS INTERRUPTION). HOWEVER CAUSED AND ON ANY THEORY
* OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
* NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
* SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
===================================================================
* Author: SongChang Jin.
*/
#include <storage/sqliteDao.h>

namespace micros_storage
{
  using std::string;
  using std::vector;

  SqliteDao::SqliteDao()
  {
    _isOpen = 0;
    ROS_INFO("[scene][storage] SqliteDB initialized without create DB.");
  }

  SqliteDao::SqliteDao(string aDBName)
  {
    int32_t retCode;
    char* errMsg = NULL;

    _dbName = aDBName;

    retCode = sqlite3_open_v2(_dbName.c_str(), &_dbHandle, SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE | SQLITE_OPEN_NOMUTEX, NULL);
    if(retCode != SQLITE_OK)
    {
      ROS_ERROR("[scene][storage] Can't open '%s': %s\n", _dbName.c_str(), sqlite3_errmsg(_dbHandle));
      exit(1);
    }
    else
      _isOpen = 1;
    ROS_INFO("[scene][storage] Open the sqlite3 DB successfully!");

    retCode = sqlite3_enable_load_extension(_dbHandle,true);
    if(retCode == SQLITE_OK)
    {
      ROS_INFO("[scene][storage] Enable the sqlite3 json extension successfully!");
    }
    else{
      ROS_ERROR("[scene][storage] Enable the sqlite3 json extension failed!");
      exit(1);
    }


   retCode = sqlite3_load_extension(_dbHandle,"/usr/local/lib/libjson.so","sqlite3_json_init",&errMsg);
   if(retCode == SQLITE_OK)
   {
     ROS_INFO("[scene][storage] Load the sqlite3 json extension successfully!");
   }
   else
   {
     ROS_ERROR("[scene][storage] Load the sqlite3 json extension failed");
     exit(-11);
   }

    sqlite3_free(errMsg);
  }


  SqliteDao::~SqliteDao()
  {
    char* errMsg = NULL;
    if(_isOpen == 1)
    {
      int32_t retCode = sqlite3_close_v2(_dbHandle);
      if(retCode != SQLITE_OK)
      {
        ROS_ERROR ("[scene][storage] Close DB error: %s.", sqlite3_errmsg(_dbHandle));
      } 
    }
    sqlite3_free(errMsg);
  }

  int32_t SqliteDao::createDB(string aDataBaseName, string aCreateTableSentence)
  {
    ROS_INFO("[scene][storage] Creating DB, please wait some minutes.");
    int32_t retCode;
    char sql[512];
    char* errMsg = NULL;
    sqlite3* handle;

    retCode = sqlite3_open_v2(aDataBaseName.c_str(), &handle, SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE, NULL);
    if(retCode != SQLITE_OK)
    {
      ROS_ERROR("[scene][storage] Can't open '%s': %s", aDataBaseName.c_str(), sqlite3_errmsg(handle));
      sqlite3_close_v2(handle);
      return -1;
    }
    _isOpen = 1;

   // create ordinary part of table
    strcpy(sql, aCreateTableSentence.c_str());
    retCode = sqlite3_exec(handle, sql, NULL, NULL, &errMsg);
    if(retCode != SQLITE_OK)
    {
      ROS_ERROR("[scene][storage] CREATE TABLE  error: %s", errMsg);
      goto abort;
    }

    retCode = sqlite3_close_v2(handle);
    if(retCode != SQLITE_OK)
      ROS_ERROR("[scene][storage] Close DB error: %s", sqlite3_errmsg(handle));
    else
      _isOpen = 0;

    return 0;

  abort:
    sqlite3_free(errMsg);

    sqlite3_close_v2(handle);
    if(retCode != SQLITE_OK)
      ROS_ERROR("[scene][storage] Close DB error: %s", sqlite3_errmsg(handle));
    else
      _isOpen = 0;

    return -1;
  }

  int32_t SqliteDao::insertDB(uint64_t* pOffset, uint64_t* pSize, string aFileName, boost::shared_array<abstract_msgs::TmpMSG> aBuffer, uint32_t aBegin, uint32_t aWriteSize, uint32_t aMaxSize)
  {
    //所有的函数都要增加判断语句，判断数据库是否打开
    sqlite3_stmt *stmt;
    char sql[512];
    char *errMsg = NULL;
    unsigned char* blob;
    int32_t blobSize;
    int32_t retCode;
    // prepare data, insert batch data to database

    strcpy(sql, "BEGIN");
    retCode = sqlite3_exec(_dbHandle, sql, NULL, NULL, &errMsg);
    if (retCode != SQLITE_OK) 
    {
      ROS_ERROR("[scene][storage] BEGIN error: %s", errMsg);
      goto abort;
    }

    strcpy(sql,"INSERT INTO observe (swarmID, taskName, actorName, robotID, timeStamp, dataType, content, offset, size, fileName, positionX, positionY, positionZ) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");

    retCode = sqlite3_prepare_v2(_dbHandle, sql, strlen(sql), &stmt, NULL);
    if (retCode != SQLITE_OK)
    {
      ROS_ERROR("[scene][storage] INSERT SQL error: %s", sqlite3_errmsg(_dbHandle));
      goto abort;
    }

    uint32_t h;
    uint32_t j;
    j = 0;
    for (h = 0; h < aWriteSize; h++)
    {
      uint32_t curr = (aBegin + h) % aMaxSize;
      sqlite3_reset(stmt);
      sqlite3_clear_bindings(stmt);
 
      sqlite3_bind_int(stmt, 1, aBuffer[curr].swarmID);
      sqlite3_bind_text(stmt, 2, aBuffer[curr].taskName.c_str(), std::strlen(aBuffer[curr].taskName.c_str()), SQLITE_TRANSIENT);
      sqlite3_bind_text(stmt, 3, aBuffer[curr].actorName.c_str(), std::strlen(aBuffer[curr].actorName.c_str()), SQLITE_TRANSIENT);
      sqlite3_bind_int(stmt, 4, aBuffer[curr].robotID);
      sqlite3_bind_text(stmt, 5, aBuffer[curr].timeStamp.c_str(), std::strlen(aBuffer[curr].timeStamp.c_str()), SQLITE_TRANSIENT);
      sqlite3_bind_text(stmt, 6, aBuffer[curr].dataType.c_str(), std::strlen(aBuffer[curr].dataType.c_str()), SQLITE_TRANSIENT);
      sqlite3_bind_text(stmt, 7, aBuffer[curr].content.c_str(), std::strlen(aBuffer[curr].content.c_str()), SQLITE_TRANSIENT);
      sqlite3_bind_int64(stmt, 8, pOffset[j]);
      sqlite3_bind_int64(stmt, 9, pSize[j]);
      sqlite3_bind_text(stmt, 10, aFileName.c_str(), std::strlen(aFileName.c_str()), SQLITE_TRANSIENT);
      sqlite3_bind_double(stmt, 11, aBuffer[curr].positionX);
      sqlite3_bind_double(stmt, 12, aBuffer[curr].positionY);
      sqlite3_bind_double(stmt, 13, aBuffer[curr].positionZ);

      j++;

      retCode = sqlite3_step(stmt);
      if(retCode == SQLITE_DONE || retCode == SQLITE_ROW)
        ;
      else
      {
        ROS_ERROR("[scene][storage] Sqlite3_step error: %s", sqlite3_errmsg(_dbHandle));
        sqlite3_finalize(stmt);
        goto abort;
      }
    }
    /* we have now to finalize the query [memory cleanup] */
    sqlite3_finalize(stmt);
    strcpy(sql, "COMMIT");
    retCode = sqlite3_exec(_dbHandle, sql, NULL, NULL, &errMsg);
    if (retCode != SQLITE_OK) 
    {
      /* some error occurred */
      ROS_ERROR("[scene][storage] COMMIT error: %s", errMsg);
      goto abort;
    }
    ROS_DEBUG("[scene][storage] Write Sqlite DB successful,count: %d.",h);

    return 0;

  abort:
    sqlite3_free(errMsg);

    return -1;

  }


  //support json extension
  int32_t SqliteDao::searchDB(vector<struct DBSearchResultStructure> &aVec, int32_t aSwarmID, string anActorName,
                   int32_t aRobotID, vector<string> aDataTypeVec, string jsonCondition, double anXMinToFind, double anXMaxToFind,
                   double aYMinToFind, double aYMaxToFind, double aZMinToFind, double aZMaxToFind,
                   string aTimeBegin, string aTimeEnd)
  {
    string sql ="select offset, size, fileName from observe ";
    //jsonCondition = "json_extract(observe.content,'$."+
    //json_extract(observe.content,'$.b');
    if((anXMinToFind != -1) && (anXMaxToFind != -1) && (aYMinToFind != -1) && (aYMaxToFind != -1) && (aZMinToFind != -1) && (aZMaxToFind != -1))
    {
      sql = sql + " where ((positionX <= "+std::to_string(anXMaxToFind)+" and positionX >= "+std::to_string(anXMinToFind)+") and (positionY <= "+std::to_string(aYMaxToFind)+" and positionY >= "+std::to_string(aYMinToFind)+")"+" and (positionZ <= "+std::to_string(aZMaxToFind)+" and positionZ >= "+std::to_string(aZMinToFind)+")) ";
    }

    if((aTimeBegin.compare("0") == 0) && (aTimeEnd.compare("0") == 0))
      ;
    else
    {
      string::size_type idx;
      idx = sql.find("where");
      if(idx == string::npos )
        sql = sql+" where ";
      else
        sql = sql +" and ";

      if(aTimeBegin.compare("0") == 0)
        sql = sql + " (timeStamp <= \""+aTimeEnd+"\") ";
      else if(aTimeEnd.compare("0") == 0)
        sql = sql + " (timeStamp >= \""+aTimeBegin+"\") ";
      else
      {
        if(aTimeBegin.compare(aTimeEnd) == 0)
          sql = sql +"(abs(timeStamp - "+aTimeBegin+ ") in (select min(abs(timeStamp - "+aTimeBegin+")) from observe))";
        else
          sql = sql + " (timeStamp >= \""+aTimeBegin+"\" and timeStamp <= \""+aTimeEnd+"\") ";
      }
    }

    if(aSwarmID != -1)
    {
      string::size_type idx;
      idx = sql.find("where");
      if(idx == string::npos )
        sql = sql + " where swarmID="+std::to_string(aSwarmID);
      else
        sql = sql + " and swarmID="+std::to_string(aSwarmID);
    }

    if(anActorName.empty() == 0)
    {
      string::size_type idx;
      idx = sql.find("where");
      if(idx == string::npos )
        sql = sql + " where actorName=\""+anActorName+"\"";
      else
        sql = sql + " and actorName=\""+anActorName+"\"";
    }

      //if(robotID.empty() == 0)
    if(aRobotID != -1)
    {
      string::size_type idx;
      idx = sql.find("where");
      if(idx == string::npos )
        sql = sql + " where robotID="+std::to_string(aRobotID);
      else
        sql = sql + " and robotID="+std::to_string(aRobotID);
    }

    if(aDataTypeVec.size() > 0)
    {
      string::size_type idx;
      idx = sql.find("where");
      if(idx == string::npos )
      {
        sql = sql + " where dataType=\""+aDataTypeVec[0]+"\"";
        int i;
        for(i=1;i<aDataTypeVec.size();i++)
          sql = sql + " or dataType=\""+aDataTypeVec[i]+"\"";
      }
      else
      {
        sql = sql + " and dataType=\""+aDataTypeVec[0]+"\"";
        int i;
        for(i=1;i<aDataTypeVec.size();i++)
          sql = sql + " or dataType=\""+aDataTypeVec[i]+"\"";
      }
    }

    if(jsonCondition.length()>1)
    {
      string::size_type idx;
      idx = sql.find("where");
      if(idx == string::npos )
        sql = sql + " where "+ jsonCondition;
      else
        sql = sql + " and "+ jsonCondition;
    }

    sql = sql + ";";
    ROS_DEBUG("[scene][storage] Query for %s is [%s]", anActorName.c_str(), sql.c_str());

      //uint32_t nrow, ncolumn;
    char** azResult = 0;
    sqlite3_stmt* stmt = NULL;
    const char* zTail;
    DBSearchResultStructure rsltStruct;
    char* errMsg = NULL;

    uint32_t i, j;

    // traverse db to find corresponding record

    int32_t retCode = sqlite3_prepare_v2(_dbHandle, sql.c_str(), -1, &stmt, &zTail);

    //  ROS_INFO("retCode is %d\n", retCode);

    if(SQLITE_OK != retCode)
    {
      ROS_DEBUG("[scene][storage] QUERY SQL error: %s", sqlite3_errmsg(_dbHandle));
      sqlite3_free(errMsg);
      return -1;
    }

    //ROS_INFO("sqlite3_step code is %d =================================================\n", sqlite3_step(stmt));
    // int32_t code;
    while((retCode = sqlite3_step(stmt)) == SQLITE_ROW)
    {
      //ROS_INFO("sqlite3_step code is %d =================================================\n", retCode);
      //ROS_INFO(".............................................results\n");
      uint64_t offset = sqlite3_column_int(stmt, 0);
      uint64_t size = sqlite3_column_int(stmt, 1);
      const unsigned char* fileName = sqlite3_column_text(stmt, 2);
      string name = (const char*)(fileName);
      rsltStruct.offset = offset;
      rsltStruct.size = size;

      rsltStruct.fileName = name;
      aVec.push_back(rsltStruct);
    }
    ROS_DEBUG("[scene][storage] Sqlite3_step code is %d ,aVec size is %d", retCode,aVec.size());

    sqlite3_free(errMsg);
    return aVec.size();
  }

  // a general method to search the sqlite3 db
  int32_t SqliteDao::searchDB(vector<struct DBSearchResultStructure> &aVec, string aCondition)
  {
    string sql ="select offset, size, fileName from observe ";
    if(aCondition.length()>1) sql =sql + " where "+ aCondition +";";

    char** azResult = 0;
    sqlite3_stmt* stmt = NULL;
    const char* zTail;
    DBSearchResultStructure rsltStruct;
    char* errMsg = NULL;

    uint32_t i, j;
    // traverse db to find corresponding record

    int32_t retCode = sqlite3_prepare_v2(_dbHandle, sql.c_str(), -1, &stmt, &zTail);
    if(SQLITE_OK != retCode)
    {
      ROS_DEBUG("[scene][storage] QUERY SQL error: %s", sqlite3_errmsg(_dbHandle));
      sqlite3_free(errMsg);

      return -1;
    }

    //ROS_INFO("sqlite3_step code is %d =================================================\n", sqlite3_step(stmt));
    //int32_t code;
    while((retCode = sqlite3_step(stmt)) == SQLITE_ROW)
    {
      //ROS_INFO("sqlite3_step code is %d =================================================\n", retCode);
      //ROS_INFO(".............................................results\n");
      uint64_t offset = sqlite3_column_int(stmt, 0);
      uint64_t size = sqlite3_column_int(stmt, 1);
      const unsigned char* fileName = sqlite3_column_text(stmt, 2);
      string name = (const char*)(fileName);

      rsltStruct.offset = offset;
      rsltStruct.size = size;

      rsltStruct.fileName= name;
      aVec.push_back(rsltStruct);
    }
    ROS_DEBUG("[scene][storage]sqlite3_step code is %d,aVec size is %d.", retCode, aVec.size());

    sqlite3_free(errMsg);

    return aVec.size();

  }

  int32_t SqliteDao::searchDB(vector<struct DBSearchResultStructure> &aVec, int32_t aSwarmID, string anActorName,
                          int32_t aRobotID, vector<string> aDataTypeVec, double anXMinToFind, double anXMaxToFind,
                          double aYMinToFind, double aYMaxToFind, double aZMinToFind, double aZMaxToFind,
                          string aTimeBegin, string aTimeEnd)
  {

    string sql ="select offset, size, fileName from observe ";

    if((anXMinToFind != -1) && (anXMaxToFind != -1) && (aYMinToFind != -1) && (aYMaxToFind != -1) && (aZMinToFind != -1) && (aZMaxToFind != -1))
    {
        sql = sql + " where ((positionX <= "+std::to_string(anXMaxToFind)+" and positionX >= "+std::to_string(anXMinToFind)+") and (positionY <= "+std::to_string(aYMaxToFind)+" and positionY >= "+std::to_string(aYMinToFind)+")"+" and (positionZ <= "+std::to_string(aZMaxToFind)+" and positionZ >= "+std::to_string(aZMinToFind)+")) ";
    }

    if((aTimeBegin.compare("0") == 0) && (aTimeEnd.compare("0") == 0))
      ;
    else
    {
      string::size_type idx;
      idx = sql.find("where");

      if(idx == string::npos )
        sql = sql+" where ";
      else
        sql = sql +" and ";

      if(aTimeBegin.compare("0") == 0)
        sql = sql + " (timeStamp <= \""+aTimeEnd+"\") ";
      else if(aTimeEnd.compare("0") == 0)
        sql = sql + " (timeStamp >= \""+aTimeBegin+"\") ";
      else
      {
        if(aTimeBegin.compare(aTimeEnd) == 0)
          sql = sql +"(abs(timeStamp - "+aTimeBegin+ ") in (select min(abs(timeStamp - "+aTimeBegin+")) from observe))";
        else
          sql = sql + " (timeStamp >= \""+aTimeBegin+"\" and timeStamp <= \""+aTimeEnd+"\") ";
      }
    }

    if(aSwarmID != -1)
    {
      string::size_type idx;
      idx = sql.find("where");

      if(idx == string::npos )
        sql = sql + " where swarmID="+std::to_string(aSwarmID);
      else
        sql = sql + " and swarmID="+std::to_string(aSwarmID);
    }

    if(anActorName.empty() == 0)
    {
      string::size_type idx;
      idx = sql.find("where");

      if(idx == string::npos )
        sql = sql + " where actorName=\""+anActorName+"\"";
      else
        sql = sql + " and actorName=\""+anActorName+"\"";
    }

    //if(robotID.empty() == 0)
    if(aRobotID != -1)
    {
      string::size_type idx;
      idx = sql.find("where");

      if(idx == string::npos )
        sql = sql + " where robotID="+std::to_string(aRobotID);
      else
        sql = sql + " and robotID="+std::to_string(aRobotID);
    }

    if(aDataTypeVec.size() > 0)
    {
      string::size_type idx;
      idx = sql.find("where");

      if(idx == string::npos )
      {
        sql = sql + " where dataType=\""+aDataTypeVec[0]+"\"";
        int i;
        for(i=1;i<aDataTypeVec.size();i++)
          sql = sql + " or dataType=\""+aDataTypeVec[i]+"\"";
      }
      else
      {
        sql = sql + " and dataType=\""+aDataTypeVec[0]+"\"";
        int i;
        for(i=1;i<aDataTypeVec.size();i++)
          sql = sql + " or dataType=\""+aDataTypeVec[i]+"\"";
      }
    }

    sql = sql + ";";

    ROS_DEBUG("[scene][storge] Query for %s is [%s]", anActorName.c_str(), sql.c_str());

    //uint32_t nrow, ncolumn;
    char** azResult = 0;
    sqlite3_stmt* stmt = NULL;
    const char* zTail;
    DBSearchResultStructure rsltStruct;
    char* errMsg = NULL;

    uint32_t i, j;


    // traverse db to find corresponding record

    int32_t retCode = sqlite3_prepare_v2(_dbHandle, sql.c_str(), -1, &stmt, &zTail);

    ROS_DEBUG("[scene][storge] RetCode is %d", retCode);

    if(SQLITE_OK != retCode)
    {
      ROS_ERROR("[scene][storge] QUERY SQL error: %s", sqlite3_errmsg(_dbHandle));
      sqlite3_free(errMsg);

      return -1;
    }

    //ROS_INFO("sqlite3_step code is %d =================================================\n", sqlite3_step(stmt));
  // int32_t code;
    while((retCode = sqlite3_step(stmt)) == SQLITE_ROW)
    {
      //ROS_INFO("sqlite3_step code is %d =================================================\n", retCode);
      //ROS_INFO(".............................................results\n");
      uint64_t offset = sqlite3_column_int(stmt, 0);
      uint64_t size = sqlite3_column_int(stmt, 1);
      const unsigned char* fileName = sqlite3_column_text(stmt, 2);
      string name = (const char*)(fileName);

      rsltStruct.offset = offset;
      rsltStruct.size = size;

      rsltStruct.fileName = name;
      aVec.push_back(rsltStruct);
    }
    ROS_DEBUG("[scene][storge]sqlite3_step code is %d, aVec size is %d.", retCode, aVec.size());

    sqlite3_free(errMsg);

    return aVec.size();
  }


  int32_t SqliteDao::searchDBWithMultiActors(vector<struct DBSearchResultStructure> &aVec, int32_t aSwarmID,
                                      vector<string> anActorNameList, string aRobotID, vector<string> aDataTypeList,
                                      double anXMinToFind, double anXMaxToFind, double aYMinToFind, double aYMaxToFind,
                                      double aZMinToFind, double aZMaxToFind, string aTimeBegin, string aTimeEnd)
  {
    string sql ="select offset, size, fileName from observe ";

    if((anXMinToFind != -1) && (anXMaxToFind != -1) && (aYMinToFind != -1) && (aYMaxToFind != -1) && (aZMinToFind != -1) && (aZMaxToFind != -1))
    {
        sql = sql + " where ((positionX <= "+std::to_string(anXMaxToFind)+" and positionX >= "+std::to_string(anXMinToFind)+") and (positionY <= "+std::to_string(aYMaxToFind)+" and positionY >= "+std::to_string(aYMinToFind)+")"+" and (positionZ <= "+std::to_string(aZMaxToFind)+" and positionZ >= "+std::to_string(aZMinToFind)+")) ";
    }

    if((aTimeBegin.compare("0") == 0) && (aTimeEnd.compare("0") == 0))
      ;
    else
    {
      string::size_type idx;
      idx = sql.find("where");

      if(idx == string::npos )
        sql = sql+" where ";
      else
        sql = sql +" and ";

      if(aTimeBegin.compare("0") == 0)
        sql = sql + " (timeStamp <= \""+aTimeEnd+"\") ";
      else if(aTimeEnd.compare("0") == 0)
        sql = sql + " (timeStamp >= \""+aTimeBegin+"\") ";
      else
      {
        if(aTimeBegin.compare(aTimeEnd) == 0)
          sql = sql +"(abs(timeStamp - "+aTimeBegin+ ") in (select min(abs(timeStamp - "+aTimeBegin+")) from observe))";
        else
          sql = sql + " (timeStamp >= \""+aTimeBegin+"\" and timeStamp <= \""+aTimeEnd+"\") ";
      }
    }

    if(aSwarmID != -1)
    {
      string::size_type idx;
      idx = sql.find("where");

      if(idx == string::npos )
        sql = sql + " where swarmID="+std::to_string(aSwarmID);
      else
        sql = sql + " and swarmID="+std::to_string(aSwarmID);
    }

    if(anActorNameList.size() > 0)
    {
      string::size_type idx;
      idx = sql.find("where");

      if(idx == string::npos )
      {
        sql = sql + " where (actorName=\""+anActorNameList[0]+"\"";
        int i;
        for(i=1;i<anActorNameList.size();i++)
          sql = sql + " or actorName=\""+anActorNameList[i]+"\"";

        sql = sql + ")";
      }
      else
      {
        sql = sql + " and (actorName=\""+anActorNameList[0]+"\"";
        int i;
        for(i=1;i<anActorNameList.size();i++)
          sql = sql + " or actorName=\""+anActorNameList[i]+"\"";

        sql = sql + ")";
      }
    }

    if(aRobotID.empty() == 0)
    {
      string::size_type idx;
      idx = sql.find("where");

      if(idx == string::npos )
        sql = sql + " where robotID=\""+aRobotID+"\"";
      else
        sql = sql + " and robotID=\""+aRobotID+"\"";
    }

    if(aDataTypeList.size() > 0)
    {
      string::size_type idx;
      idx = sql.find("where");

      if(idx == string::npos )
      {
        sql = sql + " where (dataType=\""+aDataTypeList[0]+"\"";
        int i;
        for(i=1;i<aDataTypeList.size();i++)
          sql = sql + " or dataType=\""+aDataTypeList[i]+"\"";

        sql = sql + ")";
      }
      else
      {
        sql = sql + " and (dataType=\""+aDataTypeList[0]+"\"";
        int i;
        for(i=1;i<aDataTypeList.size();i++)
          sql = sql + " or dataType=\""+aDataTypeList[i]+"\"";

        sql = sql + ")";
      }
    }

    sql = sql + ";";

    ROS_DEBUG("[scene][storge] Query for actors is [%s]", sql.c_str());

    //uint32_t nRow, nColumn;
    char** azResult = 0;
    sqlite3_stmt* stmt = NULL;
    const char* zTail;
    DBSearchResultStructure rsltStruct;
    char* errMsg = NULL;

    uint32_t i, j;


    // traverse db to find corresponding record

    int32_t retCode = sqlite3_prepare_v2(_dbHandle, sql.c_str(), -1, &stmt, &zTail);
    if(SQLITE_OK != retCode)
    {
      ROS_ERROR("[scene][storge] QUERY SQL error: %s", sqlite3_errmsg(_dbHandle));
      sqlite3_free(errMsg);

      return -1;
    }

    //ROS_INFO("sqlite3_step code is %d =================================================\n", sqlite3_step(stmt));
    //int32_t code;
    while((retCode = sqlite3_step(stmt)) == SQLITE_ROW)
    {
      //ROS_INFO("sqlite3_step code is %d =================================================\n", retCode);
      //ROS_INFO(".............................................results\n");
      uint64_t offset = sqlite3_column_int(stmt, 0);
      uint64_t size = sqlite3_column_int(stmt, 1);
      const unsigned char* fileName = sqlite3_column_text(stmt, 2);
      string name = (const char*)(fileName);

      rsltStruct.offset = offset;
      rsltStruct.size = size;

      rsltStruct.fileName = name;
      aVec.push_back(rsltStruct);
    }
    ROS_DEBUG("[scene][storge] Sqlite3_step code is %d, aVec size is %d", retCode, aVec.size());

    sqlite3_free(errMsg);


    return aVec.size();
  }
    
  int32_t SqliteDao::searchDBWithMultiActors(vector<struct DBSearchResultStructure> &aVec, int32_t aSwarmID,
                                      vector<string> anActorNameList, string aRobotID, vector<string> aDataTypeList, string jsonCondition,
                                      double anXMinToFind, double anXMaxToFind, double aYMinToFind, double aYMaxToFind,
                                      double aZMinToFind, double aZMaxToFind, string aTimeBegin, string aTimeEnd)
  {
    string sql ="select offset, size, fileName from observe ";

    if((anXMinToFind != -1) && (anXMaxToFind != -1) && (aYMinToFind != -1) && (aYMaxToFind != -1) && (aZMinToFind != -1) && (aZMaxToFind != -1))
    {
      sql = sql + " where ((positionX <= "+std::to_string(anXMaxToFind)+" and positionX >= "+std::to_string(anXMinToFind)+") and (positionY <= "+std::to_string(aYMaxToFind)+" and positionY >= "+std::to_string(aYMinToFind)+")"+" and (positionZ <= "+std::to_string(aZMaxToFind)+" and positionZ >= "+std::to_string(aZMinToFind)+")) ";
    }

    if((aTimeBegin.compare("0") == 0) && (aTimeEnd.compare("0") == 0))
      ;
    else
    {
      string::size_type idx;
      idx = sql.find("where");

      if(idx == string::npos )
        sql = sql+" where ";
      else
        sql = sql +" and ";

      if(aTimeBegin.compare("0") == 0)
        sql = sql + " (timeStamp <= \""+aTimeEnd+"\") ";
      else if(aTimeEnd.compare("0") == 0)
        sql = sql + " (timeStamp >= \""+aTimeBegin+"\") ";
      else
      {
        if(aTimeBegin.compare(aTimeEnd) == 0)
          sql = sql +"(abs(timeStamp - "+aTimeBegin+ ") in (select min(abs(timeStamp - "+aTimeBegin+")) from observe))";
        else
          sql = sql + " (timeStamp >= \""+aTimeBegin+"\" and timeStamp <= \""+aTimeEnd+"\") ";
      }
    }

    if(aSwarmID != -1)
    {
      string::size_type idx;
      idx = sql.find("where");

      if(idx == string::npos )
        sql = sql + " where swarmID="+std::to_string(aSwarmID);
      else
        sql = sql + " and swarmID="+std::to_string(aSwarmID);
    }

    if(anActorNameList.size() > 0)
    {
      string::size_type idx;
      idx = sql.find("where");

      if(idx == string::npos )
      {
        sql = sql + " where (actorName=\""+anActorNameList[0]+"\"";
        int i;
        for(i=1;i<anActorNameList.size();i++)
          sql = sql + " or actorName=\""+anActorNameList[i]+"\"";

        sql = sql + ")";
      }
      else
      {
        sql = sql + " and (actorName=\""+anActorNameList[0]+"\"";
        int i;
        for(i=1;i<anActorNameList.size();i++)
          sql = sql + " or actorName=\""+anActorNameList[i]+"\"";
          sql = sql + ")";
      }
    }

    if(aRobotID.empty() == 0)
    {
      string::size_type idx;
      idx = sql.find("where");

      if(idx == string::npos )
        sql = sql + " where robotID=\""+aRobotID+"\"";
      else
        sql = sql + " and robotID=\""+aRobotID+"\"";
    }

    if(aDataTypeList.size() > 0)
    {
      string::size_type idx;
      idx = sql.find("where");

      if(idx == string::npos )
      {
        sql = sql + " where (dataType=\""+aDataTypeList[0]+"\"";
        int i;
        for(i=1;i<aDataTypeList.size();i++)
          sql = sql + " or dataType=\""+aDataTypeList[i]+"\"";

        sql = sql + ")";
      }
      else
      {
        sql = sql + " and (dataType=\""+aDataTypeList[0]+"\"";
        int i;
        for(i=1;i<aDataTypeList.size();i++)
          sql = sql + " or dataType=\""+aDataTypeList[i]+"\"";

        sql = sql + ")";
      }
    }

    if(jsonCondition.length()<1)
    {
      string::size_type idx;
      idx = sql.find("where");

      if(idx == string::npos )
        sql = sql + " where " +jsonCondition;
      else
        sql = sql + " and "+ jsonCondition;
    }


    sql = sql + ";";

    ROS_DEBUG("[scene][storge] Query for actors is [%s]", sql.c_str());

    //uint32_t nRow, nColumn;
    char** azResult = 0;
    sqlite3_stmt* stmt = NULL;
    const char* zTail;
    DBSearchResultStructure rsltStruct;
    char* errMsg = NULL;

    uint32_t i, j;


    // traverse db to find corresponding record

    int32_t retCode = sqlite3_prepare_v2(_dbHandle, sql.c_str(), -1, &stmt, &zTail);
    if(SQLITE_OK != retCode)
    {
      ROS_ERROR("[scene][storge] QUERY SQL error: %s", sqlite3_errmsg(_dbHandle));
      sqlite3_free(errMsg);

      return -1;
    }

    //ROS_INFO("sqlite3_step code is %d =================================================\n", sqlite3_step(stmt));
    //int32_t code;
    while((retCode = sqlite3_step(stmt)) == SQLITE_ROW)
    {
      //ROS_INFO("sqlite3_step code is %d =================================================\n", retCode);
      //ROS_INFO(".............................................results\n");
      uint64_t offset = sqlite3_column_int(stmt, 0);
      uint64_t size = sqlite3_column_int(stmt, 1);
      const unsigned char* fileName = sqlite3_column_text(stmt, 2);
      string name = (const char*)(fileName);

      rsltStruct.offset = offset;
      rsltStruct.size = size;

      rsltStruct.fileName = name;
      aVec.push_back(rsltStruct);
    }
    ROS_DEBUG("[scene][storge] Sqlite3_step code is %d, aVec size is %d", retCode, aVec.size());

    sqlite3_free(errMsg);

    return aVec.size();
  }

  string SqliteDao::getFirstRec()
  {
    string sql;
    //sql ="select file_name from observe where id in (select min(id) from observe)";
    sql = "select fileName from observe order by id asc limit 0,1";

    int32_t nRow, nColumn;
    char** results;
    char* errMsg = NULL;

    int32_t r = sqlite3_get_table(_dbHandle, sql.c_str(), &results, &nRow, &nColumn, &errMsg);
    if (r != SQLITE_OK)
    {
      ROS_ERROR("[scene][storge] SQL error: %s", errMsg);
      sqlite3_free(errMsg);
      sqlite3_free_table(results);
      return NULL;
    }
    ROS_DEBUG("[scene][storge] It has got first record.");
    sqlite3_free_table(results);

    string name = results[1];
    return name;
  }

  int64_t SqliteDao::getFileSize(string aFileName)
  {
    string sql;
    sql = "select size from observe where file_name=\""+aFileName+"\"";

    sqlite3_stmt * stmt = NULL;
    const char *zTail;
      
    uint64_t fileSize = 0;

    // traverse db to find corresponding record

    int32_t retCode = sqlite3_prepare_v2(_dbHandle, sql.c_str(), -1, &stmt, &zTail);
    if(SQLITE_OK != retCode)
    {
      sqlite3_close_v2(_dbHandle);
      return -1;
    }

    while(sqlite3_step(stmt) == SQLITE_ROW)
    {
      fileSize += sqlite3_column_int(stmt,0);
    }

    return fileSize;
  }

  int SqliteDao::close()
  {
    return sqlite3_close_v2(_dbHandle);
  }

  int64_t SqliteDao::deleteDB(string aFileName)
  {
    char *errMsg = NULL;

    string sql;
    sql = "delete from observe where file_name=\""+aFileName+"\"";

    int32_t retCode = sqlite3_exec(_dbHandle, sql.c_str(), NULL, NULL, &errMsg);
    if(retCode != SQLITE_OK)
    {
      ROS_ERROR("[scene][storge] Delete record  error: %s", errMsg);
      sqlite3_free(errMsg);
      return -1;
    }
    ROS_DEBUG("[scene][storge] Delete DB sucessfully.");
    sqlite3_free(errMsg);

    return 0;
  }
}
