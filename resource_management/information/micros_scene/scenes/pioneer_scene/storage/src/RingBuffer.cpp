/*=================================================================
* Copyright (c) 2019, micROS Group, NIIDT, TAIIC, HPCL.
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without modification, are permitted
* provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice, this list of conditions and
* the following disclaimer.
* 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions
* and the following disclaimer in the documentation and/or other materials provided with the
* distribution.
* 3. All advertising materials mentioning features or use of this software must display the following
* acknowledgement: This product includes software developed by the micROS Group and its
* contributors.
* 4. Neither the name of the Group nor the names of contributors may be used to endorse or promote
* products derived from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY MICROS GROUP AND CONTRIBUTORS ''AS IS''AND
* ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
* PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE MICROS, GROUP OR
* CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
* EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
* PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
* PROFITS; OR BUSINESS INTERRUPTION). HOWEVER CAUSED AND ON ANY THEORY
* OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
* NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
* SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
===================================================================
* Author: SongChang Jin, Bin Lin.
*/
#include <storage/RingBuffer.h>

namespace micros_storage
{
  using std::string;
  using std::vector;

  RingBuffer::RingBuffer(){}
  RingBuffer::~RingBuffer(){}

  void RingBuffer::initialize(uint32_t aBufferSize, std::string aSerielizeDataDir, std::string aDatabaseName)
  {
    _ringBufferCapacity = aBufferSize;
    _writeThreshold = _ringBufferCapacity / 10;

    _serielizeDataDir = aSerielizeDataDir;
    _databaseName = aDatabaseName;

    _ringBuffer.reset(new abstract_msgs::TmpMSG[_ringBufferCapacity]);

    _dbDao.reset(new SqliteDao(_databaseName));

    atomic_set(&_cursorH, 0);
    atomic_set(&_cursorT, 0);
    atomic_set(&_isWriting, 0);

    _currStorageUsed = getDataSize(_serielizeDataDir.c_str());

    _threadRun = 1; // flag is 1 means write_thread is running
    _writeThread.reset(new boost::thread(boost::bind(&RingBuffer::writeHandle, this)));
    _writeThread->detach();
    //detach调用之后，目标线程就成为了守护线程，驻留后台运行，与之关联的std::thread对象失去对目标线程的关联，无法再通过std::thread对象取得该线程的控制权。当线程主函数执行完之后，线程就结束了，运行时库负责清理与该线程相关的资源。
    ROS_INFO("[scene][storge] RingBuffer init.");
  }

  void RingBuffer::destroy()
  {
    uint32_t cursorH = atomic_read(&_cursorH);
    uint32_t cursorT = atomic_read(&_cursorT);

    // force write of the rest data in RingBuffer before destroy
    if(cursorT == cursorH)
    {
      // free cond and write thread
      _threadRun = 0;
      {
        boost::mutex::scoped_lock lock(_mutex);
        _cond.notify_one();
      }
      sleep(5);
      _writeThread->join();
      //thread::join(): 阻塞当前线程，直至 *this 所标识的线程完成其执行。*this 所标识的线程的完成同步于从 join() 的成功返回。
    }
    else
    {
      if(cursorT > cursorH)
      {
        while(checkIsWriting() == 1)
          sleep(1);
        uint32_t writeSize = cursorT - cursorH;
        flush(cursorH, writeSize);
      }
      else
      {
        while(checkIsWriting() == 1)
          sleep(1);
        uint32_t writeSize = _ringBufferCapacity - cursorH + cursorT;
        flush(cursorH, writeSize);
      }

      // free cond and write thread
      _threadRun = 0;
      {
        boost::mutex::scoped_lock lock(_mutex);
        _cond.notify_one();
      }
      sleep(5);
      _writeThread->join();
    }
    ROS_INFO("[scene][storge] Flush and destory ringBuffer.");
  }

  void RingBuffer::writeHandle()
  {
    while(_threadRun == 1)
    {
      // wait util write threshold is reached or timeout
      boost::mutex::scoped_lock lock(_mutex);
      _cond.timed_wait(lock, boost::get_system_time() + boost::posix_time::seconds(20));

      if(_threadRun == 0)
        return;

      // flush is not performing, or else wait
      if(checkIsWriting() == 0)
      {
        uint32_t cursorH = atomic_read(&_cursorH);
        uint32_t cursorT = atomic_read(&_cursorT);

        if(cursorT != cursorH)
        {
          if(cursorT > cursorH)
          {
            if((cursorT - cursorH) >= _writeThreshold)
            {
              flush(cursorH, _writeThreshold);
              uint32_t tmp = (cursorH + _writeThreshold) % _ringBufferCapacity;
              atomic_set(&_cursorH, tmp);
            }
            else
            {
              //timeout
              uint32_t size = cursorT - cursorH;
              flush(cursorH, size);
              atomic_set(&_cursorH, cursorT);
            }
          }
          else
          {
            if((_ringBufferCapacity - cursorH + cursorT) >= _writeThreshold)
            {
              flush(cursorH, _writeThreshold);
              uint32_t tmp = (cursorH + _writeThreshold) % _ringBufferCapacity;
              atomic_set(&_cursorH, tmp);
            }
            else
            {
              //timeout
              uint32_t size = _ringBufferCapacity - cursorH + cursorT;
              flush(cursorH, size);
              atomic_set(&_cursorH, cursorT);
            }
          }

          atomic_set(&_isWriting, 0);
        }
      }
    }
  }

  uint64_t RingBuffer::getStorageUsed()
  {
    return _currStorageUsed;
  }

  int32_t RingBuffer::getBufSize()
  {
    return _ringBufferCapacity;
  }

  abstract_msgs::TmpMSG RingBuffer::getData(uint32_t anIndex)
  {
    if(anIndex > _ringBufferCapacity-1)
    {
      ROS_ERROR("the index is out of the ringbuffer capacity!");
      exit(-1);
    }
    return _ringBuffer[anIndex];
  }

  bool RingBuffer::checkWriteReady()
  {
    uint32_t cursorH = atomic_read(&_cursorH);
    uint32_t cursorT = atomic_read(&_cursorT);

    if(cursorT >= cursorH)
    {
      if((cursorT - cursorH) >= _writeThreshold)
        return 1;
      else
        return 0;
    }
    else
    {
      if((_ringBufferCapacity - cursorH + cursorT) >= _writeThreshold)
        return 1;
      else
        return 0;
    }
  }

  bool RingBuffer::checkIsWriting()
  {
    return atomic_read(&_isWriting);
  }

  bool RingBuffer::checkBufferIsFull()
  {
    uint32_t cursorH = atomic_read(&_cursorH);
    uint32_t cursorT = atomic_read(&_cursorT);

    if(cursorT == cursorH)
      return 0;

    if(cursorT > cursorH)
    {
      if((cursorT - cursorH) >= _ringBufferCapacity - 1)
        return 1;
      else
        return 0;
    }
    else
    {
      if((cursorH - cursorT) <= 1)
        return 1;
      else
        return 0;
    }
  }

  int32_t RingBuffer::flush(uint32_t aBegin, uint32_t aWriteSize)
  {
    atomic_set(&_isWriting, 1);

    string fileName = _serielizeDataDir + "data_" + _ringBuffer[aBegin].timeStamp;

    uint64_t offset[aWriteSize];
    uint64_t size[aWriteSize];

    //Serialize & Produce Metadata
    uint64_t serialSize = 0;
    uint32_t i;
    for(i = 0; i < aWriteSize;i++)
    {
      uint32_t curr = (aBegin + i) % _ringBufferCapacity;

      abstract_msgs::UnifiedData ud;
      _tools.tmpMSG2UnifiedData(ud,_ringBuffer[curr]);
      serialSize = serialSize + ros::serialization::serializationLength(ud);
      // serialSize = serialSize + ros::serialization::serializationLength(_ringBuffer[curr]);//这里是要将一系列的msg写入文件，所以这里的msg应当是UnifiledData
    }

    boost::shared_array<char> buffer(new char[serialSize + 1]);//end is '\0'

    uint64_t pos = 0;
    for(i = 0;i < aWriteSize;i++)
    {
      uint32_t curr = (aBegin + i) % _ringBufferCapacity;
    
      abstract_msgs::UnifiedData msg;
      int32_t rc = _tools.tmpMSG2UnifiedData(msg, _ringBuffer[curr]);
      if(rc == -1)
        ROS_ERROR("Format transfer from TmpMSG to UnifiedData error!");

      uint64_t tmpSerialSize = ros::serialization::serializationLength(msg);
      boost::shared_array<uint8_t> tmpBuffer(new uint8_t[tmpSerialSize]);
      ros::serialization::OStream stream(tmpBuffer.get(), tmpSerialSize);

      ros::serialization::serialize(stream, msg);//

      memcpy(&buffer[pos], tmpBuffer.get(), tmpSerialSize);

      if(i == 0)
        offset[i] = 0;
      else
        offset[i] = offset[i-1] + size[i-1];

      size[i] = tmpSerialSize;
      pos = pos + size[i];
    }
    buffer[pos] = '\0';

    //Write to file
    FILE* file;
    if((file = fopen(fileName.c_str(), "wb")) == NULL)
    {
      ROS_INFO("Cannot create file %s\n", fileName.c_str());
      return -1;
    }
    fwrite(buffer.get(), 1, pos, file);
    fclose(file);

    _currStorageUsed += pos;

    //Write to database with the content column
    _dbDao->insertDB(offset, size, fileName, _ringBuffer, aBegin, aWriteSize, _ringBufferCapacity);

    return 0;
  }

  uint64_t RingBuffer::getDataSize(const char* aDir)
  {
    DIR *dp;
    struct dirent *entry;
    struct stat statBuf;
    int64_t totalSize = 0;

    if((dp = opendir(aDir)) == NULL)
    {
      ROS_INFO("Cannot open dir: %s\n", aDir);
      return -1;
    }

    while((entry = readdir(dp)) != NULL)
    {
      char subdir[256];
      sprintf(subdir, "%s/%s", aDir, entry->d_name);
      lstat(subdir, &statBuf);

      if(S_ISDIR(statBuf.st_mode))
      {
        if(strcmp(".", entry->d_name) == 0 || strcmp("..", entry->d_name) == 0)
            continue;
        int64_t subDirSize = getDataSize(subdir);
        totalSize += subDirSize;
      }
      else
        totalSize += statBuf.st_size;
    }

    closedir(dp);
    return totalSize;
  }

  void RingBuffer::dataWrite(const abstract_msgs::TmpMSG aMsg)
  {
    boost::unique_lock<boost::mutex> lock(_writeLock);
    uint32_t cursorT = atomic_read(&_cursorT);
    uint32_t cursorH = atomic_read(&_cursorH);
    uint32_t writePtr = cursorT;
    if(checkBufferIsFull() == 1)
    {
      lock.unlock();
      return;
    }
    else
    {
      //RingBuffer is not full, insert data to RingBuffer
      //RingBuffer write threshold is reached && not flush is performing
      if((checkWriteReady() == 1) && (checkIsWriting() ==0))
      {
        boost::mutex::scoped_lock lock(_mutex);
        _cond.notify_one();
      }

      _ringBuffer[writePtr] = aMsg;

      if(cursorT == (_ringBufferCapacity - 1))
        atomic_set(&_cursorT, 0);
      else
        atomic_inc(&_cursorT);
    }
    lock.unlock();
  }

}
