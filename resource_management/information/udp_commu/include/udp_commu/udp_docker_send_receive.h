/*******************************
 *  docker send receive use UDP 
 *  author by YYT
 * 	date:2019-09-04
 **************************/
#include "ros/ros.h"
#include "std_msgs/String.h"
#include <rosplane_msgs/State.h>
#include <rosplane_msgs/Command.h>
#include <rosplane_msgs/Controller_Commands.h>
#include <rosplane_msgs/Controller_Internals.h>
#include <rosplane_msgs/Path.h>
#include <rosplane_msgs/Waypoint.h>
#include <rosplane_msgs/Formation.h>
#include <nav_msgs/Odometry.h>
#include <udp_commu/UdpCommPacket.h>
#include <nav_msgs/Path.h>
#include <std_msgs/Empty.h>

#include "udp_commu/Down_Data_Speed_Nav.h"
#include <geometry_msgs/Twist.h>
#include "udp_commu/Gazebo_State.h"
#include <udp_commu/Heart_Beat.h>
#include <vector>

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

//串口相关的头文件  
#include<stdio.h>      /*标准输入输出定义*/  
#include<stdlib.h>     /*标准函数库定义*/  
#include<unistd.h>     /*Unix 标准函数定义*/  
#include<sys/types.h>   
#include<sys/stat.h>     
#include<fcntl.h>      /*文件控制定义*/  
#include<termios.h>    /*PPSIX 终端控制定义*/  
#include<errno.h>      /*错误号定义*/  
#include<string.h>  
#include <math.h>
#include <boost/thread/thread.hpp>

//宏定义  
#define FALSE  -1  
#define TRUE   0  
#define REC_BUFFER_SIZE 200


namespace udp_commu 
{

	using std::string;
    //1. send
	class Udp_commu_docker_send
	{
	public:
		Udp_commu_docker_send();
        ~Udp_commu_docker_send();
        // all the callback
        void controller_commandsSendCallback(const rosplane_msgs::Controller_Commands::ConstPtr& msg);
        void trajectorySendCallback(const nav_msgs::Path::ConstPtr& msg);
        void send_to_UTOSendCallback(const std_msgs::String::ConstPtr& msg);

        void udp_Close();
        void print_count();

    private:
        ros::NodeHandle n;

        ros::Subscriber controller_commands_sub_;
        ros::Subscriber trajectory_sub_;
        ros::Subscriber send_to_UTO_sub_;

        string gazebo_ipaddress_;
        string namespace_;
        int port_;
        int client_sockfd;

        struct sockaddr_in remote_addr; //服务器端网络地址结构体
        int sin_size;
        char buf[BUFSIZ];  //数据传送的缓冲区
        int total_count;
        int controller_commands_count;
        int trajectory_count;
        int send_to_UTO_count;
        
        //序号
        int controller_commands_num;
        int trajectory_num;
        int send_to_UTO_num;

    };

    //2. receive
    class Udp_commu_docker_receive
	{
	public:
		Udp_commu_docker_receive();
        ~Udp_commu_docker_receive();

        void receive_gazebo_data();

        void udp_Close();
        void receive();
        void print_count();
        
    private:
		string namespace_; 
        int htonsnumber_;

        ros::Publisher truth_pub;
        ros::Publisher car_pub;
        ros::Publisher obstacle_pub;
        ros::Publisher target_waypoint_pub;
        ros::Publisher gathering_waypoint_pub;
        ros::Publisher areacover_done_pub;
        ros::Publisher formation_type_pub;

        ros::Publisher controller_commands_pub;

		int server_sockfd;
		struct sockaddr_in my_addr;   //服务器网络地址结构体
		struct sockaddr_in remote_addr; //客户端网络地址结构体

		socklen_t sin_size;
		int count_gazeboread;
        boost::thread* gazebo_thread_;

        int total_count;
        int truth_count;
        int car_count;
        int obstacle_count;
        int target_waypoint_count;
        int gathering_waypoint_count;
        int areacover_done_count;
        int formation_type_count;

        int last_total_num;
        int last_truth_num;
        int last_car_num;
        int last_obstacle_num;
        int last_target_waypoint_num;
        int last_gathering_waypoint_num;
        int last_areacover_done_num;
        int last_formation_type_num;

    };

}// end of namesapce