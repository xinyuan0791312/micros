#include <iostream>
#include <ros/ros.h>

#include "fastrtps_pubsub/Fastrtps_PacketPublisher.h"
#include "fastrtps_pubsub/Fastrtps_PacketSubscriber.h"
#include "fastrtps_pubsub/Fastrtps_PacketPubSubTypes.h"
//#include "fastrtps_pubsub/Fastrtps_PacketPartcipent.h"
#include <fastrtps/Domain.h>
#include <fastrtps/utils/eClock.h>
#include <fastrtps/log/Log.h>

#include <sensor_msgs/Image.h>
#include <sensor_msgs/CompressedImage.h>
#include <sensor_msgs/CameraInfo.h>
#include <nav_msgs/Path.h>
#include <std_msgs/String.h>
#include <std_msgs/Empty.h>

#include <rosplane_msgs/State.h>
#include <rosplane_msgs/Command.h>
#include <rosplane_msgs/Controller_Commands.h>
#include <rosplane_msgs/Controller_Internals.h>
#include <rosplane_msgs/MultiVaGoal.h>
#include <rosplane_msgs/Path.h>
#include <rosplane_msgs/VirtualAgent.h>
#include <rosplane_msgs/Waypoint.h>
#include <rosplane_msgs/Formation.h>
#include <nav_msgs/Odometry.h>

#include "udp_commu/Down_Data_Speed_Nav.h"
#include <geometry_msgs/Twist.h>
#include "udp_commu/Gazebo_State.h"
#include <udp_commu/Heart_Beat.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
  
#include<stdio.h>      /*标准输入输出定义*/  
#include<stdlib.h>     /*标准函数库定义*/  
#include<unistd.h>     /*Unix 标准函数定义*/  
#include<sys/types.h>   
#include<sys/stat.h>     
#include<fcntl.h>      /*文件控制定义*/  
#include<termios.h>    /*PPSIX 终端控制定义*/  
#include<errno.h>      /*错误号定义*/  
#include<string.h>  
#include <math.h>
#include <boost/thread/thread.hpp>
#include <ros/callback_queue.h>
#include <ros/spinner.h>

namespace udp_commu 
{

	using std::string;
    using namespace eprosima;
    using namespace eprosima::fastrtps;

	class Fastrtps_gazebo_send_receive
	{
	public:
		Fastrtps_gazebo_send_receive();
        ~Fastrtps_gazebo_send_receive();
        void fastrtps_init();
        void image_raw_callback(const sensor_msgs::CompressedImage::ConstPtr& msg, const int num);
        void camera_info_callback(const sensor_msgs::CameraInfo::ConstPtr& msg, const int num);
        void truth_callback(const rosplane_msgs::State::ConstPtr& msg,const int num);
        void truth_update(const ros::TimerEvent &);
        void obstacle_callback(const rosplane_msgs::WaypointConstPtr& msg);
        void target_waypoint_callback(const rosplane_msgs::WaypointConstPtr& msg);
        void gathering_waypoint_callback(const rosplane_msgs::WaypointConstPtr& msg);
        void areacover_done_callback(const std_msgs::Empty& msg);
        void formation_type_callback(const rosplane_msgs::FormationConstPtr& msg);

        void RTPS_controller_commands_callback(std::vector<uint8_t> msg , int num);
        void RTPS_trajectory_callback(std::vector<uint8_t> msg , int num);
        void RTPS_send_to_UTO_callback(std::vector<uint8_t> msg);

    private:
        ros::NodeHandle nh;
        ros::NodeHandle nh_private_;
        
        std::string pre_namespace;
        int max_num;

        ros::Subscriber truth_sub;
        std::vector<ros::Subscriber> truth_sub_vector;
        ros::Subscriber image_raw_sub;
        std::vector<ros::Subscriber> image_raw_sub_vector;
        ros::Subscriber camera_info_sub;
        std::vector<ros::Subscriber> camera_info_sub_vector;

        ros::Publisher controller_commands_pub;
        ros::Publisher trajectory_pub;
        std::vector<ros::Publisher> controller_commands_pub_vector;
        std::vector<ros::Publisher> trajectory_pub_vector;

        boost::shared_ptr<Fastrtps_PacketPublisher> RTPS_truth_Publisher;
        boost::shared_ptr<Fastrtps_PacketPublisher> RTPS_image_raw_Publisher;
        boost::shared_ptr<Fastrtps_PacketPublisher> RTPS_Camera_Info_Publisher;

        std::vector<boost::shared_ptr<Fastrtps_PacketPublisher>>  RTPS_truth_Publisher_vector;
        std::vector<boost::shared_ptr<Fastrtps_PacketPublisher>>  RTPS_image_raw_Publisher_array;
        std::vector<boost::shared_ptr<Fastrtps_PacketPublisher>>  RTPS_Camera_Info_Publisher_array;

        boost::shared_ptr<Fastrtps_PacketSubscriber> RTPS_trajectory_Subscriber;
        boost::shared_ptr<Fastrtps_PacketSubscriber> RTPS_controller_commands_Subscriber;
        std::vector<boost::shared_ptr<Fastrtps_PacketSubscriber>> RTPS_controller_commands_Subscriber_vector;
        std::vector<boost::shared_ptr<Fastrtps_PacketSubscriber>> RTPS_trajectory_Subscriber_vector;

        ros::Publisher send_to_UTO_pub;

        Fastrtps_PacketPublisher RTPS_state_Publisher;
        //Fastrtps_PacketPublisher RTPS_truth_Publisher;
        Fastrtps_PacketPublisher RTPS_car_Publisher;
        Fastrtps_PacketPublisher RTPS_obstacle_pub;
        Fastrtps_PacketPublisher RTPS_target_waypoint_pub;
        Fastrtps_PacketPublisher RTPS_gathering_waypoint_pub;
        Fastrtps_PacketPublisher RTPS_areacover_done_pub;
        Fastrtps_PacketPublisher RTPS_formation_type_pub;
        Fastrtps_PacketSubscriber RTPS_send_to_UTO_Subscriber;
        
        rosplane_msgs::State state_global;
        ros::CallbackQueue truth_queue;
        ros::SubscribeOptions truth_sub_ops;
        std::vector<ros::SubscribeOptions> truth_sub_ops_vector;
       // ros::AsyncSpinner async_spinner;
        boost::shared_ptr<ros::AsyncSpinner> async_spinner;

    };

}// end of namesapce