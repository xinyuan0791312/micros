#include "ros/ros.h"
#include "std_msgs/String.h"
#include <rosplane_msgs/State.h>
#include <rosplane_msgs/Command.h>
#include <rosplane_msgs/Controller_Commands.h>
#include <rosplane_msgs/Controller_Internals.h>
#include <rosplane_msgs/MultiVaGoal.h>
#include <rosplane_msgs/Path.h>
#include <rosplane_msgs/VirtualAgent.h>
#include <rosplane_msgs/Waypoint.h>
#include <rosplane_msgs/Formation.h>
#include <nav_msgs/Odometry.h>
#include <udp_commu/UdpCommPacket.h>
#include <nav_msgs/Path.h>
#include <std_msgs/Empty.h>

#include "udp_commu/Down_Data_Speed_Nav.h"
#include <geometry_msgs/Twist.h>
#include "udp_commu/Gazebo_State.h"
#include <udp_commu/Heart_Beat.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

//串口相关的头文件  
#include<stdio.h>      /*标准输入输出定义*/  
#include<stdlib.h>     /*标准函数库定义*/  
#include<unistd.h>     /*Unix 标准函数定义*/  
#include<sys/types.h>   
#include<sys/stat.h>     
#include<fcntl.h>      /*文件控制定义*/  
#include<termios.h>    /*PPSIX 终端控制定义*/  
#include<errno.h>      /*错误号定义*/  
#include<string.h>  
#include <math.h>
#include <boost/thread/thread.hpp>

//宏定义  
#define FALSE  -1  
#define TRUE   0  
#define REC_BUFFER_SIZE 200

namespace udp_commu 
{

	using std::string;
    //1. send
	class Udp_commu_gazebo_send
	{
	public:
		Udp_commu_gazebo_send();
        ~Udp_commu_gazebo_send();
        // all the callback

        void truthSendCallback(const rosplane_msgs::State::ConstPtr& msg);
        void carSendCallback(const nav_msgs::Odometry::ConstPtr& msg);
        void obstacleSendCallback(const rosplane_msgs::Waypoint::ConstPtr& msg);
        void target_waypointSendCallback(const rosplane_msgs::Waypoint::ConstPtr& msg);
        void gathering_waypointSendCallback(const rosplane_msgs::Waypoint::ConstPtr& msg);
        void areacover_doneSendCallback(const std_msgs::Empty::ConstPtr& msg);
        void formation_typeSendCallback(const rosplane_msgs::Formation::ConstPtr& msg);
        void print_count();

        void udp_Close();

    private:
        ros::NodeHandle n;

        ros::Subscriber state_sub_;
        ros::Subscriber camera_sub_;
        ros::Subscriber compressed_image_sub_;

        ros::Subscriber truth_sub_;
        ros::Subscriber car_sub_;

        ros::Subscriber obstacle_sub_;
        ros::Subscriber target_waypoint_sub_;
        ros::Subscriber gathering_waypoint_sub_;
        ros::Subscriber areacover_done_sub_;
        ros::Subscriber formation_type_sub_;

        string dockerleader_ipaddress_;
        string namespace_;
        udp_commu::Gazebo_State gazebostate_msg;
        int client_sockfd;

        struct sockaddr_in remote_addr; //服务器端网络地址结构体
        int sin_size;
        char buf[BUFSIZ];  //数据传送的缓冲区
        
        int total_count;
        int truth_count;
        int car_count;
        int obstacle_count;
        int target_waypoint_count;
        int gathering_waypoint_count;
        int areacover_done_count;
        int formation_type_count;
       
        //序号
        int truth_num;
        int car_num;
        int obstacle_num;
        int target_waypoint_num;
        int gathering_waypoint_num;
        int areacover_done_num;
        int formation_type_num;

    };

    //2. receive
    class Udp_commu_gazebo_receive
	{
	public:
		Udp_commu_gazebo_receive();
        ~Udp_commu_gazebo_receive();

        void receive_docker_data();
        void print_count();

        void udp_Close();
        void receive();
        
    private:
		string namespace_; 
        int htonsnumber_;

        ros::Publisher controller_commands_pub;
        ros::Publisher trajectory_pub;
        ros::Publisher send_to_UTO_pub;

		int server_sockfd;
		struct sockaddr_in my_addr;   //服务器网络地址结构体
		struct sockaddr_in remote_addr; //客户端网络地址结构体

		socklen_t sin_size;
		int count_gazeboread;
        boost::thread* docker_thread_;

        int total_count;
        int controller_commands_count;
        int trajectory_count;
        int send_to_UTO_count;

        //上次接收的序号
        int last_controller_commands_num;
        int last_trajectory_num;
        int last_send_to_UTO_num;

    };

}// end of namesapce