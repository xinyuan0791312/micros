
#include <iostream>
#include <ros/ros.h>

#include "fastrtps_pubsub/Fastrtps_PacketPublisher.h"
#include "fastrtps_pubsub/Fastrtps_PacketSubscriber.h"
#include "fastrtps_pubsub/Fastrtps_PacketPubSubTypes.h"

#include <fastrtps/Domain.h>
#include <fastrtps/utils/eClock.h>
#include <fastrtps/log/Log.h>

#include <sensor_msgs/Image.h>
#include <sensor_msgs/CompressedImage.h>
#include <std_msgs/String.h>

#include "ros/ros.h"
#include <rosplane_msgs/State.h>
#include <rosplane_msgs/Command.h>
#include <rosplane_msgs/Controller_Commands.h>
#include <rosplane_msgs/Controller_Internals.h>
#include <rosplane_msgs/MultiVaGoal.h>
#include <rosplane_msgs/Path.h>
#include <rosplane_msgs/VirtualAgent.h>

#include "udp_commu/Down_Data_Speed_Nav.h"
#include <geometry_msgs/Twist.h>
#include "udp_commu/Gazebo_State.h"
#include <udp_commu/Heart_Beat.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
  
#include <stdio.h>      /*标准输入输出定义*/  
#include <stdlib.h>     /*标准函数库定义*/  
#include <unistd.h>     /*Unix 标准函数定义*/  
#include <sys/types.h>   
#include <sys/stat.h>     
#include <fcntl.h>      /*文件控制定义*/  
#include <termios.h>    /*PPSIX 终端控制定义*/  
#include <errno.h>      /*错误号定义*/  
#include <string.h>  
#include <math.h>
#include <boost/thread/thread.hpp>

using std::string;

using namespace eprosima;
using namespace eprosima::fastrtps;
eprosima::fastrtps::Participant *mp_participant = NULL;


Fastrtps_PacketPublisher RTPS_s_area_vertex_Publisher;
Fastrtps_PacketPublisher RTPS_v_area_vertex_Publisher;
Fastrtps_PacketPublisher RTPS_assemble_goals_Publisher;

ros::Publisher cmd_vel_pub;
string namespace_; 
ros::Publisher watchdog_pub;	
int count_receive_cmd = 0;
uint64_t starttime;
uint64_t endtime;

uint64_t getCurrentTime()
{
    struct timeval tv;
    gettimeofday(&tv,NULL);
    return tv.tv_sec * 1000 + tv.tv_usec / 1000;
}

template<class T>
std::vector<uint8_t> serialize_ros(T t)
{
    std::vector<uint8_t> vec;
    uint32_t serial_size = ros::serialization::serializationLength(t);
    boost::shared_array<uint8_t> buffer(new uint8_t[serial_size]);
    ros::serialization::OStream ostream(buffer.get(), serial_size);
    ros::serialization::serialize(ostream, t);
    vec.resize(serial_size);
    std::copy(buffer.get(), buffer.get() + serial_size, vec.begin());
    return vec;
}

template<class T>
T deserialize_ros(const std::vector<uint8_t>& vec)
{
    T t;
    uint32_t serial_size = vec.size();
    std::vector<uint8_t> buffer(serial_size);
    std::copy(vec.begin(), vec.begin() + serial_size, buffer.begin());
    ros::serialization::IStream istream(buffer.data(), serial_size);
    ros::serialization::Serializer<T>::read(istream, t);
    return t;
}


void s_area_vertex_callback(const std_msgs::String::ConstPtr& msg)
{
    // ROS_INFO("success subscribing td_msgs::String (s_area_vertex) from ground station!!!!! \n");
    std::vector<uint8_t> vec_data;
    vec_data = serialize_ros(*msg);
    RTPS_s_area_vertex_Publisher.DDS_pub(vec_data);
    // ROS_INFO("td_msgs::String (s_area_vertex) msg has been published by fastrtps in ground station \n");

}

// void v_area_vertex_callback(const va_msgs::AreaVertex::ConstPtr& msg)
// {
//     // ROS_INFO("success subscribing va_msgs::AreaVertex(v_area_vertex) from ground station!!!!!\n");
//     std::vector<uint8_t> vec_data;
//     vec_data = serialize_ros(*msg);
//     RTPS_v_area_vertex_Publisher.DDS_pub(vec_data);
//     // ROS_INFO("va_msgs::AreaVertex(v_area_vertex) msg has been published by fastrtps in ground station \n");

// }

void assemble_goals_callback(const std_msgs::String::ConstPtr& msg)
{
    // ROS_INFO("success subscribing std_msgs::String(assemble_goals) from ground station!!!!!\n");
    std::vector<uint8_t> vec_data;
    vec_data = serialize_ros(*msg);
    RTPS_assemble_goals_Publisher.DDS_pub(vec_data);
    // ROS_INFO("std_msgs::String(assemble_goals) msg has been published by fastrtps in ground station \n");
}

int main(int argc, char **argv)
{
    std::cout << "fastrtps_pub_sub started in ground station" << std::endl;
    ros::init(argc,argv,"fastrtps_gazebosend_receive");
		
	ros::NodeHandle n;
	string docker_ns = "bingo";
    n.param("namespace", namespace_, docker_ns);
    printf("namespace is %s.................................................................\n", namespace_.c_str());
    //int domainID = 0;
    //domainID = namespace_to_int(namespace_);
    
    ros::Subscriber s_area_vertex_sub = n.subscribe("/s_area_vertex",10,s_area_vertex_callback);
    // ros::Subscriber v_area_vertex_sub = n.subscribe("/v_area_vertex",10,v_area_vertex_callback);
    ros::Subscriber assemble_goals_sub = n.subscribe("/assemble_goals",10,assemble_goals_callback);

    Fastrtps_PacketPublisher RTPS_Publisher;
    RTPS_Publisher.createRTPSParticipant(mp_participant,99,"Participant_information");
	Fastrtps_PacketPubSubType myType;
	Domain::registerType(mp_participant, static_cast<TopicDataType*>(&myType));

    RTPS_s_area_vertex_Publisher.getParticipant(mp_participant);
    RTPS_s_area_vertex_Publisher.init("/fastrtps_s_area_vertex");
    RTPS_v_area_vertex_Publisher.getParticipant(mp_participant);
    RTPS_v_area_vertex_Publisher.init("/fastrtps_v_area_vertex");
    RTPS_assemble_goals_Publisher.getParticipant(mp_participant);
    RTPS_assemble_goals_Publisher.init("/fastrtps_assemble_goals");

    ros::spin();
    Domain::stopAll();
    Log::Reset();
    return 0;
}
