
#include "udp_commu/fastrtps_gazebo_send_receive.h"

using namespace eprosima;
using namespace eprosima::fastrtps;
eprosima::fastrtps::Participant *mp_participant = NULL;

namespace udp_commu{

    template<class T>
    std::vector<uint8_t> serialize_ros(T t)
    {
        std::vector<uint8_t> vec;
        uint32_t serial_size = ros::serialization::serializationLength(t);
        boost::shared_array<uint8_t> buffer(new uint8_t[serial_size]);
        ros::serialization::OStream ostream(buffer.get(), serial_size);
        ros::serialization::serialize(ostream, t);
        vec.resize(serial_size);
        std::copy(buffer.get(), buffer.get() + serial_size, vec.begin());
        return vec;
    }

    template<class T>
    T deserialize_ros(const std::vector<uint8_t>& vec)
    {
        T t;
        uint32_t serial_size = vec.size();
        std::vector<uint8_t> buffer(serial_size);
        std::copy(vec.begin(), vec.begin() + serial_size, buffer.begin());
        ros::serialization::IStream istream(buffer.data(), serial_size);
        ros::serialization::Serializer<T>::read(istream, t);
        return t;
    }
    Fastrtps_gazebo_send_receive::Fastrtps_gazebo_send_receive():nh_private_("~"){}

    void Fastrtps_gazebo_send_receive::fastrtps_init()
    {
        //nh_private_ = ros::NodeHandle("~");
        nh_private_.param<std::string>("pre_namespace", pre_namespace, "robot_1");//命名空间前缀
        nh_private_.param<int>("max_num", max_num,0);//最大数量
        double update_rate_;
        ros::Timer update_timer_;
        
        RTPS_truth_Publisher_vector.reserve(max_num);
        RTPS_image_raw_Publisher_array.reserve(max_num);
        RTPS_Camera_Info_Publisher_array.reserve(max_num);

        //2. 创建多个pub

        truth_sub_vector.reserve(max_num);
        image_raw_sub_vector.reserve(max_num);
        camera_info_sub_vector.reserve(max_num);
        truth_sub_ops_vector.reserve(max_num);

        for(int i = 0; i < max_num; i++){
            string namespace_final = pre_namespace + std::to_string(i+1);
            ROS_INFO("*************** the namespace_final=%s",namespace_final.c_str());
            
            RTPS_Camera_Info_Publisher.reset(new Fastrtps_PacketPublisher());
            RTPS_Camera_Info_Publisher->getParticipant(mp_participant);
            RTPS_Camera_Info_Publisher->init( "/"+namespace_final+"/camera_info" );
            RTPS_Camera_Info_Publisher_array.push_back(RTPS_Camera_Info_Publisher);
            camera_info_sub = nh.subscribe<sensor_msgs::CameraInfo>("/"+namespace_final+"/downward_cam/"+namespace_final+"/camera/camera_info",10,boost::bind(&Fastrtps_gazebo_send_receive::camera_info_callback,this,_1,i));
            camera_info_sub_vector.push_back(camera_info_sub);

            
            RTPS_image_raw_Publisher.reset(new Fastrtps_PacketPublisher());
            RTPS_image_raw_Publisher->getParticipant(mp_participant);
            RTPS_image_raw_Publisher->init( "/"+namespace_final+"/downward_cam/camera/image/compressed");
            RTPS_image_raw_Publisher_array.push_back(RTPS_image_raw_Publisher);
            image_raw_sub = nh.subscribe<sensor_msgs::CompressedImage>("/"+namespace_final+"/downward_cam/"+namespace_final+"/camera/compressed",10,boost::bind(&Fastrtps_gazebo_send_receive::image_raw_callback,this,_1,i));
            image_raw_sub_vector.push_back(image_raw_sub);

            //boost::shared_ptr<Fastrtps_PacketPublisher> RTPS_truth_Publisher(boost::make_shared<Fastrtps_PacketPublisher>());
            RTPS_truth_Publisher.reset(new Fastrtps_PacketPublisher());
            RTPS_truth_Publisher->getParticipant(mp_participant);
            RTPS_truth_Publisher->init( "/"+namespace_final+"/fastrtps_truth" );
            RTPS_truth_Publisher_vector.push_back(RTPS_truth_Publisher);
            //truth_sub = nh.subscribe<rosplane_msgs::State>("/"+namespace_final+"/truth",10,boost::bind(&Fastrtps_gazebo_send_receive::truth_callback,this,_1,i));
            truth_sub_ops =ros::SubscribeOptions::create<rosplane_msgs::State>(
                    "/"+namespace_final+"/truth", // topic name
                    10, // queue length
                    boost::bind(&Fastrtps_gazebo_send_receive::truth_callback,this,_1,i), // callback
                    ros::VoidPtr(), // tracked object, we don't need one thus NULL
                    &truth_queue // pointer to callback queue object
             );
            // subscribe
            truth_sub = nh.subscribe(truth_sub_ops);
            truth_sub_vector.push_back(truth_sub);
            truth_sub_ops_vector.push_back(truth_sub_ops);
        }

        // spawn async spinner with 10 thread, running on our custom queue
        async_spinner.reset(new ros::AsyncSpinner(10, &truth_queue));
        //async_spinner.AsyncSpinner(10, &truth_queue);
        // start the spinner
        async_spinner->start();


        
        nh_private_.param<double>("update_rate", update_rate_, 20.0);
        update_timer_ = nh.createTimer(ros::Duration(1.0 / update_rate_), &Fastrtps_gazebo_send_receive::truth_update,this);

        RTPS_obstacle_pub.getParticipant(mp_participant);       
        RTPS_obstacle_pub.init( "/fastrtps_obstacle" );
        ros::Subscriber obstacle_sub = nh.subscribe("/obstacle",10,&Fastrtps_gazebo_send_receive::obstacle_callback,this);

        RTPS_target_waypoint_pub.getParticipant(mp_participant);
        RTPS_target_waypoint_pub.init( "/fastrtps_target_waypoint" );
        ros::Subscriber target_waypoint_sub = nh.subscribe("/target_waypoint",10,&Fastrtps_gazebo_send_receive::target_waypoint_callback,this);

        RTPS_gathering_waypoint_pub.getParticipant(mp_participant);
        RTPS_gathering_waypoint_pub.init( "/fastrtps_gathering_waypoint" );
        ros::Subscriber gathering_waypoint_sub = nh.subscribe("/gathering_waypoint",10,&Fastrtps_gazebo_send_receive::gathering_waypoint_callback,this);

        RTPS_areacover_done_pub.getParticipant(mp_participant);
        RTPS_areacover_done_pub.init( "/fastrtps_areacover_done" );
        ros::Subscriber areacover_done_sub = nh.subscribe("/areacover_done",10,&Fastrtps_gazebo_send_receive::areacover_done_callback,this);

        RTPS_formation_type_pub.getParticipant(mp_participant);
        RTPS_formation_type_pub.init( "/fastrtps_formation_type" );
        ros::Subscriber formation_type_sub = nh.subscribe("/formation_type",10,&Fastrtps_gazebo_send_receive::formation_type_callback,this);

        //gazebo 需要接收的fastrtps信息 3种
        controller_commands_pub_vector.reserve(max_num);
        trajectory_pub_vector.reserve(max_num);
        RTPS_controller_commands_Subscriber_vector.reserve(max_num);
        RTPS_trajectory_Subscriber_vector.reserve(max_num);

        for(int i = 0; i < max_num; i++)
        {
            string namespace_final = pre_namespace + std::to_string(i+1);
            controller_commands_pub = nh.advertise<rosplane_msgs::Controller_Commands>("/"+namespace_final+"/controller_commands",1); 
            controller_commands_pub_vector.push_back(controller_commands_pub);
            RTPS_controller_commands_Subscriber.reset(new Fastrtps_PacketSubscriber);
            RTPS_controller_commands_Subscriber->getParticipant(mp_participant);
            if(RTPS_controller_commands_Subscriber->init( "/"+namespace_final+"/fastrtps_controller_commands"))
            {
               boost::function<void(const std::vector<uint8_t>&)> func1 = boost::bind(&Fastrtps_gazebo_send_receive::RTPS_controller_commands_callback,this, _1, i);
               RTPS_controller_commands_Subscriber->receive(func1);
               RTPS_controller_commands_Subscriber_vector.push_back(RTPS_controller_commands_Subscriber);
            }

            trajectory_pub = nh.advertise<nav_msgs::Path>("/"+namespace_final+"/trajectory",1);
            trajectory_pub_vector.push_back(trajectory_pub);
            RTPS_trajectory_Subscriber.reset(new Fastrtps_PacketSubscriber);
            RTPS_trajectory_Subscriber->getParticipant(mp_participant);
            if(RTPS_trajectory_Subscriber->init( "/fastrtps_trajectory" ))
            {
                boost::function<void(const std::vector<uint8_t>&)> func2 = boost::bind(&Fastrtps_gazebo_send_receive::RTPS_trajectory_callback,this, _1, i);
                RTPS_trajectory_Subscriber->receive(func2);
                RTPS_trajectory_Subscriber_vector.push_back(RTPS_trajectory_Subscriber);
            }

        }
    
        send_to_UTO_pub = nh.advertise<std_msgs::String>("/send_to_UTO",1);
        RTPS_send_to_UTO_Subscriber.getParticipant(mp_participant);
        if(RTPS_send_to_UTO_Subscriber.init( "/fastrtps_send_to_UTO"))
        {
            boost::function<void(const std::vector<uint8_t>&)> func3 = boost::bind(&Fastrtps_gazebo_send_receive::RTPS_send_to_UTO_callback,this, _1);
            RTPS_send_to_UTO_Subscriber.receive(func3);
        }
    }

    Fastrtps_gazebo_send_receive::~Fastrtps_gazebo_send_receive(){
        ROS_INFO("!!!!!!!!!!!!!!!!!!!!!!!!!!!! Fastrtps_gazebo_send_receive xigoule  ");
    }



    void Fastrtps_gazebo_send_receive::image_raw_callback(const sensor_msgs::CompressedImage::ConstPtr& msg, const int num)
    {
        std::vector<uint8_t> vec_data;
        vec_data = serialize_ros(*msg);
        RTPS_image_raw_Publisher_array[num]->DDS_pub(vec_data);
        vec_data.clear();	
    }

    void Fastrtps_gazebo_send_receive::camera_info_callback(const sensor_msgs::CameraInfo::ConstPtr& msg, const int num)
    {
        std::vector<uint8_t> vec_data;
        vec_data = serialize_ros(*msg);
        RTPS_Camera_Info_Publisher_array[num]->DDS_pub(vec_data);
        vec_data.clear();		
    }

    //truth 
    void Fastrtps_gazebo_send_receive::truth_callback(const rosplane_msgs::State::ConstPtr& msg,const int num)
    {
        //state_global = *msg;
        std::vector<uint8_t> vec_data;
        vec_data = serialize_ros(*msg);
        //int size_num0 = RTPS_truth_Publisher_vector[num].use_count();
        //ROS_INFO("*******************************pub ,size_num0=%d************************\n",size_num0);
        //std::string topicName = RTPS_truth_Publisher_vector[num]->getTopicName();
        //ROS_INFO("*******************************topicName=%s************************\n",topicName.c_str());
        if(RTPS_truth_Publisher_vector[num]->DDS_pub(vec_data)){
            //ROS_INFO("*******************************matched _gazebo_send_receive::fastrtps_init,num=%d************************\n",num);
        }//else{_gazebo_send_receive::fastrtps_init
            //ROS_INFO("*******************************  un  ma_gazebo_send_receive::fastrtps_inittched ,num=%d************************\n",num);
        //}
              
    }

    void Fastrtps_gazebo_send_receive::truth_update(const ros::TimerEvent &)
    {
        // 后期处理，是否可以传参
        //std::vector<uint8_t> vec_data;
        //vec_data = serialize_ros(state_global);
        //RTPS_truth_Publisher.DDS_pub(vec_data);

    }

    void Fastrtps_gazebo_send_receive::obstacle_callback(const rosplane_msgs::WaypointConstPtr& msg)
    {
        std::vector<uint8_t> obstacle_data;
        obstacle_data = serialize_ros(*msg);
        RTPS_obstacle_pub.DDS_pub(obstacle_data);
        //ROS_INFO("obstacle_data msg has been published by fastrtps in docker \n");
        obstacle_data.clear();
    }

    void Fastrtps_gazebo_send_receive::target_waypoint_callback(const rosplane_msgs::WaypointConstPtr& msg)
    {
        std::vector<uint8_t> target_waypoint_data;
        target_waypoint_data = serialize_ros(*msg);
        RTPS_target_waypoint_pub.DDS_pub(target_waypoint_data);
        //ROS_INFO("target_waypoint_data msg has been published by fastrtps in docker \n");
        target_waypoint_data.clear();
    }

    void Fastrtps_gazebo_send_receive::gathering_waypoint_callback(const rosplane_msgs::WaypointConstPtr& msg)
    {
        std::vector<uint8_t> gathering_waypoint_data;
        gathering_waypoint_data = serialize_ros(*msg);
        RTPS_gathering_waypoint_pub.DDS_pub(gathering_waypoint_data);
        //ROS_INFO("gathering_waypoint_data msg has been published by fastrtps in docker \n");
        gathering_waypoint_data.clear();
    }

    void Fastrtps_gazebo_send_receive::areacover_done_callback(const std_msgs::Empty& msg)
    {
        std::vector<uint8_t> areacover_done_data;
        areacover_done_data = serialize_ros(msg);
        RTPS_areacover_done_pub.DDS_pub(areacover_done_data);
        //ROS_INFO("RTPS_areacover_done_pub msg has been published by fastrtps in docker \n");
        areacover_done_data.clear();
    }

    void Fastrtps_gazebo_send_receive::formation_type_callback(const rosplane_msgs::FormationConstPtr& msg)
    {
        std::vector<uint8_t> formation_type_data;
        formation_type_data = serialize_ros(*msg);
        RTPS_formation_type_pub.DDS_pub(formation_type_data);
        //ROS_INFO("RTPS_formation_type_pub msg has been published by fastrtps in docker \n");
        formation_type_data.clear();
    }

    //controller_commands
    void Fastrtps_gazebo_send_receive::RTPS_controller_commands_callback(std::vector<uint8_t> msg , int num)
    {
        rosplane_msgs::Controller_Commands info;
        info = deserialize_ros<rosplane_msgs::Controller_Commands>(msg);
        controller_commands_pub_vector[num].publish(info);
    }

    //trajectory
    void Fastrtps_gazebo_send_receive::RTPS_trajectory_callback(std::vector<uint8_t> msg , int num)
    {
        nav_msgs::Path info;
        info = deserialize_ros<nav_msgs::Path>(msg);
        trajectory_pub_vector[num].publish(info);
    }

    //send_to_UTO
    void Fastrtps_gazebo_send_receive::RTPS_send_to_UTO_callback(std::vector<uint8_t> msg)
    {
        std_msgs::String info;
        info = deserialize_ros<std_msgs::String>(msg);
        send_to_UTO_pub.publish(info);
    }
}

    int main(int argc, char **argv)
    {
        std::cout << "fastrtps_pub_sub started in gazebo" << std::endl;
        ros::init(argc,argv,"fastrtps_gazebosend_receive");

        Fastrtps_PacketPublisher RTPS_Publisher;
        RTPS_Publisher.createRTPSParticipant(mp_participant,99,"Participant_information");
	    Fastrtps_PacketPubSubType myType;
	    Domain::registerType(mp_participant, static_cast<TopicDataType*>(&myType));

        udp_commu::Fastrtps_gazebo_send_receive gazebosend_receive;
        gazebosend_receive.fastrtps_init();
        ros::spin();   
        return 0;
    }

