/*******************************
 *
 *  This udp_gazebo_send_receive.cpp is finished by YAN JIE. .
 * 
 **************************/
#include <iostream>
#include <ros/ros.h>

#include "fastrtps_pubsub/Fastrtps_PacketPublisher.h"
#include "fastrtps_pubsub/Fastrtps_PacketSubscriber.h"
#include "fastrtps_pubsub/Fastrtps_PacketPubSubTypes.h"
//#include "fastrtps_pubsub/Fastrtps_PacketPartcipent.h"



#include <fastrtps/Domain.h>
#include <fastrtps/utils/eClock.h>
#include <fastrtps/log/Log.h>

#include <sensor_msgs/Image.h>
#include <sensor_msgs/CompressedImage.h>
#include <nav_msgs/Path.h>
#include <std_msgs/String.h>


#include "ros/ros.h"
//#include "std_msgs/String.h"
#include <rosplane_msgs/State.h>
#include <rosplane_msgs/Command.h>
#include <rosplane_msgs/Controller_Commands.h>
#include <rosplane_msgs/Controller_Internals.h>
#include <rosplane_msgs/MultiVaGoal.h>
#include <rosplane_msgs/Path.h>
#include <rosplane_msgs/VirtualAgent.h>
#include <nav_msgs/Odometry.h>

#include "udp_commu/Down_Data_Speed_Nav.h"
#include <geometry_msgs/Twist.h>
#include "udp_commu/Gazebo_State.h"
#include <udp_commu/Heart_Beat.h>



#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

//串口相关的头文件  
#include<stdio.h>      /*标准输入输出定义*/  
#include<stdlib.h>     /*标准函数库定义*/  
#include<unistd.h>     /*Unix 标准函数定义*/  
#include<sys/types.h>   
#include<sys/stat.h>     
#include<fcntl.h>      /*文件控制定义*/  
#include<termios.h>    /*PPSIX 终端控制定义*/  
#include<errno.h>      /*错误号定义*/  
#include<string.h>  
#include <math.h>
#include <boost/thread/thread.hpp>

using std::string;
using namespace eprosima;
using namespace eprosima::fastrtps;
eprosima::fastrtps::Participant *mp_participant = NULL;

Fastrtps_PacketPublisher RTPS_car_Publisher;

template<class T>
std::vector<uint8_t> serialize_ros(T t)
{
    std::vector<uint8_t> vec;
    uint32_t serial_size = ros::serialization::serializationLength(t);
    boost::shared_array<uint8_t> buffer(new uint8_t[serial_size]);
    ros::serialization::OStream ostream(buffer.get(), serial_size);
    ros::serialization::serialize(ostream, t);
    vec.resize(serial_size);
    std::copy(buffer.get(), buffer.get() + serial_size, vec.begin());
    return vec;
}

template<class T>
T deserialize_ros(const std::vector<uint8_t>& vec)
{
    T t;
    uint32_t serial_size = vec.size();
    std::vector<uint8_t> buffer(serial_size);
    std::copy(vec.begin(), vec.begin() + serial_size, buffer.begin());
    ros::serialization::IStream istream(buffer.data(), serial_size);
    ros::serialization::Serializer<T>::read(istream, t);
    return t;
}

void car_callback(const nav_msgs::Odometry::ConstPtr& msg)
{
    std::vector<uint8_t> vec_data;
    vec_data = serialize_ros(*msg);
    RTPS_car_Publisher.DDS_pub(vec_data);
 
}

int main(int argc, char **argv)
{
    std::cout << "fastrtps_car_pub_sub started in gazebo" << std::endl;
    ros::init(argc,argv,"fastrtps_gazebo_car_send");
		
	ros::NodeHandle nh_private_("~");
    ros::NodeHandle nh;
    //Fastrtps_PacketPartcipent partcipent;
    //partcipent.createParticipant("taiic_information_docker",99);// name, domain
    Fastrtps_PacketPublisher RTPS_Publisher;
    RTPS_Publisher.createRTPSParticipant(mp_participant,99,"Participant_information");
	Fastrtps_PacketPubSubType myType;
	Domain::registerType(mp_participant, static_cast<TopicDataType*>(&myType));

    RTPS_car_Publisher.getParticipant(mp_participant); 
    RTPS_car_Publisher.init("/fastrtps_enemy_ground_truth_state" );
    ros::Subscriber car_sub = nh.subscribe("/enemy/ground_truth/state",10,car_callback);

    ros::spin();
    Domain::stopAll();
    Log::Reset();
    return 0;
}

