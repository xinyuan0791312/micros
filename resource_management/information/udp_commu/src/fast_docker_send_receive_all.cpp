/*************************************************************
 *
 *  This fast_docker_send_receive.cpp is finished by YAN JIE. .
 *   Modify by yyt 2019-09-20
 *************************************************************/
#include <iostream>
#include <ros/ros.h>
#include "fastrtps_pubsub/Fastrtps_PacketPublisher.h"
#include "fastrtps_pubsub/Fastrtps_PacketSubscriber.h"
#include "fastrtps_pubsub/Fastrtps_PacketPubSubTypes.h"
//#include "fastrtps_pubsub/Fastrtps_PacketPartcipent.h"
#include <fastrtps/Domain.h>
#include <fastrtps/utils/eClock.h>
#include <fastrtps/log/Log.h>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/CompressedImage.h>
#include <sensor_msgs/CameraInfo.h>
#include "std_msgs/String.h"
#include <rosplane_msgs/State.h>
#include <rosplane_msgs/State_Path.h>
#include <rosplane_msgs/Command.h>
#include <rosplane_msgs/Controller_Commands.h>
#include <rosplane_msgs/Controller_Internals.h>
#include <rosplane_msgs/Waypoint.h>
#include <rosplane_msgs/Formation.h>
#include <rosplane_msgs/Takeoff_Command.h>
#include <nav_msgs/Path.h>
#include <nav_msgs/Odometry.h>
#include <geometry_msgs/Twist.h>
#include <udp_commu/Heart_Beat.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
  
#include<stdio.h>  
#include<stdlib.h>   
#include<unistd.h>  
#include<sys/types.h>   
#include<sys/stat.h>     
#include<fcntl.h>   
#include<termios.h>     
#include<errno.h>     
#include<string.h>  
#include <math.h>
#include <boost/thread/thread.hpp>
#include <std_msgs/Empty.h>

using namespace eprosima;
using namespace eprosima::fastrtps;
eprosima::fastrtps::Participant *mp_participant = NULL;
 
uint64_t getCurrentTime()
{
    struct timeval tv;
    gettimeofday(&tv,NULL);
    return tv.tv_sec * 1000 + tv.tv_usec / 1000;
}

ros::Publisher state_pub;
ros::Publisher compressed_image_pub;
ros::Publisher watchdog_pub;	
ros::Publisher truth_pub;
ros::Publisher goal_pub;
ros::Publisher Leader_state_path_pub;
ros::Publisher s_area_vertex_pub;
ros::Publisher v_area_vertex_pub;
ros::Publisher assemble_goals_pub;
ros::Publisher car_pub;
ros::Publisher camera_info_pub;
ros::Publisher obstacle_pub;
ros::Publisher target_waypoint_pub;
ros::Publisher gathering_waypoint_pub;
ros::Publisher areacover_done_pub;
ros::Publisher formation_type_pub;
ros::Publisher gathering_reached_pub;
ros::Publisher Take_off_pub;
ros::Publisher Leader_state_pub;

Fastrtps_PacketPublisher RTPS_goal_Publisher;
Fastrtps_PacketPublisher RTPS_trajectory_Publisher;
Fastrtps_PacketPublisher RTPS_cmd_vel_Publisher;
Fastrtps_PacketPublisher RTPS_controller_commands_Publisher;
Fastrtps_PacketPublisher RTPS_Take_off_Publisher;
Fastrtps_PacketPublisher RTPS_Leader_state_Publisher;
Fastrtps_PacketPublisher RTPS_state_path_Publisher;
Fastrtps_PacketPublisher RTPS_send_to_UTO_Publisher;
Fastrtps_PacketPublisher RTPS_gathering_reached_Publisher;
Fastrtps_PacketPublisher RTPS_areacover_done_Publisher;

int count_receive_state = 0;
uint64_t starttime;
uint64_t endtime;

std::string namespace_; 

template<class T>
std::vector<uint8_t> serialize_ros(T t)
{
    std::vector<uint8_t> vec;
    uint32_t serial_size = ros::serialization::serializationLength(t);
    boost::shared_array<uint8_t> buffer(new uint8_t[serial_size]);
    ros::serialization::OStream ostream(buffer.get(), serial_size);
    ros::serialization::serialize(ostream, t);
    vec.resize(serial_size);
    std::copy(buffer.get(), buffer.get() + serial_size, vec.begin());
    return vec;
}

template<class T>
T deserialize_ros(const std::vector<uint8_t>& vec)
{
    T t;
    uint32_t serial_size = vec.size();
    std::vector<uint8_t> buffer(serial_size);
    std::copy(vec.begin(), vec.begin() + serial_size, buffer.begin());
    ros::serialization::IStream istream(buffer.data(), serial_size);
    ros::serialization::Serializer<T>::read(istream, t);
    return t;
}

void cmd_vel_callback(const geometry_msgs::Twist::ConstPtr& msg)
{
	std::vector<uint8_t> vec_data;
	vec_data = serialize_ros(*msg);
	RTPS_cmd_vel_Publisher.DDS_pub(vec_data);
	vec_data.clear();
}

void RTPS_state_callback(std::vector<uint8_t> msg)
{
    rosplane_msgs::State state;
	state = deserialize_ros<rosplane_msgs::State>(msg);
	if(state_pub == NULL)
    {
        ros::NodeHandle nh;
        state_pub = nh.advertise<rosplane_msgs::State>("/cur_est_state",1);        
    }
    state_pub.publish(state);
    
    count_receive_state++;
    if(1 == count_receive_state)
    {
        starttime = getCurrentTime();
    }

    if(10001 == count_receive_state)
    {
        endtime = getCurrentTime();
        udp_commu::Heart_Beat watchdog_msg;
        watchdog_msg.status = "";
        char error_state[100];
        //sprintf(error_state, "Time to receive 10000 state pkt is: %llu ms ", endtime - starttime);
        watchdog_msg.status += error_state;
        if(watchdog_pub == NULL)
        {
            ros::NodeHandle nh;
            watchdog_pub = nh.advertise<udp_commu::Heart_Beat>("/jkwatchdog", 1);      
        }               
        watchdog_pub.publish(watchdog_msg);
        count_receive_state = 0;
    }
}

void RTPS_image_compressed_callback(std::vector<uint8_t> msg)
{
    sensor_msgs::CompressedImage image;
	image = deserialize_ros<sensor_msgs::CompressedImage>(msg);

	if(compressed_image_pub == NULL)
    {
        ros::NodeHandle nh;
        compressed_image_pub = nh.advertise<sensor_msgs::CompressedImage>("/downward_cam/camera/image/compressed", 1);      
    }
    compressed_image_pub.publish(image);
}

void RTPS_camera_info_callback(std::vector<uint8_t> msg)
{
    sensor_msgs::CameraInfo camera_info;
        camera_info = deserialize_ros<sensor_msgs::CameraInfo>(msg);
        if(camera_info_pub == NULL)
    {
        ros::NodeHandle nh;
        camera_info_pub = nh.advertise<sensor_msgs::CameraInfo>("/camera_info", 1);
    }
    camera_info_pub.publish(camera_info);
}

void RTPS_Take_off_callback(std::vector<uint8_t> msg)
{
    rosplane_msgs::Takeoff_Command info;
    info = deserialize_ros<rosplane_msgs::Takeoff_Command>(msg);
  
    if(Take_off_pub == NULL)
    {
        ros::NodeHandle nh;
        Take_off_pub = nh.advertise<rosplane_msgs::Takeoff_Command>("/takeoff_commands_f",1);        
    }
    Take_off_pub.publish(info);
}

void RTPS_truth_callback(std::vector<uint8_t> msg)
{
    rosplane_msgs::State info;
    info = deserialize_ros<rosplane_msgs::State>(msg);
  
    if(truth_pub == NULL)
    {
        ros::NodeHandle nh;
        truth_pub = nh.advertise<rosplane_msgs::State>("/"+namespace_+"/truth",1);        
    }
    truth_pub.publish(info);
}

void RTPS_obstacle_callback(std::vector<uint8_t> msg)
{
    rosplane_msgs::Waypoint info;
    info = deserialize_ros<rosplane_msgs::Waypoint>(msg);
  
    if(obstacle_pub == NULL)
    {
        ros::NodeHandle nh;
        obstacle_pub = nh.advertise<rosplane_msgs::Waypoint>("/obstacle",1);        
    }
    obstacle_pub.publish(info);
}

void RTPS_target_waypoint_callback(std::vector<uint8_t> msg)
{
    rosplane_msgs::Waypoint info;
    info = deserialize_ros<rosplane_msgs::Waypoint>(msg);
  
    if(target_waypoint_pub == NULL)
    {
        ros::NodeHandle nh;
        target_waypoint_pub = nh.advertise<rosplane_msgs::Waypoint>("/target_waypoint",1);        
    }
    target_waypoint_pub.publish(info);
}

void RTPS_gathering_waypoint_callback(std::vector<uint8_t> msg)
{
    rosplane_msgs::Waypoint info;
    info = deserialize_ros<rosplane_msgs::Waypoint>(msg);
  
    if(gathering_waypoint_pub == NULL)
    {
        ros::NodeHandle nh;
        gathering_waypoint_pub = nh.advertise<rosplane_msgs::Waypoint>("/gathering_waypoint",1);        
    }
    gathering_waypoint_pub.publish(info);
}

void RTPS_areacover_done_callback(std::vector<uint8_t>  msg)
{
    std_msgs::Empty info;
    if(areacover_done_pub == NULL)
    {
        ros::NodeHandle nh;
        areacover_done_pub = nh.advertise<std_msgs::Empty>("/areacover_done_f",1);
    }
    areacover_done_pub.publish(info);
}

void RTPS_formation_type_callback(std::vector<uint8_t> msg)
{
    rosplane_msgs::Formation info;
    info = deserialize_ros<rosplane_msgs::Formation>(msg);
  
    if(formation_type_pub == NULL)
    {
        ros::NodeHandle nh;
        formation_type_pub = nh.advertise<rosplane_msgs::Formation>("/formation_type",1);        
    }
    formation_type_pub.publish(info);
}

void RTPS_gathering_reached_callback(std::vector<uint8_t> msg)
{
    rosplane_msgs::Formation info;
    info = deserialize_ros<rosplane_msgs::Formation>(msg);
  
    if(gathering_reached_pub == NULL)
    {
        ros::NodeHandle nh;
        gathering_reached_pub = nh.advertise<rosplane_msgs::Formation>("/gathering_reached_f",1);        
    }
    gathering_reached_pub.publish(info);
}

void RTPS_s_area_vertex_callback(std::vector<uint8_t> msg)
{
    std_msgs::String info;
    info = deserialize_ros<std_msgs::String>(msg);
  
    if(s_area_vertex_pub == NULL)
    {
        ros::NodeHandle nh;
        s_area_vertex_pub = nh.advertise<std_msgs::String>("/s_area_vertex",1);        
    }
    s_area_vertex_pub.publish(info);
}

// ***********102  接受地面站******************
// void RTPS_v_area_vertex_callback(std::vector<uint8_t> msg)
// {
//     va_msgs::AreaVertex info;
//     info = deserialize_ros<va_msgs::AreaVertex>(msg);
  
//     if(v_area_vertex_pub == NULL)
//     {
//         ros::NodeHandle nh;
//         v_area_vertex_pub = nh.advertise<va_msgs::AreaVertex>("/v_area_vertex",10);        
//     }
//     v_area_vertex_pub.publish(info);
// }

void RTPS_assemble_goals_callback(std::vector<uint8_t> msg)
{
    std_msgs::String info;
    info = deserialize_ros<std_msgs::String>(msg);
  
    if(assemble_goals_pub == NULL)
    {
        ros::NodeHandle nh;
        assemble_goals_pub = nh.advertise<std_msgs::String>("/assemble_goals",1);        
    }
    assemble_goals_pub.publish(info);
}

void RTPS_goal_callback(std::vector<uint8_t> msg)
{
    std_msgs::String info;
    info = deserialize_ros<std_msgs::String>(msg);
  
    if(goal_pub == NULL)
    {
        ros::NodeHandle nh;
        goal_pub = nh.advertise<std_msgs::String>("/goal_f",1);        
    }
    goal_pub.publish(info);
}

void sub_goal_callback(const std_msgs::String::ConstPtr& msg)
{
    std::vector<uint8_t> vec_data;
    vec_data = serialize_ros(*msg);
    RTPS_goal_Publisher.DDS_pub(vec_data);
}

void sub_areacover_done_callback(const std_msgs::Empty::ConstPtr& msg)
{
    std::vector<uint8_t> vec_data;
    vec_data = serialize_ros(*msg);
    RTPS_areacover_done_Publisher.DDS_pub(vec_data);
}

void sub_state_path_callback(const rosplane_msgs::State_Path::ConstPtr& msg)
{
    std::vector<uint8_t> vec_data;
    vec_data = serialize_ros(*msg);
    RTPS_state_path_Publisher.DDS_pub(vec_data);
}

void sub_gathering_reached_callback(const rosplane_msgs::Formation::ConstPtr& msg)
{
    std::vector<uint8_t> vec_data;
    vec_data = serialize_ros(*msg);
    RTPS_gathering_reached_Publisher.DDS_pub(vec_data);
}

void sub_controller_commands_callback(const rosplane_msgs::Controller_Commands::ConstPtr& msg)
{
    std::vector<uint8_t> vec_data;
    vec_data = serialize_ros(*msg);
    RTPS_controller_commands_Publisher.DDS_pub(vec_data);
}

//trajectory
void sub_trajectory_callback(const nav_msgs::Path::ConstPtr& msg)
{
    std::vector<uint8_t> vec_data;
    vec_data = serialize_ros(*msg);
    RTPS_trajectory_Publisher.DDS_pub(vec_data);
}

void sub_Take_off_callback(const rosplane_msgs::Takeoff_Command::ConstPtr& msg)
{
    std::vector<uint8_t> vec_data;
    vec_data = serialize_ros(*msg);
    RTPS_Take_off_Publisher.DDS_pub(vec_data);
}

//leader to follower
void RTPS_Leader_state_callback(std::vector<uint8_t> msg)
{
    rosplane_msgs::State info;
    info = deserialize_ros<rosplane_msgs::State>(msg);
  
    if(Leader_state_pub == NULL)
    {
        ros::NodeHandle nh;
        Leader_state_pub = nh.advertise<rosplane_msgs::State>("/Leader_state_f",10);        
    }
    Leader_state_pub.publish(info);
}

void RTPS_state_path_callback(std::vector<uint8_t> msg)
{
    rosplane_msgs::State_Path info;
    info = deserialize_ros<rosplane_msgs::State_Path>(msg);
  
    if(Leader_state_path_pub == NULL)
    {
        ros::NodeHandle nh;
        Leader_state_path_pub = nh.advertise<rosplane_msgs::State_Path>("/leader/state_path_f",1);        
    }
    Leader_state_path_pub.publish(info);
}

void sub_Leader_state_callback(const rosplane_msgs::State::ConstPtr& msg)
{
    std::vector<uint8_t> vec_data;
    vec_data = serialize_ros(*msg);
    RTPS_Leader_state_Publisher.DDS_pub(vec_data);
}

//UTO
void sub_send_to_UTO_callback(const std_msgs::String::ConstPtr& msg)
{
    std::vector<uint8_t> vec_data;
    vec_data = serialize_ros(*msg);
    RTPS_send_to_UTO_Publisher.DDS_pub(vec_data);
}

//car
void RTPS_car_callback(std::vector<uint8_t> msg)
{
    nav_msgs::Odometry info;
    info = deserialize_ros<nav_msgs::Odometry>(msg);
    car_pub.publish(info);
}

int main(int argc, char **argv)
{
    std::cout << "fastrtps_pub_sub started in docker" << std::endl;
    ros::init(argc,argv,"fastrtps_dockersend_receive");
		
	ros::NodeHandle n;
    n.param<std::string>("namespace", namespace_, "robot_0");

    Fastrtps_PacketPublisher RTPS_Publisher;
    RTPS_Publisher.createRTPSParticipant(mp_participant,99,"Participant_information");
	Fastrtps_PacketPubSubType myType;
	Domain::registerType(mp_participant, static_cast<TopicDataType*>(&myType));

    Fastrtps_PacketSubscriber RTPS_image_raw_Subscriber;
    RTPS_image_raw_Subscriber.getParticipant(mp_participant);
	if(RTPS_image_raw_Subscriber.init("/"+namespace_+"/downward_cam/camera/image/compressed" ))
    {
    	RTPS_image_raw_Subscriber.receive(&RTPS_image_compressed_callback);
    }

    Fastrtps_PacketSubscriber RTPS_camera_info_Subscriber;
    RTPS_camera_info_Subscriber.getParticipant(mp_participant);
    if(RTPS_camera_info_Subscriber.init("/"+namespace_+"/camera_info"))
    {
        RTPS_camera_info_Subscriber.receive(&RTPS_camera_info_callback);
    }

    Fastrtps_PacketSubscriber RTPS_truth_Subscriber;
    RTPS_truth_Subscriber.getParticipant(mp_participant); 
    if(RTPS_truth_Subscriber.init("/"+namespace_+"/fastrtps_truth" )) 
    {
        RTPS_truth_Subscriber.receive(&RTPS_truth_callback);
    }

    Fastrtps_PacketSubscriber RTPS_s_area_vertex_Subscriber; 
    RTPS_s_area_vertex_Subscriber.getParticipant(mp_participant); 
    if(RTPS_s_area_vertex_Subscriber.init("/fastrtps_s_area_vertex")) 
    {
        RTPS_s_area_vertex_Subscriber.receive(&RTPS_s_area_vertex_callback);
    }

    // Fastrtps_PacketSubscriber RTPS_v_area_vertex_Subscriber; //
    // if(RTPS_v_area_vertex_Subscriber.init(70002,"/fastrtps_v_area_vertex")) 
    // {
    //     RTPS_v_area_vertex_Subscriber.receive(&RTPS_v_area_vertex_callback);
    // }

    Fastrtps_PacketSubscriber RTPS_assemble_goals_Subscriber; 
    RTPS_assemble_goals_Subscriber.getParticipant(mp_participant);
    if(RTPS_assemble_goals_Subscriber.init("/fastrtps_assemble_goals")) 
    {
        RTPS_assemble_goals_Subscriber.receive(&RTPS_assemble_goals_callback);
    }

    Fastrtps_PacketSubscriber RTPS_goal_Subscriber; 
    RTPS_goal_Subscriber.getParticipant(mp_participant);
    if(RTPS_goal_Subscriber.init("/fastrtps_goal")) 
    {
        RTPS_goal_Subscriber.receive(&RTPS_goal_callback);
    }

    Fastrtps_PacketSubscriber RTPS_state_path_Subscriber; 
    RTPS_state_path_Subscriber.getParticipant(mp_participant);
    if(RTPS_state_path_Subscriber.init("/fastrtps_state_path")) 
    {
        RTPS_state_path_Subscriber.receive(&RTPS_state_path_callback);
    }

    ros::Subscriber goal_sub = n.subscribe("/goal",10,sub_goal_callback);
    RTPS_goal_Publisher.getParticipant(mp_participant);
    RTPS_goal_Publisher.init("/fastrtps_goal");

    ros::Subscriber areacover_done_sub = n.subscribe("/areacover_done",10,sub_areacover_done_callback);
    RTPS_areacover_done_Publisher.getParticipant(mp_participant);
    RTPS_areacover_done_Publisher.init("/fastrtps_areacover_done");


    ros::Subscriber state_path_sub = n.subscribe("/leader/state_path",10,sub_state_path_callback);
    RTPS_state_path_Publisher.getParticipant(mp_participant);
    RTPS_state_path_Publisher.init("/fastrtps_state_path");

    Fastrtps_PacketSubscriber RTPS_Take_off_Subscriber;
    RTPS_Take_off_Subscriber.getParticipant(mp_participant);
    if(RTPS_Take_off_Subscriber.init("/fastrtps_takeoff_commands")) 
    {
        RTPS_Take_off_Subscriber.receive(&RTPS_Take_off_callback);
    }
 
    ros::Subscriber Take_off_sub = n.subscribe("/takeoff_commands",10,sub_Take_off_callback);
    RTPS_Take_off_Publisher.getParticipant(mp_participant);
    RTPS_Take_off_Publisher.init("/fastrtps_takeoff_commands");

    ros::Subscriber controller_commands_sub = n.subscribe("/"+namespace_+"/controller_commands",10,sub_controller_commands_callback);
    RTPS_controller_commands_Publisher.getParticipant(mp_participant);
    RTPS_controller_commands_Publisher.init("/"+namespace_+"/fastrtps_controller_commands" );

    RTPS_trajectory_Publisher.getParticipant(mp_participant);
    RTPS_trajectory_Publisher.init("/fastrtps_trajectory");
    ros::Subscriber trajectory_sub = n.subscribe("/"+namespace_+"/trajectory",10,sub_trajectory_callback);
    
    Fastrtps_PacketSubscriber RTPS_Leader_state_Subscriber;
    RTPS_Leader_state_Subscriber.getParticipant(mp_participant);
    if(RTPS_Leader_state_Subscriber.init("/fastrtps_Leader_state")) 
    {
        RTPS_Leader_state_Subscriber.receive(&RTPS_Leader_state_callback);
    }

    ros::Subscriber Leader_state_sub = n.subscribe("/Leader_state",10,sub_Leader_state_callback);
    RTPS_Leader_state_Publisher.getParticipant(mp_participant);
    RTPS_Leader_state_Publisher.init("/fastrtps_Leader_state");
    
    ros::Subscriber send_to_UTO_sub = n.subscribe("/send_to_UTO",10,sub_send_to_UTO_callback);
    RTPS_send_to_UTO_Publisher.getParticipant(mp_participant);
    RTPS_send_to_UTO_Publisher.init("/fastrtps_send_to_UTO");

    car_pub = n.advertise<nav_msgs::Odometry>("/enemy/ground_truth/state",1);  
    Fastrtps_PacketSubscriber RTPS_car_Subscriber; 
    RTPS_car_Subscriber.getParticipant(mp_participant);
    if(RTPS_car_Subscriber.init("/fastrtps_enemy_ground_truth_state")) 
    {
        RTPS_car_Subscriber.receive(&RTPS_car_callback);
    }

    Fastrtps_PacketSubscriber RTPS_obstacle_Subscriber; 
    RTPS_obstacle_Subscriber.getParticipant(mp_participant);
    if(RTPS_obstacle_Subscriber.init("/fastrtps_obstacle")) 
    {
        RTPS_obstacle_Subscriber.receive(&RTPS_obstacle_callback);
    }

    Fastrtps_PacketSubscriber RTPS_target_waypoint_Subscriber;
    RTPS_target_waypoint_Subscriber.getParticipant(mp_participant); 
    if(RTPS_target_waypoint_Subscriber.init("/fastrtps_target_waypoint")) 
    {
        RTPS_target_waypoint_Subscriber.receive(&RTPS_target_waypoint_callback);
    }

    Fastrtps_PacketSubscriber RTPS_gathering_waypoint_Subscriber; 
    RTPS_gathering_waypoint_Subscriber.getParticipant(mp_participant);
    if(RTPS_gathering_waypoint_Subscriber.init("/fastrtps_gathering_waypoint")) 
    {
        RTPS_gathering_waypoint_Subscriber.receive(&RTPS_gathering_waypoint_callback);
    }

    Fastrtps_PacketSubscriber RTPS_areacover_done_Subscriber; 
    RTPS_areacover_done_Subscriber.getParticipant(mp_participant);
    if(RTPS_areacover_done_Subscriber.init("/fastrtps_areacover_done")) 
    {
        RTPS_areacover_done_Subscriber.receive(&RTPS_areacover_done_callback);
    }

    Fastrtps_PacketSubscriber RTPS_formation_type_Subscriber; 
    RTPS_formation_type_Subscriber.getParticipant(mp_participant);
    if(RTPS_formation_type_Subscriber.init("/fastrtps_formation_type")) 
    {
        RTPS_formation_type_Subscriber.receive(&RTPS_formation_type_callback);
    }

    Fastrtps_PacketSubscriber RTPS_gathering_reached_Subscriber; 
    RTPS_gathering_reached_Subscriber.getParticipant(mp_participant);
    if(RTPS_gathering_reached_Subscriber.init("/fastrtps_gathering_reached")) 
    {
        RTPS_gathering_reached_Subscriber.receive(&RTPS_gathering_reached_callback);
    }

    ros::Subscriber gathering_reached_sub = n.subscribe("/gathering_reached",10,sub_gathering_reached_callback);
    RTPS_gathering_reached_Publisher.getParticipant(mp_participant);
    RTPS_gathering_reached_Publisher.init("/fastrtps_gathering_reached");

    ros::spin();
    Domain::stopAll();
    Log::Reset();
    return 0;
}
