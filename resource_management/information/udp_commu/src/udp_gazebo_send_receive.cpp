/*******************************
 *
 *  This udp_gazebo_send_receive.cpp is finished by YAN JIE. .
 * 
 **************************/
#include "udp_commu/udp_gazebo_send_receive.h"

using std::string;

namespace udp_commu 
{

	using std::string;

	template<class T>
	std::vector<uint8_t> serialize_ros(T t)
	{
		std::vector<uint8_t> vec;
		uint32_t serial_size = ros::serialization::serializationLength(t);
		boost::shared_array<uint8_t> buffer(new uint8_t[serial_size]);
		ros::serialization::OStream ostream(buffer.get(), serial_size);
		ros::serialization::serialize(ostream, t);
		vec.resize(serial_size);
		std::copy(buffer.get(), buffer.get() + serial_size, vec.begin());
		return vec;
	}

	template<class T>
	T deserialize_ros(const std::vector<uint8_t>& vec)
	{
		T t;
		uint32_t serial_size = vec.size();
		std::vector<uint8_t> buffer(serial_size);
		std::copy(vec.begin(), vec.begin() + serial_size, buffer.begin());
		ros::serialization::IStream istream(buffer.data(), serial_size);
		ros::serialization::Serializer<T>::read(istream, t);
		return t;
	}


	// 1- Udp_commu_gazebo_send

	Udp_commu_gazebo_send::Udp_commu_gazebo_send()
	{
		string dockerleader_ip = "10.10.10.64";
		n.param("dockerleader_ipaddress", dockerleader_ipaddress_, dockerleader_ip);

		string docker_ns = "robot_1";
		n.param("namespace", namespace_, docker_ns);

		
		memset(&remote_addr,0,sizeof(remote_addr)); //数据初始化--清零
		remote_addr.sin_family=AF_INET; //设置为IP通信
		remote_addr.sin_addr.s_addr=inet_addr(dockerleader_ipaddress_.c_str());//服务器(每个docker)的IP地址
		remote_addr.sin_port=htons(8000); //服务器端口号

		/*创建客户端套接字--IPv4协议，面向无连接通信，UDP协议*/
		if((client_sockfd=socket(PF_INET,SOCK_DGRAM,0))<0)
		{  
			perror("socket error");
		}
		sin_size=sizeof(struct sockaddr_in);

		int snd_size;    /* 发送缓冲区大小为240K */ 
		socklen_t optlen = sizeof(snd_size); 

		int	err = getsockopt(client_sockfd, SOL_SOCKET, SO_SNDBUF,&snd_size, &optlen); 
		if(err<0)
		{ 
			printf("获取发送缓冲区大小错误\n"); 
		}   					
		printf(" 发送缓冲区原始大小为: %d 字节\n",snd_size); 

		//1-truth
		truth_sub_ = n.subscribe("/"+namespace_+"/truth", 10, &Udp_commu_gazebo_send::truthSendCallback, this);
		//2-car
		car_sub_ = n.subscribe("/enemy/ground_truth/state", 10, &Udp_commu_gazebo_send::carSendCallback, this);
		//3-obstacle
		obstacle_sub_ = n.subscribe("/obstacle", 10, &Udp_commu_gazebo_send::obstacleSendCallback, this);
		//4-target_waypoint
		target_waypoint_sub_ = n.subscribe("/target_waypoint", 10, &Udp_commu_gazebo_send::target_waypointSendCallback, this);
		//5-gathering_waypoint
		gathering_waypoint_sub_ = n.subscribe("/gathering_waypoint", 10, &Udp_commu_gazebo_send::gathering_waypointSendCallback, this);
		//6-areacover_done
		areacover_done_sub_ = n.subscribe("/areacover_done", 10, &Udp_commu_gazebo_send::areacover_doneSendCallback, this);
		//7-gathering_waypoint
		formation_type_sub_ = n.subscribe("/formation_type", 10, &Udp_commu_gazebo_send::formation_typeSendCallback, this);

		total_count = 0;
        truth_count = 0;
        car_count = 0;
        obstacle_count = 0;
        target_waypoint_count = 0;
        gathering_waypoint_count = 0;
        areacover_done_count = 0;
        formation_type_count = 0;

		truth_num = 0;
        car_num = 0;
        obstacle_num = 0;
        target_waypoint_num = 0;
        gathering_waypoint_num = 0;
        areacover_done_num = 0;
        formation_type_num = 0;
	}

	Udp_commu_gazebo_send::~Udp_commu_gazebo_send()
	{	
	}

	void Udp_commu_gazebo_send::truthSendCallback(const rosplane_msgs::State::ConstPtr& msg)
	{
		udp_commu::UdpCommPacket packet_msg;
		std::vector<uint8_t> vec_data_temp;
    	vec_data_temp = serialize_ros(*msg);
		packet_msg.name =" truth";
		packet_msg.data = vec_data_temp;
		packet_msg.num = ++truth_num;

		std::vector<uint8_t> vec_data;
    	vec_data = serialize_ros(packet_msg);

		int data_size = vec_data.size();

		char *data = new char[data_size];
		for(int i = 0; i < data_size; i++) {
			*(data + i) = (char)(vec_data[i]);
		}

		int len=sendto(client_sockfd, data, data_size, 0, (struct sockaddr *)&remote_addr, sizeof(struct sockaddr));
		if(len > 0){
			total_count++;
			truth_count ++;
			print_count();
			//printf(" %d byte truth data send by Gazebo successfully\n",len); 
		}  
		else  
			printf("Gazebo send truth data failed!\n"); 
	}

	void Udp_commu_gazebo_send::carSendCallback(const nav_msgs::Odometry::ConstPtr& msg)
	{
		udp_commu::UdpCommPacket packet_msg;
		std::vector<uint8_t> vec_data_temp;
    	vec_data_temp = serialize_ros(*msg);
		packet_msg.name =" car";
		packet_msg.data = vec_data_temp;
		packet_msg.num = ++car_num;

		std::vector<uint8_t> vec_data;
    	vec_data = serialize_ros(packet_msg);

		int data_size = vec_data.size();

		char *data = new char[data_size];
		for(int i = 0; i < data_size; i++) {
			*(data + i) = (char)(vec_data[i]);
		}

		int len=sendto(client_sockfd, data, data_size, 0, (struct sockaddr *)&remote_addr, sizeof(struct sockaddr));
		if(len > 0){
			total_count++;
			car_count ++;
			print_count();
			//printf(" %d byte car data send by Gazebo successfully\n",len);  
		}
		else  
			printf("Gazebo send car data failed!\n"); 
	}

	void Udp_commu_gazebo_send::obstacleSendCallback(const rosplane_msgs::Waypoint::ConstPtr& msg)
	{
		udp_commu::UdpCommPacket packet_msg;
		std::vector<uint8_t> vec_data_temp;
    	vec_data_temp = serialize_ros(*msg);
		packet_msg.name =" obstacle";
		packet_msg.data = vec_data_temp;
		packet_msg.num = ++obstacle_num;

		std::vector<uint8_t> vec_data;
    	vec_data = serialize_ros(packet_msg);
		int data_size = vec_data.size();

		char *data = new char[data_size];
		for(int i = 0; i < data_size; i++) {
			*(data + i) = (char)(vec_data[i]);
		}

		int len=sendto(client_sockfd, data, data_size, 0, (struct sockaddr *)&remote_addr, sizeof(struct sockaddr));
		if(len > 0){
			total_count++;  
			obstacle_count++;
			print_count();
			//printf(" %d byte obstacle data send by Gazebo successfully\n",len);  
		}
		else  
			printf("Gazebo send obstacle data failed!\n"); 
	}

	void Udp_commu_gazebo_send::target_waypointSendCallback(const rosplane_msgs::Waypoint::ConstPtr& msg)
	{
		udp_commu::UdpCommPacket packet_msg;
		std::vector<uint8_t> vec_data_temp;
    	vec_data_temp = serialize_ros(*msg);
		packet_msg.name =" target_waypoint";
		packet_msg.data = vec_data_temp;
		packet_msg.num = ++target_waypoint_num;

		std::vector<uint8_t> vec_data;
    	vec_data = serialize_ros(packet_msg);
		int data_size = vec_data.size();

		char *data = new char[data_size];
		for(int i = 0; i < data_size; i++) {
			*(data + i) = (char)(vec_data[i]);
		}

		int len=sendto(client_sockfd, data, data_size, 0, (struct sockaddr *)&remote_addr, sizeof(struct sockaddr));
		if(len > 0){  
			total_count++;
			target_waypoint_count++;
			print_count();
			//printf(" %d byte target_waypoint data send by Gazebo successfully\n",len); 
		} 
		else  
			printf("Gazebo send target_waypoint data failed!\n"); 
	}

	void Udp_commu_gazebo_send::gathering_waypointSendCallback(const rosplane_msgs::Waypoint::ConstPtr& msg)
	{
		udp_commu::UdpCommPacket packet_msg;
		std::vector<uint8_t> vec_data_temp;
    	vec_data_temp = serialize_ros(*msg);
		packet_msg.name =" gathering_waypoint";
		packet_msg.data = vec_data_temp;
		packet_msg.num = ++gathering_waypoint_num;

		std::vector<uint8_t> vec_data;
    	vec_data = serialize_ros(packet_msg);
		int data_size = vec_data.size();

		char *data = new char[data_size];
		for(int i = 0; i < data_size; i++) {
			*(data + i) = (char)(vec_data[i]);
		}

		int len=sendto(client_sockfd, data, data_size, 0, (struct sockaddr *)&remote_addr, sizeof(struct sockaddr));
		if(len > 0){  
			total_count++;
			gathering_waypoint_count++;
			print_count();
			//printf(" %d byte gathering_waypoint data send by Gazebo successfully\n",len);  
		}
		else  
			printf("Gazebo send gathering_waypoint data failed!\n"); 
	}

	void Udp_commu_gazebo_send::areacover_doneSendCallback(const std_msgs::Empty::ConstPtr& msg)
	{
		udp_commu::UdpCommPacket packet_msg;
		std::vector<uint8_t> vec_data_temp;
    	vec_data_temp = serialize_ros(*msg);
		packet_msg.name =" areacover_done";
		packet_msg.data = vec_data_temp;
		packet_msg.num = ++areacover_done_num;

		std::vector<uint8_t> vec_data;
    	vec_data = serialize_ros(packet_msg);
		int data_size = vec_data.size();

		char *data = new char[data_size];
		for(int i = 0; i < data_size; i++) {
			*(data + i) = (char)(vec_data[i]);
		}

		int len=sendto(client_sockfd, data, data_size, 0, (struct sockaddr *)&remote_addr, sizeof(struct sockaddr));
		if(len > 0){ 
			total_count++;
			areacover_done_count++; 
			print_count();
			//printf(" %d byte areacover_done data send by Gazebo successfully\n",len);  
		}
		else  
			printf("Gazebo send areacover_done data failed!\n"); 
	}

	void Udp_commu_gazebo_send::formation_typeSendCallback(const rosplane_msgs::Formation::ConstPtr& msg)
	{
		udp_commu::UdpCommPacket packet_msg;
		std::vector<uint8_t> vec_data_temp;
    	vec_data_temp = serialize_ros(*msg);
		packet_msg.name =" formation_type";
		packet_msg.data = vec_data_temp;
		packet_msg.num = ++formation_type_num;

		std::vector<uint8_t> vec_data;
    	vec_data = serialize_ros(packet_msg);
		int data_size = vec_data.size();

		char *data = new char[data_size];
		for(int i = 0; i < data_size; i++) {
			*(data + i) = (char)(vec_data[i]);
		}

		int len=sendto(client_sockfd, data, data_size, 0, (struct sockaddr *)&remote_addr, sizeof(struct sockaddr));
		if(len > 0){
			total_count++;
			formation_type_count++;  
			print_count();
			//printf(" %d byte formation_type data send by Gazebo successfully\n",len); 
		} 
		else  
			printf("Gazebo send formation_type data failed!\n"); 
	}

	void Udp_commu_gazebo_send::print_count(){
		if(total_count % 1000 == 0){
			printf("\n gazebe send the total_count=%d,the truth_count=%d,\
									the car_count=%d,\
									the obstacle_count=%d,\
			                        the target_waypoint_count=%d,\
									the gathering_waypoint_count=%d,\
									the areacover_done_count=%d,\
									the formation_type_count=%d \n",total_count,truth_count,car_count,obstacle_count,target_waypoint_count,\
									gathering_waypoint_count,areacover_done_count,formation_type_count);
		}
	}


	void Udp_commu_gazebo_send::udp_Close()
	{
		close(client_sockfd);
	}
			


	// 2- Udp_commu_gazebo_receive

	Udp_commu_gazebo_receive::Udp_commu_gazebo_receive()
	{
		ros::NodeHandle n;
		n.getParam("namespace",namespace_);
		
		controller_commands_pub = n.advertise<rosplane_msgs::Controller_Commands>("/"+namespace_+"/controller_commands",1000); 
		trajectory_pub = n.advertise<nav_msgs::Path>("/"+namespace_+"/trajectory",1000); 
		send_to_UTO_pub = n.advertise<std_msgs::String>("/send_to_UTO",1000); 
		
		memset(&my_addr,0,sizeof(my_addr)); //数据初始化--清零
		my_addr.sin_family=AF_INET; //设置为IP通信
		my_addr.sin_addr.s_addr=INADDR_ANY;//服务器IP地址--允许连接到所有本地地址上
	
		n.getParam("htonsnumber",htonsnumber_);
		my_addr.sin_port=htons(htonsnumber_); //服务器端口号
		
		/*创建服务器端套接字--IPv4协议，面向无连接通信，UDP协议*/
		if((server_sockfd=socket(PF_INET,SOCK_DGRAM,0))<0)
		{  
			perror("socket error");
		}

		int mw_optval = 1;
		setsockopt(server_sockfd, SOL_SOCKET, SO_REUSEADDR, (char *)&mw_optval,sizeof(mw_optval));
		/*将套接字绑定到服务器的网络地址上*/
		if (bind(server_sockfd,(struct sockaddr *)&my_addr,sizeof(struct sockaddr))<0)
		{
			perror("bind error");
		}

		// pub init
		controller_commands_pub = n.advertise<rosplane_msgs::Controller_Commands>("/"+namespace_+"/controller_commands", 1000);
		trajectory_pub = n.advertise<nav_msgs::Path>("/"+namespace_+"/trajectory", 1000);
		send_to_UTO_pub = n.advertise<std_msgs::String>("/send_to_UTO", 1000);

		sin_size=sizeof(struct sockaddr_in);

		total_count = 0;
        controller_commands_count = 0;
        trajectory_count = 0;
        send_to_UTO_count = 0;
	}

	Udp_commu_gazebo_receive::~Udp_commu_gazebo_receive()
	{	
		close(server_sockfd);

		if(!docker_thread_){
			docker_thread_->join();
		} 
		
	}

	void Udp_commu_gazebo_receive::receive_docker_data()
	{
		while(ros::ok()){
			char buf[BUFSIZ];
			bzero(buf, sizeof(buf));
			int len =recvfrom(server_sockfd,buf,BUFSIZ,0,(struct sockaddr *)&remote_addr,&sin_size);
			if(len >0){
				total_count++;
				std::vector<uint8_t> u_data_buf(buf, buf + sizeof(buf));
				udp_commu::UdpCommPacket packet_msg;
				packet_msg = deserialize_ros<udp_commu::UdpCommPacket>(u_data_buf);
				char* name_char  = (char *)(packet_msg.name.c_str());
				string name(name_char);
				if(!name.empty()){
					name.erase(0,name.find_first_not_of(" "));
					name.erase(name.find_last_not_of(" ")+1);
				}
				int current_num = packet_msg.num;

				if(name.compare("controller_commands")==0 && current_num > last_controller_commands_num ){
					last_controller_commands_num = current_num;
					controller_commands_count++;
					rosplane_msgs::Controller_Commands info;
					info = deserialize_ros<rosplane_msgs::Controller_Commands>(packet_msg.data);
					controller_commands_pub.publish(info);
					//printf("\n Gazebo socket start to read controller_command data!\n"); 
				}
				if(name.compare("trajectory")==0 && current_num > last_trajectory_num ){
					last_trajectory_num = current_num;
					trajectory_count++;
					nav_msgs::Path info;
					info = deserialize_ros<nav_msgs::Path>(packet_msg.data);
					trajectory_pub.publish(info);
					//printf("\n Gazebo socket start to read trajectory data!\n");
				}
				if(name.compare("send_to_UTO")==0 && current_num > last_send_to_UTO_num ){ 
					last_send_to_UTO_num = current_num;
					send_to_UTO_count++;
					std_msgs::String info;
					info = deserialize_ros<std_msgs::String>(packet_msg.data);
					send_to_UTO_pub.publish(info);
					//printf("\n Gazebo socket start to read send_to_UTO data!\n");
				}
				//print
				print_count();

			}else{
				printf("\n Wrong in recvfrom() from docker data\n");
			}	
		}//end of while
	}

	void Udp_commu_gazebo_receive::udp_Close()
	{
		close(server_sockfd);
	}

	void Udp_commu_gazebo_receive::receive()
	{
		docker_thread_ = new boost::thread(&Udp_commu_gazebo_receive::receive_docker_data, this);
	}

	void Udp_commu_gazebo_receive::print_count()
	{
		if(total_count % 1000 == 0){
			printf("\n gazebe receive the total_count=%d,\
									the controller_commands_count=%d,\
									the trajectory_count=%d,\
									the send_to_UTO_count=%d \n",total_count,controller_commands_count,trajectory_count,send_to_UTO_count);
		}
	}

		

}

int main(int argc, char **argv)
{

  	ros::init(argc, argv, "udp_gazebo_send_receive");

	udp_commu::Udp_commu_gazebo_send gazebosend;
	udp_commu::Udp_commu_gazebo_receive gazeboreceive;
	gazeboreceive.receive();	      

	ros::spin();
	/*关闭套接字*/
	gazebosend.udp_Close();
	gazeboreceive.udp_Close();  

  	return 0;
}


