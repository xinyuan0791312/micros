/*******************************
 *
 *  This fast_docker_send_receive.cpp is finished by YAN JIE. .
 * 
 **************************/
#include <iostream>
#include <ros/ros.h>

#include "fastrtps_pubsub/Fastrtps_PacketPublisher.h"
#include "fastrtps_pubsub/Fastrtps_PacketSubscriber.h"



#include <fastrtps/Domain.h>
#include <fastrtps/utils/eClock.h>
#include <fastrtps/log/Log.h>

#include <sensor_msgs/Image.h>
#include <sensor_msgs/CompressedImage.h>
#include <sensor_msgs/CameraInfo.h>


#include "ros/ros.h"
#include "std_msgs/String.h"
#include <rosplane_msgs/State.h>
#include <rosplane_msgs/State_Path.h>
#include <rosplane_msgs/Command.h>
#include <rosplane_msgs/Controller_Commands.h>
#include <rosplane_msgs/Controller_Internals.h>
#include <rosplane_msgs/Waypoint.h>
#include <rosplane_msgs/Formation.h>
#include <rosplane_msgs/Takeoff_Command.h>
#include <nav_msgs/Path.h>
#include <nav_msgs/Odometry.h>

#include <geometry_msgs/Twist.h>
//#include "udp_commu/Gazebo_State.h"

#include <udp_commu/Heart_Beat.h>




#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

//串口相关的头文件  
#include<stdio.h>      /*标准输入输出定义*/  
#include<stdlib.h>     /*标准函数库定义*/  
#include<unistd.h>     /*Unix 标准函数定义*/  
#include<sys/types.h>   
#include<sys/stat.h>     
#include<fcntl.h>      /*文件控制定义*/  
#include<termios.h>    /*PPSIX 终端控制定义*/  
#include<errno.h>      /*错误号定义*/  
#include<string.h>  
#include <math.h>
#include <boost/thread/thread.hpp>
/*        0513          */
#include <std_msgs/Empty.h>

//宏定义  
#define REC_BUFFER_SIZE 200

uint64_t getCurrentTime()
{
    struct timeval tv;
    gettimeofday(&tv,NULL);
    return tv.tv_sec * 1000 + tv.tv_usec / 1000;
}

//#define BUFSIZ 200
//#define BUFSIZ 1*1024*1024

using std::string;

using namespace eprosima;
using namespace eprosima::fastrtps;


Fastrtps_PacketPublisher RTPS_cmd_vel_Publisher;

ros::Publisher state_pub;
ros::Publisher compressed_image_pub;

ros::Publisher watchdog_pub;	

// ***********001*****************
ros::Publisher truth_pub;

ros::Publisher goal_pub;
ros::Publisher Leader_state_path_pub;
Fastrtps_PacketPublisher RTPS_goal_Publisher;


ros::Publisher s_area_vertex_pub;

ros::Publisher v_area_vertex_pub;

ros::Publisher assemble_goals_pub;

// ***********002****************** 
Fastrtps_PacketPublisher RTPS_controller_commands_Publisher;
ros::Publisher Take_off_pub;
Fastrtps_PacketPublisher RTPS_Take_off_Publisher;

ros::Publisher Leader_state_pub;
Fastrtps_PacketPublisher RTPS_Leader_state_Publisher;
Fastrtps_PacketPublisher RTPS_state_path_Publisher;
Fastrtps_PacketPublisher RTPS_send_to_UTO_Publisher;
Fastrtps_PacketPublisher RTPS_gathering_reached_Publisher;
Fastrtps_PacketPublisher RTPS_areacover_done_Publisher;

ros::Publisher car_pub;
ros::Publisher camera_info_pub;

//*****************************************************

ros::Publisher obstacle_pub;

ros::Publisher target_waypoint_pub;

ros::Publisher gathering_waypoint_pub;

ros::Publisher areacover_done_pub;

ros::Publisher formation_type_pub;

ros::Publisher gathering_reached_pub;

//***************************************************** 


int count_receive_state = 0;
uint64_t starttime;
uint64_t endtime;



string namespace_; 

template<class T>
std::vector<uint8_t> serialize_ros(T t)
{
    std::vector<uint8_t> vec;
    uint32_t serial_size = ros::serialization::serializationLength(t);
    boost::shared_array<uint8_t> buffer(new uint8_t[serial_size]);
    ros::serialization::OStream ostream(buffer.get(), serial_size);
    ros::serialization::serialize(ostream, t);
    vec.resize(serial_size);
    std::copy(buffer.get(), buffer.get() + serial_size, vec.begin());
    return vec;
}

template<class T>
T deserialize_ros(const std::vector<uint8_t>& vec)
{
    T t;
    uint32_t serial_size = vec.size();
    std::vector<uint8_t> buffer(serial_size);
    std::copy(vec.begin(), vec.begin() + serial_size, buffer.begin());
    ros::serialization::IStream istream(buffer.data(), serial_size);
    ros::serialization::Serializer<T>::read(istream, t);
    return t;
}

//将命名空间转为整形数字 add by yyt 0301
int namespace_to_int(std::string namespace_str)
{
    //如果包含follower，则跟据follower的index值处理
     if(namespace_str.find("robot") !=std::string::npos)
     {
        int start_index = namespace_str.find_last_of("_");
        string index_num="";
        if(start_index != std::string::npos){
            index_num = namespace_str.substr(start_index+1);
            int index = atoi(index_num.c_str());
            return index+100;
        }
     }
     return 1000;
}

void RTPS_Take_off_callback(std::vector<uint8_t> msg)
{
    ROS_INFO("*********RTPS_Take_off_callback   come  in*****");
    rosplane_msgs::Takeoff_Command info;
    info = deserialize_ros<rosplane_msgs::Takeoff_Command>(msg);
  
    if(Take_off_pub == NULL)
    {
        ros::NodeHandle nh;
        Take_off_pub = nh.advertise<rosplane_msgs::Takeoff_Command>("/takeoff_commands_f",10);        
    }
    Take_off_pub.publish(info);
    ROS_INFO("*********RTPS_Take_off_callback   come  in  and  publish success*****");
}

void RTPS_gathering_reached_callback(std::vector<uint8_t> msg)
{
    ROS_INFO("*********RTPS_gathering_reached_callback   come  in*****");
    rosplane_msgs::Formation info;
    info = deserialize_ros<rosplane_msgs::Formation>(msg);
  
    if(gathering_reached_pub == NULL)
    {
        ros::NodeHandle nh;
        gathering_reached_pub = nh.advertise<rosplane_msgs::Formation>("/gathering_reached_f",10);        
    }
    gathering_reached_pub.publish(info);
    ROS_INFO("*********RTPS_gathering_reached_callback   come  in  and  publish success*****");
}


// ***********101 接受地面站******************
void RTPS_s_area_vertex_callback(std::vector<uint8_t> msg)
{
    ROS_INFO("*********RTPS_s_area_vertex_callback   come  in*****");
    std_msgs::String info;
    info = deserialize_ros<std_msgs::String>(msg);
  
    if(s_area_vertex_pub == NULL)
    {
        ros::NodeHandle nh;
        s_area_vertex_pub = nh.advertise<std_msgs::String>("/s_area_vertex",10);        
    }
    s_area_vertex_pub.publish(info);
    ROS_INFO("*********RTPS_s_area_vertex_callback   come  in  and  publish success*****");
}

// ***********102  接受地面站******************
// void RTPS_v_area_vertex_callback(std::vector<uint8_t> msg)
// {
//     ROS_INFO("*********RTPS_v_area_vertex_callback   come  in*****");
//     va_msgs::AreaVertex info;
//     info = deserialize_ros<va_msgs::AreaVertex>(msg);
  
//     if(v_area_vertex_pub == NULL)
//     {
//         ros::NodeHandle nh;
//         v_area_vertex_pub = nh.advertise<va_msgs::AreaVertex>("/v_area_vertex",10);        
//     }
//     v_area_vertex_pub.publish(info);
//     ROS_INFO("*********RTPS_v_area_vertex_callback   come  in  and  publish success*****");
// }

// ***********103 接受地面站******************
void RTPS_assemble_goals_callback(std::vector<uint8_t> msg)
{
    ROS_INFO("*********RTPS_assemble_goals_callback   come  in*****");
    std_msgs::String info;
    info = deserialize_ros<std_msgs::String>(msg);
  
    if(assemble_goals_pub == NULL)
    {
        ros::NodeHandle nh;
        assemble_goals_pub = nh.advertise<std_msgs::String>("/assemble_goals",10);        
    }
    assemble_goals_pub.publish(info);
    ROS_INFO("*********RTPS_assemble_goals_callback   come  in  and  publish success*****");
}

// ***********104 leader to follower******************
void RTPS_goal_callback(std::vector<uint8_t> msg)
{
    ROS_INFO("*********RTPS_goal_callback   come  in*****");
    std_msgs::String info;
    info = deserialize_ros<std_msgs::String>(msg);
  
    if(goal_pub == NULL)
    {
        ros::NodeHandle nh;
        goal_pub = nh.advertise<std_msgs::String>("/goal_f",10);        
    }
    goal_pub.publish(info);
    ROS_INFO("*********RTPS_goal_callback   come  in  and  publish success*****");
}

void sub_goal_callback(const std_msgs::String::ConstPtr& msg)
{
    ROS_INFO("success subscribing std_msgs::String (goal) from leader!!!!! \n");
    std::vector<uint8_t> vec_data;
    vec_data = serialize_ros(*msg);
    RTPS_goal_Publisher.DDS_pub(vec_data);
    ROS_INFO("std_msgs::String (goal) msg has been published by fastrtps in leader \n");

}

void sub_areacover_done_callback(const std_msgs::Empty::ConstPtr& msg)
{
    ROS_INFO("areacover_done from leader!!!!! \n");
    std::vector<uint8_t> vec_data;
    vec_data = serialize_ros(*msg);
    RTPS_areacover_done_Publisher.DDS_pub(vec_data);
    ROS_INFO("areacover_done has been published by fastrtps in leader \n");

}

void sub_state_path_callback(const rosplane_msgs::State_Path::ConstPtr& msg)
{
    ROS_INFO("success subscribing State_Path from leader!!!!! \n");
    std::vector<uint8_t> vec_data;
    vec_data = serialize_ros(*msg);
    RTPS_state_path_Publisher.DDS_pub(vec_data);
    ROS_INFO("State_Path msg has been published by fastrtps in leader \n");

}

void sub_gathering_reached_callback(const rosplane_msgs::Formation::ConstPtr& msg)
{
    ROS_INFO("success subscribing gathering_reached!!!!! \n");
    std::vector<uint8_t> vec_data;
    vec_data = serialize_ros(*msg);
    RTPS_gathering_reached_Publisher.DDS_pub(vec_data);
    ROS_INFO("gathering_reached msg has been published by fastrtps \n");

}

void sub_Take_off_callback(const rosplane_msgs::Takeoff_Command::ConstPtr& msg)
{
    ROS_INFO("success subscribing std_msgs::String (Takeoff_Command) from leader!!!!! \n");
    std::vector<uint8_t> vec_data;
    vec_data = serialize_ros(*msg);
    RTPS_Take_off_Publisher.DDS_pub(vec_data);
    ROS_INFO("rosplane_msgs::State (Takeoff_Command) msg has been published by fastrtps in leader \n");

}

// ***********105 leader to follower******************
void RTPS_Leader_state_callback(std::vector<uint8_t> msg)
{
    ROS_INFO("*********RTPS_Leader_state_callback   come  in*****");
    rosplane_msgs::State info;
    info = deserialize_ros<rosplane_msgs::State>(msg);
  
    if(Leader_state_pub == NULL)
    {
        ros::NodeHandle nh;
        Leader_state_pub = nh.advertise<rosplane_msgs::State>("/Leader_state_f",10);        
    }
    Leader_state_pub.publish(info);
    ROS_INFO("*********RTPS_Leader_state_callback   come  in  and  publish success*****");
}

void RTPS_state_path_callback(std::vector<uint8_t> msg)
{
    ROS_INFO("*********RTPS_state_path_callback   come  in*****");
    rosplane_msgs::State_Path info;
    info = deserialize_ros<rosplane_msgs::State_Path>(msg);
  
    if(Leader_state_path_pub == NULL)
    {
        ros::NodeHandle nh;
        Leader_state_path_pub = nh.advertise<rosplane_msgs::State_Path>("/leader/state_path_f",10);        
    }
    Leader_state_path_pub.publish(info);
    ROS_INFO("*********RTPS_state_path_callback   come  in  and  publish success*****");
}

void sub_Leader_state_callback(const rosplane_msgs::State::ConstPtr& msg)
{
    ROS_INFO("success subscribing std_msgs::String (Leader_state) from leader!!!!! \n");
    std::vector<uint8_t> vec_data;
    vec_data = serialize_ros(*msg);
    RTPS_Leader_state_Publisher.DDS_pub(vec_data);
    ROS_INFO("rosplane_msgs::State (Leader_state) msg has been published by fastrtps in leader \n");

}

int main(int argc, char **argv)
{
    std::cout << "fastrtps_pub_sub started in docker" << std::endl;
    ros::init(argc,argv,"fastrtps_dockersend_receive");
		
	ros::NodeHandle n;
	string docker_ns = "robot";
    n.param("namespace", namespace_, docker_ns);
    int domainID = 0;
    domainID = namespace_to_int(namespace_);


    // ***********101******************
    Fastrtps_PacketSubscriber RTPS_s_area_vertex_Subscriber; //String.msg
    if(RTPS_s_area_vertex_Subscriber.init("/fastrtps_s_area_vertex")) 
    {
        RTPS_s_area_vertex_Subscriber.receive(&RTPS_s_area_vertex_callback);//回调函数
    }

    // ***********102******************
    // Fastrtps_PacketSubscriber RTPS_v_area_vertex_Subscriber; //
    // if(RTPS_v_area_vertex_Subscriber.init(72,"/fastrtps_v_area_vertex")) 
    // {
    //     RTPS_v_area_vertex_Subscriber.receive(&RTPS_v_area_vertex_callback);//回调函数
    // }

    // ***********103******************
    Fastrtps_PacketSubscriber RTPS_assemble_goals_Subscriber; //String.msg
    if(RTPS_assemble_goals_Subscriber.init("/fastrtps_assemble_goals")) 
    {
        RTPS_assemble_goals_Subscriber.receive(&RTPS_assemble_goals_callback);//回调函数
    }

    // ***********104 follower 接受 leader******************
    Fastrtps_PacketSubscriber RTPS_goal_Subscriber; //String.msg
    if(RTPS_goal_Subscriber.init("/fastrtps_goal")) 
    {
        RTPS_goal_Subscriber.receive(&RTPS_goal_callback);//回调函数
    }

    Fastrtps_PacketSubscriber RTPS_state_path_Subscriber; //String.msg
    if(RTPS_state_path_Subscriber.init("/fastrtps_state_path")) 
    {
        RTPS_state_path_Subscriber.receive(&RTPS_state_path_callback);//回调函数
    }
    //leader 传给follower
    ros::Subscriber goal_sub = n.subscribe("/goal",10,sub_goal_callback);
    RTPS_goal_Publisher.init("/fastrtps_goal");

    ros::Subscriber areacover_done_sub = n.subscribe("/areacover_done",10,sub_areacover_done_callback);
    RTPS_areacover_done_Publisher.init("/fastrtps_areacover_done");


    ros::Subscriber state_path_sub = n.subscribe("/leader/state_path",10,sub_state_path_callback);
    RTPS_state_path_Publisher.init("/fastrtps_state_path");

//**************************takeoff*************************************
    Fastrtps_PacketSubscriber RTPS_Take_off_Subscriber; //String.msg
    if(RTPS_Take_off_Subscriber.init("/fastrtps_takeoff_commands")) 
    {
        RTPS_Take_off_Subscriber.receive(&RTPS_Take_off_callback);//回调函数
    }
    //leader 传给follower
    ros::Subscriber Take_off_sub = n.subscribe("/takeoff_commands",10,sub_Take_off_callback);
    RTPS_Take_off_Publisher.init("/fastrtps_takeoff_commands");

    // ***********105 follower 接受 leader******************
    Fastrtps_PacketSubscriber RTPS_Leader_state_Subscriber; //String.msg
    if(RTPS_Leader_state_Subscriber.init("/fastrtps_Leader_state")) 
    {
        RTPS_Leader_state_Subscriber.receive(&RTPS_Leader_state_callback);//回调函数
    }
    //leader 传给follower
    ros::Subscriber Leader_state_sub = n.subscribe("/Leader_state",10,sub_Leader_state_callback);
    RTPS_Leader_state_Publisher.init("/fastrtps_Leader_state");
    
    Fastrtps_PacketSubscriber RTPS_gathering_reached_Subscriber; //String.msg
    if(RTPS_gathering_reached_Subscriber.init("/fastrtps_gathering_reached")) 
    {
        RTPS_gathering_reached_Subscriber.receive(&RTPS_gathering_reached_callback);//回调函数
    }

    ros::Subscriber gathering_reached_sub = n.subscribe("/gathering_reached",10,sub_gathering_reached_callback);
    RTPS_gathering_reached_Publisher.init("/fastrtps_gathering_reached");

    ros::spin();
    Domain::stopAll();
    Log::Reset();
    return 0;
}
