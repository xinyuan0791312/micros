
#include "udp_commu/udp_docker_send_receive.h"

namespace udp_commu 
{
	using std::string;

	template<class T>
	std::vector<uint8_t> serialize_ros(T t)
	{
		std::vector<uint8_t> vec;
		uint32_t serial_size = ros::serialization::serializationLength(t);
		boost::shared_array<uint8_t> buffer(new uint8_t[serial_size]);
		ros::serialization::OStream ostream(buffer.get(), serial_size);
		ros::serialization::serialize(ostream, t);
		vec.resize(serial_size);
		std::copy(buffer.get(), buffer.get() + serial_size, vec.begin());
		return vec;
	}

	template<class T>
	T deserialize_ros(const std::vector<uint8_t>& vec)
	{
		T t;
		uint32_t serial_size = vec.size();
		std::vector<uint8_t> buffer(serial_size);
		std::copy(vec.begin(), vec.begin() + serial_size, buffer.begin());
		ros::serialization::IStream istream(buffer.data(), serial_size);
		ros::serialization::Serializer<T>::read(istream, t);
		return t;
	}

//1-Udp_commu_docker_send class 

	Udp_commu_docker_send::Udp_commu_docker_send()
	{
		string gazebo_ip = "10.10.10.3";
		n.param("gazebo_ipaddress", gazebo_ipaddress_, gazebo_ip);

		string docker_ns = "robot_1";
		n.param("namespace", namespace_, docker_ns);
		int port=7999;
		n.param("htonsnumber",port_,port);
		printf("******* docker send gazebo_IP=%s ************\n",gazebo_ipaddress_.c_str()); 
        printf("******* docker send port=%d ************\n",port_); 
		
		memset(&remote_addr,0,sizeof(remote_addr)); //数据初始化--清零
		remote_addr.sin_family=AF_INET; //设置为IP通信
		remote_addr.sin_addr.s_addr=inet_addr(gazebo_ipaddress_.c_str());//gazeboIP地址
		remote_addr.sin_port=htons(port_); //服务器端口号

		/*创建客户端套接字--IPv4协议，面向无连接通信，UDP协议*/
		if((client_sockfd=socket(PF_INET,SOCK_DGRAM,0))<0)
		{  
			perror("socket error");
		}
		sin_size=sizeof(struct sockaddr_in);

		int snd_size;    /* 发送缓冲区大小为240K */ 
		socklen_t optlen = sizeof(snd_size); 

		int	err = getsockopt(client_sockfd, SOL_SOCKET, SO_SNDBUF,&snd_size, &optlen); 
		if(err<0)
		{ 
			printf("获取发送缓冲区大小错误\n"); 
		}   					
		
		//1-controller-commands
		controller_commands_sub_ = n.subscribe("/"+namespace_+"/controller_commands", 10, &Udp_commu_docker_send::controller_commandsSendCallback, this);
		//2-trajectory
		trajectory_sub_ = n.subscribe("/"+namespace_+"/trajectory", 10, &Udp_commu_docker_send::trajectorySendCallback, this);
		//3-send_to_UTO
		send_to_UTO_sub_ = n.subscribe("/send_to_UTO", 10, &Udp_commu_docker_send::send_to_UTOSendCallback, this);

		total_count = 0;
        controller_commands_count = 0;
        trajectory_count = 0;
        send_to_UTO_count = 0;

		controller_commands_num = 0;
        trajectory_num = 0;
        send_to_UTO_num = 0;

	}

	Udp_commu_docker_send::~Udp_commu_docker_send()
	{	

	}

	void Udp_commu_docker_send::controller_commandsSendCallback(const rosplane_msgs::Controller_Commands::ConstPtr& msg)
	{
		udp_commu::UdpCommPacket packet_msg;
		std::vector<uint8_t> vec_data_temp;
		vec_data_temp = serialize_ros(*msg);
		packet_msg.name ="controller_commands";
		packet_msg.data = vec_data_temp;
		packet_msg.num = ++controller_commands_num;

		std::vector<uint8_t> vec_data;
		vec_data = serialize_ros(packet_msg);
		int data_size = vec_data.size();

		char *data = new char[data_size];
		for(int i = 0; i < data_size; i++) {
			*(data + i) = (char)(vec_data[i]);
		}

		int len=sendto(client_sockfd, data, data_size, 0, (struct sockaddr *)&remote_addr, sizeof(struct sockaddr));
		if(len > 0){ 
			total_count++;
			controller_commands_count++;
			print_count();
			//printf(" *************** %d byte controller_command data send by Docker successfully\n",len); 
		} 
		else  
			printf("Docker send controller_command data failed!\n"); 
	}
	
	void Udp_commu_docker_send::trajectorySendCallback(const nav_msgs::Path::ConstPtr& msg)
	{
		udp_commu::UdpCommPacket packet_msg;
		std::vector<uint8_t> vec_data_temp;
		vec_data_temp = serialize_ros(*msg);
		packet_msg.name ="trajectory";
		packet_msg.data = vec_data_temp;
		packet_msg.num = ++trajectory_num;


		std::vector<uint8_t> vec_data;
		vec_data = serialize_ros(packet_msg);
		int data_size = vec_data.size();

		char *data = new char[data_size];
		for(int i = 0; i < data_size; i++) {
			*(data + i) = (char)(vec_data[i]);
		}

		int len=sendto(client_sockfd, data, data_size, 0, (struct sockaddr *)&remote_addr, sizeof(struct sockaddr));
		if(len > 0){  
			total_count++;
			trajectory_count++;
			print_count();
			//printf(" %d byte trajectory data send by Docker successfully\n",len); 
		} 
		else  
			printf("Docker send trajectory data failed!\n"); 
	}

	void Udp_commu_docker_send::send_to_UTOSendCallback(const std_msgs::String::ConstPtr& msg)
	{
		udp_commu::UdpCommPacket packet_msg;
		std::vector<uint8_t> vec_data_temp;
		vec_data_temp = serialize_ros(*msg);
		packet_msg.name ="send_to_UTO";
		packet_msg.data = vec_data_temp;
		packet_msg.num = ++send_to_UTO_num;

		std::vector<uint8_t> vec_data;
		vec_data = serialize_ros(packet_msg);
		int data_size = vec_data.size();

		char *data = new char[data_size];
		for(int i = 0; i < data_size; i++) {
			*(data + i) = (char)(vec_data[i]);
		}

		int len=sendto(client_sockfd, data, data_size, 0, (struct sockaddr *)&remote_addr, sizeof(struct sockaddr));
		if(len > 0){  
			total_count++;
			send_to_UTO_count++;
			print_count();
			//printf(" %d byte send_to_UTO data send by Docker successfully\n",len);  
		}
		else  
			printf("Docker send send_to_UTO data failed!\n"); 
	}

	void Udp_commu_docker_send::udp_Close()
	{
		close(client_sockfd);
	} 

	void Udp_commu_docker_send::print_count()
	{
		if(total_count % 1000 == 0){
			printf("\n docker send the total_count= %d,the controller_commands_count= %d,the trajectory_count= %d,the send_to_UTO_count= %d. \n",total_count,controller_commands_count,trajectory_count,send_to_UTO_count);
		}
	} 

			


//2-Udp_commu_docker_receive class
	Udp_commu_docker_receive::Udp_commu_docker_receive()
	{
		ros::NodeHandle n;
		n.getParam("namespace",namespace_);
		
		//controller_commands_pub = n.advertise<rosplane_msgs::Controller_Commands>("/"+namespace_+"/controller_commands",1000); 

		memset(&my_addr,0,sizeof(my_addr)); //数据初始化--清零
		my_addr.sin_family=AF_INET; //设置为IP通信
		my_addr.sin_addr.s_addr=INADDR_ANY;//服务器IP地址--允许连接到所有本地地址上
	
		n.getParam("htonsnumber",htonsnumber_);
		my_addr.sin_port=htons(8000); //服务器端口号
		
		/*创建服务器端套接字--IPv4协议，面向无连接通信，UDP协议*/
		if((server_sockfd=socket(PF_INET,SOCK_DGRAM,0))<0)
		{  
			perror("socket error");
		}

		int mw_optval = 1;
		setsockopt(server_sockfd, SOL_SOCKET, SO_REUSEADDR, (char *)&mw_optval,sizeof(mw_optval));
		/*将套接字绑定到服务器的网络地址上*/
		if (bind(server_sockfd,(struct sockaddr *)&my_addr,sizeof(struct sockaddr))<0)
		{
			perror("bind error");
		}

		//pub init
		truth_pub = n.advertise<rosplane_msgs::State>("/"+namespace_+"/truth", 1000);
		car_pub = n.advertise<nav_msgs::Odometry>("/enemy/ground_truth/state", 1000);
		obstacle_pub = n.advertise<rosplane_msgs::Waypoint>("/obstacle", 1000);
		target_waypoint_pub = n.advertise<rosplane_msgs::Waypoint>("/target_waypoint", 1000);
		gathering_waypoint_pub = n.advertise<rosplane_msgs::Waypoint>("/gathering_waypoint", 1000);
		areacover_done_pub = n.advertise<std_msgs::Empty>("/areacover_done", 1000);
		formation_type_pub = n.advertise<rosplane_msgs::Formation>("/formation_type", 1000);

		sin_size=sizeof(struct sockaddr_in);
		total_count = 0;
        truth_count = 0;
        car_count = 0;
        obstacle_count = 0;
        target_waypoint_count = 0;
        gathering_waypoint_count = 0;
        areacover_done_count = 0;
        formation_type_count = 0;

	}

	Udp_commu_docker_receive::~Udp_commu_docker_receive()
	{	
		close(server_sockfd);

		if(!gazebo_thread_){
			gazebo_thread_->join();
		} 

	}

	void Udp_commu_docker_receive::receive_gazebo_data()
	{
		while(ros::ok()){
			char buf[BUFSIZ];
			bzero(buf, sizeof(buf));
			int len =recvfrom(server_sockfd,buf,BUFSIZ,0,(struct sockaddr *)&remote_addr,&sin_size);
			if(len >0){
				total_count++;
				std::vector<uint8_t> u_data_buf(buf, buf + sizeof(buf));
				udp_commu::UdpCommPacket packet_msg;
				packet_msg = deserialize_ros<udp_commu::UdpCommPacket>(u_data_buf);
				char* name_char  = (char *)(packet_msg.name.c_str());
				string name(name_char);
				if(!name.empty()){
					name.erase(0,name.find_first_not_of(" "));
					name.erase(name.find_last_not_of(" ")+1);
				}
				int current_num = packet_msg.num;

				if(name.compare("truth")==0 && current_num > last_truth_num){
					last_truth_num = current_num;
					truth_count++;
					rosplane_msgs::State info;
					info = deserialize_ros<rosplane_msgs::State>(packet_msg.data);
					truth_pub.publish(info);
					//printf("\n Docker socket start to truth data!\n"); 
				}
				if(name.compare("car")==0 && current_num > last_car_num){
					last_car_num = current_num;
					car_count++;
					nav_msgs::Odometry info;
					info = deserialize_ros<nav_msgs::Odometry>(packet_msg.data);
					car_pub.publish(info);
					//printf("\n Docker socket start to car data!\n"); 
				}
				if(name.compare("obstacle")==0 && current_num > last_obstacle_num){
					last_obstacle_num = current_num;
					obstacle_count++;
					rosplane_msgs::Waypoint info;
					info = deserialize_ros<rosplane_msgs::Waypoint>(packet_msg.data);
					obstacle_pub.publish(info);
					//printf("\n Docker socket start to obstacle data!\n");
				}
				if(name.compare("target_waypoint")==0 && current_num > last_target_waypoint_num){
					last_target_waypoint_num = current_num;
					target_waypoint_count++;
					rosplane_msgs::Waypoint info;
					info = deserialize_ros<rosplane_msgs::Waypoint>(packet_msg.data);
					target_waypoint_pub.publish(info);
					//printf("\n Docker socket start to target_waypoint data!\n");
				}
				if(name.compare("gathering_waypoint")==0 && current_num > last_gathering_waypoint_num){
					last_gathering_waypoint_num = current_num;
					gathering_waypoint_count++;
					rosplane_msgs::Waypoint info;
					info = deserialize_ros<rosplane_msgs::Waypoint>(packet_msg.data);
					gathering_waypoint_pub.publish(info);
					//printf("\n Docker socket start to gathering_waypoint data!\n");
				}
				if(name.compare("areacover_done")==0 && current_num > last_areacover_done_num){
					last_areacover_done_num = current_num;
					areacover_done_count++;
					std_msgs::Empty info;
					info = deserialize_ros<std_msgs::Empty>(packet_msg.data);
					areacover_done_pub.publish(info);
					//printf("\n Docker socket start to areacover_done data!\n");
				}
				if(name.compare("formation_type")==0 && current_num > last_formation_type_num){
					last_formation_type_num = current_num;
					formation_type_count++;
					rosplane_msgs::Formation info;
					info = deserialize_ros<rosplane_msgs::Formation>(packet_msg.data);
					formation_type_pub.publish(info);
					//printf("\n Docker socket start to formation_type data!\n");
				}
				//print
				print_count();
				
			}else{
				printf("\n Wrong in recvfrom  for controller_command data\n");
			}	
		}//end of while
	}



	void Udp_commu_docker_receive::udp_Close()
	{
		close(server_sockfd);
	}

	void Udp_commu_docker_receive::receive()
	{
		gazebo_thread_ = new boost::thread(&Udp_commu_docker_receive::receive_gazebo_data, this);
	}

	void Udp_commu_docker_receive::print_count(){
		if(total_count % 1000 == 0){
			printf(" \n docker receive the total_count=%d, the truth_count=%d, the car_count=%d,the obstacle_count=%d,the target_waypoint_count=%d,the gathering_waypoint_count=%d,the areacover_done_count=%d,the formation_type_count=%d \n ",total_count,truth_count,car_count,obstacle_count,target_waypoint_count,gathering_waypoint_count,areacover_done_count,formation_type_count);
		}
	}

}

int main(int argc, char **argv)
{

  	ros::init(argc, argv, "udp_docker_send_receive");

	udp_commu::Udp_commu_docker_send dockersend;
	udp_commu::Udp_commu_docker_receive dockerreceive;
	dockerreceive.receive();	      

	ros::spin();
	/*关闭套接字*/
	dockersend.udp_Close();
	dockerreceive.udp_Close();  

  	return 0;
}


