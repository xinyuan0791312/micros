#include "statistics_data.h"
#include <iostream>
#include <mutex>

extern data_list node_info[11];
extern bool verbose;

std::mutex m_info;

void statistics_data(unsigned int topic_id, int host_id, int msg_id)
{
	if(host_id>=100 || host_id <0)	{
		if(verbose)
			std::cout<<"Invalid HOST_ID"<<std::endl;
		return;
	}

	if(topic_id>10) {
		std::cout<<"Invalid Topic ID"<<std::endl;
		return;
	}
	
		

	m_info.lock();
	node_info[topic_id].id[host_id]=node_info[topic_id].id[host_id]+1;
	node_info[topic_id].valid[host_id]=1;
	if(node_info[topic_id].min[host_id]>msg_id)
		node_info[topic_id].min[host_id] = msg_id;

	if(node_info[topic_id].max[host_id]<msg_id)
		node_info[topic_id].max[host_id] = msg_id;
	//std::cout <<"msg_id: "<<msg_id<<std::endl;
	m_info.unlock();
}
