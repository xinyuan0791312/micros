#include <pcap.h>
#include <iostream>
#include <boost/thread/thread.hpp>
#include <arpa/inet.h>

#define PORT "eth0"

extern long udp_data_cnt;
extern long pkg_cnt;

typedef struct udp_header
{
	u_short sport;
	u_short dport;
	u_short len;
	u_short crc;
} udp_header;

void _protocal_callback(u_char *param, const struct pcap_pkthdr *header, const u_char *pkt_data)
{
	udp_header *u_hd;
	u_short data_len;	
	u_hd = (udp_header *)(pkt_data+34);
	
	data_len = ntohs(u_hd->len);	
	udp_data_cnt += data_len;
	pkg_cnt++;
}


void net_statics()
{
	pcap_t *pcap_handle = NULL;
	struct pcap_pkthdr protcol_hdr;
	char errBuf[100];
	unsigned char *p_packet_content;

	struct bpf_program fcode;

	pcap_handle = pcap_open_live(PORT,3000,0,0,errBuf);
	if(NULL==pcap_handle) {
		std::cout<<"Open "<<PORT<<" error"<<std::endl;
		return;
	}
	
	if(-1==pcap_compile(pcap_handle,&fcode,"udp and udp[8:4]=0x52545053",1,0)) {
		std::cout << "pcap_compile error"<<std::endl;
		pcap_close(pcap_handle);
		return;
	}
	
	if(-1==pcap_setfilter(pcap_handle, &fcode)) {
		std::cout << "pcap_setfilter error"<<std::endl;
		pcap_close(pcap_handle);
		return;
	}

	if(pcap_loop(pcap_handle,-1,_protocal_callback,NULL)<0) {
		std::cout << "pcap loop error"<<std::endl;
	}
	
	pcap_close(pcap_handle);
}
