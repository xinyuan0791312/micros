/*
	Packet sniffer using libpcap library
*/
#include<pcap.h>
#include<stdio.h>
#include<stdlib.h> // for exit()
#include<string.h> //for memset
#include<sys/socket.h>
#include<arpa/inet.h> // for inet_ntoa()
#include<net/ethernet.h>
#include<netinet/ip_icmp.h>	//Provides declarations for icmp header
#include<netinet/udp.h>	//Provides declarations for udp header
#include<netinet/tcp.h>	//Provides declarations for tcp header
#include<netinet/ip.h>	//Provides declarations for ip header
#include <time.h>       // 结构体time_t和函数time

void process_packet(u_char *, const struct pcap_pkthdr *, const u_char *);

FILE *logfile;
struct sockaddr_in source,dest;
int getINT16(const unsigned char *dataptr)
{
    int tmp = 0;
    tmp = (int)((int)(dataptr[0] <<  8) & 0xFF00);
    tmp |= (int)((int) dataptr[1] & 0xFF);
    return tmp;	
}
int getINT32(const unsigned char *dataptr)
{
    int tmp = 0;
    tmp  = (int)((int)(dataptr[0] << 24) & 0xFF000000);
    tmp |= (int)((int)(dataptr[1] << 16) & 0xFF0000);
    tmp |= (int)((int)(dataptr[2] <<  8) & 0xFF00);
    tmp |= (int)((int) dataptr[3] & 0xFF);
    return tmp;	
}
int main()
{
    pcap_if_t *alldevsp , *device;
    pcap_t *handle; //Handle of the device that shall be sniffed

    char errbuf[100] , *devname;
	
    //First get the list of available devices
    if( pcap_findalldevs( &alldevsp , errbuf) )
    {
	printf("Error finding devices : %s" , errbuf);
	exit(1);
    }
	
    char dev[10] = "eth0";  //network card name
    devname = dev;

    //Open the device for sniffing
    printf("Opening device %s for sniffing ... " , devname);
    handle = pcap_open_live(devname , 65536 , 1 , 0 , errbuf);
	
    if (handle == NULL) 
    {
	fprintf(stderr, "Couldn't open device %s : %s\n" , devname , errbuf);
	exit(1);
    }
    printf("Done\n");

    time_t nowT ;      // time_t就是long int 类型
    nowT = time(0);    // 取得当前时间 ,秒级
    // time(&nowT);    // 取得当前时间 ,秒级
    // nowT = time(NULL); // 取得当前时间 ,秒级
    char strT[32];
    /* 使用 strftime 将时间格式化成字符串（"YYYY-MM-DD hh:mm:ss"格式)*/
    strftime(strT, sizeof(strT), "%Y-%m-%d-%H-%M-%S", localtime(&nowT));

    logfile=fopen(strT,"w");
    if(logfile==NULL) 
    {
	printf("Unable to create file.");
    }
	
    //Put the device in sniff loop
    pcap_loop(handle , -1 , process_packet , NULL);

    return 0;	
}
typedef struct data_stu{
    unsigned char peerID[6];
    struct timeval ts;
    unsigned int tx_packets;
    unsigned int tx_bytes;
    unsigned int tx_retries;
    unsigned int tx_fail;
    signed int tx_bitrate;
    signed char signal;
}__attribute__((packed))ORG_DATA_STU;
void process_packet(u_char *args, const struct pcap_pkthdr *header, const u_char *buffer)
{
    int size = header->len;
    char data[1000];
    if(0x11==buffer[23]&&0x27==buffer[34]&&0x18==buffer[35]&&0x27==buffer[36]&&0x18==buffer[37]){
    //if(0x11==buffer[23]){
	for(int i=0;i<size;i++){
	    data[i]=buffer[i];
	}
	if(0x01==buffer[42]){
	    printf("发送请求指令\n");
	    fprintf(logfile,"发送请求指令\n");
	}
	if(0x02==buffer[42]){
	    printf("设备响应\n");
	    fprintf(logfile,"设备响应\n");
	}
	for(int i=0;i<size;i++){
	    printf("%02x",data[i]);
	    fprintf(logfile,"%02x ",data[i]);
	}
	printf("\n\n");
	fprintf(logfile,"\n");
	if(0x03==buffer[42]){
	    printf("comeback!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n\n\n");
	    __attribute__((packed))ORG_DATA_STU stu;
	    /*stu.peerID[0]=buffer[53];printf("another mac : %02x ",stu.peerID[0]);
	    stu.peerID[1]=buffer[54];printf("%02x ",stu.peerID[1]);
	    stu.peerID[2]=buffer[55];printf("%02x ",stu.peerID[2]);
	    stu.peerID[3]=buffer[56];printf("%02x ",stu.peerID[3]);
	    stu.peerID[4]=buffer[57];printf("%02x ",stu.peerID[4]);
	    stu.peerID[5]=buffer[58];printf("%02x\n",stu.peerID[5]);*/
	    printf("\n\n\nsize = %d\n\n\n",size);
	    unsigned int len;
	    unsigned char mac[6];
	    unsigned int tx_num;
	    len=getINT16(&buffer[43]);printf("%02x len = %d\n",len,len);
	    fprintf(logfile,"长度 = %d\n ",len);
	    mac[0]=buffer[45];printf("mac : %02x ",mac[0]);
	    mac[1]=buffer[46];printf("%02x ",mac[1]);
	    mac[2]=buffer[47];printf("%02x ",mac[2]);
	    mac[3]=buffer[48];printf("%02x ",mac[3]);
	    mac[4]=buffer[49];printf("%02x ",mac[4]);
	    mac[5]=buffer[50];printf("%02x\n",mac[5]);printf("ip = 10.%d.%d.%d\n",mac[3],mac[4],mac[5]);
	    fprintf(logfile,"本端mac : %02x ",mac[0]);
	    fprintf(logfile,"%02x ",mac[1]);
	    fprintf(logfile,"%02x ",mac[2]);
	    fprintf(logfile,"%02x ",mac[3]);
	    fprintf(logfile,"%02x ",mac[4]);
	    fprintf(logfile,"%02x\n",mac[5]);
	    tx_num=getINT16(&buffer[51]);printf("%02x tx_num = %d\n",tx_num,tx_num);
	    fprintf(logfile,"发送者数目 = %d\n",tx_num);
	    //stu.ts.tv_sec = getINT32(&buffer[59]);printf("%02x %02x %02x %02x %02x 秒 = %d\n",buffer[59],buffer[60],buffer[61],buffer[62],stu.ts.tv_sec,stu.ts.tv_sec);
	    //stu.ts.tv_usec = getINT32(&buffer[63]);printf("%02x %02x %02x %02x %02x 微秒 = %d\n",buffer[63],buffer[64],buffer[65],buffer[66],stu.ts.tv_usec,stu.ts.tv_usec);
	    //stu.tx_bytes = getINT32(&buffer[71]);printf("%02x %02x %02x %02x %02x 发送字节数 = %d\n",buffer[71],buffer[72],buffer[73],buffer[74],stu.tx_bytes,stu.tx_bytes);
	    int count=0;
	    for(int i=87;i<size;i+=35){
		fprintf(logfile,"No : %d\n",++count);
		stu.peerID[0]=buffer[i-34];printf("another mac : %02x ",stu.peerID[0]);
		stu.peerID[1]=buffer[i-33];printf("%02x ",stu.peerID[1]);
		stu.peerID[2]=buffer[i-32];printf("%02x ",stu.peerID[2]);
		stu.peerID[3]=buffer[i-31];printf("%02x ",stu.peerID[3]);
		stu.peerID[4]=buffer[i-30];printf("%02x ",stu.peerID[4]);
		stu.peerID[5]=buffer[i-29];printf("%02x\n",stu.peerID[5]);
		fprintf(logfile,"对端mac : %02x ",stu.peerID[0]);
		fprintf(logfile,"%02x ",stu.peerID[1]);
		fprintf(logfile,"%02x ",stu.peerID[2]);
		fprintf(logfile,"%02x ",stu.peerID[3]);
		fprintf(logfile,"%02x ",stu.peerID[4]);
		fprintf(logfile,"%02x\n",stu.peerID[5]);
		printf("ip = 10.%d.%d.%d\n",stu.peerID[3],stu.peerID[4],stu.peerID[5]);
		stu.ts.tv_sec = getINT32(&buffer[i-28]);
		stu.ts.tv_usec = getINT32(&buffer[i-24]);printf("时间 = %ld.%ld\n",stu.ts.tv_sec,stu.ts.tv_usec);
		fprintf(logfile,"时间 : %ld.%ld\n",stu.ts.tv_sec,stu.ts.tv_usec);
		stu.tx_packets = getINT32(&buffer[i-20]);printf("%02x %02x %02x %02x   %02x 发送报文数 = %d\n",buffer[i-20],buffer[i-19],buffer[i-18],buffer[i-17],stu.tx_packets,stu.tx_packets);
		fprintf(logfile,"发送报文数 = %d\n",stu.tx_packets);
		stu.tx_bytes = getINT32(&buffer[i-16]);printf("%02x %02x %02x %02x   %02x 发送字节数 = %d\n",buffer[i-16],buffer[i-15],buffer[i-14],buffer[i-13],stu.tx_bytes,stu.tx_bytes);
		fprintf(logfile,"发送字节数 = %d\n",stu.tx_bytes);
		stu.tx_retries = getINT32(&buffer[i-12]);printf("%02x %02x %02x %02x   %02x 重传次数 = %d\n",buffer[i-12],buffer[i-11],buffer[i-10],buffer[i-9],stu.tx_retries,stu.tx_retries);
		fprintf(logfile,"重传次数 = %d\n",stu.tx_retries);
		stu.tx_fail = getINT32(&buffer[i-8]);printf("%02x %02x %02x %02x   %02x 失败次数 = %d\n",buffer[i-8],buffer[i-7],buffer[i-6],buffer[i-5],stu.tx_fail,stu.tx_fail);
		fprintf(logfile,"失败次数 = %d\n",stu.tx_fail);
		stu.tx_bitrate = getINT32(&buffer[i-4]);printf("%02x %02x %02x %02x   %02x 速率 = %d\n",buffer[i-4],buffer[i-3],buffer[i-2],buffer[i-1],stu.tx_bitrate,stu.tx_bitrate);
		fprintf(logfile,"速率 = %d\n",stu.tx_bitrate);
		stu.signal = buffer[i];printf("%02x 信号强度 = %d\n",stu.signal,stu.signal);
		fprintf(logfile,"信号强度 = %d\n",stu.signal);fprintf(logfile,"\n########################\n");
	    }
	}
	int a=0;
	/*while(a<=size){
	    //fprintf(logfile,"data[%d] = %02x\t",a,data[a]);
	    fprintf(logfile,"%02x ",data[a]);
	    a++;
	}
	fprintf(logfile,"\n########################\n");*/
    }
}


