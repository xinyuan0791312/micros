#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<sys/types.h>
#include<sys/socket.h>
#include<sys/wait.h>
#include<netinet/in.h>
#include<arpa/inet.h>
#include<errno.h>
#include <unistd.h>
int main(){
    char msg[128];
    //01 00 17 77 6C 61 6E 31 5F 6D 77 64 73 00 00 00 00 00 00 00 00 00 00 00 00 01
    msg[0] = 0x01; 
    msg[1] = 0x00;
    msg[2] = 0x17;
    msg[3] = 0x77;
    msg[4] = 0x6C;
    msg[5] = 0x61;
    msg[6] = 0x6E;
    msg[7] = 0x31;
    msg[8] = 0x5F;
    msg[9] = 0x6D;
    msg[10] = 0x77;
    msg[11] = 0x64;
    msg[12] = 0x73;
    msg[13] = 0x00;
    msg[14] = 0x00;
    msg[15] = 0x00;
    msg[16] = 0x00;
    msg[17] = 0x00;
    msg[18] = 0x00;
    msg[19] = 0x00;
    msg[20] = 0x00;
    msg[21] = 0x00;
    msg[22] = 0x00;
    msg[23] = 0x00;  //0x01  stop
    msg[24] = 0x00;
    msg[25] = 0x01;
    int brdcFd;
    if((brdcFd = socket(PF_INET, SOCK_DGRAM, 0)) == -1){
	printf("socket fail\n");
	return -1;
    }

    struct sockaddr_in local;
    memset(&local, 0, sizeof(struct sockaddr_in));
    local.sin_family = AF_INET;
    local.sin_addr.s_addr = INADDR_ANY;
    local.sin_port = htons(10008);//UDP 广播包 本地端口
    socklen_t local_len = sizeof(struct sockaddr);
    if(bind(brdcFd, (struct sockaddr *)&local, sizeof(local)))//绑定端口
    {
        printf("####L(%d) client bind port failed!\n", __LINE__);
        close(brdcFd);//关闭socket
        exit(-1);
    }

    int optval = 1;//这个值一定要设置，否则可能导致sendto()失败
    setsockopt(brdcFd, SOL_SOCKET, SO_BROADCAST | SO_REUSEADDR, &optval, sizeof(int));
    struct sockaddr_in theirAddr;
    memset(&theirAddr, 0, sizeof(struct sockaddr_in));
    theirAddr.sin_family = AF_INET;
    theirAddr.sin_addr.s_addr = inet_addr("10.3.116.238"); //网线直连通信板ip
    theirAddr.sin_port = htons(10008);
    int sendBytes;
    if((sendBytes = sendto(brdcFd, msg, 26, 0,
	(struct sockaddr *)&theirAddr, sizeof(struct sockaddr))) == -1){
	printf("sendto fail, errno=%d\n", errno);
	return -1;
    }
    printf("msg=%s, sendBytes=%d\n", msg, sendBytes);
    close(brdcFd);
    return 0;
}
