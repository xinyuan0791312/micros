/* ==================================================================
*
* Copyright (c) 2016-2019, micROS Group, NIIDT, TAIIC, HPCL.
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions 
* are met: 
*
* 1. Redistributions of source code must retain the above copyright 
*     notice, this list of conditions and the following disclaimer. 
* 2. Redistributions in binary form must reproduce the above copyright 
*     notice, this list of conditions and the following disclaimer in the 
*     documentation and/or other materials provided with the 
*     distribution. 
* 3. All advertising materials mentioning features or use of this software 
*     must display the following acknowledgement: 
*     This product includes software developed by the micROS Group. and 
*     its contributors. 
* 4. Neither the name of the Group nor the names of its contributors may 
*     be used to endorse or promote products derived from this software 
*     without specific prior written permission. 
*
* THIS SOFTWARE IS PROVIDED BY MICROS,GROUP AND CONTRIBUTORS
* ''AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT
* NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
* FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL 
* THE MICROS,GROUP OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS 
* OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) 
* HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
* OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN 
* IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
* ===================================================================
*
* Author: Bin Di, Chunlian Fu, Junling Gao, Ruihao Li, Wei Yi, Zaile Jiang.
*         (in alphabetical order)
*
*/
#ifndef MICROS_PHYSICS_SOFTBUS_H
#define MICROS_PHYSICS_SOFTBUS_H

#include <ros/ros.h>
#include <string>
#include "device_core/device_base.h"
#include <pluginlib/class_loader.h>
#include "std_msgs/String.h"
#include <boost/thread.hpp>
#include <queue>
#include <map>
#include "physics_ros_msgs/DeviceParamConfig.h"

namespace micros
{
namespace physics
{
namespace physics_softbus
{

const int gDeviceMax = 12;

enum EDeviceActionType {
    PARAM_CONFIG = 0,  ///< Config the parameters of devices
    ZED_OPEN = 1,  ///< Open zed camera
    ZED_CLOSE = 2,  ///< Close zed camera
    KINECT_OPEN = 3,  ///< Open kinect camera
    KINECT_CLOSE = 4,  ///< Close zed camera
    IMU_OPEN = 5,  ///< Open xsens imu
    IMU_CLOSE = 6,   ///< Close xsens imu
    VELODYNE_OPEN = 7,  ///< Open velodyne
    VELODYNE_CLOSE = 8,   ///< Close velodyne
    FLIR_OPEN,   ///< Open flir camera
    FLIR_CLOSE   ///< Close flir camera
};

class PhysicsSoftbus
{
public:
	PhysicsSoftbus();
	virtual ~PhysicsSoftbus();
	bool execute();
	void dynamicListener();

private:
	void initDevices();
	void listenerCallback(const std_msgs::String::ConstPtr &msg);

	pluginlib::ClassLoader<micros::physics::device_core::DeviceBase> _deviceLoader;

	boost::shared_ptr<device_core::DeviceBase> _pRegisteredDevice[gDeviceMax];
	std::map<std::string, int> _deviceDict;
	int _registeredDeviceNum;

	ros::NodeHandle _nh;

	/**
	 * @brief Delete plugin thread of a device
	 * 
	 * @param[in] aDeviceName Device name
	 */
	void unloadDevicePlugin(std::string aDeviceName);

	boost::shared_ptr<boost::thread> _pThreadList[gDeviceMax];

	std::queue<physics_ros_msgs::DeviceParamConfig> _deviceActionQueue;

	boost::mutex _mutex;

	/**
	 * @brief Check whether device plugin is loaded
	 * 
	 * @param[in] aDeviceName Device name
	 * @return true Device is loaded
	 * @return false Device isn't loaded
	 */
	bool isDeviceLoaded(const std::string aDeviceName);

	std::map<std::string, EDeviceActionType> _msgHistory;

	/**
	 * @brief Subscribe ros topic '/physics_softbus/config_device_param'
	 * 
	 * @param[in] msg Msg from this topic
	 */
	void paramConfigCallback(const physics_ros_msgs::DeviceParamConfig::ConstPtr &msg);

	/**
	 * @brief Create physics_ros_msgs::DeviceParamConfig msg and push into _deviceActionQueue
	 * 
	 * @param[in] type Device action type
	 * @param[in] name Device name
	 */
	void push2DeviceActionQueue(const std::string &name, EDeviceActionType type);
};

}
}
}

#endif
