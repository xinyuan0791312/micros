/* ==================================================================
*
* Copyright (c) 2016-2019, micROS Group, NIIDT, TAIIC, HPCL.
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions 
* are met: 
*
* 1. Redistributions of source code must retain the above copyright 
*     notice, this list of conditions and the following disclaimer. 
* 2. Redistributions in binary form must reproduce the above copyright 
*     notice, this list of conditions and the following disclaimer in the 
*     documentation and/or other materials provided with the 
*     distribution. 
* 3. All advertising materials mentioning features or use of this software 
*     must display the following acknowledgement: 
*     This product includes software developed by the micROS Group. and 
*     its contributors. 
* 4. Neither the name of the Group nor the names of its contributors may 
*     be used to endorse or promote products derived from this software 
*     without specific prior written permission. 
*
* THIS SOFTWARE IS PROVIDED BY MICROS,GROUP AND CONTRIBUTORS
* ''AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT
* NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
* FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL 
* THE MICROS,GROUP OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS 
* OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) 
* HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
* OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN 
* IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
* ===================================================================
*
* Author: Bin Di, Chunlian Fu, Junling Gao, Ruihao Li, Wei Yi, Zaile Jiang.
*         (in alphabetical order)
*
*/
#include "physics_softbus/physics_softbus.h"

//int ZED_STATUS_FLAG = 0;  // 0:close 1:open
//std::string ZED_ACTION="Close";  // Open, Close

namespace micros
{
namespace physics
{
namespace physics_softbus
{

const std::string gZedDevice = "zed";
const std::string gKinectDevice = "kinect";
const std::string gImuDevice = "imu";
const std::string gVelodyneDevice = "velodyne";
const std::string gFlirDevice = "flir";

PhysicsSoftbus::PhysicsSoftbus() : _deviceLoader("device_core", "micros::physics::device_core::DeviceBase"),
                                   _registeredDeviceNum(0),
                                   _nh("~") ///< The namespace of this ros::NodeHandle is "physics_softbus"
{
  initDevices();
}

PhysicsSoftbus::~PhysicsSoftbus()
{
  for (int i = 0; i < _registeredDeviceNum; i++)
  {
    _pRegisteredDevice[i]->stop();
    _pRegisteredDevice[i].reset();
  }
  _registeredDeviceNum = 0;
  _deviceDict.clear();
}

bool PhysicsSoftbus::execute()
{
  // for (int i = 0; i < _registeredDeviceNum; i++)
  // {
  //   if (!_pRegisteredDevice[i]->start())
  //     return false;
  // }
  dynamicListener(); ///< Start dynamicListener
  return true;
}

void PhysicsSoftbus::initDevices()
{
  std::string device_name = "";
  //initialize the devices
  while (_nh.getParam("devices/device" + std::to_string(_registeredDeviceNum), device_name) && (_registeredDeviceNum < gDeviceMax))
  {
    ROS_INFO("Get device name: %s", device_name.c_str());
    try
    {
      _pRegisteredDevice[_registeredDeviceNum] = _deviceLoader.createInstance(device_name);
      _pRegisteredDevice[_registeredDeviceNum]->init();
    }
    catch (const pluginlib::PluginlibException &ex)
    {
      ROS_FATAL("Failed to create the %s device, are you sure it is properly registered and that the containing library is built? Exception: %s", device_name.c_str(), ex.what());
      exit(1);
    }
    _deviceDict[device_name] = _registeredDeviceNum;
    _registeredDeviceNum++;
  }
}

void PhysicsSoftbus::unloadDevicePlugin(std::string aDeviceName)
{
  if (isDeviceLoaded(aDeviceName))
  {
    int num = _deviceDict[aDeviceName];
    _pRegisteredDevice[num]->stop();
    if (aDeviceName == gZedDevice || aDeviceName == gImuDevice || aDeviceName == gFlirDevice)
    {
      ROS_INFO("[Physics Softbus] %s device is unloading.", aDeviceName.c_str());
      ///< Just release thread, don't release plugin object, bcz the service in object cann't release
      _pThreadList[num]->interrupt();
      _pThreadList[num]->join();
      _pThreadList[num].reset();
    }
    else
    {
      _pRegisteredDevice[num].reset();
      _deviceDict.erase(aDeviceName);
      _registeredDeviceNum--;
    }
  }
  ROS_INFO("[Physics Softbus] finish unload device %s", aDeviceName.c_str());
}

/***
 * ZED camera driver control callback function,
 * receive the trigger signal and change ZED_ACTION flag
 */
void PhysicsSoftbus::listenerCallback(const std_msgs::String::ConstPtr &msg)
{
  // ROS_INFO("I heard %s", msg->data.c_str());
  if (msg->data == "Open_ZED_Camera" && (_msgHistory.find(gZedDevice) == _msgHistory.end() || _msgHistory[gZedDevice] == ZED_CLOSE))
  {
    ROS_INFO("I heard %s and find camera %s", msg->data.c_str(), gZedDevice.c_str());
    _msgHistory[gZedDevice] = ZED_OPEN;
    push2DeviceActionQueue(gZedDevice, ZED_OPEN);
  }
  else if (msg->data == "Close_ZED_Camera" && _msgHistory.find(gZedDevice) != _msgHistory.end() && _msgHistory[gZedDevice] == ZED_OPEN)
  {
    ROS_INFO("I heard %s and find camera %s", msg->data.c_str(), gZedDevice.c_str());
    _msgHistory[gZedDevice] = ZED_CLOSE;
    push2DeviceActionQueue(gZedDevice, ZED_CLOSE);
  }
  else if (msg->data == "Open_Kinect_Camera" && (_msgHistory.find(gKinectDevice) == _msgHistory.end() || _msgHistory[gKinectDevice] == KINECT_CLOSE))
  {
    ROS_INFO("I heard %s and find camera %s", msg->data.c_str(), gKinectDevice.c_str());
    _msgHistory[gKinectDevice] = KINECT_OPEN;
    push2DeviceActionQueue(gKinectDevice, KINECT_OPEN);
  }
  else if (msg->data == "Close_Kinect_Camera" && _msgHistory.find(gKinectDevice) != _msgHistory.end() && _msgHistory[gKinectDevice] == KINECT_OPEN)
  {
    ROS_INFO("I heard %s and find camera %s", msg->data.c_str(), gKinectDevice.c_str());
    _msgHistory[gKinectDevice] = KINECT_CLOSE;
    push2DeviceActionQueue(gKinectDevice, KINECT_CLOSE);
  }
  else if (msg->data == "Open_Xsens_IMU" && (_msgHistory.find(gImuDevice) == _msgHistory.end() || _msgHistory[gImuDevice] == IMU_CLOSE))
  {
    ROS_INFO("I heard %s and find %s", msg->data.c_str(), gImuDevice.c_str());
    _msgHistory[gImuDevice] = IMU_OPEN;
    push2DeviceActionQueue(gImuDevice, IMU_OPEN);
  }
  else if (msg->data == "Close_Xsens_IMU" && _msgHistory.find(gImuDevice) != _msgHistory.end() && _msgHistory[gImuDevice] == IMU_OPEN)
  {
    ROS_INFO("I heard %s and find %s", msg->data.c_str(), gImuDevice.c_str());
    _msgHistory[gImuDevice] = IMU_CLOSE;
    push2DeviceActionQueue(gImuDevice, IMU_CLOSE);
  }
  else if (msg->data == "Open_Velodyne" && (_msgHistory.find(gVelodyneDevice) == _msgHistory.end() || _msgHistory[gVelodyneDevice] == VELODYNE_CLOSE))
  {
    ROS_INFO("I heard %s and find %s", msg->data.c_str(), gVelodyneDevice.c_str());
    _msgHistory[gVelodyneDevice] = VELODYNE_OPEN;
    push2DeviceActionQueue(gVelodyneDevice, VELODYNE_OPEN);
  }
  else if (msg->data == "Close_Velodyne" && _msgHistory.find(gVelodyneDevice) != _msgHistory.end() && _msgHistory[gVelodyneDevice] == VELODYNE_OPEN)
  {
    ROS_INFO("I heard %s and find %s", msg->data.c_str(), gVelodyneDevice.c_str());
    _msgHistory[gVelodyneDevice] = VELODYNE_CLOSE;
    push2DeviceActionQueue(gVelodyneDevice, VELODYNE_CLOSE);
  }
  else if (msg->data == "Open_Flir_Camera" && (_msgHistory.find(gFlirDevice) == _msgHistory.end() || _msgHistory[gFlirDevice] == FLIR_CLOSE))
  {
    ROS_INFO("I heard %s and find %s", msg->data.c_str(), gFlirDevice.c_str());
    _msgHistory[gFlirDevice] = FLIR_OPEN;
    push2DeviceActionQueue(gFlirDevice, FLIR_OPEN);
  }
  else if (msg->data == "Close_Flir_Camera" && _msgHistory.find(gFlirDevice) != _msgHistory.end() && _msgHistory[gFlirDevice] == FLIR_OPEN)
  {
    ROS_INFO("I heard %s and find %s", msg->data.c_str(), gFlirDevice.c_str());
    _msgHistory[gFlirDevice] = FLIR_CLOSE;
    push2DeviceActionQueue(gFlirDevice, FLIR_CLOSE);
  }
}

void PhysicsSoftbus::push2DeviceActionQueue(const std::string &name, EDeviceActionType type)
{
  physics_ros_msgs::DeviceParamConfig msg;
  msg.name = name;
  msg.type = type;
  boost::unique_lock<boost::mutex> lock(_mutex);
  _deviceActionQueue.push(msg);
}

/*
* Dynamic listener to control the device drivers
*/
void PhysicsSoftbus::dynamicListener()
{
  ros::Subscriber sub = _nh.subscribe("/physics_softbus/driver_trigger_chatter", 1000, &PhysicsSoftbus::listenerCallback, this);
  ros::Subscriber _configSub = _nh.subscribe("/physics_softbus/config_device_param", 1000, &PhysicsSoftbus::paramConfigCallback, this);
  bool flag;
  physics_ros_msgs::DeviceParamConfig msg;
  std::string device;
  while (ros::ok())
  {
    ros::spinOnce();
    flag = false;
    {
      // Wait and hang-up the thread when _deviceActionQueue is empty.
      boost::unique_lock<boost::mutex> lock(_mutex);
      if (!_deviceActionQueue.empty())
      {
        // _cv.wait(lock);
        msg = _deviceActionQueue.front();
        _deviceActionQueue.pop();
        flag = true;
      }
    }

    if (!flag)
    {
      usleep(100000);
      continue;
    }

    switch (msg.type)
    {
    case PARAM_CONFIG:
      if (msg.name == gZedDevice && isDeviceLoaded(msg.name))
      {
        int num = _deviceDict[msg.name];
        _pRegisteredDevice[num]->setParams(msg.paramKeys, msg.paramValues);
      }
      else if (msg.name == gKinectDevice && isDeviceLoaded(msg.name))
      {
        int num = _deviceDict[msg.name];
        _pRegisteredDevice[num]->setParams(msg.paramKeys, msg.paramValues);
      }
      break;
    case IMU_OPEN:
    case KINECT_OPEN:
    case FLIR_OPEN:
      // Imu, Kinect and Flir need time to calibration
      sleep(10);
    case ZED_OPEN:
    case VELODYNE_OPEN:
      try
      {
        int num = _registeredDeviceNum;
        if (!isDeviceLoaded(msg.name))
        {
          _pRegisteredDevice[num] = _deviceLoader.createInstance(msg.name);
          _pRegisteredDevice[num]->init();
          _deviceDict[msg.name] = num;
          _registeredDeviceNum++;
        }
        else
        {
          num = _deviceDict[msg.name];
        }
        ROS_INFO("===============come to create thread!================");
        if (msg.type == KINECT_OPEN || msg.type == VELODYNE_OPEN)
        {
          _pRegisteredDevice[num]->start();
        }
        else
          _pThreadList[num] = boost::shared_ptr<boost::thread>(new boost::thread(&device_core::DeviceBase::start, _pRegisteredDevice[num]));

        ROS_INFO("[Physics Softbus] Open %s camera", msg.name.c_str());
      }
      catch (pluginlib::PluginlibException &ex)
      {
        ROS_ERROR("[Physics Softbus] The plugin failed to load for some reason. Error: %s", ex.what());
      }
      break;
    default:
      ROS_INFO("[Physics Softbus] Close %s camera", msg.name.c_str());
      unloadDevicePlugin(msg.name);
      break;
    }
  }
}

bool PhysicsSoftbus::isDeviceLoaded(const std::string aDeviceName)
{
  if (_deviceDict.find(aDeviceName) != _deviceDict.end())
    return true;
  return false;
}

void PhysicsSoftbus::paramConfigCallback(const physics_ros_msgs::DeviceParamConfig::ConstPtr &msg)
{
  ROS_INFO("[Physics Softbus] Get msg from ros topic /physics_softbus/config_device_param', device name : %s, msg type %d", msg->name.c_str(), msg->type);
  boost::unique_lock<boost::mutex> lock(_mutex);
  _deviceActionQueue.push(*msg);
}

} // namespace physics_softbus
} // namespace physics
} // namespace micros
