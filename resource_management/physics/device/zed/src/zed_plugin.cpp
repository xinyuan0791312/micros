#include <zed_plugin.h>
#include <cmath>
#include <pluginlib/class_list_macros.h>
#include <stdio.h>
#include <string>
#include <iostream>
#include <fstream>
#include <opencv2/opencv.hpp>

#include <ros/ros.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/CameraInfo.h>
#include <sensor_msgs/image_encodings.h>
#include <sensor_msgs/distortion_models.h>
#include <image_transport/image_transport.h>
#include <boost/thread.hpp> 
#include <boost/optional.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/ini_parser.hpp>
#include <camera_info_manager/camera_info_manager.h>

namespace micros
{
namespace physics
{
namespace zed
{
  Zed::~Zed(){
    _pZed.reset();
  }

  bool Zed::init()
  {
    _pZed = boost::shared_ptr<arti::ZedCameraROS>(new arti::ZedCameraROS());
    return true;
  }
  
  bool Zed::pause()
  {
    return true;
  }
  
  bool Zed::stop()
  {
    return true;
  }

  void Zed::setParams(std::vector<std::string> &keys, std::vector<std::string> &values)
  {
    _pZed->setParams(keys, values);
  }


  bool Zed::start()
  {
    try
    {
      _pZed->StartZedCameraROS();
    }
    catch(std::runtime_error& e)
    {
      ROS_INFO("[zed_plugin] runtime error!");
      // ros::shutdown();
      return false;
    }
    return true;
  }
  
  PLUGINLIB_EXPORT_CLASS(micros::physics::zed::Zed, micros::physics::device_core::DeviceBase)

}
}
}




