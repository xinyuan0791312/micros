#ifndef ZED_CPU_ROS_ZED_CPU_ROS_TEST_H_
#define ZED_CPU_ROS_ZED_CPU_ROS_TEST_H_
#include <stdio.h>
#include <string>
#include <opencv2/opencv.hpp>
#include <ros/ros.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/CameraInfo.h>
#include <sensor_msgs/image_encodings.h>
#include <sensor_msgs/distortion_models.h>
#include <image_transport/image_transport.h>
#include <boost/optional.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/ini_parser.hpp>
#include <camera_info_manager/camera_info_manager.h>
#include <boost/thread/shared_mutex.hpp>
#include "physics_ros_msgs/ZedParamConfig.h"

namespace arti
{

class StereoCamera
{
public:
  /**
   * @brief      { stereo camera driver }
   *
   * @param[in]  resolution  The resolution
   * @param[in]  frame_rate  The frame rate
   */
  StereoCamera(int device_id, int resolution, double frame_rate);


  ~StereoCamera();

  /**
   * @brief      Sets the resolution.
   *
   * @param[in]  type  The type
   */
  void setResolution(int type);


  /**
   * @brief      Sets the frame rate.
   *
   * @param[in]  frame_rate  The frame rate
   */
  void setFrameRate(double frame_rate);


  /**
   * @brief      Gets the images.
   *
   * @param      left_image   The left image
   * @param      right_image  The right image
   *
   * @return     The images.
   */
  bool getImages(cv::Mat& left_image, cv::Mat& right_image);


private:
  cv::VideoCapture* camera_;
  int width_;
  int height_;
  double frame_rate_;
  bool cv_three_;
  boost::shared_mutex _mutex;
};

/**
 * @brief       the camera ros warpper class
 */
class ZedCameraROS
{
public:

  ZedCameraROS();
  
  /**
   * @brief      { function_description }
   *
   * @param[in]  resolution  The resolution
   * @param[in]  frame_rate  The frame rate
   */
  void StartZedCameraROS();


  /**
   * @brief      Gets the camera information From Zed config.
   *
   * @param[in]  config_file         The configuration file
   * @param[in]  resolution          The resolution
   * @param[in]  left_cam_info_msg   The left camera information message
   * @param[in]  right_cam_info_msg  The right camera information message
   */
  void getZedCameraInfo(std::string config_file, int resolution, sensor_msgs::CameraInfo& left_info,
                        sensor_msgs::CameraInfo& right_info);


  /**
   * @brief      { publish camera info }
   *
   * @param[in]  pub_cam_info  The pub camera information
   * @param[in]  cam_info_msg  The camera information message
   * @param[in]  now           The now
   */
  void publishCamInfo(const ros::Publisher& pub_cam_info, sensor_msgs::CameraInfo& cam_info_msg, ros::Time now);


  /**
   * @brief      { publish image }
   *
   * @param[in]  img           The image
   * @param      img_pub       The image pub
   * @param[in]  img_frame_id  The image frame identifier
   * @param[in]  t             { parameter_description }
   * @param[in]  encoding      image_transport encoding
   */
  void publishImage(const cv::Mat& img, image_transport::Publisher& img_pub, const std::string& img_frame_id,
                    ros::Time t);

  /**
   * @brief      Correct frame rate according to resolution
   *
   * @param[in]  resolution          The resolution
   * @param      frame_rate   			 The camera frame rate
   */
  void correctFramerate(int resolution, double& frame_rate);

  void setParams(std::vector<std::string> &keys, std::vector<std::string> &values);

private:
  ros::Subscriber _paramSub;
  void configParamCallBack(const physics_ros_msgs::ZedParamConfig::ConstPtr &msg);

  int device_id_, resolution_;
  double frame_rate_;
  bool show_image_, use_zed_config_;
  double width_, height_;
  std::string left_frame_id_, right_frame_id_;
  std::string config_file_location_;
  std::string encoding_;

  ros::NodeHandle _nh;

  image_transport::Publisher left_image_pub;
  image_transport::Publisher right_image_pub;
  ros::Publisher left_cam_info_pub;
  ros::Publisher right_cam_info_pub;

  boost::shared_ptr<camera_info_manager::CameraInfoManager> _pLeftManager;
  boost::shared_ptr<camera_info_manager::CameraInfoManager> _pRightManager;

  boost::shared_ptr<StereoCamera> _stereoCamera;
  boost::shared_mutex _mutex;

  bool _resetParam;
  double _paramWidth, _paramHeight;
};
}
#endif

