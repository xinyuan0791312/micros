#ifndef PLUGINLIB_ZED_TEST__ZED_PLUGINS_H_
#define PLUGINLIB_ZED_TEST__ZED_PLUGINS_H_
#include <device_core/device_base.h>
#include <cmath>
#include <boost/thread.hpp> 
#include "zed_cpu_ros_class.h"
#include <vector>

namespace micros
{
namespace physics
{
namespace zed
{
  
  class Zed : public micros::physics::device_core::DeviceBase
  {
    public:
      ~Zed();

      bool init();
      bool pause();
      bool start();
      bool stop();

      void setParams(std::vector<std::string> &keys, std::vector<std::string> &values);
    private:
      boost::shared_ptr<arti::ZedCameraROS> _pZed;
  };

}
}
}

#endif

