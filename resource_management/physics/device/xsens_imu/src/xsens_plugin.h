#ifndef PLUGINLIB__XSENS_PLUGINS_H_
#define PLUGINLIB__XSENS_PLUGINS_H_

#include <stdio.h>
#include <device_core/device_base.h>
#include <cmath>
#include <boost/thread.hpp>

#include <ros/ros.h>
#include "xdainterface.h"
#include <iostream>
#include <stdexcept>
#include <string>
//#include <nodelet/loader.h>

using std::chrono::milliseconds;

Journaller *gJournal = 0;

namespace micros
{
namespace physics
{
namespace imu
{

class XsensPlugins : public micros::physics::device_core::DeviceBase
{
public:
    XsensPlugins();
    ~XsensPlugins();

    bool init();
    bool pause();
    bool start();
    bool stop();

    //Journaller *_pgJournal;
    XdaInterface *_pxdaInterface;
    ros::NodeHandle _xsensNode;
};

}
}
}
#endif