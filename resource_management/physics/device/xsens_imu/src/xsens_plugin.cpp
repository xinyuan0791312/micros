#include "xsens_plugin.h"
#include <ros/ros.h>
#include <pluginlib/class_list_macros.h>

namespace micros
{
namespace physics
{
namespace imu
{

XsensPlugins::XsensPlugins()
{
    //_pgJournal = 0;
    _pxdaInterface = new XdaInterface();
    _pxdaInterface->registerPublishers(_xsensNode);
}

XsensPlugins::~XsensPlugins()
{
    delete _pxdaInterface;
}

bool XsensPlugins::init()
{
    return true;
}

bool XsensPlugins::pause()
{
    return true;
}

bool XsensPlugins::stop()
{
    _pxdaInterface->close();
    return true;
}

bool XsensPlugins::start()
{
    try
    {
        if (!_pxdaInterface->connectDevice())
        {
            ROS_INFO("Can not connect to Xsens device, please check the usb port");
            return false;
        }

        if (!_pxdaInterface->prepare())
        {
            ROS_INFO("The XdaInterface is not prepared correctly");
            return false;
        }

        while (ros::ok())
        {
            try
            {
                boost::this_thread::sleep(boost::posix_time::seconds(0.001));
                _pxdaInterface->spinFor(milliseconds(100));
                ros::spinOnce();
            }
            catch (boost::thread_interrupted &)
            {
                ROS_INFO("imu thread_interrupted");
                break;
            }
        }
    }
    catch (std::runtime_error &e)
    {
        // ros::shutdown();
        return EXIT_FAILURE;
    }
    return true;
}

PLUGINLIB_EXPORT_CLASS(micros::physics::imu::XsensPlugins, micros::physics::device_core::DeviceBase)

}
}
}
