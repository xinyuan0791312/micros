1. 安装驱动（直接下载驱动源代码，此步骤可省略）

  sudo apt-get install ros-kinetic-velodyne  

2. 配置网络，连接到激光雷达（此步骤可省略）：

  sudo ifconfig eth0 up
  sudo ip addr add 192.168.1.200 dev eth0
  sudo route add -net 192.168.1.0 netmask 255.255.255.0 dev eth0

然后打开浏览器输入192.168.1.201可以看到激光雷达的配置文件。

3. 新建一个ROS的工程

  mkdir -p catkin_velodyne/src  
  cd catkin_velodyne/src  
  git clone https://github.com/ros-drivers/velodyne.git  
  cd ..  
  catkin_make  
  source devel/setup.bash  

4. 把Velodyne XML文件转成ROS节点的YAML文件（转换后生成VLP-16.yaml，/home/XXXX/velodyne/velodyne_pointcloud/params已有，可省略）:

  rosrun velodyne_pointcloud gen_calibration.py ~/Desktop/VLP-16.xml  （根据自己保存的目录来）

5. 加载：

  roslaunch velodyne_pointcloud VLP16_points.launch calibration:=/home/XXXX/VLP-16.yaml  （根据自己保存的目录来）

6. 实时显示点云图：

  rosrun rviz rviz -f velodyne  

然后在rviz中点Add by topic，增加PointCloud2，这样就可以实时显示获取的3D点云图。
---------------------------------------------------------------  
  也可以

  rosrun rviz rviz

之后在手动设置Fixed Frame= velodyne
7. 记录数据：

  rosbag record -O saved_filename /velodyne_points  


