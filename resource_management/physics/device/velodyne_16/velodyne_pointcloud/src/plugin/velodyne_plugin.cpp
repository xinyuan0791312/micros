#include "velodyne_plugin.h"
#include <ros/ros.h>
#include <pluginlib/class_list_macros.h>

namespace micros
{
namespace physics
{
namespace velodyne
{

  VelodynePlugins::VelodynePlugins():_manager(true)
  {

  }

  bool VelodynePlugins::init()
  {
    return true;
  }
  
  bool VelodynePlugins::pause()
  {
    return true;
  }
  
  bool VelodynePlugins::stop()
  {
    // _manager.unload("/velodyne_nodelet_manager");
    _manager.unload("/velodyne_nodelet_manager_laserscan");
    _manager.unload("/velodyne_nodelet_manager_pointcloud");
    _manager.unload("/velodyne_nodelet_manager_driver");
    return true;
  }

  bool VelodynePlugins::start()
  {
    try
    {
      nodelet::M_string remappings;
      nodelet::V_string my_argv;
      // Driver nodelet
      _manager.load("/velodyne_nodelet_manager_driver", "velodyne_driver/DriverNodelet", remappings, my_argv);
      _manager.load("/velodyne_nodelet_manager_pointcloud", "velodyne_pointcloud/CloudNodelet", remappings, my_argv);
      _manager.load("/velodyne_nodelet_manager_laserscan", "velodyne_laserscan/LaserScanNodelet", remappings, my_argv);
    }
    catch (std::runtime_error& e)
    {
      // ros::shutdown();
      return EXIT_FAILURE;
    }
    return true;
  }


PLUGINLIB_EXPORT_CLASS(micros::physics::velodyne::VelodynePlugins, micros::physics::device_core::DeviceBase)

}
}
}
