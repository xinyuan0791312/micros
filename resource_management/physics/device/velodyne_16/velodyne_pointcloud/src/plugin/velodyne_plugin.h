#ifndef PLUGINLIB__VELODYNE_PLUGINS_H_
#define PLUGINLIB__VELODYNE_PLUGINS_H_

#include <stdio.h>
#include <device_core/device_base.h>
#include <cmath>
#include <boost/thread.hpp> 
#include <nodelet/loader.h>

namespace micros
{
namespace physics
{
namespace velodyne
{

  class VelodynePlugins : public micros::physics::device_core::DeviceBase
  {
    public:
      VelodynePlugins();

      bool init();
      bool pause();
      bool start();
      bool stop();

      nodelet::Loader _manager; // Bring up manager ROS API
  };

}
}
}
#endif