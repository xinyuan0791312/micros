#include <stdio.h>
#include <ros/ros.h>
#include <nodelet/loader.h>

int main(int argc, char** argv)
{
  ros::init(argc, argv, "velodyne_nodelet_manager");

  try
  {
    nodelet::Loader manager(true); // Bring up manager ROS API
    nodelet::M_string remappings;
    nodelet::V_string my_argv;

    // Driver nodelet
    manager.load("/velodyne_nodelet_manager_driver", "velodyne_driver/DriverNodelet", remappings, my_argv);
    manager.load("/velodyne_nodelet_manager_pointcloud", "velodyne_pointcloud/CloudNodelet", remappings, my_argv);
    manager.load("/velodyne_nodelet_manager_laserscan", "velodyne_laserscan/LaserScanNodelet", remappings, my_argv);

    // Manager service calls are on global callback queue
    while (ros::ok())
    {
      ros::spinOnce();
    }
  }
  catch (std::runtime_error& e)
  {
    ros::shutdown();
    return EXIT_FAILURE;
  }
}
