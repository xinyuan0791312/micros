-----------------------------------------------
###  1. openni as the driver 
-----------------------------------------------
sudo apt-get install ros-kinetic-openni*
roslaunch openni_launch openni.launch



sudo apt-get install libfreenect-dev
sudo apt-get install ros-kinetic-libfreenect


---------------------------------------------------
###  openni as the driver, independent package 
---------------------------------------------------
$ cd ~/catkin_ws/src
$ git clone https://github.com/ros-drivers/openni_camera.git
$ cd ~/catkin_ws
$ catkin_make

rosrun openni_camera openni_node


---------------------------------------------------
###  freenect as the driver, independent package 
---------------------------------------------------
$ cd ~/catkin_ws/src
$ git clone https://github.com/ros-drivers/freenect_stack.git
$ cd ~/catkin_ws
$ catkin_make

roslaunch freenect_launch freenect.launch
# or
rosrun freenect_camera freenect_node



------------------------------------------------------------------------------------
###  additional features present in the Kinect sensor: accelerometer, tilt, and LED
------------------------------------------------------------------------------------
sudo apt-get install ros-kinetic-kinect-aux

$ cd ~/catkin_ws/src
$ git clone https://github.com/muhrix/kinect_aux.git -b indigo
$ cd ~/catkin_ws
$ catkin_make

rosrun kinect_aux kinect_aux_node
