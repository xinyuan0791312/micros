#include "openni_plugin.h"
#include <pluginlib/class_list_macros.h>
#include "physics_ros_msgs/KinectParamConfig.h"

namespace micros
{
namespace physics
{
namespace kinect
{

OpenniPlugins::OpenniPlugins() : _manager(true)
{
}

bool OpenniPlugins::init()
{
  return true;
}

bool OpenniPlugins::pause()
{
  return true;
}

bool OpenniPlugins::stop()
{
  ROS_INFO("Test openni_plugin, count : %d", _manager.listLoadedNodelets().size());
  _manager.unload(ros::this_node::getName());
  if(_pub)
    _pub.shutdown();
  return true;
}

bool OpenniPlugins::start()
{
  // for (int i=0;i<100;i++)
  // {
  //   ROS_INFO("Test openni_plugin, count : %d", i);
  // }

  try
  {
    nodelet::M_string remappings;
    nodelet::V_string my_argv;
    // Driver nodelet
    _manager.load(ros::this_node::getName(), "openni_camera/driver", remappings, my_argv);

    // remappings._data_skip = 10;
    // remappings.setResolution();

    // while (ros::ok())
    // {
    // try
    // {
    //   // boost::this_thread::sleep(boost::posix_time::seconds(0.01));
    // }
    // catch (boost::thread_interrupted&)
    // {
    //   ROS_INFO("Kinect camera thread interrupted");
    //   break;
    // }
    // ros::spinOnce();
    // }
  }
  catch (std::runtime_error &e)
  {
    ros::shutdown();
    return EXIT_FAILURE;
  }
  return true;
}

void OpenniPlugins::setParams(std::vector<std::string> &keys, std::vector<std::string> &values)
{
  int tag;
  physics_ros_msgs::KinectParamConfig kinectMsg;
  for (std::vector<std::string>::iterator it = keys.begin(); it != keys.end(); it++)
  {
    if (*it == "imageWidth")
    {
      tag = it - keys.begin();
      unsigned int w = atoi(values[tag].c_str());
      if(w > 0) {
        ROS_INFO("come to set kinect camera parameters -> imageWidth %d", w);
        kinectMsg.imageWidth = w;
      }
    }
    else if (*it == "imageHeight")
    {
      tag = it - keys.begin();
      unsigned int h = atoi(values[tag].c_str());
      if (h > 0)
      {
        kinectMsg.imageHeight = h;
        ROS_INFO("Come to set kinect camera parameters -> imageHeight %d", h);
      }
    }
    else if (*it == "depthWidth")
    {
      tag = it - keys.begin();
      unsigned int w = atoi(values[tag].c_str());
      if (w > 0)
      {
        kinectMsg.depthWidth = w;
        ROS_INFO("Come to set kinect camera parameters -> depthWidth %d", w);
      }
    }
    else if (*it == "depthHeight")
    {
      tag = it - keys.begin();
      unsigned int w = atoi(values[tag].c_str());
      if (w > 0)
      {
        kinectMsg.depthHeight = w;
        ROS_INFO("Come to set kinect camera parameters -> depthHeight %d", w);
      }
    }
    else if (*it == "dataSkip")
    {
      tag = it - keys.begin();
      unsigned int w = atoi(values[tag].c_str());
      if (w > 0)
      {
        kinectMsg.dataSkip = w;
        ROS_INFO("Come to set kinect camera parameters -> dataSkip %d", w);
      }
    }
  }

  if(kinectMsg.imageHeight > 0 || kinectMsg.depthWidth > 0 ||  kinectMsg.depthHeight > 0 || kinectMsg.dataSkip > 0) {
    if(!_pub)
      _pub = _nh.advertise<physics_ros_msgs::KinectParamConfig>("/config_kinect_param", 10000);
    _pub.publish(kinectMsg);
  }
}

PLUGINLIB_EXPORT_CLASS(micros::physics::kinect::OpenniPlugins, micros::physics::device_core::DeviceBase)

} // namespace kinect
} // namespace physics
} // namespace micros
