#ifndef PLUGINLIB__OPENNI_PLUGINS_H_
#define PLUGINLIB__OPENNI_PLUGINS_H_

#include <stdio.h>
#include <device_core/device_base.h>
#include <cmath>
#include <boost/thread.hpp> 
#include <nodelet/loader.h>
#include <ros/ros.h>

namespace micros
{
namespace physics
{
namespace kinect
{

  class OpenniPlugins : public micros::physics::device_core::DeviceBase
  {
    public:
      OpenniPlugins();

      bool init();
      bool pause();
      bool start();
      bool stop();

      void setParams(std::vector<std::string> &keys, std::vector<std::string> &values);

      nodelet::Loader _manager; // Bring up manager ROS API
      ros::Publisher _pub;
      ros::NodeHandle _nh;
  };

}
}
}

#endif