#include <stdio.h>
#include <ros/ros.h>
#include <nodelet/loader.h>

int main(int argc, char** argv)
{
  ros::init(argc, argv, "openni_driver");

  try
  {
    nodelet::Loader manager(true); // Bring up manager ROS API
    nodelet::M_string remappings;
    nodelet::V_string my_argv;

    // Driver nodelet
    manager.load(ros::this_node::getName(), "openni_camera/driver", remappings, my_argv);

    // Manager service calls are on global callback queue
    while (ros::ok())
    {
      ros::spinOnce();
    }
  }
  catch (std::runtime_error& e)
  {
    ros::shutdown();
    return EXIT_FAILURE;
  }
}
