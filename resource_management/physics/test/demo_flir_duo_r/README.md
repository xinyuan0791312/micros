# demo_flir_duo_r

Test flir driver.

## Configs

1. Add local network connection.
- IP: 192.168.10.19
- Netmask: 24
- Gateway: 192.168.10.255

## Steps

1. Execute test script.
```
chmod +x ./flir_plugin_test.sh
./flir_plugin_test.sh
```
2. View sensor data output.
```
rosrun rqt_gui rqt_gui
```
Add the topic: **image_raw**.
Message type: **sensor_msgs/Image.msg**.