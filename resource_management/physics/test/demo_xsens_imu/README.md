# demo_xsens_imu

Test xsens_imu driver.

## Configs

None.

## Steps

1. Execute test script.
```
chmod +x ./imu_plugin_test.sh
./imu_plugin_test.sh
```
2. View sensor data output in rviz.
topic: **/imu/data**.
Message type: **sensor_msgs/Imu.msg**.