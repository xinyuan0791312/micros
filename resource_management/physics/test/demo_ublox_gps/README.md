# demo_ublox_gps

Test ublox driver.

## Configs

None.

## Steps

1. Execute test script.
```
chmod +x ./gps_plugin_test.sh
./gps_plugin_test.sh
```
2. View sensor data output.
```
rosrun rqt_gui rqt_gui
```
Add the topic: **/fix**.
Message type: **sensor_msgs/NavSatFix.msg**.