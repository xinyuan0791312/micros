/* ==================================================================
*
* Copyright (c) 2016-2019, micROS Group, NIIDT, TAIIC, HPCL.
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions 
* are met: 
*
* 1. Redistributions of source code must retain the above copyright 
*     notice, this list of conditions and the following disclaimer. 
* 2. Redistributions in binary form must reproduce the above copyright 
*     notice, this list of conditions and the following disclaimer in the 
*     documentation and/or other materials provided with the 
*     distribution. 
* 3. All advertising materials mentioning features or use of this software 
*     must display the following acknowledgement: 
*     This product includes software developed by the micROS Group. and 
*     its contributors. 
* 4. Neither the name of the Group nor the names of its contributors may 
*     be used to endorse or promote products derived from this software 
*     without specific prior written permission. 
*
* THIS SOFTWARE IS PROVIDED BY MICROS,GROUP AND CONTRIBUTORS
* ''AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT
* NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
* FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL 
* THE MICROS,GROUP OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS 
* OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) 
* HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
* OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN 
* IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
* ===================================================================
*
* Author: Bin Di, Chunlian Fu, Junling Gao, Ruihao Li, Wei Yi.
*         (in alphabetical order)
*
*/
#include <ros/ros.h>
#include "physics_ros_msgs/DeviceParamConfig.h"
#include "physics_ros_msgs/KinectParamConfig.h"
#include "physics_ros_msgs/ZedParamConfig.h"
#include "std_msgs/String.h"
#include <vector>

// #define VELODYNE
// #define IMU
#define KINECT
// #define ZED

// #define SOFTBUS_PARAM

// #define START_STOP

// #define DEVICE_PARAM


int main(int argc, char **argv)
{
    ros::init(argc, argv, "softbus_topic_test");

    ros::NodeHandle nh;

    ros::Publisher actionPub = nh.advertise<std_msgs::String>("/physics_softbus/driver_trigger_chatter", 10000);
#ifdef SOFTBUS_PARAM
    ros::Publisher paramPub = nh.advertise<physics_ros_msgs::DeviceParamConfig>("/physics_softbus/config_device_param", 10000);
#endif
#ifdef KINECT
    ros::Publisher kenictPub = nh.advertise<physics_ros_msgs::KinectParamConfig>("/config_kinect_param", 10000);
#endif
#ifdef ZED
    ros::Publisher zedPub = nh.advertise<physics_ros_msgs::ZedParamConfig>("/config_zed_param", 10000);
#endif

    int i = 0;
    std_msgs::String msg;
#ifdef SOFTBUS_PARAM
    physics_ros_msgs::DeviceParamConfig paramMsg;
#endif
#ifdef KINECT
    physics_ros_msgs::KinectParamConfig kinectMsg;
#endif
#ifdef ZED
    physics_ros_msgs::ZedParamConfig zedMsg;
#endif

    while (ros::ok())
    {
        // ros::spinOnce();
        if (i < 10) {
        #ifdef VELODYNE
            msg.data = "Open_Velodyne";
        #endif
        #ifdef IMU
            msg.data = "Open_Xsens_IMU";
        #endif
        #ifdef KINECT
            msg.data = "Open_Kinect_Camera";
        #endif
        #ifdef ZED
            msg.data = "Open_ZED_Camera";
        #endif
            actionPub.publish(msg);
            ROS_INFO("[softbus_topic_test] pub start device msg");
        } else if (i > 20 && i < 30) {
        #ifdef VELODYNE
            msg.data = "Close_Velodyne";
        #endif
        #ifdef IMU
            msg.data = "Close_Xsens_IMU";
        #endif
        #ifdef KINECT
            msg.data = "Close_Kinect_Camera";
        #endif
        #ifdef ZED
            msg.data = "Close_ZED_Camera";
        #endif
        #ifdef START_STOP
            actionPub.publish(msg);
            ROS_INFO("[softbus_topic_test] pub close device msg");
        #endif
        } else if (i < 40) {
        #ifdef VELODYNE
            msg.data = "Open_Velodyne";
        #endif
        #ifdef IMU
            msg.data = "Open_Xsens_IMU";
        #endif
        #ifdef KINECT
            msg.data = "Open_Kinect_Camera";
        #endif
        #ifdef ZED
            msg.data = "Open_ZED_Camera";
        #endif
        #ifdef START_STOP
            actionPub.publish(msg);
            ROS_INFO("[softbus_topic_test] pub start device msg");
        #endif
        } else if (i < 50) {
        #ifdef VELODYNE
            msg.data = "Close_Velodyne";
        #endif
        #ifdef IMU
            msg.data = "Close_Xsens_IMU";
        #endif
        #ifdef KINECT
            msg.data = "Close_Kinect_Camera";
        #endif
        #ifdef ZED
            msg.data = "Close_ZED_Camera";
        #endif
        #ifdef START_STOP
            actionPub.publish(msg);
            ROS_INFO("[softbus_topic_test] pub close device msg");
        #endif
        }
    
    #ifdef ZED
    #ifdef DEVICE_PARAM
        if(i == 20) {
            ROS_INFO("[softbus_topic_test] pub config device param msg to zed");
            zedMsg.width = 1144;
            zedMsg.height = 320;
            zedMsg.framerate = 10;
            zedPub.publish(zedMsg);
        }
        else if(i == 40) {
            ROS_INFO("[softbus_topic_test] pub config device param msg to zed");
            zedMsg.width = 858;
            zedMsg.height = 240;
            zedMsg.framerate = 5;
            zedPub.publish(zedMsg);
        }
    #endif
    #endif

    #ifdef SOFTBUS_PARAM
        if(i == 20) {
            paramMsg.paramKeys.clear();
            paramMsg.paramValues.clear();
            ROS_INFO("[softbus_topic_test] pub config device param msg to softbus");
            paramMsg.type = 0;  ///< PARAM_CONFIG
            std::vector<std::string> keys;
            std::vector<std::string> values;
        #ifdef ZED
            paramMsg.name = "zed";
            keys.push_back("frame_rate_");
            values.push_back("10");
            keys.push_back("width");
            values.push_back("1144");
            keys.push_back("height");
            values.push_back("320");
        #endif
        #ifdef KINECT
            paramMsg.name = "kinect";
            keys.push_back("imageWidth");
            values.push_back("320");
            keys.push_back("imageHeight");
            values.push_back("240");
            keys.push_back("depthWidth");
            values.push_back("320");
            keys.push_back("depthHeight");
            values.push_back("240");
            keys.push_back("dataSkip");   // 320x240@30Hz
            values.push_back("10");   
        #endif
            paramMsg.paramKeys = keys;
            paramMsg.paramValues = values;
            paramPub.publish(paramMsg);
        }
        else if(i == 40) {
            paramMsg.paramKeys.clear();
            paramMsg.paramValues.clear();
            ROS_INFO("[softbus_topic_test] pub config device param msg to softbus");
            paramMsg.type = 0;  ///< PARAM_CONFIG
            std::vector<std::string> keys;
            std::vector<std::string> values;
        #ifdef ZED
            paramMsg.name = "zed";
            keys.push_back("frame_rate_");
            values.push_back("5");
            keys.push_back("width");
            values.push_back("858");
            keys.push_back("height");
            values.push_back("240");
        #endif
        #ifdef KINECT
            paramMsg.name = "kinect";
            keys.push_back("imageWidth");
            values.push_back("160");
            keys.push_back("imageHeight");
            values.push_back("120");
            keys.push_back("depthWidth");
            values.push_back("160");
            keys.push_back("depthHeight");
            values.push_back("120");
            keys.push_back("dataSkip");   //640x480@30Hz
            values.push_back("20");   // 30/20
        #endif
            paramMsg.paramKeys = keys;
            paramMsg.paramValues = values;
            paramPub.publish(paramMsg);
        }
    #endif

    #ifdef KINECT
    #ifdef DEVICE_PARAM
        if(i == 20) {
            ROS_INFO("[softbus_topic_test] pub config device param msg to kinect");
            kinectMsg.imageWidth = 320;
            kinectMsg.imageHeight = 240;
            kinectMsg.depthWidth = 320;
            kinectMsg.depthHeight = 240;  //320x240@30Hz
            kinectMsg.dataSkip = 10;  // 30/10
            kenictPub.publish(kinectMsg);
        }
        else if(i == 40) {
            ROS_INFO("[softbus_topic_test] pub config device param msg to kinect");
            kinectMsg.imageWidth = 160;
            kinectMsg.imageHeight = 120;
            kinectMsg.depthWidth = 160;
            kinectMsg.depthHeight = 120;  //640x480@30Hz
            kinectMsg.dataSkip = 20;  // 30/20 Hz
            kenictPub.publish(kinectMsg);
        }
    #endif
    #endif
    
        ROS_INFO("[softbus_topic_test] running for %d time", i);
        sleep(3);
        i++;
    }

    return 0;
}