/* ==================================================================
*
* Copyright (c) 2016-2019, micROS Group, NIIDT, TAIIC, HPCL.
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions 
* are met: 
*
* 1. Redistributions of source code must retain the above copyright 
*     notice, this list of conditions and the following disclaimer. 
* 2. Redistributions in binary form must reproduce the above copyright 
*     notice, this list of conditions and the following disclaimer in the 
*     documentation and/or other materials provided with the 
*     distribution. 
* 3. All advertising materials mentioning features or use of this software 
*     must display the following acknowledgement: 
*     This product includes software developed by the micROS Group. and 
*     its contributors. 
* 4. Neither the name of the Group nor the names of its contributors may 
*     be used to endorse or promote products derived from this software 
*     without specific prior written permission. 
*
* THIS SOFTWARE IS PROVIDED BY MICROS,GROUP AND CONTRIBUTORS
* ''AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT
* NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
* FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL 
* THE MICROS,GROUP OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS 
* OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) 
* HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
* OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN 
* IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
* ===================================================================
*
* Author: Bin Di, Chunlian Fu, Junling Gao, Ruihao Li, Wei Yi.
*         (in alphabetical order)
*
*/
#include <ros/ros.h>
#include "physics_ros_msgs/DeviceParamConfig.h"
#include "std_msgs/String.h"
#include <vector>

int main(int argc, char **argv)
{
    ros::init(argc, argv, "softbus_topic_test");

    ros::NodeHandle nh;

    ros::Publisher actionPub = nh.advertise<std_msgs::String>("/physics_softbus/driver_trigger_chatter", 10000);
    ros::Publisher paramPub = nh.advertise<physics_ros_msgs::DeviceParamConfig>("/physics_softbus/config_device_param", 10000);

    int i = 0;
    std_msgs::String msg;
    physics_ros_msgs::DeviceParamConfig paramMsg;

    while (ros::ok())
    {
        // ros::spinOnce();
        if (i < 10) {
            msg.data = "Open_ZED_Camera";
            actionPub.publish(msg);
            ROS_INFO("[softbus_topic_test] pub start device msg");
        }

        if(i == 15) {
            paramMsg.paramKeys.clear();
            paramMsg.paramValues.clear();
            ROS_INFO("[softbus_topic_test] pub config device param msg to softbus");
            paramMsg.type = 0;  ///< PARAM_CONFIG
            std::vector<std::string> keys;
            std::vector<std::string> values;

            paramMsg.name = "zed";
            keys.push_back("frame_rate_");
            values.push_back("10");
            keys.push_back("width");
            values.push_back("1144");
            keys.push_back("height");
            values.push_back("320");

            paramMsg.paramKeys = keys;
            paramMsg.paramValues = values;
            paramPub.publish(paramMsg);
        }
        else if(i == 25) {
            paramMsg.paramKeys.clear();
            paramMsg.paramValues.clear();
            ROS_INFO("[softbus_topic_test] pub config device param msg to softbus");
            paramMsg.type = 0;  ///< PARAM_CONFIG
            std::vector<std::string> keys;
            std::vector<std::string> values;

            paramMsg.name = "zed";
            keys.push_back("frame_rate_");
            values.push_back("5");
            keys.push_back("width");
            values.push_back("858");
            keys.push_back("height");
            values.push_back("240");
        
            paramMsg.paramKeys = keys;
            paramMsg.paramValues = values;
            paramPub.publish(paramMsg);
        }

        ROS_INFO("[softbus_topic_test] running for %d time", i);
        sleep(3);
        i++;
    }

    return 0;
}