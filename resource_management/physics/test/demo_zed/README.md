﻿# demo_zed

Test zed driver.

## Configs

None.

## Steps

# Test zed plugin

1. Execute test script.
```
chmod +x ./zed_plugin_test.sh
./zed_plugin_test.sh
```
2. View sensor data output.
```
rosrun rqt_gui rqt_gui
```
Add the topic: **right/image_raw**、**left/image_raw**.
Message type: **sensor_msgs/Image.msg**.

---

# Test zed dynamic found

1. Modify /etc/udev/rules.d/99-usb-ZED-Kinect-Xsens.rules

Change "/home/spring/rgzn/micros_alpha/micros_actor_alpha/devel/lib/udev_trigger/udev_trigger" to your project directory.

```
sudo udevadm control --reload-rules && sudo udevadm trigger
sudo udevadm control --reload-rules && sudo service udev restart && sudo udevadm trigger
```
2. Execute test script.
```
chmod +x ./zed_dynamic_found_test.sh
./zed_dynamic_found_test.sh
```
3. View sensor data, follow the previous steps.

---

# Test zed dynamic bind

1. Execute test script.
```
chmod +x ./zed_dynamic_bind_test.sh
./zed_dynamic_bind_test.sh
```
2. View sensor data, follow the previous steps.

---

# Test zed softbus config start stop

1. Execute test script.
```
chmod +x ./zed_softbus_config_start_stop_test.sh
./zed_softbus_config_start_stop_test.sh
```
2. View sensor data, follow the previous steps.

---

# Test zed softbus config param

1. Execute test script.
```
chmod +x ./zed_softbus_config_param_test.sh
./zed_softbus_config_param_test.sh
```
2. View sensor data, follow the previous steps.

---

# Test zed msg config param

1. Execute test script.
```
chmod +x ./zed_msg_config_param_test.sh
./zed_msg_config_param_test.sh
```
2. View sensor data, follow the previous steps.

---




