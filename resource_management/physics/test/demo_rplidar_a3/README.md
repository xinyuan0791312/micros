# demo_rplidar_a3

Test rplidar_a3 driver.

## Configs

None.

## Steps

1. Execute test script.
```
chmod +x ./rplidar_plugin_test.sh
./rplidar_plugin_test.sh
```
2. View sensor data output.
```
rosrun rqt_gui rqt_gui
```
Add the topic: **/scan**.
Message type: **sensor_msgs/LaserScan.msg**.
