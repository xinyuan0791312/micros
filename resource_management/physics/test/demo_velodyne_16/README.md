# demo_velodyne_16

Test velodyne driver.

## Configs

1. Add local network connection.
- IP: 192.168.1.200
- Netmask: 24
- Gateway: 192.168.1.255

## Steps

1. Execute test script.
```
chmod +x ./velodyne_plugin_test.sh
./velodyne_plugin_test.sh
```
2. View sensor data output.
```
rosrun rviz rviz -f velodyne
```
Add by topic: **PointCloud2**.
Message type: **sensor_msgs/PointCloud2.msg**.