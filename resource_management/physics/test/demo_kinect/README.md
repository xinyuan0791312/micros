# demo_kinect

Test kinect driver.

## Configs

None.

## Steps

# Test kinect plugin

1. Execute test script.
```
chmod +x ./kinect_plugin_test.sh
./kinect_plugin_test.sh
```
2. View sensor data output.
```
rosrun rqt_gui rqt_gui
```
Add the topic: **depth/image_raw**.
Message type: **sensor_msgs/Image.msg**.

---

# Test kinect dynamic found

1. Modify /etc/udev/rules.d/99-usb-kinect-Kinect-Xsens.rules

Change "/home/spring/rgzn/micros_alpha/micros_actor_alpha/devel/lib/udev_trigger/udev_trigger" to your project directory.

```
sudo udevadm control --reload-rules && sudo udevadm trigger
sudo udevadm control --reload-rules && sudo service udev restart && sudo udevadm trigger
```
2. Execute test script.
```
chmod +x ./kinect_dynamic_found_test.sh
./kinect_dynamic_found_test.sh
```
3. View sensor data, follow the previous steps.

---

# Test kinect dynamic bind

1. Execute test script.
```
chmod +x ./kinect_dynamic_bind_test.sh
./kinect_dynamic_bind_test.sh
```
2. View sensor data, follow the previous steps.

---

# Test kinect softbus config start stop

1. Execute test script.
```
chmod +x ./kinect_softbus_config_start_stop_test.sh
./kinect_softbus_config_start_stop_test.sh
```
2. View sensor data, follow the previous steps.

---

# Test kinect softbus config param

1. Execute test script.
```
chmod +x ./kinect_softbus_config_param_test.sh
./kinect_softbus_config_param_test.sh
```
2. View sensor data, follow the previous steps.

---

# Test kinect msg config param

1. Execute test script.
```
chmod +x ./kinect_msg_config_param_test.sh
./kinect_msg_config_param_test.sh
```
2. View sensor data, follow the previous steps.

---