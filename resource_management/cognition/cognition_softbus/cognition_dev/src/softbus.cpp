#include "cognition/framwork/softbus.h"

namespace micros_cognition {

/**
 * @brief SoftBus::SoftBus
 * @param bus_type
 * @note In Future, will add function calling of model_select and so on;
 */
SoftBus::SoftBus(BusType bus_type )
    :bus_type_(bus_type)
{
    if (BusType::Direct == bus_type_) {
        ROS_INFO("Initialized Cognition SoftBus With Model-Call Direct Mode");
    }
    else {
      bus_loader_ = new pluginlib::ClassLoader<cognition_bus::SoftBusBase>("cognition_bus","cognition_bus::SoftBusBase");

      //bus_impl_name will be defined from Task Param which created by general_plugin instence
      string bus_impl_name;
      switch (bus_type)
      {
          case BusType::Auto:
              ROS_INFO("Auto Initialized Cognition SoftBus With getModelFromRecommend()");
              break;
          case BusType::Detection_TF:
              bus_impl_name = "cognition_bus::DetectionTFImpl";
          case BusType::Detection_DNN:
              bus_impl_name = "cognition_bus::DetectionDNNImpl";
              break;
          case BusType::Detection_TensorRT:
              bus_impl_name = "cognition_bus::DetectionTensorRTImpl";
              break;
          case BusType::Detection_TF_RPC:
              bus_impl_name = "cognition_bus::DetectionTFImplRPC";
              break;
          default:
              break;
      }

      bus_impl_ptr_ = bus_loader_->createInstance(bus_impl_name);

      ROS_INFO("Initialized Cognition SoftBus With Resource %s", bus_impl_name.c_str());
    }
}

SoftBus::~SoftBus(){
    if (BusType::Direct == bus_type_ ) {
        ROS_INFO("Closed Cognition SoftBus Direct Mode");
    }
    else {
        bus_impl_ptr_.reset();
        delete bus_loader_;
        ROS_INFO("Closed Cognition SoftBus With Close <pluginlib>");
    }
}

//BusType::Auto
bool SoftBus::call(vector<boost::any>& inputs, vector<boost::any>& results)
{
    return bus_impl_ptr_->call(inputs, results);
}

bool SoftBus::async_call(vector<boost::any>& inputs, vector<boost::any>& results, int duration)
{
    return bus_impl_ptr_->async_call(inputs, results, duration);
}

ModelMeta SoftBus::getModelFromRecommend()
{
    //TODO
//    //get all model of coginition resource
//    std::map<std::string, model_vector> model_list = load_model_list();

//    //update Task Param, and get recommended models, return a list of sturct ModelOrigin
//    task_description task;
//    task.task_type = "detection";
//    task.image_type = "colored";
//    task.image_width = 800;
//    task.image_height = 600;
//    task.cpu_mem_limit_gb = 3.5;
//    task.gpu_mem_limit_gb = 3.5;
//    task.time_limit_ms = 1000;
//    task.is_dark = 0;
//    std::vector<std::string> result_list = get_recommended_models(vectorize_task(task), model_list);

}


//BusType::Direct
//get model path according model name(<framework_name>, <model_name>, <dataset_name>)
ModelMeta SoftBus::getModelFromName(const FrameworkType& framework_name, const string& model_name, const string &dataset_name)
{
    string package_name = "ml_database";
    string package_dir = ros::package::getPath("ml_database");
    string pretrained_model_dir = package_dir + "/pretrained_model";

    int idx = static_cast<int>(framework_name);
    string frame_name = FrameworkList_[idx];
    string model_root_path = pretrained_model_dir +"/"+ frame_name +"/"+ model_name +"/";

    //return
    ModelMeta modelmeta;
    modelmeta.model_root_path = model_root_path;
    return modelmeta;
}

//get model path according model_id(<framework_name>_<model_name>_<dataset_name>)
ModelMeta SoftBus::getModelFromId(const string& model_id)
{
    //splitWithPattern into resVec
    std::vector<std::string> resVec;
    std::string pattern = "_";
    std::string strs = model_id + pattern; //方便截取最后一段数据

    size_t pos = strs.find(pattern);
    size_t size = strs.size();

    while (pos != std::string::npos)
    {
        std::string x = strs.substr(0,pos);
        resVec.push_back(x);
        strs = strs.substr(pos+1,size);
        pos = strs.find(pattern);
    }

    //get getModelMeta
    string frame_name = resVec[0];
    string model_name = resVec[1];
    string dataset_name = resVec[2];

    string package_name = "ml_database";
    string package_dir = ros::package::getPath(package_name);
    string pretrained_model_dir = package_dir + "/pretrained_model";
    string model_root_path = pretrained_model_dir +"/"+ frame_name +"/"+ model_name +"/";

    //return
    ModelMeta modelmeta;
    modelmeta.model_root_path = model_root_path;
    return modelmeta;
}



}//namespace
