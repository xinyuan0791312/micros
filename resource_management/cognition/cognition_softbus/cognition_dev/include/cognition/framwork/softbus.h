#ifndef CognitionSoftBus_H
#define CognitionSoftBus_H

// C and C++ headers
#include <string>
#include <vector>
#include <boost/any.hpp>
#include <boost/shared_ptr.hpp>

// ROS headers
#include <ros/ros.h>
#include <pluginlib/class_loader.h>

// Interface headers
#include <cognition_bus/softbus_base.h>
#include <model_recommend/recommending.h>

/**
 * @brief namespace micros_cognition
 */
namespace micros_cognition {
using namespace std;

enum class BusType{
    Auto,
    Direct,
    Detection_TF,
    Detection_DNN,
    Detection_TensorRT,
    Detection_TF_RPC,
};

enum class FrameworkType{
    Caffe,
    DNN,
    TensorRT,
    TensorFlow,
//    NCNN,
//    Pytorch,
};

struct ModelMeta{
    string model_id;       // = "TensorRT_MobileNetSSD_Car"
    string maintainer;     // = "observe"
    string task_type;      // = "Detection"
    string famework_type;  // = "Caffe"
    string model_root_path;// model_root_path
};

/**
 * @brief The interface class of cognition SoftBus
 */
class SoftBus{
public:
    SoftBus(BusType bus_type = BusType::Auto);
    ~SoftBus();

    //BusType::Direct
    ModelMeta getModelFromName(const FrameworkType& framework_name, const string& model_name, const string& dataset_name);
    ModelMeta getModelFromId(const string& model_id);

    //BusType::Auto
    bool call(vector<boost::any>& inputs, vector<boost::any>& results);
    bool async_call(vector<boost::any>& inputs, vector<boost::any>& results, int duration = 10);
    ModelMeta getModelFromRecommend();


private:
    BusType bus_type_;

    pluginlib::ClassLoader<cognition_bus::SoftBusBase>* bus_loader_; // Pointer to Bus loader
    boost::shared_ptr<cognition_bus::SoftBusBase> bus_impl_ptr_;    // Pointer to loaded class

    vector<string> FrameworkList_ = {
        "Caffe",
        "DNN",
        "TensorRT",
        "TensorFlow",
    };
};


}//namespace


#endif // CognitionSoftBus_H
