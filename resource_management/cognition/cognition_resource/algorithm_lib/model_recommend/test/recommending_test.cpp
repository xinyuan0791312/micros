#include <unistd.h>
#include <iostream>
#include <queue>
#include <tinyxml2.h>
#include <model_recommend/recommending.h>

using namespace tinyxml2;

int main()
{
    sleep(3);
    // api-1
    std::map<std::string, model_vector> model_list = load_model_list();
    // task data
    std::string package_path = ros::package::getPath("model_recommend");
    XMLDocument *task_xml = new XMLDocument();
    XMLError error = task_xml->LoadFile((package_path + "/test/task_descriptions.xml").c_str());
    if (error != XML_SUCCESS)
    {
        std::cerr << "Loading file task_descriptions.xml failed." << std::endl;
        delete task_xml;
        return -1;
    }
    XMLElement *meta = task_xml->RootElement();
    XMLElement *task_node = meta->FirstChildElement("task");
    while(task_node != NULL)
    {
        std::string task_id = task_node->Attribute("task_id");
        task_description task;
        task.task_type = task_node->Attribute("task_type");
        task.image_type = task_node->Attribute("image_type");
        task.image_width = atoi(task_node->Attribute("image_width"));
        task.image_height = atoi(task_node->Attribute("image_height"));
        task.cpu_mem_limit_gb = atof(task_node->Attribute("cpu_mem_limit_gb"));
        task.gpu_mem_limit_gb = atof(task_node->Attribute("gpu_mem_limit_gb"));
        task.time_limit_ms = atof(task_node->Attribute("time_limit_ms"));
        task.is_dark = std::strcmp(task_node->Attribute("is_dark"), "true") == 0;
        // api-2-3
        std::vector<std::string> result_list = get_recommended_models(vectorize_task(task), model_list);
        int result_length = result_list.size();
        std::cout << task_id << ":";
        for(int i = 0; i < result_length; i++)
        {
            std::cout << " " << result_list[i];
        }
        std::cout << std::endl;
        task_node = task_node->NextSiblingElement();
    }
    delete task_xml;

    return 0;

    //model list init  <- class construct

    //task list init   <- class function_1

    //call recommended function    <- class function_2
    //return/print result
}



