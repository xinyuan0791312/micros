#include "ssd_detector/ssd_detector.h"
#include <cv_bridge/cv_bridge.h>
#include "ros_thread.h"

void grabImageCallback(const sensor_msgs::Image::ConstPtr &img_msg){
    std::cout << "~~~~~~~~~~~~~~~~~"<< std::endl;
    // Copy ROS image to an OpenCV matrix
    cv::Mat current_frame;
    sensor_msgs::Image msg = *img_msg;

    cv_bridge::CvImageConstPtr cv_ptr;
    try
    {
        cv_ptr = cv_bridge::toCvCopy(msg,"bgr8");
    }
    catch (cv_bridge::Exception& expt)
    {
        ROS_ERROR("cv_bridge exception: %s", expt.what());
        return;
    }
    cv_ptr->image.copyTo(current_frame); //保存为cv::mat

    Rect rt;
    Mat frame_draw;
    int contours_size;
    cognition::getRect(current_frame, rt, frame_draw, contours_size); //计算bbox，并绘图
    cv::imshow("SSD_Detector", frame_draw);
    cv::waitKey(1);
}

int main(int argc, char *argv[])
{
    sleep(3);
    ros::init(argc,argv,"orient_test_node");
    //general_bus::SSDDetectorPlugin w;
    //w.start();
    ros::NodeHandle nh_;
    ros::Subscriber image_sub_ = nh_.subscribe("/camera",100,&grabImageCallback);

    //RosThread ros_thread;
    ros::spin();
    return 0; //
}
