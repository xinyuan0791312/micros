#ifndef ROS_THREAD_H
#define ROS_THREAD_H
#include <ros/ros.h>
#include <boost/thread.hpp>

class RosThread
{
public:
    RosThread();
    ~RosThread();
    void rosrunThread();

private:
    bool m_isExt;
    boost::thread *m_ros_thread;
};
#endif // ROS_THREAD_H
