#ifndef __SSD_DETECTORS_H__
#define __SSD_DETECTORS_H__

#include <opencv2/opencv.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/core/utility.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/tracking.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/highgui.hpp>

#include <opencv2/dnn.hpp>
#include <opencv2/dnn/shape_utils.hpp>

#include <ros/ros.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/NavSatFix.h>
//#include <sensor_msgs/CameraInfo.h>

#include <vector>
#include <iostream>
#include <stdio.h>
#include <cstring>
#include <stdint.h>
#include <fstream>
#include <string>
#include <time.h>
#include <thread>
#include <algorithm>
#include <ros/callback_queue.h>
#include <nav_msgs/Odometry.h>
#include <Eigen/Dense>
#include <image_transport/image_transport.h>

using std::vector;
using std::string;
using cv::Mat;
using cv::MatND;
using cv::Rect;
using cv::Rect2d;
using cv::Point;
using cv::Scalar;

namespace cognition
{
  void getRect(cv::Mat& current_frame, Rect& rt, cv::Mat& frame_draw, int &contours_size);
  bool biggerSort(vector<Point> v1, vector<Point> v2);
}

#endif
