#include "ssd_detector/ssd_detector.h"

namespace cognition
{
    void getRect(cv::Mat& current_frame, Rect& rt, cv::Mat& frame_draw, int& contours_size)
    {
        bool ok_ssd;
        // Running with hsv
        Mat hsv;                          //HSV颜色空间，在色调H上跟踪目标（camshift是基于颜色直方图的算法）
        vector<Mat> hsvSplit;
        Mat imgThresholded;
        //bbox
        vector<vector<Point>>contours;

        //current_frame.copyTo(frame_draw1); // only copy can do the real copy, just equal not.
        cv::resize(current_frame,frame_draw,cv::Size(640,480));
        cv::cvtColor(frame_draw, hsv, cv::COLOR_BGR2HSV); //Convert the captured frame from BGR to HSV
        //因为我们读取的是彩色图，直方图均衡化需要在HSV空间做
        cv::split(hsv, hsvSplit);
        cv::equalizeHist(hsvSplit[2],hsvSplit[2]);
        cv::merge(hsvSplit,hsv);
        cv::inRange(hsv, Scalar(0,40,0), Scalar(15, 255, 90), imgThresholded); //Threshold the image
        //开操作 (去除一些噪点)
        Mat element = cv::getStructuringElement(cv::MORPH_RECT, cv::Size(5,5));
        cv::morphologyEx(imgThresholded, imgThresholded, cv::MORPH_OPEN, element);
        //闭操作 (连接一些连通域)
        cv::morphologyEx(imgThresholded, imgThresholded, cv::MORPH_CLOSE, element);
        cv::findContours( imgThresholded, contours, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_NONE);
        Mat result1(imgThresholded.size(), CV_8U, Scalar(255));

        contours_size = contours.size();
        if(contours_size >= 1)
        {
            //对连通分量进行排序
            std::sort(contours.begin(),contours.end(),biggerSort);

            float centx = frame_draw.cols / 2;
            float centy = frame_draw.rows / 2;

            //for(int k=0;k<contours.size();++k)
            for(int k=0;k<1;++k)
            {
                //第k个连通分量的外接矩形框
                if(contourArea(contours[k])<contourArea(contours[0])/5)
                    break;
                rt = boundingRect(contours[k]);

                int avgX = (rt.x + rt.x + rt.width)/2; //运动物体的矩形的中点X位置
                int avgY = (rt.y + rt.y + rt.height)/2;//运动物体的矩形的中点Y位置

                ROS_INFO("Centroid of the image: x = %f, y = %f; object positon: x1 = %d, y1 = %d, x2 = %d; y2 = %d",
                        centx, centy, rt.x, rt.y, rt.x + rt.width, rt.y + rt.height);
                rectangle(frame_draw, rt, Scalar(0,0,255,0), 4);
            }
        }
    }

    bool biggerSort(vector<Point> v1, vector<Point> v2)
    {
        return contourArea(v1)>contourArea(v2);
    }

} //namespace

