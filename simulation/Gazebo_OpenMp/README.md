简介：本项目（Gazebo_exercise）在官方Gazebo7.14版本仿真器的基础上，采用OpenMP、线程池、异步等技术进行了优化。
日期：2019-12-01
作者：zhangshuai，lihao，zenglei,jianghunnan

---------------------------------------- 

本项目包含三个部分，分别是Gazebo源码，第三方依赖包以及测试案例，其中：
	gazebo7_7.14.0_exercise文件夹为优化后的Gazebo源码，里面实现了多线程优化及异步等相关内容；
	需安装第三方依赖包，里面包含sdformat标签功能包；
	Test_hector_cases为测试案例文件夹，里面包含Hector四旋翼无人机进行简单飞行动作的测试案例。
	

使用说明
------------
请按照如下顺序编译安装相关软件包。

一、编译与安装sdformat
	1、cd sdformat
	2、mkdir build
	3、cd build
	4、cmake ../
	5、sudo make -jX（X为编译时启用的线程数，其根据CPU核心数确定，不要超过CPU核心数）
	6、sudo make install

注意：编译安装sdformat包后，需要将sdformat包下sdf文件夹下1.*（1.0、1.2、1.3、1.4、1.5、1.6）标签库拷贝到/usr/share/sdformat目录下。

二、编译与安装libevent-2.1.10-stable
	1、cd libevent-2.1.10-stable
	2、mkdir build
	3、cd build
	4、../configure
	5、sudo make -jX（X为编译时启用的线程数，其根据CPU核心数确定，不要超过CPU核心数）
	6、sudo make install

三、编译与安装Gazebo
	1、cd gazebo
	2、mkdir build
	3、cd build
	4、cmake ../
	5、sudo make -jX（X为编译时启用的线程数，其根据CPU核心数确定，不要超过CPU核心数）
	6、sudo make install
	
注意：利用cmake生成makefile期间，可能会出现依赖库缺失的情况，需要利用sudo apt install或源码编译安装方式进行依赖库的安装

四、编译运行Hector_Quadrotor_Case四旋翼无人机仿真案例
	1、cd Test_hector_cases/Hector_Quadrotor_Case
	2、catkin_make
	3、source devel/setup.bash
	4、roslaunch hector_quadrotor_gazebo  hector_quadrotor_one_node_30.launch

五、使用Gazebo-exercise中的优化
	为了使用这些优化，需要在仿真的主世界文件（一般以.world结尾）中，添加如下所示的优化标签：
	```
	<world>
	...
	<use_asyn_event use_sim_time=1 /> 
   	 <exercise_opt>
     	 	<event_signal parallel_type=1 threads=2 />
	  	<collide_space>1</collide_space>
      		<dxhashspace_collide threads=2 />
      		<updatephysics_dxprocessislands parallel_type=1 threads=2 />
   	 </exercise_opt>
	...
	</world>
	```
	标签释义如下：
	- <use_asyn_event use_sim_time=1 /> 1开启异步优化 0不开启
	- <exercise_opt>标签：优化总开关。
	- <event_signal>标签：用于设置模型预处理模块并行优化参数，parallel_type为1~5表示OpenMp优化（一般设置为1即可，其他是采用不同的任务并发方式），threads为开启的线程数；parallel_type为其他数字时表示不开启优化。
	- <collide_space>标签：用于设置碰撞更新模块中碰撞空间的类型，值为1时表示SAP空间类型，其他数字表示默认的Hash空间类型。
	- <dxhashspace_collide>标签：用于设置碰撞更新模块中dxHashSpace::collide模块并行参数，当<collide_space>标签设置为1时，该参数没有意义；其他情况下，threads大于1，表示采用OpenMP并行优化，threads为开启的线程  数，否则不开启优化。
	- <updatephysics_dxprocessislands>标签：用于设置物理更新模块中dxProcessIslands模块并行参数；parallel_type为1~5表示OpenMp优化（一般设置为1即可，其他是采用不同的任务并发方式），threads为开启的线程数；parallel_type为其他数字时表示不开启优化。
	
