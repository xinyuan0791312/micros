/*
 * Copyright (C) 2012 Open Source Robotics Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
*/
/*=================================================================
* Copyright (c) 2016, micROS Group, TAIIC.
* All rights reserved.
*
* This version adds parallelization by openmp and asynevent etc.
*
* Redistribution and use in source and binary forms, with or without modification, are permitted
* provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice, this list of conditions and
*
the following disclaimer.
* 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions
*
and the following disclaimer in the documentation and/or other materials provided with the
*
distribution.
* 3. All advertising materials mentioning features or use of this software must display the following
*
acknowledgement: This product includes software developed by the micROS Group and its
*
contributors.
* 4. Neither the name of the Group nor the names of contributors may be used to endorse or promote
*
products derived from this software without specific prior written permission.  
*
* THIS SOFTWARE IS PROVIDED BY MICROS GROUP AND CONTRIBUTORS ''AS IS''AND
* ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
* PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE MICROS, GROUP OR
 
 
1* CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
* EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
* PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
* PROFITS; OR BUSINESS INTERRUPTION). HOWEVER CAUSED AND ON ANY THEORY
* OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
* NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
* SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
===================================================================
* Author: Hao Li, Lei Zeng, Shuai Zhang.
*/
#include "gazebo/common/Events.hh"

using namespace gazebo;
using namespace event;

EventT<void (bool)> Events::pause;
EventT<void ()> Events::step;
EventT<void ()> Events::stop;
EventT<void ()> Events::sigInt;

EventT<void (std::string)> Events::worldCreated;
EventT<void (std::string)> Events::entityCreated;
EventT<void (std::string, std::string)> Events::setSelectedEntity;
EventT<void (std::string)> Events::addEntity;
EventT<void (std::string)> Events::deleteEntity;

//////////////////////////Added for asyn event by zenglei////////////////////////////////

EventAsynT<void (const common::UpdateInfo &)> EventAsyns::worldUpdateBeginAsyn;
void EventAsyns::DisconnectWorldUpdateBegin(ConnectionPtr _subscriber)
{
  worldUpdateBeginAsyn.Disconnect(_subscriber);
}

/////////////////////////////////////////////////////////////////////////////////////////

EventT<void (const common::UpdateInfo &)> Events::worldUpdateBegin;
EventT<void (const common::UpdateInfo &)> Events::beforePhysicsUpdate;

EventT<void ()> Events::worldUpdateEnd;
EventT<void ()> Events::worldReset;
EventT<void ()> Events::timeReset;

EventT<void ()> Events::preRender;
EventT<void ()> Events::render;
EventT<void ()> Events::postRender;

EventT<void (std::string)> Events::diagTimerStart;
EventT<void (std::string)> Events::diagTimerStop;

EventT<void (std::string)> Events::removeSensor;

EventT<void (sdf::ElementPtr, const std::string &,
    const std::string &, const uint32_t)> Events::createSensor;

/////////////////////////////////////////////////
void Events::DisconnectWorldUpdateBegin(ConnectionPtr _subscriber)
{
  worldUpdateBegin.Disconnect(_subscriber);
}
