(cl:in-package hector_msgs-msg)
(cl:export '(VA_C-VAL
          VA_C
          H_C-VAL
          H_C
          CHI_C-VAL
          CHI_C
          PHI_FF-VAL
          PHI_FF
          AUX-VAL
          AUX
          AUX_VALID-VAL
          AUX_VALID
          TAKEOFF-VAL
          TAKEOFF
          LANDING-VAL
          LANDING
))