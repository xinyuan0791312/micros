; Auto-generated. Do not edit!


(cl:in-package hector_msgs-msg)


;//! \htmlinclude Formation_Type.msg.html

(cl:defclass <Formation_Type> (roslisp-msg-protocol:ros-message)
  ((header
    :reader header
    :initarg :header
    :type std_msgs-msg:Header
    :initform (cl:make-instance 'std_msgs-msg:Header))
   (formation_type
    :reader formation_type
    :initarg :formation_type
    :type cl:float
    :initform 0.0))
)

(cl:defclass Formation_Type (<Formation_Type>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <Formation_Type>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'Formation_Type)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name hector_msgs-msg:<Formation_Type> is deprecated: use hector_msgs-msg:Formation_Type instead.")))

(cl:ensure-generic-function 'header-val :lambda-list '(m))
(cl:defmethod header-val ((m <Formation_Type>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader hector_msgs-msg:header-val is deprecated.  Use hector_msgs-msg:header instead.")
  (header m))

(cl:ensure-generic-function 'formation_type-val :lambda-list '(m))
(cl:defmethod formation_type-val ((m <Formation_Type>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader hector_msgs-msg:formation_type-val is deprecated.  Use hector_msgs-msg:formation_type instead.")
  (formation_type m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <Formation_Type>) ostream)
  "Serializes a message object of type '<Formation_Type>"
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'header) ostream)
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'formation_type))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <Formation_Type>) istream)
  "Deserializes a message object of type '<Formation_Type>"
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'header) istream)
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'formation_type) (roslisp-utils:decode-single-float-bits bits)))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<Formation_Type>)))
  "Returns string type for a message object of type '<Formation_Type>"
  "hector_msgs/Formation_Type")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'Formation_Type)))
  "Returns string type for a message object of type 'Formation_Type"
  "hector_msgs/Formation_Type")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<Formation_Type>)))
  "Returns md5sum for a message object of type '<Formation_Type>"
  "9911b76ddfe46429bec037d09a0bb075")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'Formation_Type)))
  "Returns md5sum for a message object of type 'Formation_Type"
  "9911b76ddfe46429bec037d09a0bb075")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<Formation_Type>)))
  "Returns full string definition for message of type '<Formation_Type>"
  (cl:format cl:nil "# Vehicle state 'x_hat' output from the estimator or from simulator ~%~%Header header~%~%# Original States~%# @warning roll, pitch and yaw have always to be valid, the quaternion is optional~%float32 formation_type		    # Airspeed (m/s)~%~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')~%# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%# 0: no frame~%# 1: global frame~%string frame_id~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'Formation_Type)))
  "Returns full string definition for message of type 'Formation_Type"
  (cl:format cl:nil "# Vehicle state 'x_hat' output from the estimator or from simulator ~%~%Header header~%~%# Original States~%# @warning roll, pitch and yaw have always to be valid, the quaternion is optional~%float32 formation_type		    # Airspeed (m/s)~%~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')~%# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%# 0: no frame~%# 1: global frame~%string frame_id~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <Formation_Type>))
  (cl:+ 0
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'header))
     4
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <Formation_Type>))
  "Converts a ROS message object to a list"
  (cl:list 'Formation_Type
    (cl:cons ':header (header msg))
    (cl:cons ':formation_type (formation_type msg))
))
