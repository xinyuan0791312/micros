; Auto-generated. Do not edit!


(cl:in-package hector_msgs-msg)


;//! \htmlinclude State29.msg.html

(cl:defclass <State29> (roslisp-msg-protocol:ros-message)
  ((header
    :reader header
    :initarg :header
    :type std_msgs-msg:Header
    :initform (cl:make-instance 'std_msgs-msg:Header))
   (position_gps
    :reader position_gps
    :initarg :position_gps
    :type (cl:vector cl:float)
   :initform (cl:make-array 3 :element-type 'cl:float :initial-element 0.0))
   (attitude_angle
    :reader attitude_angle
    :initarg :attitude_angle
    :type (cl:vector cl:float)
   :initform (cl:make-array 3 :element-type 'cl:float :initial-element 0.0))
   (velocity
    :reader velocity
    :initarg :velocity
    :type (cl:vector cl:float)
   :initform (cl:make-array 3 :element-type 'cl:float :initial-element 0.0))
   (angular_velocity
    :reader angular_velocity
    :initarg :angular_velocity
    :type (cl:vector cl:float)
   :initform (cl:make-array 3 :element-type 'cl:float :initial-element 0.0))
   (acceleration
    :reader acceleration
    :initarg :acceleration
    :type (cl:vector cl:float)
   :initform (cl:make-array 3 :element-type 'cl:float :initial-element 0.0))
   (electric_quantity
    :reader electric_quantity
    :initarg :electric_quantity
    :type cl:float
    :initform 0.0)
   (state_word
    :reader state_word
    :initarg :state_word
    :type cl:fixnum
    :initform 0))
)

(cl:defclass State29 (<State29>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <State29>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'State29)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name hector_msgs-msg:<State29> is deprecated: use hector_msgs-msg:State29 instead.")))

(cl:ensure-generic-function 'header-val :lambda-list '(m))
(cl:defmethod header-val ((m <State29>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader hector_msgs-msg:header-val is deprecated.  Use hector_msgs-msg:header instead.")
  (header m))

(cl:ensure-generic-function 'position_gps-val :lambda-list '(m))
(cl:defmethod position_gps-val ((m <State29>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader hector_msgs-msg:position_gps-val is deprecated.  Use hector_msgs-msg:position_gps instead.")
  (position_gps m))

(cl:ensure-generic-function 'attitude_angle-val :lambda-list '(m))
(cl:defmethod attitude_angle-val ((m <State29>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader hector_msgs-msg:attitude_angle-val is deprecated.  Use hector_msgs-msg:attitude_angle instead.")
  (attitude_angle m))

(cl:ensure-generic-function 'velocity-val :lambda-list '(m))
(cl:defmethod velocity-val ((m <State29>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader hector_msgs-msg:velocity-val is deprecated.  Use hector_msgs-msg:velocity instead.")
  (velocity m))

(cl:ensure-generic-function 'angular_velocity-val :lambda-list '(m))
(cl:defmethod angular_velocity-val ((m <State29>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader hector_msgs-msg:angular_velocity-val is deprecated.  Use hector_msgs-msg:angular_velocity instead.")
  (angular_velocity m))

(cl:ensure-generic-function 'acceleration-val :lambda-list '(m))
(cl:defmethod acceleration-val ((m <State29>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader hector_msgs-msg:acceleration-val is deprecated.  Use hector_msgs-msg:acceleration instead.")
  (acceleration m))

(cl:ensure-generic-function 'electric_quantity-val :lambda-list '(m))
(cl:defmethod electric_quantity-val ((m <State29>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader hector_msgs-msg:electric_quantity-val is deprecated.  Use hector_msgs-msg:electric_quantity instead.")
  (electric_quantity m))

(cl:ensure-generic-function 'state_word-val :lambda-list '(m))
(cl:defmethod state_word-val ((m <State29>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader hector_msgs-msg:state_word-val is deprecated.  Use hector_msgs-msg:state_word instead.")
  (state_word m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <State29>) ostream)
  "Serializes a message object of type '<State29>"
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'header) ostream)
  (cl:map cl:nil #'(cl:lambda (ele) (cl:let ((bits (roslisp-utils:encode-single-float-bits ele)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream)))
   (cl:slot-value msg 'position_gps))
  (cl:map cl:nil #'(cl:lambda (ele) (cl:let ((bits (roslisp-utils:encode-single-float-bits ele)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream)))
   (cl:slot-value msg 'attitude_angle))
  (cl:map cl:nil #'(cl:lambda (ele) (cl:let ((bits (roslisp-utils:encode-single-float-bits ele)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream)))
   (cl:slot-value msg 'velocity))
  (cl:map cl:nil #'(cl:lambda (ele) (cl:let ((bits (roslisp-utils:encode-single-float-bits ele)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream)))
   (cl:slot-value msg 'angular_velocity))
  (cl:map cl:nil #'(cl:lambda (ele) (cl:let ((bits (roslisp-utils:encode-single-float-bits ele)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream)))
   (cl:slot-value msg 'acceleration))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'electric_quantity))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let* ((signed (cl:slot-value msg 'state_word)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 256) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    )
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <State29>) istream)
  "Deserializes a message object of type '<State29>"
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'header) istream)
  (cl:setf (cl:slot-value msg 'position_gps) (cl:make-array 3))
  (cl:let ((vals (cl:slot-value msg 'position_gps)))
    (cl:dotimes (i 3)
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:aref vals i) (roslisp-utils:decode-single-float-bits bits)))))
  (cl:setf (cl:slot-value msg 'attitude_angle) (cl:make-array 3))
  (cl:let ((vals (cl:slot-value msg 'attitude_angle)))
    (cl:dotimes (i 3)
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:aref vals i) (roslisp-utils:decode-single-float-bits bits)))))
  (cl:setf (cl:slot-value msg 'velocity) (cl:make-array 3))
  (cl:let ((vals (cl:slot-value msg 'velocity)))
    (cl:dotimes (i 3)
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:aref vals i) (roslisp-utils:decode-single-float-bits bits)))))
  (cl:setf (cl:slot-value msg 'angular_velocity) (cl:make-array 3))
  (cl:let ((vals (cl:slot-value msg 'angular_velocity)))
    (cl:dotimes (i 3)
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:aref vals i) (roslisp-utils:decode-single-float-bits bits)))))
  (cl:setf (cl:slot-value msg 'acceleration) (cl:make-array 3))
  (cl:let ((vals (cl:slot-value msg 'acceleration)))
    (cl:dotimes (i 3)
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:aref vals i) (roslisp-utils:decode-single-float-bits bits)))))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'electric_quantity) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'state_word) (cl:if (cl:< unsigned 128) unsigned (cl:- unsigned 256))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<State29>)))
  "Returns string type for a message object of type '<State29>"
  "hector_msgs/State29")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'State29)))
  "Returns string type for a message object of type 'State29"
  "hector_msgs/State29")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<State29>)))
  "Returns md5sum for a message object of type '<State29>"
  "a717d0d4d01f8d9677c78dc8f2b4efb4")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'State29)))
  "Returns md5sum for a message object of type 'State29"
  "a717d0d4d01f8d9677c78dc8f2b4efb4")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<State29>)))
  "Returns full string definition for message of type '<State29>"
  (cl:format cl:nil "# Vehicle state 'x_hat' output from the estimator or from simulator ~%~%Header header~%~%# Original States~%# @warning roll, pitch and yaw have always to be valid, the quaternion is optional~%float32[3] position_gps		# latitude (lat. deg), longitude (lon. deg), altitude (m)~%float32[3] attitude_angle	# Roll angle (rad), Pitch angle (rad), Yaw angle (rad)~%float32[3] velocity		# V_north (m/s), V_east (m/s), V_ground (m/s)~%float32[3] angular_velocity	# Body frame rollrate (rad/s), Body frame pitchrate (rad/s), Body frame yawrate (rad/s)~%float32[3] acceleration		# Body frame x (m/s^2), Body frame y (m/s^2), Body frame z (m/s^2)~%float32    electric_quantity	# Quantity of electric charge~%int8       state_word		# Control Status Word~%~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')~%# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%# 0: no frame~%# 1: global frame~%string frame_id~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'State29)))
  "Returns full string definition for message of type 'State29"
  (cl:format cl:nil "# Vehicle state 'x_hat' output from the estimator or from simulator ~%~%Header header~%~%# Original States~%# @warning roll, pitch and yaw have always to be valid, the quaternion is optional~%float32[3] position_gps		# latitude (lat. deg), longitude (lon. deg), altitude (m)~%float32[3] attitude_angle	# Roll angle (rad), Pitch angle (rad), Yaw angle (rad)~%float32[3] velocity		# V_north (m/s), V_east (m/s), V_ground (m/s)~%float32[3] angular_velocity	# Body frame rollrate (rad/s), Body frame pitchrate (rad/s), Body frame yawrate (rad/s)~%float32[3] acceleration		# Body frame x (m/s^2), Body frame y (m/s^2), Body frame z (m/s^2)~%float32    electric_quantity	# Quantity of electric charge~%int8       state_word		# Control Status Word~%~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')~%# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%# 0: no frame~%# 1: global frame~%string frame_id~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <State29>))
  (cl:+ 0
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'header))
     0 (cl:reduce #'cl:+ (cl:slot-value msg 'position_gps) :key #'(cl:lambda (ele) (cl:declare (cl:ignorable ele)) (cl:+ 4)))
     0 (cl:reduce #'cl:+ (cl:slot-value msg 'attitude_angle) :key #'(cl:lambda (ele) (cl:declare (cl:ignorable ele)) (cl:+ 4)))
     0 (cl:reduce #'cl:+ (cl:slot-value msg 'velocity) :key #'(cl:lambda (ele) (cl:declare (cl:ignorable ele)) (cl:+ 4)))
     0 (cl:reduce #'cl:+ (cl:slot-value msg 'angular_velocity) :key #'(cl:lambda (ele) (cl:declare (cl:ignorable ele)) (cl:+ 4)))
     0 (cl:reduce #'cl:+ (cl:slot-value msg 'acceleration) :key #'(cl:lambda (ele) (cl:declare (cl:ignorable ele)) (cl:+ 4)))
     4
     1
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <State29>))
  "Converts a ROS message object to a list"
  (cl:list 'State29
    (cl:cons ':header (header msg))
    (cl:cons ':position_gps (position_gps msg))
    (cl:cons ':attitude_angle (attitude_angle msg))
    (cl:cons ':velocity (velocity msg))
    (cl:cons ':angular_velocity (angular_velocity msg))
    (cl:cons ':acceleration (acceleration msg))
    (cl:cons ':electric_quantity (electric_quantity msg))
    (cl:cons ':state_word (state_word msg))
))
