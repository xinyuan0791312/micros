(cl:in-package hector_msgs-msg)
(cl:export '(PATH_TYPE-VAL
          PATH_TYPE
          VA_D-VAL
          VA_D
          R-VAL
          R
          Q-VAL
          Q
          C-VAL
          C
          RHO-VAL
          RHO
          H_C-VAL
          H_C
          LAMBDA-VAL
          LAMBDA
          LANDING-VAL
          LANDING
          TAKEOFF-VAL
          TAKEOFF
))