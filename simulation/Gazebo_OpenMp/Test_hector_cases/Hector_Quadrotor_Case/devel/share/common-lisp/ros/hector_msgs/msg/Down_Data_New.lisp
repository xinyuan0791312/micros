; Auto-generated. Do not edit!


(cl:in-package hector_msgs-msg)


;//! \htmlinclude Down_Data_New.msg.html

(cl:defclass <Down_Data_New> (roslisp-msg-protocol:ros-message)
  ((status_word
    :reader status_word
    :initarg :status_word
    :type cl:integer
    :initform 0)
   (longitude
    :reader longitude
    :initarg :longitude
    :type cl:float
    :initform 0.0)
   (latitude
    :reader latitude
    :initarg :latitude
    :type cl:float
    :initform 0.0)
   (altitude
    :reader altitude
    :initarg :altitude
    :type cl:float
    :initform 0.0)
   (plane_speed
    :reader plane_speed
    :initarg :plane_speed
    :type cl:float
    :initform 0.0))
)

(cl:defclass Down_Data_New (<Down_Data_New>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <Down_Data_New>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'Down_Data_New)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name hector_msgs-msg:<Down_Data_New> is deprecated: use hector_msgs-msg:Down_Data_New instead.")))

(cl:ensure-generic-function 'status_word-val :lambda-list '(m))
(cl:defmethod status_word-val ((m <Down_Data_New>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader hector_msgs-msg:status_word-val is deprecated.  Use hector_msgs-msg:status_word instead.")
  (status_word m))

(cl:ensure-generic-function 'longitude-val :lambda-list '(m))
(cl:defmethod longitude-val ((m <Down_Data_New>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader hector_msgs-msg:longitude-val is deprecated.  Use hector_msgs-msg:longitude instead.")
  (longitude m))

(cl:ensure-generic-function 'latitude-val :lambda-list '(m))
(cl:defmethod latitude-val ((m <Down_Data_New>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader hector_msgs-msg:latitude-val is deprecated.  Use hector_msgs-msg:latitude instead.")
  (latitude m))

(cl:ensure-generic-function 'altitude-val :lambda-list '(m))
(cl:defmethod altitude-val ((m <Down_Data_New>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader hector_msgs-msg:altitude-val is deprecated.  Use hector_msgs-msg:altitude instead.")
  (altitude m))

(cl:ensure-generic-function 'plane_speed-val :lambda-list '(m))
(cl:defmethod plane_speed-val ((m <Down_Data_New>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader hector_msgs-msg:plane_speed-val is deprecated.  Use hector_msgs-msg:plane_speed instead.")
  (plane_speed m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <Down_Data_New>) ostream)
  "Serializes a message object of type '<Down_Data_New>"
  (cl:let* ((signed (cl:slot-value msg 'status_word)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 4294967296) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) unsigned) ostream)
    )
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'longitude))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'latitude))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'altitude))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'plane_speed))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <Down_Data_New>) istream)
  "Deserializes a message object of type '<Down_Data_New>"
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'status_word) (cl:if (cl:< unsigned 2147483648) unsigned (cl:- unsigned 4294967296))))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'longitude) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'latitude) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'altitude) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'plane_speed) (roslisp-utils:decode-single-float-bits bits)))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<Down_Data_New>)))
  "Returns string type for a message object of type '<Down_Data_New>"
  "hector_msgs/Down_Data_New")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'Down_Data_New)))
  "Returns string type for a message object of type 'Down_Data_New"
  "hector_msgs/Down_Data_New")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<Down_Data_New>)))
  "Returns md5sum for a message object of type '<Down_Data_New>"
  "a298ef3ba270277bca4819083a5239f4")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'Down_Data_New)))
  "Returns md5sum for a message object of type 'Down_Data_New"
  "a298ef3ba270277bca4819083a5239f4")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<Down_Data_New>)))
  "Returns full string definition for message of type '<Down_Data_New>"
  (cl:format cl:nil "int32 status_word ~%float32 longitude~%float32 latitude~%float32 altitude~%float32 plane_speed    #desired speed~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'Down_Data_New)))
  "Returns full string definition for message of type 'Down_Data_New"
  (cl:format cl:nil "int32 status_word ~%float32 longitude~%float32 latitude~%float32 altitude~%float32 plane_speed    #desired speed~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <Down_Data_New>))
  (cl:+ 0
     4
     4
     4
     4
     4
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <Down_Data_New>))
  "Converts a ROS message object to a list"
  (cl:list 'Down_Data_New
    (cl:cons ':status_word (status_word msg))
    (cl:cons ':longitude (longitude msg))
    (cl:cons ':latitude (latitude msg))
    (cl:cons ':altitude (altitude msg))
    (cl:cons ':plane_speed (plane_speed msg))
))
