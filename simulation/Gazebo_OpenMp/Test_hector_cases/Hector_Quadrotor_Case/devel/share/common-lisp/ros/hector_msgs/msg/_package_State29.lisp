(cl:in-package hector_msgs-msg)
(cl:export '(HEADER-VAL
          HEADER
          POSITION_GPS-VAL
          POSITION_GPS
          ATTITUDE_ANGLE-VAL
          ATTITUDE_ANGLE
          VELOCITY-VAL
          VELOCITY
          ANGULAR_VELOCITY-VAL
          ANGULAR_VELOCITY
          ACCELERATION-VAL
          ACCELERATION
          ELECTRIC_QUANTITY-VAL
          ELECTRIC_QUANTITY
          STATE_WORD-VAL
          STATE_WORD
))