; Auto-generated. Do not edit!


(cl:in-package hector_msgs-msg)


;//! \htmlinclude Goal_Info.msg.html

(cl:defclass <Goal_Info> (roslisp-msg-protocol:ros-message)
  ((v_d
    :reader v_d
    :initarg :v_d
    :type cl:float
    :initform 0.0)
   (xy
    :reader xy
    :initarg :xy
    :type (cl:vector cl:float)
   :initform (cl:make-array 2 :element-type 'cl:float :initial-element 0.0))
   (ll
    :reader ll
    :initarg :ll
    :type (cl:vector cl:float)
   :initform (cl:make-array 2 :element-type 'cl:float :initial-element 0.0))
   (action
    :reader action
    :initarg :action
    :type cl:integer
    :initform 0)
   (altitude
    :reader altitude
    :initarg :altitude
    :type cl:float
    :initform 0.0))
)

(cl:defclass Goal_Info (<Goal_Info>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <Goal_Info>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'Goal_Info)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name hector_msgs-msg:<Goal_Info> is deprecated: use hector_msgs-msg:Goal_Info instead.")))

(cl:ensure-generic-function 'v_d-val :lambda-list '(m))
(cl:defmethod v_d-val ((m <Goal_Info>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader hector_msgs-msg:v_d-val is deprecated.  Use hector_msgs-msg:v_d instead.")
  (v_d m))

(cl:ensure-generic-function 'xy-val :lambda-list '(m))
(cl:defmethod xy-val ((m <Goal_Info>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader hector_msgs-msg:xy-val is deprecated.  Use hector_msgs-msg:xy instead.")
  (xy m))

(cl:ensure-generic-function 'll-val :lambda-list '(m))
(cl:defmethod ll-val ((m <Goal_Info>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader hector_msgs-msg:ll-val is deprecated.  Use hector_msgs-msg:ll instead.")
  (ll m))

(cl:ensure-generic-function 'action-val :lambda-list '(m))
(cl:defmethod action-val ((m <Goal_Info>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader hector_msgs-msg:action-val is deprecated.  Use hector_msgs-msg:action instead.")
  (action m))

(cl:ensure-generic-function 'altitude-val :lambda-list '(m))
(cl:defmethod altitude-val ((m <Goal_Info>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader hector_msgs-msg:altitude-val is deprecated.  Use hector_msgs-msg:altitude instead.")
  (altitude m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <Goal_Info>) ostream)
  "Serializes a message object of type '<Goal_Info>"
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'v_d))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:map cl:nil #'(cl:lambda (ele) (cl:let ((bits (roslisp-utils:encode-single-float-bits ele)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream)))
   (cl:slot-value msg 'xy))
  (cl:map cl:nil #'(cl:lambda (ele) (cl:let ((bits (roslisp-utils:encode-single-float-bits ele)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream)))
   (cl:slot-value msg 'll))
  (cl:let* ((signed (cl:slot-value msg 'action)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 4294967296) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) unsigned) ostream)
    )
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'altitude))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <Goal_Info>) istream)
  "Deserializes a message object of type '<Goal_Info>"
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'v_d) (roslisp-utils:decode-single-float-bits bits)))
  (cl:setf (cl:slot-value msg 'xy) (cl:make-array 2))
  (cl:let ((vals (cl:slot-value msg 'xy)))
    (cl:dotimes (i 2)
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:aref vals i) (roslisp-utils:decode-single-float-bits bits)))))
  (cl:setf (cl:slot-value msg 'll) (cl:make-array 2))
  (cl:let ((vals (cl:slot-value msg 'll)))
    (cl:dotimes (i 2)
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:aref vals i) (roslisp-utils:decode-single-float-bits bits)))))
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'action) (cl:if (cl:< unsigned 2147483648) unsigned (cl:- unsigned 4294967296))))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'altitude) (roslisp-utils:decode-single-float-bits bits)))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<Goal_Info>)))
  "Returns string type for a message object of type '<Goal_Info>"
  "hector_msgs/Goal_Info")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'Goal_Info)))
  "Returns string type for a message object of type 'Goal_Info"
  "hector_msgs/Goal_Info")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<Goal_Info>)))
  "Returns md5sum for a message object of type '<Goal_Info>"
  "14ef783638203eefb7fbb5a851185abb")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'Goal_Info)))
  "Returns md5sum for a message object of type 'Goal_Info"
  "14ef783638203eefb7fbb5a851185abb")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<Goal_Info>)))
  "Returns full string definition for message of type '<Goal_Info>"
  (cl:format cl:nil "# Current path goal output from the path manager, add by kobe~%~%float32 v_d		# Desired airspeed (m/s)~%float32[2] xy		# x-y-coordinate (m)~%float32[2] ll		# latitude-longtitude~%int32 action		# expected action move~%float32 altitude             # desire altitude (m)~%~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'Goal_Info)))
  "Returns full string definition for message of type 'Goal_Info"
  (cl:format cl:nil "# Current path goal output from the path manager, add by kobe~%~%float32 v_d		# Desired airspeed (m/s)~%float32[2] xy		# x-y-coordinate (m)~%float32[2] ll		# latitude-longtitude~%int32 action		# expected action move~%float32 altitude             # desire altitude (m)~%~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <Goal_Info>))
  (cl:+ 0
     4
     0 (cl:reduce #'cl:+ (cl:slot-value msg 'xy) :key #'(cl:lambda (ele) (cl:declare (cl:ignorable ele)) (cl:+ 4)))
     0 (cl:reduce #'cl:+ (cl:slot-value msg 'll) :key #'(cl:lambda (ele) (cl:declare (cl:ignorable ele)) (cl:+ 4)))
     4
     4
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <Goal_Info>))
  "Converts a ROS message object to a list"
  (cl:list 'Goal_Info
    (cl:cons ':v_d (v_d msg))
    (cl:cons ':xy (xy msg))
    (cl:cons ':ll (ll msg))
    (cl:cons ':action (action msg))
    (cl:cons ':altitude (altitude msg))
))
