(cl:in-package hector_msgs-msg)
(cl:export '(STATUS_WORD-VAL
          STATUS_WORD
          LONGITUDE-VAL
          LONGITUDE
          LATITUDE-VAL
          LATITUDE
          ALTITUDE-VAL
          ALTITUDE
          PLANE_SPEED-VAL
          PLANE_SPEED
))