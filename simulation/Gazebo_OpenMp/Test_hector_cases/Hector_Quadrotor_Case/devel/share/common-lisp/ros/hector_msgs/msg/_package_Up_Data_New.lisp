(cl:in-package hector_msgs-msg)
(cl:export '(STATUS_WORD-VAL
          STATUS_WORD
          LONGITUDE-VAL
          LONGITUDE
          LATITUDE-VAL
          LATITUDE
          ALTITUDE-VAL
          ALTITUDE
          ROLL_ANGLE-VAL
          ROLL_ANGLE
          PITCH_ANGLE-VAL
          PITCH_ANGLE
          YAW_ANGLE-VAL
          YAW_ANGLE
          ANGULAR_RATE_X-VAL
          ANGULAR_RATE_X
          ANGULAR_RATE_Y-VAL
          ANGULAR_RATE_Y
          ANGULAR_RATE_Z-VAL
          ANGULAR_RATE_Z
          NORTH_DIRECTION_SPEED-VAL
          NORTH_DIRECTION_SPEED
          EAST_DIRECTION_SPEED-VAL
          EAST_DIRECTION_SPEED
          GROUND_DIRECTION_SPEED-VAL
          GROUND_DIRECTION_SPEED
          ACCELERATION_X-VAL
          ACCELERATION_X
          ACCELERATION_Y-VAL
          ACCELERATION_Y
          ACCELERATION_Z-VAL
          ACCELERATION_Z
))