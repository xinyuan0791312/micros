(cl:in-package hector_msgs-msg)
(cl:export '(W-VAL
          W
          LAT-VAL
          LAT
          LON-VAL
          LON
          CHI_D-VAL
          CHI_D
          CHI_VALID-VAL
          CHI_VALID
          VA_D-VAL
          VA_D
          SET_CURRENT-VAL
          SET_CURRENT
          CLEAR_WP_LIST-VAL
          CLEAR_WP_LIST
          LANDING-VAL
          LANDING
          TAKEOFF-VAL
          TAKEOFF
))