; Auto-generated. Do not edit!


(cl:in-package hector_msgs-msg)


;//! \htmlinclude Up_Data_New.msg.html

(cl:defclass <Up_Data_New> (roslisp-msg-protocol:ros-message)
  ((status_word
    :reader status_word
    :initarg :status_word
    :type cl:integer
    :initform 0)
   (longitude
    :reader longitude
    :initarg :longitude
    :type cl:float
    :initform 0.0)
   (latitude
    :reader latitude
    :initarg :latitude
    :type cl:float
    :initform 0.0)
   (altitude
    :reader altitude
    :initarg :altitude
    :type cl:float
    :initform 0.0)
   (roll_angle
    :reader roll_angle
    :initarg :roll_angle
    :type cl:float
    :initform 0.0)
   (pitch_angle
    :reader pitch_angle
    :initarg :pitch_angle
    :type cl:float
    :initform 0.0)
   (yaw_angle
    :reader yaw_angle
    :initarg :yaw_angle
    :type cl:float
    :initform 0.0)
   (angular_rate_x
    :reader angular_rate_x
    :initarg :angular_rate_x
    :type cl:float
    :initform 0.0)
   (angular_rate_y
    :reader angular_rate_y
    :initarg :angular_rate_y
    :type cl:float
    :initform 0.0)
   (angular_rate_z
    :reader angular_rate_z
    :initarg :angular_rate_z
    :type cl:float
    :initform 0.0)
   (north_direction_speed
    :reader north_direction_speed
    :initarg :north_direction_speed
    :type cl:float
    :initform 0.0)
   (east_direction_speed
    :reader east_direction_speed
    :initarg :east_direction_speed
    :type cl:float
    :initform 0.0)
   (ground_direction_speed
    :reader ground_direction_speed
    :initarg :ground_direction_speed
    :type cl:float
    :initform 0.0)
   (acceleration_x
    :reader acceleration_x
    :initarg :acceleration_x
    :type cl:float
    :initform 0.0)
   (acceleration_y
    :reader acceleration_y
    :initarg :acceleration_y
    :type cl:float
    :initform 0.0)
   (acceleration_z
    :reader acceleration_z
    :initarg :acceleration_z
    :type cl:float
    :initform 0.0))
)

(cl:defclass Up_Data_New (<Up_Data_New>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <Up_Data_New>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'Up_Data_New)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name hector_msgs-msg:<Up_Data_New> is deprecated: use hector_msgs-msg:Up_Data_New instead.")))

(cl:ensure-generic-function 'status_word-val :lambda-list '(m))
(cl:defmethod status_word-val ((m <Up_Data_New>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader hector_msgs-msg:status_word-val is deprecated.  Use hector_msgs-msg:status_word instead.")
  (status_word m))

(cl:ensure-generic-function 'longitude-val :lambda-list '(m))
(cl:defmethod longitude-val ((m <Up_Data_New>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader hector_msgs-msg:longitude-val is deprecated.  Use hector_msgs-msg:longitude instead.")
  (longitude m))

(cl:ensure-generic-function 'latitude-val :lambda-list '(m))
(cl:defmethod latitude-val ((m <Up_Data_New>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader hector_msgs-msg:latitude-val is deprecated.  Use hector_msgs-msg:latitude instead.")
  (latitude m))

(cl:ensure-generic-function 'altitude-val :lambda-list '(m))
(cl:defmethod altitude-val ((m <Up_Data_New>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader hector_msgs-msg:altitude-val is deprecated.  Use hector_msgs-msg:altitude instead.")
  (altitude m))

(cl:ensure-generic-function 'roll_angle-val :lambda-list '(m))
(cl:defmethod roll_angle-val ((m <Up_Data_New>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader hector_msgs-msg:roll_angle-val is deprecated.  Use hector_msgs-msg:roll_angle instead.")
  (roll_angle m))

(cl:ensure-generic-function 'pitch_angle-val :lambda-list '(m))
(cl:defmethod pitch_angle-val ((m <Up_Data_New>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader hector_msgs-msg:pitch_angle-val is deprecated.  Use hector_msgs-msg:pitch_angle instead.")
  (pitch_angle m))

(cl:ensure-generic-function 'yaw_angle-val :lambda-list '(m))
(cl:defmethod yaw_angle-val ((m <Up_Data_New>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader hector_msgs-msg:yaw_angle-val is deprecated.  Use hector_msgs-msg:yaw_angle instead.")
  (yaw_angle m))

(cl:ensure-generic-function 'angular_rate_x-val :lambda-list '(m))
(cl:defmethod angular_rate_x-val ((m <Up_Data_New>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader hector_msgs-msg:angular_rate_x-val is deprecated.  Use hector_msgs-msg:angular_rate_x instead.")
  (angular_rate_x m))

(cl:ensure-generic-function 'angular_rate_y-val :lambda-list '(m))
(cl:defmethod angular_rate_y-val ((m <Up_Data_New>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader hector_msgs-msg:angular_rate_y-val is deprecated.  Use hector_msgs-msg:angular_rate_y instead.")
  (angular_rate_y m))

(cl:ensure-generic-function 'angular_rate_z-val :lambda-list '(m))
(cl:defmethod angular_rate_z-val ((m <Up_Data_New>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader hector_msgs-msg:angular_rate_z-val is deprecated.  Use hector_msgs-msg:angular_rate_z instead.")
  (angular_rate_z m))

(cl:ensure-generic-function 'north_direction_speed-val :lambda-list '(m))
(cl:defmethod north_direction_speed-val ((m <Up_Data_New>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader hector_msgs-msg:north_direction_speed-val is deprecated.  Use hector_msgs-msg:north_direction_speed instead.")
  (north_direction_speed m))

(cl:ensure-generic-function 'east_direction_speed-val :lambda-list '(m))
(cl:defmethod east_direction_speed-val ((m <Up_Data_New>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader hector_msgs-msg:east_direction_speed-val is deprecated.  Use hector_msgs-msg:east_direction_speed instead.")
  (east_direction_speed m))

(cl:ensure-generic-function 'ground_direction_speed-val :lambda-list '(m))
(cl:defmethod ground_direction_speed-val ((m <Up_Data_New>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader hector_msgs-msg:ground_direction_speed-val is deprecated.  Use hector_msgs-msg:ground_direction_speed instead.")
  (ground_direction_speed m))

(cl:ensure-generic-function 'acceleration_x-val :lambda-list '(m))
(cl:defmethod acceleration_x-val ((m <Up_Data_New>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader hector_msgs-msg:acceleration_x-val is deprecated.  Use hector_msgs-msg:acceleration_x instead.")
  (acceleration_x m))

(cl:ensure-generic-function 'acceleration_y-val :lambda-list '(m))
(cl:defmethod acceleration_y-val ((m <Up_Data_New>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader hector_msgs-msg:acceleration_y-val is deprecated.  Use hector_msgs-msg:acceleration_y instead.")
  (acceleration_y m))

(cl:ensure-generic-function 'acceleration_z-val :lambda-list '(m))
(cl:defmethod acceleration_z-val ((m <Up_Data_New>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader hector_msgs-msg:acceleration_z-val is deprecated.  Use hector_msgs-msg:acceleration_z instead.")
  (acceleration_z m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <Up_Data_New>) ostream)
  "Serializes a message object of type '<Up_Data_New>"
  (cl:let* ((signed (cl:slot-value msg 'status_word)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 4294967296) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) unsigned) ostream)
    )
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'longitude))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'latitude))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'altitude))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'roll_angle))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'pitch_angle))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'yaw_angle))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'angular_rate_x))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'angular_rate_y))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'angular_rate_z))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'north_direction_speed))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'east_direction_speed))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'ground_direction_speed))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'acceleration_x))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'acceleration_y))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'acceleration_z))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <Up_Data_New>) istream)
  "Deserializes a message object of type '<Up_Data_New>"
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'status_word) (cl:if (cl:< unsigned 2147483648) unsigned (cl:- unsigned 4294967296))))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'longitude) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'latitude) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'altitude) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'roll_angle) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'pitch_angle) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'yaw_angle) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'angular_rate_x) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'angular_rate_y) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'angular_rate_z) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'north_direction_speed) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'east_direction_speed) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'ground_direction_speed) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'acceleration_x) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'acceleration_y) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'acceleration_z) (roslisp-utils:decode-single-float-bits bits)))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<Up_Data_New>)))
  "Returns string type for a message object of type '<Up_Data_New>"
  "hector_msgs/Up_Data_New")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'Up_Data_New)))
  "Returns string type for a message object of type 'Up_Data_New"
  "hector_msgs/Up_Data_New")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<Up_Data_New>)))
  "Returns md5sum for a message object of type '<Up_Data_New>"
  "f4d381e3081b932960cb54e2b4ced91c")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'Up_Data_New)))
  "Returns md5sum for a message object of type 'Up_Data_New"
  "f4d381e3081b932960cb54e2b4ced91c")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<Up_Data_New>)))
  "Returns full string definition for message of type '<Up_Data_New>"
  (cl:format cl:nil "int32 status_word ~%float32 longitude~%float32 latitude  ~%float32 altitude                     # (m)~%float32 roll_angle                   #(rad)     ~%float32 pitch_angle ~%float32 yaw_angle ~%float32 angular_rate_x               #Body frame rollrate (rad/s)     ~%float32 angular_rate_y               #Body frame pitchrate (rad/s)~%float32 angular_rate_z               #Body frame yawrate (rad/s)~%float32 north_direction_speed        #(m/s)~%float32 east_direction_speed ~%float32 ground_direction_speed  ~%float32 acceleration_x               #(m/s^2)~%float32 acceleration_y~%float32 acceleration_z~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'Up_Data_New)))
  "Returns full string definition for message of type 'Up_Data_New"
  (cl:format cl:nil "int32 status_word ~%float32 longitude~%float32 latitude  ~%float32 altitude                     # (m)~%float32 roll_angle                   #(rad)     ~%float32 pitch_angle ~%float32 yaw_angle ~%float32 angular_rate_x               #Body frame rollrate (rad/s)     ~%float32 angular_rate_y               #Body frame pitchrate (rad/s)~%float32 angular_rate_z               #Body frame yawrate (rad/s)~%float32 north_direction_speed        #(m/s)~%float32 east_direction_speed ~%float32 ground_direction_speed  ~%float32 acceleration_x               #(m/s^2)~%float32 acceleration_y~%float32 acceleration_z~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <Up_Data_New>))
  (cl:+ 0
     4
     4
     4
     4
     4
     4
     4
     4
     4
     4
     4
     4
     4
     4
     4
     4
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <Up_Data_New>))
  "Converts a ROS message object to a list"
  (cl:list 'Up_Data_New
    (cl:cons ':status_word (status_word msg))
    (cl:cons ':longitude (longitude msg))
    (cl:cons ':latitude (latitude msg))
    (cl:cons ':altitude (altitude msg))
    (cl:cons ':roll_angle (roll_angle msg))
    (cl:cons ':pitch_angle (pitch_angle msg))
    (cl:cons ':yaw_angle (yaw_angle msg))
    (cl:cons ':angular_rate_x (angular_rate_x msg))
    (cl:cons ':angular_rate_y (angular_rate_y msg))
    (cl:cons ':angular_rate_z (angular_rate_z msg))
    (cl:cons ':north_direction_speed (north_direction_speed msg))
    (cl:cons ':east_direction_speed (east_direction_speed msg))
    (cl:cons ':ground_direction_speed (ground_direction_speed msg))
    (cl:cons ':acceleration_x (acceleration_x msg))
    (cl:cons ':acceleration_y (acceleration_y msg))
    (cl:cons ':acceleration_z (acceleration_z msg))
))
