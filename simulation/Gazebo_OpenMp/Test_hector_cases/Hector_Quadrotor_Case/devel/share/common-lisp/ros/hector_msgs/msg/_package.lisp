(cl:defpackage hector_msgs-msg
  (:use )
  (:export
   "<CONTROLLER_COMMANDS>"
   "CONTROLLER_COMMANDS"
   "<CONTROLLER_INTERNALS>"
   "CONTROLLER_INTERNALS"
   "<CURRENT_PATH>"
   "CURRENT_PATH"
   "<DOWN_DATA_NEW>"
   "DOWN_DATA_NEW"
   "<FORMATION_TYPE>"
   "FORMATION_TYPE"
   "<GOAL_INFO>"
   "GOAL_INFO"
   "<STATE>"
   "STATE"
   "<STATE29>"
   "STATE29"
   "<UP_DATA_NEW>"
   "UP_DATA_NEW"
   "<WAYPOINT>"
   "WAYPOINT"
   "<WAYPOINT (复件)>"
   "WAYPOINT (复件)"
  ))

