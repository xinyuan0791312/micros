;; Auto-generated. Do not edit!


(when (boundp 'hector_msgs::Up_Data_New)
  (if (not (find-package "HECTOR_MSGS"))
    (make-package "HECTOR_MSGS"))
  (shadow 'Up_Data_New (find-package "HECTOR_MSGS")))
(unless (find-package "HECTOR_MSGS::UP_DATA_NEW")
  (make-package "HECTOR_MSGS::UP_DATA_NEW"))

(in-package "ROS")
;;//! \htmlinclude Up_Data_New.msg.html


(defclass hector_msgs::Up_Data_New
  :super ros::object
  :slots (_status_word _longitude _latitude _altitude _roll_angle _pitch_angle _yaw_angle _angular_rate_x _angular_rate_y _angular_rate_z _north_direction_speed _east_direction_speed _ground_direction_speed _acceleration_x _acceleration_y _acceleration_z ))

(defmethod hector_msgs::Up_Data_New
  (:init
   (&key
    ((:status_word __status_word) 0)
    ((:longitude __longitude) 0.0)
    ((:latitude __latitude) 0.0)
    ((:altitude __altitude) 0.0)
    ((:roll_angle __roll_angle) 0.0)
    ((:pitch_angle __pitch_angle) 0.0)
    ((:yaw_angle __yaw_angle) 0.0)
    ((:angular_rate_x __angular_rate_x) 0.0)
    ((:angular_rate_y __angular_rate_y) 0.0)
    ((:angular_rate_z __angular_rate_z) 0.0)
    ((:north_direction_speed __north_direction_speed) 0.0)
    ((:east_direction_speed __east_direction_speed) 0.0)
    ((:ground_direction_speed __ground_direction_speed) 0.0)
    ((:acceleration_x __acceleration_x) 0.0)
    ((:acceleration_y __acceleration_y) 0.0)
    ((:acceleration_z __acceleration_z) 0.0)
    )
   (send-super :init)
   (setq _status_word (round __status_word))
   (setq _longitude (float __longitude))
   (setq _latitude (float __latitude))
   (setq _altitude (float __altitude))
   (setq _roll_angle (float __roll_angle))
   (setq _pitch_angle (float __pitch_angle))
   (setq _yaw_angle (float __yaw_angle))
   (setq _angular_rate_x (float __angular_rate_x))
   (setq _angular_rate_y (float __angular_rate_y))
   (setq _angular_rate_z (float __angular_rate_z))
   (setq _north_direction_speed (float __north_direction_speed))
   (setq _east_direction_speed (float __east_direction_speed))
   (setq _ground_direction_speed (float __ground_direction_speed))
   (setq _acceleration_x (float __acceleration_x))
   (setq _acceleration_y (float __acceleration_y))
   (setq _acceleration_z (float __acceleration_z))
   self)
  (:status_word
   (&optional __status_word)
   (if __status_word (setq _status_word __status_word)) _status_word)
  (:longitude
   (&optional __longitude)
   (if __longitude (setq _longitude __longitude)) _longitude)
  (:latitude
   (&optional __latitude)
   (if __latitude (setq _latitude __latitude)) _latitude)
  (:altitude
   (&optional __altitude)
   (if __altitude (setq _altitude __altitude)) _altitude)
  (:roll_angle
   (&optional __roll_angle)
   (if __roll_angle (setq _roll_angle __roll_angle)) _roll_angle)
  (:pitch_angle
   (&optional __pitch_angle)
   (if __pitch_angle (setq _pitch_angle __pitch_angle)) _pitch_angle)
  (:yaw_angle
   (&optional __yaw_angle)
   (if __yaw_angle (setq _yaw_angle __yaw_angle)) _yaw_angle)
  (:angular_rate_x
   (&optional __angular_rate_x)
   (if __angular_rate_x (setq _angular_rate_x __angular_rate_x)) _angular_rate_x)
  (:angular_rate_y
   (&optional __angular_rate_y)
   (if __angular_rate_y (setq _angular_rate_y __angular_rate_y)) _angular_rate_y)
  (:angular_rate_z
   (&optional __angular_rate_z)
   (if __angular_rate_z (setq _angular_rate_z __angular_rate_z)) _angular_rate_z)
  (:north_direction_speed
   (&optional __north_direction_speed)
   (if __north_direction_speed (setq _north_direction_speed __north_direction_speed)) _north_direction_speed)
  (:east_direction_speed
   (&optional __east_direction_speed)
   (if __east_direction_speed (setq _east_direction_speed __east_direction_speed)) _east_direction_speed)
  (:ground_direction_speed
   (&optional __ground_direction_speed)
   (if __ground_direction_speed (setq _ground_direction_speed __ground_direction_speed)) _ground_direction_speed)
  (:acceleration_x
   (&optional __acceleration_x)
   (if __acceleration_x (setq _acceleration_x __acceleration_x)) _acceleration_x)
  (:acceleration_y
   (&optional __acceleration_y)
   (if __acceleration_y (setq _acceleration_y __acceleration_y)) _acceleration_y)
  (:acceleration_z
   (&optional __acceleration_z)
   (if __acceleration_z (setq _acceleration_z __acceleration_z)) _acceleration_z)
  (:serialization-length
   ()
   (+
    ;; int32 _status_word
    4
    ;; float32 _longitude
    4
    ;; float32 _latitude
    4
    ;; float32 _altitude
    4
    ;; float32 _roll_angle
    4
    ;; float32 _pitch_angle
    4
    ;; float32 _yaw_angle
    4
    ;; float32 _angular_rate_x
    4
    ;; float32 _angular_rate_y
    4
    ;; float32 _angular_rate_z
    4
    ;; float32 _north_direction_speed
    4
    ;; float32 _east_direction_speed
    4
    ;; float32 _ground_direction_speed
    4
    ;; float32 _acceleration_x
    4
    ;; float32 _acceleration_y
    4
    ;; float32 _acceleration_z
    4
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; int32 _status_word
       (write-long _status_word s)
     ;; float32 _longitude
       (sys::poke _longitude (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _latitude
       (sys::poke _latitude (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _altitude
       (sys::poke _altitude (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _roll_angle
       (sys::poke _roll_angle (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _pitch_angle
       (sys::poke _pitch_angle (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _yaw_angle
       (sys::poke _yaw_angle (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _angular_rate_x
       (sys::poke _angular_rate_x (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _angular_rate_y
       (sys::poke _angular_rate_y (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _angular_rate_z
       (sys::poke _angular_rate_z (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _north_direction_speed
       (sys::poke _north_direction_speed (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _east_direction_speed
       (sys::poke _east_direction_speed (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _ground_direction_speed
       (sys::poke _ground_direction_speed (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _acceleration_x
       (sys::poke _acceleration_x (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _acceleration_y
       (sys::poke _acceleration_y (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _acceleration_z
       (sys::poke _acceleration_z (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; int32 _status_word
     (setq _status_word (sys::peek buf ptr- :integer)) (incf ptr- 4)
   ;; float32 _longitude
     (setq _longitude (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _latitude
     (setq _latitude (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _altitude
     (setq _altitude (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _roll_angle
     (setq _roll_angle (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _pitch_angle
     (setq _pitch_angle (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _yaw_angle
     (setq _yaw_angle (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _angular_rate_x
     (setq _angular_rate_x (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _angular_rate_y
     (setq _angular_rate_y (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _angular_rate_z
     (setq _angular_rate_z (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _north_direction_speed
     (setq _north_direction_speed (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _east_direction_speed
     (setq _east_direction_speed (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _ground_direction_speed
     (setq _ground_direction_speed (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _acceleration_x
     (setq _acceleration_x (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _acceleration_y
     (setq _acceleration_y (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _acceleration_z
     (setq _acceleration_z (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;;
   self)
  )

(setf (get hector_msgs::Up_Data_New :md5sum-) "f4d381e3081b932960cb54e2b4ced91c")
(setf (get hector_msgs::Up_Data_New :datatype-) "hector_msgs/Up_Data_New")
(setf (get hector_msgs::Up_Data_New :definition-)
      "int32 status_word 
float32 longitude
float32 latitude  
float32 altitude                     # (m)
float32 roll_angle                   #(rad)     
float32 pitch_angle 
float32 yaw_angle 
float32 angular_rate_x               #Body frame rollrate (rad/s)     
float32 angular_rate_y               #Body frame pitchrate (rad/s)
float32 angular_rate_z               #Body frame yawrate (rad/s)
float32 north_direction_speed        #(m/s)
float32 east_direction_speed 
float32 ground_direction_speed  
float32 acceleration_x               #(m/s^2)
float32 acceleration_y
float32 acceleration_z

")



(provide :hector_msgs/Up_Data_New "f4d381e3081b932960cb54e2b4ced91c")


