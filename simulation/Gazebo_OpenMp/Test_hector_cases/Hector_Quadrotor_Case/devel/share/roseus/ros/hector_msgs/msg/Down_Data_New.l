;; Auto-generated. Do not edit!


(when (boundp 'hector_msgs::Down_Data_New)
  (if (not (find-package "HECTOR_MSGS"))
    (make-package "HECTOR_MSGS"))
  (shadow 'Down_Data_New (find-package "HECTOR_MSGS")))
(unless (find-package "HECTOR_MSGS::DOWN_DATA_NEW")
  (make-package "HECTOR_MSGS::DOWN_DATA_NEW"))

(in-package "ROS")
;;//! \htmlinclude Down_Data_New.msg.html


(defclass hector_msgs::Down_Data_New
  :super ros::object
  :slots (_status_word _longitude _latitude _altitude _plane_speed ))

(defmethod hector_msgs::Down_Data_New
  (:init
   (&key
    ((:status_word __status_word) 0)
    ((:longitude __longitude) 0.0)
    ((:latitude __latitude) 0.0)
    ((:altitude __altitude) 0.0)
    ((:plane_speed __plane_speed) 0.0)
    )
   (send-super :init)
   (setq _status_word (round __status_word))
   (setq _longitude (float __longitude))
   (setq _latitude (float __latitude))
   (setq _altitude (float __altitude))
   (setq _plane_speed (float __plane_speed))
   self)
  (:status_word
   (&optional __status_word)
   (if __status_word (setq _status_word __status_word)) _status_word)
  (:longitude
   (&optional __longitude)
   (if __longitude (setq _longitude __longitude)) _longitude)
  (:latitude
   (&optional __latitude)
   (if __latitude (setq _latitude __latitude)) _latitude)
  (:altitude
   (&optional __altitude)
   (if __altitude (setq _altitude __altitude)) _altitude)
  (:plane_speed
   (&optional __plane_speed)
   (if __plane_speed (setq _plane_speed __plane_speed)) _plane_speed)
  (:serialization-length
   ()
   (+
    ;; int32 _status_word
    4
    ;; float32 _longitude
    4
    ;; float32 _latitude
    4
    ;; float32 _altitude
    4
    ;; float32 _plane_speed
    4
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; int32 _status_word
       (write-long _status_word s)
     ;; float32 _longitude
       (sys::poke _longitude (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _latitude
       (sys::poke _latitude (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _altitude
       (sys::poke _altitude (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _plane_speed
       (sys::poke _plane_speed (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; int32 _status_word
     (setq _status_word (sys::peek buf ptr- :integer)) (incf ptr- 4)
   ;; float32 _longitude
     (setq _longitude (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _latitude
     (setq _latitude (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _altitude
     (setq _altitude (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _plane_speed
     (setq _plane_speed (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;;
   self)
  )

(setf (get hector_msgs::Down_Data_New :md5sum-) "a298ef3ba270277bca4819083a5239f4")
(setf (get hector_msgs::Down_Data_New :datatype-) "hector_msgs/Down_Data_New")
(setf (get hector_msgs::Down_Data_New :definition-)
      "int32 status_word 
float32 longitude
float32 latitude
float32 altitude
float32 plane_speed    #desired speed

")



(provide :hector_msgs/Down_Data_New "a298ef3ba270277bca4819083a5239f4")


