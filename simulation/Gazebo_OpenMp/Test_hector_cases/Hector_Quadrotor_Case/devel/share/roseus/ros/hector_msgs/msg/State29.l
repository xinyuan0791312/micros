;; Auto-generated. Do not edit!


(when (boundp 'hector_msgs::State29)
  (if (not (find-package "HECTOR_MSGS"))
    (make-package "HECTOR_MSGS"))
  (shadow 'State29 (find-package "HECTOR_MSGS")))
(unless (find-package "HECTOR_MSGS::STATE29")
  (make-package "HECTOR_MSGS::STATE29"))

(in-package "ROS")
;;//! \htmlinclude State29.msg.html
(if (not (find-package "STD_MSGS"))
  (ros::roseus-add-msgs "std_msgs"))


(defclass hector_msgs::State29
  :super ros::object
  :slots (_header _position_gps _attitude_angle _velocity _angular_velocity _acceleration _electric_quantity _state_word ))

(defmethod hector_msgs::State29
  (:init
   (&key
    ((:header __header) (instance std_msgs::Header :init))
    ((:position_gps __position_gps) (make-array 3 :initial-element 0.0 :element-type :float))
    ((:attitude_angle __attitude_angle) (make-array 3 :initial-element 0.0 :element-type :float))
    ((:velocity __velocity) (make-array 3 :initial-element 0.0 :element-type :float))
    ((:angular_velocity __angular_velocity) (make-array 3 :initial-element 0.0 :element-type :float))
    ((:acceleration __acceleration) (make-array 3 :initial-element 0.0 :element-type :float))
    ((:electric_quantity __electric_quantity) 0.0)
    ((:state_word __state_word) 0)
    )
   (send-super :init)
   (setq _header __header)
   (setq _position_gps __position_gps)
   (setq _attitude_angle __attitude_angle)
   (setq _velocity __velocity)
   (setq _angular_velocity __angular_velocity)
   (setq _acceleration __acceleration)
   (setq _electric_quantity (float __electric_quantity))
   (setq _state_word (round __state_word))
   self)
  (:header
   (&rest __header)
   (if (keywordp (car __header))
       (send* _header __header)
     (progn
       (if __header (setq _header (car __header)))
       _header)))
  (:position_gps
   (&optional __position_gps)
   (if __position_gps (setq _position_gps __position_gps)) _position_gps)
  (:attitude_angle
   (&optional __attitude_angle)
   (if __attitude_angle (setq _attitude_angle __attitude_angle)) _attitude_angle)
  (:velocity
   (&optional __velocity)
   (if __velocity (setq _velocity __velocity)) _velocity)
  (:angular_velocity
   (&optional __angular_velocity)
   (if __angular_velocity (setq _angular_velocity __angular_velocity)) _angular_velocity)
  (:acceleration
   (&optional __acceleration)
   (if __acceleration (setq _acceleration __acceleration)) _acceleration)
  (:electric_quantity
   (&optional __electric_quantity)
   (if __electric_quantity (setq _electric_quantity __electric_quantity)) _electric_quantity)
  (:state_word
   (&optional __state_word)
   (if __state_word (setq _state_word __state_word)) _state_word)
  (:serialization-length
   ()
   (+
    ;; std_msgs/Header _header
    (send _header :serialization-length)
    ;; float32[3] _position_gps
    (* 4    3)
    ;; float32[3] _attitude_angle
    (* 4    3)
    ;; float32[3] _velocity
    (* 4    3)
    ;; float32[3] _angular_velocity
    (* 4    3)
    ;; float32[3] _acceleration
    (* 4    3)
    ;; float32 _electric_quantity
    4
    ;; int8 _state_word
    1
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; std_msgs/Header _header
       (send _header :serialize s)
     ;; float32[3] _position_gps
     (dotimes (i 3)
       (sys::poke (elt _position_gps i) (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
       )
     ;; float32[3] _attitude_angle
     (dotimes (i 3)
       (sys::poke (elt _attitude_angle i) (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
       )
     ;; float32[3] _velocity
     (dotimes (i 3)
       (sys::poke (elt _velocity i) (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
       )
     ;; float32[3] _angular_velocity
     (dotimes (i 3)
       (sys::poke (elt _angular_velocity i) (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
       )
     ;; float32[3] _acceleration
     (dotimes (i 3)
       (sys::poke (elt _acceleration i) (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
       )
     ;; float32 _electric_quantity
       (sys::poke _electric_quantity (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; int8 _state_word
       (write-byte _state_word s)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; std_msgs/Header _header
     (send _header :deserialize buf ptr-) (incf ptr- (send _header :serialization-length))
   ;; float32[3] _position_gps
   (dotimes (i (length _position_gps))
     (setf (elt _position_gps i) (sys::peek buf ptr- :float)) (incf ptr- 4)
     )
   ;; float32[3] _attitude_angle
   (dotimes (i (length _attitude_angle))
     (setf (elt _attitude_angle i) (sys::peek buf ptr- :float)) (incf ptr- 4)
     )
   ;; float32[3] _velocity
   (dotimes (i (length _velocity))
     (setf (elt _velocity i) (sys::peek buf ptr- :float)) (incf ptr- 4)
     )
   ;; float32[3] _angular_velocity
   (dotimes (i (length _angular_velocity))
     (setf (elt _angular_velocity i) (sys::peek buf ptr- :float)) (incf ptr- 4)
     )
   ;; float32[3] _acceleration
   (dotimes (i (length _acceleration))
     (setf (elt _acceleration i) (sys::peek buf ptr- :float)) (incf ptr- 4)
     )
   ;; float32 _electric_quantity
     (setq _electric_quantity (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; int8 _state_word
     (setq _state_word (sys::peek buf ptr- :char)) (incf ptr- 1)
     (if (> _state_word 127) (setq _state_word (- _state_word 256)))
   ;;
   self)
  )

(setf (get hector_msgs::State29 :md5sum-) "a717d0d4d01f8d9677c78dc8f2b4efb4")
(setf (get hector_msgs::State29 :datatype-) "hector_msgs/State29")
(setf (get hector_msgs::State29 :definition-)
      "# Vehicle state 'x_hat' output from the estimator or from simulator 

Header header

# Original States
# @warning roll, pitch and yaw have always to be valid, the quaternion is optional
float32[3] position_gps		# latitude (lat. deg), longitude (lon. deg), altitude (m)
float32[3] attitude_angle	# Roll angle (rad), Pitch angle (rad), Yaw angle (rad)
float32[3] velocity		# V_north (m/s), V_east (m/s), V_ground (m/s)
float32[3] angular_velocity	# Body frame rollrate (rad/s), Body frame pitchrate (rad/s), Body frame yawrate (rad/s)
float32[3] acceleration		# Body frame x (m/s^2), Body frame y (m/s^2), Body frame z (m/s^2)
float32    electric_quantity	# Quantity of electric charge
int8       state_word		# Control Status Word

================================================================================
MSG: std_msgs/Header
# Standard metadata for higher-level stamped data types.
# This is generally used to communicate timestamped data 
# in a particular coordinate frame.
# 
# sequence ID: consecutively increasing ID 
uint32 seq
#Two-integer timestamp that is expressed as:
# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')
# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')
# time-handling sugar is provided by the client library
time stamp
#Frame this data is associated with
# 0: no frame
# 1: global frame
string frame_id

")



(provide :hector_msgs/State29 "a717d0d4d01f8d9677c78dc8f2b4efb4")


