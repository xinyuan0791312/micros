;; Auto-generated. Do not edit!


(when (boundp 'hector_msgs::Waypoint)
  (if (not (find-package "HECTOR_MSGS"))
    (make-package "HECTOR_MSGS"))
  (shadow 'Waypoint (find-package "HECTOR_MSGS")))
(unless (find-package "HECTOR_MSGS::WAYPOINT")
  (make-package "HECTOR_MSGS::WAYPOINT"))

(in-package "ROS")
;;//! \htmlinclude Waypoint.msg.html


(defclass hector_msgs::Waypoint
  :super ros::object
  :slots (_w _lat _lon _chi_d _chi_valid _Va_d _set_current _clear_wp_list _landing _takeoff ))

(defmethod hector_msgs::Waypoint
  (:init
   (&key
    ((:w __w) (make-array 3 :initial-element 0.0 :element-type :float))
    ((:lat __lat) 0.0)
    ((:lon __lon) 0.0)
    ((:chi_d __chi_d) 0.0)
    ((:chi_valid __chi_valid) nil)
    ((:Va_d __Va_d) 0.0)
    ((:set_current __set_current) nil)
    ((:clear_wp_list __clear_wp_list) nil)
    ((:landing __landing) nil)
    ((:takeoff __takeoff) nil)
    )
   (send-super :init)
   (setq _w __w)
   (setq _lat (float __lat))
   (setq _lon (float __lon))
   (setq _chi_d (float __chi_d))
   (setq _chi_valid __chi_valid)
   (setq _Va_d (float __Va_d))
   (setq _set_current __set_current)
   (setq _clear_wp_list __clear_wp_list)
   (setq _landing __landing)
   (setq _takeoff __takeoff)
   self)
  (:w
   (&optional __w)
   (if __w (setq _w __w)) _w)
  (:lat
   (&optional __lat)
   (if __lat (setq _lat __lat)) _lat)
  (:lon
   (&optional __lon)
   (if __lon (setq _lon __lon)) _lon)
  (:chi_d
   (&optional __chi_d)
   (if __chi_d (setq _chi_d __chi_d)) _chi_d)
  (:chi_valid
   (&optional __chi_valid)
   (if __chi_valid (setq _chi_valid __chi_valid)) _chi_valid)
  (:Va_d
   (&optional __Va_d)
   (if __Va_d (setq _Va_d __Va_d)) _Va_d)
  (:set_current
   (&optional __set_current)
   (if __set_current (setq _set_current __set_current)) _set_current)
  (:clear_wp_list
   (&optional __clear_wp_list)
   (if __clear_wp_list (setq _clear_wp_list __clear_wp_list)) _clear_wp_list)
  (:landing
   (&optional __landing)
   (if __landing (setq _landing __landing)) _landing)
  (:takeoff
   (&optional __takeoff)
   (if __takeoff (setq _takeoff __takeoff)) _takeoff)
  (:serialization-length
   ()
   (+
    ;; float32[3] _w
    (* 4    3)
    ;; float32 _lat
    4
    ;; float32 _lon
    4
    ;; float32 _chi_d
    4
    ;; bool _chi_valid
    1
    ;; float32 _Va_d
    4
    ;; bool _set_current
    1
    ;; bool _clear_wp_list
    1
    ;; bool _landing
    1
    ;; bool _takeoff
    1
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; float32[3] _w
     (dotimes (i 3)
       (sys::poke (elt _w i) (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
       )
     ;; float32 _lat
       (sys::poke _lat (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _lon
       (sys::poke _lon (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _chi_d
       (sys::poke _chi_d (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; bool _chi_valid
       (if _chi_valid (write-byte -1 s) (write-byte 0 s))
     ;; float32 _Va_d
       (sys::poke _Va_d (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; bool _set_current
       (if _set_current (write-byte -1 s) (write-byte 0 s))
     ;; bool _clear_wp_list
       (if _clear_wp_list (write-byte -1 s) (write-byte 0 s))
     ;; bool _landing
       (if _landing (write-byte -1 s) (write-byte 0 s))
     ;; bool _takeoff
       (if _takeoff (write-byte -1 s) (write-byte 0 s))
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; float32[3] _w
   (dotimes (i (length _w))
     (setf (elt _w i) (sys::peek buf ptr- :float)) (incf ptr- 4)
     )
   ;; float32 _lat
     (setq _lat (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _lon
     (setq _lon (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _chi_d
     (setq _chi_d (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; bool _chi_valid
     (setq _chi_valid (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;; float32 _Va_d
     (setq _Va_d (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; bool _set_current
     (setq _set_current (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;; bool _clear_wp_list
     (setq _clear_wp_list (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;; bool _landing
     (setq _landing (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;; bool _takeoff
     (setq _takeoff (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;;
   self)
  )

(setf (get hector_msgs::Waypoint :md5sum-) "4da62ebb0923fd3200acaafec04c8bd7")
(setf (get hector_msgs::Waypoint :datatype-) "hector_msgs/Waypoint")
(setf (get hector_msgs::Waypoint :definition-)
      "# New waypoint, input to path manager

# @warning w and Va_d always have to be valid; the chi_d is optional.
float32[3] w		# Waypoint in local NED (m)

# add by kobe
float32 lat		# latitude
float32 lon		# longtitude
 
float32 chi_d		# Desired course at this waypoint (rad)
bool chi_valid		# Desired course valid (dubin or fillet paths)
float32 Va_d		# Desired airspeed (m/s)
bool set_current	# Sets this waypoint to be executed now! Starts a new list
bool clear_wp_list  # Removes all waypoints and returns to origin.  The rest of
                    # this message will be ignored
bool landing
bool takeoff

")



(provide :hector_msgs/Waypoint "4da62ebb0923fd3200acaafec04c8bd7")


