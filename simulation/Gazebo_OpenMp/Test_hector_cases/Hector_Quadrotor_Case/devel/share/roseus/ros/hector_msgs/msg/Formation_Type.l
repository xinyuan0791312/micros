;; Auto-generated. Do not edit!


(when (boundp 'hector_msgs::Formation_Type)
  (if (not (find-package "HECTOR_MSGS"))
    (make-package "HECTOR_MSGS"))
  (shadow 'Formation_Type (find-package "HECTOR_MSGS")))
(unless (find-package "HECTOR_MSGS::FORMATION_TYPE")
  (make-package "HECTOR_MSGS::FORMATION_TYPE"))

(in-package "ROS")
;;//! \htmlinclude Formation_Type.msg.html
(if (not (find-package "STD_MSGS"))
  (ros::roseus-add-msgs "std_msgs"))


(defclass hector_msgs::Formation_Type
  :super ros::object
  :slots (_header _formation_type ))

(defmethod hector_msgs::Formation_Type
  (:init
   (&key
    ((:header __header) (instance std_msgs::Header :init))
    ((:formation_type __formation_type) 0.0)
    )
   (send-super :init)
   (setq _header __header)
   (setq _formation_type (float __formation_type))
   self)
  (:header
   (&rest __header)
   (if (keywordp (car __header))
       (send* _header __header)
     (progn
       (if __header (setq _header (car __header)))
       _header)))
  (:formation_type
   (&optional __formation_type)
   (if __formation_type (setq _formation_type __formation_type)) _formation_type)
  (:serialization-length
   ()
   (+
    ;; std_msgs/Header _header
    (send _header :serialization-length)
    ;; float32 _formation_type
    4
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; std_msgs/Header _header
       (send _header :serialize s)
     ;; float32 _formation_type
       (sys::poke _formation_type (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; std_msgs/Header _header
     (send _header :deserialize buf ptr-) (incf ptr- (send _header :serialization-length))
   ;; float32 _formation_type
     (setq _formation_type (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;;
   self)
  )

(setf (get hector_msgs::Formation_Type :md5sum-) "9911b76ddfe46429bec037d09a0bb075")
(setf (get hector_msgs::Formation_Type :datatype-) "hector_msgs/Formation_Type")
(setf (get hector_msgs::Formation_Type :definition-)
      "# Vehicle state 'x_hat' output from the estimator or from simulator 

Header header

# Original States
# @warning roll, pitch and yaw have always to be valid, the quaternion is optional
float32 formation_type		    # Airspeed (m/s)

================================================================================
MSG: std_msgs/Header
# Standard metadata for higher-level stamped data types.
# This is generally used to communicate timestamped data 
# in a particular coordinate frame.
# 
# sequence ID: consecutively increasing ID 
uint32 seq
#Two-integer timestamp that is expressed as:
# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')
# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')
# time-handling sugar is provided by the client library
time stamp
#Frame this data is associated with
# 0: no frame
# 1: global frame
string frame_id

")



(provide :hector_msgs/Formation_Type "9911b76ddfe46429bec037d09a0bb075")


