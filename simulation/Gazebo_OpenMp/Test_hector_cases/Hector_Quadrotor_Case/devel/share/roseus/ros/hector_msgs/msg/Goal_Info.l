;; Auto-generated. Do not edit!


(when (boundp 'hector_msgs::Goal_Info)
  (if (not (find-package "HECTOR_MSGS"))
    (make-package "HECTOR_MSGS"))
  (shadow 'Goal_Info (find-package "HECTOR_MSGS")))
(unless (find-package "HECTOR_MSGS::GOAL_INFO")
  (make-package "HECTOR_MSGS::GOAL_INFO"))

(in-package "ROS")
;;//! \htmlinclude Goal_Info.msg.html


(defclass hector_msgs::Goal_Info
  :super ros::object
  :slots (_v_d _xy _ll _action _altitude ))

(defmethod hector_msgs::Goal_Info
  (:init
   (&key
    ((:v_d __v_d) 0.0)
    ((:xy __xy) (make-array 2 :initial-element 0.0 :element-type :float))
    ((:ll __ll) (make-array 2 :initial-element 0.0 :element-type :float))
    ((:action __action) 0)
    ((:altitude __altitude) 0.0)
    )
   (send-super :init)
   (setq _v_d (float __v_d))
   (setq _xy __xy)
   (setq _ll __ll)
   (setq _action (round __action))
   (setq _altitude (float __altitude))
   self)
  (:v_d
   (&optional __v_d)
   (if __v_d (setq _v_d __v_d)) _v_d)
  (:xy
   (&optional __xy)
   (if __xy (setq _xy __xy)) _xy)
  (:ll
   (&optional __ll)
   (if __ll (setq _ll __ll)) _ll)
  (:action
   (&optional __action)
   (if __action (setq _action __action)) _action)
  (:altitude
   (&optional __altitude)
   (if __altitude (setq _altitude __altitude)) _altitude)
  (:serialization-length
   ()
   (+
    ;; float32 _v_d
    4
    ;; float32[2] _xy
    (* 4    2)
    ;; float32[2] _ll
    (* 4    2)
    ;; int32 _action
    4
    ;; float32 _altitude
    4
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; float32 _v_d
       (sys::poke _v_d (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32[2] _xy
     (dotimes (i 2)
       (sys::poke (elt _xy i) (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
       )
     ;; float32[2] _ll
     (dotimes (i 2)
       (sys::poke (elt _ll i) (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
       )
     ;; int32 _action
       (write-long _action s)
     ;; float32 _altitude
       (sys::poke _altitude (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; float32 _v_d
     (setq _v_d (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32[2] _xy
   (dotimes (i (length _xy))
     (setf (elt _xy i) (sys::peek buf ptr- :float)) (incf ptr- 4)
     )
   ;; float32[2] _ll
   (dotimes (i (length _ll))
     (setf (elt _ll i) (sys::peek buf ptr- :float)) (incf ptr- 4)
     )
   ;; int32 _action
     (setq _action (sys::peek buf ptr- :integer)) (incf ptr- 4)
   ;; float32 _altitude
     (setq _altitude (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;;
   self)
  )

(setf (get hector_msgs::Goal_Info :md5sum-) "14ef783638203eefb7fbb5a851185abb")
(setf (get hector_msgs::Goal_Info :datatype-) "hector_msgs/Goal_Info")
(setf (get hector_msgs::Goal_Info :definition-)
      "# Current path goal output from the path manager, add by kobe

float32 v_d		# Desired airspeed (m/s)
float32[2] xy		# x-y-coordinate (m)
float32[2] ll		# latitude-longtitude
int32 action		# expected action move
float32 altitude             # desire altitude (m)


")



(provide :hector_msgs/Goal_Info "14ef783638203eefb7fbb5a851185abb")


