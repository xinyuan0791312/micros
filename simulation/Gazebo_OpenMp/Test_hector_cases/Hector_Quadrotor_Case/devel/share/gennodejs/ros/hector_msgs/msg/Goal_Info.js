// Auto-generated. Do not edit!

// (in-package hector_msgs.msg)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;

//-----------------------------------------------------------

class Goal_Info {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.v_d = null;
      this.xy = null;
      this.ll = null;
      this.action = null;
      this.altitude = null;
    }
    else {
      if (initObj.hasOwnProperty('v_d')) {
        this.v_d = initObj.v_d
      }
      else {
        this.v_d = 0.0;
      }
      if (initObj.hasOwnProperty('xy')) {
        this.xy = initObj.xy
      }
      else {
        this.xy = new Array(2).fill(0);
      }
      if (initObj.hasOwnProperty('ll')) {
        this.ll = initObj.ll
      }
      else {
        this.ll = new Array(2).fill(0);
      }
      if (initObj.hasOwnProperty('action')) {
        this.action = initObj.action
      }
      else {
        this.action = 0;
      }
      if (initObj.hasOwnProperty('altitude')) {
        this.altitude = initObj.altitude
      }
      else {
        this.altitude = 0.0;
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type Goal_Info
    // Serialize message field [v_d]
    bufferOffset = _serializer.float32(obj.v_d, buffer, bufferOffset);
    // Check that the constant length array field [xy] has the right length
    if (obj.xy.length !== 2) {
      throw new Error('Unable to serialize array field xy - length must be 2')
    }
    // Serialize message field [xy]
    bufferOffset = _arraySerializer.float32(obj.xy, buffer, bufferOffset, 2);
    // Check that the constant length array field [ll] has the right length
    if (obj.ll.length !== 2) {
      throw new Error('Unable to serialize array field ll - length must be 2')
    }
    // Serialize message field [ll]
    bufferOffset = _arraySerializer.float32(obj.ll, buffer, bufferOffset, 2);
    // Serialize message field [action]
    bufferOffset = _serializer.int32(obj.action, buffer, bufferOffset);
    // Serialize message field [altitude]
    bufferOffset = _serializer.float32(obj.altitude, buffer, bufferOffset);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type Goal_Info
    let len;
    let data = new Goal_Info(null);
    // Deserialize message field [v_d]
    data.v_d = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [xy]
    data.xy = _arrayDeserializer.float32(buffer, bufferOffset, 2)
    // Deserialize message field [ll]
    data.ll = _arrayDeserializer.float32(buffer, bufferOffset, 2)
    // Deserialize message field [action]
    data.action = _deserializer.int32(buffer, bufferOffset);
    // Deserialize message field [altitude]
    data.altitude = _deserializer.float32(buffer, bufferOffset);
    return data;
  }

  static getMessageSize(object) {
    return 28;
  }

  static datatype() {
    // Returns string type for a message object
    return 'hector_msgs/Goal_Info';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return '14ef783638203eefb7fbb5a851185abb';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    # Current path goal output from the path manager, add by kobe
    
    float32 v_d		# Desired airspeed (m/s)
    float32[2] xy		# x-y-coordinate (m)
    float32[2] ll		# latitude-longtitude
    int32 action		# expected action move
    float32 altitude             # desire altitude (m)
    
    
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new Goal_Info(null);
    if (msg.v_d !== undefined) {
      resolved.v_d = msg.v_d;
    }
    else {
      resolved.v_d = 0.0
    }

    if (msg.xy !== undefined) {
      resolved.xy = msg.xy;
    }
    else {
      resolved.xy = new Array(2).fill(0)
    }

    if (msg.ll !== undefined) {
      resolved.ll = msg.ll;
    }
    else {
      resolved.ll = new Array(2).fill(0)
    }

    if (msg.action !== undefined) {
      resolved.action = msg.action;
    }
    else {
      resolved.action = 0
    }

    if (msg.altitude !== undefined) {
      resolved.altitude = msg.altitude;
    }
    else {
      resolved.altitude = 0.0
    }

    return resolved;
    }
};

module.exports = Goal_Info;
