
"use strict";

let Altimeter = require('./Altimeter.js');
let AttitudeCommand = require('./AttitudeCommand.js');
let Compass = require('./Compass.js');
let ControllerState = require('./ControllerState.js');
let HeadingCommand = require('./HeadingCommand.js');
let HeightCommand = require('./HeightCommand.js');
let MotorCommand = require('./MotorCommand.js');
let MotorPWM = require('./MotorPWM.js');
let MotorStatus = require('./MotorStatus.js');
let PositionXYCommand = require('./PositionXYCommand.js');
let RawImu = require('./RawImu.js');
let RawMagnetic = require('./RawMagnetic.js');
let RawRC = require('./RawRC.js');
let RC = require('./RC.js');
let RuddersCommand = require('./RuddersCommand.js');
let ServoCommand = require('./ServoCommand.js');
let Supply = require('./Supply.js');
let ThrustCommand = require('./ThrustCommand.js');
let VelocityXYCommand = require('./VelocityXYCommand.js');
let VelocityZCommand = require('./VelocityZCommand.js');
let YawrateCommand = require('./YawrateCommand.js');

module.exports = {
  Altimeter: Altimeter,
  AttitudeCommand: AttitudeCommand,
  Compass: Compass,
  ControllerState: ControllerState,
  HeadingCommand: HeadingCommand,
  HeightCommand: HeightCommand,
  MotorCommand: MotorCommand,
  MotorPWM: MotorPWM,
  MotorStatus: MotorStatus,
  PositionXYCommand: PositionXYCommand,
  RawImu: RawImu,
  RawMagnetic: RawMagnetic,
  RawRC: RawRC,
  RC: RC,
  RuddersCommand: RuddersCommand,
  ServoCommand: ServoCommand,
  Supply: Supply,
  ThrustCommand: ThrustCommand,
  VelocityXYCommand: VelocityXYCommand,
  VelocityZCommand: VelocityZCommand,
  YawrateCommand: YawrateCommand,
};
