// Auto-generated. Do not edit!

// (in-package hector_msgs.msg)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;
let std_msgs = _finder('std_msgs');

//-----------------------------------------------------------

class State29 {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.header = null;
      this.position_gps = null;
      this.attitude_angle = null;
      this.velocity = null;
      this.angular_velocity = null;
      this.acceleration = null;
      this.electric_quantity = null;
      this.state_word = null;
    }
    else {
      if (initObj.hasOwnProperty('header')) {
        this.header = initObj.header
      }
      else {
        this.header = new std_msgs.msg.Header();
      }
      if (initObj.hasOwnProperty('position_gps')) {
        this.position_gps = initObj.position_gps
      }
      else {
        this.position_gps = new Array(3).fill(0);
      }
      if (initObj.hasOwnProperty('attitude_angle')) {
        this.attitude_angle = initObj.attitude_angle
      }
      else {
        this.attitude_angle = new Array(3).fill(0);
      }
      if (initObj.hasOwnProperty('velocity')) {
        this.velocity = initObj.velocity
      }
      else {
        this.velocity = new Array(3).fill(0);
      }
      if (initObj.hasOwnProperty('angular_velocity')) {
        this.angular_velocity = initObj.angular_velocity
      }
      else {
        this.angular_velocity = new Array(3).fill(0);
      }
      if (initObj.hasOwnProperty('acceleration')) {
        this.acceleration = initObj.acceleration
      }
      else {
        this.acceleration = new Array(3).fill(0);
      }
      if (initObj.hasOwnProperty('electric_quantity')) {
        this.electric_quantity = initObj.electric_quantity
      }
      else {
        this.electric_quantity = 0.0;
      }
      if (initObj.hasOwnProperty('state_word')) {
        this.state_word = initObj.state_word
      }
      else {
        this.state_word = 0;
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type State29
    // Serialize message field [header]
    bufferOffset = std_msgs.msg.Header.serialize(obj.header, buffer, bufferOffset);
    // Check that the constant length array field [position_gps] has the right length
    if (obj.position_gps.length !== 3) {
      throw new Error('Unable to serialize array field position_gps - length must be 3')
    }
    // Serialize message field [position_gps]
    bufferOffset = _arraySerializer.float32(obj.position_gps, buffer, bufferOffset, 3);
    // Check that the constant length array field [attitude_angle] has the right length
    if (obj.attitude_angle.length !== 3) {
      throw new Error('Unable to serialize array field attitude_angle - length must be 3')
    }
    // Serialize message field [attitude_angle]
    bufferOffset = _arraySerializer.float32(obj.attitude_angle, buffer, bufferOffset, 3);
    // Check that the constant length array field [velocity] has the right length
    if (obj.velocity.length !== 3) {
      throw new Error('Unable to serialize array field velocity - length must be 3')
    }
    // Serialize message field [velocity]
    bufferOffset = _arraySerializer.float32(obj.velocity, buffer, bufferOffset, 3);
    // Check that the constant length array field [angular_velocity] has the right length
    if (obj.angular_velocity.length !== 3) {
      throw new Error('Unable to serialize array field angular_velocity - length must be 3')
    }
    // Serialize message field [angular_velocity]
    bufferOffset = _arraySerializer.float32(obj.angular_velocity, buffer, bufferOffset, 3);
    // Check that the constant length array field [acceleration] has the right length
    if (obj.acceleration.length !== 3) {
      throw new Error('Unable to serialize array field acceleration - length must be 3')
    }
    // Serialize message field [acceleration]
    bufferOffset = _arraySerializer.float32(obj.acceleration, buffer, bufferOffset, 3);
    // Serialize message field [electric_quantity]
    bufferOffset = _serializer.float32(obj.electric_quantity, buffer, bufferOffset);
    // Serialize message field [state_word]
    bufferOffset = _serializer.int8(obj.state_word, buffer, bufferOffset);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type State29
    let len;
    let data = new State29(null);
    // Deserialize message field [header]
    data.header = std_msgs.msg.Header.deserialize(buffer, bufferOffset);
    // Deserialize message field [position_gps]
    data.position_gps = _arrayDeserializer.float32(buffer, bufferOffset, 3)
    // Deserialize message field [attitude_angle]
    data.attitude_angle = _arrayDeserializer.float32(buffer, bufferOffset, 3)
    // Deserialize message field [velocity]
    data.velocity = _arrayDeserializer.float32(buffer, bufferOffset, 3)
    // Deserialize message field [angular_velocity]
    data.angular_velocity = _arrayDeserializer.float32(buffer, bufferOffset, 3)
    // Deserialize message field [acceleration]
    data.acceleration = _arrayDeserializer.float32(buffer, bufferOffset, 3)
    // Deserialize message field [electric_quantity]
    data.electric_quantity = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [state_word]
    data.state_word = _deserializer.int8(buffer, bufferOffset);
    return data;
  }

  static getMessageSize(object) {
    let length = 0;
    length += std_msgs.msg.Header.getMessageSize(object.header);
    return length + 65;
  }

  static datatype() {
    // Returns string type for a message object
    return 'hector_msgs/State29';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return 'a717d0d4d01f8d9677c78dc8f2b4efb4';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    # Vehicle state 'x_hat' output from the estimator or from simulator 
    
    Header header
    
    # Original States
    # @warning roll, pitch and yaw have always to be valid, the quaternion is optional
    float32[3] position_gps		# latitude (lat. deg), longitude (lon. deg), altitude (m)
    float32[3] attitude_angle	# Roll angle (rad), Pitch angle (rad), Yaw angle (rad)
    float32[3] velocity		# V_north (m/s), V_east (m/s), V_ground (m/s)
    float32[3] angular_velocity	# Body frame rollrate (rad/s), Body frame pitchrate (rad/s), Body frame yawrate (rad/s)
    float32[3] acceleration		# Body frame x (m/s^2), Body frame y (m/s^2), Body frame z (m/s^2)
    float32    electric_quantity	# Quantity of electric charge
    int8       state_word		# Control Status Word
    
    ================================================================================
    MSG: std_msgs/Header
    # Standard metadata for higher-level stamped data types.
    # This is generally used to communicate timestamped data 
    # in a particular coordinate frame.
    # 
    # sequence ID: consecutively increasing ID 
    uint32 seq
    #Two-integer timestamp that is expressed as:
    # * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')
    # * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')
    # time-handling sugar is provided by the client library
    time stamp
    #Frame this data is associated with
    # 0: no frame
    # 1: global frame
    string frame_id
    
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new State29(null);
    if (msg.header !== undefined) {
      resolved.header = std_msgs.msg.Header.Resolve(msg.header)
    }
    else {
      resolved.header = new std_msgs.msg.Header()
    }

    if (msg.position_gps !== undefined) {
      resolved.position_gps = msg.position_gps;
    }
    else {
      resolved.position_gps = new Array(3).fill(0)
    }

    if (msg.attitude_angle !== undefined) {
      resolved.attitude_angle = msg.attitude_angle;
    }
    else {
      resolved.attitude_angle = new Array(3).fill(0)
    }

    if (msg.velocity !== undefined) {
      resolved.velocity = msg.velocity;
    }
    else {
      resolved.velocity = new Array(3).fill(0)
    }

    if (msg.angular_velocity !== undefined) {
      resolved.angular_velocity = msg.angular_velocity;
    }
    else {
      resolved.angular_velocity = new Array(3).fill(0)
    }

    if (msg.acceleration !== undefined) {
      resolved.acceleration = msg.acceleration;
    }
    else {
      resolved.acceleration = new Array(3).fill(0)
    }

    if (msg.electric_quantity !== undefined) {
      resolved.electric_quantity = msg.electric_quantity;
    }
    else {
      resolved.electric_quantity = 0.0
    }

    if (msg.state_word !== undefined) {
      resolved.state_word = msg.state_word;
    }
    else {
      resolved.state_word = 0
    }

    return resolved;
    }
};

module.exports = State29;
