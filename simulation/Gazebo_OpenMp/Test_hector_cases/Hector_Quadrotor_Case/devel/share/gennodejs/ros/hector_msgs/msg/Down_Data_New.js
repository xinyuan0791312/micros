// Auto-generated. Do not edit!

// (in-package hector_msgs.msg)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;

//-----------------------------------------------------------

class Down_Data_New {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.status_word = null;
      this.longitude = null;
      this.latitude = null;
      this.altitude = null;
      this.plane_speed = null;
    }
    else {
      if (initObj.hasOwnProperty('status_word')) {
        this.status_word = initObj.status_word
      }
      else {
        this.status_word = 0;
      }
      if (initObj.hasOwnProperty('longitude')) {
        this.longitude = initObj.longitude
      }
      else {
        this.longitude = 0.0;
      }
      if (initObj.hasOwnProperty('latitude')) {
        this.latitude = initObj.latitude
      }
      else {
        this.latitude = 0.0;
      }
      if (initObj.hasOwnProperty('altitude')) {
        this.altitude = initObj.altitude
      }
      else {
        this.altitude = 0.0;
      }
      if (initObj.hasOwnProperty('plane_speed')) {
        this.plane_speed = initObj.plane_speed
      }
      else {
        this.plane_speed = 0.0;
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type Down_Data_New
    // Serialize message field [status_word]
    bufferOffset = _serializer.int32(obj.status_word, buffer, bufferOffset);
    // Serialize message field [longitude]
    bufferOffset = _serializer.float32(obj.longitude, buffer, bufferOffset);
    // Serialize message field [latitude]
    bufferOffset = _serializer.float32(obj.latitude, buffer, bufferOffset);
    // Serialize message field [altitude]
    bufferOffset = _serializer.float32(obj.altitude, buffer, bufferOffset);
    // Serialize message field [plane_speed]
    bufferOffset = _serializer.float32(obj.plane_speed, buffer, bufferOffset);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type Down_Data_New
    let len;
    let data = new Down_Data_New(null);
    // Deserialize message field [status_word]
    data.status_word = _deserializer.int32(buffer, bufferOffset);
    // Deserialize message field [longitude]
    data.longitude = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [latitude]
    data.latitude = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [altitude]
    data.altitude = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [plane_speed]
    data.plane_speed = _deserializer.float32(buffer, bufferOffset);
    return data;
  }

  static getMessageSize(object) {
    return 20;
  }

  static datatype() {
    // Returns string type for a message object
    return 'hector_msgs/Down_Data_New';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return 'a298ef3ba270277bca4819083a5239f4';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    int32 status_word 
    float32 longitude
    float32 latitude
    float32 altitude
    float32 plane_speed    #desired speed
    
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new Down_Data_New(null);
    if (msg.status_word !== undefined) {
      resolved.status_word = msg.status_word;
    }
    else {
      resolved.status_word = 0
    }

    if (msg.longitude !== undefined) {
      resolved.longitude = msg.longitude;
    }
    else {
      resolved.longitude = 0.0
    }

    if (msg.latitude !== undefined) {
      resolved.latitude = msg.latitude;
    }
    else {
      resolved.latitude = 0.0
    }

    if (msg.altitude !== undefined) {
      resolved.altitude = msg.altitude;
    }
    else {
      resolved.altitude = 0.0
    }

    if (msg.plane_speed !== undefined) {
      resolved.plane_speed = msg.plane_speed;
    }
    else {
      resolved.plane_speed = 0.0
    }

    return resolved;
    }
};

module.exports = Down_Data_New;
