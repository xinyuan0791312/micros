// Auto-generated. Do not edit!

// (in-package hector_msgs.msg)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;

//-----------------------------------------------------------

class Up_Data_New {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.status_word = null;
      this.longitude = null;
      this.latitude = null;
      this.altitude = null;
      this.roll_angle = null;
      this.pitch_angle = null;
      this.yaw_angle = null;
      this.angular_rate_x = null;
      this.angular_rate_y = null;
      this.angular_rate_z = null;
      this.north_direction_speed = null;
      this.east_direction_speed = null;
      this.ground_direction_speed = null;
      this.acceleration_x = null;
      this.acceleration_y = null;
      this.acceleration_z = null;
    }
    else {
      if (initObj.hasOwnProperty('status_word')) {
        this.status_word = initObj.status_word
      }
      else {
        this.status_word = 0;
      }
      if (initObj.hasOwnProperty('longitude')) {
        this.longitude = initObj.longitude
      }
      else {
        this.longitude = 0.0;
      }
      if (initObj.hasOwnProperty('latitude')) {
        this.latitude = initObj.latitude
      }
      else {
        this.latitude = 0.0;
      }
      if (initObj.hasOwnProperty('altitude')) {
        this.altitude = initObj.altitude
      }
      else {
        this.altitude = 0.0;
      }
      if (initObj.hasOwnProperty('roll_angle')) {
        this.roll_angle = initObj.roll_angle
      }
      else {
        this.roll_angle = 0.0;
      }
      if (initObj.hasOwnProperty('pitch_angle')) {
        this.pitch_angle = initObj.pitch_angle
      }
      else {
        this.pitch_angle = 0.0;
      }
      if (initObj.hasOwnProperty('yaw_angle')) {
        this.yaw_angle = initObj.yaw_angle
      }
      else {
        this.yaw_angle = 0.0;
      }
      if (initObj.hasOwnProperty('angular_rate_x')) {
        this.angular_rate_x = initObj.angular_rate_x
      }
      else {
        this.angular_rate_x = 0.0;
      }
      if (initObj.hasOwnProperty('angular_rate_y')) {
        this.angular_rate_y = initObj.angular_rate_y
      }
      else {
        this.angular_rate_y = 0.0;
      }
      if (initObj.hasOwnProperty('angular_rate_z')) {
        this.angular_rate_z = initObj.angular_rate_z
      }
      else {
        this.angular_rate_z = 0.0;
      }
      if (initObj.hasOwnProperty('north_direction_speed')) {
        this.north_direction_speed = initObj.north_direction_speed
      }
      else {
        this.north_direction_speed = 0.0;
      }
      if (initObj.hasOwnProperty('east_direction_speed')) {
        this.east_direction_speed = initObj.east_direction_speed
      }
      else {
        this.east_direction_speed = 0.0;
      }
      if (initObj.hasOwnProperty('ground_direction_speed')) {
        this.ground_direction_speed = initObj.ground_direction_speed
      }
      else {
        this.ground_direction_speed = 0.0;
      }
      if (initObj.hasOwnProperty('acceleration_x')) {
        this.acceleration_x = initObj.acceleration_x
      }
      else {
        this.acceleration_x = 0.0;
      }
      if (initObj.hasOwnProperty('acceleration_y')) {
        this.acceleration_y = initObj.acceleration_y
      }
      else {
        this.acceleration_y = 0.0;
      }
      if (initObj.hasOwnProperty('acceleration_z')) {
        this.acceleration_z = initObj.acceleration_z
      }
      else {
        this.acceleration_z = 0.0;
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type Up_Data_New
    // Serialize message field [status_word]
    bufferOffset = _serializer.int32(obj.status_word, buffer, bufferOffset);
    // Serialize message field [longitude]
    bufferOffset = _serializer.float32(obj.longitude, buffer, bufferOffset);
    // Serialize message field [latitude]
    bufferOffset = _serializer.float32(obj.latitude, buffer, bufferOffset);
    // Serialize message field [altitude]
    bufferOffset = _serializer.float32(obj.altitude, buffer, bufferOffset);
    // Serialize message field [roll_angle]
    bufferOffset = _serializer.float32(obj.roll_angle, buffer, bufferOffset);
    // Serialize message field [pitch_angle]
    bufferOffset = _serializer.float32(obj.pitch_angle, buffer, bufferOffset);
    // Serialize message field [yaw_angle]
    bufferOffset = _serializer.float32(obj.yaw_angle, buffer, bufferOffset);
    // Serialize message field [angular_rate_x]
    bufferOffset = _serializer.float32(obj.angular_rate_x, buffer, bufferOffset);
    // Serialize message field [angular_rate_y]
    bufferOffset = _serializer.float32(obj.angular_rate_y, buffer, bufferOffset);
    // Serialize message field [angular_rate_z]
    bufferOffset = _serializer.float32(obj.angular_rate_z, buffer, bufferOffset);
    // Serialize message field [north_direction_speed]
    bufferOffset = _serializer.float32(obj.north_direction_speed, buffer, bufferOffset);
    // Serialize message field [east_direction_speed]
    bufferOffset = _serializer.float32(obj.east_direction_speed, buffer, bufferOffset);
    // Serialize message field [ground_direction_speed]
    bufferOffset = _serializer.float32(obj.ground_direction_speed, buffer, bufferOffset);
    // Serialize message field [acceleration_x]
    bufferOffset = _serializer.float32(obj.acceleration_x, buffer, bufferOffset);
    // Serialize message field [acceleration_y]
    bufferOffset = _serializer.float32(obj.acceleration_y, buffer, bufferOffset);
    // Serialize message field [acceleration_z]
    bufferOffset = _serializer.float32(obj.acceleration_z, buffer, bufferOffset);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type Up_Data_New
    let len;
    let data = new Up_Data_New(null);
    // Deserialize message field [status_word]
    data.status_word = _deserializer.int32(buffer, bufferOffset);
    // Deserialize message field [longitude]
    data.longitude = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [latitude]
    data.latitude = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [altitude]
    data.altitude = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [roll_angle]
    data.roll_angle = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [pitch_angle]
    data.pitch_angle = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [yaw_angle]
    data.yaw_angle = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [angular_rate_x]
    data.angular_rate_x = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [angular_rate_y]
    data.angular_rate_y = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [angular_rate_z]
    data.angular_rate_z = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [north_direction_speed]
    data.north_direction_speed = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [east_direction_speed]
    data.east_direction_speed = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [ground_direction_speed]
    data.ground_direction_speed = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [acceleration_x]
    data.acceleration_x = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [acceleration_y]
    data.acceleration_y = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [acceleration_z]
    data.acceleration_z = _deserializer.float32(buffer, bufferOffset);
    return data;
  }

  static getMessageSize(object) {
    return 64;
  }

  static datatype() {
    // Returns string type for a message object
    return 'hector_msgs/Up_Data_New';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return 'f4d381e3081b932960cb54e2b4ced91c';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    int32 status_word 
    float32 longitude
    float32 latitude  
    float32 altitude                     # (m)
    float32 roll_angle                   #(rad)     
    float32 pitch_angle 
    float32 yaw_angle 
    float32 angular_rate_x               #Body frame rollrate (rad/s)     
    float32 angular_rate_y               #Body frame pitchrate (rad/s)
    float32 angular_rate_z               #Body frame yawrate (rad/s)
    float32 north_direction_speed        #(m/s)
    float32 east_direction_speed 
    float32 ground_direction_speed  
    float32 acceleration_x               #(m/s^2)
    float32 acceleration_y
    float32 acceleration_z
    
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new Up_Data_New(null);
    if (msg.status_word !== undefined) {
      resolved.status_word = msg.status_word;
    }
    else {
      resolved.status_word = 0
    }

    if (msg.longitude !== undefined) {
      resolved.longitude = msg.longitude;
    }
    else {
      resolved.longitude = 0.0
    }

    if (msg.latitude !== undefined) {
      resolved.latitude = msg.latitude;
    }
    else {
      resolved.latitude = 0.0
    }

    if (msg.altitude !== undefined) {
      resolved.altitude = msg.altitude;
    }
    else {
      resolved.altitude = 0.0
    }

    if (msg.roll_angle !== undefined) {
      resolved.roll_angle = msg.roll_angle;
    }
    else {
      resolved.roll_angle = 0.0
    }

    if (msg.pitch_angle !== undefined) {
      resolved.pitch_angle = msg.pitch_angle;
    }
    else {
      resolved.pitch_angle = 0.0
    }

    if (msg.yaw_angle !== undefined) {
      resolved.yaw_angle = msg.yaw_angle;
    }
    else {
      resolved.yaw_angle = 0.0
    }

    if (msg.angular_rate_x !== undefined) {
      resolved.angular_rate_x = msg.angular_rate_x;
    }
    else {
      resolved.angular_rate_x = 0.0
    }

    if (msg.angular_rate_y !== undefined) {
      resolved.angular_rate_y = msg.angular_rate_y;
    }
    else {
      resolved.angular_rate_y = 0.0
    }

    if (msg.angular_rate_z !== undefined) {
      resolved.angular_rate_z = msg.angular_rate_z;
    }
    else {
      resolved.angular_rate_z = 0.0
    }

    if (msg.north_direction_speed !== undefined) {
      resolved.north_direction_speed = msg.north_direction_speed;
    }
    else {
      resolved.north_direction_speed = 0.0
    }

    if (msg.east_direction_speed !== undefined) {
      resolved.east_direction_speed = msg.east_direction_speed;
    }
    else {
      resolved.east_direction_speed = 0.0
    }

    if (msg.ground_direction_speed !== undefined) {
      resolved.ground_direction_speed = msg.ground_direction_speed;
    }
    else {
      resolved.ground_direction_speed = 0.0
    }

    if (msg.acceleration_x !== undefined) {
      resolved.acceleration_x = msg.acceleration_x;
    }
    else {
      resolved.acceleration_x = 0.0
    }

    if (msg.acceleration_y !== undefined) {
      resolved.acceleration_y = msg.acceleration_y;
    }
    else {
      resolved.acceleration_y = 0.0
    }

    if (msg.acceleration_z !== undefined) {
      resolved.acceleration_z = msg.acceleration_z;
    }
    else {
      resolved.acceleration_z = 0.0
    }

    return resolved;
    }
};

module.exports = Up_Data_New;
