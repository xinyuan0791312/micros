#!/usr/bin/env sh
# generated from catkin/python/catkin/environment_cache.py

# based on a snapshot of the environment before and after calling the setup script
# it emulates the modifications of the setup script without recurring computations

# new environment variables

# modified environment variables
export CMAKE_PREFIX_PATH="/media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/devel:$CMAKE_PREFIX_PATH"
export LD_LIBRARY_PATH="/home/wn/catkin_ws/devel/lib:/opt/ros/kinetic/lib:/opt/ros/kinetic/lib/x86_64-linux-gnu:/opt/Fast-RTPS/usr/local//lib:/usr/local/cuda-9.0/lib64"
export PATH="/opt/ros/kinetic/bin:/opt/Fast-RTPS/usr/local//bin:/home/wn/bin:/home/wn/.local/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/snap/bin:/usr/local/cuda-9.0/bin"
export PWD="/media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/build"
export ROSLISP_PACKAGE_DIRECTORIES="/media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/devel/share/common-lisp:$ROSLISP_PACKAGE_DIRECTORIES"
export ROS_PACKAGE_PATH="/media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/src:$ROS_PACKAGE_PATH"