# CMake generated Testfile for 
# Source directory: /media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/src
# Build directory: /media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs(gtest)
subdirs(hector_quadrotor/hector_quadrotor)
subdirs(hector_quadrotor/hector_quadrotor_demo)
subdirs(hector_quadrotor/hector_quadrotor_description)
subdirs(hector_quadrotor/hector_msgs)
subdirs(hector_quadrotor/hector_uav_msgs)
subdirs(hector_quadrotor/hector_quadrotor_model)
subdirs(hector_quadrotor/hector)
subdirs(hector_quadrotor/hector_quadrotor_controller)
subdirs(hector_quadrotor/hector_quadrotor_teleop)
subdirs(hector_quadrotor/hector_quadrotor_gazebo_plugins)
subdirs(hector_quadrotor/hector_quadrotor_pose_estimation)
subdirs(hector_quadrotor/hector_quadrotor_controller_gazebo)
subdirs(hector_quadrotor/hector_quadrotor_gazebo)
