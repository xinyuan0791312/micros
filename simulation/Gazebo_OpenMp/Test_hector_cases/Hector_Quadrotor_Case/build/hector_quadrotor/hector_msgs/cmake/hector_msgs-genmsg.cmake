# generated from genmsg/cmake/pkg-genmsg.cmake.em

message(STATUS "hector_msgs: 10 messages, 0 services")

set(MSG_I_FLAGS "-Ihector_msgs:/media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/src/hector_quadrotor/hector_msgs/msg;-Istd_msgs:/opt/ros/kinetic/share/std_msgs/cmake/../msg")

# Find all generators
find_package(gencpp REQUIRED)
find_package(geneus REQUIRED)
find_package(genlisp REQUIRED)
find_package(gennodejs REQUIRED)
find_package(genpy REQUIRED)

add_custom_target(hector_msgs_generate_messages ALL)

# verify that message/service dependencies have not changed since configure



get_filename_component(_filename "/media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/src/hector_quadrotor/hector_msgs/msg/Controller_Commands.msg" NAME_WE)
add_custom_target(_hector_msgs_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "hector_msgs" "/media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/src/hector_quadrotor/hector_msgs/msg/Controller_Commands.msg" ""
)

get_filename_component(_filename "/media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/src/hector_quadrotor/hector_msgs/msg/State29.msg" NAME_WE)
add_custom_target(_hector_msgs_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "hector_msgs" "/media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/src/hector_quadrotor/hector_msgs/msg/State29.msg" "std_msgs/Header"
)

get_filename_component(_filename "/media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/src/hector_quadrotor/hector_msgs/msg/Current_Path.msg" NAME_WE)
add_custom_target(_hector_msgs_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "hector_msgs" "/media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/src/hector_quadrotor/hector_msgs/msg/Current_Path.msg" ""
)

get_filename_component(_filename "/media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/src/hector_quadrotor/hector_msgs/msg/Up_Data_New.msg" NAME_WE)
add_custom_target(_hector_msgs_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "hector_msgs" "/media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/src/hector_quadrotor/hector_msgs/msg/Up_Data_New.msg" ""
)

get_filename_component(_filename "/media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/src/hector_quadrotor/hector_msgs/msg/Goal_Info.msg" NAME_WE)
add_custom_target(_hector_msgs_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "hector_msgs" "/media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/src/hector_quadrotor/hector_msgs/msg/Goal_Info.msg" ""
)

get_filename_component(_filename "/media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/src/hector_quadrotor/hector_msgs/msg/State.msg" NAME_WE)
add_custom_target(_hector_msgs_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "hector_msgs" "/media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/src/hector_quadrotor/hector_msgs/msg/State.msg" "std_msgs/Header"
)

get_filename_component(_filename "/media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/src/hector_quadrotor/hector_msgs/msg/Down_Data_New.msg" NAME_WE)
add_custom_target(_hector_msgs_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "hector_msgs" "/media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/src/hector_quadrotor/hector_msgs/msg/Down_Data_New.msg" ""
)

get_filename_component(_filename "/media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/src/hector_quadrotor/hector_msgs/msg/Controller_Internals.msg" NAME_WE)
add_custom_target(_hector_msgs_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "hector_msgs" "/media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/src/hector_quadrotor/hector_msgs/msg/Controller_Internals.msg" ""
)

get_filename_component(_filename "/media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/src/hector_quadrotor/hector_msgs/msg/Formation_Type.msg" NAME_WE)
add_custom_target(_hector_msgs_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "hector_msgs" "/media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/src/hector_quadrotor/hector_msgs/msg/Formation_Type.msg" "std_msgs/Header"
)

get_filename_component(_filename "/media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/src/hector_quadrotor/hector_msgs/msg/Waypoint.msg" NAME_WE)
add_custom_target(_hector_msgs_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "hector_msgs" "/media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/src/hector_quadrotor/hector_msgs/msg/Waypoint.msg" ""
)

#
#  langs = gencpp;geneus;genlisp;gennodejs;genpy
#

### Section generating for lang: gencpp
### Generating Messages
_generate_msg_cpp(hector_msgs
  "/media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/src/hector_quadrotor/hector_msgs/msg/Waypoint.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/hector_msgs
)
_generate_msg_cpp(hector_msgs
  "/media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/src/hector_quadrotor/hector_msgs/msg/Controller_Commands.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/hector_msgs
)
_generate_msg_cpp(hector_msgs
  "/media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/src/hector_quadrotor/hector_msgs/msg/Current_Path.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/hector_msgs
)
_generate_msg_cpp(hector_msgs
  "/media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/src/hector_quadrotor/hector_msgs/msg/Controller_Internals.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/hector_msgs
)
_generate_msg_cpp(hector_msgs
  "/media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/src/hector_quadrotor/hector_msgs/msg/Goal_Info.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/hector_msgs
)
_generate_msg_cpp(hector_msgs
  "/media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/src/hector_quadrotor/hector_msgs/msg/Up_Data_New.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/hector_msgs
)
_generate_msg_cpp(hector_msgs
  "/media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/src/hector_quadrotor/hector_msgs/msg/Down_Data_New.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/hector_msgs
)
_generate_msg_cpp(hector_msgs
  "/media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/src/hector_quadrotor/hector_msgs/msg/State29.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/hector_msgs
)
_generate_msg_cpp(hector_msgs
  "/media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/src/hector_quadrotor/hector_msgs/msg/Formation_Type.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/hector_msgs
)
_generate_msg_cpp(hector_msgs
  "/media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/src/hector_quadrotor/hector_msgs/msg/State.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/hector_msgs
)

### Generating Services

### Generating Module File
_generate_module_cpp(hector_msgs
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/hector_msgs
  "${ALL_GEN_OUTPUT_FILES_cpp}"
)

add_custom_target(hector_msgs_generate_messages_cpp
  DEPENDS ${ALL_GEN_OUTPUT_FILES_cpp}
)
add_dependencies(hector_msgs_generate_messages hector_msgs_generate_messages_cpp)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/src/hector_quadrotor/hector_msgs/msg/Controller_Commands.msg" NAME_WE)
add_dependencies(hector_msgs_generate_messages_cpp _hector_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/src/hector_quadrotor/hector_msgs/msg/State29.msg" NAME_WE)
add_dependencies(hector_msgs_generate_messages_cpp _hector_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/src/hector_quadrotor/hector_msgs/msg/Current_Path.msg" NAME_WE)
add_dependencies(hector_msgs_generate_messages_cpp _hector_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/src/hector_quadrotor/hector_msgs/msg/Up_Data_New.msg" NAME_WE)
add_dependencies(hector_msgs_generate_messages_cpp _hector_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/src/hector_quadrotor/hector_msgs/msg/Goal_Info.msg" NAME_WE)
add_dependencies(hector_msgs_generate_messages_cpp _hector_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/src/hector_quadrotor/hector_msgs/msg/State.msg" NAME_WE)
add_dependencies(hector_msgs_generate_messages_cpp _hector_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/src/hector_quadrotor/hector_msgs/msg/Down_Data_New.msg" NAME_WE)
add_dependencies(hector_msgs_generate_messages_cpp _hector_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/src/hector_quadrotor/hector_msgs/msg/Controller_Internals.msg" NAME_WE)
add_dependencies(hector_msgs_generate_messages_cpp _hector_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/src/hector_quadrotor/hector_msgs/msg/Formation_Type.msg" NAME_WE)
add_dependencies(hector_msgs_generate_messages_cpp _hector_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/src/hector_quadrotor/hector_msgs/msg/Waypoint.msg" NAME_WE)
add_dependencies(hector_msgs_generate_messages_cpp _hector_msgs_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(hector_msgs_gencpp)
add_dependencies(hector_msgs_gencpp hector_msgs_generate_messages_cpp)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS hector_msgs_generate_messages_cpp)

### Section generating for lang: geneus
### Generating Messages
_generate_msg_eus(hector_msgs
  "/media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/src/hector_quadrotor/hector_msgs/msg/Waypoint.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/hector_msgs
)
_generate_msg_eus(hector_msgs
  "/media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/src/hector_quadrotor/hector_msgs/msg/Controller_Commands.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/hector_msgs
)
_generate_msg_eus(hector_msgs
  "/media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/src/hector_quadrotor/hector_msgs/msg/Current_Path.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/hector_msgs
)
_generate_msg_eus(hector_msgs
  "/media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/src/hector_quadrotor/hector_msgs/msg/Controller_Internals.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/hector_msgs
)
_generate_msg_eus(hector_msgs
  "/media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/src/hector_quadrotor/hector_msgs/msg/Goal_Info.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/hector_msgs
)
_generate_msg_eus(hector_msgs
  "/media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/src/hector_quadrotor/hector_msgs/msg/Up_Data_New.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/hector_msgs
)
_generate_msg_eus(hector_msgs
  "/media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/src/hector_quadrotor/hector_msgs/msg/Down_Data_New.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/hector_msgs
)
_generate_msg_eus(hector_msgs
  "/media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/src/hector_quadrotor/hector_msgs/msg/State29.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/hector_msgs
)
_generate_msg_eus(hector_msgs
  "/media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/src/hector_quadrotor/hector_msgs/msg/Formation_Type.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/hector_msgs
)
_generate_msg_eus(hector_msgs
  "/media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/src/hector_quadrotor/hector_msgs/msg/State.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/hector_msgs
)

### Generating Services

### Generating Module File
_generate_module_eus(hector_msgs
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/hector_msgs
  "${ALL_GEN_OUTPUT_FILES_eus}"
)

add_custom_target(hector_msgs_generate_messages_eus
  DEPENDS ${ALL_GEN_OUTPUT_FILES_eus}
)
add_dependencies(hector_msgs_generate_messages hector_msgs_generate_messages_eus)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/src/hector_quadrotor/hector_msgs/msg/Controller_Commands.msg" NAME_WE)
add_dependencies(hector_msgs_generate_messages_eus _hector_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/src/hector_quadrotor/hector_msgs/msg/State29.msg" NAME_WE)
add_dependencies(hector_msgs_generate_messages_eus _hector_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/src/hector_quadrotor/hector_msgs/msg/Current_Path.msg" NAME_WE)
add_dependencies(hector_msgs_generate_messages_eus _hector_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/src/hector_quadrotor/hector_msgs/msg/Up_Data_New.msg" NAME_WE)
add_dependencies(hector_msgs_generate_messages_eus _hector_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/src/hector_quadrotor/hector_msgs/msg/Goal_Info.msg" NAME_WE)
add_dependencies(hector_msgs_generate_messages_eus _hector_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/src/hector_quadrotor/hector_msgs/msg/State.msg" NAME_WE)
add_dependencies(hector_msgs_generate_messages_eus _hector_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/src/hector_quadrotor/hector_msgs/msg/Down_Data_New.msg" NAME_WE)
add_dependencies(hector_msgs_generate_messages_eus _hector_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/src/hector_quadrotor/hector_msgs/msg/Controller_Internals.msg" NAME_WE)
add_dependencies(hector_msgs_generate_messages_eus _hector_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/src/hector_quadrotor/hector_msgs/msg/Formation_Type.msg" NAME_WE)
add_dependencies(hector_msgs_generate_messages_eus _hector_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/src/hector_quadrotor/hector_msgs/msg/Waypoint.msg" NAME_WE)
add_dependencies(hector_msgs_generate_messages_eus _hector_msgs_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(hector_msgs_geneus)
add_dependencies(hector_msgs_geneus hector_msgs_generate_messages_eus)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS hector_msgs_generate_messages_eus)

### Section generating for lang: genlisp
### Generating Messages
_generate_msg_lisp(hector_msgs
  "/media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/src/hector_quadrotor/hector_msgs/msg/Waypoint.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/hector_msgs
)
_generate_msg_lisp(hector_msgs
  "/media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/src/hector_quadrotor/hector_msgs/msg/Controller_Commands.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/hector_msgs
)
_generate_msg_lisp(hector_msgs
  "/media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/src/hector_quadrotor/hector_msgs/msg/Current_Path.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/hector_msgs
)
_generate_msg_lisp(hector_msgs
  "/media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/src/hector_quadrotor/hector_msgs/msg/Controller_Internals.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/hector_msgs
)
_generate_msg_lisp(hector_msgs
  "/media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/src/hector_quadrotor/hector_msgs/msg/Goal_Info.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/hector_msgs
)
_generate_msg_lisp(hector_msgs
  "/media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/src/hector_quadrotor/hector_msgs/msg/Up_Data_New.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/hector_msgs
)
_generate_msg_lisp(hector_msgs
  "/media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/src/hector_quadrotor/hector_msgs/msg/Down_Data_New.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/hector_msgs
)
_generate_msg_lisp(hector_msgs
  "/media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/src/hector_quadrotor/hector_msgs/msg/State29.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/hector_msgs
)
_generate_msg_lisp(hector_msgs
  "/media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/src/hector_quadrotor/hector_msgs/msg/Formation_Type.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/hector_msgs
)
_generate_msg_lisp(hector_msgs
  "/media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/src/hector_quadrotor/hector_msgs/msg/State.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/hector_msgs
)

### Generating Services

### Generating Module File
_generate_module_lisp(hector_msgs
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/hector_msgs
  "${ALL_GEN_OUTPUT_FILES_lisp}"
)

add_custom_target(hector_msgs_generate_messages_lisp
  DEPENDS ${ALL_GEN_OUTPUT_FILES_lisp}
)
add_dependencies(hector_msgs_generate_messages hector_msgs_generate_messages_lisp)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/src/hector_quadrotor/hector_msgs/msg/Controller_Commands.msg" NAME_WE)
add_dependencies(hector_msgs_generate_messages_lisp _hector_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/src/hector_quadrotor/hector_msgs/msg/State29.msg" NAME_WE)
add_dependencies(hector_msgs_generate_messages_lisp _hector_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/src/hector_quadrotor/hector_msgs/msg/Current_Path.msg" NAME_WE)
add_dependencies(hector_msgs_generate_messages_lisp _hector_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/src/hector_quadrotor/hector_msgs/msg/Up_Data_New.msg" NAME_WE)
add_dependencies(hector_msgs_generate_messages_lisp _hector_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/src/hector_quadrotor/hector_msgs/msg/Goal_Info.msg" NAME_WE)
add_dependencies(hector_msgs_generate_messages_lisp _hector_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/src/hector_quadrotor/hector_msgs/msg/State.msg" NAME_WE)
add_dependencies(hector_msgs_generate_messages_lisp _hector_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/src/hector_quadrotor/hector_msgs/msg/Down_Data_New.msg" NAME_WE)
add_dependencies(hector_msgs_generate_messages_lisp _hector_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/src/hector_quadrotor/hector_msgs/msg/Controller_Internals.msg" NAME_WE)
add_dependencies(hector_msgs_generate_messages_lisp _hector_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/src/hector_quadrotor/hector_msgs/msg/Formation_Type.msg" NAME_WE)
add_dependencies(hector_msgs_generate_messages_lisp _hector_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/src/hector_quadrotor/hector_msgs/msg/Waypoint.msg" NAME_WE)
add_dependencies(hector_msgs_generate_messages_lisp _hector_msgs_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(hector_msgs_genlisp)
add_dependencies(hector_msgs_genlisp hector_msgs_generate_messages_lisp)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS hector_msgs_generate_messages_lisp)

### Section generating for lang: gennodejs
### Generating Messages
_generate_msg_nodejs(hector_msgs
  "/media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/src/hector_quadrotor/hector_msgs/msg/Waypoint.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/hector_msgs
)
_generate_msg_nodejs(hector_msgs
  "/media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/src/hector_quadrotor/hector_msgs/msg/Controller_Commands.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/hector_msgs
)
_generate_msg_nodejs(hector_msgs
  "/media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/src/hector_quadrotor/hector_msgs/msg/Current_Path.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/hector_msgs
)
_generate_msg_nodejs(hector_msgs
  "/media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/src/hector_quadrotor/hector_msgs/msg/Controller_Internals.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/hector_msgs
)
_generate_msg_nodejs(hector_msgs
  "/media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/src/hector_quadrotor/hector_msgs/msg/Goal_Info.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/hector_msgs
)
_generate_msg_nodejs(hector_msgs
  "/media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/src/hector_quadrotor/hector_msgs/msg/Up_Data_New.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/hector_msgs
)
_generate_msg_nodejs(hector_msgs
  "/media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/src/hector_quadrotor/hector_msgs/msg/Down_Data_New.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/hector_msgs
)
_generate_msg_nodejs(hector_msgs
  "/media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/src/hector_quadrotor/hector_msgs/msg/State29.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/hector_msgs
)
_generate_msg_nodejs(hector_msgs
  "/media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/src/hector_quadrotor/hector_msgs/msg/Formation_Type.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/hector_msgs
)
_generate_msg_nodejs(hector_msgs
  "/media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/src/hector_quadrotor/hector_msgs/msg/State.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/hector_msgs
)

### Generating Services

### Generating Module File
_generate_module_nodejs(hector_msgs
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/hector_msgs
  "${ALL_GEN_OUTPUT_FILES_nodejs}"
)

add_custom_target(hector_msgs_generate_messages_nodejs
  DEPENDS ${ALL_GEN_OUTPUT_FILES_nodejs}
)
add_dependencies(hector_msgs_generate_messages hector_msgs_generate_messages_nodejs)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/src/hector_quadrotor/hector_msgs/msg/Controller_Commands.msg" NAME_WE)
add_dependencies(hector_msgs_generate_messages_nodejs _hector_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/src/hector_quadrotor/hector_msgs/msg/State29.msg" NAME_WE)
add_dependencies(hector_msgs_generate_messages_nodejs _hector_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/src/hector_quadrotor/hector_msgs/msg/Current_Path.msg" NAME_WE)
add_dependencies(hector_msgs_generate_messages_nodejs _hector_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/src/hector_quadrotor/hector_msgs/msg/Up_Data_New.msg" NAME_WE)
add_dependencies(hector_msgs_generate_messages_nodejs _hector_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/src/hector_quadrotor/hector_msgs/msg/Goal_Info.msg" NAME_WE)
add_dependencies(hector_msgs_generate_messages_nodejs _hector_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/src/hector_quadrotor/hector_msgs/msg/State.msg" NAME_WE)
add_dependencies(hector_msgs_generate_messages_nodejs _hector_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/src/hector_quadrotor/hector_msgs/msg/Down_Data_New.msg" NAME_WE)
add_dependencies(hector_msgs_generate_messages_nodejs _hector_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/src/hector_quadrotor/hector_msgs/msg/Controller_Internals.msg" NAME_WE)
add_dependencies(hector_msgs_generate_messages_nodejs _hector_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/src/hector_quadrotor/hector_msgs/msg/Formation_Type.msg" NAME_WE)
add_dependencies(hector_msgs_generate_messages_nodejs _hector_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/src/hector_quadrotor/hector_msgs/msg/Waypoint.msg" NAME_WE)
add_dependencies(hector_msgs_generate_messages_nodejs _hector_msgs_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(hector_msgs_gennodejs)
add_dependencies(hector_msgs_gennodejs hector_msgs_generate_messages_nodejs)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS hector_msgs_generate_messages_nodejs)

### Section generating for lang: genpy
### Generating Messages
_generate_msg_py(hector_msgs
  "/media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/src/hector_quadrotor/hector_msgs/msg/Waypoint.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/hector_msgs
)
_generate_msg_py(hector_msgs
  "/media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/src/hector_quadrotor/hector_msgs/msg/Controller_Commands.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/hector_msgs
)
_generate_msg_py(hector_msgs
  "/media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/src/hector_quadrotor/hector_msgs/msg/Current_Path.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/hector_msgs
)
_generate_msg_py(hector_msgs
  "/media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/src/hector_quadrotor/hector_msgs/msg/Controller_Internals.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/hector_msgs
)
_generate_msg_py(hector_msgs
  "/media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/src/hector_quadrotor/hector_msgs/msg/Goal_Info.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/hector_msgs
)
_generate_msg_py(hector_msgs
  "/media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/src/hector_quadrotor/hector_msgs/msg/Up_Data_New.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/hector_msgs
)
_generate_msg_py(hector_msgs
  "/media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/src/hector_quadrotor/hector_msgs/msg/Down_Data_New.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/hector_msgs
)
_generate_msg_py(hector_msgs
  "/media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/src/hector_quadrotor/hector_msgs/msg/State29.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/hector_msgs
)
_generate_msg_py(hector_msgs
  "/media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/src/hector_quadrotor/hector_msgs/msg/Formation_Type.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/hector_msgs
)
_generate_msg_py(hector_msgs
  "/media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/src/hector_quadrotor/hector_msgs/msg/State.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/hector_msgs
)

### Generating Services

### Generating Module File
_generate_module_py(hector_msgs
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/hector_msgs
  "${ALL_GEN_OUTPUT_FILES_py}"
)

add_custom_target(hector_msgs_generate_messages_py
  DEPENDS ${ALL_GEN_OUTPUT_FILES_py}
)
add_dependencies(hector_msgs_generate_messages hector_msgs_generate_messages_py)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/src/hector_quadrotor/hector_msgs/msg/Controller_Commands.msg" NAME_WE)
add_dependencies(hector_msgs_generate_messages_py _hector_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/src/hector_quadrotor/hector_msgs/msg/State29.msg" NAME_WE)
add_dependencies(hector_msgs_generate_messages_py _hector_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/src/hector_quadrotor/hector_msgs/msg/Current_Path.msg" NAME_WE)
add_dependencies(hector_msgs_generate_messages_py _hector_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/src/hector_quadrotor/hector_msgs/msg/Up_Data_New.msg" NAME_WE)
add_dependencies(hector_msgs_generate_messages_py _hector_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/src/hector_quadrotor/hector_msgs/msg/Goal_Info.msg" NAME_WE)
add_dependencies(hector_msgs_generate_messages_py _hector_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/src/hector_quadrotor/hector_msgs/msg/State.msg" NAME_WE)
add_dependencies(hector_msgs_generate_messages_py _hector_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/src/hector_quadrotor/hector_msgs/msg/Down_Data_New.msg" NAME_WE)
add_dependencies(hector_msgs_generate_messages_py _hector_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/src/hector_quadrotor/hector_msgs/msg/Controller_Internals.msg" NAME_WE)
add_dependencies(hector_msgs_generate_messages_py _hector_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/src/hector_quadrotor/hector_msgs/msg/Formation_Type.msg" NAME_WE)
add_dependencies(hector_msgs_generate_messages_py _hector_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/src/hector_quadrotor/hector_msgs/msg/Waypoint.msg" NAME_WE)
add_dependencies(hector_msgs_generate_messages_py _hector_msgs_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(hector_msgs_genpy)
add_dependencies(hector_msgs_genpy hector_msgs_generate_messages_py)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS hector_msgs_generate_messages_py)



if(gencpp_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/hector_msgs)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/hector_msgs
    DESTINATION ${gencpp_INSTALL_DIR}
  )
endif()
if(TARGET std_msgs_generate_messages_cpp)
  add_dependencies(hector_msgs_generate_messages_cpp std_msgs_generate_messages_cpp)
endif()

if(geneus_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/hector_msgs)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/hector_msgs
    DESTINATION ${geneus_INSTALL_DIR}
  )
endif()
if(TARGET std_msgs_generate_messages_eus)
  add_dependencies(hector_msgs_generate_messages_eus std_msgs_generate_messages_eus)
endif()

if(genlisp_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/hector_msgs)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/hector_msgs
    DESTINATION ${genlisp_INSTALL_DIR}
  )
endif()
if(TARGET std_msgs_generate_messages_lisp)
  add_dependencies(hector_msgs_generate_messages_lisp std_msgs_generate_messages_lisp)
endif()

if(gennodejs_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/hector_msgs)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/hector_msgs
    DESTINATION ${gennodejs_INSTALL_DIR}
  )
endif()
if(TARGET std_msgs_generate_messages_nodejs)
  add_dependencies(hector_msgs_generate_messages_nodejs std_msgs_generate_messages_nodejs)
endif()

if(genpy_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/hector_msgs)
  install(CODE "execute_process(COMMAND \"/usr/bin/python\" -m compileall \"${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/hector_msgs\")")
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/hector_msgs
    DESTINATION ${genpy_INSTALL_DIR}
  )
endif()
if(TARGET std_msgs_generate_messages_py)
  add_dependencies(hector_msgs_generate_messages_py std_msgs_generate_messages_py)
endif()
