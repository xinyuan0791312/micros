set(_CATKIN_CURRENT_PACKAGE "hector")
set(hector_VERSION "0.0.0")
set(hector_MAINTAINER "Gary Ellingson <gary.ellingson@byu.edu>")
set(hector_PACKAGE_FORMAT "1")
set(hector_BUILD_DEPENDS "cmake_modules" "dynamic_reconfigure" "rosflight_msgs" "hector_msgs" "roscpp" "rospy" "sensor_msgs" "geometry_msgs")
set(hector_BUILD_EXPORT_DEPENDS "dynamic_reconfigure" "rosflight_msgs" "hector_msgs" "roscpp" "rospy" "sensor_msgs" "geometry_msgs")
set(hector_BUILDTOOL_DEPENDS "catkin")
set(hector_BUILDTOOL_EXPORT_DEPENDS )
set(hector_EXEC_DEPENDS "dynamic_reconfigure" "rosflight_msgs" "hector_msgs" "roscpp" "rospy" "sensor_msgs" "geometry_msgs")
set(hector_RUN_DEPENDS "dynamic_reconfigure" "rosflight_msgs" "hector_msgs" "roscpp" "rospy" "sensor_msgs" "geometry_msgs")
set(hector_TEST_DEPENDS )
set(hector_DOC_DEPENDS )
set(hector_URL_WEBSITE "")
set(hector_URL_BUGTRACKER "")
set(hector_URL_REPOSITORY "")
set(hector_DEPRECATED "")