# Install script for directory: /media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/src/hector_quadrotor/hector_uav_msgs

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/install")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "1")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/hector_uav_msgs/msg" TYPE FILE FILES
    "/media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/src/hector_quadrotor/hector_uav_msgs/msg/Altimeter.msg"
    "/media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/src/hector_quadrotor/hector_uav_msgs/msg/AttitudeCommand.msg"
    "/media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/src/hector_quadrotor/hector_uav_msgs/msg/Compass.msg"
    "/media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/src/hector_quadrotor/hector_uav_msgs/msg/ControllerState.msg"
    "/media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/src/hector_quadrotor/hector_uav_msgs/msg/HeadingCommand.msg"
    "/media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/src/hector_quadrotor/hector_uav_msgs/msg/HeightCommand.msg"
    "/media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/src/hector_quadrotor/hector_uav_msgs/msg/MotorCommand.msg"
    "/media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/src/hector_quadrotor/hector_uav_msgs/msg/MotorPWM.msg"
    "/media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/src/hector_quadrotor/hector_uav_msgs/msg/MotorStatus.msg"
    "/media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/src/hector_quadrotor/hector_uav_msgs/msg/PositionXYCommand.msg"
    "/media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/src/hector_quadrotor/hector_uav_msgs/msg/RawImu.msg"
    "/media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/src/hector_quadrotor/hector_uav_msgs/msg/RawMagnetic.msg"
    "/media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/src/hector_quadrotor/hector_uav_msgs/msg/RawRC.msg"
    "/media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/src/hector_quadrotor/hector_uav_msgs/msg/RC.msg"
    "/media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/src/hector_quadrotor/hector_uav_msgs/msg/RuddersCommand.msg"
    "/media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/src/hector_quadrotor/hector_uav_msgs/msg/ServoCommand.msg"
    "/media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/src/hector_quadrotor/hector_uav_msgs/msg/Supply.msg"
    "/media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/src/hector_quadrotor/hector_uav_msgs/msg/ThrustCommand.msg"
    "/media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/src/hector_quadrotor/hector_uav_msgs/msg/VelocityXYCommand.msg"
    "/media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/src/hector_quadrotor/hector_uav_msgs/msg/VelocityZCommand.msg"
    "/media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/src/hector_quadrotor/hector_uav_msgs/msg/YawrateCommand.msg"
    )
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/hector_uav_msgs/cmake" TYPE FILE FILES "/media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/build/hector_quadrotor/hector_uav_msgs/catkin_generated/installspace/hector_uav_msgs-msg-paths.cmake")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include" TYPE DIRECTORY FILES "/media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/devel/include/hector_uav_msgs")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/roseus/ros" TYPE DIRECTORY FILES "/media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/devel/share/roseus/ros/hector_uav_msgs")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/common-lisp/ros" TYPE DIRECTORY FILES "/media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/devel/share/common-lisp/ros/hector_uav_msgs")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/gennodejs/ros" TYPE DIRECTORY FILES "/media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/devel/share/gennodejs/ros/hector_uav_msgs")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  execute_process(COMMAND "/usr/bin/python" -m compileall "/media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/devel/lib/python2.7/dist-packages/hector_uav_msgs")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/python2.7/dist-packages" TYPE DIRECTORY FILES "/media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/devel/lib/python2.7/dist-packages/hector_uav_msgs")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/pkgconfig" TYPE FILE FILES "/media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/build/hector_quadrotor/hector_uav_msgs/catkin_generated/installspace/hector_uav_msgs.pc")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/hector_uav_msgs/cmake" TYPE FILE FILES "/media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/build/hector_quadrotor/hector_uav_msgs/catkin_generated/installspace/hector_uav_msgs-msg-extras.cmake")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/hector_uav_msgs/cmake" TYPE FILE FILES
    "/media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/build/hector_quadrotor/hector_uav_msgs/catkin_generated/installspace/hector_uav_msgsConfig.cmake"
    "/media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/build/hector_quadrotor/hector_uav_msgs/catkin_generated/installspace/hector_uav_msgsConfig-version.cmake"
    )
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/hector_uav_msgs" TYPE FILE FILES "/media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/src/hector_quadrotor/hector_uav_msgs/package.xml")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/hector_uav_msgs" TYPE DIRECTORY FILES "/media/wn/新加卷/micROS_20191226/20191226/micROS-V1.0-Beta2/src/micros/simulation/Gazebo_OpenMp/Test_hector_cases/Hector_Quadrotor_Case/src/hector_quadrotor/hector_uav_msgs/include/hector_uav_msgs/" FILES_MATCHING REGEX "/[^/]*\\.h$" REGEX "/\\.svn$" EXCLUDE)
endif()

