#! /bin/sh

cd src/micros/behavior_management/decide

rm -rf plugins
tar -zxvf plugins.tar.gz
cd plugins

rm -rf cclvf_standofftracking_plugin/
rm -rf follower_lgvf_plugin/
rm -rf follower_navigate_plugin/
rm -rf gather_plugin/
rm -rf gohome_swarm_quadrotor_plugin/
rm -rf land_quadrotor_plugin/
rm -rf leader_lgvf_plugin/
rm -rf leader_navigate_plugin/
rm -rf ready_plugin/
rm -rf rrt_path_planner_plugin/
rm -rf stick_tracking_plugin/
rm -rf takeoff_swarm_quadrotor_plugin/

cd ../../observe
rm -rf plugins
rm -rf cv_bridge
rm -rf observe_base
rm -rf observe_models
rm -rf observe_msgs
tar -zxvf cv_bridge.tar.gz
tar -zxvf observe_msgs.tar.gz
tar -zxvf plugins.tar.gz
cd plugins
rm -rf detector_wcs_plugin/
rm -rf detect_tracker_wcs_plugin/
cd ../../../simulation
tar -zxvf Gazebo_OpenMp.tar.gz
cd ../resource_management
rm -rf cognition
tar -zxvf physics.tar.gz
cd physics
sudo apt-get install libpcap0.8-dev
sudo apt-get install libv4l-0 libv4l-dev
cd ../../../..
catkin_make -j1
