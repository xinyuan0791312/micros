/* ==================================================================
* Copyright (c) 2018, micROS Group, TAIIC, NIIDT & HPCL.
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions
* are met:
*
* 1. Redistributions of source code must retain the above copyright
* notice, this list of conditions and the following disclaimer.
* 2. Redistributions in binary form must reproduce the above copyright
* notice, this list of conditions and the following disclaimer in the
* documentation and/or other materials provided with the
* distribution.
* 3. All advertising materials mentioning features or use of this software
* must display the following acknowledgement:
* This product includes software developed by the micROS Group. and
* its contributors.
* 4. Neither the name of the Group nor the names of its contributors may
* be used to endorse or promote products derived from this software
* without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY MICROS,GROUP AND CONTRIBUTORS
* ''AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT
* NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
* FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
* THE MICROS,GROUP OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
* OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
* HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
* OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
* IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
* ===================================================================
* Author: Ren Xiaoguang, Wu Yunlong, Li Jinghua, micROS-DA Team, NIIDT, TAIIC.
*/

#include <perf_probe_mem_impl/perf_probe_mem_impl.h>

namespace perf_probe_mem_impl
{	
	PerfProbeMemImpl::PerfProbeMemImpl()
	{
		ROS_INFO("[Class] PerfProbeMemImpl Loaded!");
	}
  
	void PerfProbeMemImpl::getMem()
	{
		process_mem_usage(vm_usage_,resident_set_,getpid());
		std::cout << "PID: " << getpid() << " VM: " << vm_usage_ << " KB; RSS: " << resident_set_ <<" KB"<< std::endl;
	}

	void PerfProbeMemImpl::process_mem_usage(double& vm_usage, double& resident_set,int pid_int)
	{
		using std::ios_base;
		using std::ifstream;
		using std::string;

		vm_usage     = 0.0;
		resident_set = 0.0;

		// 'file' stat seems to give the most reliable results
		//
		std::stringstream ss;
		ss << pid_int;
		string pid_csr = ss.str();
		string path = "/proc/"+pid_csr+"/stat";

		ifstream stat_stream(path,ios_base::in);

		// dummy vars for leading entries in stat that we don't care about
		//
		string pid, comm, state, ppid, pgrp, session, tty_nr;
		string tpgid, flags, minflt, cminflt, majflt, cmajflt;
		string utime, stime, cutime, cstime, priority, nice;
		string O, itrealvalue, starttime;

		// the two fields we want
		//
		unsigned long vsize;
		long rss;

		stat_stream >> pid >> comm >> state >> ppid >> pgrp >> session >> tty_nr
					>> tpgid >> flags >> minflt >> cminflt >> majflt >> cmajflt
					>> utime >> stime >> cutime >> cstime >> priority >> nice
					>> O >> itrealvalue >> starttime >> vsize >> rss; // don't care about the rest

		stat_stream.close();

		long page_size_kb = sysconf(_SC_PAGE_SIZE) / 1024; // in case x86-64 is configured to use 2MB pages
		vm_usage     = vsize / 1024.0;
		resident_set = rss * page_size_kb;
	}

	PerfProbeMemImpl::~PerfProbeMemImpl()
	{
		
	}
}

PLUGINLIB_EXPORT_CLASS(perf_probe_mem_impl::PerfProbeMemImpl, perf_probe_mem_interface::PerfProbeMemInterface)






