/* ==================================================================
* Copyright (c) 2018, micROS Group, TAIIC, NIIDT & HPCL.
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions
* are met:
*
* 1. Redistributions of source code must retain the above copyright
* notice, this list of conditions and the following disclaimer.
* 2. Redistributions in binary form must reproduce the above copyright
* notice, this list of conditions and the following disclaimer in the
* documentation and/or other materials provided with the
* distribution.
* 3. All advertising materials mentioning features or use of this software
* must display the following acknowledgement:
* This product includes software developed by the micROS Group. and
* its contributors.
* 4. Neither the name of the Group nor the names of its contributors may
* be used to endorse or promote products derived from this software
* without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY MICROS,GROUP AND CONTRIBUTORS
* ''AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT
* NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
* FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
* THE MICROS,GROUP OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
* OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
* HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
* OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
* IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
* ===================================================================
* Author: Ren Xiaoguang, Wu Yunlong, Li Jinghua, micROS-DA Team, NIIDT, TAIIC.
*/

#ifndef PERF_PROBE_MEM_IMPL_H
#define PERF_PROBE_MEM_IMPL_H

#include <cmath>
#include <sys/time.h>
#include <unistd.h>
#include <ios>
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <pluginlib/class_list_macros.h>
#include <perf_probe_mem_impl/perf_probe_mem_interface.h>
#include <ros/ros.h>

/**
 * @brief Performance Probe Memmory Impl Namespace.
 * 
 */
namespace perf_probe_mem_impl
{
  /**
   * @brief This is a class for evaluating performance of memory cost.
   */
  class PerfProbeMemImpl : public perf_probe_mem_interface::PerfProbeMemInterface
  {
    public:
      PerfProbeMemImpl();
      ~PerfProbeMemImpl();

      /**
       * @brief Record control frequency
       */
      virtual void getMem();

    private:
      void process_mem_usage(double& vm_usage, double& resident_set,int pid);
      double vm_usage_;
		  double resident_set_;
  };
};
#endif
