#! /bin/sh

cd src/micros/behavior_management/decide

rm -rf plugins
tar -zxvf plugins.tar.gz

cd plugins
rm -rf cclvf_standofftracking_plugin_sim
rm -rf follower_lgvf_plugin_sim
rm -rf follower_navigate_plugin_sim
rm -rf gather_plugin_sim
rm -rf landing_path_follower_base_plugin_sim
rm -rf landing_path_manager_base_plugin_sim
rm -rf leader_lgvf_plugin_sim
rm -rf leader_navigate_plugin_sim
rm -rf takeoff_base_plugin_sim
cd ../../observe
rm -rf plugins
rm -rf cv_bridge
rm -rf observe_base
rm -rf observe_models
rm -rf observe_msgs
tar -zxvf cv_bridge.tar.gz
tar -zxvf observe_base.tar.gz
tar -zxvf observe_models.tar.gz
tar -zxvf observe_msgs.tar.gz
tar -zxvf plugins.tar.gz
cd plugins
rm -rf detector_sim_plugin
cd ../../../resource_management
rm -rf cognition
tar -zxvf cognition.tar.gz
cd ../../..
catkin_make -j1
