#! /bin/sh
echo "deb http://mirrors.ustc.edu.cn/ros/ubuntu/ $(lsb_release -sc) main" > /etc/apt/sources.list.d/ros-latest.list \
&& DEBIAN_FRONTEND="noninteractive" \
&& apt-get update && apt-get install --assume-yes apt-utils 
apt-get install -y ros-kinetic-qt-build 
apt-get install -y ros-kinetic-control-toolbox 
apt-get install -y ros-kinetic-ecl-geometry 
apt-get install -y ros-kinetic-ecl-streams 
apt-get install -y ros-kinetic-kobuki 
apt-get install -y libdc1394-22-dev 
apt-get install -y ros-kinetic-parrot-arsdk 
apt-get install -y ros-kinetic-geodesy 
apt-get install -y ros-kinetic-eigen-stl-containers 
apt-get install -y ros-kinetic-tf 
apt-get install -y ros-kinetic-swri-yaml-util 
apt-get install -y ros-kinetic-rqt-gui 
apt-get install -y ros-kinetic-rqt-gui-py 
apt-get install -y ros-kinetic-gazebo-plugins 
apt-get install -y ros-kinetic-qt-ros 
apt-get install -y libsdl1.2-dev 
apt-get install -y libsdl-image1.2-dev 
apt-get install -y libsdl-mixer1.2-dev 
apt-get install -y libsdl-ttf2.0-dev 
apt-get install -y libsdl-gfx1.2-dev 
apt-get install -y ros-kinetic-joy 
apt-get install -y python-pip python-dev build-essential 
apt-get install -y cmake git libgtk2.0-dev pkg-config libavcodec-dev libavformat-dev libswscale-dev  libproj-dev 
apt-get install -y python-dev python-numpy libtbb2 libtbb-dev libjpeg-dev libpng-dev libtiff-dev libjasper-dev libdc1394-22-dev 
apt-get install -y ros-kinetic-hector-pose-estimation* 
apt-get install -y ros-kinetic-ros-control 
apt-get install -y ros-kinetic-gazebo-ros-control 
apt-get install -y ros-kinetic-hector-gazebo-plugins 
source /opt/ros/kinetic/setup.bash 
apt-get clean 
rm -rf /var/lib/apt/lists/*
