#! /bin/bash
cd ../../../dependency
tar -zxvf mosquitto-1.4.10.tar.gz
cd mosquitto-1.4.10
sed -i 's|WITH_SRV:=yes|WITH_SRV:=no|' config.mk
make
sudo make install
