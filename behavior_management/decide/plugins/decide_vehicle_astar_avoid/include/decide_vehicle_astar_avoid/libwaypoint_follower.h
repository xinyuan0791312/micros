/* ==================================================================
* Copyright (c) 2018, micROS Group, TAIIC, NIIDT & HPCL.
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions
* are met:
*
* 1. Redistributions of source code must retain the above copyright
* notice, this list of conditions and the following disclaimer.
* 2. Redistributions in binary form must reproduce the above copyright
* notice, this list of conditions and the following disclaimer in the
* documentation and/or other materials provided with the
* distribution.
* 3. All advertising materials mentioning features or use of this software
* must display the following acknowledgement:
* This product includes software developed by the micROS Group. and
* its contributors.
* 4. Neither the name of the Group nor the names of its contributors may
* be used to endorse or promote products derived from this software
* without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY MICROS,GROUP AND CONTRIBUTORS
* ''AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT
* NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
* FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
* THE MICROS,GROUP OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
* OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
* HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
* OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
* IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
* ===================================================================
* Author: micROS-DA Team, TAIIC.
*/

#ifndef LIBWAYPOINT_FOLLOWER_H
#define LIBWAYPOINT_FOLLOWER_H

#include <iostream>
#include <sstream>
#include <fstream>

#include <tf/transform_broadcaster.h>
#include <tf/transform_listener.h>
#include "sirius_msgs/Lane.h"
#include "astar_util.h"

/**
 * @brief Waypoints Follower.
 */
class WayPoints
{
  protected:
    sirius_msgs::Lane current_waypoints_;

  public:

    /**
     * @brief Set current planned path
     */
    void setPath(const sirius_msgs::Lane &waypoints)
    {
      current_waypoints_ = waypoints;
    }

    /**
     * @brief Get size of current planned path
     */
    int getSize() const;

    /**
     * @brief Check if current path is empty
     */
    bool isEmpty() const
    {
      return current_waypoints_.waypoints.empty();
    }

    /**
     * @brief Get distance between two paypoints
     */
    double getInterval() const;

    /**
     * @brief Get position of waypoint
     */
    geometry_msgs::Point getWaypointPosition(int waypoint) const;

    /**
     * @brief Get orientation of waypoint
     */
    geometry_msgs::Quaternion getWaypointOrientation(int waypoint) const;

    /**
     * @brief Get pose of waypoint
     */
    geometry_msgs::Pose getWaypointPose(int waypoint) const;

    /**
     * @brief Get velocity of waypoint
     */
    double getWaypointVelocityMPS(int waypoint) const;
    sirius_msgs::Lane getCurrentWaypoints() const
    {
      return current_waypoints_;
    }

    /**
     * @brief Check if current pose is front of the car
     */
    bool isFront(int waypoint, geometry_msgs::Pose current_pose) const;
};

/**
 * @brief Transform point to vector
 */
tf::Vector3 point2vector(geometry_msgs::Point point);

/**
 * @brief Transform vector to point
 */
geometry_msgs::Point vector2point(tf::Vector3 vector);

/**
 * @brief Rotate unit vector by degree
 */
tf::Vector3 rotateUnitVector(tf::Vector3 unit_vector, double degree);

/**
 * @brief Rotate point vector by degree
 */
geometry_msgs::Point rotatePoint(geometry_msgs::Point point, double degree);

/**
 * @brief Decelerate velocity
 */
double decelerateVelocity(double distance, double prev_velocity);

/**
 * @brief Transform point into the coordinate of current_pose
 */
geometry_msgs::Point calcRelativeCoordinate(geometry_msgs::Point point,
                                            geometry_msgs::Pose current_pose);

/**
 * @brief Transform point into the global coordinate
 */
geometry_msgs::Point calcAbsoluteCoordinate(geometry_msgs::Point point,
                                            geometry_msgs::Pose current_pose);

/**
 * @brief Get 2 dimentional distance between target 1 and target 2
 */
double getPlaneDistance(geometry_msgs::Point target1,
                        geometry_msgs::Point target2);

/**
 * @brief Get closest waypoint
 */
int getClosestWaypoint(const sirius_msgs::Lane &current_path, geometry_msgs::Pose current_pose);

/**
 * @brief Get linear equation
 */
bool getLinearEquation(geometry_msgs::Point start, geometry_msgs::Point end, double *a, double *b, double *c);

/**
 * @brief Get distance between line and point
 */
double getDistanceBetweenLineAndPoint(geometry_msgs::Point point, double sa, double b, double c);

/**
 * @brief Get relative angle
 */
double getRelativeAngle(geometry_msgs::Pose waypoint_pose, geometry_msgs::Pose vehicle_pose);
#endif
