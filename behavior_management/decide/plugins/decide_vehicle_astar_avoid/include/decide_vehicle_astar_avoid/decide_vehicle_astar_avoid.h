/* ==================================================================
* Copyright (c) 2018, micROS Group, TAIIC, NIIDT & HPCL.
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions
* are met:
*
* 1. Redistributions of source code must retain the above copyright
* notice, this list of conditions and the following disclaimer.
* 2. Redistributions in binary form must reproduce the above copyright
* notice, this list of conditions and the following disclaimer in the
* documentation and/or other materials provided with the
* distribution.
* 3. All advertising materials mentioning features or use of this software
* must display the following acknowledgement:
* This product includes software developed by the micROS Group. and
* its contributors.
* 4. Neither the name of the Group nor the names of its contributors may
* be used to endorse or promote products derived from this software
* without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY MICROS,GROUP AND CONTRIBUTORS
* ''AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT
* NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
* FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
* THE MICROS,GROUP OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
* OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
* HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
* OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
* IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
* ===================================================================
* Author: micROS-DA Team, TAIIC.
*/

#ifndef DECIDE_VEHICLE_ASTAR_AVOID_H
#define DECIDE_VEHICLE_ASTAR_AVOID_H

#include "general_plugin/general_plugin.h"

#include <opencv2/opencv.hpp>
#include <iostream>
#include <vector>
#include <thread>
#include <mutex>

#include <ros/ros.h>
#include <tf/transform_listener.h>
#include <std_msgs/Int32.h>
#include <std_msgs/Float32.h>
#include <std_msgs/String.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/TwistStamped.h>

#include "libwaypoint_follower.h"
#include "astar_search.h"


#define PI 3.1415926

/**
 * @brief General Bus Namespace
 */
namespace general_bus
{
/**
 * @brief Decide Vehicle Astar Avoid
 */
class DecideVehicleAstarAvoid: public GeneralPlugin
{
  public:
    /**
     * @brief Overload start function of General Plugin
     */
    virtual void start();

    /**
     * @brief Costmap message callback
     */
    void costmapCallback(const nav_msgs::OccupancyGrid::ConstPtr& msg);

    /**
     * @brief Target point pose message callback
     */
    void goalPoseCallback(const sirius_msgs::GuidePointL::ConstPtr& msg);

    /**
     * @brief Reference path points message callback
     */
    void baseWaypointsCallback(const sirius_msgs::GuideTrajectoryL::ConstPtr& msg);

    /**
     * @brief Local_pose message callback
     */
    void localPoseCallback(const sirius_msgs::Pose::ConstPtr& msg);

    /**
     * @brief Global_pose message callback
     */
    void globalPoseCallback(const sirius_msgs::Pose::ConstPtr& msg);

    /**
     * @brief Current work mode message of the car callback
     */
    void workModeCallback(const sirius_msgs::UnmannedDrivingCommand::ConstPtr &msg);

    /**
     * @brief Convert axis angle to quaternion format
     */
    geometry_msgs::Quaternion direction(double theta_);

    /**
     * @brief Setting target point pose according to point index
     */
    bool setGoalPoseLocal(int point_index);

    /**
     * @brief Publish final planning path
     */
    void publishWaypoints();

    /**
     * @brief Calculate distance ahead the car
     */
    int lookaheadDistance(sirius_msgs::GuideTrajectoryL waypoints, double thread);

    /**
     * @brief calculate yaw angle of the car at target point
     */
    double goalPoseDir(double x1, double y1, double x2, double y2);

    /**
     * @brief Dilate the costmap
     */
    void costmapDilate(cv::Mat element);

    /**
     * @brief Check if the planned path is safe
     */
    bool baseWaypointsSafetyCheck(sirius_msgs::GuideTrajectoryL waypoints,nav_msgs::OccupancyGrid costmap);

    /**
     * @brief Find a path to avoid the obstacle
     */
    void avoidPlan();

    /**
     * @brief Entery of main function of astar
     */
    void test1();

    /**
     * @brief Calculate speed according to planned path
     */
    void speedControl();

    /**
     * @brief If can't find new path, then keep former way
     */
    sirius_msgs::GuideTrajectoryL keepWay(sirius_msgs::GuideTrajectoryL way);

    /**
     * @brief Translate local pose to global pose
     */
    void local2Global(double local_coord_x, double local_coord_y, double local_coord_yaw, double local_x, double local_y, double* const p_global_x, double* const p_global_y);

    /**
     * @brief Translate global pose to local pose
     */
    void global2Local(double local_coord_x, double local_coord_y, double local_coord_yaw, double global_x, double global_y, double* const p_local_x, double* const p_local_y);

    /**
     * @brief Publish some message for display
     */
    void serveForShowResult();

  private:
    ros::NodeHandle nh_;
    ros::NodeHandle private_nh_;
    ros::Publisher safety_waypoints_pub_;
    ros::Publisher GuideTrajectoryL_pub_;
    ros::Subscriber costmap_sub_;
    ros::Subscriber current_pose_sub_;
    ros::Subscriber global_pose_sub_;
    ros::Subscriber goal_pose_sub_;
    ros::Subscriber base_waypoints_sub_;
    ros::Subscriber mode_sub_;

    ros::Publisher temp_guide_waypoints_pub_;
    ros::Publisher temp_goal_pose_local_pub_;
    ros::Publisher temp_costmap_pub_;
    ros::Publisher temp_safe_way_pub_;
    ros::Publisher temp_current_pose_local_pub_;
    ros::Publisher temp_speed_value_pub_;
    ros::Publisher temp_count_value_pub_;

    ros::Rate *rate_;
    ros::Rate *rate_thread_;
    tf::TransformListener tf_listener_;
    int count_sek;
    nav_msgs::Path temp_path;

    int safety_waypoints_size_;   // output waypoint size [-]
    double update_rate_;          // publishing rate [Hz]
    double thread_rate_;          // publishing rate [Hz]

    AstarSearch astar_;

    std::thread publish_thread_;
    std::mutex mutex_;

    ros::WallTime global_time;
    bool terminate_thread_;
    int closest_waypoint_index_;
    nav_msgs::OccupancyGrid costmap_,temp_costmap_;
    geometry_msgs::PoseStamped current_pose_local_;
    geometry_msgs::PoseStamped goal_pose_local_;
    sirius_msgs::GuideTrajectoryL safe_way_pub;
    sirius_msgs::GuideTrajectoryL guide_waypoints_;
    sirius_msgs::GuideTrajectoryL safe_way_;
    sirius_msgs::UnmannedDrivingCommand driveMode_;
    sirius_msgs::Pose local_pose_;
    double speed_value_;
    double last_speed_value_;
    bool guide_waypoints_initialized_;
    bool costmap_initialized_;
    bool current_pose_initialized_;
    bool goal_pose_initialized_;
    bool base_waypoints_initialized_;
    bool baseWayMode_;
    bool local_pose_initialized_;
};  // end class DecideVehicleAstarAvoid
} // end namespace
#endif
