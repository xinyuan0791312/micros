/* ==================================================================
* Copyright (c) 2018, micROS Group, TAIIC, NIIDT & HPCL.
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions
* are met:
*
* 1. Redistributions of source code must retain the above copyright
* notice, this list of conditions and the following disclaimer.
* 2. Redistributions in binary form must reproduce the above copyright
* notice, this list of conditions and the following disclaimer in the
* documentation and/or other materials provided with the
* distribution.
* 3. All advertising materials mentioning features or use of this software
* must display the following acknowledgement:
* This product includes software developed by the micROS Group. and
* its contributors.
* 4. Neither the name of the Group nor the names of its contributors may
* be used to endorse or promote products derived from this software
* without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY MICROS,GROUP AND CONTRIBUTORS
* ''AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT
* NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
* FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
* THE MICROS,GROUP OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
* OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
* HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
* OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
* IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
* ===================================================================
* Author: micROS-DA Team, TAIIC.
*/

#ifndef ASTAR_SEARCH_H
#define ASTAR_SEARCH_H

#include <iostream>
#include <vector>
#include <queue>
#include <string>

#include <ros/ros.h>
#include <tf/tf.h>
#include <nav_msgs/OccupancyGrid.h>
#include <geometry_msgs/PoseArray.h>
#include <nav_msgs/Path.h>
#include "sirius_msgs/GuideTrajectoryL.h"
#include "sirius_msgs/SinglePointMotionTask.h"
#include "sirius_msgs/GuidePointL.h"
#include "sirius_msgs/UnmannedDrivingCommand.h"
#include "astar_util.h"

/**
 * @brief This is a class to implement the astar search algorithm.
 */
class AstarSearch
{
  friend class TestClass;

public:

  /**
   * @brief Constructor function
   */
  AstarSearch();

  /**
   * @brief Destructor function
   */
  ~AstarSearch();

  /**
   * @brief Initialize costmap
   */
  void initialize(const nav_msgs::OccupancyGrid& costmap);

  /**
   * @brief Plan path between start_pose and goal_pose
   */
  bool makePlan(const geometry_msgs::Pose& start_pose, const geometry_msgs::Pose& goal_pose);

  /**
   * @brief Reset planned path
   */
  void reset();

  sirius_msgs::GuideTrajectoryL close_way_;
  bool close_way_flag;
  sirius_msgs::GuideTrajectoryL close_keep_way_;
  bool close_keep_way_flag;
  const nav_msgs::Path& getPath() const
  {
    return path_;
  }

private:

  /**
   * @brief Find closest point
   */
  double findClosePoint(sirius_msgs::GuideTrajectoryL close_way_, double x,double y);

  /**
   * @brief Create state update table
   */
  void createStateUpdateTable();

  /**
   * @brief Search path
   */
  bool search();

  /**
   * @brief Get index according to pose
   */
  void poseToIndex(const geometry_msgs::Pose& pose, int* index_x, int* index_y, int* index_theta);

  /**
   * @brief Get pose according to index
   */
  void pointToIndex(const geometry_msgs::Point& point, int* index_x, int* index_y);

  /**
   * @brief Judge if the index of x and y are out of range
   */
  bool isOutOfRange(int index_x, int index_y);

  /**
   * @brief Set planned path
   */
  void setPath(const SimpleNode& goal);

  /**
   * @brief Set start node
   */
  bool setStartNode(const geometry_msgs::Pose& start_pose);

  /**
   * @brief Set target node
   */
  bool setGoalNode(const geometry_msgs::Pose& goal_pose);

  /**
   * @brief Check if the next state is the goal
   */
  bool isGoal(double x, double y, double theta);

  /**
   * @brief Check if the point is obstacle
   */
  bool isObs(int index_x, int index_y);

  /**
   * @brief Detect collision
   */
  bool detectCollision(const SimpleNode& sn);

  /**
   * @brief CXalculate wave front heuristic
   */
  bool calcWaveFrontHeuristic(const SimpleNode& sn);

  /**
   * @brief Simple collidion detection for wavefront search
   */
  bool detectCollisionWaveFront(const WaveFrontNode& sn);

  // ros param
  ros::NodeHandle n_;

  // base configs
  bool use_back_;                 // backward search
  bool use_potential_heuristic_;  // potential cost function
  bool use_wavefront_heuristic_;  // wavefront cost function
  double time_limit_;             // planning time limit [msec]

  // robot configs (TODO: obtain from vehicle_info)
  double robot_length_;           // X [m]
  double robot_width_;            // Y [m]
  double robot_base2back_;        // base_link to rear [m]
  double minimum_turning_radius_; // [m]]

  // search configs
  int theta_size_;                  // descritized angle table size [-]
  double curve_weight_;             // curve moving cost [-]
  double reverse_weight_;           // backward moving cost [-]
  double lateral_goal_range_;       // reaching threshold, lateral error [m]
  double longitudinal_goal_range_;  // reaching threshold, longitudinal error [m]
  double angle_goal_range_;         // reaching threshold, angle error [deg]

  // costmap configs
  int obstacle_threshold_;            // obstacle threshold on grid [-]
  double potential_weight_;           // weight of potential cost [-]
  double distance_heuristic_weight_;  // obstacle threshold on grid [0,255]

  // hybrid astar variables
  std::vector<std::vector<NodeUpdate>> state_update_table_;
  std::vector<std::vector<std::vector<AstarNode>>> nodes_;
  std::priority_queue<SimpleNode, std::vector<SimpleNode>, std::greater<SimpleNode>> openlist_;
  std::vector<SimpleNode> goallist_;

  // costmap as occupancy grid
  nav_msgs::OccupancyGrid costmap_;

  // pose in costmap frame
  geometry_msgs::PoseStamped start_pose_local_;
  geometry_msgs::PoseStamped goal_pose_local_;
  double goal_yaw_;

  // result path
  nav_msgs::Path path_;
};

#endif
