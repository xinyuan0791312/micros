/* ==================================================================
* Copyright (c) 2018, micROS Group, TAIIC, NIIDT & HPCL.
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions
* are met:
*
* 1. Redistributions of source code must retain the above copyright
* notice, this list of conditions and the following disclaimer.
* 2. Redistributions in binary form must reproduce the above copyright
* notice, this list of conditions and the following disclaimer in the
* documentation and/or other materials provided with the
* distribution.
* 3. All advertising materials mentioning features or use of this software
* must display the following acknowledgement:
* This product includes software developed by the micROS Group. and
* its contributors.
* 4. Neither the name of the Group nor the names of its contributors may
* be used to endorse or promote products derived from this software
* without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY MICROS,GROUP AND CONTRIBUTORS
* ''AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT
* NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
* FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
* THE MICROS,GROUP OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
* OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
* HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
* OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
* IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
* ===================================================================
* Author: Ren Xiaoguang, Wu Yunlong, Li Jinghua, micROS-DA Team, TAIIC.
*/

#include "decide_vehicle_astar_avoid/decide_vehicle_astar_avoid.h"
#include <pluginlib/class_list_macros.h>

PLUGINLIB_EXPORT_CLASS(general_bus::DecideVehicleAstarAvoid, general_bus::GeneralPlugin)

namespace general_bus
{
void DecideVehicleAstarAvoid::start()
{
  nh_ = ros::NodeHandle();
  private_nh_ = ros::NodeHandle("~");
	
  closest_waypoint_index_ = -1;
  costmap_initialized_ = false;
  base_waypoints_initialized_ = false; 
  terminate_thread_ = false;
  guide_waypoints_initialized_ = false;
  local_pose_initialized_ = false;
  baseWayMode_ = false;
  
  private_nh_.param<double>("update_rate", update_rate_, 100.0);
  private_nh_.param<double>("thread_rate", thread_rate_, 10.0);
  
  GuideTrajectoryL_pub_ = pluginAdvertise<sirius_msgs::GuideTrajectoryL>("/sirius/planning/guide_trajectory_l", 1);

  costmap_sub_ = pluginSubscribe<nav_msgs::OccupancyGrid>("/sirius/perception/occupancy_grid", 1, &DecideVehicleAstarAvoid::costmapCallback, this);
  goal_pose_sub_ = pluginSubscribe<sirius_msgs::GuidePointL>("sirius/ccob/guide_point_l", 1, &DecideVehicleAstarAvoid::goalPoseCallback, this);
  current_pose_sub_ = pluginSubscribe<sirius_msgs::Pose>("/sirius/localization/local_pose", 1, &DecideVehicleAstarAvoid::localPoseCallback, this);
  global_pose_sub_ = pluginSubscribe<sirius_msgs::Pose>("/sirius/sensor/ins/global_pose", 1, &DecideVehicleAstarAvoid::globalPoseCallback, this);
  base_waypoints_sub_ = pluginSubscribe<sirius_msgs::GuideTrajectoryL>("/sirius/ccob/guide_trajectory_l", 1, &DecideVehicleAstarAvoid::baseWaypointsCallback, this);
  mode_sub_= pluginSubscribe<sirius_msgs::UnmannedDrivingCommand>("/sirius/ccob/udc", 1, &DecideVehicleAstarAvoid::workModeCallback, this);

  //! added on 2019_1012.
  temp_guide_waypoints_pub_ = pluginAdvertise<sirius_msgs::GuideTrajectoryL>("/sirius/astar_avoid_plugin/temp_guide_waypoints", 1);
  temp_goal_pose_local_pub_ = pluginAdvertise<geometry_msgs::PoseStamped>("/sirius/astar_avoid_plugin/temp_goal_pose_local", 1);
  temp_costmap_pub_ = pluginAdvertise<nav_msgs::OccupancyGrid>("/sirius/astar_avoid_plugin/temp_costmap", 1);
  temp_safe_way_pub_ = pluginAdvertise<sirius_msgs::GuideTrajectoryL>("/sirius/astar_avoid_plugin/temp_safe_way", 1);
  temp_current_pose_local_pub_ = pluginAdvertise<geometry_msgs::PoseStamped>("/sirius/astar_avoid_plugin/temp_current_pose_local", 1);
  temp_speed_value_pub_ = pluginAdvertise<std_msgs::Float32>("/sirius/astar_avoid_plugin/temp_speed_value", 1);
  temp_count_value_pub_ = pluginAdvertise<std_msgs::Float32>("/sirius/astar_avoid_plugin/temp_count_value", 1);


  rate_ = new ros::Rate(update_rate_);
  rate_thread_ = new ros::Rate(thread_rate_);
  costmap_.info.height=250;//规划地图大小为50*50米，分辨率为0.2米
  costmap_.info.width=250;//源地图的大小为500*500，分辨率为0.2米，车辆在地图中心
  costmap_.info.resolution=1;
  costmap_.info.origin.position.x=0;    costmap_.info.origin.position.y=0;    costmap_.info.origin.position.z=0;
  costmap_.info.origin.orientation=direction(0);
  for(int i=0;i<costmap_.info.height;i++)
  {
    for(int j=0;j<costmap_.info.width;j++)
    {
      costmap_.data.push_back(0);
    }
  }

  current_pose_local_.pose.position.x= costmap_.info.width/2;
  current_pose_local_.pose.position.y=1;
  current_pose_local_.pose.position.z=0;
  current_pose_local_.pose.orientation=direction(90);
  count_sek=10;
  
  global_time= ros::WallTime::now();

  publish_thread_ = std::thread(&DecideVehicleAstarAvoid::publishWaypoints, this);
  ROS_INFO(" [DecideVehicleAstarAvoid] 222 !!!");
  while(ros::ok())
  {
    GOON_OR_RETURN;

    test1();

    usleep(50000);
  }	
  publish_thread_.join();
}

void DecideVehicleAstarAvoid::workModeCallback(const sirius_msgs::UnmannedDrivingCommand::ConstPtr& msg)
{
    driveMode_= *msg;
    ROS_INFO("receive driveMode success!mode = %d ",driveMode_.cmd);
}

void DecideVehicleAstarAvoid::localPoseCallback(const sirius_msgs::Pose::ConstPtr& msg)
{
    local_pose_= *msg;
    local_pose_initialized_ = true;
}

void DecideVehicleAstarAvoid::globalPoseCallback(const sirius_msgs::Pose::ConstPtr& msg)
{
     //global_pose_= msg;
     //global_pose_initialized_ = true;
     //local_pose_= msg;
     //local_pose_initialized_ = true;
}

void DecideVehicleAstarAvoid::costmapCallback(const nav_msgs::OccupancyGrid::ConstPtr& msg)
{
      temp_costmap_ = *msg;
      //把temp_costmap的数据给costmap，统一单位

      for(int i=0;i<costmap_.info.height;i++)
      {
          for(int j=0;j<costmap_.info.width;j++)
          {
              //if(i<10)
              //    costmap_.data[i*costmap_.info.width+j]=0;
             // else
                  costmap_.data[i*costmap_.info.width+j]=temp_costmap_.data[(i+temp_costmap_.info.height/2)*temp_costmap_.info.width+j+costmap_.info.width/2];
          }
      }
      costmap_initialized_ = true;
      ROS_INFO("receive OccupancyGrid ok");
}

void DecideVehicleAstarAvoid::goalPoseCallback(const sirius_msgs::GuidePointL::ConstPtr& msg)
{
    goal_pose_local_.pose.position.x=msg->point.x*5+125;//接收到的目标点单位为米，在车体坐标系下
    goal_pose_local_.pose.position.y=msg->point.y*5;
    goal_pose_local_.pose.position.z=0;
    goal_pose_local_.pose.orientation=direction(90);
    goal_pose_initialized_ = true;
    //ROS_INFO("receive SinglePointMotionTask ok,x=%f,y=%f",msg.point.x,msg.point.y);
}

bool DecideVehicleAstarAvoid::baseWaypointsSafetyCheck(sirius_msgs::GuideTrajectoryL waypoints,nav_msgs::OccupancyGrid costmap)
{
    double dis=calcDistance(waypoints.points[0].x,waypoints.points[0].y,
            current_pose_local_.pose.position.x,current_pose_local_.pose.position.y);
    if(dis<10)
    {
        for(int i=0;i<waypoints.point_num;i++)
        {
             int x=(int)(waypoints.points[i].x);
             int y=(int)(waypoints.points[i].y);
            if (x > 0 && x <costmap.info.width && y>0 && y <costmap.info.width)
                if(costmap.data[y*costmap_.info.width+x]>0)
                    return(false);
        }
    }
    else
    {
        return(false);
    }
    return (true);
}

void DecideVehicleAstarAvoid::baseWaypointsCallback(const sirius_msgs::GuideTrajectoryL::ConstPtr& msg)
{
  sirius_msgs::GuideTrajectoryL temp;
  temp = *msg;
  guide_waypoints_.point_num=temp.point_num;
  ROS_INFO("guide_waypoints_.point_num=%d",guide_waypoints_.point_num);
  for(int i=0; i<guide_waypoints_.point_num; i++)
  {
    guide_waypoints_.points[i].x = temp.points[i].x * 5 + costmap_.info.width/2;
    guide_waypoints_.points[i].y = temp.points[i].y * 5;
  }

  closest_waypoint_index_ = lookaheadDistance(guide_waypoints_, 120.0);
  guide_waypoints_initialized_ = true;
}

int DecideVehicleAstarAvoid::lookaheadDistance(sirius_msgs::GuideTrajectoryL waypoints, double thread)
{
  double look_distance_threshold = 40;
  std::vector<cv::Point2f> P;

  cv::Point2f Pstart, Pend, tempP;
  int i;
  for (i=0; i<waypoints.point_num; i++)
  {
    tempP.x = waypoints.points[i].x;
    tempP.y = waypoints.points[i].y;
    P.push_back(tempP);
    double evaluate_value = 0;
    if (i>10)
    {
      Pstart = P[1];
      Pend = P[i];
      for (int j=1; j<P.size()-1; j++)
      {
        tempP = P[j];
        double denominator = calcDistance(Pend.x, Pend.y, Pstart.x, Pstart.y);
        double numerator =   (Pend.y - Pstart.y) * tempP.x - (Pend.x - Pstart.x) * tempP.y
                           + (Pend.x - Pstart.x) * Pend.y - (Pend.y - Pstart.y) * Pend.x;
        if (denominator != 0)
          evaluate_value = evaluate_value + fabs(numerator / denominator);

        if (evaluate_value < thread)
          continue;
        else
          return (j > look_distance_threshold ? look_distance_threshold : j);
      }
    }
  }
  int pointT = i-1 > look_distance_threshold ? look_distance_threshold : i-1;
  if (pointT < 10)
    pointT = waypoints.point_num>10 ? 10 : waypoints.point_num;

  return(pointT);
}

geometry_msgs::Quaternion DecideVehicleAstarAvoid::direction(double theta_)
{//向右为0度，逆时针增加
    double theta=theta_*PI/180;//为偏航角
    double a[9]={0};
    a[8]=1.0;
    double q[4]={0};
    q[0]=1.0;
    a[0]=cos(theta);
     a[1]=sin(theta);
     a[3]=-a[1];
     a[4]=a[0];
     geometry_msgs::Quaternion result;
     q[0]=sqrt(a[0]+a[4]+a[8]+1)/2;
     q[1]=(a[5]-a[7])/(4*q[0]);
     q[2]=(a[6]-a[2])/(4*q[0]);
     q[3]=(a[1]-a[3])/(4*q[0]);
     result.w=q[0];
     result.x=q[1];
     result.y=q[2];
     result.z=q[3];
    return result;
}

double DecideVehicleAstarAvoid::goalPoseDir(double x1, double y1, double x2, double y2)
{
  double goal_pose_dir_value;
  double dx = x2 - x1;
  double dy = y2 - y1;
  if (dx==0)
    goal_pose_dir_value = 90;
  else
    goal_pose_dir_value = atan(dy/dx)*180/PI;
  if (y2>=y1)
    return(goal_pose_dir_value<0 ? 180+goal_pose_dir_value : goal_pose_dir_value);
  else
    return(goal_pose_dir_value>0 ? 180+goal_pose_dir_value : goal_pose_dir_value);
}

void DecideVehicleAstarAvoid::costmapDilate(cv::Mat element)
{
  //从costmap取点，膨胀
  cv::Mat img_pengzhang_src = cv::Mat::zeros(costmap_.info.height,costmap_.info.width,CV_8UC3);
  cv::Mat img_pengzhang_dir = cv::Mat::zeros(costmap_.info.height,costmap_.info.width,CV_8UC3);
  for(int y=0;y<costmap_.info.height;y++)
    for(int x=0;x<costmap_.info.width;x++)
      if(costmap_.data[y*costmap_.info.width+x] > 0 )
        img_pengzhang_src.at<cv::Vec3b>(x,y) = cv::Vec3b(0,0,255);

  cv::dilate( img_pengzhang_src, img_pengzhang_dir, element );

  for(int y=0;y<costmap_.info.height;y++)
    for(int x=0;x<costmap_.info.width;x++)
      if( img_pengzhang_dir.at<cv::Vec3b>(x,y)[2]>0)
        costmap_.data[y*costmap_.info.width+x] =102;
}

bool DecideVehicleAstarAvoid::setGoalPoseLocal(int point_index)
{
    //int guide_index=closest_waypoint_index_;
    int guide_index=point_index;

    while(guide_index>10 && guide_index< guide_waypoints_.point_num)
    {
        int x=(int)guide_waypoints_.points[guide_index].x;
        int y=(int)guide_waypoints_.points[guide_index].y;
        int sum_value=0;
        for(int i=x-5;i<x+5;i++)
        {
            for(int j=y-5;j<y+5;j++)
            {
                if(i>0 && i <costmap_.info.width && j > 0 && j < costmap_.info.height)
                    sum_value=sum_value+costmap_.data[j*costmap_.info.width+i];
                else
                    sum_value++;
                //ROS_INFO("sum_value= %d",sum_value);
            }
        }
        if(sum_value>0)
        {
            guide_index--;
            //ROS_INFO("guide_index= %d",guide_index);
        }
        else
        {
            closest_waypoint_index_=guide_index;
            //ROS_INFO("guide_index= %d",guide_index);
            return(true);
        }
    }
    closest_waypoint_index_=guide_index;
    return(false);
}

void DecideVehicleAstarAvoid::test1(void)
{
  if (costmap_initialized_ )
  {
    ROS_INFO("Astar working!");
    costmap_initialized_ = false;
    cv::Mat element = cv::getStructuringElement(0, cv::Size(5, 13));
    costmapDilate(element);

    {
      avoidPlan();
    }

    ROS_INFO("count_sek=%d", count_sek);

    speedControl();

    serveForShowResult();
  }
}

void  DecideVehicleAstarAvoid::local2Global(double local_coord_x, double local_coord_y, double local_coord_yaw, double local_x, double local_y, double* const p_global_x, double* const p_global_y) {
    *p_global_x = local_x*cos(local_coord_yaw) - local_y*sin(local_coord_yaw) + local_coord_x;
    *p_global_y = local_y*cos(local_coord_yaw) + local_x*sin(local_coord_yaw) + local_coord_y;
}

void  DecideVehicleAstarAvoid::global2Local(double local_coord_x, double local_coord_y, double local_coord_yaw, double global_x, double global_y, double* const p_local_x, double* const p_local_y) {
    // translation
    double x1 = global_x - local_coord_x;
    double y1 = global_y - local_coord_y;
    // rotation
    *p_local_x  = x1*cos(local_coord_yaw) + y1*sin(local_coord_yaw);
    *p_local_y  = y1*cos(local_coord_yaw) - x1*sin(local_coord_yaw);
}

sirius_msgs::GuideTrajectoryL DecideVehicleAstarAvoid::keepWay(sirius_msgs::GuideTrajectoryL way)
{
  sirius_msgs::GuideTrajectoryL way_keep;
  way_keep.point_num = 0;
  double global_x;
  double global_y;
  double local_x;
  double local_y;
  double local_coord_yaw = way.frz_pose.yaw - M_PI*0.5;
  double local_coord_x = way.frz_pose.x*5;
  double local_coord_y = way.frz_pose.y*5;
  double local_coord_yaw2 = local_pose_.yaw - M_PI*0.5;
  double local_coord_x2 = local_pose_.x*5;
  double local_coord_y2 = local_pose_.y*5;
  for(int i=0; i<way.point_num; i++)
  {
    local_x = way.points[i].x;
    local_y = way.points[i].y;
    local2Global(local_coord_x,local_coord_y,local_coord_yaw,local_x,local_y, &global_x, &global_y);
    global2Local(local_coord_x2,local_coord_y2,local_coord_yaw2, global_x,  global_y, &local_x, &local_y);
    double x3 = local_x;
    double y3 = local_y;
    if(x3>0 && x3 < costmap_.info.width && y3 >0 && y3 < costmap_.info.height)
    {
      way_keep.points[way_keep.point_num].x = x3;
      way_keep.points[way_keep.point_num].y = y3;
      way_keep.point_num++;
    }
  }
  return(way_keep);
}

void DecideVehicleAstarAvoid::speedControl()
{
    //calculate speed
    int speed_fan = lookaheadDistance(safe_way_, 100.0);
    ROS_INFO("speed_fan = %d", speed_fan);
    if (speed_fan < 5)
    {
      speed_value_=0;
    }
    else
    {
      double dis = safe_way_.points[speed_fan].y * 0.2 * 0.1;

      speed_value_ = dis< 1 ? 1 : dis;
      speed_value_ = dis< 3 ? dis : 3;
      baseWayMode_ = baseWaypointsSafetyCheck(guide_waypoints_, costmap_);

      if (baseWayMode_ == false)
      {
        speed_value_ = 1;
      }

      if ((safe_way_.points[speed_fan].y*0.2) < 10.0)
      {
        speed_value_ = 1;
      }
    }

    if (count_sek > 9)
    {
      speed_value_ = 0;
    }

    speed_value_ = 1.5;
    for (int i=0; i<safe_way_.point_num; i++)
        safe_way_.points[i].speed = speed_value_;

    last_speed_value_=speed_value_;
}

void DecideVehicleAstarAvoid::avoidPlan()
{
  ROS_INFO("guide_waypoints_.point_num=%d", guide_waypoints_.point_num);
  if (!setGoalPoseLocal(closest_waypoint_index_))
  {
    if (!setGoalPoseLocal(guide_waypoints_.point_num-1))
    {
      if (!setGoalPoseLocal(guide_waypoints_.point_num-7))
      {
        ROS_WARN("goal is in obstacles!!!");

        if (count_sek<10)
        {
          count_sek++;
        }
        return;
      }
    }
  }

  ROS_INFO("closest_waypoint_index_=%d", closest_waypoint_index_);
  goal_pose_local_.pose.position.x = guide_waypoints_.points[closest_waypoint_index_].x;
  goal_pose_local_.pose.position.y = guide_waypoints_.points[closest_waypoint_index_].y;
  goal_pose_local_.pose.position.z = 0;
  double theta = goalPoseDir(guide_waypoints_.points[closest_waypoint_index_].x,
                             guide_waypoints_.points[closest_waypoint_index_].y,
                             guide_waypoints_.points[closest_waypoint_index_+1].x,
                             guide_waypoints_.points[closest_waypoint_index_+1].y);

  goal_pose_local_.pose.orientation = direction(theta);
  safe_way_.frz_pose = local_pose_;
  astar_.initialize(costmap_);

  if (guide_waypoints_.point_num>0)
  {
    astar_.close_way_ = guide_waypoints_;
    astar_.close_way_flag = true;
  }
  else
  {
    astar_.close_way_flag = false;
  }

  bool found_path = false;
  found_path = astar_.makePlan(current_pose_local_.pose, goal_pose_local_.pose);
  if (found_path)
  {
    temp_path = astar_.getPath();
    int a = 0;
    if (temp_path.poses.size()>500)
    {
      a = temp_path.poses.size()/500+1;
      safe_way_.point_num = temp_path.poses.size()/a;
      for (int i=0; (i*a)<temp_path.poses.size(); i++)
      {
        safe_way_.points[i].x = temp_path.poses[i*a].pose.position.x;
        safe_way_.points[i].y = temp_path.poses[i*a].pose.position.y;
      }
    }
    else
    {
      safe_way_.point_num = temp_path.poses.size();
      for (int i=0; i<temp_path.poses.size(); i++)
      {
        safe_way_.points[i].x = temp_path.poses[i].pose.position.x;
        safe_way_.points[i].y = temp_path.poses[i].pose.position.y;
      }
    }
  }
  else
  {
    safe_way_ = keepWay(safe_way_);
  }

  if (found_path)
    count_sek = 0;
  else if (count_sek<10)
    count_sek++;

  astar_.reset();
}

//! added on 2019_1012.
void DecideVehicleAstarAvoid::serveForShowResult() {

  temp_costmap_pub_.publish(costmap_);
  temp_current_pose_local_pub_.publish(current_pose_local_);
  temp_goal_pose_local_pub_.publish(goal_pose_local_);
  temp_safe_way_pub_.publish(safe_way_);

  std_msgs::Float32 num_msg;
  num_msg.data = count_sek;
  temp_count_value_pub_.publish(num_msg);
  num_msg.data = speed_value_;
  temp_speed_value_pub_.publish(num_msg);

  if ( guide_waypoints_initialized_)
    temp_guide_waypoints_pub_.publish(guide_waypoints_);
}

void DecideVehicleAstarAvoid::publishWaypoints()
{

  while (!terminate_thread_)
  {
    if(count_sek<10 )
    {
       // ROS_INFO("publish road is working!!!!");
        for(int i=0;i<safe_way_.point_num;i++)
        {
            safe_way_pub.points[i].x=(safe_way_.points[i].x-costmap_.info.width/2)*0.2;
            safe_way_pub.points[i].y=safe_way_.points[i].y*0.2;
            safe_way_pub.points[i].speed=safe_way_.points[i].speed;
         }
        safe_way_pub.point_num=safe_way_.point_num;
        safe_way_pub.frz_pose=safe_way_.frz_pose;
        GuideTrajectoryL_pub_.publish(safe_way_pub);
        //safe_way_.point_num=0;
    }
    rate_thread_->sleep();
  }
}

} 

// int main(int argc, char** argv)
// {
//   ros::init(argc, argv, "act_patrol_follower");

    
//   general_bus::DecideVehicleAstarAvoid da_plugin;


//   return 0; 
// }

