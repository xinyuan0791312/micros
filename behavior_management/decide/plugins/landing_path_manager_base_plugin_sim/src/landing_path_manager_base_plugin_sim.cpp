#include "landing_path_manager_base_plugin_sim.h"
#include <pluginlib/class_list_macros.h>

PLUGINLIB_EXPORT_CLASS(general_bus::LandingPathManagerBasePlugin, general_bus::GeneralPlugin)

namespace general_bus
{  
/**  Start plugin */
void LandingPathManagerBasePlugin::start()
{
  ros::NodeHandle nh_private_("~");
  nh_private_.param<int>("robot_id", ID_, 1); 
  nh_private_.param<double>("R_min", params_.R_min, 25.0);
  nh_private_.param<float>("loiter_radius_", loiter_radius_, 30.0);
  nh_private_.param<float>("High_c_delta", high_c_delta, 3);
  nh_private_.param<float>("Land_X_delta", land_X_delta, 20);
  nh_private_.param<float>("Va_delta", Va_delta, 0.05);
  nh_private_.param<float>("threshold_of_health", threshold_of_health, 20);  
  nh_private_.param<float>("Distance_x", distance_x, 0);
  nh_private_.param<float>("Distance_y", distance_y, 250);
  nh_private_.param<float>("High_base", high_base, 50);  
  nh_private_.param<float>("Gather_x", gathering_waypoint_.w[0], -1600);
  nh_private_.param<float>("Gather_y", gathering_waypoint_.w[1], -800);
  nh_private_.param<int>("Healthy_notification_time", healthy_notification_time, 15000);
  nh_private_.param<int>("Healthy_decision_time", healthy_decision_time, 1000);
  nh_private_.param<int>("Broadcast_fre", broadcast_fre, 10);

  high_c_delta = atof(getParam("High_c_delta").c_str());  
  land_X_delta = atof(getParam("Land_X_delta").c_str());
  Va_delta = atof(getParam("Va_delta").c_str());
  threshold_of_health = atof(getParam("threshold_of_health").c_str());
  gathering_waypoint_.w[0] = atof(getParam("Gather_x").c_str());
  gathering_waypoint_.w[1] = atof(getParam("Gather_y").c_str()); 
  healthy_notification_time = atof(getParam("Healthy_notification_time").c_str());  
  healthy_decision_time = atof(getParam("Healthy_decision_time").c_str());
  distance_x = atof(getParam("Distance_x").c_str());
  distance_y = atof(getParam("Distance_y").c_str());
  high_base = atof(getParam("High_base").c_str());
  broadcast_fre = atof(getParam("Broadcast_fre").c_str());

  ROS_INFO("robot_id:%d", ID_);
  ROS_INFO("high_c_delta:%g, Va_delta:%g", high_c_delta, Va_delta);  
  ROS_INFO("gathering_waypoint_X: %f %s, gathering_waypoint_Y: %f %s", gathering_waypoint_.w[0], gathering_waypoint_.w[1], getParam("Gather_x").c_str(), getParam("Gather_y").c_str());

  num_waypoints_line = num_waypoints - 1; /** +1 */
  num_waypoints_ = 0;
  num_waypoints_total_ = 0;
  healthy_robots_id_min = 1;
  t_set = 0;
  idex_count = 0;

  robot_state_init_ = false;
  flag_new_waypoint_callback = true;
  flag_manage_switch = true;             /** added flag */
  flag_manage_fillet_point = true;       /** added flag */
  flag_manage_line_point = true;         /** added flag */
  flag_now_healthy_infor = false;        /** added for healthy */
  flag_all_robots_healthy_infor = false; /** added for healthy */
  flag_obtain_next_robot_ID = false;     /** added for healthy */

  end_loiter_mission = false;
  flag_waypoints_sent = false;

  fil_state_ = fillet_state::STRAIGHT;
  dub_state_ = dubin_state::FIRST;

  healthy_robots_id.insert(std::pair<int, int>(ID_, ID_));

  delta_high_robot = high_c_delta * (float)(ID_);
  delta_x_robot = land_X_delta * (float)(ID_-1);

  /** read data */
  /** -800, -400, -50, 0, 15 */
  float wps[5*num_waypoints] =
  {
   gathering_waypoint_.w[0], gathering_waypoint_.w[1],  -50,   0,   20,
   -600,  -500,  -40,   0,   18,
   -550,  -100,  -40,   0,   15,
   -500,   0,    -40,   0,   15,
   -450,   0,    -40,   0,   15,
   -400,   0,   -40,    0,   12,
   -300,   0,   -40,    0,   10,
   -200,   0,   -30,    0,   8,
   -100,   0,   -20,    0,   8, 
    -50,   0,   -5,     0,   0,
    -10,   0,   -1,     0,   0,
     -5,   0,   -1,     0,   0,
     -2,   0,   -1,     0,   0,
     -1,   0,   -0.5,   0,   0,
   -0.5,   0,   -0.3,   0,   0,
   -0.2,   0,   -0.1,   0,   0,
      0,   0,    0,     0,   0,
  };

  float N, E, D, Chi, V;
  std::vector<std::vector<float>> all_wps;
  std::vector<float> wp;

  for (int i = 0; i < num_waypoints; i++)
  {
    wp.clear();
    N = wps[i * 5 + 0];
    E = wps[i * 5 + 1];
    D = wps[i * 5 + 2];
    Chi = wps[i * 5 + 3];
    V = wps[i * 5 + 4];

    wp.push_back(N);   /** North */
    wp.push_back(E);   /** Eastc */
    wp.push_back(D);   /** Down  */
    wp.push_back(Chi); /** Chi   */
    wp.push_back(V);   /** Va    */

    all_wps.push_back(wp);
   // ROS_WARN("n: %f, e: %f, d: %f, Va_d: %f", all_wps[i][0],  all_wps[i][1],  all_wps[i][2], all_wps[i][4]);
  }

  /** vector assignment */ 
  struct waypoint_s new_waypoint;
  for (int i = 0; i < all_wps.size(); i++)
  {
    new_waypoint.w[0] = all_wps[i][0] - delta_x_robot; /** for landing */
    new_waypoint.w[1] = all_wps[i][1];
    new_waypoint.w[2] = all_wps[i][2];
    new_waypoint.chi_d = all_wps[i][3] * M_PI / 180; 
    new_waypoint.Va_d = all_wps[i][4];              
    new_waypoint.chi_valid = true;

    // if (int ii = 1; ii < 3; ii++)
    // {
    //   new_waypoint.w[2] = all_wps[ii][2] - delta_high_robot;
    // }

    /** for loiter_point */
    if (i == 0)
    {
      new_waypoint.loiter_point = true; /** loiter_point */
      new_waypoint.w[0] = all_wps[i][0];
      new_waypoint.w[1] = all_wps[i][1];
      new_waypoint.w[2] = all_wps[i][2] - delta_high_robot;  
      new_waypoint.Va_d = all_wps[i][4] - Va_delta * (float)(ID_ - 1);
    }
    else
    {
      new_waypoint.loiter_point = false;
    }

    /** for landing */
    if (i >= all_wps.size() - 2)
    {
      new_waypoint.landing = true;
    }
    else
    {
      new_waypoint.landing = false;
    }

    ROS_INFO("robot_id:%d", ID_);     
    ROS_INFO("new_waypoint.w[0]:%f, new_waypoint.w[1]:%f", new_waypoint.w[0],new_waypoint.w[1]);

    if (i == 0)
    {
      new_waypoint.set_current = true;
    }
    else
    {
      new_waypoint.set_current = false;
    }

    new_waypoint.clear_wp_list = false;

    new_all_waypoints.push_back(new_waypoint);
  }

  vehicle_state_sub_ = pluginSubscribe("truth", 10, &LandingPathManagerBasePlugin::vehicle_state_callback, this);
  current_path_pub_ = pluginAdvertise<rosplane_msgs::Current_Path>("current_path", 10);

  /** funtion 1 is used for healthy robots; funtion 2 is used for end_loiter_commands. */
  end_loiter_commands_pub_1 = pluginSubscribe<rosplane_msgs::Land_Command>("/land_commands_f", 10, &LandingPathManagerBasePlugin::landing_callback, this);  
  end_loiter_commands_pub_2 = pluginAdvertise<rosplane_msgs::Land_Command>("/land_commands", 10); 

  while (true)
  {
    GOON_OR_RETURN;
    current_path_publish();
    usleep(10000);

    if (robot_state_init_ == true && flag_new_waypoint_callback == true)
    {
      new_waypoint_callback();
      flag_new_waypoint_callback = false;
    }
  }
}

/** Callback function for end loiter command */
void LandingPathManagerBasePlugin::landing_callback(const rosplane_msgs::Land_CommandConstPtr &msg)
{ 
  /** 1. used for health information */
  if (flag_now_healthy_infor == true)
  {
    ROS_INFO("---------2----------");
    if (msg->tag == 1)
    {
      int robot_id = msg->ID_value;
      ROS_INFO("flag_now_healthy_infor = true; robot_id:%d", robot_id);

      bool bRec = healthy_robots_id.count(robot_id);
      if (bRec == false)
      {
        healthy_robots_id.insert(std::pair<int, int>(robot_id, robot_id)); /** default sort by key value */
      }

      std::map<int, int>::iterator iter = healthy_robots_id.find(robot_id);
      if (iter == healthy_robots_id.end())
      {
        healthy_robots_id.insert(std::pair<int, int>(robot_id, robot_id));
      }
    }
  }

  /** 2. no longer obtain health information of robots */
  if ((flag_all_robots_healthy_infor == true) && (flag_obtain_next_robot_ID == false))
  {
      ROS_INFO("--------3---------");
    std::map<int, int>::iterator iter;
    for (iter = healthy_robots_id.begin(); iter != healthy_robots_id.end(); iter++)
    {
      healthy_robots.push_back(iter->first); /** obtain healthy robots */
      ROS_WARN("healthy_robots, iter->first, %d", iter->first);
    }

    iter = healthy_robots_id.begin();
    healthy_robots_id_min = iter->first; /** minimum ID value of healthy robots */
    ROS_INFO("healthy_robots_id_min = %d", healthy_robots_id_min);

    if ((ID_ == healthy_robots_id_min) || (ID_ == 1))
    {
      ROS_INFO("---------4----------");
      end_loiter_mission = true; /** minimum ID healthy robots stop loiter mission */
    }
    else
    {   
      end_loiter_mission = false;
    }

    flag_obtain_next_robot_ID = true; /** obtain next robot_ID is OPEN*/
  }

  /** 3. used for end loiter and to landing */
  if (msg->ID_value == ID_ && msg->tag == 2)
  {
    end_loiter_mission = true;
  }
}

/** Callback function for vehicle state */
void LandingPathManagerBasePlugin::vehicle_state_callback(const rosplane_msgs::StateConstPtr &msg)
{
  vehicle_state_ = *msg;
  robot_state_init_ = true;
}

/** Callback function for obtain the new waypoint */
void LandingPathManagerBasePlugin::new_waypoint_callback()
{
  if (robot_state_init_) 
  {
    for (int i = 0; i < new_all_waypoints.size(); i++)
    {
      if (new_all_waypoints[i].set_current || num_waypoints_ == 0)
      {
        waypoint_s currentwp;
        currentwp.w[0] = vehicle_state_.position[0];
        currentwp.w[1] = vehicle_state_.position[1];
        currentwp.w[2] = (vehicle_state_.position[2] > -25 ? new_all_waypoints[i].w[2] : vehicle_state_.position[2]);
        currentwp.Va_d = new_all_waypoints[i].Va_d;
        currentwp.chi_d = vehicle_state_.chi;             
        currentwp.chi_valid = new_all_waypoints[i].chi_valid; 
        currentwp.landing = false;                        /** used for landing */
        currentwp.loiter_point = false;                   /** used for loiter */

        waypoints_.clear();
        waypoints_.push_back(currentwp);

        num_waypoints_ = 1;
        idx_a_ = 0;
        ROS_WARN("[vehicle_state_callback]:vehicle_state_.n[0]:%f,vehicle_state_.e[1]:%f,vehicle_state_.g[2]:%f", vehicle_state_.position[0], vehicle_state_.position[1], vehicle_state_.position[2]); 
      }

      waypoint_s nextwp;
      nextwp.w[0] = new_all_waypoints[i].w[0];
      nextwp.w[1] = new_all_waypoints[i].w[1];
      nextwp.w[2] = new_all_waypoints[i].w[2];
      nextwp.chi_d = new_all_waypoints[i].chi_d;
      nextwp.chi_valid = new_all_waypoints[i].chi_valid;
      nextwp.Va_d = new_all_waypoints[i].Va_d;
      nextwp.landing = new_all_waypoints[i].landing;
      nextwp.loiter_point = new_all_waypoints[i].loiter_point;
      nextwp.set_current = new_all_waypoints[i].set_current;
      nextwp.clear_wp_list = new_all_waypoints[i].clear_wp_list;

      /** ROS_WARN("[landing_path_manager_base_plugin_sim.cpp]:"); */
      //ROS_WARN("recieved waypoint: n: %f, e: %f, d: %f, Va_d: %f", nextwp.w[0], nextwp.w[1], nextwp.w[2], nextwp.Va_d);

      if (nextwp.landing)
      {
        ROS_WARN("landing = true");
      }
      else
      {
        ROS_WARN("landing = false");
      }

      if (nextwp.loiter_point)
      {
        ROS_WARN("loiter_point = true");
      }
      else
      {
        ROS_WARN("loiter_point = false");
      }

      waypoints_.push_back(nextwp);

      num_waypoints_++;

      if (new_all_waypoints[i].clear_wp_list == true)
      {
        waypoints_.clear();
        num_waypoints_ = 0;
        idx_a_ = 0;
      }

      if (num_waypoints_ != waypoints_.size())
      {
        ROS_FATAL("incorrect number of waypoints");
      }
    }

    ROS_WARN("Waypoints succesfully sent");
    
    flag_waypoints_sent = true;

    waypoints_total_.assign(waypoints_.begin(), waypoints_.end());                          /** vector assignment */
    num_waypoints_total_ = num_waypoints_;                                                  
    num_no_line = num_waypoints_total_ - num_waypoints_line;                                /** no manager_line  */
    waypoints1_.assign(waypoints_total_.begin(), waypoints_total_.begin() + num_no_line);   /** vector assignment */
    waypoints2_.assign(waypoints_total_.begin() + num_no_line + 1, waypoints_total_.end()); /** vector assignment */
    /** ROS_WARN("num_waypoints_total_:%d;num_:%d", num_waypoints_total_, num);   */
    /** ROS_WARN("waypoints0_[1].w[0]: %f, i waypoints0_[1].w[1]: %f", waypoints0_[1].w[0], waypoints0_[1].w[1]); */
  }
}

/** function for current path publish */
void LandingPathManagerBasePlugin::current_path_publish()
{ 
  struct input_s input;
  input.pn = vehicle_state_.position[0];
  input.pe = vehicle_state_.position[1];
  input.h = -vehicle_state_.position[2];
  input.chi = vehicle_state_.chi;

  t_set++;  
  //ROS_INFO("t_set = %d", t_set);

  /** pub and sub */
  if (t_set <= healthy_notification_time)
  {  
    flag_now_healthy_infor = true;
  }
  else
  {  
    flag_now_healthy_infor = false;
  }

  /** sub all */
  if ((healthy_notification_time < t_set) && (t_set < (healthy_notification_time + healthy_decision_time)))
  {
    flag_all_robots_healthy_infor = true;
  }
  else
  {
    flag_all_robots_healthy_infor = false;
  }

  // 频率降低10倍
  if (t_set % broadcast_fre == 0)
  {
    /** for health information */
    if ((threshold_of_health < input.h) && (flag_now_healthy_infor == true))
    {
      rosplane_msgs::Land_Command health_information;
      health_information.ID_value = ID_;
      health_information.tag = 1;
      end_loiter_commands_pub_2.publish(health_information); /** ID_ */
      ROS_WARN("robot %d: advertise information robot_ID is healthy", ID_);
    }
 

    /** advertise the next robot ID_ end loiter and go to landing */
    if (flag_obtain_next_robot_ID == true)
    {
      //if ((distance_x <= abs(input.pe)) && (abs(input.pe) <= distance_y) && ( delta_high_robot < input.h) && (input.h < high_base + delta_high_robot )) //50+
      if ((distance_x <= abs(input.pe)) && (abs(input.pe) <= distance_y)) //
      {
        /** Delete duplicate elements in vector*/
        // sort(healthy_robots.begin(), healthy_robots.end()); 
        // std::vector<int>::iterator ite = unique(healthy_robots.begin(), healthy_robots.end());
        // healthy_robots.erase(ite, healthy_robots.end());

        for (int i = 0; i < healthy_robots.size(); i++)
        {
          ROS_WARN("healthy_robots i = %d", healthy_robots.at(i)); //[2,3,4,5]
        }

        /** publish end loiter command*/
        std::vector<int>::iterator iter = find(healthy_robots.begin(), healthy_robots.end(), ID_); /** search idex number */
        int index_ID = -1;

        if ((iter++) != healthy_robots.end())
        {
          index_ID = *iter;
          rosplane_msgs::Land_Command endloiter_command;
          endloiter_command.ID_value = index_ID;
          endloiter_command.tag = 2;
          end_loiter_commands_pub_2.publish(endloiter_command); /** publish ID+1 or ID+2...*/
          ROS_WARN("[1]robot %d: advertise the next robot to landing; the next landing robots_ID is:%d", ID_, index_ID);

          if (index_ID != (ID_ + 1))
          {
            for (int i = (ID_ + 1); i <= index_ID; i++)
            {
              rosplane_msgs::Land_Command endloiter_command2;
              endloiter_command2.ID_value = i;
              endloiter_command2.tag = 2;                            
              end_loiter_commands_pub_2.publish(endloiter_command2); /** ID+1 */
              ROS_WARN("[2]robot %d: advertise the next robot to landing; the next landing robots_ID is:%d", ID_, index_ID);
            }
          }
        }

        ROS_WARN("advertise the next robot ID end loiter and go to landing");
      }
    }
  }

  /** path management  */
  if (flag_waypoints_sent == true)
  {
    struct output_s output;
    if (robot_state_init_ == true)
    {
      manage(params_, input, output);
    }

    rosplane_msgs::Current_Path current_path;
    if (output.flag)
      current_path.path_type = current_path.LINE_PATH;
    else
      current_path.path_type = current_path.ORBIT_PATH;

    current_path.Va_d = output.Va_d;

    for (int i = 0; i < 3; i++)
    {
      current_path.r[i] = output.r[i];
      current_path.q[i] = output.q[i];
      current_path.c[i] = output.c[i];
    }
    current_path.rho = output.rho;
    current_path.lambda = output.lambda;
    current_path.landing = output.landing;
    current_path_pub_.publish(current_path);
  }
}

/** function for path management */
void LandingPathManagerBasePlugin::manage(const params_s &params, const input_s &input, output_s &output)
{
  if (num_waypoints_ < 2)
  {
    ROS_WARN_THROTTLE(4, "No waypoints received! Loitering about origin at 50m");
    output.flag = false;
    output.Va_d = 12;
    output.c[0] = 0.0f;
    output.c[1] = 0.0f;
    output.c[2] = -50.0f;
    output.rho = params.R_min;
    output.lambda = 1;
  }
  else
  {
    if (num_no_line <= 4) /** */
    {
      manage_line(params, input, output); /** manage_line */
      //ROS_WARN("Now is manage_line");
    }
    else
    {
      /** fillet points must be greater than 2 */
      if (idex_count == num_no_line - 2 && flag_manage_switch == true)
      {
        flag_manage_switch = false;
      }
      /** ROS_WARN("waypoints1_.size():%d,waypoints2_.size():%d,idex_count:%d", waypoints1_.size(), waypoints2_.size(), idex_count);*/

      if (flag_manage_switch == true) /** run only once */
      {
        if (flag_manage_fillet_point == true) /** run only once */
        {
          waypoints_.clear();                          /** clear container memory */
          waypoints_ = waypoints1_;                    /** vector assignment */
          waypoints1_.clear();                         /** clear container memory */
          std::vector<waypoint_s>().swap(waypoints1_); /** clear and empty container memory */
          num_waypoints_ = num_no_line;
          flag_manage_fillet_point = false;
        }
        manage_fillet(params, input, output); /** manage_fillet */
        /**  manage_dubins(params, input, output); */
        ROS_WARN("manage_fillet/dubins:num_waypoints_=%d", num_waypoints_);
        /** ROS_WARN("idex_count,%d; idx_a_:%d", idex_count, idx_a_); */
      }
      else
      {
        if (flag_manage_line_point == true) /** run only once */
        {
          waypoint_s waypoints2_0;
          waypoints2_0.w[0] = vehicle_state_.position[0];
          waypoints2_0.w[1] = vehicle_state_.position[1];
          waypoints2_0.w[2] = (vehicle_state_.position[2] > -25 ? waypoints2_[0].w[2] : vehicle_state_.position[2]);
          /** waypoints2_0.Va_d = vehicle_state_.Va; */
          waypoints2_0.Va_d = (vehicle_state_.Va > 1 ? 12 : vehicle_state_.Va);
          waypoints2_0.chi_d = vehicle_state_.chi; 
          waypoints2_0.chi_valid = true;           
          waypoints2_0.landing = false;
          waypoints2_.insert(waypoints2_.begin(), waypoints2_0); /** vector assignment */

          waypoints_.clear();                          /** clear container memory */
          waypoints_ = waypoints2_;                    /** vector assignment */
          waypoints2_.clear();                         /** clear container memory */
          std::vector<waypoint_s>().swap(waypoints2_); /** clear and empty container memory */
          idx_a_ = 0;
          num_waypoints_ = num_waypoints_line + 1;

          flag_manage_line_point = false;
        }
        manage_line(params, input, output);
        ROS_WARN("manage_line:num_waypoints_=%d", num_waypoints_);
        /** ROS_WARN("idex_count=%d;idx_a_=%d", idex_count, idx_a_); */
      }
    }
  }
}

/** function for path management line*/
void LandingPathManagerBasePlugin::manage_line(const params_s &params, const input_s &input, output_s &output)
{
  Eigen::Vector3f p;
  p << input.pn, input.pe, -input.h;

  //ROS_WARN("[manage_line()] num_waypo _=%d", num_waypoints_ ); 

  int idx_b;
  int idx_c;
  if (idx_a_ == num_waypoints_ - 1)
  {
    idx_b = num_waypoints_ - 1; 
    idx_c = num_waypoints_ - 1; 
  }
  else if (idx_a_ == num_waypoints_ - 2)
  {
    idx_b = num_waypoints_ - 1;
    idx_c = num_waypoints_ - 1; 
  }
  else
  {
    idx_b = idx_a_ + 1;
    idx_c = idx_b + 1;
  }

  if ((waypoints_[idx_b].loiter_point == true) && (end_loiter_mission == false))
  {
     /** ROS_WARN("robot %d: to loiter point",ID_); */
    output.flag = false; /** fly an orbit */
    output.Va_d = waypoints_[idx_b].Va_d;
    output.c[0] = waypoints_[idx_b].w[0];
    output.c[1] = waypoints_[idx_b].w[1];
    output.c[2] = waypoints_[idx_b].w[2];
    output.rho = loiter_radius_;
    output.lambda = -1;
    output.landing = false;
    return;
  }

  /** ROS_WARN("robot %d: to landing",ID_); */

/**
 for (int i=0;i<12;i++)
 {
  ROS_WARN("---i:%i",i);
  ROS_WARN("n: %f, e: %f, d: %f, Va_d: %f", waypoints_[i].w[0], waypoints_[i].w[1], waypoints_[i].w[2], waypoints_[i].Va_d);
  ROS_WARN("chi_d:%i", waypoints_[i].chi_d );
}
*/

  Eigen::Vector3f w_im1(waypoints_[idx_a_].w); /** Corresponds to W_i-1 */
  Eigen::Vector3f w_i(waypoints_[idx_b].w);    /** Corresponds to W_i */
  Eigen::Vector3f w_ip1(waypoints_[idx_c].w);  /** Corresponds to W_i+1 */

  output.flag = true;
  output.Va_d = waypoints_[idx_a_].Va_d;
  output.landing = waypoints_[idx_b].landing;

  output.r[0] = w_im1(0);
  output.r[1] = w_im1(1);
  output.r[2] = w_im1(2);

  Eigen::Vector3f q_im1 = (w_i - w_im1).normalized();
  Eigen::Vector3f q_i = (w_ip1 - w_i).normalized();

  output.q[0] = q_im1(0);
  output.q[1] = q_im1(1);
  output.q[2] = q_im1(2);

  Eigen::Vector3f n_i = (q_im1 + q_i).normalized();

  if ((p - w_i).dot(n_i) > 0.0f)
  {
    idx_a_++;
  }
}


/** function for path management manage_fillet*/
void LandingPathManagerBasePlugin::manage_fillet(const params_s &params, const input_s &input, output_s &output)
{

  Eigen::Vector3f p;
  p << input.pn, input.pe, -input.h;

  int idx_b;
  int idx_c;

  if (idx_a_ == num_waypoints_ - 3)
  {
    idx_b = num_waypoints_ - 2;
    idx_c = num_waypoints_ - 1; 
  }
  else
  {
    idx_b = idx_a_ + 1;
    idx_c = idx_b + 1;
  }

  idex_count = idx_a_ + 1; /** count */

  Eigen::Vector3f w_im1(waypoints_[idx_a_].w); /** Corresponds to W_i-1 */
  Eigen::Vector3f w_i(waypoints_[idx_b].w);    /** Corresponds to W_i */
  Eigen::Vector3f w_ip1(waypoints_[idx_c].w);  /** Corresponds to W_i+1 */

  float R_min = params.R_min;

  output.Va_d = waypoints_[idx_a_].Va_d;
  output.r[0] = w_im1(0);
  output.r[1] = w_im1(1);
  output.r[2] = w_im1(2); /** count */

  output.landing = waypoints_[idx_b].landing;

  Eigen::Vector3f q_im1 = (w_i - w_im1).normalized();
  Eigen::Vector3f q_i = (w_ip1 - w_i).normalized();
  float n_qim1_dot_qi = -q_im1.dot(q_i);
  n_qim1_dot_qi = -q_im1.dot(q_i) < -1.0f + 0.0001f ? -1.0f + 0.0001f : n_qim1_dot_qi; /** this prevents beta from being nan */
  n_qim1_dot_qi = -q_im1.dot(q_i) > 1.0f - 0.0001f ? 1.0f - 0.0001f : n_qim1_dot_qi;   /** Still allows for 0.8 - 179.2 degrees */
  float beta = acosf(n_qim1_dot_qi);

  Eigen::Vector3f z;
  switch (fil_state_)
  {

  case fillet_state::STRAIGHT: /** follows a straight line to the next waypoint */
      if (waypoints_[idx_c].loiter_point == true && end_loiter_mission == false)
    {
     ROS_WARN("robot %d: loiter point",ID_);
      output.flag = false; /** fly an orbit */
      output.Va_d = waypoints_[idx_c].Va_d;
      output.c[0] = waypoints_[idx_c].w[0];
      output.c[1] = waypoints_[idx_c].w[1];
      output.c[2] = waypoints_[idx_c].w[2];
      output.rho = loiter_radius_;
      output.lambda = -1;
      output.landing = false;
      return; /** loiter at this point */
    }    
    output.flag = true;
    output.q[0] = q_im1(0);
    output.q[1] = q_im1(1);
    output.q[2] = q_im1(2);
    output.c[0] = 1;
    output.c[1] = 1;
    output.c[2] = 1;
    output.rho = 1;
    output.lambda = 1;

    z = w_i - q_im1 * (R_min / tanf(beta / 2.0));

    if (waypoints_[idx_c].loiter_point == true && w_i == w_ip1)
    {
      z = w_i; /** for loiter_point */
    }

    if ((p - z).dot(q_im1) > 0)
      fil_state_ = fillet_state::ORBIT;
    break;

  case fillet_state::ORBIT:
    if (waypoints_[idx_c].loiter_point == true && end_loiter_mission == false)
    {
      output.flag = false; /** fly an orbit */
      output.Va_d = waypoints_[idx_c].Va_d;
      output.c[0] = waypoints_[idx_c].w[0];
      output.c[1] = waypoints_[idx_c].w[1];
      output.c[2] = waypoints_[idx_c].w[2];
      output.rho = loiter_radius_;
      output.lambda = -1;
      output.landing = false;
      return; /** loiter at this point */
    }

    NED_t w_im1_s(w_im1(0), w_im1(1), w_im1(2));
    NED_t w_i_s(w_i(0), w_i(1), w_i(2));
    NED_t w_ip1_s(w_ip1(0), w_ip1(1), w_ip1(2));

    fillet_s fil;
    fil.calculate(w_im1_s, w_i_s, w_ip1_s, R_min);

    Eigen::Vector3f c;
    c << fil.c.N, fil.c.E, fil.c.D;
    output.flag = false;
    output.q[0] = fil.q_i.N;
    output.q[1] = fil.q_i.E;
    output.q[2] = fil.q_i.D;
    output.c[0] = c(0);
    output.c[1] = c(1);
    output.c[2] = c(2);

    output.rho = R_min;
    output.lambda = fil.lambda;
    z << fil.z2.N, fil.z2.E, fil.z2.D;

    if ((p - z).dot(q_i) > 0)
    {
      idx_a_++;
      fil_state_ = fillet_state::STRAIGHT;
    }
    break;
  }
}

bool fillet_s::calculate(NED_t w_im1_in, NED_t w_i_in, NED_t w_ip1_in, float R_in)
{
  w_im1 = w_im1_in;
  w_i = w_i_in;
  w_ip1 = w_ip1_in;
  R = R_in;
  q_im1 = (w_i - w_im1).normalize();
  q_i = (w_ip1 - w_i).normalize();
  float n_qim1_dot_qi = -q_im1.dot(q_i);
  float tolerance = 0.0001f;
  n_qim1_dot_qi = n_qim1_dot_qi < -1.0f + tolerance ? -1.0f + tolerance : n_qim1_dot_qi;
  n_qim1_dot_qi = n_qim1_dot_qi > 1.0f - tolerance ? 1.0f - tolerance : n_qim1_dot_qi;
  float varrho = acosf(n_qim1_dot_qi);
  z1 = w_i - q_im1 * (R / tanf(varrho / 2.0f));
  z2 = w_i + q_i * (R / tanf(varrho / 2.0f));
  z1.D = w_i.D;
  z2.D = w_i.D;
  c = w_i - ((q_im1 - q_i).normalize()) * (R / sinf(varrho / 2.0f));
  c.D = w_i.D;
  lambda = q_im1.N * q_i.E - q_im1.E * q_i.N > 0.0f ? 1 : -1;                             /** 1 = cw; -1 = ccw */
  adj = 2.0f * R / tanf(varrho / 2.0f) - 2.0f * asinf((z2 - z1).norm() / (2.0f * R)) * R; /** adjustment length */
  if ((w_i - c).norm() < R)
  {
    NED_t q_rotated;
    float rot = M_PI / 2.0f;
    if (lambda == -1)
      rot = -M_PI / 2.0f;
    q_rotated.N = q_i.N * cosf(rot) - q_i.E * sinf(rot);
    q_rotated.E = q_i.N * sinf(rot) + q_i.E * cosf(rot);
    q_rotated.D = q_i.D;
    c = w_i + q_rotated * (R / sinf(varrho / 2.0f));
  }

  if (q_im1.dot(z1 - w_im1) > 0.0f && (q_i * -1.0f).dot(z2 - w_ip1) > 0.0f)
    return true;
  else
    return false;
}

/** function for path management manage_dubins*/
void LandingPathManagerBasePlugin::manage_dubins(const params_s &params, const input_s &input, output_s &output)
{
  Eigen::Vector3f p;
  p << input.pn, input.pe, -input.h;

  output.Va_d = waypoints_[idx_a_].Va_d;
  output.landing = waypoints_[idx_a_].landing;
  /** output.loiter_point = waypoints_[idx_a_].loiter_point; */
  output.r[0] = 0;
  output.r[1] = 0;
  output.r[2] = 0;
  output.q[0] = 0;
  output.q[1] = 0;
  output.q[2] = 0;
  output.c[0] = 0;
  output.c[1] = 0;
  output.c[2] = 0;

  switch (dub_state_)
  {
  case dubin_state::FIRST:
    dubinsParameters(waypoints_[0], waypoints_[1], params.R_min);
    output.flag = false;
    output.c[0] = dubinspath_.cs(0);
    output.c[1] = dubinspath_.cs(1);
    output.c[2] = dubinspath_.cs(2);
    output.rho = dubinspath_.R;
    output.lambda = dubinspath_.lams;
    if ((p - dubinspath_.w1).dot(dubinspath_.q1) >= 0) /** start in H1*/
    {
      dub_state_ = dubin_state::BEFORE_H1_WRONG_SIDE;
    }
    else
    {
      dub_state_ = dubin_state::BEFORE_H1;
    }
    break;
  case dubin_state::BEFORE_H1:
    output.flag = false;
    output.c[0] = dubinspath_.cs(0);
    output.c[1] = dubinspath_.cs(1);
    output.c[2] = dubinspath_.cs(2);
    output.rho = dubinspath_.R;
    output.lambda = dubinspath_.lams;
    if ((p - dubinspath_.w1).dot(dubinspath_.q1) >= 0) /** entering H1 */
    {
      dub_state_ = dubin_state::STRAIGHT;
    }
    break;
  case dubin_state::BEFORE_H1_WRONG_SIDE:
    output.flag = false;
    output.c[0] = dubinspath_.cs(0);
    output.c[1] = dubinspath_.cs(1);
    output.c[2] = dubinspath_.cs(2);
    output.rho = dubinspath_.R;
    output.lambda = dubinspath_.lams;
    if ((p - dubinspath_.w1).dot(dubinspath_.q1) < 0) /** exit H1 */
    {
      dub_state_ = dubin_state::BEFORE_H1;
    }
    break;
  case dubin_state::STRAIGHT:
    output.flag = true;
    output.r[0] = dubinspath_.w1(0);
    output.r[1] = dubinspath_.w1(1);
    output.r[2] = dubinspath_.w1(2);
    /** output.r[0] = dubinspath_.z1(0); */
    /** output.r[1] = dubinspath_.z1(1); */
    /** output.r[2] = dubinspath_.z1(2); */
    output.q[0] = dubinspath_.q1(0);
    output.q[1] = dubinspath_.q1(1);
    output.q[2] = dubinspath_.q1(2);
    output.rho = 1;
    output.lambda = 1;
    if ((p - dubinspath_.w2).dot(dubinspath_.q1) >= 0) /** entering H2 */
    {
      if ((p - dubinspath_.w3).dot(dubinspath_.q3) >= 0) /** start in H3 */
      {
        dub_state_ = dubin_state::BEFORE_H3_WRONG_SIDE;
      }
      else
      {
        dub_state_ = dubin_state::BEFORE_H3;
      }
    }
    break;
  case dubin_state::BEFORE_H3:
    output.flag = false;
    output.c[0] = dubinspath_.ce(0);
    output.c[1] = dubinspath_.ce(1);
    output.c[2] = dubinspath_.ce(2);
    output.rho = dubinspath_.R;
    output.lambda = dubinspath_.lame;
    if ((p - dubinspath_.w3).dot(dubinspath_.q3) >= 0) /** entering H3 */
    {

      if (waypoints_[idx_a_].loiter_point == true && end_loiter_mission == false)
      {
        dub_state_ = dubin_state::BEFORE_H3_WRONG_SIDE; /** for loiter_point */
      }

      /** increase the waypoint pointer */
      int idx_b;
      idx_a_++;
      idx_b = idx_a_ + 1;
      /** ROS_WARN("manage_dubins():idx_a_:%d", idx_a_); */

      /** plan new Dubin's path to next waypoint configuration */
      dubinsParameters(waypoints_[idx_a_], waypoints_[idx_b], params.R_min);

      /** start new path */
      if ((p - dubinspath_.w1).dot(dubinspath_.q1) >= 0) /** start in H1 */
      {
        dub_state_ = dubin_state::BEFORE_H1_WRONG_SIDE;
      }
      else
      {
        dub_state_ = dubin_state::BEFORE_H1;
      }
    }
    break;
  case dubin_state::BEFORE_H3_WRONG_SIDE:
    output.flag = false;
    output.c[0] = dubinspath_.ce(0);
    output.c[1] = dubinspath_.ce(1);
    output.c[2] = dubinspath_.ce(2);
    output.rho = dubinspath_.R;
    output.lambda = dubinspath_.lame;
    if ((p - dubinspath_.w3).dot(dubinspath_.q3) < 0) /** exit H3 */
    {
      dub_state_ = dubin_state::BEFORE_H1;
    }
    break;
  }
  idex_count = idx_a_ + 1; /** count */
}

Eigen::Matrix3f LandingPathManagerBasePlugin::rotz(float theta)
{
  Eigen::Matrix3f R;
  R << cosf(theta), -sinf(theta), 0,
      sinf(theta), cosf(theta), 0,
      0, 0, 1;

  return R;
}

float LandingPathManagerBasePlugin::mo(float in)
{
  float val;
  if (in > 0)
    val = fmod(in, 2.0 * M_PI_F);
  else
  {
    float n = floorf(in / 2.0 / M_PI_F);
    val = in - n * 2.0 * M_PI_F;
  }
  return val;
}

/** function for dubinsParameters */
void LandingPathManagerBasePlugin::dubinsParameters(const waypoint_s start_node, const waypoint_s end_node, float R)
{
  float ell = sqrtf((start_node.w[0] - end_node.w[0]) * (start_node.w[0] - end_node.w[0]) +
                    (start_node.w[1] - end_node.w[1]) * (start_node.w[1] - end_node.w[1]));
  if (ell < 2.0 * R)
  {
    ROS_ERROR("The distance between nodes must be larger than 2R.");
  }
  else
  {
    dubinspath_.ps(0) = start_node.w[0];
    dubinspath_.ps(1) = start_node.w[1];
    dubinspath_.ps(2) = start_node.w[2];
    dubinspath_.chis = start_node.chi_d;
    dubinspath_.pe(0) = end_node.w[0];
    dubinspath_.pe(1) = end_node.w[1];
    dubinspath_.pe(2) = end_node.w[2];
    dubinspath_.chie = end_node.chi_d;

    Eigen::Vector3f crs = dubinspath_.ps;
    crs(0) += R * (cosf(M_PI_2_F) * cosf(dubinspath_.chis) - sinf(M_PI_2_F) * sinf(dubinspath_.chis));
    crs(1) += R * (sinf(M_PI_2_F) * cosf(dubinspath_.chis) + cosf(M_PI_2_F) * sinf(dubinspath_.chis));
    Eigen::Vector3f cls = dubinspath_.ps;
    cls(0) += R * (cosf(-M_PI_2_F) * cosf(dubinspath_.chis) - sinf(-M_PI_2_F) * sinf(dubinspath_.chis));
    cls(1) += R * (sinf(-M_PI_2_F) * cosf(dubinspath_.chis) + cosf(-M_PI_2_F) * sinf(dubinspath_.chis));
    Eigen::Vector3f cre = dubinspath_.pe;
    cre(0) += R * (cosf(M_PI_2_F) * cosf(dubinspath_.chie) - sinf(M_PI_2_F) * sinf(dubinspath_.chie));
    cre(1) += R * (sinf(M_PI_2_F) * cosf(dubinspath_.chie) + cosf(M_PI_2_F) * sinf(dubinspath_.chie));
    Eigen::Vector3f cle = dubinspath_.pe;
    cle(0) += R * (cosf(-M_PI_2_F) * cosf(dubinspath_.chie) - sinf(-M_PI_2_F) * sinf(dubinspath_.chie));
    cle(1) += R * (sinf(-M_PI_2_F) * cosf(dubinspath_.chie) + cosf(-M_PI_2_F) * sinf(dubinspath_.chie));

    float theta, theta2;
    /** compute L1 */
    theta = atan2f(cre(1) - crs(1), cre(0) - crs(0));
    float L1 = (crs - cre).norm() + R * mo(2.0 * M_PI_F + mo(theta - M_PI_2_F) - mo(dubinspath_.chis - M_PI_2_F)) + R * mo(2.0 * M_PI_F + mo(dubinspath_.chie - M_PI_2_F) - mo(theta - M_PI_2_F));

    /** compute L2 */
    ell = (cle - crs).norm();
    theta = atan2f(cle(1) - crs(1), cle(0) - crs(0));
    float L2;
    if (2.0 * R > ell)
      L2 = 9999.0f;
    else
    {
      theta2 = theta - M_PI_2_F + asinf(2.0 * R / ell);
      L2 = sqrtf(ell * ell - 4.0 * R * R) + R * mo(2.0 * M_PI_F + mo(theta2) - mo(dubinspath_.chis - M_PI_2_F)) + R * mo(2.0 * M_PI_F + mo(theta2 + M_PI_F) - mo(dubinspath_.chie + M_PI_2_F));
    }

    /** compute L3 */
    ell = (cre - cls).norm();
    theta = atan2f(cre(1) - cls(1), cre(0) - cls(0));
    float L3;
    if (2.0 * R > ell)
      L3 = 9999.0f;
    else
    {
      theta2 = acosf(2.0 * R / ell);
      L3 = sqrtf(ell * ell - 4 * R * R) + R * mo(2.0 * M_PI_F + mo(dubinspath_.chis + M_PI_2_F) - mo(theta + theta2)) + R * mo(2.0 * M_PI_F + mo(dubinspath_.chie - M_PI_2_F) - mo(theta + theta2 - M_PI_F));
    }

    /** compute L4 */
    theta = atan2f(cle(1) - cls(1), cle(0) - cls(0));
    float L4 = (cls - cle).norm() + R * mo(2.0 * M_PI_F + mo(dubinspath_.chis + M_PI_2_F) - mo(theta + M_PI_2_F)) + R * mo(2.0 * M_PI_F + mo(theta + M_PI_2_F) - mo(dubinspath_.chie + M_PI_2_F));

    /** L is the minimum distance */
    int idx = 1;
    dubinspath_.L = L1;
    if (L2 < dubinspath_.L)
    {
      dubinspath_.L = L2;
      idx = 2;
    }
    if (L3 < dubinspath_.L)
    {
      dubinspath_.L = L3;
      idx = 3;
    }
    if (L4 < dubinspath_.L)
    {
      dubinspath_.L = L4;
      idx = 4;
    }

    Eigen::Vector3f e1;
    /* e1.zero(); */
    e1(0) = 1;
    e1(1) = 0;
    e1(2) = 0;
    switch (idx)
    {
    case 1:
      dubinspath_.cs = crs;
      dubinspath_.lams = 1;
      dubinspath_.ce = cre;
      dubinspath_.lame = 1;
      dubinspath_.q1 = (cre - crs).normalized();
      dubinspath_.w1 = dubinspath_.cs + (rotz(-M_PI_2_F) * dubinspath_.q1) * R;
      dubinspath_.w2 = dubinspath_.ce + (rotz(-M_PI_2_F) * dubinspath_.q1) * R;
      break;
    case 2:
      dubinspath_.cs = crs;
      dubinspath_.lams = 1;
      dubinspath_.ce = cle;
      dubinspath_.lame = -1;
      ell = (cle - crs).norm();
      theta = atan2f(cle(1) - crs(1), cle(0) - crs(0));
      theta2 = theta - M_PI_2_F + asinf(2.0 * R / ell);
      dubinspath_.q1 = rotz(theta2 + M_PI_2_F) * e1;
      dubinspath_.w1 = dubinspath_.cs + (rotz(theta2) * e1) * R;
      dubinspath_.w2 = dubinspath_.ce + (rotz(theta2 + M_PI_F) * e1) * R;
      break;
    case 3:
      dubinspath_.cs = cls;
      dubinspath_.lams = -1;
      dubinspath_.ce = cre;
      dubinspath_.lame = 1;
      ell = (cre - cls).norm();
      theta = atan2f(cre(1) - cls(1), cre(0) - cls(0));
      theta2 = acosf(2.0 * R / ell);
      dubinspath_.q1 = rotz(theta + theta2 - M_PI_2_F) * e1;
      dubinspath_.w1 = dubinspath_.cs + (rotz(theta + theta2) * e1) * R;
      dubinspath_.w2 = dubinspath_.ce + (rotz(theta + theta2 - M_PI_F) * e1) * R;
      break;
    case 4:
      dubinspath_.cs = cls;
      dubinspath_.lams = -1;
      dubinspath_.ce = cle;
      dubinspath_.lame = -1;
      dubinspath_.q1 = (cle - cls).normalized();
      dubinspath_.w1 = dubinspath_.cs + (rotz(M_PI_2_F) * dubinspath_.q1) * R;
      dubinspath_.w2 = dubinspath_.ce + (rotz(M_PI_2_F) * dubinspath_.q1) * R;
      break;
    }
    dubinspath_.w3 = dubinspath_.pe;
    dubinspath_.q3 = rotz(dubinspath_.chie) * e1;
    dubinspath_.R = R;
  }
}

} /** namespace general_bus */