/* ==================================================================
* Copyright (c) 2018, micROS Group, TAIIC, NIIDT & HPCL.
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions
* are met:
*
* 1. Redistributions of source code must retain the above copyright
* notice, this list of conditions and the following disclaimer.
* 2. Redistributions in binary form must reproduce the above copyright
* notice, this list of conditions and the following disclaimer in the
* documentation and/or other materials provided with the
* distribution.
* 3. All advertising materials mentioning features or use of this software
* must display the following acknowledgement:
* This product includes software developed by the micROS Group. and
* its contributors.
* 4. Neither the name of the Group nor the names of its contributors may
* be used to endorse or promote products derived from this software
* without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY MICROS,GROUP AND CONTRIBUTORS
* ''AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT
* NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
* FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
* THE MICROS,GROUP OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
* OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
* HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
* OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
* IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
* ===================================================================
* Author: zhaolin, micROS Group
*/


#ifndef _LNADING_PATH_MANAGER_BASE_
#define _LANDING_PATH_MANAGER_BASE_

#include "general_plugin/general_plugin.h"
#include <ros/ros.h>
#include <rosplane_msgs/State.h>
#include <rosplane_msgs/Current_Path.h>
#include <rosplane_msgs/Waypoint.h>
#include <math.h>
#include <Eigen/Eigen>
#include <cmath>
#include <rosplane_msgs/Land_Command.h>

#define M_PI_F 3.14159265358979323846f
#define M_PI_2_F 1.57079632679489661923f

#define num_waypoints 17

namespace general_bus
{

enum class fillet_state
{
  STRAIGHT,
  ORBIT
};

enum class dubin_state
{
  FIRST,
  BEFORE_H1,
  BEFORE_H1_WRONG_SIDE,
  STRAIGHT,
  BEFORE_H3,
  BEFORE_H3_WRONG_SIDE
};

struct NED_t
{
  double N; /** North (m) */
  double E; /** East  (m) */
  double D; /** Down  (m) */
  NED_t()
  {
    N = 0.0f;
    E = 0.0f;
    D = 0.0f;
  }
  NED_t(double N_in, double E_in, double D_in)
  {
    N = N_in;
    E = E_in;
    D = D_in;
  }
  bool operator==(const NED_t s)
  {
    return N == s.N && E == s.E && D == s.D;
  }
  bool operator!=(const NED_t s)
  {
    return N != s.N || E != s.E || D != s.D;
  }
  NED_t operator+(const NED_t s)
  {
    NED_t n;
    n.N = N + s.N;
    n.E = E + s.E;
    n.D = D + s.D;
    return n;
  }
  NED_t operator-(const NED_t s)
  {
    NED_t n;
    n.N = N - s.N;
    n.E = E - s.E;
    n.D = D - s.D;
    return n;
  }
  double norm()
  {
    return sqrt(N * N + E * E + D * D);
  }
  NED_t normalize()
  {
    NED_t out;
    double magnitude = norm();
    if (magnitude <= 1.0e-9f)
      return out; /** 0, 0, 0 */
    out.N = N / magnitude;
    out.E = E / magnitude;
    out.D = D / magnitude;
    return out;
  }
  double dot(NED_t in)
  {
    return N * in.N + E * in.E + D * in.D;
  }
  double getChi()
  {
    return atan2(E, N);
  }
  NED_t operator*(const double num)
  {
    NED_t n;
    n.N = N * num;
    n.E = E * num;
    n.D = D * num;
    return n;
  }
};

struct fillet_s
{
  fillet_s()
  {
    lambda = 0;
    R = 0.0f;
    adj = 0.0f;
  }
  NED_t w_im1;
  NED_t w_i;
  NED_t w_ip1;
  NED_t z1;
  NED_t z2;
  NED_t c;
  NED_t q_im1;
  NED_t q_i;
  int lambda;
  float R;
  float adj;
  bool calculate(NED_t w_im1_in, NED_t w_i_in, NED_t w_ip1_in, float R_in);
};

class LandingPathManagerBasePlugin : public GeneralPlugin
{

public:
  virtual void start();

protected:
  fillet_state fil_state_;

  struct waypoint_s
  {
    float w[3];
    float chi_d;
    float Va_d;
    bool chi_valid;
    bool landing;
    bool loiter_point;
    bool set_current;
    bool clear_wp_list;
  };

  waypoint_s gathering_waypoint_;
  std::vector<waypoint_s> waypoints_;
  std::vector<waypoint_s> waypoints_total_;
  std::vector<waypoint_s> waypoints1_;
  std::vector<waypoint_s> waypoints2_;
  std::vector<waypoint_s> new_all_waypoints;

  int healthy_decision_time;
  int healthy_notification_time;
  float distance_x;
  float distance_y;
  float distance_z;

  struct input_s
  {
    float pn;  /** position north */
    float pe;  /** position east */
    float h;   /** altitude */
    float chi; /** course angle */
    float Va;
  };

  struct output_s
  {
    bool flag;     /** Inicates strait line or orbital path (true is line, false is orbit) */
    float Va_d;    /** Desired airspeed (m/s) */
    float r[3];    /** Vector to origin of straight line path (m) */
    float q[3];    /** Unit vector, desired direction of travel for line path */
    float c[3];    /** Center of orbital path (m) */
    float rho;     /** Radius of orbital path (m) */
    int8_t lambda; /** Direction of orbital path (cw is 1, ccw is -1) */
    bool landing;  /** True if we want to land */
  };

  struct params_s
  {
    double R_min;
  };

  void manage(const struct params_s &params, const struct input_s &input, struct output_s &output);

  int idx_a_; /** index to the waypoint that was most recently achieved */

  std::vector<waypoint_s> old_waypoints_;
  int old_num_waypoints_;
  int old_idx_a_; /** index to the waypoint that was mactuator_clish()*/

  int num_waypoints_;
  int num_waypoints_line;
  int num_no_line;
  int num_waypoints_total_;
  int idex_count;

private:
  ros::NodeHandle nh_;
  ros::NodeHandle nh_private_;
  ros::Subscriber vehicle_state_sub_; /** vehicle state subscription */
  ros::Subscriber new_waypoint_sub_;  /** new waypoint subscription */
  ros::Subscriber end_loiter_commands_pub_1;
  ros::Publisher current_path_pub_;         /** controller commands publication */
  ros::Publisher end_loiter_commands_pub_2; /** end_loiter */

  struct params_s params_;
  rosplane_msgs::State vehicle_state_; /** vehicle state */

  void vehicle_state_callback(const rosplane_msgs::StateConstPtr &msg);
  void landing_callback(const rosplane_msgs::Land_CommandConstPtr &msg);
  void current_path_publish();
  void new_waypoint_callback();

  void manage_line(const struct params_s &params, const struct input_s &input, struct output_s &output);
  void manage_fillet(const struct params_s &params, const struct input_s &input, struct output_s &output);
  void manage_dubins(const struct params_s &params, const struct input_s &input, struct output_s &output);
  dubin_state dub_state_;

  struct dubinspath_s
  {
    Eigen::Vector3f ps; /** the start position */
    float chis;         /** the start course angle */
    Eigen::Vector3f pe; /** the end position */
    float chie;         /** the end course angle */
    float R;            /** turn radius */
    float L;            /** length of the path */
    Eigen::Vector3f cs; /** center of the start circle */
    int lams;           /** direction of the start circle */
    Eigen::Vector3f ce; /** center of the endcircle */
    int lame;           /** direction of the end circle */
    Eigen::Vector3f w1; /** vector defining half plane H1 */
    Eigen::Vector3f q1; /** unit vector along striaght line path */
    Eigen::Vector3f w2; /** vector defining half plane H2 */
    Eigen::Vector3f w3; /** vector defining half plane H3 */
    Eigen::Vector3f q3; /** unit vector defining direction of half plane H3 */
  };
  struct dubinspath_s dubinspath_;
  void dubinsParameters(const struct waypoint_s start_node, const struct waypoint_s end_node, float R);
  Eigen::Matrix3f rotz(float theta);
  float mo(float in);

  float loiter_radius_; /** for loiter_point */

  bool robot_state_init_;
  bool end_loiter_mission;     /** for loiter_point */

  bool flag_pub_healthy_infor; /** each robots first publish healthy information */
  bool flag_new_waypoint_callback;
  bool flag_manage_switch;
  bool flag_manage_fillet_point;
  bool flag_manage_line_point;
  bool flag_now_healthy_infor;        /** added for healthy */
  bool flag_all_robots_healthy_infor; /** added for healthy */
  bool flag_obtain_next_robot_ID;     /** added for healthy */
  bool flag_waypoints_sent;
  bool flag_self_health;

 
  std::map<int, int> healthy_robots_id;
  std::vector<int> healthy_robots; /** total ID_ number of the healthy robots */

  int ID_;
  int t_set;
  int healthy_robots_id_min; /** the default minimum ID_ of the healthy robots */
  int broadcast_fre;

  float high_c_delta;
  float land_X_delta;
  float Va_delta;
  float threshold_of_health;
  float delta_high_robot;
  float high_base;
  float delta_x_robot;
 };

} // namespace general_bus

#endif