/* ==================================================================
* Copyright (c) 2018, micROS Group, TAIIC, NIIDT & HPCL.
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions
* are met:
*
* 1. Redistributions of source code must retain the above copyright
* notice, this list of conditions and the following disclaimer.
* 2. Redistributions in binary form must reproduce the above copyright
* notice, this list of conditions and the following disclaimer in the
* documentation and/or other materials provided with the
* distribution.
* 3. All advertising materials mentioning features or use of this software
* must display the following acknowledgement:
* This product includes software developed by the micROS Group. and
* its contributors.
* 4. Neither the name of the Group nor the names of its contributors may
* be used to endorse or promote products derived from this software
* without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY MICROS,GROUP AND CONTRIBUTORS
* ''AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT
* NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
* FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
* THE MICROS,GROUP OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
* OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
* HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
* OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
* IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
* ===================================================================
* Author: Ren Xiaoguang, Wu Yunlong, Li Jinghua, micROS-DA Team, TAIIC.
*/


#ifndef LEADER_LGVF_PLUGIN_SIM
#define LEADER_LGVF_PLUGIN_SIM

#include "general_plugin/general_plugin.h"

#include <vector>
#include <string>
#include <math.h>
#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <termios.h>
#include <ros/ros.h>
#include <nav_msgs/Path.h>
#include <std_msgs/Empty.h>
#include <std_msgs/String.h>
#include <geometry_msgs/Quaternion.h>
#include <geometry_msgs/PoseStamped.h>
#include <rosplane_msgs/State.h>
#include <rosplane_msgs/Controller_Commands.h>

using std::vector;
using std::string;

#define PRECISION  1e-5

/**
 * @brief General Bus Namespace
 * 
 */
namespace general_bus
{
    /* @brief saves point
     * elements: x   	position along x-axis
     *           y	    position along y-axis
     */
    typedef struct vertex
    {
        double x;
        double y;
    }vertex;

    /**
     * @brief Leader LGVF Plugin
     * 
     */
    class LeaderLgvfPluginSim: public GeneralPlugin
    {
        public:
            /**
             * @brief Overload start function of General Plugin
             */
            virtual void start(); 

            /**
             * @brief Own state callback function
             * 
             * @param[in] msg specifies own state information
             */
            void stateCallback(const rosplane_msgs::State::ConstPtr& msg);

            /**
             * @brief Calculate control commands to controller function
             */
            void calculateCommand();  

            /**
             * @brief Discriminate events to triger function
             */
            void discriminateEvents();  

            /**
             * @brief Visualize trajectory in rviz function
             */                
            void visualizeTrajectory();              
            
            /**
             * @brief Get the Regions object
             * 
             * @param vertices 
             * @param string_regions 
             */
            void getRegions(const std::string& vertices, std::vector<std::string> &string_regions);
      
            /**
             * @brief Covert ordered vertices of the region from string to vector
             * 
             * @param[in] vertices specifies ordered vertices of region int string type
             * @param[out] origin_region obtains ordered vertices of region in vector
             */
            void getAreaVertices(const string& vertices, vector<vertex>& origin_region);

            /**
             * @brief Find parallels of target line function
             * 
             * @param[in] d specifies the desired distance of parallels from target line
             * @param[in] p1 specifies the start point of target line
             * @param[in] p2 specifies the end point of target line
             * @param[out] parallel_lines obtains vertices of parallels 
             */
            void getParallels(const double& d, const vertex& p1, const vertex& p2, vector<vertex> &parallel_lines);

            /**
             * @brief Find the adjacent points function
             * 
             * @param[in] r_real specifies the distance between adjacent point and target point
             * @param[in] theta_in specifies the angle of the longest boundary (x axis)
             * @param[in] tmp specifies the position of target point
             * @param[out] adjacent obtains 6 adjacent points of target point
             */
            void getAdjacentPoints(const double& r_real, const double& theta_in, const vertex& tmp, vector<vertex>& adjacent);

            /**
             * @brief Calculate midpoints of single row region
             * 
             * @param[in] region specifies the ordered vertices of single row region
             * @param[out] midpoints obtains midpoints of single row region
             */
            void calculateMidpoints(const vector<vertex>& region, vector<vertex>& midpoints);

            /**
             * @brief Partition origional region into multirow region and single row regions
             * 
             * @param[in] r_d specifies the radius of trjectory
             * @param[in] region_o specifies original region
             * @param[out] region_m obtains multirow region
             * @param[out] region_s obtains single row region
             */
            void partition(const double& r_d, const vector<vertex> &region_o, vector<vertex> &region_m, vector<vector<vertex>> &region_s);
            
            /** */
            void deployInRegions();
            /**
             * @brief Deploy virtual agents of original region function
             * 
             * @param[in] region specifies the input original region
             * @param[out] rd obtains desired radius of trajectory after deploy
             */
            void deployVirtualAgents(const vector<vertex>& region, float& rd);                        

            /**
             * @brief Deploy virtual agents of inner multirow region function
             * 
             * @param[in] r_d specifies the radius of trajectory 
             * @param[in] region specifies multirow region
             * @param[out] set_of_deploy_point obtains position of virtual agents of inner multirow region
             */
            void deployMultiRowInner(const double& r_d, const vector<vertex>& region, vector<vertex> &set_of_deploy_point);

            /**
             * @brief Deploy virtual agents of remaining boundries of multirow region function
             * 
             * @param[in] r_d specifies the radius of trajectory
             * @param[in] i_start specifies the number of the longest boundry in the vertices of multirow region 
             * @param[in] region specifies multirow region
             * @param[out] set_of_deploy_point obtains position of virtual agents of boundries of multirow region 
             */
            void deployMultiRowBoundaries(const double& r_d, const int& i_start, const vector<vertex>& region, vector<vertex> &set_of_deploy_point);

            /**
             * @brief Deploy virtual agents of original region function
             * 
             * @param[in] r_d specifies the radius of trajectory
             * @param[in] segments specifies segment to deploy virtual agent
             * @param[out] segment_deploy_points obtains position of virtual agents of target segment
             */
            void deploySegment(const double& r_d, const vector<vertex>& segments, vector<vertex> &segment_deploy_points);

            /**
             * @brief Deploy virtual agents of original region function
             * 
             * @param[in] r_d specifies the radius of trajectory
             * @param[in] single_row_regions specifies single rew region 
             * @param[out] set_of_deploy_point obtains position of virtual agents of single row region
             */
            void deploySingleRows(const double& r_d, const vector< vector <vertex> >& single_row_regions, vector<vertex> &set_of_deploy_point);

            /**
             * @brief Keep course angle in range function
             * 
             * @param[in] chi specifies MAV's real course angle
             * @param[out] chi_d obtains MAV's desired course angle 
             */
            void keepChiInRange(const float& chi, float& chi_desired);

            /**
             * @brief Keep flight velocity in range
             * 
             * @param[in] v_min specifies permitted mininum velocity 
             * @param[in] v_max specifies permitted maxium velocity
             * @param[out] vel_d obtain desired velocity in range
             */
            void keepVelInRange(const float& v_min, const float& v_max, float& vel_d);

            /**
             * @brief Get the course angle adaptive parameter according to velcity and radius of trajectory
             * 
             * @param[in] v0 specifies flight velocity
             * @param[in] rd specifies radius of trajectory
             * @return float adaptive parameter of course angle
             */
            float getChiParam(const float& v0, const float& rd);

            /**
             * @brief Get desired course angle according to LGVF formula
             * 
             * @param[in] state_N specifies MAV's real positon along Northern direction
             * @param[in] state_E specifies MAV's real positon along Eastern direction
             * @param[in] g_N specifies virtual agent's real positon along Northern direction
             * @param[in] g_E specifies virtual agent's real positon along Eastern direction
             * @param[in] r_d specifies desired radius of trajectory
             * @param[out] r_real obtains MAV's real distance from virtual agent
             * @return float obtains desired course angle
             */
            float getLgvfChi(const float& state_N, const float& state_E, const float& g_N, const float& g_E, const float& r_d, float& r_real);

            /**
             * @brief Oobtain phase angle difference in range
             * 
             * @param[in] real_phase specifies MAV's real phase angle
             * @param[in] target_phase specifies target's real phase angle 
             * @return float obtains phase angle difference [ - M_PI, M_PI ]   
             */
            float standAngleDiff(const float& real_phase, const float& target_phase);

            /**
             * @brief Get the Distance object
             * 
             * @param[in] x1 specifies the position along x axis of point1
             * @param[in] y1 specifies the position along y axis of point1
             * @param[in] x2 specifies the position along x axis of point2
             * @param[in] y2 specifies the position along y axis of point2
             * @return double distance between point1 and point2
             */
            double getDistance(const double& x1, const double& y1, const double& x2, const double& y2);

            /**
             * @brief Get the Line Para object
             * 
             * @param[in] start specifies the start point of the targrt segement
             * @param[in] end specifies the end point of the targrt segement
             * @param[out] linepara obtains the parameters, for instance intercept, slope
             * @return true demonstrates slope is finite
             * @return false demonstrates slope is infinite
             */
            bool getLinePara(const vertex& start, const vertex& end, vertex &linepara);

            /**
             * @brief Get the cross of 2 segments function
             * 
             * @param[in] point_1 specifies start point of segment1
             * @param[in] point_2 specifies end point of segment1
             * @param[in] point_3 specifies start point of segment2
             * @param[in] point_4 specifies end point of segment2
             * @param[out] cross obtains the cross of segment1 and segment2
             * @return true demonstrates find the cross of segment1 and segment2
             * @return false demonstrates did NOT find the cross of segment1 and segment2
             */
            bool getCross(const vertex& point_1, const vertex& point_2, const vertex& point_3, const vertex& point_4, vertex &cross);

            /**
             * @brief Discriminate whether the point is at current boundries function
             * 
             * @param[in] tmp specifies the point to be discriminated
             * @param[in] region specifies current boundaries
             * @return true demonstrates the point is at boundries
             * @return false demonstrates the point is NOT at boundries
             */
            bool isAtBoundaries(const vertex& tmp, const vector<vertex>& region);

            /**
             * @brief Discriminate whether the point is in current region function
             * 
             * @param[in] p specifies specifies the point to be discriminated
             * @param[in] region specifies specifies current region
             * @return true demonstrates the point is in current region
             * @return false demonstrates the point is NOT in current region
             */
            bool isInRegion(const vertex& p, const vector<vertex>& region);

            /**
             * @brief Discriminate whether the region is long & narrow region function
             * 
             * @param[in] d specifies the radius of trajectory
             * @param[in] region specifies the region to be discriminated
             * @return true demonstrates the region is long & narrow region  
             * @return false demonstrates the region is NOT long & narrow region 
             */
            bool isLNArea(const double& d, const vector<vertex>& region);

            /**
             * @brief Record data function
             * @param[in] data specifies data to be recorded
             * @param[in] file_name specifies the name of file
             * @param[in] plane_id specifies the id of MAV
             * @param[in] column_number specifies the desired columns per rank
             */
            template <typename T>
            void recordData(const std::vector<T>& data, const string& file_name, const int& plane_id, const int& column_number);

        protected:
            /**
             * @brief saves MAV's state information
             * elements: pos_N;		position of North
             *           pos_E;		position of East
             *           pos_D;		position of Down (positive above ground)
             *           Va;	  	airspeed
             *           chi;	    course angle
             */
            typedef struct state
            {
                float pos_N;							
                float pos_E;					 		 
                float pos_D;							
                float Va;	  						   
                float chi;	    					 
            }state;
            
            /* @brief saves virtual agent's position
             * elements: goal_N;	position of North
             *           goal_E;	position of East
             *           goal_D;	position of Down (positive above ground)
             */
            typedef struct goal
            {
                float goal_N;					    
                float goal_E;							 
                float goal_D;					    	 
            }goal;

        private:
            ros::NodeHandle nh_;
            ros::NodeHandle nh_private_;

            ros::Subscriber state_sub_;	                     ///< Indicate subscriber for own state
                
            ros::Publisher cmd_to_controller_pub_;	         ///< Indicate publisher for command_controller
            ros::Publisher path_to_rviz_pub_;	             ///< Indicate publisher for msg to rviz
            ros::Publisher goals_to_follower_pub_;           ///< Indicate publisher for virtual agents to followers
            ros::Publisher leader_state_pub_;	             ///< Indicate publisher for leader state to followers
            ros::Publisher areacover_done_pub_;	             ///< Indicate publisher for areacover done signal to followers

            std_msgs::String virtual_agents_;                ///< Indicate msg saved virtual agents to followers
            nav_msgs::Path path_;                            ///< Indicate msg to rviz
            rosplane_msgs::Controller_Commands cmd_;         ///< Indicate msg to controller_commands

            state mav_;                                      ///< Indicate structure variable saves own state   
            goal goal_;                                      ///< Indicate structure goal variable saves postion of own virtual agent

            vector<goal> goals_v_;	                         ///< Indicate save for goal position of VAs
            vector<vertex> set_of_deploy_point_;             ///< Indicate set of position of VAs
            vector<vertex> origin_region_;                   ///< Indicate set of vertices of origin_region (in order)
            vector<vertex> multi_row_region_;                ///< Indicate set of vertices of multi_row_region (in order)
            vector< vector <vertex> > single_row_regions_;   ///< Indicate set of vertices of single_row_regions (in order)
            string area_vertices_;                           ///< Indicate ordered vertices of target area
            string actor_name_;                              ///< Indicate actor name based on actor plugin
            
            bool goal_received_ = false;                    ///< Indicate whether the postion of virtual agents are received
            bool finish_event_published_ = false;            ///< Indicate whether finish_event is published

            float Klamda_;                                   ///< Indicate the course angle adjustment coefficient for trajectory convergence
            float v0_;								         ///< Indicate desired velocity of MAV
            float rd_;								         ///< Indicate desired distance of MAV and VA
            float h_d_;								         ///< Indicate desired distance of MAV and VA
            float delta_h_;
            float v_max_;							         ///< Indicate maximum velocity of MAV
            float v_min_;							         ///< Indicate minimum velocity of MAV
            int ID_;								         ///< Indicate number of MAV
            int total_number_;						         ///< Indicate total number of MAVs
            int begin_time_;                                 ///< Indicate start time of plugin
            int cover_finish_duration_;                      ///< Indicate the finish time of plugin
            int goals_pub_finish_duration_;                  ///< Indicate the finish time of publishing goals
            int sleep_duration_;                             ///< Indicate the sleep duration
            int i_start_ = 0;                                ///< Indicate the number of the longest boundary
    };  

    /**
     * @brief Is Match 
     */
    class isMatch
    {
        public:
            /**
             * @brief Discriminate whether 2 custom structure variables is equal
             * 
             * @param[in] vv the custom structure variable to be discriminated
             */
            isMatch(vertex vv) : v(vv){}

            /**
             * @brief Overload operator () function
             * 
             * @param[in] tmp the custom structure variable to be discriminated
             * @return true demonstrates 2 custom structure variables is equal
             * @return false demonstrates 2 custom structure variables is NOT equal
             */
            bool operator()(vertex tmp) const
            {
                return (fabs(v.x - tmp.x) <= PRECISION && fabs(v.y - tmp.y) < PRECISION);
            }
        private:
            vertex v;
    };

} 
#endif
