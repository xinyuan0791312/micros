/* ==================================================================
* Copyright (c) 2018, micROS Group, TAIIC, NIIDT & HPCL.
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions
* are met:
*
* 1. Redistributions of source code must retain the above copyright
* notice, this list of conditions and the following disclaimer.
* 2. Redistributions in binary form must reproduce the above copyright
* notice, this list of conditions and the following disclaimer in the
* documentation and/or other materials provided with the
* distribution.
* 3. All advertising materials mentioning features or use of this software
* must display the following acknowledgement:
* This product includes software developed by the micROS Group. and
* its contributors.
* 4. Neither the name of the Group nor the names of its contributors may
* be used to endorse or promote products derived from this software
* without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY MICROS,GROUP AND CONTRIBUTORS
* ''AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT
* NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
* FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
* THE MICROS,GROUP OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
* OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
* HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
* OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
* IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
* ===================================================================
* Author: Ren Xiaoguang, Wu Yunlong, Li Jinghua, micROS-DA Team, TAIIC.
*/

#include "leader_lgvf_plugin_sim/leader_lgvf_plugin_sim.h"
#include <pluginlib/class_list_macros.h>

PLUGINLIB_EXPORT_CLASS(general_bus::LeaderLgvfPluginSim,general_bus::GeneralPlugin)

namespace general_bus
{
void LeaderLgvfPluginSim::start()
{
   	nh_ = ros::NodeHandle();
    nh_private_ = ros::NodeHandle("~");

    nh_private_.param<int>("robot_id", ID_, 1);
	// nh_private_.param<float>("v_max", v_max_, 4);
	// nh_private_.param<float>("v_min", v_min_, 0.5);
    // nh_private_.param<float>("delta_h", delta_h_, 3.0);
    // nh_private_.param<int>("goals_pub_finish_duration", goals_pub_finish_duration_, 30);
    // nh_private_.param<int>("sleep_duration", sleep_duration_, 10000);

    area_vertices_ = getParam("vertices");
    v0_ = atof( getParam("v0").c_str() );
    rd_ = atof( getParam("rd").c_str() );
	h_d_ = atof( getParam("h_d").c_str() );
    total_number_ = atoi( getParam("total_number").c_str() );
	cover_finish_duration_ = atoi( getParam("cover_finish_duration").c_str() );

    v_max_ = atof( getParam("v_max").c_str() );
	v_min_ = atof( getParam("v_min").c_str() );
    delta_h_ = atof( getParam("delta_h").c_str() );
    goals_pub_finish_duration_ = atoi( getParam("goals_pub_finish_duration").c_str() );
    sleep_duration_ = atoi( getParam("sleep_duration").c_str() );

    getActorName(_actorID, actor_name_);
    for(auto &c:actor_name_)
    {
        if(c == '/') c = '_';
    }

    path_.header.stamp=ros::Time::now();
	path_.header.frame_id="/my_frame";
    begin_time_ = (int)ros::Time::now().toSec();
    h_d_ += ID_*delta_h_;

    deployInRegions();
    
    Klamda_ = getChiParam(v0_, rd_); 
	// ROS_INFO("[Leader start] ID_:%d, rd_:%f, v0_:%f, h_d_:%f, Klamda_:%f\n", ID_, rd_, v0_, h_d_, Klamda_);

    leader_state_pub_ = pluginAdvertise<rosplane_msgs::State>("/Leader_state", 10);  
    goals_to_follower_pub_ = pluginAdvertise<std_msgs::String>("/goal", 10);
    areacover_done_pub_ = pluginAdvertise<std_msgs::Empty>("/areacover_done", 10);
	cmd_to_controller_pub_ = pluginAdvertise<rosplane_msgs::Controller_Commands>("controller_commands", 10);
	path_to_rviz_pub_ = pluginAdvertise<nav_msgs::Path>("trajectory", 10);

	state_sub_ = pluginSubscribe<rosplane_msgs::State>("truth", 10,&LeaderLgvfPluginSim::stateCallback,this);
	
    while(true)
    {
        GOON_OR_RETURN;
        discriminateEvents();
        // visualizeTrajectory();
        if( (int)ros::Time::now().toSec() < (begin_time_ + goals_pub_finish_duration_) )
        {
            goal_ = goals_v_[ID_-1];
            goals_to_follower_pub_.publish(virtual_agents_); 
            // for(auto &c: goals_v_)  
            // printf("size: %d    x: %f,   y: %f\n", (int)goals_v_.size(), c.goal_N, c.goal_E);
            
            // vector<goal> tmp_goal;
            // tmp_goal.resize(virtual_agents_.data.size()/8-1);
            // float tmp_r,tmp_va;int tmp_id; 
            // memcpy(&tmp_id,&virtual_agents_.data[0],4);
            // memcpy(&tmp_r,&virtual_agents_.data[4],4);
            // memcpy(&tmp_va,&virtual_agents_.data[8],4);
            // printf("************ tmp_id: %d    tmp_r: %f   %f\n", tmp_id, tmp_r, tmp_va);
            // for(size_t i = 0; i < tmp_goal.size(); i++)
            // {
            //     memcpy(&tmp_goal[i].goal_N,&virtual_agents_.data[8*i+8],4);
		    //     memcpy(&tmp_goal[i].goal_E,&virtual_agents_.data[8*i+12],4);
               
            //     //printf("************ size: %d    x: %f,   y: %f\n", (int)tmp_goal.size(), tmp_goal[i].goal_N, tmp_goal[i].goal_E);
            // }
            
        }
        calculateCommand();
        usleep(sleep_duration_);
    }
}    

void LeaderLgvfPluginSim::calculateCommand()
{  
	float r = 0;
    float vel_d = v0_;
    float chi_d = getLgvfChi(mav_.pos_N, mav_.pos_E, goal_.goal_N, goal_.goal_E, rd_, r);
    chi_d += Klamda_;
    keepChiInRange(mav_.chi, chi_d);
    //ROS_INFO("[Leader calculateCommand] rd_: %f, r: %f, chi_d: %f, vel_d: %f, mav_.N: %f, mav_.E: %f, goal_.N: %f, goal_.E: %f ", rd_, r, chi_d, vel_d, mav_.pos_N, mav_.pos_E, goals_v_[ID_-1].goal_N, goals_v_[ID_-1].goal_E);
			
	cmd_.chi_c = chi_d;
	cmd_.Va_c = vel_d;
	cmd_.h_c = h_d_;
    cmd_.aux[0] = 49;
	cmd_to_controller_pub_.publish(cmd_);	
}

void LeaderLgvfPluginSim::discriminateEvents()
{
	if( (int)ros::Time::now().toSec() > (begin_time_ + cover_finish_duration_) )  
    {
        std_msgs::Empty msg;
        areacover_done_pub_.publish(msg);

        if(!finish_event_published_)
        {
            pubEventMsg("finish_event");
            finish_event_published_ = true;
        }
    }
}

void LeaderLgvfPluginSim::deployInRegions()
{
    std::vector<std::string> string_regions;
    getRegions(area_vertices_, string_regions);
    std::vector<std::vector<vertex>> vertex_regions(string_regions.size());
    
    float rd_tmp = rd_;  
    do
    {
        vector<vertex>().swap(set_of_deploy_point_);
        for(size_t i=0; i < string_regions.size(); i++)
        {   
            getAreaVertices(string_regions[i], vertex_regions[i]);
            if(vertex_regions[i].size() < 3)
            {
                ROS_INFO("[Leader start] Not a region ! \n");
                continue;
            } 
            else
                deployMultiRowInner(rd_tmp, vertex_regions[i], set_of_deploy_point_);
            printf("size: %d    \n", set_of_deploy_point_.size());
        }
        if(rd_tmp > 1.0)
            rd_tmp -= 1.0;
        // printf("size: %d    \n", set_of_deploy_point_.size());
    }while (set_of_deploy_point_.size() < total_number_);

    

    size_t goal_counts = set_of_deploy_point_.size();
    goals_v_.resize(goal_counts);
    virtual_agents_.data.resize(goal_counts*2*4+8);
    memcpy(&virtual_agents_.data[0],&ID_,4);
    memcpy(&virtual_agents_.data[4],&rd_,4);
    for(int i = 0; i < goal_counts; i++)
    {
        goals_v_[i].goal_N = (float)set_of_deploy_point_[i].y;
        goals_v_[i].goal_E = (float)set_of_deploy_point_[i].x;
        memcpy(&virtual_agents_.data[8*i+8],&goals_v_[i].goal_N,4);
        memcpy(&virtual_agents_.data[8*i+12],&goals_v_[i].goal_E,4);
    } 
    ROS_INFO("[Leader deployVirtualAgents] goals_received!!!");
    goal_received_ = true;
}

void LeaderLgvfPluginSim::stateCallback(const rosplane_msgs::State::ConstPtr& msg)
{
    leader_state_pub_.publish(*msg); 
    mav_.pos_N = msg->position[0];
	mav_.pos_E = msg->position[1];
	mav_.pos_D = - msg->position[2];
	mav_.Va = msg->Va;
	mav_.chi = msg->chi;
}

void LeaderLgvfPluginSim::getRegions(const std::string& vertices, std::vector<std::string> &string_regions)
{
  std::vector<std::string>().swap(string_regions);
  std::string::size_type pos1, pos2;
  pos1 = vertices.find("{");
  
  if(std::string::npos == pos1) 
  {
    string_regions.push_back(vertices);
    printf("%d\n", string_regions.size());
    for(auto&c:string_regions)
      printf("%s\n", c.c_str());
    return;
  }

  while(std::string::npos != pos1) 
  {
    pos2 = vertices.find("}", pos1);
    string_regions.push_back(vertices.substr(pos1+1, pos2-pos1-1));
    pos1 = vertices.find("{", pos2);
  }
  printf("%d\n", string_regions.size());
  for(auto&c:string_regions)
      printf("%s\n", c.c_str());
}

void LeaderLgvfPluginSim::getAreaVertices(const string& vertices, vector<vertex>& origin_region)
{
    std::vector<vertex>().swap(origin_region);
    std::vector<std::string> aRet;
    std::string::size_type pos1, pos2;
    pos2 = vertices.find(" ");
    pos1 = 0;
    int cnt = 0;
    while(std::string::npos != pos2) 
    {
      aRet.push_back(vertices.substr(pos1, pos2-pos1));
      pos1 = pos2 + 1;
      pos2 = vertices.find(" ", pos1);
      cnt++;
    }
    if(pos1 != vertices.length())
    {
      aRet.push_back(vertices.substr(pos1));
      cnt++;
    }
    origin_region.resize(cnt/2);
    for(int i = 0; i < cnt/2; i++)
    {
      origin_region[i].x = atof(aRet.at(i*2).c_str());
      origin_region[i].y = atof(aRet.at(i*2 + 1).c_str());
    }
}
  
void LeaderLgvfPluginSim::visualizeTrajectory()
{
	geometry_msgs::PoseStamped this_pose_stamped;
	this_pose_stamped.pose.position.x = mav_.pos_N;
	this_pose_stamped.pose.position.y = - mav_.pos_E;
	this_pose_stamped.pose.orientation.x = 0;
	this_pose_stamped.pose.orientation.y = 0;
	this_pose_stamped.pose.orientation.z = 0;
	this_pose_stamped.pose.orientation.w = 1;
	this_pose_stamped.header.stamp=ros::Time::now();
	this_pose_stamped.header.frame_id="/my_frame";

	path_.poses.push_back(this_pose_stamped);
	path_to_rviz_pub_.publish(path_); 
}
 
void LeaderLgvfPluginSim::deployVirtualAgents(const vector<vertex>& region, float& rd)
{
    float rd_tmp =rd;
    int goal_counts = -1;
    while( goal_counts < total_number_ )
    {
        GOON_OR_RETURN;
        if( goal_counts != -1 )
        {
            rd_tmp -= 1;
            if(rd_tmp<=0)
            {
                ROS_INFO("[Leader deployVirtualAgents] NO enough points to deploy!!!");
                break;
            }   
        } 
        vector<vertex>().swap(set_of_deploy_point_);
        partition(rd_tmp, origin_region_, multi_row_region_, single_row_regions_);
        deployMultiRowInner(rd_tmp, multi_row_region_, set_of_deploy_point_);
        ROS_INFO("[Leader deployVirtualAgents] After INNER deploy, set_of_deploy_point_.size() : %d \n", (int)set_of_deploy_point_.size());
        // deployMultiRowBoundaries(rd_tmp, i_start_, multi_row_region_, set_of_deploy_point_);
        // deploySingleRows(rd_tmp, single_row_regions_, set_of_deploy_point_);
        goal_counts = set_of_deploy_point_.size();  
    }
    if(rd_tmp > 0)
    {
        std::cout << goal_counts << std::endl;
        goals_v_.resize(goal_counts);
        virtual_agents_.data.resize(goal_counts*2*4+8);
        memcpy(&virtual_agents_.data[0],&ID_,4);
        memcpy(&virtual_agents_.data[4],&rd,4);
        for(int i = 0; i < goal_counts; i++)
        {
            goals_v_[i].goal_N = set_of_deploy_point_[i].y;
            goals_v_[i].goal_E = set_of_deploy_point_[i].x;
            memcpy(&virtual_agents_.data[8*i+8],&goals_v_[i].goal_N,4);
            memcpy(&virtual_agents_.data[8*i+12],&goals_v_[i].goal_E,4);
        } 
        ROS_INFO("[Leader deployVirtualAgents] goals_received!!!");
        goal_received_ = true;
    }	
}

void LeaderLgvfPluginSim::partition(const double& r_d, const vector<vertex> &region_o, vector<vertex> &region_m, vector<vector<vertex>> &region_s)
{
    vector<vertex>().swap(region_m);
    vector<vector<vertex>>().swap(region_s);
    region_m = region_o;
    double d = sqrt(3)*r_d;
    int i = 0,j;
    
    while(i < region_m.size())
    {
        GOON_OR_RETURN;
        j = i + 2;
        while(j < region_m.size())
        {
            GOON_OR_RETURN;
            double d_i_j = getDistance(region_m[i].x, region_m[i].y, region_m[j].x, region_m[j].y);
            if(d_i_j > d)  
            {
                j++;
                continue;
            }else if(j == i + 2)    
            {
                vector<vertex> tmp = { region_m[i], region_m[i+1], region_m[j] };
                region_s.push_back(tmp);
                region_m.erase(region_m.begin()+i+1);
            }else// divide the region into 2 parts to distinguish whether there is single row region
            {
                vector<vertex> o_1, o_2 = region_m;
                for(int m = i; m <=j; m++)
                {
                    GOON_OR_RETURN;
                    o_1.push_back(region_m[m]); 
                }                    
                o_2.insert(o_2.begin(), o_2.begin() + j, o_2.end());
                o_2.erase(o_2.begin()+region_m.size()-j+i+1, o_2.end());
                bool flag_1 = isLNArea(d, o_1);
                bool flag_2 = isLNArea(d, o_2);
                if(flag_1 && flag_2)
                {
                    region_s.push_back(o_1); 
                    region_s.push_back(o_2); 
                    vector<vertex>().swap(region_m);
                }else if(flag_1 && !flag_2)
                {
                    region_s.push_back(o_1);  
                    region_m = o_2;
                }else if(!flag_1 && flag_2)
                {
                    region_s.push_back(o_2);   
                    region_m = o_1;
                } 
            }
            j++;
        }
        i++;
    }
    
    i = 0, j = 0;
    while(i < region_m.size())
    {
        GOON_OR_RETURN;
        int k = region_m.size(),h;
        if(i == k - 1)    j = 0;
        else    j = i + 1;
        if(i == 0)    h = k - 1;
        else h = i - 1;
        double theta_i_j = atan2(region_m[j].y - region_m[i].y, region_m[j].x - region_m[i].x);
        double theta_h_i = atan2(region_m[h].y - region_m[i].y, region_m[h].x - region_m[i].x);
        double inner_angle = theta_h_i - theta_i_j;
        bool flag = false;
        for(int l = 0; l < region_s.size(); l++)
        {
            GOON_OR_RETURN;
            bool f_1 = (std::find_if(region_s[l].begin(), region_s[l].end(), isMatch(region_m[i])) != region_s[l].end()); 
            bool f_2 = (std::find_if(region_s[l].begin(), region_s[l].end(), isMatch(region_m[h])) != region_s[l].end());   
            bool f_3 = (std::find_if(region_s[l].begin(), region_s[l].end(), isMatch(region_m[j])) != region_s[l].end()); 
            if(f_1 && (f_2 || f_3))
            {
                flag = true;
                break;
            }
        }
        if(fabs(inner_angle) > M_PI / 2 || flag)
        {
            i++;
            ROS_INFO("[Leader Partition] continue i %d\n", i);
            continue;
        }
        vector<vertex> lines;
        getParallels(sqrt(3)*r_d, region_m[h], region_m[i], lines);
        vertex cross;
        bool flag_1 = false, flag_2 = false;
        getCross(region_m[i], region_m[j], lines[0], lines[1], cross);
        vector<vertex> segment = {region_m[i], region_m[j]};
        flag_1 = isAtBoundaries(cross,segment);
        if(!flag_1)
        {
            getCross(region_m[i], region_m[j], lines[2], lines[3], cross);
            flag_2 = isAtBoundaries(cross,segment);   
        }
        vertex start;
        region_s.resize(region_s.size()+1);
        // make vertical line through point cross
        if(flag_1 || flag_2)
        {
            double d = sqrt(3)*r_d;
            if(theta_h_i == 0 || theta_h_i == M_PI)
            {
                start.x = cross.x;
                start.y = region_m[i].y; 
            }else if(theta_h_i == M_PI/2 || theta_h_i == - M_PI/2)
            {
                start.x = region_m[i].x;
                start.y = cross.y; 
            }else
            {
                start.x = region_m[i].x + d*cos(theta_h_i)/tan(fabs(inner_angle));
                start.y = region_m[i].y + d*sin(theta_h_i)/tan(fabs(inner_angle));
            }
            // the vertical line of segment hi through point cross is not at segment hi, then make vertical line of segment ij, the cross point must be at segment hi
            if(!isAtBoundaries(start, { region_m[h], region_m[i] })) 
            {
                start = region_m[h];
                double d_s_i = getDistance(start.x, start.y, region_m[i].x, region_m[i].y);
                vertex end = { region_m[i].x + d_s_i*cos(fabs(inner_angle))*cos(theta_i_j), region_m[i].y + d_s_i*cos(fabs(inner_angle))*sin(theta_i_j) };

                region_s[region_s.size()-1].push_back(start);
                region_s[region_s.size()-1].push_back(region_m[i]);
                region_s[region_s.size()-1].push_back(end);

                vector<vertex>::iterator it = region_m.begin() + i;
                it = region_m.erase(it);
                region_m.insert(it, end);
            }else// the vertical line of segment hi through point cross is at segment hi
            {
                region_s[region_s.size()-1].push_back(start);
                region_s[region_s.size()-1].push_back(region_m[i]);
                region_s[region_s.size()-1].push_back(cross);

                vector<vertex>::iterator it = region_m.begin() + i;
                it = region_m.erase(it);
                region_m.insert(it, cross);
                it = region_m.begin() + i;//Add BY LJH modify error.20191217
                region_m.insert(it, start);
            } 
        }else
        {
            double d_i_j = getDistance(region_m[j].x, region_m[j].y, region_m[i].x, region_m[i].y);
            start = { region_m[i].x + d_i_j*cos(fabs(inner_angle))*cos(theta_h_i), region_m[i].y + d_i_j*cos(fabs(inner_angle))*sin(theta_h_i) };

            region_s[region_s.size()-1].push_back(start);
            region_s[region_s.size()-1].push_back(region_m[i]);
            region_s[region_s.size()-1].push_back(region_m[j]);
               
            vector<vertex>::iterator it = region_m.begin() + i;
            it = region_m.erase(it);
            region_m.insert(it, start);
        }
        i++;
    }// ** end while
}// * end partition

// multirow region deployment
void LeaderLgvfPluginSim::deployMultiRowInner(const double& r_d, const vector<vertex>& region, vector<vertex> &set_of_deploy_point)
{
    int k = region.size();
    // find the longest boundary and its endpoints of multirow region
    i_start_ = k - 1;    // number of start point of the longest boundary
    double d = (region[i_start_].x - region[0].x) * (region[i_start_].x - region[0].x)
            + (region[i_start_].y - region[0].y) * (region[i_start_].y - region[0].y);
    for(int i = 0 ; i < k-1; i++)
    {
        GOON_OR_RETURN;
        double tmp = (region[i+1].x - region[i].x) * (region[i+1].x - region[i].x)
                  + (region[i+1].y - region[i].y) * (region[i+1].y - region[i].y);
        if(tmp > d)
        {
            d = tmp;
            i_start_ = i;
        }
    }
    ROS_INFO("[Leader deployMultiRowInner] i_start_: %d \n", i_start_);

    // find start point of deployment
    double theta = atan2((region[i_start_+1].y - region[i_start_].y) , (region[i_start_+1].x - region[i_start_].x));
    bool flag_find_start_point = false;
    vertex start;
    for(int i = i_start_; i <= i_start_ + 1; i++)
    {
        GOON_OR_RETURN;
        vector<vertex> possible_start_point
        = { {region[i].x + r_d * cos(theta+asin(0.5)),region[i].y + r_d * sin(theta+asin(0.5))},
            {region[i].x - r_d * cos(theta+asin(0.5)),region[i].y - r_d * sin(theta+asin(0.5))},
            {region[i].x + r_d * cos(theta-asin(0.5)),region[i].y + r_d * sin(theta-asin(0.5))},
            {region[i].x - r_d * cos(theta-asin(0.5)),region[i].y - r_d * sin(theta-asin(0.5))}
          };
        int j;
        for(j = 0; j < 6; j++)
        {
            GOON_OR_RETURN;
            if(isInRegion(possible_start_point[j], region) == true)
            {
                flag_find_start_point = true;
                break;
            }
        }
        if(flag_find_start_point)
        {
            start = possible_start_point[j];
            ROS_INFO("[Leader deployMultiRowInner] start_point: %f, %f \n", start.x, start.y);
            break;
        }
    }
    if(!flag_find_start_point)
        ROS_INFO("[Leader deployMultiRowInner] Failed to find the start point ! \n");
    // ** end find start point of deployment

    // BFS
    vector<vertex> Q_F;  // frontier searching queue
    vector<vertex> Q_N;  // next searching queue// multirow region deployment
    Q_N.push_back(start);
    set_of_deploy_point.push_back(start);
    vector<vertex> adjacent_points; // save adjacent points
    vector<vertex> v_expanded;      // save enpanded points
    while(!Q_N.empty())
    {
        GOON_OR_RETURN;
        Q_F = Q_N;
        vector<vertex>().swap(Q_N);
        for(int i = 0; i < Q_F.size(); i++)
        {
            GOON_OR_RETURN;
            vector<vertex>::iterator iter = std::find_if(v_expanded.begin(), v_expanded.end(), isMatch(Q_F[i]));
            if(iter == v_expanded.end())
                v_expanded.push_back(Q_F[i]);
            getAdjacentPoints(r_d, theta, Q_F[i], adjacent_points);
            for(int j = 0; j < 6; j++)
            {
                GOON_OR_RETURN;
                if(std::find_if( v_expanded.begin(),  v_expanded.end(), isMatch(adjacent_points[j])) != v_expanded.end()
                  || std::find_if(Q_N.begin(), Q_N.end(), isMatch(adjacent_points[j])) != Q_N.end()
                  || (!isInRegion(adjacent_points[j], region) && !isAtBoundaries(adjacent_points[j], region))
                  )
                    continue;
                else
                {
                    Q_N.push_back(adjacent_points[j]);
                    if(std::find_if(set_of_deploy_point.begin(), set_of_deploy_point.end(), isMatch(adjacent_points[j])) == set_of_deploy_point.end())
                        set_of_deploy_point.push_back(adjacent_points[j]);
                }
            }
            vector<vertex>().swap(adjacent_points);
        }
    }// ** end BFS
}// * end multirow region deployment

void LeaderLgvfPluginSim::deployMultiRowBoundaries(const double& r_d, const int& i_start, const vector<vertex>& region, vector<vertex> &set_of_deploy_point)
{
    int k = region.size();
    // adjust the order of boundaries according to i_start
    vector<vertex> order_adjusted_boundaries = region;
    if(i_start <= (k - 1)/2)
    {
        order_adjusted_boundaries.insert(order_adjusted_boundaries.end(), order_adjusted_boundaries.begin(), order_adjusted_boundaries.begin() + i_start + 1);
        order_adjusted_boundaries.erase(order_adjusted_boundaries.begin(), order_adjusted_boundaries.begin() + i_start + 1);
    }else
    {
        order_adjusted_boundaries.insert(order_adjusted_boundaries.begin(), order_adjusted_boundaries.begin() + i_start + 1, order_adjusted_boundaries.end());
        order_adjusted_boundaries.erase(order_adjusted_boundaries.begin() + k , order_adjusted_boundaries.end());
    }
    // ** end adjust the order of boundaries according to i_start

    reverse(order_adjusted_boundaries.begin(), order_adjusted_boundaries.end());

    // calculate the first point of boundaries to be deployed of multirow region
    vector<vertex> seg = { order_adjusted_boundaries[0], order_adjusted_boundaries[1] };
    vector<vertex> para_lines;
    int k_o = order_adjusted_boundaries.size();
    getParallels(r_d/2, order_adjusted_boundaries[0], order_adjusted_boundaries[k_o - 1], para_lines);
    for(int i = 0; i < 2; i++)
    {
        GOON_OR_RETURN;
        vertex cross;
        bool flag_getcross = getCross(para_lines[2*i], para_lines[2*i + 1], order_adjusted_boundaries[0], order_adjusted_boundaries[1], cross);
        bool k = isAtBoundaries(cross, seg);
        if(flag_getcross && k)
        {
            order_adjusted_boundaries[0] = cross;
            ROS_INFO("[Leader deployMultiRowInner] Boundary start point: %f %f \n", order_adjusted_boundaries[0].x, order_adjusted_boundaries[0].y);
            break;
        }
    }
    // ** end calculate the first point of boundaries to be deployed of multirow region

    vector<vertex> seg_points;
    deploySegment(r_d, order_adjusted_boundaries, seg_points);
    int k_seg = seg_points.size();
    for(int i = 0; i < k_seg; i++)
    {
        GOON_OR_RETURN;
        if(std::find_if(set_of_deploy_point_.begin(), set_of_deploy_point_.end(), isMatch(seg_points[i])) ==  set_of_deploy_point_.end())
        {
            set_of_deploy_point_.push_back(seg_points[i]);
        }        
    }
}

void LeaderLgvfPluginSim::deploySingleRows(const double& r_d, const vector< vector <vertex> >& single_row_regions, vector<vertex> &set_of_deploy_point)
{
    int k = single_row_regions.size();
    for(int i = 0; i < k; i++)
    {
        GOON_OR_RETURN;
        vector <vertex> tmp;
        calculateMidpoints(single_row_regions[i], tmp);
        deploySegment(r_d, tmp, set_of_deploy_point_);
    }
}

bool LeaderLgvfPluginSim::getLinePara(const vertex& start, const vertex& end, vertex &linepara)
{
    if(fabs(start.x - end.x) <= PRECISION)
        return false;
    else
    {
        linepara.x = (end.y - start.y) / (end.x - start.x);
	    linepara.y = start.y - linepara.x * start.x;
        return true;
    }
}
bool LeaderLgvfPluginSim::getCross(const vertex& point_1, const vertex& point_2, const vertex& point_3, const vertex& point_4, vertex &cross)
{
	vertex para1, para2;
	bool k_1 = getLinePara(point_1, point_2, para1);
	bool k_2 = getLinePara(point_3, point_4, para2);

    if((!k_1 && !k_2) || para1.x == para2.x)
    {
        return false;
    }else if (!k_1 && k_2)
    {
        cross.x = (point_1.x + point_2.x) / 2;
        cross.y = para2.x * cross.x + para2.y;
        return true;
    }else if (!k_2 && k_1)
    {
        cross.x = (point_3.x + point_4.x) / 2;
        cross.y = para1.x * cross.x + para1.y;
        return true;
    }else
    {
        cross.x = (para2.y - para1.y) / (para1.x - para2.x);
		cross.y = para1.x * cross.x + para1.y;
        return true;
    }
}

bool LeaderLgvfPluginSim::isAtBoundaries(const vertex& tmp, const vector<vertex>& region)
{
    int k = region.size();
    int i ,j;
    for(i = 0 ; i < k; i++)
    {
        GOON_OR_RETURN;
        if(i == k - 1)
            j = 0;
        else
            j = i + 1;
        double d_seg = getDistance(region[j].x, region[j].y, region[i].x, region[i].y);
        double d_seg_i = getDistance(tmp.x, tmp.y, region[i].x, region[i].y);
        double d_seg_j = getDistance(region[j].x, region[j].y,  tmp.x,  tmp.y);
        if(fabs(d_seg_i + d_seg_j - d_seg) <= PRECISION)
            return true;
    }
    return false;
}

bool LeaderLgvfPluginSim::isInRegion(const vertex& p, const vector<vertex>& region)
{
    if(isAtBoundaries(p, region))
        return false;
    int nCross = 0;
    int k = region.size();
    for (int i = 0; i < k; i++)
    {
        vertex p1 = region[i];
        vertex p2 = region[(i + 1) % k];

        if(fabs(p1.y - p2.y) <= PRECISION || p.y < std::min(p1.y, p2.y) || p.y > std::max(p1.y, p2.y))
            continue;
        double x = (double)(p.y - p1.y) * (double)(p2.x - p1.x) / (double)(p2.y - p1.y) + p1.x;
        if (x > p.x)
            nCross++;
    }
    return (nCross % 2 == 1);
}

void LeaderLgvfPluginSim::getParallels(const double& d, const vertex& p1, const vertex& p2, vector<vertex> &parallel_lines)
{
    parallel_lines.resize(4);
    if(fabs(p1.x - p2.x) <= PRECISION)
        parallel_lines = { {p1.x - d, p1.y}, {p2.x - d, p2.y}, {p1.x + d, p1.y}, {p2.x + d, p2.y} };
    else
    {
        // angle with respect to the X-axis[-π, π)
        double theta = atan2((p2.y - p1.y) , (p2.x - p1.x));
        parallel_lines = { {p1.x, p1.y - d/cos(theta)}, {p2.x, p2.y - d/cos(theta)}, {p1.x, p1.y + d/cos(theta)}, {p2.x, p2.y + d/cos(theta)} };
    }
}

void LeaderLgvfPluginSim::getAdjacentPoints(const double& r_real, const double& theta_in, const vertex& tmp, vector<vertex>& adjacent)
{
    for(int i = 0; i < 6; i++)
    {
        GOON_OR_RETURN;
        double alpha = i * M_PI / 3.0;
        adjacent.push_back({ tmp.x + sqrt(3) * r_real * cos(theta_in - alpha), tmp.y + sqrt(3) * r_real * sin(theta_in - alpha) });
    }
}

void LeaderLgvfPluginSim::deploySegment(const double& r_d, const vector<vertex>& segments, vector<vertex> &segment_deploy_points)
{
    int k = segments.size();
    vertex tmp;
    bool flag_endpoint = false;
    for(int i = 0; i < k-1; i++)
    {
        GOON_OR_RETURN;
        vector<vertex> current_segment = { segments[i], segments[i+1] };
        double theta = atan2((segments[i+1].y - segments[i].y) , (segments[i+1].x - segments[i].x));
        tmp = segments[i];
        while(isAtBoundaries(tmp, current_segment))
        {
            GOON_OR_RETURN;
            if(std::find_if(segment_deploy_points.begin(), segment_deploy_points.end(), isMatch(tmp)) ==  segment_deploy_points.end())
                segment_deploy_points.push_back(tmp);

            vertex v = { tmp.x + sqrt(3) * r_d * cos(theta), tmp.y + sqrt(3) * r_d * sin(theta) };
            if(std::find_if(segment_deploy_points.begin(), segment_deploy_points.end(), isMatch(v)) !=  segment_deploy_points.end() || !isAtBoundaries(v, current_segment))
                v = { tmp.x + sqrt(3) * r_d * cos(theta + M_PI), tmp.y + sqrt(3) * r_d * sin(theta + M_PI) };
            tmp = v;
            bool a = isAtBoundaries(v, current_segment);
        }
    }
    if(std::find_if(segment_deploy_points.begin(), segment_deploy_points.end(), isMatch(segments[k - 1])) ==  segment_deploy_points.end())
        segment_deploy_points.push_back(segments[k - 1]);
}

void LeaderLgvfPluginSim::calculateMidpoints(const vector<vertex>& region, vector<vertex>& midpoints)
{
    int k = region.size();
    int i = 0, j = k - 1;
    while(i <= j)
    {
        GOON_OR_RETURN;
        midpoints.push_back({(region[i].x + region[j].x)/2, (region[i].y + region[j].y)/2});
        if(j - i > 2)
            midpoints.push_back({(region[i].x + region[j-1].x)/2, (region[i].y + region[j-1].y)/2});
        i++;
        j--;
    }
}

bool LeaderLgvfPluginSim::isLNArea(const double& d, const vector<vertex>& region) 
{
    int s = region.size();
    int i = 0, j = s-1;
    while(i <= j)
    {
        GOON_OR_RETURN;
        double d_i_j = getDistance(region[j].x, region[j].y,  region[i].x, region[i].y);
        if(d_i_j > d)
            return false;
        i++;
        j--;
    }
    return true;
}

float LeaderLgvfPluginSim::standAngleDiff(const float& real_phase, const float& target_phase)
{
	float diff = real_phase - target_phase;
  	if (diff < -M_PI && diff >= -2*M_PI)  
		return (diff + 2*M_PI);
  	else if (diff <= 2*M_PI && diff > M_PI) 
		return (diff - 2*M_PI);
  	else 
		return diff;
}

double LeaderLgvfPluginSim::getDistance(const double& x1, const double& y1, const double& x2, const double& y2)
{
    return (sqrt(pow(x1 - x2, 2) + pow(y1 - y2, 2)));
}

float LeaderLgvfPluginSim::getLgvfChi(const float& state_N, const float& state_E, const float& g_N, const float& g_E, const float& r_d, float& r_real)
{
    float delta_N = state_N - g_N;
	float delta_E = state_E - g_E;
	r_real = sqrt(pow(delta_N,2) +  pow(delta_E,2));
    float A = r_real*r_real + r_d*r_d;
    float B = r_real*r_real - r_d*r_d;
    float C = r_real*r_d;
    
    if(fabs(delta_N*B + 2*delta_E*C) > PRECISION)
    {
        
        return (atan2(-((delta_E)*B - 2*(delta_N)*C), -((delta_N)*B + 2*(delta_E)*C)));
    }else
    {
        return mav_.chi;
    }
}

float LeaderLgvfPluginSim::getChiParam(const float& v0, const float& rd)
{
	return(-0.1569 +  0.0862*v0 - 0.006063*rd - 0.0009796*v0*rd + 0.0009238*v0*v0 + 0.0001031*rd*rd);
}

void LeaderLgvfPluginSim::keepVelInRange(const float& v_min, const float& v_max, float& vel_d)
{
	if(vel_d < v_min)
		vel_d = v_min;
	else if(vel_d > v_max)
		vel_d = v_max;
	else
		return;	
}

void LeaderLgvfPluginSim::keepChiInRange(const float& chi, float& chi_desired)
{
	if(chi_desired - mav_.chi > M_PI)
		chi_desired -= 2*M_PI;
	else if(chi_desired - mav_.chi < - M_PI)
		chi_desired += 2*M_PI;
	else
		return;	
}

template <typename T>
void LeaderLgvfPluginSim::recordData(const std::vector<T>& data, const string& file_name, const int& plane_id, const int& column_number)
{
    int i = 0;
    string file = "/home/ljh/Data/" + file_name + "_" + actor_name_ + "_" + std::to_string(plane_id) + ".txt";
    char fp_o[100];
    snprintf( fp_o, file.size()+1, "%s", file.c_str() );
    FILE *pFile_o = fopen( fp_o, "a" );
    for( auto &c:data )
    {
        fprintf( pFile_o, "%f ",c);
        if( (++i) % column_number == 0 )
            fprintf( pFile_o, "\n");

    }
    fclose( pFile_o );
}

} // end namespece




