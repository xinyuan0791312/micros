/* ==================================================================
* Copyright (c) 2018, micROS Group, TAIIC, NIIDT & HPCL.
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions
* are met:
*
* 1. Redistributions of source code must retain the above copyright
* notice, this list of conditions and the following disclaimer.
* 2. Redistributions in binary form must reproduce the above copyright
* notice, this list of conditions and the following disclaimer in the
* documentation and/or other materials provided with the
* distribution.
* 3. All advertising materials mentioning features or use of this software
* must display the following acknowledgement:
* This product includes software developed by the micROS Group. and
* its contributors.
* 4. Neither the name of the Group nor the names of its contributors may
* be used to endorse or promote products derived from this software
* without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY MICROS,GROUP AND CONTRIBUTORS
* ''AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT
* NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
* FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
* THE MICROS,GROUP OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
* OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
* HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
* OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
* IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
* ===================================================================
* Author: Ren Xiaoguang, Wu Yunlong, Li Jinghua, micROS-DA Team, TAIIC.
*/


#ifndef FOLLOWER_LGVF_PLUGIN_SIM
#define FOLLOWER_LGVF_PLUGIN_SIM

#include "general_plugin/general_plugin.h"

#include <vector>
#include <string>
#include <math.h>
#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <termios.h>
#include <ros/ros.h>
#include <nav_msgs/Path.h>
#include <std_msgs/Empty.h>
#include <std_msgs/String.h>
#include <geometry_msgs/Point.h>
#include <geometry_msgs/Quaternion.h>
#include <geometry_msgs/PoseStamped.h>
#include <rosplane_msgs/State.h>
#include <rosplane_msgs/Controller_Commands.h>

using std::vector;
using std::string;

#define PRECISION  1e-5
#define TIME_PROBE getTimeInterval()

/**
 * @brief General Bus Namespace
 */
namespace general_bus
{
    /**
     * @brief Follower LGVF Plugin
     */
    class FollowerLgvfPluginSim: public GeneralPlugin
    {
        public:
            /**
             * @brief Overload start function of General Plugin
             */
            virtual void start(); 

            /**
             * @brief Goal callback function
             * 
             * @param[in] msg specifies the goal message
             */
            void goalCallback(const std_msgs::String::ConstPtr& msg);

            /**
             * @brief Enemy information callback function
             * 
             * @param[in] msg specifies the enemy position
             */
            void enemyInfoCallback(const geometry_msgs::Point::ConstPtr& msg);

            /**
             * @brief Areacover done callback function
             * 
             * @param[in] msg specifies areacover done message
             */
            void areacoverDoneCallback(const std_msgs::Empty::ConstPtr& msg);

            /**
             * @brief Leader state callback function
             * 
             * @param[in] msg specifies leader state information
             */
            void leaderStateCallback(const rosplane_msgs::State::ConstPtr& msg);

            /**
             * @brief Own state callback function
             * 
             * @param[in] msg specifies own state information
             */
            void stateCallback(const rosplane_msgs::State::ConstPtr& msg);

            /**
             * @brief Calculate control commands to controller function
             */
            void calculateCommand();  

            /**
             * @brief Discriminate events to triger function
             */
            void discriminateEvents(); 

            /**
             * @brief Visualize trajectory in rviz function
             */
            void visualizeTrajectory(); 

            /**
             * @brief Keep course angle in range function
             * 
             * @param[in] chi specifies MAV's real course angle
             * @param[out] chi_d obtains MAV's desired course angle 
             */
            void keepChiInRange(const float& chi, float& chi_desired);

            /**
             * @brief Keep flight velocity in range
             * 
             * @param[in] v_min specifies permitted mininum velocity 
             * @param[in] v_max specifies permitted maxium velocity
             * @param[out] vel_d obtain desired velocity in range
             */
            void keepVelInRange(const float& v_min, const float& v_max, float& vel_d);

            /**
             * @brief Keep flight radius in range
             * 
             * @param[in] r_min specifies permitted mininum radius 
             * @param[in] r_max specifies permitted maxium radius
             * @param[out] r_d obtain desired radius in range
             */
            void keepRadiusInRange(const float& r_min, const float& r_max, float& r_d);
            /**
             * @brief Synchronize phase angle function 
             * @param[in] v_d specifies desired velocity of MAV
             * @param[in] r_d specifies desired radius of trajectory
             * @param[in] r specifies MAV's real distance from virtual agent
             * @param[in] diff_phase specifies difference between Follower phase angle and Leader's
             * @param[out] chi_d obtain desired course angle
             * @param[out] vel_d obtain desired velocity 
             */
            void synchronizePhase(const float& v_d, const float& r_d, const float& r, const float& diff_phase, float& chi_d, float& vel_d);

            /**
             * @brief Get the Time Interval object
             */
            void getTimeInterval();

            /**
             * @brief Oobtain phase angle difference in range
             * 
             * @param[in] real_phase specifies MAV's real phase angle
             * @param[in] target_phase specifies target's real phase angle 
             * @return float obtains phase angle difference [ - M_PI, M_PI ]   
             */
            float standAngleDiff(const float& real_phase, const float& target_phase);// phase angle difference normalized function (a1-a2)
            
            /**
             * @brief Get the course angle adaptive parameter according to velcity and radius of trajectory
             * 
             * @param[in] v specifies flight velocity
             * @param[in] d specifies radius of trajectory
             * @return float adaptive parameter of course angle
             */
            float getChiParam(const float& v, const float& d);

            /**
             * @brief Get desired course angle according to LGVF formula
             * 
             * @param[in] state_N specifies MAV's real positon along Northern direction
             * @param[in] state_E specifies MAV's real positon along Eastern direction
             * @param[in] g_N specifies virtual agent's real positon along Northern direction
             * @param[in] g_E specifies virtual agent's real positon along Eastern direction
             * 
             * @param[out] r obtains MAV's real distance from virtual agent
             * @return float obtains desired course angle
             */
            float getLgvfChi(const float& state_N, const float& state_E, const float& g_N, const float& g_E, const float& r_d, float& r_real);

            /**
             * @brief Get MAV's phase angle 
             * @param[in] r_d specifies desired radius of trajectory
             * @param[in] state_N specifies MAV's real positon along Northern direction
             * @param[in] state_E specifies MAV's real positon along Eastern direction
             * @param[in] g_N specifies virtual agent's real positon along Northern direction
             * @param[in] g_E specifies virtual agent's real positon along Eastern direction
             * @return float obtains phase angle
             */
            float getPhase(const float& state_N, const float& state_E, const float& g_N, const float& g_E);

            /**
             * @brief Record data function
             * @param[in] data specifies data to be recorded
             * @param[in] file_name specifies the name of file
             * @param[in] plane_id specifies the id of MAV
             * @param[in] column_number specifies the desired columns per rank
             */
            template <typename T>
            void recordData(const std::vector<T>& data, const string& file_name, const int& plane_id, const int& column_number);

        protected:
            /**
             * @brief saves MAV's state information
             * elements: pos_N;		position of North
             *           pos_E;		position of East
             *           pos_D;		position of Down (positive above ground)
             *           Va;	  	airspeed
             *           chi;	    course angle
             */
            typedef struct state
            {
                float pos_N;							
                float pos_E;					 		 
                float pos_D;							
                float Va;	  						   
                float chi;	    					 
            }state;
            
            /* @brief saves virtual agent's position
             * elements: goal_N;	position of North
             *           goal_E;	position of East
             *           goal_D;	position of Down (positive above ground)
             */
            typedef struct goal
            {
                float goal_N;					    
                float goal_E;							 
                float goal_D;					    	 
            }goal;

        private:
            ros::NodeHandle nh_;
            ros::NodeHandle nh_private_;

            ros::Subscriber leader_state_sub_;	     ///< Indicate subscriber for leader's state
            ros::Subscriber state_sub_;	             ///< Indicate subscriber for MAV's state
            ros::Subscriber goal_sub_;	             ///< Indicate subscriber for position of virtual agents
            ros::Subscriber areacover_done_sub_;     ///< Indicate subscriber for areacover done signal
            ros::Subscriber enemy_info_sub_;         ///< Indicate subscriber for enemy position

            ros::Publisher cmd_to_controller_pub_;	 ///< Indicate publisher for msg to controller
            ros::Publisher path_to_rviz_pub_;	     ///< Indicate publisher for msg to rviz

            nav_msgs::Path path_;                    ///< Indicate msg to rviz
            geometry_msgs::Point enemy_pos_;         ///< Indicate msg for enemy position
            rosplane_msgs::Controller_Commands cmd_; ///< Indicate msg to controller_commands
            
            state Leader_state_;                     ///< Indicate structure state variable saves leader's state
            state mav_;                              ///< Indicate structure variable saves own state   
            
            goal goal_;                              ///< Indicate structure goal variable saves postion of own virtual agent
            goal leader_goal_;                       ///< Indicate structure goal variable saves postion of virtual agent of leader
            vector<goal> goals_v_;	                 ///< Indicate all the postion of virtual agents of MAVs

            string actor_name_;                      ///< Indicate actor name based on actor plugin

            bool lost_leader_state_ = false;         ///< Indicate whether the state of leader is lost
            bool last_leader_state_inited_ = false;  ///< Indicate whether the state of leader is inited
            bool goal_received_ = false;            ///< Indicate whether the postion of virtual agents are received
            bool areacover_done_received_ = false;  ///< Indicate whether the areacover done signal is received
            bool leader_state_received_ = false;    ///< Indicate whether the leader state is received
            bool enemy_info_received_ = false;      ///< Indicate whether the enemy information is received

            bool finish_event_pubed_ = false;        ///< Indicate whether finish_event is published
            bool lost_leader_event_pubed_ = false;   ///< Indicate whether lost_leader_event is published
            bool enemy_find_event_pubed_ = false;    ///< Indicate whether enemy_find_event is published

            float Klamda_;                           ///< Indicate the course angle adjustment coefficient for trajectory convergence under v0 and rd
            float rd_;								 ///< Indicate desired distance of MAV and VA
            float h_d_;								 ///< Indicate desired height of MAV 
            float v0_;								 ///< Indicate desired velocity of MAV
            float v_max_;							 ///< Indicate maximum velocity of MAV
            float v_min_;							 ///< Indicate minimum velocity of MAV
            float delta_h_;
            float last_leader_phase_ = 0;            ///< Indicate leader phase at last time 
            double latest_leader_palstance_ = 0;     ///< Indicate leader palstance of phase at last time 
            int ID_;								 ///< Indicate the number of MAV
            int total_number_;	                     ///< Indicate total number of MAVs
            int leader_id_;                          ///< Indicate the id of leader
            int begin_time_;                         ///< Indicate the start time of plugin
            int lost_leader_start_time_;             ///< Indicate the start time of lost leader state
            int lost_leader_state_start_cnt_;
            int enemy_find_duration_;                ///< Indicate the duration of enemy finded from the start of plugin
            int lost_leader_find_duration_;          ///< Indicate the duration of leader lost finded from the start of plugin  
            int sleep_duration_;                     ///< Indicate the sleep duration
            long int cnt_cycle_ = 0;                 ///< Indicate the count of entering function getTimeInterval()
            long int t_v_;                           ///< Indicate the time interval between start and end time of time probe
            struct timeval tv_start_;                ///< Indicate the start time of time probe
            struct timeval tv_end_;                  ///< Indicate the end time of time probe              
    };  // end class LeaderLgvfPlugin
} // end namespace
#endif
