/* ==================================================================
* Copyright (c) 2018, micROS Group, TAIIC, NIIDT & HPCL.
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions
* are met:
*
* 1. Redistributions of source code must retain the above copyright
* notice, this list of conditions and the following disclaimer.
* 2. Redistributions in binary form must reproduce the above copyright
* notice, this list of conditions and the following disclaimer in the
* documentation and/or other materials provided with the
* distribution.
* 3. All advertising materials mentioning features or use of this software
* must display the following acknowledgement:
* This product includes software developed by the micROS Group. and
* its contributors.
* 4. Neither the name of the Group nor the names of its contributors may
* be used to endorse or promote products derived from this software
* without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY MICROS,GROUP AND CONTRIBUTORS
* ''AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT
* NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
* FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
* THE MICROS,GROUP OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
* OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
* HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
* OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
* IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
* ===================================================================
* Author: Ren Xiaoguang, Wu Yunlong, Li Jinghua, micROS-DA Team, TAIIC.
*/

#include "follower_lgvf_plugin_sim/follower_lgvf_plugin_sim.h"
#include <pluginlib/class_list_macros.h>

PLUGINLIB_EXPORT_CLASS(general_bus::FollowerLgvfPluginSim,general_bus::GeneralPlugin)

namespace general_bus
{
void FollowerLgvfPluginSim::start()
{	
	nh_ = ros::NodeHandle();
	nh_private_ = ros::NodeHandle("~");

	nh_private_.param<int>("robot_id", ID_, 1);

	// nh_private_.param<int>("lost_leader_find_duration", lost_leader_find_duration_, 10);
	// nh_private_.param<int>("enemy_find_duration", enemy_find_duration_, 30);
	// nh_private_.param<float>("v_max", v_max_, 4);
	// nh_private_.param<float>("v_min", v_min_, 0.5);
	// nh_private_.param<float>("delta_h", delta_h_, 3.0);
	// nh_private_.param<int>("sleep_duration", sleep_duration_, 10000);

	v0_ = atof( getParam("v0").c_str() );
	rd_ = atof( getParam("rd").c_str() );
	h_d_ = atof( getParam("h_d").c_str() );
	total_number_ = atoi( getParam("total_number").c_str() );

	v_max_ = atof( getParam("v_max").c_str() );
	v_min_ = atof( getParam("v_min").c_str() );
	delta_h_ = atof( getParam("delta_h").c_str() );
	lost_leader_find_duration_ = atoi( getParam("lost_leader_find_duration").c_str() );
	enemy_find_duration_ = atoi( getParam("enemy_find_duration").c_str() );
	sleep_duration_ = atoi( getParam("sleep_duration").c_str() );

	getActorName(_actorID, actor_name_);
  for( auto &c:actor_name_ )
  {
    if(c == '/') c = '_';
  }

	path_.header.stamp=ros::Time::now();
	path_.header.frame_id="/my_frame";
	h_d_ += ID_*delta_h_;
	Klamda_ = getChiParam(v0_, rd_);
	lost_leader_start_time_ = begin_time_ = (int)ros::Time::now().toSec();
	goal_.goal_N = 0;goal_.goal_E = 0;// Unit Patrol test
	
	cmd_to_controller_pub_ = pluginAdvertise<rosplane_msgs::Controller_Commands>("controller_commands", 10);
	path_to_rviz_pub_ = pluginAdvertise<nav_msgs::Path>("trajectory", 10);

	goal_sub_ =  pluginSubscribe<std_msgs::String>("/goal_f", 10,&FollowerLgvfPluginSim::goalCallback,this);
	leader_state_sub_ = pluginSubscribe<rosplane_msgs::State>("/Leader_state_f", 10,&FollowerLgvfPluginSim::leaderStateCallback,this);
	// enemy_info_sub_ = pluginSubscribe<geometry_msgs::Point>("/enemy_find", 10,&FollowerLgvfPluginSim::enemyInfoCallback,this);
	areacover_done_sub_ = pluginSubscribe<std_msgs::Empty>("/areacover_done_f", 10,&FollowerLgvfPluginSim::areacoverDoneCallback,this);
	state_sub_ = pluginSubscribe<rosplane_msgs::State>("truth", 10,&FollowerLgvfPluginSim::stateCallback,this);
	
	while(true)
	{
		GOON_OR_RETURN;
		discriminateEvents();
		// visualizeTrajectory();
		calculateCommand();
		usleep(sleep_duration_);
	}	
}

void FollowerLgvfPluginSim::calculateCommand()
{
	float r = 0;
	float vel_d = v0_;
	float chi_d = getLgvfChi(mav_.pos_N, mav_.pos_E, goal_.goal_N, goal_.goal_E, rd_, r);
	if(goal_received_ && leader_state_received_)
	{
		float phase = getPhase(mav_.pos_N, mav_.pos_E, goal_.goal_N, goal_.goal_E);  
		float Leader_phase = getPhase(Leader_state_.pos_N, Leader_state_.pos_E, leader_goal_.goal_N, leader_goal_.goal_E); 
		if( last_leader_state_inited_ && fabs( Leader_phase - last_leader_phase_ ) < PRECISION ) 
		{
			struct timeval tv_duration_end;
			gettimeofday(&tv_duration_end, NULL);
			long int delta_tv = tv_duration_end.tv_sec*1e6 + tv_duration_end.tv_usec - (tv_end_.tv_sec*1e6 + tv_end_.tv_usec);
			std::cout<< " state LOST palstance : " << latest_leader_palstance_ << std::endl;
			Leader_phase = last_leader_phase_ + latest_leader_palstance_ * (delta_tv/1000.0);
			while(Leader_phase > M_PI)
				Leader_phase -= 2*M_PI;
			while(Leader_phase < -M_PI)
				Leader_phase += 2*M_PI;
			if(!lost_leader_state_)
			{
				lost_leader_state_start_cnt_ = 0;
				// lost_leader_start_time_ = (int)ros::Time::now().toSec();
				lost_leader_event_pubed_ = false;
				lost_leader_state_ = true;
			}
			if(lost_leader_state_start_cnt_ ++ > (1e6 / sleep_duration_) * lost_leader_find_duration_)
				lost_leader_state_start_cnt_ = (1e6 / sleep_duration_) * lost_leader_find_duration_;

		}else 
		{
			TIME_PROBE;
			if(t_v_ > 0)
				latest_leader_palstance_ = (Leader_phase - last_leader_phase_) / (t_v_/1000.0);
			std::cout<< " state UPDATED palstance : " << latest_leader_palstance_ << std::endl;
			last_leader_phase_ = Leader_phase;	
			last_leader_state_inited_ = true;
			lost_leader_state_ = false;
		}
		float diff_phase = standAngleDiff(phase, Leader_phase);
		synchronizePhase(v0_, rd_, r, diff_phase, chi_d, vel_d);
		// ROS_INFO("[Follower calculateCommand] rd_: %f, r: %f, chi_d: %f, vel_d: %f, diff_phase: %f, mav_.N: %f, mav_.E: %f, goal_.N: %f, goal_.E: %f ", rd_, r, chi_d, vel_d, diff_phase, mav_.pos_N, mav_.pos_E, goal_.goal_N, goal_.goal_E);
	}else
	{
		chi_d += Klamda_;	
		keepChiInRange(mav_.chi, chi_d);
	}
	
	cmd_.chi_c = chi_d;
	cmd_.Va_c = vel_d;
	cmd_.h_c = h_d_;
	cmd_.aux[0] = 50;
	cmd_to_controller_pub_.publish(cmd_);
}

void FollowerLgvfPluginSim::synchronizePhase(const float& v_d, const float& r_d, const float& r, const float& diff_phase, float& chi_desired, float& vel_desired)
{
	vel_desired = v_d + diff_phase*4;
	keepVelInRange(v_min_, v_max_, vel_desired);
	chi_desired += getChiParam(vel_desired, r_d);
	keepChiInRange(mav_.chi, chi_desired);	 
}

void FollowerLgvfPluginSim::discriminateEvents()
{
  if( !finish_event_pubed_ && areacover_done_received_ )
	{
		areacover_done_received_ = false;
		finish_event_pubed_ = true;
		ROS_INFO("[Follower stateCallback] Publishing finish event to UTO !");
		pubEventMsg("finish_event");

	}else if( !lost_leader_event_pubed_ && (!leader_state_received_ && ((int)ros::Time::now().toSec() > begin_time_ + lost_leader_find_duration_) 
	                                        || lost_leader_state_ && lost_leader_state_start_cnt_ >= (1e6 / sleep_duration_) * lost_leader_find_duration_ ) )
	{
		lost_leader_event_pubed_ = true;
		ROS_INFO("[Follower stateCallback] Publishing lost leader event to UTO !");
		pubEventMsg("lost_leader_event"); 

	}else if( !enemy_find_event_pubed_ && ( enemy_info_received_ || (int)ros::Time::now().toSec() >  begin_time_ + enemy_find_duration_ ))
	{
		enemy_info_received_ = false;
		enemy_find_event_pubed_ = true;
		ROS_INFO("[Follower calculateCommand] Publishing enemy event to UTO !");
		pubEventMsg("enemy_event");
	}
}

float FollowerLgvfPluginSim::getChiParam(const float& v, const float& d)
{
	return(-0.1569 +  0.0862*v - 0.006063*d - 0.0009796*v*d + 0.0009238*v*v + 0.0001031*d*d);
}

void FollowerLgvfPluginSim::keepVelInRange(const float& v_min, const float& v_max, float& vel_d)
{
	if(vel_d < v_min)
		vel_d = v_min;
	else if(vel_d > v_max)
		vel_d = v_max;
	else
		return;	
}

void FollowerLgvfPluginSim::keepRadiusInRange(const float& r_min, const float& r_max, float& r_d)
{
	if(r_d < r_min)
		r_d = r_min;
	else if(r_d > r_max)
		r_d = r_max;
	else
		return;	
}

void FollowerLgvfPluginSim::keepChiInRange(const float& chi, float& chi_desired)
{
	if(chi_desired - chi > M_PI)
		chi_desired -= 2*M_PI;
	else if(chi_desired - chi < - M_PI)
		chi_desired += 2*M_PI;
	else
		return;	
}

float FollowerLgvfPluginSim::getLgvfChi(const float& state_N, const float& state_E, const float& g_N, const float& g_E, const float& r_d, float& r_real)
{
	float delta_N = state_N - g_N;
	float delta_E = state_E - g_E;
	r_real = sqrt(pow(delta_N,2) +  pow(delta_E,2));
  float A = r_real*r_real + r_d*r_d;
  float B = r_real*r_real - r_d*r_d;
  float C = r_real*r_d;
    
  if(fabs(delta_N*B + 2*delta_E*C) > PRECISION)
  {  
    return (atan2(-((delta_E)*B - 2*(delta_N)*C), -((delta_N)*B + 2*(delta_E)*C)));
  }else
  {
    return mav_.chi;
  }
}

float FollowerLgvfPluginSim::getPhase(const float& state_N, const float& state_E, const float& g_N, const float& g_E)
{
	float delta_N = state_N - g_N;
	float delta_E = state_E - g_E;
	return (atan2(delta_N, delta_E));
}
 
void FollowerLgvfPluginSim::visualizeTrajectory()
{
	geometry_msgs::PoseStamped this_pose_stamped;
	this_pose_stamped.pose.position.x = mav_.pos_N;
	this_pose_stamped.pose.position.y = - mav_.pos_E;
	this_pose_stamped.pose.orientation.x = 0;
	this_pose_stamped.pose.orientation.y = 0;
	this_pose_stamped.pose.orientation.z = 0;
	this_pose_stamped.pose.orientation.w = 1;
	this_pose_stamped.header.stamp=ros::Time::now();
	this_pose_stamped.header.frame_id="/my_frame";

	path_.poses.push_back(this_pose_stamped);
	path_to_rviz_pub_.publish(path_);
}

void FollowerLgvfPluginSim::leaderStateCallback(const rosplane_msgs::State::ConstPtr& msg)
{
	Leader_state_.pos_N = msg->position[0];
	Leader_state_.pos_E = msg->position[1];
	Leader_state_.pos_D = - msg->position[2];
	Leader_state_.Va = msg->Va;
	Leader_state_.chi = msg->chi;
	
	leader_state_received_ = true;
}

void FollowerLgvfPluginSim::stateCallback(const rosplane_msgs::State::ConstPtr& msg)
{
	mav_.pos_N = msg->position[0];
	mav_.pos_E = msg->position[1];
	mav_.pos_D = - msg->position[2];
	mav_.Va = msg->Va;
	mav_.chi = msg->chi;
}

void FollowerLgvfPluginSim::goalCallback(const std_msgs::String::ConstPtr& msg)
{
	memcpy(&leader_id_,&msg->data[0],4);
	memcpy(&rd_,&msg->data[4],4);
	goals_v_.resize((msg->data).size()/8-1);
	for(int i = 0; i < goals_v_.size(); i++)
	{
		memcpy(&goals_v_[i].goal_N,&msg->data[8*i+8],4);
		memcpy(&goals_v_[i].goal_E,&msg->data[8*i+12],4);
	}
	leader_goal_ = goals_v_[leader_id_-1];
	goal_ = goals_v_[ID_-1];
	goal_received_ = true;	

	// printf("[goalCallback] goals_v_.size(): %d\n", (int)goals_v_.size());
    // for(auto &c:goals_v_)
		// printf("****************** %f %f\n", c.goal_N, c.goal_E);
}

void FollowerLgvfPluginSim::enemyInfoCallback(const geometry_msgs::Point::ConstPtr& msg)
{
	enemy_info_received_ = true;
	enemy_pos_ = *msg;
}

void FollowerLgvfPluginSim::areacoverDoneCallback(const std_msgs::Empty::ConstPtr& msg)
{
	areacover_done_received_ = true;
}

float FollowerLgvfPluginSim::standAngleDiff(const float& real_phase, const float& target_phase)
{
	float diff = real_phase - target_phase;
  if (diff < -M_PI && diff >= -2*M_PI)  
		return (diff + 2*M_PI);
  else if (diff <= 2*M_PI && diff > M_PI) 
		return (diff - 2*M_PI);
  else 
		return diff;
}

void FollowerLgvfPluginSim::getTimeInterval()
{
  if( cnt_cycle_ == 0 )
  {
    gettimeofday(&tv_start_,NULL);
    t_v_ = -1;
		cnt_cycle_++;
  }else
  {
    gettimeofday(&tv_end_,NULL);
    t_v_ = tv_end_.tv_sec*1e6 + tv_end_.tv_usec - tv_start_.tv_sec*1e6 - tv_start_.tv_usec;   
    tv_start_.tv_sec = tv_end_.tv_sec;
    tv_start_.tv_usec = tv_end_.tv_usec;
  }
}

template <typename T>
void FollowerLgvfPluginSim::recordData(const std::vector<T>& data, const string& file_name, const int& plane_id, const int& column_number)
{
    int i = 0;
    string file = "/home/ljh/Data/" + file_name + "_" + actor_name_ + "_" + std::to_string(plane_id) + ".txt";
    char fp_o[100];
    snprintf( fp_o, file.size()+1, "%s",file.c_str() );
    FILE *pFile_o = fopen( fp_o, "a" );
    for( auto &c:data )
    {
        fprintf( pFile_o, "%f ",c);
        if( (++i) % column_number == 0 )
            fprintf( pFile_o, "\n");
    }
    fclose( pFile_o );
}

} 



