#ifndef FOLLOWER_NAVIGATE_PLUGIN_SIM_H
#define FOLLOWER_NAVIGATE_PLUGIN_SIM_H

#include <ros/ros.h>
#include <rosplane_msgs/State.h>
#include <rosplane_msgs/Current_Path.h>
#include <rosplane_msgs/Waypoint.h>
#include <rosplane_msgs/Controller_Commands.h>
#include <rosplane_msgs/State_Path.h>
#include <rosplane_msgs/Formation.h>
#include <math.h>
#include <Eigen/Eigen>
//#include <sensor_msgs/Imu.h>
//#include <std_msgs/Float32.h>
//#include <std_msgs/Float32MultiArray.h>
//#include <sensor_msgs/FluidPressure.h>
//#include <rosplane/ControllerConfig.h>
#include "general_plugin/general_plugin.h"

#define EARTH_RADIUS 6378137.0f //6378145.0f
#define M_PI_F 3.14159265358979323846f
#define M_PI_2_F 1.57079632679489661923f


namespace general_bus
{
/*
enum class fillet_state
{
  STRAIGHT,
  ORBIT
};

enum class dubin_state
{
  FIRST,
  BEFORE_H1,
  BEFORE_H1_WRONG_SIDE,
  STRAIGHT,
  BEFORE_H3,
  BEFORE_H3_WRONG_SIDE
};
*/
enum class path_type
{
  Orbit,
  Line
};

class FollowerNavigatePluginSim : public GeneralPlugin
{
public:
  FollowerNavigatePluginSim() {};
  ~FollowerNavigatePluginSim() {};
  virtual void start();

protected:
  struct waypoint_s
  {
    float w[3];
    float chi_d;
    bool  chi_valid;
    float Va_d;
  };
  waypoint_s default_waypoint_, threat_;
  //int num_waypoints_;

  struct plane_state_s
  {
    float pn;  /** position north */
    float pe;  /** position east */
    float h;   /** altitude */
    float chi; /** course angle */
    float va;
  };
  plane_state_s plane_state_, leader_state_;

  struct plane_path_s
  {
    bool flag;     /** Inicates strait line or orbital path (true is line, false is orbit) */
    float Va_d;    /** Desired airspeed (m/s) */
    float r[3];    /** Vector to origin of straight line path (m) */
    float q[3];    /** Unit vector, desired direction of travel for line path */
    float c[3];    /** Center of orbital path (m) */
    float rho;     /** Radius of orbital path (m) */
    //float h_c;
    int8_t lambda; /** Direction of orbital path (cw is 1, ccw is -1) */
  };
  plane_path_s plane_path_;

  struct plane_cmd_s
  {
    double Va_c;             /** commanded airspeed (m/s) */
    double h_c;              /** commanded altitude (m) */
    double chi_c;            /** commanded course (rad) */
    double phi_ff;           /** feed forward term for orbits (rad) */
  };  
  plane_cmd_s plane_cmd_;

  struct params_s
  {
    double R_min;
    double v_max;
    double v_min;
    double v_default;
    double delta_diff;
    double h_default;
    double x_diff;
    double y_diff;
    int lost_leader_counter_threshold;
    //double z_diff;
    double chi_infty;
    double k_path;
    double k_orbit;
  };
  struct params_s params_;

  
private:
  //ros::NodeHandle nh_;
  ros::NodeHandle nh_private_;

  ros::Subscriber plane_state_sub_; /**< vehicle state subscription */
  void planeStateCallback(const rosplane_msgs::StateConstPtr &msg);
  bool state_init_;

//  ros::Subscriber default_waypoint_sub_;  /**< new waypoint subscription */
//  void gatheringWaypointCallback(const rosplane_msgs::WaypointConstPtr &msg);

  ros::Subscriber leader_state_path_sub_;
  void leaderStatePathCallback(const rosplane_msgs::State_PathConstPtr &msg);
  bool leader_state_path_init_; 

  ros::Subscriber formation_type_sub_;
  void formationTypeCallback(const rosplane_msgs::FormationConstPtr &msg);

  ros::Publisher controller_commands_pub_;   /**< controller commands publication */
  void currentControlCmdGenerate();

  void getXYDiffInFormation(int formation_type, int id);
  void checkThreat();

//  void manageLine(const struct params_s &params, const struct input_s &input, struct output_s &output);
//  void manageFillet(const struct params_s &params, const struct input_s &input, struct output_s &output);
//  fillet_state fil_state_;
/*
  dubin_state dub_state_;
  void manageDubins(const struct params_s &params, const struct plane_state_s &input, struct plane_path_s &output);
  Eigen::Matrix3f rotz(float theta);
  float mo(float in);  
  void dubinsParameters(const struct waypoint_s start_node, const struct waypoint_s end_node, float R);
*/
  void followDubins(const struct params_s &params, const struct plane_state_s &input1, const struct plane_path_s &input2, struct plane_cmd_s &output);
  void followerFollowDubins(const struct params_s &params, const struct plane_state_s &input1, const struct plane_path_s &input2, const struct plane_state_s &input3, struct plane_cmd_s &output);

  int ID_, id_in_swarm_, cur_formation_, pre_formation_;
  int lost_leader_counter_;
  bool done_, select_leader_msg_sent_, formation_changed_;

};
} // namespace general_bus

#endif
