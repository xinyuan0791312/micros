/* ==================================================================
* Copyright (c) 2018, micROS Group, NIIDT & HPCL.
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions
* are met:
*
* 1. Redistributions of source code must retain the above copyright
* notice, this list of conditions and the following disclaimer.
* 2. Redistributions in binary form must reproduce the above copyright
* notice, this list of conditions and the following disclaimer in the
* documentation and/or other materials provided with the
* distribution.
* 3. All advertising materials mentioning features or use of this software
* must display the following acknowledgement:
* This product includes software developed by the micROS Group. and
* its contributors.
* 4. Neither the name of the Group nor the names of its contributors may
* be used to endorse or promote products derived from this software
* without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY MICROS,GROUP AND CONTRIBUTORS
* ''AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT
* NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
* FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
* THE MICROS,GROUP OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
* OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
* HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
* OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
* IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
* ===================================================================
* Author: micROS-DA Team.
*/

#include "follower_navigate_plugin_sim.h"
#include <pluginlib/class_list_macros.h>

#define PLANE_NUM_IN_SWARM 30

#ifndef NUM_PLANE_PER_FORMATION
#define NUM_PLANE_PER_FORMATION 15
#endif
#ifndef LEADER_TARGET_REACHED
#define LEADER_TARGET_REACHED -1 
#endif



namespace general_bus
{

PLUGINLIB_EXPORT_CLASS(general_bus::FollowerNavigatePluginSim, general_bus::GeneralPlugin)

void FollowerNavigatePluginSim::start()
{
  nh_private_ = ros::NodeHandle("~");
  nh_private_.param<int>("robot_id", ID_, 0);

  nh_private_.param<double>("Chi_infty", params_.chi_infty, 1.0472);  
  nh_private_.param<double>("K_PATH", params_.k_path, 0.015);//0.025
  nh_private_.param<double>("K_ORBIT", params_.k_orbit, 4.0);

  nh_private_.param<double>("R_min", params_.R_min, 6.0); //25.0
  nh_private_.param<double>("V_max", params_.v_max, 1.0); //20.0
  nh_private_.param<double>("V_min", params_.v_min, 0.1); //8.0
  nh_private_.param<double>("V_default", params_.v_default, 0.5); //12.0
  nh_private_.param<double>("Delta_diff", params_.delta_diff, 3.0); //5.0
  nh_private_.param<double>("H_default", params_.h_default, 3.0); //30.0
  nh_private_.param<int>("Lost_leader_counter_threshold", params_.lost_leader_counter_threshold, 100); //30.0
  nh_private_.param<float>("Default_x", default_waypoint_.w[0], 0.0);
  nh_private_.param<float>("Default_y", default_waypoint_.w[1], 0.0); 
  nh_private_.param<float>("Threat_x", threat_.w[0], 0.0);
  nh_private_.param<float>("Threat_y", threat_.w[1], 0.0);
  nh_private_.param<float>("Threat_r", threat_.w[2], 100.0);

  params_.R_min = atof(getParam("R_min").c_str());
  params_.v_max = atof(getParam("V_max").c_str());
  params_.v_min = atof(getParam("V_min").c_str());
  params_.v_default = atof(getParam("V_default").c_str());
  params_.delta_diff = atof(getParam("Delta_diff").c_str());
  params_.h_default = atof(getParam("H_default").c_str());
  params_.lost_leader_counter_threshold = atoi(getParam("Lost_leader_counter_threshold").c_str());
  default_waypoint_.w[0] = atof(getParam("Default_x").c_str());
  default_waypoint_.w[1] = atof(getParam("Default_y").c_str());
  threat_.w[0] = atof(getParam("Threat_x").c_str());
  threat_.w[1] = atof(getParam("Threat_y").c_str());
  threat_.w[2] = atof(getParam("Threat_r").c_str());
  ////ROS_INFO("T_X: %f %s, T_Y: %f %s",default_waypoint_.w[0], default_waypoint_.w[1], getParam("Gather_x").c_str(), getParam("Gather_y").c_str());

  nh_private_.param<double>("X_diff", params_.x_diff, -1*params_.delta_diff); 
  nh_private_.param<double>("Y_diff", params_.y_diff, params_.delta_diff); 
  nh_private_.param<float>("Default_z", default_waypoint_.w[2], -params_.h_default-params_.delta_diff*((ID_-1)%PLANE_NUM_IN_SWARM+1)); 
  nh_private_.param<float>("Default_chi", default_waypoint_.chi_d, 0.0);
  nh_private_.param<float>("Default_v", default_waypoint_.Va_d, params_.v_default);
  default_waypoint_.chi_valid = true;

  plane_path_.flag = false;
  plane_path_.c[0] = default_waypoint_.w[0];
  plane_path_.c[1] = default_waypoint_.w[1];
  plane_path_.c[2] = default_waypoint_.w[2];
  plane_path_.Va_d = default_waypoint_.Va_d;      
  plane_path_.rho = params_.R_min;
  plane_path_.lambda = 1;

  plane_state_sub_ = pluginSubscribe("truth", 10, &FollowerNavigatePluginSim::planeStateCallback, this);  
  leader_state_path_sub_ = pluginSubscribe("/leader/state_path_f", 1, &FollowerNavigatePluginSim::leaderStatePathCallback, this);
  //leader_state_path_sub_ = pluginSubscribe("/leader/state_path", 1, &FollowerNavigatePluginSim::leaderStatePathCallback, this);
  formation_type_sub_ = pluginSubscribe("/formation_type", 1, &FollowerNavigatePluginSim::formationTypeCallback, this);

  controller_commands_pub_ = pluginAdvertise<rosplane_msgs::Controller_Commands>("controller_commands", 1);

  state_init_ = false;
  leader_state_path_init_ = false;

  done_ = false;

  lost_leader_counter_ = 0;
  select_leader_msg_sent_ = false;

  formation_changed_ = false;
  pre_formation_ = -1;
  cur_formation_ = 0;
  id_in_swarm_ = ID_;
  getXYDiffInFormation(cur_formation_, id_in_swarm_);

  while (true)
  {
    GOON_OR_RETURN;
    if(state_init_)
    {
	int id_in_swarm_n_ = getFormationPos() + 1;
	if(id_in_swarm_n_ != 0 && id_in_swarm_n_ != id_in_swarm_)
	{
                ROS_INFO("ID in swarm: %d --> %d ... \n", id_in_swarm_, id_in_swarm_n_);
		id_in_swarm_ = id_in_swarm_n_;
		getXYDiffInFormation(cur_formation_, id_in_swarm_);
	}

        if(formation_changed_)
	{
		ROS_INFO("transform formation to %d ... \n", cur_formation_);
		formation_changed_ = false;
		getXYDiffInFormation(cur_formation_, id_in_swarm_);
	}

    	currentControlCmdGenerate();
	checkThreat();
    }
    usleep(10000);
  }
}


void FollowerNavigatePluginSim::checkThreat()
{
  float distance_to_threat_ = sqrt((plane_state_.pn-threat_.w[0])*(plane_state_.pn-threat_.w[0])+(plane_state_.pe-threat_.w[1])*(plane_state_.pe-threat_.w[1]));
  if(distance_to_threat_ <= threat_.w[2])
  {
     if(cur_formation_ != 2)
     {
	pre_formation_ = cur_formation_;
        cur_formation_ = 2;
	formation_changed_ = true;
     }
  }
  else
  {
     if(pre_formation_ != -1)
     {
	cur_formation_ = pre_formation_;
        pre_formation_ = -1;
	formation_changed_ = true;
     }
  }
}


void FollowerNavigatePluginSim::getXYDiffInFormation(int formation_type, int id)
{
////uncomment the following line if getFormationPos() is called  
  int ID_in_formation_ = (id-1)%NUM_PLANE_PER_FORMATION;
////otherwise
//  int ID_in_formation_ = (id+NUM_PLANE_PER_FORMATION-3)%NUM_PLANE_PER_FORMATION; 

  int row_ = (id-1)/NUM_PLANE_PER_FORMATION;
  int swarm_ = (id-1)/PLANE_NUM_IN_SWARM;

  int side_ = (ID_in_formation_%2==0)? -1 : 1;  //-1:middile or on left; 1:on right
  int id_on_side = (ID_in_formation_+1)/2;
  
  if(formation_type == 0)
  {

/*
	  switch(ID_in_formation_)
	  {
	    case 0: 
		params_.x_diff = 0;
		params_.y_diff = 0;
		break; 
	    case 1: 
		params_.x_diff = -1*params_.delta_diff;
		params_.y_diff =    params_.delta_diff;
		break;        
	    case 2: 
		params_.x_diff = -1*params_.delta_diff;
		params_.y_diff = -1*params_.delta_diff; 
		break;              
	    case 3: 
		params_.x_diff = -2*params_.delta_diff;
		params_.y_diff =  2*params_.delta_diff;
		break;      
	    case 4: 
		params_.x_diff = -2*params_.delta_diff;
		params_.y_diff = -2*params_.delta_diff;
		break;  
	  }
*/
     params_.x_diff = -id_on_side*params_.delta_diff;
     params_.y_diff = side_*id_on_side*params_.delta_diff;
  }
  else if(formation_type == 1)
  {
/*
	  switch(ID_in_formation_)
	  {
	    case 0:    
		params_.x_diff = 0;
		params_.y_diff = 0;
		break;   
	    case 1: 
		params_.x_diff = 0;
		params_.y_diff = params_.delta_diff;
		break;        
	    case 2: 
		params_.x_diff = 0;
		params_.y_diff = -1*params_.delta_diff; 
		break;             
	    case 3: 
		params_.x_diff = 0;
		params_.y_diff = 2*params_.delta_diff;
		break;    
	    case 4: 
		params_.x_diff = 0;
		params_.y_diff = -2*params_.delta_diff;
		break;  
	  }
*/
     params_.x_diff = 0;
     params_.y_diff = side_*id_on_side*params_.delta_diff;
  }
  else if(formation_type == 2)
  {
/*
          switch(ID_in_formation_)
          {
            case 0:
                params_.x_diff = 0;
                params_.y_diff = 0;
                break;
            case 1:
                params_.x_diff = 0;
                params_.y_diff = 3*params_.delta_diff;
                break;
            case 2:
                params_.x_diff = 0;
                params_.y_diff = -3*params_.delta_diff;
                break;
            case 3:
                params_.x_diff = 0;
                params_.y_diff = 6*params_.delta_diff;
                break;
            case 4:
                params_.x_diff = 0;
                params_.y_diff = -6*params_.delta_diff;
                break;
          }
          ////default_waypoint_.w[2] = -params_.h_default-params_.delta_diff*ID_; //-50.0-6*ID_;
*/
     params_.x_diff = 0;
     params_.y_diff = side_*id_on_side*params_.delta_diff*2;
  }
  params_.x_diff += -(swarm_*15+(row_%(PLANE_NUM_IN_SWARM/NUM_PLANE_PER_FORMATION))*3)*params_.delta_diff;
}


void FollowerNavigatePluginSim::planeStateCallback(const rosplane_msgs::StateConstPtr &msg)
{
  plane_state_.pn = msg->position[0];
  plane_state_.pe = msg->position[1];
  plane_state_.h = -msg->position[2];
  plane_state_.chi = msg->chi;
  plane_state_.va = msg->Va;
  if(!state_init_)
    state_init_ = true;
}


void FollowerNavigatePluginSim::leaderStatePathCallback(const rosplane_msgs::State_PathConstPtr &msg)
{
  if(!leader_state_path_init_)
    leader_state_path_init_ = true;
  lost_leader_counter_ = 0;

  leader_state_.pn = msg->position[0];
  leader_state_.pe = msg->position[1];
  leader_state_.h = -msg->position[2];
  leader_state_.chi = msg->chi;
  leader_state_.va = msg->Va;
  plane_path_.flag = msg->path_type;
  plane_path_.r[0] = msg->r[0];
  plane_path_.r[1] = msg->r[1];
  plane_path_.r[2] = msg->r[2];
  plane_path_.q[0] = msg->q[0];
  plane_path_.q[1] = msg->q[1];
  plane_path_.q[2] = msg->q[2];
  plane_path_.c[0] = msg->c[0];
  plane_path_.c[1] = msg->c[1];
  plane_path_.c[2] = msg->c[2];    
  plane_path_.Va_d = msg->Va_d;      
  plane_path_.lambda = msg->lambda;
    
  if(msg->rho == LEADER_TARGET_REACHED)
  {
    if(!done_)
    {
      ROS_INFO( "[Follower Navigate] reached the target waypoint, pub uto event finish_event");
      pubEventMsg("finish_event");  
      done_ = true;      
    }
    plane_path_.rho = params_.R_min + 20.0; 
    //leader_state_path_init_ = false; 
  }
  else
    plane_path_.rho = msg->rho;  
}


void FollowerNavigatePluginSim::formationTypeCallback(const rosplane_msgs::FormationConstPtr &msg)
{
  cur_formation_ = msg->formation;
  formation_changed_ = true;
}


void FollowerNavigatePluginSim::currentControlCmdGenerate()
{
  if(!done_)
  {
    if(!leader_state_path_init_)
    {
      if(ID_ == 3 && !select_leader_msg_sent_)
      {
        ROS_INFO( "[Follower Navigate] leader state & path unknown, pub uto event select_leader_event");
        pubEventMsg("select_leader_event");
        select_leader_msg_sent_ = true;
      }
    }
    else
    {
      lost_leader_counter_++;
      if(lost_leader_counter_ == params_.lost_leader_counter_threshold)  //leader unexist for 1 second
      {
      /*
      if(leader_state_path_init_)
      {
        plane_path_.flag = false;
        plane_path_.c[0] = plane_state_.pn;
        plane_path_.c[1] = plane_state_.pe;
        plane_path_.c[2] = default_waypoint_.w[2];
        plane_path_.Va_d = default_waypoint_.Va_d;      
        plane_path_.rho = params_.R_min;
        plane_path_.lambda = 1;
        leader_state_path_init_ = false;         
      }
      */      
        if(ID_ <= NUM_PLANE_PER_FORMATION)
        {
          ROS_INFO( "[Follower Navigate] leader lost, pub uto event lost_leader_event");
          pubEventMsg("lost_leader_event");
        }
      }
    }
  }

  if(!leader_state_path_init_)
  {
    followDubins(params_, plane_state_, plane_path_, plane_cmd_);
  }
  else
  {
    followerFollowDubins(params_, plane_state_, plane_path_, leader_state_, plane_cmd_);
  }

  rosplane_msgs::Controller_Commands current_control_cmd;
  current_control_cmd.chi_c = plane_cmd_.chi_c;
  current_control_cmd.Va_c = plane_cmd_.Va_c;
  current_control_cmd.h_c = plane_cmd_.h_c;
  current_control_cmd.phi_ff = plane_cmd_.phi_ff;//667;  

  if(!std::isnormal(current_control_cmd.chi_c))
	current_control_cmd.chi_c = 0;
  if(!std::isnormal(current_control_cmd.Va_c))
        current_control_cmd.Va_c = params_.v_default;
  if(!std::isnormal(current_control_cmd.h_c))
        current_control_cmd.h_c = params_.h_default+params_.delta_diff*((ID_-1)%PLANE_NUM_IN_SWARM+1);

  controller_commands_pub_.publish(current_control_cmd);
}


void FollowerNavigatePluginSim::followDubins(const struct params_s &params, const struct plane_state_s &input1, const struct plane_path_s &input2, struct plane_cmd_s &output)
{
  if (input2.flag)// == path_type::Line // follow straight line path specified by r and q
  {
    // compute wrapped version of the path angle
    float chi_q = atan2f(input2.q[1], input2.q[0]);
    while (chi_q - input1.chi < -M_PI)
      chi_q += 2*M_PI;
    while (chi_q - input1.chi > M_PI)
      chi_q -= 2*M_PI;

    float path_error = -sinf(chi_q)*(input1.pn - input2.r[0]) + cosf(chi_q)*(input1.pe - input2.r[1]);
    // heading command
    output.chi_c = chi_q - params.chi_infty*2/M_PI*atanf(params_.k_path*path_error);

    // desired altitude
    float h_d = -input2.r[2] - sqrtf(powf((input2.r[0] - input1.pn), 2) + powf((input2.r[1] - input1.pe),
                                         2))*(input2.q[2])/sqrtf(powf(input2.q[0], 2) + powf(input2.q[1], 2));
    //ROS_INFO("[Follower Navigate] r_path:(%f,%f,%f), q_path:(%f,%f,%f), h_d:%f", input2.r[0], input2.r[1], input2.r[2], input2.q[0], input2.q[1], input2.q[2], h_d);
    // commanded altitude is desired altitude
    output.h_c = h_d;
    output.phi_ff = 0.0;
  }
  else // follow a orbit path specified by c_orbit, rho_orbit, and lam_orbit
  {
    float d = sqrtf(powf((input1.pn - input2.c[0]), 2) + powf((input1.pe - input2.c[1]), 2)); // distance from orbit center
    // compute wrapped version of angular position on orbit
    float varphi = atan2f(input1.pe - input2.c[1], input1.pn - input2.c[0]);
    while ((varphi - input1.chi) < -M_PI)
      varphi += 2*M_PI;
    while ((varphi - input1.chi) > M_PI)
      varphi -= 2*M_PI;
    //compute orbit error
    float norm_orbit_error = (d - input2.rho)/input2.rho;
    output.chi_c = varphi + input2.lambda*(M_PI/2 + atanf(params_.k_orbit*norm_orbit_error));
    //ROS_INFO("[Follower Navigate] d,varphi,norm_orbit_error,chi_c:(%f,%f,%f,%f)", d,varphi,norm_orbit_error,output.chi_c);
    // commanded altitude is the height of the orbit
    float h_d = -input2.c[2];
    output.h_c = h_d;
    //改动：
    output.phi_ff = 0;//(norm_orbit_error < 0.5 ? input.lam_orbit*atanf(input.Va*input.Va/(9.8*input.rho_orbit)) : 0);
  }
  output.Va_c = input2.Va_d;
}


void FollowerNavigatePluginSim::followerFollowDubins(const params_s &params, const struct plane_state_s &input1, const struct plane_path_s &input2, const struct plane_state_s &input3, struct plane_cmd_s &output)
{
  if (input2.flag) // follow straight line path specified by r and q
  {
    // compute wrapped version of the path angle  ///P187: the equation under (10.8)
    float chi_q = atan2f(input2.q[1], input2.q[0]);

    ////changed by Hao
    while (chi_q - input1.chi < -M_PI)
      chi_q += 2*M_PI;
    while (chi_q - input1.chi > M_PI)
      chi_q -= 2*M_PI;

    ///P183: the equation above (10.2) 
    float path_error = -sinf(chi_q)*(input1.pn - input3.pn) + cosf(chi_q)*(input1.pe - input3.pe);
    path_error = path_error - params.y_diff;
    // heading command  ///P187: (10.8)
    output.chi_c = chi_q - params.chi_infty*2/M_PI*atanf(path_error*params.k_path*15);

    // desired altitude  ///P184: (10.5)
    float h_d = -input2.r[2] - sqrtf(powf((input2.r[0] - input1.pn), 2) + powf((input2.r[1] - input1.pe),
                                         2))*(input2.q[2])/sqrtf(powf(input2.q[0], 2) + powf(input2.q[1], 2));
    // commanded altitude is desired altitude
    output.h_c = -default_waypoint_.w[2];//params.z_diff;//h_d + params.z_diff;//input3.h;
    
    ///calculate the line speed 
    path_error = cosf(chi_q)*(input1.pn - input3.pn) + sinf(chi_q)*(input1.pe - input3.pe);
    ///if path_error < 0, speed-up; if path_error > 0, slow-down.
    path_error = path_error - params.x_diff;
    double v_delta = (path_error>=0)? input3.va : params_.v_max-input3.va ;
    //double v_delta = (input.lVa < params.v_infty-input.lVa) ? input.lVa : params.v_infty-input.lVa;
    
    output.Va_c = input3.va - v_delta*2/M_PI*atanf(path_error*params.k_path*10);
    output.Va_c = (output.Va_c < params_.v_min) ? params_.v_min : output.Va_c;
    //if(output.Va_c > params.v_infty)
      //output.Va_c = params.v_infty;

    output.phi_ff = 0.0;
  }
  else // follow a orbit path specified by c_orbit, rho_orbit, and lam_orbit
  {
    float c_orbit_n = input2.c[0];    ////might need to optimise this
    float c_orbit_e = input2.c[1];    ////might need to optimise this
    float r_orbit = input2.rho - input2.lambda*params.y_diff;

    // compute wrapped version of angular position on orbit  ///P190: the equation under (10.13)
    float varphi = atan2f(input1.pe - c_orbit_e, input1.pn - c_orbit_n);
    while ((varphi - input1.chi) < -M_PI)
      varphi += 2*M_PI;
    while ((varphi - input1.chi) > M_PI)
      varphi -= 2*M_PI;
    //compute orbit error  ///P190: (10.13)
    float d = sqrtf(powf((input1.pn - c_orbit_n), 2) + powf((input1.pe - c_orbit_e), 2)); // distance from orbit center    
    float norm_orbit_error = (d - r_orbit)/r_orbit;
    output.chi_c = varphi + input2.lambda*(M_PI/2 + atanf(norm_orbit_error*params.k_orbit));

    // commanded altitude is the height of the orbit
    float h_d = -input2.c[2];
    output.h_c = -default_waypoint_.w[2];//params.z_diff;//h_d + params.z_diff;

    // calculate the line speed 
    output.Va_c = input3.va*(r_orbit/input2.rho);   ///the speed depends on the orbit rho    
    float va = output.Va_c;

////DA (added by Hao): calculate the target angle difference between leader & follower
    float edge_a, edge_b, edge_c, target_phi = 0.0, phi = 0.0;
    ////if target_phi_side<0, the vector direction of < leader-->follower > should be CCW; 
    ////if target_phi_side>0, the vector direction of < leader-->follower > should be CW; 
    float target_phi_side = -input2.lambda*params.x_diff; 
    if(target_phi_side != 0) {
      edge_a = powf(input2.rho, 2);
      edge_b = powf(r_orbit, 2);
      edge_c = powf(params.y_diff, 2)+powf(params.x_diff, 2);
      target_phi = acosf((edge_a+edge_b-edge_c)/(2*sqrtf(edge_a*edge_b)));
    }  
    ////if target_phi_side<0, the vector direction of < leader-->follower > now is CCW; 
    ////if target_phi_side>0, the vector direction of < leader-->follower > now is CW;     
    float phi_side = ((input3.pe-c_orbit_e)*input1.pn+(c_orbit_n-input3.pn)*input1.pe+input3.pn*c_orbit_e-c_orbit_n*input3.pe); //-input.lam_orbit*
    if(phi_side != 0){
      edge_a = powf((input3.pe - c_orbit_e), 2)+powf((input3.pn - c_orbit_n), 2);
      edge_b = powf((input1.pe - c_orbit_e), 2)+powf((input1.pn - c_orbit_n), 2);
      edge_c = powf((input1.pn - input3.pn), 2)+powf((input1.pe - input3.pe), 2);
      phi = acosf((edge_a+edge_b-edge_c)/(2*sqrtf(edge_a*edge_b)));
    }
    //ROS_WARN("target_phi_side: %f, target_phi: %f, phi_side: %f, phi: %f, input.lam_orbit: %d",target_phi_side,target_phi,phi_side,phi,input.lam_orbit);
 
    ////if phi_error > 0, speed-up; if phi_error < 0, slow-down;
    float phi_error = 0;
    if(target_phi_side*phi_side > 0){
      phi_error = phi - target_phi;
      if(target_phi_side > 0)
          phi_error = (input2.lambda < 0)? -phi_error:phi_error;
      else
          phi_error = (input2.lambda < 0)? phi_error:-phi_error;
    }
    else if(target_phi_side*phi_side < 0){
        phi_error = phi + target_phi;
        if(target_phi_side > 0)
          phi_error = (input2.lambda < 0)? phi_error:-phi_error;
        else
          phi_error = (input2.lambda < 0)? -phi_error:phi_error;
    }
    else if((target_phi_side*phi_side == 0) && (target_phi_side+phi_side != 0)){
      if(target_phi_side == 0){
        if(phi_side > 0)
          phi_error = (input2.lambda < 0)? -phi:phi;
        else
          phi_error = (input2.lambda < 0)? phi:-phi;
      }
      else{
        if(target_phi_side > 0)
          phi_error = (input2.lambda < 0)? target_phi:-target_phi;
        else
          phi_error = (input2.lambda < 0)? -target_phi:target_phi;        
      }
    }

    while(phi_error < -M_PI)
      phi_error = phi_error + 2*M_PI;
    while(phi_error > M_PI)
      phi_error = phi_error - 2*M_PI; 
    double v_delta_max = (phi_error <= 0)? output.Va_c : params_.v_max-output.Va_c;  
    output.Va_c = output.Va_c + v_delta_max*sinf(phi_error*0.5); //*params.k_orbit*4   
    output.Va_c = (output.Va_c < params_.v_min) ? params_.v_min : output.Va_c; 
////////
    /// ??? not mentioned in Chapter 10
    output.phi_ff = 0;//(norm_orbit_error < 0.5 ? input.lam_orbit*atanf(input.Va*input.Va/(9.8*input.rho_orbit)) : 0);
  }
}

} // namespace general_bus
