#include "takeoff_base_plugin_sim.h"
#include <pluginlib/class_list_macros.h>

namespace general_bus
{

 std::string Command_str;
 std::string takeoffCommand_adv;

PLUGINLIB_EXPORT_CLASS(general_bus::takeoff_base_plugin_sim, general_bus::GeneralPlugin)

/**  Start plugin */
void takeoff_base_plugin_sim::start()
{
  ros::NodeHandle nh_private_("~"); 
  vehicle_state_sub_ = pluginSubscribe<rosplane_msgs::State>("truth", 10, &takeoff_base_plugin_sim::vehicle_state_callback, this);   
  nh_private_.param<double>("CHI_INFTY", params_.chi_infty, 1.0472);
  nh_private_.param<double>("K_PATH", params_.k_path, 0.025);
  nh_private_.param<double>("K_ORBIT", params_.k_orbit, 4.0);
  nh_private_.param<int>("takeoff_h", takeoff_h, -50);
  nh_private_.param<int>("R_min", R_min, 25);
  nh_private_.param<int>("robot_id", ID_, 1);
  nh_private_.param<float>("High_c_takeoff", high_c_takeoff, 25);
  nh_private_.param<float>("High_0", high_0, 2);
  nh_private_.param<float>("High_1", high_1, 20);

  high_c_takeoff = atof(getParam("High_c_takeoff").c_str());
  high_0 = atof(getParam("High_0").c_str());
  high_1 = atof(getParam("High_1").c_str());

  
  controller_commands_pub_ = pluginAdvertise<rosplane_msgs::Controller_Commands>("controller_commands", 10);

  std::string str_ID_sub = std::to_string(ID_);
  Command_str = str_ID_sub; /** ID */
  takeoff_commands_pub_1 = pluginSubscribe<rosplane_msgs::Takeoff_Command>("/takeoff_commands_f", 10, &takeoff_base_plugin_sim::takeoff_callback, this);

  std::string str_ID_adv = std::to_string(ID_ + 1);
  takeoffCommand_adv = str_ID_adv; /** ID+1 */
  takeoff_commands_pub_2 = pluginAdvertise<rosplane_msgs::Takeoff_Command>("/takeoff_commands", 10);

  state_init_ = false;
  event_flag = true;
  takeoff_now = false;

  input_.p_type = Orbit;
  input_.Va_d = 8; 

  input_.c_orbit[0] = 500;       /** x */
  input_.c_orbit[1] = 0;         /** y */
  input_.c_orbit[2] = takeoff_h; /** z */

  input_.rho_orbit = R_min;
  input_.lam_orbit = 1;
  input_.h_c_path = takeoff_h;
 

  while (true)
  {
    if (ID_ == 1)
    {
      rosplane_msgs::Controller_Commands msg;
      msg.chi_c = 0;
      msg.Va_c = 10;
      msg.h_c = 50; 
      msg.phi_ff = 0;
      controller_commands_pub_.publish(msg); 
    }

    GOON_OR_RETURN;
    usleep(10000);
  }
}

/** Callback function for takeoff command subscribe */
void takeoff_base_plugin_sim::takeoff_callback(const rosplane_msgs::Takeoff_CommandConstPtr &takeoffmsg)
{
  if (Command_str.compare(takeoffmsg->Delay) == 0) 
  {
    takeoff_now = true;
  }
}

/** Callback function for takeoff command advertise */
void takeoff_base_plugin_sim::vehicle_state_callback(const rosplane_msgs::StateConstPtr &msg)
{
  input_.pn = msg->position[0]; /** position north */
  input_.pe = msg->position[1]; /** position east */
  input_.h = -msg->position[2]; /** -altitude */
  input_.chi = msg->chi;
  input_.Va = msg->Va;
  state_init_ = true;

  if (input_.h > high_0 && input_.h < high_1) 
  {
    rosplane_msgs::Takeoff_Command takeoff_commmand;  
    takeoff_commmand.Delay = takeoffCommand_adv;      
    takeoff_commands_pub_2.publish(takeoff_commmand); /** ID+1 */
  }
  
  if (event_flag && input_.h >= high_c_takeoff) //25
  {
    pubEventMsg("finish_event");
    event_flag = false;
  }
  
  if (takeoff_now)
  {
    rosplane_msgs::Controller_Commands msg;
    msg.chi_c = 0;                         
    msg.Va_c = 10;                         
    msg.h_c = 50;                          
    msg.phi_ff = 0;                        
    controller_commands_pub_.publish(msg); 
  } 
}

} /** namespace general_bus */
