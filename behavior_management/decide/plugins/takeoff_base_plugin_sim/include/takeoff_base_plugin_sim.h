/* ==================================================================
* Copyright (c) 2018, micROS Group, TAIIC, NIIDT & HPCL.
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions
* are met:
*
* 1. Redistributions of source code must retain the above copyright
* notice, this list of conditions and the following disclaimer.
* 2. Redistributions in binary form must reproduce the above copyright
* notice, this list of conditions and the following disclaimer in the
* documentation and/or other materials provided with the
* distribution.
* 3. All advertising materials mentioning features or use of this software
* must display the following acknowledgement:
* This product includes software developed by the micROS Group. and
* its contributors.
* 4. Neither the name of the Group nor the names of its contributors may
* be used to endorse or promote products derived from this software
* without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY MICROS,GROUP AND CONTRIBUTORS
* ''AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT
* NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
* FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
* THE MICROS,GROUP OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
* OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
* HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
* OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
* IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
* ===================================================================
* Author: zhaolin, micROS Group
*/


#ifndef _takeoff_base_plugin_sim_
#define _takeoff_base_plugin_sim_

#include "general_plugin/general_plugin.h"
#include <ros/ros.h>
#include <rosplane_msgs/State.h>
#include <rosplane_msgs/Controller_Commands.h>
#include <rosplane_msgs/Takeoff_Command.h>
#include <string>

#define EARTH_RADIUS 6378145.0f

namespace general_bus
{
enum path_types
{
  Orbit,
  Line
};

class takeoff_base_plugin_sim : public GeneralPlugin
{

public:
  virtual void start();

protected:
  struct input_s
  {
    enum path_types p_type;
    float Va_d;
    float r_path[3];
    float q_path[3];
    float c_orbit[3];
    float rho_orbit;
    int lam_orbit;
    float pn;       /** position north */
    float pe;       /** position east */
    float h;        /** altitude */
    float Va;       /** airspeed */
    float chi;      /** course angle */
    float h_c_path; /** goal attitude */
  };

  struct output_s
  {
    double Va_c;   /** commanded airspeed (m/s) */
    double h_c;    /** commanded altitude (m) */
    double chi_c;  /** commanded course (rad) */
    double phi_ff; /** feed forward term for orbits (rad) */
  };

  struct params_s
  {
    double chi_infty;
    double k_path;
    double k_orbit;
  };

private:
  ros::Subscriber vehicle_state_sub_;
  ros::Publisher controller_commands_pub_;
  ros::Subscriber takeoff_commands_pub_1;
  ros::Publisher takeoff_commands_pub_2;
  
  void takeoff_callback(const rosplane_msgs::Takeoff_CommandConstPtr &msg);

  struct params_s params_;
  struct input_s input_; 
 

  void vehicle_state_callback(const rosplane_msgs::StateConstPtr &takeoffmsg);

  int takeoff_h;
  int R_min;
  int ID_;
  bool event_flag;
  bool state_init_;
  bool takeoff_now;

  float high_c_takeoff;
  float high_0;
  float high_1;
};

} // namespace general_bus

#endif