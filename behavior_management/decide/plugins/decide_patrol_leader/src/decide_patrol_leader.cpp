/* ==================================================================
* Copyright (c) 2018, micROS Group, TAIIC, NIIDT & HPCL.
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions
* are met:
*
* 1. Redistributions of source code must retain the above copyright
* notice, this list of conditions and the following disclaimer.
* 2. Redistributions in binary form must reproduce the above copyright
* notice, this list of conditions and the following disclaimer in the
* documentation and/or other materials provided with the
* distribution.
* 3. All advertising materials mentioning features or use of this software
* must display the following acknowledgement:
* This product includes software developed by the micROS Group. and
* its contributors.
* 4. Neither the name of the Group nor the names of its contributors may
* be used to endorse or promote products derived from this software
* without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY MICROS,GROUP AND CONTRIBUTORS
* ''AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT
* NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
* FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
* THE MICROS,GROUP OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
* OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
* HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
* OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
* IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
* ===================================================================
* Author: Ren Xiaoguang, Wu Yunlong, Li Jinghua, micROS-DA Team, NIIDT, TAIIC.
*/

#include "decide_patrol_leader/decide_patrol_leader.h"

namespace decide_patrol_leader
{
  #ifdef RUN_ALONE
    DecidePatrolLeader::DecidePatrolLeader()
    {
      start();
    }
  #endif
  
  void DecidePatrolLeader::start()
  {
    //  Step 1 : Initialize arguments  
    initArgs();
    
    //  Step 2 : Create Control Nodes and Leaf Nodes of Behavior Tree  
    BT::FallbackNode* _fallbackRoot = new BT::FallbackNode("_fallbackRoot");
    BT::DecideActionNode<decide_action_interface::DecideActionInterface>
      *_actionVA = new BT::DecideActionNode<decide_action_interface::DecideActionInterface>("_actionVA");
    
    //  Step 3 : Set Action Nodes and Condition Nodes  
    _actionVA->setParam(_paramBuffer[0]);
    _actionVA->setMethod("decide_softbus", 
                         "decide_action_interface::DecideActionInterface", 
                         "fwing_decide_action_virtual_impl::FwingDecideActionVirtualImpl");
    _actionVA->setBuffer(_inputGlobalBuffer, _outputGlobalBuffer);
    
    //  Step 4 : Build Behavior Tree  
    _root->AddChild(_fallbackRoot);
    _fallbackRoot->AddChild(_actionVA);
    
    ros::Rate loop_rate(1);
    while(ros::ok())
    {
      #ifdef RUN_ALONE
        ros::spinOnce();
      #else
        GOON_OR_RETURN
      #endif

      if(_areaChanged)
      {
        //  Step 5 : Update Buffers  
        updateBuffers();

        //  Step 6 : Execute Behavior Tree  
        BT::ReturnStatus state = _root->Tick();

        _areaChanged = false;
      }

      //  Step 7 : Analyze _outputGlobalBuffer and send plan  
      sendPlan(_outputGlobalBuffer); 
      
      if (_root->get_status() != BT::RUNNING)
      {
          _root->ResetColorState();
      }
      
      loop_rate.sleep();
    } 
    //  Step 8 : Release memory of Control Nodes and Leaf Nodes of Behavior Tree  
    delete _fallbackRoot;
    delete _actionVA;
  }

  void DecidePatrolLeader::initArgs()
  {
    _nh = ros::NodeHandle();
    _nhPrivate = ros::NodeHandle("~");
    
    #ifdef RUN_ALONE
      _runAlone = true;
    #else
      _runAlone = false;  
    #endif

    _nhPrivate.param<int>("robot_id", _mavID, 1);
    
    if(_runAlone)
    {
      _nhPrivate.param<std::string>("vertices", _originRegionS, "0 0 150 0 150 150 0 150"); 
      _nhPrivate.param<float>("rd", _payload.rd, 50);
      _nhPrivate.param<int>("total_number", _payload.total_number, 4);
    }
    else
    {
      _originRegionS = getParam("vertices");
      _payload.rd = atof( getParam("rd").c_str() );
      _payload.total_number = atoi( getParam("total_number").c_str() );
    }
    _areaChanged = true;

    getAreaVertices(_originRegionS, _payload.origin_region);
    _payload.region_size = _payload.origin_region.size();
    _payload.struct_size = sizeof(_payload);
    
    addParamBuffer();
    param_precision_t tmp = {1e-5};
    _paramBuffer[0]->setPayload(tmp);
    
    _inputGlobalBuffer->setValue("type", 8);
    _inputGlobalBuffer->setValue("error_code", 0);

    _outputGlobalBuffer->setValue("type", 7);
    _outputGlobalBuffer->setValue("error_code", 0);

    _goalsPub = advertiseDA<std_msgs::String>("/goal", 10, _runAlone);
  }

  void DecidePatrolLeader::sendPlan(unified_msg::unified_message_t *pOutBuffer)
  {
    int va_v_size = 0;
    if(pOutBuffer->error_code[0] == 0x00 && pOutBuffer->error_code[1] == 0x00 && pOutBuffer->error_code[2] == 0x00)
    {
      memcpy(&va_v_size, &(pOutBuffer->payload[0]), sizeof(int));
      _goalsOut.virtual_agents.resize(va_v_size/4);
      memcpy(&_goalsOut.virtual_agents[0], &(pOutBuffer->payload[sizeof(int)]), va_v_size);
      
      _goalsMsg.data.resize(va_v_size + 8);
      memcpy(&(_goalsMsg.data[0]), &_mavID, 4);
      memcpy(&(_goalsMsg.data[4]), &_payload.rd, 4);
        
      memcpy(&(_goalsMsg.data[8]), &(_goalsOut.virtual_agents[0]), va_v_size);
    }else
    {
      ROS_INFO("[DecidePatrolLeader] _outputGlobalBuffer type is WRONG !\n");
    }
    
    if(va_v_size != 0)
    {
      _goalsPub.publish(_goalsMsg); 

    }else
    {
      ROS_INFO(" [DecidePatrolLeader] No points to deploy !\n");
    }	
  } 

  void DecidePatrolLeader::updateBuffers()
  {
    _inputGlobalBuffer->setPayloadPtr(_payload);
  }

  void DecidePatrolLeader::getAreaVertices(const std::string& aVertexSetS, 
                                                 std::vector<float>& anOriginRegion)
  {
    std::vector<std::string> aRet;
    std::string::size_type posOne, posTwo;
    posTwo = aVertexSetS.find(" ");
    posOne = 0;
    int cnt = 0;
    while(std::string::npos != posTwo) 
    {
      aRet.push_back(aVertexSetS.substr(posOne, posTwo-posOne));
      posOne = posTwo + 1;
      posTwo = aVertexSetS.find(" ", posOne);
      cnt++;
    }
    if(posOne != aVertexSetS.length())
    {
      aRet.push_back(aVertexSetS.substr(posOne));
      cnt++;
    }
    anOriginRegion.resize(cnt);
    for(int i = 0; i < cnt; i++)
    {
      anOriginRegion[i] = atof(aRet.at(i).c_str());
    }
  }

} // end namespace


#ifdef RUN_ALONE
  int main(int argc, char** argv)
  {
    ros::init(argc, argv, "decide_patrol_leader");

    ros::NodeHandle n; 
    
    decide_patrol_leader::DecidePatrolLeader da_plugin;

    ROS_INFO("Decide Test BT 1");

    return 0; 
  }
#else
  PLUGINLIB_EXPORT_CLASS(decide_patrol_leader::DecidePatrolLeader, general_bus::GeneralPlugin)
#endif
