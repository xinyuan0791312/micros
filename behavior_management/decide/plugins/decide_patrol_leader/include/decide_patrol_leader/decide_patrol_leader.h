/* ==================================================================
* Copyright (c) 2018, micROS Group, TAIIC, NIIDT & HPCL.
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions
* are met:
*
* 1. Redistributions of source code must retain the above copyright
* notice, this list of conditions and the following disclaimer.
* 2. Redistributions in binary form must reproduce the above copyright
* notice, this list of conditions and the following disclaimer in the
* documentation and/or other materials provided with the
* distribution.
* 3. All advertising materials mentioning features or use of this software
* must display the following acknowledgement:
* This product includes software developed by the micROS Group. and
* its contributors.
* 4. Neither the name of the Group nor the names of its contributors may
* be used to endorse or promote products derived from this software
* without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY MICROS,GROUP AND CONTRIBUTORS
* ''AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT
* NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
* FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
* THE MICROS,GROUP OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
* OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
* HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
* OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
* IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
* ===================================================================
* Author: Ren Xiaoguang, Wu Yunlong, Li Jinghua, micROS-DA Team, NIIDT, TAIIC.
*/

#ifndef DECIDE_PATROL_LEADER_H
#define DECIDE_PATROL_LEADER_H

#include "message_types.h"
#include "da_adapter/da_adapter.h"
#include "decide_softbus/decide_softbus.h"
#include <pluginlib/class_list_macros.h>

#include <std_msgs/String.h>
#include "rosplane_msgs/State.h"
#include "rosplane_msgs/Controller_Commands.h"

#include "common/param_precision.h"
#include "common/decide/action/decide_action_virtual_agent.h"
#include "common/decide/action/decide_action_virtual_agent_out.h"

/**
 * @brief Fwing Decide Patrol Leader Namespace.
 */
namespace decide_patrol_leader
{
  /**
   * @brief This is a class to implement the actor plugin for actor decide_patrol_leader.
   */
  class DecidePatrolLeader : public decide_softbus::DecideSoftbus, public da_adapter::DaAdapter
  {
    public:

      #ifdef RUN_ALONE
        DecidePatrolLeader();
      #endif

      /**
       * @brief Overload start function of General Plugin.
       */
      virtual void start();

      /**
       * @brief Initialize arguments 
       * for instance, remapping parameters from launch files or description files, etc.
       */
      virtual void initArgs();

    private:

      ros::NodeHandle _nh;
      ros::NodeHandle _nhPrivate;
      
      ros::Publisher _goalsPub;        
      
      decide_action_virtual_agent_t _payload;  ///< This is the struct saving input data of the loaded algorithm plugin
      decide_action_va_out_t _goalsOut;  ///< This is the struct saving output data of the loaded algorithm plugin

      std_msgs::String _goalsMsg;  ///< This is the ros message saving postions of virtual agents
            
      std::string _originRegionS;  ///< This is the ordered vertices of region to be covered in form of std::string
                  
      bool _runAlone;  ///< This specifies whether the plugin is running alone or not
      bool _areaChanged = false;  ///< This specifies whether the area is changed or not      

      int _mavID;  ///< This is the identification number of the MAV
            
      /**
       * @brief Broadcast the result of the loaded algorithm plugin. 
       * 
       * @param[in] pOutBuffer specifies pointer variable pointing to the output global buffer of the loaded algorithm plugin 
       */
      void sendPlan(unified_msg::unified_message_t *pOutBuffer);  

      /**
       * @brief Covert the ordered vertices of the region from type std::string to type std::vector.
       * 
       * @param[in] aVertexSetS specifies the ordered vertices of region in form of std::string 
       * @param[out] anOriginRegion obtains the ordered vertices of region in form of std::vector
       */
      void getAreaVertices(const std::string& aVertexSetS, 
                                 std::vector<float>& anOriginRegion);
      
      /**
       * @brief Update buffers, for instance input buffers, param buffers, etc.
       * 
       */
      void updateBuffers(); 
  };
} //end namespace

#endif
