#include "landing_path_follower_base_plugin_sim.h"
#include <pluginlib/class_list_macros.h>

PLUGINLIB_EXPORT_CLASS(general_bus::LandingPathFollowerBasePlugin, general_bus::GeneralPlugin)

namespace general_bus
{
/**  Start plugin */
void LandingPathFollowerBasePlugin::start()
{
  vehicle_state_sub_ = pluginSubscribe<rosplane_msgs::State>("truth", 1, &LandingPathFollowerBasePlugin::vehicle_state_callback, this);
  current_path_sub_ = pluginSubscribe<rosplane_msgs::Current_Path>("current_path", 1, &LandingPathFollowerBasePlugin::current_path_callback, this);

  nh_private_.param<double>("CHI_INFTY", params_.chi_infty, 1.0472);
  nh_private_.param<double>("K_PATH", params_.k_path, 0.025);
  nh_private_.param<double>("K_ORBIT", params_.k_orbit, 4.0);

  controller_commands_pub_ = pluginAdvertise<rosplane_msgs::Controller_Commands>("controller_commands", 1);

  state_init_ = false;
  current_path_init_ = false;

  while (true)
  {
    GOON_OR_RETURN;
    update();
    usleep(10000);
  }
}

/** function for update */
void LandingPathFollowerBasePlugin::update()
{
  struct output_s output;
  if (state_init_ == true && current_path_init_ == true)
  {
    follow(params_, input_, output);
    rosplane_msgs::Controller_Commands current_control_cmd; 
    current_control_cmd.chi_c = output.chi_c;
    current_control_cmd.Va_c = output.Va_c;
    current_control_cmd.h_c = output.h_c;
    current_control_cmd.phi_ff = output.phi_ff;

    current_control_cmd.landing = input_.landing;
    current_control_cmd.aux[0] = 55555;

    if (std::isnan(current_control_cmd.chi_c))
    {
      ROS_FATAL("[landing_path_follower_base_plugin_sim.cpp]:chi_c caught nan.");
    }
    if (std::isnan(current_control_cmd.Va_c))
    {
      ROS_FATAL("[landing_path_follower_base_plugin_sim.cpp]:Va_c caught nan.");
    }
    if (std::isnan(current_control_cmd.h_c))
    {
      ROS_FATAL("[landing_path_follower_base_plugin_sim.cpp]:h_c caught nan.");
    }
    if (std::isnan(current_control_cmd.phi_ff))
    {
      ROS_FATAL("[landing_path_follower_base_plugin_sim.cpp]:phi_ff caught nan.");
    }

    controller_commands_pub_.publish(current_control_cmd);
  }
}

/** Callback function for vehicle state */
void LandingPathFollowerBasePlugin::vehicle_state_callback(const rosplane_msgs::StateConstPtr &msg)
{
  input_.pn = msg->position[0]; /** position north */
  input_.pe = msg->position[1]; /** position east */
  input_.h = -msg->position[2]; /** position altitude */
  input_.chi = msg->chi;
  input_.Va = msg->Va;
  state_init_ = true;
  /** ROS_WARN("[landing_path_follower_base_plugin_sim.cpp]---vehicle_state_callback()"); */
}

/** function for current path publish */
void LandingPathFollowerBasePlugin::current_path_callback(const rosplane_msgs::Current_PathConstPtr &msg)
{
  if (msg->path_type == msg->LINE_PATH)
    input_.p_type = path_type::Line;
  else if (msg->path_type == msg->ORBIT_PATH)
    input_.p_type = path_type::Orbit;

  input_.Va_d = msg->Va_d;
  for (int i = 0; i < 3; i++)
  {
    input_.r_path[i] = msg->r[i];
    input_.q_path[i] = msg->q[i];
    input_.c_orbit[i] = msg->c[i];
  }
  input_.rho_orbit = msg->rho;
  input_.lam_orbit = msg->lambda;
  input_.landing = msg->landing;
  
  current_path_init_ = true;
}

/** function for path follow */
void LandingPathFollowerBasePlugin::follow(const params_s &params, const input_s &input, output_s &output)
{
  if (input.p_type == path_type::Line)
  {
    float chi_q = atan2f(input.q_path[1], input.q_path[0]);
    while (chi_q - input.chi < -M_PI)
      chi_q += 2.0 * M_PI;
    while (chi_q - input.chi > M_PI)
      chi_q -= 2.0 * M_PI;

    float path_error = -sinf(chi_q) * (input.pn - input.r_path[0]) + cosf(chi_q) * (input.pe - input.r_path[1]);
    output.chi_c = chi_q - params.chi_infty * 2 / M_PI * atanf(params.k_path * path_error);
    float h_d = -input.r_path[2] - sqrtf(powf((input.r_path[0] - input.pn), 2) + powf((input.r_path[1] - input.pe), 2)) * (input.q_path[2]) / sqrtf(powf(input.q_path[0], 2) + powf(input.q_path[1], 2));
    
    //ROS_INFO("r_path:(%f,%f,%f), q_path:(%f,%f,%f), h_d:%f", input.r_path[0], input.r_path[1], input.r_path[2], input.q_path[0], input.q_path[1], input.q_path[2], h_d);

    output.h_c = h_d;
    output.phi_ff = 0.0;
  }
  else
  {
    float d = sqrtf(powf((input.pn - input.c_orbit[0]), 2) + powf((input.pe - input.c_orbit[1]), 2));

    float varphi = atan2f(input.pe - input.c_orbit[1], input.pn - input.c_orbit[0]);
    while ((varphi - input.chi) < -M_PI)
      varphi += 2.0 * M_PI;
    while ((varphi - input.chi) > M_PI)
      varphi -= 2.0 * M_PI;

    float norm_orbit_error = (d - input.rho_orbit) / input.rho_orbit;
    output.chi_c = varphi + input.lam_orbit * (M_PI / 2.0 + atanf(params.k_orbit * norm_orbit_error));
    float h_d = -input.c_orbit[2];
    output.h_c = h_d;
    /** output.phi_ff = (norm_orbit_error < 0.5 ? input.lam_orbit * atanf(input.Va * input.Va / (9.8 * input.rho_orbit)) : 0); */
    output.phi_ff = 0.0;
  }
  output.Va_c = input.Va_d;
}

} /** namespace general_bus */