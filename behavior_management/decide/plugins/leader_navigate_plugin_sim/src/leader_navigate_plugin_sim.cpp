/* ==================================================================
* Copyright (c) 2018, micROS Group, NIIDT & HPCL.
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions
* are met:
*
* 1. Redistributions of source code must retain the above copyright
* notice, this list of conditions and the following disclaimer.
* 2. Redistributions in binary form must reproduce the above copyright
* notice, this list of conditions and the following disclaimer in the
* documentation and/or other materials provided with the
* distribution.
* 3. All advertising materials mentioning features or use of this software
* must display the following acknowledgement:
* This product includes software developed by the micROS Group. and
* its contributors.
* 4. Neither the name of the Group nor the names of its contributors may
* be used to endorse or promote products derived from this software
* without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY MICROS,GROUP AND CONTRIBUTORS
* ''AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT
* NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
* FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
* THE MICROS,GROUP OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
* OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
* HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
* OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
* IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
* ===================================================================
* Author: micROS-DA Team.
*/

#include "leader_navigate_plugin_sim.h"
#include <pluginlib/class_list_macros.h>
/*
#ifndef NUM_PLANE_PER_FORMATION
#define NUM_PLANE_PER_FORMATION 3
#endif
*/
#ifndef LEADER_TARGET_REACHED
#define LEADER_TARGET_REACHED -1 
#endif

namespace general_bus
{

PLUGINLIB_EXPORT_CLASS(general_bus::LeaderNavigatePluginSim, general_bus::GeneralPlugin)

void LeaderNavigatePluginSim::start()
{
  nh_private_ = ros::NodeHandle("~");
  nh_private_.param<int>("robot_id", ID_, 0);

  nh_private_.param<double>("Speed", params_.speed_v, 0.5); //0.5
  nh_private_.param<double>("R_min", params_.R_min, 6); //25.0+50.0
  nh_private_.param<double>("Delta_diff", params_.delta_diff, 3.0); 
  nh_private_.param<double>("H_default", params_.h_default, 3.0);
  nh_private_.param<float>("Target_x", targeting_waypoint_.w[0], 0.0);
  nh_private_.param<float>("Target_y", targeting_waypoint_.w[1], 0.0);

  params_.speed_v = atof(getParam("Speed").c_str());
  params_.R_min = atof(getParam("R_min").c_str());
  params_.delta_diff = atof(getParam("Delta_diff").c_str());
  params_.h_default = atof(getParam("H_default").c_str());
  targeting_waypoint_.w[0] = atof(getParam("Target_x").c_str());
  targeting_waypoint_.w[1] = atof(getParam("Target_y").c_str());
  //ROS_INFO("[Leader Navigate] T_X: %f %s, T_Y: %f %s",targeting_waypoint_.w[0], targeting_waypoint_.w[1], getParam("Target_x").c_str(), getParam("Target_y").c_str());

  nh_private_.param<double>("Chi_infty", params_.chi_infty, 1.0472); 
  nh_private_.param<double>("K_PATH", params_.k_path, 0.025);
  nh_private_.param<double>("K_ORBIT", params_.k_orbit, 4.0);
 
  nh_private_.param<float>("Target_z", targeting_waypoint_.w[2], -params_.h_default-params_.delta_diff*ID_); 
  nh_private_.param<float>("Target_chi", targeting_waypoint_.chi_d, 0.0);
  //nh_private_.param<int>("Gather_chi_valid", targeting_waypoint_.chi_valid, 1); 
  nh_private_.param<float>("Target_v", targeting_waypoint_.Va_d, params_.speed_v); 
  //nh_private_.param<double>("Z_diff", params_.z_diff, 30.0);  
  //targeting_waypoint_.w[2] = -(50 + params_.z_diff * (ID_-1)/NUM_PLANE_PER_FORMATION);
  targeting_waypoint_.chi_valid = true;

  plane_state_sub_ = pluginSubscribe("truth", 10, &LeaderNavigatePluginSim::planeStateCallback, this);
  targeting_waypoint_sub_ = pluginSubscribe("/gathering_waypoint", 10, &LeaderNavigatePluginSim::gatheringWaypointCallback, this);

  leader_state_path_pub_ = pluginAdvertise<rosplane_msgs::State_Path>("/leader/state_path", 1);
  controller_commands_pub_ = pluginAdvertise<rosplane_msgs::Controller_Commands>("controller_commands", 1);

  formation_type_sub_ = pluginSubscribe("/formation_type", 1, &LeaderNavigatePluginSim::formationTypeCallback, this);
  //ROS_INFO("[Leader Navigate] Plane with ID %d is now the leader!",ID_);
  
  num_waypoints_ = 0;
  state_init_ = false;
  //fil_state_ = fillet_state::STRAIGHT;
  dub_state_ = dubin_state::FIRST;
  num_lap_ = 0;
  done_ = false;

  while (true)
  {
    GOON_OR_RETURN;
    if(state_init_)
    {
    	currentPathGenerate();
    	currentControlCmdGenerate();
    }
    usleep(10000);
  }
}

void LeaderNavigatePluginSim::planeStateCallback(const rosplane_msgs::StateConstPtr &msg)
{
  plane_state_.pn = msg->position[0];
  plane_state_.pe = msg->position[1];
  plane_state_.h = -msg->position[2];
  plane_state_.chi = msg->chi;
  plane_state_.va = msg->Va;
  if(!state_init_)
    state_init_ = true;
}

void LeaderNavigatePluginSim::gatheringWaypointCallback(const rosplane_msgs::WaypointConstPtr &msg)
{
  targeting_waypoint_.w[0] = msg->w[0];
  targeting_waypoint_.w[1] = msg->w[1];
  targeting_waypoint_.w[2] = msg->w[2];
  targeting_waypoint_.chi_d = msg->chi_d;
  targeting_waypoint_.chi_valid = msg->chi_valid;
  targeting_waypoint_.Va_d = msg->Va_d;

  //fil_state_ = fillet_state::STRAIGHT;
  dub_state_ = dubin_state::FIRST;
  ROS_INFO("[Leader Navigate] new target waypoint is received!\n");
}

void LeaderNavigatePluginSim::formationTypeCallback(const rosplane_msgs::FormationConstPtr &msg)
{
  int formation_t = msg->formation;

  if(formation_t == 0 || formation_t == 1)
  {
     targeting_waypoint_.Va_d = 12;
  //   targeting_waypoint_.w[2] = -50.0-5*ID_;
  }
  else if(formation_t == 2)
  {
     targeting_waypoint_.Va_d = 16;
  //   targeting_waypoint_.w[2] = -75.0-5*ID_;
  }
}

void LeaderNavigatePluginSim::currentPathGenerate()
{
  manageDubins(params_, plane_state_, plane_path_);

  rosplane_msgs::State_Path current_leader_state_path;
  current_leader_state_path.position[0] = plane_state_.pn;
  current_leader_state_path.position[1] = plane_state_.pe; 
  current_leader_state_path.position[2] = -plane_state_.h;  
  current_leader_state_path.Va = plane_state_.va;     
  current_leader_state_path.chi = plane_state_.chi;
  if (plane_path_.flag)
    current_leader_state_path.path_type = current_leader_state_path.LINE_PATH;
  else
    current_leader_state_path.path_type = current_leader_state_path.ORBIT_PATH;
  current_leader_state_path.Va_d = plane_path_.Va_d;
  for (int i = 0; i < 3; i++)
  {
    current_leader_state_path.r[i] = plane_path_.r[i];
    current_leader_state_path.q[i] = plane_path_.q[i];
    current_leader_state_path.c[i] = plane_path_.c[i];    
  }
  current_leader_state_path.rho = plane_path_.rho;
  current_leader_state_path.lambda = plane_path_.lambda;

  if(done_)
    current_leader_state_path.rho = LEADER_TARGET_REACHED;

  leader_state_path_pub_.publish(current_leader_state_path);
}

void LeaderNavigatePluginSim::currentControlCmdGenerate()
{
  followDubins(params_, plane_state_, plane_path_, plane_cmd_);

  rosplane_msgs::Controller_Commands current_control_cmd;
  current_control_cmd.chi_c = plane_cmd_.chi_c;
  current_control_cmd.Va_c = plane_cmd_.Va_c;
  current_control_cmd.h_c = plane_cmd_.h_c;
  current_control_cmd.phi_ff = plane_cmd_.phi_ff;//666;// 

  if(!std::isnormal(current_control_cmd.chi_c))
	current_control_cmd.chi_c = 0;
  if(!std::isnormal(current_control_cmd.Va_c))
        current_control_cmd.Va_c = params_.speed_v;
  if(!std::isnormal(current_control_cmd.h_c))
        current_control_cmd.h_c = params_.h_default+params_.delta_diff*ID_; 

  controller_commands_pub_.publish(current_control_cmd);
}

void LeaderNavigatePluginSim::manageDubins(const params_s &params, const plane_state_s &input, plane_path_s &output)
{
  Eigen::Vector3f p;
  //ROS_INFO("input.pn: %f, input.pe: %f, -input.h: %f",input.pn, input.pe, -input.h);
  p << input.pn, input.pe, -input.h;
  /*  uint8 path_type		# Indicates strait line or orbital path
      float32 Va_d		# Desired airspeed (m/s)
      float32[3] r		# Vector to origin of straight line path (m)
      float32[3] q		# Unit vector, desired direction of travel for line path
      float32[3] c		# Center of orbital path (m)
      float32 rho		# Radius of orbital path (m)
      int8 lambda		# Direction of orbital path (clockwise is 1, counterclockwise is -1)

      uint8 ORBIT_PATH = 0
      uint8 LINE_PATH = 1

      int8 CLOCKWISE = 1
      int8 COUNT_CLOCKWISE = -1 */
  output.Va_d = params.speed_v;//waypoint_end_.Va_d;
  output.r[0] = 0;
  output.r[1] = 0;
  output.r[2] = 0;
  output.q[0] = 0;
  output.q[1] = 0;
  output.q[2] = 0;
  output.c[0] = 0;
  output.c[1] = 0;
  output.c[2] = 0;
  //ROS_INFO("[Leader Navigate] manager: dub_state_: %d",dub_state_);
  switch (dub_state_)
  {
  case dubin_state::FIRST:
   {
    // path planning
    // Algorithm11 P211
    float waypoint_dis_ = sqrtf((targeting_waypoint_.w[0] - input.pn)*(targeting_waypoint_.w[0] - input.pn) +
                    (targeting_waypoint_.w[1] - input.pe)*(targeting_waypoint_.w[1] - input.pe));

    if (waypoint_dis_ <= 2.0*params_.R_min)
    {
      ROS_WARN_THROTTLE(4, "The distance to gathering_waypoint must be larger than 2R. Hault at current location...");
      if(num_waypoints_ != 1)
      {
        waypoint_start_.w[0] = input.pn;
        waypoint_start_.w[1] = input.pe;
        waypoint_start_.w[2] = targeting_waypoint_.w[2];//-input.h;  
        waypoint_start_.chi_d = input.chi;
        waypoint_start_.chi_valid = targeting_waypoint_.chi_valid;
        waypoint_start_.Va_d = params.speed_v;//input.va; 
        num_waypoints_ = 1;
      }
      output.flag = false;
      output.c[0] = waypoint_start_.w[0];
      output.c[1] = waypoint_start_.w[1];
      output.c[2] = waypoint_start_.w[2];
      output.Va_d = waypoint_start_.Va_d;      
      output.rho = params.R_min;
      if(waypoint_start_.chi_d > 0)
        output.lambda = 1;
      else
        output.lambda = -1;
      break;
    }

    waypoint_start_.w[0] = input.pn;
    waypoint_start_.w[1] = input.pe;
    waypoint_start_.w[2] = targeting_waypoint_.w[2];//-input.h;  
    waypoint_start_.chi_d = input.chi;
    waypoint_start_.chi_valid = targeting_waypoint_.chi_valid;
    waypoint_start_.Va_d = params_.speed_v;//targeting_waypoint_.Va_d; 
    waypoint_end_ = targeting_waypoint_;
    num_waypoints_ = 2;         
      
    dubinsParameters(waypoint_start_, waypoint_end_, params.R_min);

    output.flag = false;
    output.c[0] = dubinspath_.cs(0);
    output.c[1] = dubinspath_.cs(1);
    output.c[2] = dubinspath_.cs(2);
    output.rho = dubinspath_.R;
    output.lambda = dubinspath_.lams;
    // p: current pos
    // w1: waypoint 1
    // q1: unit vector of H1
    if ((p - dubinspath_.w1).dot(dubinspath_.q1) >= 0) // start in H1
    {
      dub_state_ = dubin_state::BEFORE_H1_WRONG_SIDE;
    }
    else
    {
      dub_state_ = dubin_state::BEFORE_H1;
    }
    break;
   }

  case dubin_state::BEFORE_H1:
    output.flag = false;
    output.c[0] = dubinspath_.cs(0);
    output.c[1] = dubinspath_.cs(1);
    output.c[2] = dubinspath_.cs(2);
    output.rho = dubinspath_.R;
    output.lambda = dubinspath_.lams;
    if ((p - dubinspath_.w1).dot(dubinspath_.q1) >= 0) // entering H1
    {
      dub_state_ = dubin_state::STRAIGHT;
    }
    break;

  case dubin_state::BEFORE_H1_WRONG_SIDE:   
    output.flag = false;
    output.c[0] = dubinspath_.cs(0);
    output.c[1] = dubinspath_.cs(1);
    output.c[2] = dubinspath_.cs(2);
    output.rho = dubinspath_.R;
    output.lambda = dubinspath_.lams;
    if ((p - dubinspath_.w1).dot(dubinspath_.q1) < 0) // exit H1
    {
      dub_state_ = dubin_state::BEFORE_H1;
    }
    break;

  case dubin_state::STRAIGHT:
    output.flag = true;
    output.r[0] = dubinspath_.w1(0);
    output.r[1] = dubinspath_.w1(1);
    output.r[2] = dubinspath_.w1(2);
    // output.r[0] = dubinspath_.z1(0);
    // output.r[1] = dubinspath_.z1(1);
    // output.r[2] = dubinspath_.z1(2);
    output.q[0] = dubinspath_.q1(0);
    output.q[1] = dubinspath_.q1(1);
    output.q[2] = dubinspath_.q1(2);
    output.rho = 1;
    output.lambda = 1;

    if ((p - dubinspath_.w2).dot(dubinspath_.q1) >= 0) // entering H2
    {
      if ((p - dubinspath_.w3).dot(dubinspath_.q3) >= 0) // start in H3
      {
        dub_state_ = dubin_state::BEFORE_H3_WRONG_SIDE;
      }
      else
      {
        dub_state_ = dubin_state::BEFORE_H3;
      }
      if(!done_);// && num_lap_==3)
      {
          done_ = true;
          ROS_INFO( "[Leader Navigate] reached the target waypoint, pub uto event finish_event");
          pubEventMsg("finish_event");
      }
    }
    break;

  case dubin_state::BEFORE_H3:
    output.flag = false;
    output.c[0] = dubinspath_.ce(0);
    output.c[1] = dubinspath_.ce(1);
    output.c[2] = dubinspath_.ce(2);
    output.rho = dubinspath_.R;
    output.lambda = dubinspath_.lame;

    if ((p - dubinspath_.w3).dot(dubinspath_.q3) >= 0) // start in H3
    {
      dub_state_ = dubin_state::BEFORE_H3_WRONG_SIDE;
/*
      num_lap_++;
      if(!done_)  //&& num_lap_==1
      {
          done_ = true;
          ROS_INFO( "[Leader Navigate] reached the target waypoint, pub uto event finish_event");
			    pubEventMsg("finish_event"); 
      }
*/
    }
    break;

  case dubin_state::BEFORE_H3_WRONG_SIDE:
    output.flag = false;
    output.c[0] = dubinspath_.ce(0);
    output.c[1] = dubinspath_.ce(1);
    output.c[2] = dubinspath_.ce(2);
    output.rho = dubinspath_.R;
    output.lambda = dubinspath_.lame;

    if ((p - dubinspath_.w3).dot(dubinspath_.q3) < 0) // exit H3
    {
      dub_state_ = dubin_state::BEFORE_H3;      
    }
    break;
  }
}

Eigen::Matrix3f LeaderNavigatePluginSim::rotz(float theta)
{
  Eigen::Matrix3f R;
  R << cosf(theta), -sinf(theta), 0,
      sinf(theta), cosf(theta), 0,
      0, 0, 1;

  return R;
}

float LeaderNavigatePluginSim::mo(float in)
{
  float val;
  if (in > 0)
    val = fmod(in, 2.0 * M_PI_F);
  else
  {
    float n = floorf(in / 2.0 / M_PI_F);
    val = in - n * 2.0 * M_PI_F;
  }
  return val;
}

void LeaderNavigatePluginSim::dubinsParameters(const waypoint_s start_node, const waypoint_s end_node, float R)
{
  float ell = sqrtf((start_node.w[0] - end_node.w[0])*(start_node.w[0] - end_node.w[0]) +
                    (start_node.w[1] - end_node.w[1])*(start_node.w[1] - end_node.w[1]));
  if (ell < 2.0*R)
  {
      ROS_FATAL("SHOULD NOT HAPPEN: The distance between nodes is %f, it should be larger than 2R ( %f ).", ell, 2.0*R);
      exit(0);   
  }
  else
  {
    dubinspath_.ps(0) = start_node.w[0];
    dubinspath_.ps(1) = start_node.w[1];
    dubinspath_.ps(2) = start_node.w[2];
    dubinspath_.chis = start_node.chi_d;
    dubinspath_.pe(0) = end_node.w[0];
    dubinspath_.pe(1) = end_node.w[1];
    dubinspath_.pe(2) = end_node.w[2];
    dubinspath_.chie = end_node.chi_d;


    Eigen::Vector3f crs = dubinspath_.ps;
    crs(0) += R*(cosf(M_PI_2_F)*cosf(dubinspath_.chis) - sinf(M_PI_2_F)*sinf(dubinspath_.chis));
    crs(1) += R*(sinf(M_PI_2_F)*cosf(dubinspath_.chis) + cosf(M_PI_2_F)*sinf(dubinspath_.chis));
    Eigen::Vector3f cls = dubinspath_.ps;
    cls(0) += R*(cosf(-M_PI_2_F)*cosf(dubinspath_.chis) - sinf(-M_PI_2_F)*sinf(dubinspath_.chis));
    cls(1) += R*(sinf(-M_PI_2_F)*cosf(dubinspath_.chis) + cosf(-M_PI_2_F)*sinf(dubinspath_.chis));
    Eigen::Vector3f cre = dubinspath_.pe;
    cre(0) += R*(cosf(M_PI_2_F)*cosf(dubinspath_.chie) - sinf(M_PI_2_F)*sinf(dubinspath_.chie));
    cre(1) += R*(sinf(M_PI_2_F)*cosf(dubinspath_.chie) + cosf(M_PI_2_F)*sinf(dubinspath_.chie));
    Eigen::Vector3f cle = dubinspath_.pe;
    cle(0) += R*(cosf(-M_PI_2_F)*cosf(dubinspath_.chie) - sinf(-M_PI_2_F)*sinf(dubinspath_.chie));
    cle(1) += R*(sinf(-M_PI_2_F)*cosf(dubinspath_.chie) + cosf(-M_PI_2_F)*sinf(dubinspath_.chie));

    float theta, theta2;
    // compute L1
    theta = atan2f(cre(1) - crs(1), cre(0) - crs(0));
    float L1 = (crs - cre).norm() + R*mo(2.0*M_PI_F + mo(theta - M_PI_2_F) - mo(dubinspath_.chis - M_PI_2_F))
               + R*mo(2.0*M_PI_F + mo(dubinspath_.chie - M_PI_2_F) - mo(theta - M_PI_2_F));

    // compute L2
    ell = (cle - crs).norm();
    theta = atan2f(cle(1) - crs(1), cle(0) - crs(0));
    float L2;
    if (2.0*R > ell)
      L2 = 9999.0f;
    else
    {
      theta2 = theta - M_PI_2_F + asinf(2.0*R/ell);
      L2 = sqrtf(ell*ell - 4.0*R*R) + R*mo(2.0*M_PI_F + mo(theta2) - mo(dubinspath_.chis - M_PI_2_F))
           + R*mo(2.0*M_PI_F + mo(theta2 + M_PI_F) - mo(dubinspath_.chie + M_PI_2_F));
    }

    // compute L3
    ell = (cre - cls).norm();
    theta = atan2f(cre(1) - cls(1), cre(0) - cls(0));
    float L3;
    if (2.0*R > ell)
      L3 = 9999.0f;
    else
    {
      theta2 = acosf(2.0*R/ell);
      L3 = sqrtf(ell*ell - 4*R*R) + R*mo(2.0*M_PI_F + mo(dubinspath_.chis + M_PI_2_F) - mo(theta + theta2))
           + R*mo(2.0*M_PI_F + mo(dubinspath_.chie - M_PI_2_F) - mo(theta + theta2 - M_PI_F));
    }

    // compute L4
    theta = atan2f(cle(1) - cls(1), cle(0) - cls(0));
    float L4 = (cls - cle).norm() + R*mo(2.0*M_PI_F + mo(dubinspath_.chis + M_PI_2_F) - mo(theta + M_PI_2_F))
               + R*mo(2.0*M_PI_F + mo(theta + M_PI_2_F) - mo(dubinspath_.chie + M_PI_2_F));

    // L is the minimum distance
    int idx = 1;
    dubinspath_.L = L1;
    if (L2 < dubinspath_.L)
    {
      dubinspath_.L = L2;
      idx = 2;
    }
    if (L3 < dubinspath_.L)
    {
      dubinspath_.L = L3;
      idx = 3;
    }
    if (L4 < dubinspath_.L)
    {
      dubinspath_.L = L4;
      idx = 4;
    }

    Eigen::Vector3f e1;
    //        e1.zero();
    e1(0) = 1;
    e1(1) = 0;
    e1(2) = 0;
    switch (idx)
    {
    case 1:
      dubinspath_.cs = crs;
      dubinspath_.lams = 1;
      dubinspath_.ce = cre;
      dubinspath_.lame = 1;
      dubinspath_.q1 = (cre - crs).normalized();
      dubinspath_.w1 = dubinspath_.cs + (rotz(-M_PI_2_F)*dubinspath_.q1)*R;
      dubinspath_.w2 = dubinspath_.ce + (rotz(-M_PI_2_F)*dubinspath_.q1)*R;
      break;
    case 2:
      dubinspath_.cs = crs;
      dubinspath_.lams = 1;
      dubinspath_.ce = cle;
      dubinspath_.lame = -1;
      ell = (cle - crs).norm();
      theta = atan2f(cle(1) - crs(1), cle(0) - crs(0));
      theta2 = theta - M_PI_2_F + asinf(2.0*R/ell);
      dubinspath_.q1 = rotz(theta2 + M_PI_2_F)*e1;
      dubinspath_.w1 = dubinspath_.cs + (rotz(theta2)*e1)*R;
      dubinspath_.w2 = dubinspath_.ce + (rotz(theta2 + M_PI_F)*e1)*R;
      break;
    case 3:
      dubinspath_.cs = cls;
      dubinspath_.lams = -1;
      dubinspath_.ce = cre;
      dubinspath_.lame = 1;
      ell = (cre - cls).norm();
      theta = atan2f(cre(1) - cls(1), cre(0) - cls(0));
      theta2 = acosf(2.0*R/ ell);
      dubinspath_.q1 = rotz(theta + theta2 - M_PI_2_F)*e1;
      dubinspath_.w1 = dubinspath_.cs + (rotz(theta + theta2)*e1)*R;
      dubinspath_.w2 = dubinspath_.ce + (rotz(theta + theta2 - M_PI_F)*e1)*R;
      break;
    case 4:
      dubinspath_.cs = cls;
      dubinspath_.lams = -1;
      dubinspath_.ce = cle;
      dubinspath_.lame = -1;
      dubinspath_.q1 = (cle - cls).normalized();
      dubinspath_.w1 = dubinspath_.cs + (rotz(M_PI_2_F)*dubinspath_.q1)*R;
      dubinspath_.w2 = dubinspath_.ce + (rotz(M_PI_2_F)*dubinspath_.q1)*R;
      break;
    }
    dubinspath_.w3 = dubinspath_.pe;
    dubinspath_.q3 = rotz(dubinspath_.chie)*e1;
    dubinspath_.R = R;
  }
}

void LeaderNavigatePluginSim::followDubins(const struct params_s &params, const struct plane_state_s &input1, const struct plane_path_s &input2, struct plane_cmd_s &output)
{
  if (input2.flag)// == path_type::Line // follow straight line path specified by r and q
  {
    // compute wrapped version of the path angle
    float chi_q = atan2f(input2.q[1], input2.q[0]);
    while (chi_q - input1.chi < -M_PI)
      chi_q += 2*M_PI;
    while (chi_q - input1.chi > M_PI)
      chi_q -= 2*M_PI;

    float path_error = -sinf(chi_q)*(input1.pn - input2.r[0]) + cosf(chi_q)*(input1.pe - input2.r[1]);
    // heading command
    output.chi_c = chi_q - params.chi_infty*2/M_PI*atanf(params_.k_path*path_error);

    // desired altitude
    float h_d = -input2.r[2] - sqrtf(powf((input2.r[0] - input1.pn), 2) + powf((input2.r[1] - input1.pe),
                                         2))*(input2.q[2])/sqrtf(powf(input2.q[0], 2) + powf(input2.q[1], 2));
    //ROS_INFO("[Leader Navigate] r_path:(%f,%f,%f), q_path:(%f,%f,%f), h_d:%f", input2.r[0], input2.r[1], input2.r[2], input2.q[0], input2.q[1], input2.q[2], h_d);
    // commanded altitude is desired altitude
    output.h_c = h_d;
    output.phi_ff = 0.0;
  }
  else // follow a orbit path specified by c_orbit, rho_orbit, and lam_orbit
  {
    float d = sqrtf(powf((input1.pn - input2.c[0]), 2) + powf((input1.pe - input2.c[1]), 2)); // distance from orbit center
    // compute wrapped version of angular position on orbit
    float varphi = atan2f(input1.pe - input2.c[1], input1.pn - input2.c[0]);
    while ((varphi - input1.chi) < -M_PI)
      varphi += 2*M_PI;
    while ((varphi - input1.chi) > M_PI)
      varphi -= 2*M_PI;
    //compute orbit error
    float norm_orbit_error = (d - input2.rho)/input2.rho;
    output.chi_c = varphi + input2.lambda*(M_PI/2 + atanf(params_.k_orbit*norm_orbit_error));
    //ROS_INFO("[Leader Navigate] d,varphi,norm_orbit_error,chi_c:(%f,%f,%f,%f)", d,varphi,norm_orbit_error,output.chi_c);
    // commanded altitude is the height of the orbit
    float h_d = -input2.c[2];
    output.h_c = h_d;
    //改动：
    output.phi_ff = 0;//(norm_orbit_error < 0.5 ? input.lam_orbit*atanf(input.Va*input.Va/(9.8*input.rho_orbit)) : 0);
  }
  output.Va_c = input2.Va_d;
}

} // namespace general_bus
