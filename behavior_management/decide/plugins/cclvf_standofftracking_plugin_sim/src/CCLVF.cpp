/* ==================================================================
* Copyright (c) 2018, micROS Group, TAIIC, NIIDT & HPCL.
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions
* are met:
*
* 1. Redistributions of source code must retain the above copyright
* notice, this list of conditions and the following disclaimer.
* 2. Redistributions in binary form must reproduce the above copyright
* notice, this list of conditions and the following disclaimer in the
* documentation and/or other materials provided with the
* distribution.
* 3. All advertising materials mentioning features or use of this software
* must display the following acknowledgement:
* This product includes software developed by the micROS Group. and
* its contributors.
* 4. Neither the name of the Group nor the names of its contributors may
* be used to endorse or promote products derived from this software
* without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY MICROS,GROUP AND CONTRIBUTORS
* ''AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT
* NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
* FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
* THE MICROS,GROUP OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
* OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
* HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
* OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
* IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
* ===================================================================
* Author: renweiya, micROS-DA Team, TAIIC.
*/

// #include <time.h>

#include "CCLVF.h"
#include <pluginlib/class_list_macros.h>

namespace general_bus
{

void cclvf_standofftracking_plugin::start()
{
	ros::NodeHandle n_private("~");
	n_private.param<float>("tracking_radius", tracking_radius, 50.0);
	n_private.param<float>("command_velocity", command_velocity, 20.0);
	n_private.param<float>("tracking_height", tracking_height, 50.0);
	//
	tracking_radius = atol(getParam("tracking_radius").c_str());
	command_velocity = atol(getParam("command_velocity").c_str());
	tracking_height = atol(getParam("tracking_height").c_str());
	//
	getActorName(_actorID, _actorName);
	ROS_INFO("[Plugin] in actorID %ld actorName %s,height: %s \n", _actorID, _actorName.c_str(),getParam("tracking_height").c_str());
	ROS_INFO("[Main] number:%d,  tracking_height:%d, tracking_radius:%f, command_velocity:%f \n", _actorID, tracking_height, tracking_radius, command_velocity);
	//
	target_sub_ = pluginSubscribe("/enemy/ground_truth/state", 10, &cclvf_standofftracking_plugin::goalCallback, this);
	state_sub_ = pluginSubscribe("truth", 10, &cclvf_standofftracking_plugin::stateCallback, this);
	controller_pub_ = pluginAdvertise<rosplane_msgs::Controller_Commands>("controller_commands", 10);
	// _pub = pluginAdvertise<actor_msgs::UTOEvent>("uto_event_msg", 10000);

	car_vel_sub_ = pluginSubscribe("/car_velocity", 10, &cclvf_standofftracking_plugin::car_vel_callback, this);
	areacover_done_sub_ = pluginSubscribe("/areacover_done_f", 10, &cclvf_standofftracking_plugin::areacoverDoneCallback, this);
	//
	target_receieved = false;
	UAV_receieved = false;
	car_vel_received = false;
	areacover_done_receieved = false;
	need_to_pub_event=true;
	effective_tracking = 0;
	while (true)
	{
		GOON_OR_RETURN;
		publish_update();
		usleep(10000);
	}
}

void cclvf_standofftracking_plugin::stateCallback(const rosplane_msgs::State::ConstPtr &msg)
{
	UAV_state = *msg;
	UAV_receieved = true;
}

void cclvf_standofftracking_plugin::goalCallback(const nav_msgs::Odometry::ConstPtr &msg)
{
	goal_state = *msg;
	target_receieved = true;
}

void cclvf_standofftracking_plugin::car_vel_callback(const geometry_msgs::Vector3::ConstPtr &msg)
{
	deltaX = msg->x; //north
	deltaY = msg->y; //east
	car_vel_received = true;
}

void cclvf_standofftracking_plugin::areacoverDoneCallback(const std_msgs::Empty::ConstPtr &msg)
{
	areacover_done_receieved = true;
}

void cclvf_standofftracking_plugin::publish_update()
{
	if (target_receieved && UAV_receieved)
	{
		float x_ = UAV_state.position[0] - goal_state.pose.pose.position.x;
		float y_ = UAV_state.position[1] - (-goal_state.pose.pose.position.y);
		float r = sqrt(x_ * x_ + y_ * y_);
		//
		// ROS_INFO("CCLVF distance between the %d-th UAV and target is :  %g",ID_, r);
		if (r < 0.01)
		{
			r = 0.01;
		}
		float c_ = 0;

		if (r < tracking_radius)
		{
			c_ = r / tracking_radius;
		}
		else
		{
			c_ = tracking_radius / r;
		}

		// desired course angle
		float r_rd_ = r * r - tracking_radius * tracking_radius;
		float chi_q = atan2(-(y_ * r_rd_ / r - c_ * tracking_radius * x_), -(x_ * r_rd_ / r + c_ * tracking_radius * y_)); //CCLVF
		//velocity correction based on the estimated car velocity
		if (car_vel_received)
		{
			float UVA_vel_deltaX = command_velocity * cos(chi_q);
			float UVA_vel_deltaY = command_velocity * sin(chi_q);
			float b = 2 * (UVA_vel_deltaX * deltaX + UVA_vel_deltaY * deltaY);
			float a = pow(UVA_vel_deltaX, 2) + pow(UVA_vel_deltaY, 2);
			if (a > pow(10, -7))
			{
				float c = pow(deltaX, 2) + pow(deltaY, 2) - pow(command_velocity, 2);
				float corr_coeff_2 = (sqrt(b * b - 4 * a * c) - b) / (2 * a);
				float UAV_corr_vel_x = corr_coeff_2 * UVA_vel_deltaX + deltaX;
				float UAV_corr_vel_y = corr_coeff_2 * UVA_vel_deltaY + deltaY;
				chi_q = atan2(UAV_corr_vel_y, UAV_corr_vel_x);
				// float now_Va_c = sqrt(pow(UAV_corr_vel_x,2)+pow(UAV_corr_vel_y,2));
			}
		}
		//
		// s.t. -pi < (chi_q - chi) < pi
		if (chi_q - UAV_state.chi > M_PI)
			chi_q -= 2 * M_PI;
		else if (chi_q - UAV_state.chi < -M_PI)
			chi_q += 2 * M_PI;

		// command setting
                //if (!isnan(chi_q))
		control_commands_pub_.chi_c = chi_q;
		control_commands_pub_.Va_c = command_velocity;
		control_commands_pub_.h_c = tracking_height;

		controller_pub_.publish(control_commands_pub_);
		if (r < tracking_radius + tracking_radius * 0.4 && r > tracking_radius - tracking_radius * 0.4)
		{
			effective_tracking++;
		}
		if (need_to_pub_event&&effective_tracking > 6000)
		{
			pubEventMsg("timeout_event");
			need_to_pub_event=false;
		}
		if (need_to_pub_event&&areacover_done_receieved)
		{
			pubEventMsg("patrol_cmd");
			need_to_pub_event=false;
		}
	}
}

PLUGINLIB_EXPORT_CLASS(general_bus::cclvf_standofftracking_plugin, general_bus::GeneralPlugin)
} // namespace general_bus
