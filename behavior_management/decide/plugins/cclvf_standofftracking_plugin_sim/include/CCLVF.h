#ifndef __CCLVF_PLUGIN__
#define __CCLVF_PLUGIN__
#include "actor_msgs/UTOEvent.h"
#include "actor_msgs/UtoActorParams.h"
#include "general_plugin/general_plugin.h"
#include "std_msgs/String.h"
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <ros/ros.h>
#include <rosplane_msgs/Controller_Commands.h>
#include <rosplane_msgs/State.h>
#include <geometry_msgs/Vector3.h>
#include <nav_msgs/Odometry.h>
#include <std_msgs/Empty.h>

namespace general_bus
{

class cclvf_standofftracking_plugin : public GeneralPlugin
{
  public:
	virtual void start();
	void goalCallback(const nav_msgs::Odometry::ConstPtr &msg);
	void stateCallback(const rosplane_msgs::State::ConstPtr &msg);
	void car_vel_callback(const geometry_msgs::Vector3::ConstPtr &msg);
	void areacoverDoneCallback(const std_msgs::Empty::ConstPtr& msg);
	void publish_update();

  private:
	float tracking_radius;
	float command_velocity;
	float tracking_height;
	float deltaX, deltaY;
	bool target_receieved;
	bool UAV_receieved;
	bool car_vel_received;
	bool areacover_done_receieved;
	bool need_to_pub_event;
	int effective_tracking;

	std::string _actorName;
	rosplane_msgs::Controller_Commands control_commands_pub_; // msg to rosplane_controller

	ros::Subscriber state_sub_;   // subscriber for UAV state
	ros::Subscriber target_sub_;  // subscriber for target position
	ros::Subscriber car_vel_sub_; // subscriber for target velocity
	ros::Subscriber areacover_done_sub_;
	ros::Publisher controller_pub_;
	//
	rosplane_msgs::State UAV_state;
	nav_msgs::Odometry goal_state;
};

} // namespace general_bus

#endif //__CCLVF_PLUGIN__
