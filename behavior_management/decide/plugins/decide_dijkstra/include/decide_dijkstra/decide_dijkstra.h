/* ==================================================================
* Copyright (c) 2018, micROS Group, TAIIC, NIIDT & HPCL.
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions
* are met:
*
* 1. Redistributions of source code must retain the above copyright
* notice, this list of conditions and the following disclaimer.
* 2. Redistributions in binary form must reproduce the above copyright
* notice, this list of conditions and the following disclaimer in the
* documentation and/or other materials provided with the
* distribution.
* 3. All advertising materials mentioning features or use of this software
* must display the following acknowledgement:
* This product includes software developed by the micROS Group. and
* its contributors.
* 4. Neither the name of the Group nor the names of its contributors may
* be used to endorse or promote products derived from this software
* without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY MICROS,GROUP AND CONTRIBUTORS
* ''AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT
* NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
* FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
* THE MICROS,GROUP OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
* OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
* HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
* OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
* IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
* ===================================================================
* Author: Ren Xiaoguang, Wu Yunlong, Li Ning, Li Jinghua, micROS-DA Team, NIIDT, TAIIC, NUDT.
*/

#ifndef DECIDE_DIJKSTRA_H
#define DECIDE_DIJKSTRA_H

#include "message_types.h"
#include "da_adapter/da_adapter.h"
#include "decide_softbus/decide_softbus.h"
#include <pluginlib/class_list_macros.h>

#include <stdlib.h>
#include <std_msgs/String.h>
#include <pluginlib/class_list_macros.h>

#include "common/param_dj_precision.h"
#include "common/decide/action/decide_action_dijkstra.h"
#include "common/decide/action/decide_action_dijkstra_out.h"

/**
 * @brief Decide Navigate Namespace.
 */
namespace decide_dijkstra 
{
  /**
   * @brief This is a class to implement the actor plugin for decide_dijkstra.
   */
  class DecideDijkstra : public decide_softbus::DecideSoftbus, public da_adapter::DaAdapter
  {
    public:
      
      #ifdef RUN_ALONE
        DecideDijkstra();
      #endif

      ~DecideDijkstra();

      /**
      * @brief Overload start function of General Plugin
      */
      virtual void start();

      /**
      * @brief Initialize arguments
      * for instnnce, remapping parameters from launch file or desciption file,
      * NodeHandle, Publisher ,etc.
      */
      virtual void initArgs();

    private:

      ros::NodeHandle nh_;
      ros::NodeHandle nh_private_;

      int id_;

      float robot_radius_;
      float grid_size_;

      bool area_changed_ = false;

      bool run_alone_;  ///< This specifies whether the plugin is running alone or not

      std::string map_filepath_;
      std::string start_point_s_;
      std::string target_point_s_;

      decide_action_dj_t _payload;
      decide_action_dj_out_t planning_output_;

      /**
        * @brief Broadcast the result of Planner
        *
        * @param[in] out pointer variable demonstrates _outputGlobalBuffer
        */
      void sendPlan(unified_msg::unified_message_t *out);

      /**
       * @brief Covert ordered vertices of the region from std::string to
       * std::vector
       *
       * @param[in] vertices specifies ordered vertices of region int std::string
       * type
       * @param[out] region obtains ordered vertices of region in std::vector
       */
      void getAreaVertices(const std::string &vertices, std::vector<float> &region);

      /**
       * @brief Update buffers, for instance input buffers, param buffers, etc.
       * 
       */
      void updateBuffers();
  };

} /*end namespace*/

#endif