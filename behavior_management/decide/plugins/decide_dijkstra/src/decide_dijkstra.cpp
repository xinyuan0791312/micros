/* ==================================================================
* Copyright (c) 2018, micROS Group, TAIIC, NIIDT & HPCL.
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions
* are met:
*
* 1. Redistributions of source code must retain the above copyright
* notice, this list of conditions and the following disclaimer.
* 2. Redistributions in binary form must reproduce the above copyright
* notice, this list of conditions and the following disclaimer in the
* documentation and/or other materials provided with the
* distribution.
* 3. All advertising materials mentioning features or use of this software
* must display the following acknowledgement:
* This product includes software developed by the micROS Group. and
* its contributors.
* 4. Neither the name of the Group nor the names of its contributors may
* be used to endorse or promote products derived from this software
* without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY MICROS,GROUP AND CONTRIBUTORS
* ''AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT
* NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
* FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
* THE MICROS,GROUP OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
* OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
* HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
* OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
* IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
* ===================================================================
* Author: Ren Xiaoguang, Wu Yunlong, Li Ning, Li Jinghua, micROS-DA Team, NIIDT, TAIIC, NUDT.
*/

#include "decide_dijkstra/decide_dijkstra.h"

namespace decide_dijkstra 
{
  #ifdef RUN_ALONE
    DecideDijkstra::DecideDijkstra() 
    { 
      start(); 
    }
  #endif

  DecideDijkstra::~DecideDijkstra() {}

  void DecideDijkstra::start() 
  {
    initArgs();

    BT::FallbackNode *fallbackRoot = new BT::FallbackNode("fallbackRoot");

    BT::DecideActionNode<decide_action_interface::DecideActionInterface> 
      *actionDJ = new BT::DecideActionNode<decide_action_interface::DecideActionInterface>("actionDJ");

    actionDJ->setParam(_paramBuffer[0]);
    actionDJ->setMethod("decide_softbus",
                        "decide_action_interface::DecideActionInterface",
                        "qrotor_decide_action_dijkstra_impl::QrotorDecideActionDijkstraImpl");
    actionDJ->setBuffer(_inputGlobalBuffer, _outputGlobalBuffer);

    _root->AddChild(fallbackRoot);
    fallbackRoot->AddChild(actionDJ);

    ros::Rate loop_rate(1);
    int count = 1;
      
    while (ros::ok()) 
    {
      #ifdef RUN_ALONE
        ros::spinOnce();
      #else
        GOON_OR_RETURN
      #endif

      ROS_INFO("This is the %d time", count);

      if (area_changed_) 
      {
        updateBuffers(); 
        
        BT::ReturnStatus state = _root->Tick();
        
        area_changed_ = false;
      }
      
      sendPlan(_outputGlobalBuffer);

      ROS_INFO("status:%d\n", _root->get_status());
      if (_root->get_status() != BT::RUNNING) {
        _root->ResetColorState();
      }

      count++;
      loop_rate.sleep();
    }
    delete fallbackRoot;
    delete actionDJ;
  }

  void DecideDijkstra::initArgs() 
  {
    nh_ = ros::NodeHandle();
    nh_private_ = ros::NodeHandle("~");
    
    #ifdef RUN_ALONE
      run_alone_ = true;
    #else
      run_alone_ = false;  
    #endif
    
    nh_private_.param<int>("robot_id_", id_, 1);

    if(run_alone_)
    {
      nh_private_.param<float>("robot_radius_", robot_radius_, 1.0);
      nh_private_.param<float>("grid_size_", grid_size_, 1.0);
      nh_private_.param<std::string>("start_point_", start_point_s_, "2 2");
      nh_private_.param<std::string>("target_point_", target_point_s_, "50 50");
      nh_private_.param<std::string>("map_filepath_", map_filepath_,
                                     "/home/wyl/Desktop/micROS_test/20191203/src/micros/behavior_management/decide/plugins/decide_dijkstra/map1.txt");
    }
    else
    {
      robot_radius_ = atof( getParam("robot_radius_").c_str() );
      grid_size_ = atof( getParam("grid_size_").c_str() );
      start_point_s_ = getParam("start_point_").c_str();
      target_point_s_ = getParam("target_point_").c_str();
      map_filepath_ = getParam("map_filepath_").c_str();
    }

    std::vector<float> spoint;
    std::vector<float> tpoint;

    getAreaVertices(start_point_s_, spoint);
    getAreaVertices(target_point_s_, tpoint);
    _payload.mf_len = map_filepath_.length();
    _payload.start_point_x = spoint[0];
    _payload.start_point_y = spoint[1];
    _payload.target_point_x = tpoint[0];
    _payload.target_point_y = tpoint[1];
    _payload.map_filepath = map_filepath_;
    _payload.struct_size = sizeof(_payload);

    area_changed_ = true;

    addParamBuffer();
    param_precision_dj_t tmp = {grid_size_, robot_radius_};
    memcpy(&(_paramBuffer[0]->payload[0]), &tmp, sizeof(tmp));

    _inputGlobalBuffer->setValue("type", 8);
    _inputGlobalBuffer->setValue("error_code", 0);

    _outputGlobalBuffer->setValue("type", 7);
    _outputGlobalBuffer->setValue("error_code", 0);
    
    printf("Initial Finished.\n");
  }

  void DecideDijkstra::sendPlan(unified_msg::unified_message_t *out) 
  {
    int rsize = 0;
    if (out->error_code[0] == 0x00 && out->error_code[1] == 0x00 && out->error_code[2] == 0x00) 
    {
      memcpy(&rsize, &out->payload[0], sizeof(rsize));
      if (rsize != 0) 
      {
        printf("out Size:%d\n", rsize);

        printf("planning_output_ vector size:%d\n",
              (int)planning_output_.result.size());

        for (size_t i = 1; i < planning_output_.result.size(); i += 2) {
          printf("(%d,%d)<---", int(planning_output_.result[i - 1]),
                int(planning_output_.result[i]));
        }
        printf("Plan\n");
      }
      else 
      {
        printf("rsize is 0\n");
      }
    }
    else 
    {
      ROS_INFO("[DecideDijkstra] _outputGlobalBuffer type is WRONG !\n");
    }
  }

  void DecideDijkstra::updateBuffers() 
  {
    _inputGlobalBuffer->setPayloadPtr(_payload);
    _outputGlobalBuffer->setPayloadPtr(planning_output_);
  }

  void DecideDijkstra::getAreaVertices(const std::string &vertices,
                                             std::vector<float> &origin_region) 
  {
    std::vector<std::string> aRet;
    std::string::size_type pos1, pos2;
    pos2 = vertices.find(" ");
    pos1 = 0;
    int cnt = 0;
    while (std::string::npos != pos2) 
    {
      aRet.push_back(vertices.substr(pos1, pos2 - pos1));
      pos1 = pos2 + 1;
      pos2 = vertices.find(" ", pos1);
      cnt++;
    }
    if (pos1 != vertices.length()) 
    {
      aRet.push_back(vertices.substr(pos1));
      cnt++;
    }
    origin_region.resize(cnt);
    for (int i = 0; i < cnt; i++) 
    {
      origin_region[i] = atof(aRet.at(i).c_str());
    }
  } // * end get vertices of search region
}

#ifdef RUN_ALONE
  int main(int argc, char *argv[]) {
    ros::init(argc, argv, "decide_dijkstra");

    ros::NodeHandle n;
    decide_dijkstra::DecideDijkstra da_plugin;
    ROS_INFO("Decide Dijkstra BT");

    return 0;
  }
#else
  PLUGINLIB_EXPORT_CLASS(decide_dijkstra::DecideDijkstra, general_bus::GeneralPlugin)
#endif