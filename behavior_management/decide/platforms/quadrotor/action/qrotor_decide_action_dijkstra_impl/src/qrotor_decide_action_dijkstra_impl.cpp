/* ==================================================================
* Copyright (c) 2018, micROS Group, TAIIC, NIIDT & HPCL.
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions
* are met:
*
* 1. Redistributions of source code must retain the above copyright
* notice, this list of conditions and the following disclaimer.
* 2. Redistributions in binary form must reproduce the above copyright
* notice, this list of conditions and the following disclaimer in the
* documentation and/or other materials provided with the
* distribution.
* 3. All advertising materials mentioning features or use of this software
* must display the following acknowledgement:
* This product includes software developed by the micROS Group. and
* its contributors.
* 4. Neither the name of the Group nor the names of its contributors may
* be used to endorse or promote products derived from this software
* without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY MICROS,GROUP AND CONTRIBUTORS
* ''AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT
* NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
* FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
* THE MICROS,GROUP OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
* OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
* HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
* OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
* IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
* ===================================================================
* Author: Ren Xiaoguang, Wu Yunlong, Li Ning, Li Jinghua, micROS-DA Team, NIIDT, TAIIC, NUDT.
*/

#include "qrotor_decide_action_dijkstra_impl/qrotor_decide_action_dijkstra_impl.h"

namespace qrotor_decide_action_dijkstra_impl 
{
  QrotorDecideActionDijkstraImpl::QrotorDecideActionDijkstraImpl() 
  {
    std::cout << "********** [decide_action_Dijkstra_impl] "
                "QrotorDecideActionDijkstraImpl Loaded! **********"
              << std::endl;
  }
  QrotorDecideActionDijkstraImpl::~QrotorDecideActionDijkstraImpl() 
  {

  }

  void QrotorDecideActionDijkstraImpl::getPlan(unified_msg::unified_message_t *in, 
                                              unified_msg::unified_message_t *out) 
  {
    if (in->type[0] == 0x08 && in->type[1] == 0x00 && in->type[2] == 0x00) 
    {
      decide_action_dj_t *data;
      int size;
      memcpy(&size, &in->payload[0], sizeof(size));
      memcpy(&data, &in->payload[sizeof(size)], size);

      decide_action_dj_out_t *tmp;
      memcpy(&tmp, &out->payload[sizeof(size)], size);
      
      int file_length = data->mf_len;
      sx = data->start_point_x;
      sy = data->start_point_y;

      gx = data->target_point_x;
      gy = data->target_point_y;
      printf("sx:%.2f,sy:%.2f,gx:%.2f,gy:%.2f\n", sx, sy, gx, gy);

      std::string map_filepath;
      map_filepath.resize(file_length);

      map_filepath = data->map_filepath;
      std::cout << "map file path:" << map_filepath << std::endl;

      std::cout << "********** [decide_action_dijkstra_impl] read and load map **********" << std::endl;
      read_data(map_filepath);
      std::vector<float> rx, ry;
      planning(rx, ry);

      int size_va = rx.size();
      tmp->result.resize(rx.size() * 2);
      int j = 0;
      for (size_t i = 0; i < size_va; i++) {
        tmp->result[j++] = rx[i];
        tmp->result[j++] = ry[i];
      }
      for (size_t i = 1; i < size_va * 2; i += 2) {
        printf("(%d,%d)<---", int(tmp->result[i - 1]), int(tmp->result[i]));
      }
      printf("Go\n");

    } else {
      out->error_code[0] = 0x01;
    }
    std::cout << "********** [decide_action_dijkstra_impl] getPlan is "
                "executed! **********"
              << std::endl;
  }

  void QrotorDecideActionDijkstraImpl::initializeParam(
      unified_msg::unified_message_t *params) {
    param_precision_dj_t temp;
    memcpy(&temp, &params->payload[0], sizeof(temp));
    reso = temp.grid_size;
    rr = temp.robot_radius;
  }

  void QrotorDecideActionDijkstraImpl::planning(std::vector<float> &rx,
                                                std::vector<float> &ry) {
    Node nstart(calc_xyindex(sx, minx), calc_xyindex(sy, miny), 0.0, -1);
    Node ngoal(calc_xyindex(gx, minx), calc_xyindex(gy, miny), 0.0, -1);

    std::vector<int> openset_id;
    std::vector<Node> openset_node;

    std::vector<int> closeset_id;
    std::vector<Node> closeset_node;

    openset_id.push_back(calc_index(nstart));
    openset_node.push_back(nstart);

    while (1) {
      int c_id = getMinCostInMap(openset_id, openset_node);
      if (c_id == EMPTY) {
        std::cout << "Empty in openset" << std::endl;
        return;
      }

      int c_index = getIdInVector(c_id, openset_id);
      if (c_index == -1) {
        std::cout << "Error openset_id" << std::endl;
        return;
      }
      Node current = openset_node[c_index];

      if (current.x == ngoal.x && current.y == ngoal.y) {
        std::cout << "Find goal!" << std::endl;
        ngoal.pind = current.pind;
        ngoal.cost = current.cost;
        break;
      }

      openset_id.erase(openset_id.begin() + c_index);
      openset_node.erase(openset_node.begin() + c_index);

      closeset_id.push_back(c_id);
      closeset_node.push_back(current);

      for (size_t i = 0; i < sizeof(motion) / sizeof(*motion); i++) {
        Node node(current.x + motion[i][0], current.y + motion[i][1],
                  current.cost + motion[i][2], c_id);
        int n_id = calc_index(node);

        if (isValueInVector(n_id, closeset_id))
          continue;

        if (!verify_node(node))
          continue;

        if (!isValueInVector(n_id, openset_id)) {
          openset_id.push_back(n_id);
          openset_node.push_back(node);
        } else {
          int n_index = getIdInVector(n_id, openset_id);
          if (openset_node[n_index].cost >= node.cost) {
            openset_node[n_index] = node;
          }
        }
      }
    }

    std::cout << "********** [decide_action_dijkstra_impl] planning is "
                "done **********"
              << std::endl;
    calc_finalpath(ngoal, closeset_id, closeset_node, rx, ry);
    std::cout << "********** [decide_action_dijkstra_impl] show result "
                "**********"
              << std::endl;
    print_map(rx, ry);
  }

  void QrotorDecideActionDijkstraImpl::calc_finalpath(
      Node &ngoal, std::vector<int> &closeset_id,
      std::vector<Node> &closeset_node, std::vector<float> &rx,
      std::vector<float> &ry) {
    rx.push_back(float(calc_position(ngoal.x, minx)));
    ry.push_back(float(calc_position(ngoal.y, miny)));

    int pind = ngoal.pind;
    float cost = 0;
    while (pind != -1) {
      int index = getIdInVector(pind, closeset_id);
      Node n = closeset_node[index];
      rx.push_back(float(calc_position(n.x, minx)));
      ry.push_back(float(calc_position(n.y, miny)));
      cost += n.cost;
      pind = n.pind;
    }
    std::cout << "Cost:" << (double)cost << std::endl;
  }

  void QrotorDecideActionDijkstraImpl::read_data(const std::string txt_path) 
  {
    std::ifstream read(txt_path);
    if (read.fail()) {
      std::cout << "Open file fail" << std::endl;
      read.close();
      return;
    }
    std::string line;
    int rowcount = 0;
    int colcount = 0;
    int i = 0, j = 0;
    while (getline(read, line)) {
      char *cstr = new char[line.length() + 1];
      std::strcpy(cstr, line.c_str());
      bool flag = true;
      char *p = std::strtok(cstr, " ");
      j = 0;
      while (p != NULL) {
        colcount++;
        map[i][j] = atoi(p);
        p = std::strtok(NULL, " ");
        j++;
      }
      i++;
      rowcount++;
      delete[] p;
      delete[] cstr;
    }
    read.close();

    std::cout << "col count:" << colcount / rowcount << std::endl;
    std::cout << "row count:" << rowcount << std::endl;
    ywidth = rowcount;
    xwidth = colcount / rowcount;
    minx = 0;
    miny = 0;
    maxx = xwidth;
    maxy = ywidth;

    for (int i = 0; i < ywidth; i++) {
      for (int j = 0; j < xwidth; j++) {
        std::cout << map[i][j] << " ";
      }
      std::cout << std::endl;
    }
  }

  void QrotorDecideActionDijkstraImpl::print_map(std::vector<float> &rx,
                                                std::vector<float> &ry) 
  {
    bool isWrited = false;
    for (int i = 0; i < ywidth; i++) {
      for (int j = 0; j < xwidth; j++) {
        for (int s = 0; s < rx.size(); s++) {

          if (int(rx[s]) == j && int(ry[s]) == i && !isWrited) 
          {
            std::cout << "*"
                      << " ";
            isWrited = true;
          }
        }
        if (!isWrited) {
          std::cout << map[i][j] << " ";
        }
        isWrited = false;
      }
      std::cout << std::endl;
    }
  }

  bool QrotorDecideActionDijkstraImpl::verify_node(Node &n)
  {
    int px = calc_position(n.x, minx);
    int py = calc_position(n.y, miny);
    if (px < minx)
      return false;
    else if (py < miny)
      return false;
    else if (px >= maxx)
      return false;
    else if (py >= maxy)
      return false;

    if (map[(int)n.y][(int)n.x] == 1)
      return false;

    return true;
  }

  int QrotorDecideActionDijkstraImpl::calc_index(Node &n) 
  {
    return (n.y - miny) * xwidth + (n.x - minx);
  }

  int QrotorDecideActionDijkstraImpl::calc_xyindex(float position, int minp) 
  {
    return round_number((position - minp) / reso);
  }

  int QrotorDecideActionDijkstraImpl::calc_position(int index, int minp) 
  {
    return index * reso + minp;
  }

  int QrotorDecideActionDijkstraImpl::getIdInVector(int value,
                                                      std::vector<int> &v_id) 
  {
    for (int i = 0; i < v_id.size(); i++) {
      if (value == v_id[i])
        return i;
    }
    return -1;
  }

  bool QrotorDecideActionDijkstraImpl::isValueInVector(int value,
                                                          std::vector<int> &v) 
  {
    for (int i = 0; i < v.size(); i++) {
      if (value == v[i])
        return true;
    }
    return false;
  }

  int QrotorDecideActionDijkstraImpl::getMinCostInMap(
      std::vector<int> &v_id, std::vector<Node> &v_node) 
  {
    if (v_id.empty() || v_node.empty()) {
      return EMPTY;
    }
    int size = v_id.size();
    int id = 0;
    int isFisrt = true;
    float mincost;
    for (int i = 0; i < size; i++) {
      if (isFisrt) {
        mincost = v_node[i].cost;
        id = v_id[i];
        isFisrt = false;
      }
      if (mincost > v_node[i].cost) {
        mincost = v_node[i].cost;
        id = v_id[i];
      }
    }
    return id;
  }

  float QrotorDecideActionDijkstraImpl::getMinFromVector(std::vector<float> &v) 
  {
    float min_v = v[0];
    for (auto i = 0; i < v.size(); i++) {
      if (min_v > v[i]) {
        min_v = v[i];
      }
    }
    return min_v;
  }

  float QrotorDecideActionDijkstraImpl::getMaxFromVector(std::vector<float> &v) 
  {
    float max_v = v[0];
    for (auto i = 0; i < v.size(); i++) {
      if (max_v < v[i]) {
        max_v = v[i];
      }
    }
    return max_v;
  }

  int QrotorDecideActionDijkstraImpl::round_number(double num) 
  {
    return (num > 0.0) ? floor(num + 0.5) : ceil(num - 0.5);
  }
}

PLUGINLIB_EXPORT_CLASS(qrotor_decide_action_dijkstra_impl::QrotorDecideActionDijkstraImpl,
                       decide_action_interface::DecideActionInterface)