/* ==================================================================
* Copyright (c) 2018, micROS Group, TAIIC, NIIDT & HPCL.
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions
* are met:
*
* 1. Redistributions of source code must retain the above copyright
* notice, this list of conditions and the following disclaimer.
* 2. Redistributions in binary form must reproduce the above copyright
* notice, this list of conditions and the following disclaimer in the
* documentation and/or other materials provided with the
* distribution.
* 3. All advertising materials mentioning features or use of this software
* must display the following acknowledgement:
* This product includes software developed by the micROS Group. and
* its contributors.
* 4. Neither the name of the Group nor the names of its contributors may
* be used to endorse or promote products derived from this software
* without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY MICROS,GROUP AND CONTRIBUTORS
* ''AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT
* NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
* FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
* THE MICROS,GROUP OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
* OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
* HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
* OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
* IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
* ===================================================================
* Author: Ren Xiaoguang, Wu Yunlong, Li Ning, Li Jinghua, micROS-DA Team, NIIDT, TAIIC, NUDT.
*/

#ifndef QROTOR_DECIDE_ACTION_DIJKSTRA_IMPL_H
#define QROTOR_DECIDE_ACTION_DIJKSTRA_IMPL_H

#include <cstring>
#include <fstream>
#include <math.h>
#include <pluginlib/class_list_macros.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <vector>

#include "common/decide/action/decide_action_dijkstra.h"
#include "common/decide/action/decide_action_dijkstra_out.h"
#include "common/param_dj_precision.h"
#include "decide_softbus/decide_action_interface.h"
#include "message_types.h"

#define MAXMAP 1000
#define EMPTY 0xff99

/**
 * @brief Qrotor Decide Action Dijkstra Impl Namespace.
 */
namespace qrotor_decide_action_dijkstra_impl 
{
  /**
   * @brief This is a class to implement the algorithm plugin fwing_decide_action_dubins_impl.
   */
  class Node 
  {
    public:
      float x;
      float y;
      float cost;
      int pind;
      
      Node(float xx, float yy, float c, float p) 
      {
        x = xx;
        y = yy;
        cost = c;
        pind = p;
      }

      void node_print() 
      { 
        printf("Node(%f,%f,%f,%d)\n", x, y, cost, pind); 
      }
  };

  /**
   * @brief This is a class to implement the algorithm plugin fwing_decide_action_dubins_impl.
   */
  class QrotorDecideActionDijkstraImpl
      : public decide_action_interface::DecideActionInterface 
  {
    public:
      QrotorDecideActionDijkstraImpl();
      ~QrotorDecideActionDijkstraImpl();

      /**
        * @brief Calculate fixed-wing platform's command
        *
        * @param[in] fwing_state specifies own state
        * @param[out] fwing_cmd obtains the calculated fix-wing platform's command
        * @param[in] va specifies the VA position
        * @param[in] input_params specifies params needed
        */
      virtual void getPlan(unified_msg::unified_message_t *in,
                          unified_msg::unified_message_t *out);

      /** @brief initializeParam
        *
        * @param[in] demonstrates parameters to be initialized
        */
      virtual void initializeParam(unified_msg::unified_message_t *params);

    private:
      float sx, sy, gx, gy;
      float reso;
      float rr;
      int minx, miny, maxx, maxy;
      int xwidth, ywidth;
      int map[MAXMAP][MAXMAP];
      float motion[8][3] = {{1, 0, 1},        {0, 1, 1},         {-1, 0, 1},
                            {0, -1, 1},       {-1, -1, sqrt(2)}, {-1, 1, sqrt(2)},
                            {1, -1, sqrt(2)}, {1, 1, sqrt(2)}};
      std::vector<float> ox, oy;
      void print_map(std::vector<float> &rx, std::vector<float> &ry);
      void read_data(const std::string txt_path);
      void calc_finalpath(Node &ngoal, std::vector<int> &closeset_id,
                          std::vector<Node> &closeset_node, std::vector<float> &rx,
                          std::vector<float> &ry);
      bool verify_node(Node &n);
      int calc_index(Node &n);
      int calc_xyindex(float position, int minp);
      int calc_position(int index, int minp);
      int getIdInVector(int value, std::vector<int> &v_id);
      bool isValueInVector(int value, std::vector<int> &v);
      int getMinCostInMap(std::vector<int> &v_id, std::vector<Node> &v_node);
      float getMinFromVector(std::vector<float> &v);
      float getMaxFromVector(std::vector<float> &v);
      int round_number(double num);
      void planning(std::vector<float> &rx, std::vector<float> &ry);
  };
}

#endif