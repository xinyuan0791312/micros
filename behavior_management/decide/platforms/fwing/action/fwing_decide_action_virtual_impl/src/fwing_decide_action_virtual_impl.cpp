/* ==================================================================
* Copyright (c) 2018, micROS Group, TAIIC, NIIDT & HPCL.
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions
* are met:
*
* 1. Redistributions of source code must retain the above copyright
* notice, this list of conditions and the following disclaimer.
* 2. Redistributions in binary form must reproduce the above copyright
* notice, this list of conditions and the following disclaimer in the
* documentation and/or other materials provided with the
* distribution.
* 3. All advertising materials mentioning features or use of this software
* must display the following acknowledgement:
* This product includes software developed by the micROS Group. and
* its contributors.
* 4. Neither the name of the Group nor the names of its contributors may
* be used to endorse or promote products derived from this software
* without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY MICROS,GROUP AND CONTRIBUTORS
* ''AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT
* NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
* FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
* THE MICROS,GROUP OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
* OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
* HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
* OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
* IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
* ===================================================================
* Author: Ren Xiaoguang, Wu Yunlong, Li Jinghua, micROS-DA Team, NIIDT, TAIIC.
*/

#include "fwing_decide_action_virtual_impl/fwing_decide_action_virtual_impl.h"

namespace fwing_decide_action_virtual_impl
{	
  FwingDecideActionVirtualImpl::FwingDecideActionVirtualImpl()
  {
    std::cout << "********** [decide_action_VA_impl] FwingDecideActionVirtualImpl Loaded! **********" << std::endl;
  }
  
  void FwingDecideActionVirtualImpl::initializeParam(unified_msg::unified_message_t *pParams)
  {
    memcpy(&_paramPrecision, &pParams->payload[0], sizeof(_paramPrecision));
  }

  void FwingDecideActionVirtualImpl::getPlan(unified_msg::unified_message_t *pInputBuffer, 
                                             unified_msg::unified_message_t *pOutputBuffer)
  {
    if(pInputBuffer->type[0] == 0x08 && pInputBuffer->type[1] == 0x00 && pInputBuffer->type[2] == 0x00)
    {
      decide_action_virtual_agent_t *payload;
      int size;
      memcpy(&size, &pInputBuffer->payload[0], sizeof(size));
      memcpy(&payload, &pInputBuffer->payload[sizeof(size)], size);

      _originRegionV.resize(payload->region_size/2);
      for(size_t i = 0; i < payload->region_size/2; i++)
      {
        _originRegionV[i].x = payload->origin_region[2*i];
        _originRegionV[i].y = payload->origin_region[2*i+1];
      }

      if(payload->region_size/2 < 3)
      {
        std::cout << "[decide_action_VA_impl] Not a region !" << std::endl;
        pOutputBuffer->error_code[0] = 0x02;

      }else
      {
        deployVirtualAgents(_originRegionV, payload->rd, payload->total_number);
      }
      
      decide_action_va_out_t tmp;
      int va_v_size = _deployPointsV.size()*2*4;
      
      for(size_t i = 0; i < va_v_size/8; i++)
      {
        tmp.virtual_agents.push_back(_deployPointsV[i].x);
        tmp.virtual_agents.push_back(_deployPointsV[i].y);
      }
      
      pOutputBuffer->setPayload(tmp.virtual_agents[0], va_v_size);
      
    }else
    {
      pOutputBuffer->error_code[0] = 0x01;
    }

    std::cout << "********** [decide_action_VA_impl] getPlan is executed! **********" << std::endl;
  }
  
  FwingDecideActionVirtualImpl::~FwingDecideActionVirtualImpl(){}

  void FwingDecideActionVirtualImpl::deployVirtualAgents(const std::vector<fwing_va> &anOriginRegion, 
                                                         const float &aDesiredDistance, 
                                                         const int &aQuantity)
  {
    float tmpDistance = aDesiredDistance;
    int goalsCounts = -1;
    while(goalsCounts < aQuantity)
    {
      if( goalsCounts != -1 )
      {
        tmpDistance -= 1;
        if(tmpDistance <= 0)
        {
          std::cout << "[decide_action_VA_impl] NO enough points to deploy!!!" << std::endl;
          break;
        }   
      } 
      std::vector<fwing_va>().swap(_deployPointsV);
      partition(tmpDistance, anOriginRegion, _multiRowRegionV, _singleRowRegionVV);
      deployMultiRowInner(tmpDistance, _multiRowRegionV, _deployPointsV);
      goalsCounts = _deployPointsV.size();  
    }
  }

  void FwingDecideActionVirtualImpl::partition(const double &aDesiredDistance, 
                                               const std::vector<fwing_va> &anOriginRegion, 
                                                     std::vector<fwing_va> &aMulitRowRegion, 
                                                     std::vector<std::vector<fwing_va>> &aSingleRowRegionSet)
  {
    std::vector<fwing_va>().swap(aMulitRowRegion);
    std::vector<std::vector<fwing_va>>().swap(aSingleRowRegionSet);
    aMulitRowRegion = anOriginRegion;
    double d = sqrt(3)*aDesiredDistance;
    int i = 0,j;
      
    while(i < aMulitRowRegion.size())
    {
      j = i + 2;
      while(j < aMulitRowRegion.size())
      {
        double tmpDistance = getDistance(aMulitRowRegion[i].x, aMulitRowRegion[i].y, aMulitRowRegion[j].x, aMulitRowRegion[j].y);
        if(tmpDistance > d)  
        {
          j++;
          continue;
        }else if(j == i + 2)    
        {
          std::vector<fwing_va> tmp = { aMulitRowRegion[i], aMulitRowRegion[i+1], aMulitRowRegion[j] };
          aSingleRowRegionSet.push_back(tmp);
          aMulitRowRegion.erase(aMulitRowRegion.begin()+i+1);
        }else// divide the region into 2 parts to distinguish whether there is single row region
        {
          std::vector<fwing_va> tmpRegionOne, tmpRegionTwo = aMulitRowRegion;
          for(int m = i; m <= j; m++)
          {
            tmpRegionOne.push_back(aMulitRowRegion[m]); 
          }                    
          tmpRegionTwo.insert(tmpRegionTwo.begin(), tmpRegionTwo.begin() + j, tmpRegionTwo.end());
          tmpRegionTwo.erase(tmpRegionTwo.begin()+aMulitRowRegion.size()-j+i+1, tmpRegionTwo.end());
          bool flagOne = isLNArea(d, tmpRegionOne);
          bool flagTwo = isLNArea(d, tmpRegionTwo);
          if(flagOne && flagTwo)
          {
            aSingleRowRegionSet.push_back(tmpRegionOne); 
            aSingleRowRegionSet.push_back(tmpRegionTwo); 
            std::vector<fwing_va>().swap(aMulitRowRegion);
          }else if(flagOne && !flagTwo)
          {
            aSingleRowRegionSet.push_back(tmpRegionOne);  
            aMulitRowRegion = tmpRegionTwo;
          }else if(!flagOne && flagTwo)
          {
            aSingleRowRegionSet.push_back(tmpRegionTwo);   
            aMulitRowRegion = tmpRegionOne;
          } 
        }// **** end divide the region into 2 parts to distinguish whether there is single row region
        j++;
      }// *** end while_j
      i++;
    }// ** end while_i

    i = 0, j = 0;
    while(i < aMulitRowRegion.size())
    {
      int k = aMulitRowRegion.size(),h;
      if(i == k - 1)    j = 0;
      else    j = i + 1;

      if(i == 0)    h = k - 1;
      else h = i - 1;

      double theta_i_j = atan2(aMulitRowRegion[j].y - aMulitRowRegion[i].y, aMulitRowRegion[j].x - aMulitRowRegion[i].x);
      double theta_h_i = atan2(aMulitRowRegion[h].y - aMulitRowRegion[i].y, aMulitRowRegion[h].x - aMulitRowRegion[i].x);
      double inner_angle = theta_h_i - theta_i_j;
      bool flag = false;
      for(int l = 0; l < aSingleRowRegionSet.size(); l++)
      {
        bool f_1 = (std::find_if(aSingleRowRegionSet[l].begin(), aSingleRowRegionSet[l].end(), IsMatch(aMulitRowRegion[i])) != aSingleRowRegionSet[l].end()); 
        bool f_2 = (std::find_if(aSingleRowRegionSet[l].begin(), aSingleRowRegionSet[l].end(), IsMatch(aMulitRowRegion[h])) != aSingleRowRegionSet[l].end());   
        bool f_3 = (std::find_if(aSingleRowRegionSet[l].begin(), aSingleRowRegionSet[l].end(), IsMatch(aMulitRowRegion[j])) != aSingleRowRegionSet[l].end()); 
        if(f_1 && (f_2 || f_3))
        {
          flag = true;
          break;
        }
      }
      if(fabs(inner_angle) > M_PI / 2 || flag)
      {
        i++;
        continue;
      }
      std::vector<fwing_va> lines;
      getParallels(sqrt(3)*aDesiredDistance, aMulitRowRegion[h], aMulitRowRegion[i], lines);
      fwing_va cross;
      bool flagOne = false, flagTwo = false;
      getCross(aMulitRowRegion[i], aMulitRowRegion[j], lines[0], lines[1], cross);
      std::vector<fwing_va> segment = {aMulitRowRegion[i], aMulitRowRegion[j]};
      flagOne = isAtBoundaries(cross,segment);
      if(!flagOne)
      {
        getCross(aMulitRowRegion[i], aMulitRowRegion[j], lines[2], lines[3], cross);
        flagTwo = isAtBoundaries(cross,segment);   
      }
      fwing_va start;
      aSingleRowRegionSet.resize(aSingleRowRegionSet.size()+1);
      // make vertical line through point cross
      if(flagOne || flagTwo)
      {
        // double d = sqrt(3)*aDesiredDistance;
        if(theta_h_i == 0 || theta_h_i == M_PI)
        {
          start.x = cross.x;
          start.y = aMulitRowRegion[i].y; 
        }else if(theta_h_i == M_PI/2 || theta_h_i == - M_PI/2)
        {
          start.x = aMulitRowRegion[i].x;
          start.y = cross.y; 
        }else
        {
          start.x = aMulitRowRegion[i].x + d*cos(theta_h_i)/tan(fabs(inner_angle));
          start.y = aMulitRowRegion[i].y + d*sin(theta_h_i)/tan(fabs(inner_angle));
        }
        // the vertical line of segment hi through point cross is not at segment hi, then make vertical line of segment ij, the cross point must be at segment hi
        if(!isAtBoundaries(start, { aMulitRowRegion[h], aMulitRowRegion[i] })) 
        {
          start = aMulitRowRegion[h];
          double siDistance = getDistance(start.x, start.y, aMulitRowRegion[i].x, aMulitRowRegion[i].y);
          fwing_va end = {aMulitRowRegion[i].x + siDistance*cos(fabs(inner_angle))*cos(theta_i_j), 
                          aMulitRowRegion[i].y + siDistance*cos(fabs(inner_angle))*sin(theta_i_j)};

          aSingleRowRegionSet[aSingleRowRegionSet.size()-1].push_back(start);
          aSingleRowRegionSet[aSingleRowRegionSet.size()-1].push_back(aMulitRowRegion[i]);
          aSingleRowRegionSet[aSingleRowRegionSet.size()-1].push_back(end);
          std::vector<fwing_va>::iterator it = aMulitRowRegion.begin() + i;
          it = aMulitRowRegion.erase(it);
          aMulitRowRegion.insert(it, end);
        }else// the vertical line of segment hi through point cross is at segment hi
        {
          aSingleRowRegionSet[aSingleRowRegionSet.size()-1].push_back(start);
          aSingleRowRegionSet[aSingleRowRegionSet.size()-1].push_back(aMulitRowRegion[i]);
          aSingleRowRegionSet[aSingleRowRegionSet.size()-1].push_back(cross);

          std::vector<fwing_va>::iterator it = aMulitRowRegion.begin() + i;
          it = aMulitRowRegion.erase(it);
          aMulitRowRegion.insert(it, cross);
          it = aMulitRowRegion.begin() + i;//Add BY LJH modify error.20191217
          aMulitRowRegion.insert(it, start);
        } 
      }else// make vertical line of segment hi through point j,the cross point must be at segment hi
      {
        double tmpDistance = getDistance(aMulitRowRegion[j].x, aMulitRowRegion[j].y, aMulitRowRegion[i].x, aMulitRowRegion[i].y);
        start = { aMulitRowRegion[i].x + tmpDistance*cos(fabs(inner_angle))*cos(theta_h_i), aMulitRowRegion[i].y + tmpDistance*cos(fabs(inner_angle))*sin(theta_h_i) };

        aSingleRowRegionSet[aSingleRowRegionSet.size()-1].push_back(start);
        aSingleRowRegionSet[aSingleRowRegionSet.size()-1].push_back(aMulitRowRegion[i]);
        aSingleRowRegionSet[aSingleRowRegionSet.size()-1].push_back(aMulitRowRegion[j]);
        std::vector<fwing_va>::iterator it = aMulitRowRegion.begin() + i;
        it = aMulitRowRegion.erase(it);
        aMulitRowRegion.insert(it, start);
      }// *** end make vertical line of segment hi through point j,the cross point must be at segment hi
      i++;
    }// ** end while
  }// * end partition

  // multirow region deployment
  void FwingDecideActionVirtualImpl::deployMultiRowInner(const double &aDesiredDistance, 
                                                         const std::vector<fwing_va> &aMultiRowRegion, 
                                                               std::vector<fwing_va> &aPointsSet)
  {
    int k = aMultiRowRegion.size();
    // find the longest boundary and its endpoints of multirow region
    _startPointNumber = k - 1;    // number of start point of the longest boundary
    double d = (aMultiRowRegion[_startPointNumber].x - aMultiRowRegion[0].x) * (aMultiRowRegion[_startPointNumber].x - aMultiRowRegion[0].x)
             + (aMultiRowRegion[_startPointNumber].y - aMultiRowRegion[0].y) * (aMultiRowRegion[_startPointNumber].y - aMultiRowRegion[0].y);
    for(int i = 0 ; i < k-1; i++)
    {
      double tmp = (aMultiRowRegion[i+1].x - aMultiRowRegion[i].x) * (aMultiRowRegion[i+1].x - aMultiRowRegion[i].x)
                 + (aMultiRowRegion[i+1].y - aMultiRowRegion[i].y) * (aMultiRowRegion[i+1].y - aMultiRowRegion[i].y);
      if(tmp > d)
      {
        d = tmp;
        _startPointNumber = i;
      }
    }
    // find start point of deployment
    double theta = atan2((aMultiRowRegion[_startPointNumber+1].y - aMultiRowRegion[_startPointNumber].y) , 
                         (aMultiRowRegion[_startPointNumber+1].x - aMultiRowRegion[_startPointNumber].x));
    bool startPointFinded = false;
    fwing_va start;
    for(int i = _startPointNumber; i <= _startPointNumber + 1; i++)
    {
      std::vector<fwing_va> possibleStartPoints = { 
        {aMultiRowRegion[i].x + aDesiredDistance * cos(theta+asin(0.5)),aMultiRowRegion[i].y + aDesiredDistance * sin(theta+asin(0.5))},
        {aMultiRowRegion[i].x - aDesiredDistance * cos(theta+asin(0.5)),aMultiRowRegion[i].y - aDesiredDistance * sin(theta+asin(0.5))},
        {aMultiRowRegion[i].x + aDesiredDistance * cos(theta-asin(0.5)),aMultiRowRegion[i].y + aDesiredDistance * sin(theta-asin(0.5))},
        {aMultiRowRegion[i].x - aDesiredDistance * cos(theta-asin(0.5)),aMultiRowRegion[i].y - aDesiredDistance * sin(theta-asin(0.5))}
      };
      int j;
      for(j = 0; j < 6; j++)
      {
        if(isInRegion(possibleStartPoints[j], aMultiRowRegion) == true)
        {
          startPointFinded = true;
          break;
        }
      }
      if(startPointFinded)
      {
        start = possibleStartPoints[j];
        break;
      }
    }
    if(!startPointFinded)
      std::cout << "[decide_action_VA_impl] Failed to find the start point !" << std::endl;
    // ** end find start point of deployment

    // BFS
    std::vector<fwing_va> queueFrontier;  // frontier searching queue
    std::vector<fwing_va> queueNext;  // next searching queue// multirow region deployment
    queueNext.push_back(start);
    aPointsSet.push_back(start);
    std::vector<fwing_va> adjacentPoints; // save adjacent points
    std::vector<fwing_va> expandedPoints;      // save enpanded points
    while(!queueNext.empty())
    {
      queueFrontier = queueNext;
      std::vector<fwing_va>().swap(queueNext);
      for(int i = 0; i < queueFrontier.size(); i++)
      {
        std::vector<fwing_va>::iterator iter = std::find_if(expandedPoints.begin(), expandedPoints.end(), IsMatch(queueFrontier[i]));
        if(iter == expandedPoints.end())
          expandedPoints.push_back(queueFrontier[i]);
        getAdjacentPoints(aDesiredDistance, theta, queueFrontier[i], adjacentPoints);
        for(int j = 0 ; j < 6; j++)
        {
          if(std::find_if( expandedPoints.begin(),  expandedPoints.end(), IsMatch(adjacentPoints[j])) != expandedPoints.end() || 
             std::find_if(queueNext.begin(), queueNext.end(), IsMatch(adjacentPoints[j])) != queueNext.end() || 
             (!isInRegion(adjacentPoints[j], aMultiRowRegion) && !isAtBoundaries(adjacentPoints[j], aMultiRowRegion))
            )                
             continue;
          else
          {
            queueNext.push_back(adjacentPoints[j]);
            if(std::find_if(aPointsSet.begin(), aPointsSet.end(), IsMatch(adjacentPoints[j])) == aPointsSet.end())
              aPointsSet.push_back(adjacentPoints[j]);
          }
        }
        std::vector<fwing_va>().swap(adjacentPoints);
      }
    }// ** end BFS

  }// * end multirow region deployment

  void FwingDecideActionVirtualImpl::deployMultiRowBoundaries(const double &aDesiredDistance, 
                                                              const int &aStartPoint, 
                                                              const std::vector<fwing_va> &aMultiRowRegion, 
                                                                    std::vector<fwing_va> &aPointsSet)
  {
    int k = aMultiRowRegion.size();
    // adjust the order of boundaries according to aStartPoint
    std::vector<fwing_va> orderAdjustedBoundaries = aMultiRowRegion;
    if(aStartPoint <= (k - 1)/2)
    {
      orderAdjustedBoundaries.insert(orderAdjustedBoundaries.end(), orderAdjustedBoundaries.begin(), orderAdjustedBoundaries.begin() + aStartPoint + 1);
      orderAdjustedBoundaries.erase(orderAdjustedBoundaries.begin(), orderAdjustedBoundaries.begin() + aStartPoint + 1);
    }else
    {
      orderAdjustedBoundaries.insert(orderAdjustedBoundaries.begin(), orderAdjustedBoundaries.begin() + aStartPoint + 1, orderAdjustedBoundaries.end());
      orderAdjustedBoundaries.erase(orderAdjustedBoundaries.begin() + k , orderAdjustedBoundaries.end());
    }// ** end adjust the order of boundaries according to aStartPoint

    reverse(orderAdjustedBoundaries.begin(), orderAdjustedBoundaries.end());

    // calculate the first point of boundaries to be deployed of multirow region
    std::vector<fwing_va> seg = { orderAdjustedBoundaries[0], orderAdjustedBoundaries[1] };
    std::vector<fwing_va> paraLines;
    int tmpSize = orderAdjustedBoundaries.size();
    getParallels(aDesiredDistance/2, orderAdjustedBoundaries[0], orderAdjustedBoundaries[tmpSize - 1], paraLines);
    for(int i = 0; i < 2; i++)
    {
      fwing_va cross;
      bool crossGot = getCross(paraLines[2*i], paraLines[2*i + 1], orderAdjustedBoundaries[0], orderAdjustedBoundaries[1], cross);
      bool k = isAtBoundaries(cross, seg);
      if(crossGot && k)
      {
        orderAdjustedBoundaries[0] = cross;
        break;
      }
    }// ** end calculate the first point of boundaries to be deployed of multirow region

    std::vector<fwing_va> segmentDeployPoints;
    deploySegment(aDesiredDistance, orderAdjustedBoundaries, segmentDeployPoints);
    int k_seg = segmentDeployPoints.size();
    for(int i = 0; i < k_seg; i++)
    {
      if(std::find_if(aPointsSet.begin(), aPointsSet.end(), IsMatch(segmentDeployPoints[i])) ==  aPointsSet.end())
      {
        aPointsSet.push_back(segmentDeployPoints[i]);
      }        
    }
  }

  void FwingDecideActionVirtualImpl::deploySingleRow(const double &aDesiredDistance, 
                                                     const std::vector<std::vector<fwing_va>> &aSingleRowRegionSet, 
                                                           std::vector<fwing_va> &aPointsSet)
  {
    int k = aSingleRowRegionSet.size();
    for(int i = 0; i < k; i++)
    {
      std::vector <fwing_va> tmp;
      calculateMidpoints(aSingleRowRegionSet[i], tmp);
      deploySegment(aDesiredDistance, tmp, aPointsSet);
    }
  }

  bool FwingDecideActionVirtualImpl::getLinePara(const fwing_va &aStartVertex, 
                                                 const fwing_va &anEndVertex, 
                                                       fwing_va &aLinePara)
  {
    if(fabs(aStartVertex.x - anEndVertex.x) <= _paramPrecision.precision)
    {
      return false;
    }
    else
    {
      aLinePara.x = (anEndVertex.y - aStartVertex.y) / (anEndVertex.x - aStartVertex.x);
      aLinePara.y = aStartVertex.y - aLinePara.x * aStartVertex.x;
      return true;
    }
  }

  bool FwingDecideActionVirtualImpl::getCross(const fwing_va &aLineOneStart, 
                                              const fwing_va &aLineOneEnd, 
                                              const fwing_va &aLineTwoStart, 
                                              const fwing_va &aLineTwoEnd,
                                                    fwing_va &aCrossPoint)
  {
    fwing_va paraOne, paraTwo;
    bool paraOneGot = getLinePara(aLineOneStart, aLineOneEnd, paraOne);
    bool paraTwoGot = getLinePara(aLineTwoStart, aLineTwoEnd, paraTwo);

    if((!paraOneGot && !paraTwoGot) || paraOne.x == paraTwo.x)
    {
      return false;
    }
    else if (!paraOneGot && paraTwoGot)
    {
      aCrossPoint.x = (aLineOneStart.x + aLineOneEnd.x) / 2;
      aCrossPoint.y = paraTwo.x * aCrossPoint.x + paraTwo.y;
      return true;
    }
    else if (!paraTwoGot && paraOneGot)
    {
      aCrossPoint.x = (aLineTwoStart.x + aLineTwoEnd.x) / 2;
      aCrossPoint.y = paraOne.x * aCrossPoint.x + paraOne.y;
      return true;
    }
    else
    {
      aCrossPoint.x = (paraTwo.y - paraOne.y) / (paraOne.x - paraTwo.x);
      aCrossPoint.y = paraOne.x * aCrossPoint.x + paraOne.y;
      return true;
    }
  }
  
  double FwingDecideActionVirtualImpl::getDistance(const double &aPointOneX, const double &aPointOneY, 
                                                   const double &aPointTwoX, const double &aPointTwoY)
  {
    return (sqrt(pow(aPointOneX - aPointTwoX, 2) + pow(aPointOneY - aPointTwoY, 2)));
  }

  bool FwingDecideActionVirtualImpl::isAtBoundaries(const fwing_va &aPoint, 
                                                    const std::vector<fwing_va> &aTargetRegion)
  {
    int k = aTargetRegion.size();
    int i ,j;
    for(i = 0 ; i < k; i++)
    {
      if(i == k - 1)
        j = 0;
      else
        j = i + 1;
      double distanceSegment = getDistance(aTargetRegion[j].x, aTargetRegion[j].y, aTargetRegion[i].x, aTargetRegion[i].y);
      double distanceStartToPoint = getDistance(aPoint.x, aPoint.y, aTargetRegion[i].x, aTargetRegion[i].y);
      double distanceEndToPoint = getDistance(aTargetRegion[j].x, aTargetRegion[j].y,  aPoint.x,  aPoint.y);
      if(fabs(distanceStartToPoint + distanceEndToPoint - distanceSegment) <= _paramPrecision.precision)
        return true;
    }
    return false;
  }

  bool FwingDecideActionVirtualImpl::isInRegion(const fwing_va &aPoint, 
                                                const std::vector<fwing_va> &aTargetRegion)
  {
    if(isAtBoundaries(aPoint, aTargetRegion))
      return false;
    int nCross = 0;
    int k = aTargetRegion.size();
    for (int i = 0; i < k; i++)
    {
      fwing_va p1 = aTargetRegion[i];
      fwing_va p2 = aTargetRegion[(i + 1) % k];

      if(fabs(p1.y - p2.y) <= _paramPrecision.precision || aPoint.y < std::min(p1.y, p2.y) || aPoint.y > std::max(p1.y, p2.y))
        continue;
      double tmpX = (double)(aPoint.y - p1.y) * (double)(p2.x - p1.x) / (double)(p2.y - p1.y) + p1.x;
      if (tmpX > aPoint.x)
        nCross++;
    }
    return (nCross % 2 == 1);
  }

  void FwingDecideActionVirtualImpl::getParallels(const double &aDesiredDistance, 
                                                  const fwing_va &aStartPoint, 
                                                  const fwing_va &anEndPoint, 
                                                        std::vector<fwing_va> &aPairParalines)
  { 
    aPairParalines.resize(4);
    if(fabs(aStartPoint.x - anEndPoint.x) <= _paramPrecision.precision)
    {
      aPairParalines = { {aStartPoint.x - aDesiredDistance, aStartPoint.y}, 
                {anEndPoint.x - aDesiredDistance, anEndPoint.y}, 
                {aStartPoint.x + aDesiredDistance, aStartPoint.y}, 
                {anEndPoint.x + aDesiredDistance, anEndPoint.y} };
    }
    else
    {
      // angle with respect to the X-axis[-π, π)
      double theta = atan2((anEndPoint.y - aStartPoint.y) , (anEndPoint.x - aStartPoint.x));
      aPairParalines = { {aStartPoint.x, aStartPoint.y - aDesiredDistance/cos(theta)}, 
                         {anEndPoint.x, anEndPoint.y - aDesiredDistance/cos(theta)}, 
                         {aStartPoint.x, aStartPoint.y + aDesiredDistance/cos(theta)}, 
                         {anEndPoint.x, anEndPoint.y + aDesiredDistance/cos(theta)} };
    }
  }

  void FwingDecideActionVirtualImpl::getAdjacentPoints(const double &aDesiredDistance, 
                                                       const double &anAngle, 
                                                       const fwing_va &aPoint, 
                                                             std::vector<fwing_va> &anAdjacentsSet)
  {
    for(int i = 0; i < 6; i++)
    {
      double alpha = i * M_PI / 3.0;
      anAdjacentsSet.push_back({ aPoint.x + sqrt(3) * aDesiredDistance * cos(anAngle - alpha), 
                                 aPoint.y + sqrt(3) * aDesiredDistance * sin(anAngle - alpha) });
    }
  }

  void FwingDecideActionVirtualImpl::deploySegment(const double &aDesiredDistance, 
                                                   const std::vector<fwing_va> &aSegmentSet, 
                                                         std::vector<fwing_va> &aPointsSet)
  {
    int k = aSegmentSet.size();
    fwing_va tmp;
    for(int i = 0; i < k-1; i++)
    {
      std::vector<fwing_va> current_segment = { aSegmentSet[i], aSegmentSet[i+1] };
      double theta = atan2((aSegmentSet[i+1].y - aSegmentSet[i].y) , (aSegmentSet[i+1].x - aSegmentSet[i].x));
      tmp = aSegmentSet[i];
      while(isAtBoundaries(tmp, current_segment))
      {
        if(std::find_if(aPointsSet.begin(), aPointsSet.end(), IsMatch(tmp)) ==  aPointsSet.end())
          aPointsSet.push_back(tmp);

        fwing_va v = { tmp.x + sqrt(3) * aDesiredDistance * cos(theta), 
                       tmp.y + sqrt(3) * aDesiredDistance * sin(theta) };
        if(std::find_if(aPointsSet.begin(), aPointsSet.end(), IsMatch(v)) !=  aPointsSet.end() || !isAtBoundaries(v, current_segment))
          v = { tmp.x + sqrt(3) * aDesiredDistance * cos(theta + M_PI), 
                tmp.y + sqrt(3) * aDesiredDistance * sin(theta + M_PI) };
        tmp = v;
      }
    }
    if(std::find_if(aPointsSet.begin(), aPointsSet.end(), IsMatch(aSegmentSet[k - 1])) ==  aPointsSet.end())
      aPointsSet.push_back(aSegmentSet[k - 1]);
  }

  void FwingDecideActionVirtualImpl::calculateMidpoints(const std::vector<fwing_va> &aTargetRegion, 
                                                              std::vector<fwing_va> &aMidpointsSet)
  {
    int k = aTargetRegion.size();
    int i = 0, j = k - 1;
    while(i <= j)
    {
      aMidpointsSet.push_back({(aTargetRegion[i].x + aTargetRegion[j].x)/2, 
                               (aTargetRegion[i].y + aTargetRegion[j].y)/2});
      if(j - i > 2)
      {
        aMidpointsSet.push_back({(aTargetRegion[i].x + aTargetRegion[j-1].x)/2, 
                                 (aTargetRegion[i].y + aTargetRegion[j-1].y)/2});
      }
      i++;
      j--;
    }
  }

  bool FwingDecideActionVirtualImpl::isLNArea(const double &aDesiredDistance, 
                                              const std::vector<fwing_va> &aTargetRegion) 
  {
    int s = aTargetRegion.size();
    int i = 0, j = s-1;
    while(i <= j)
    {
      double ijDiatance = getDistance(aTargetRegion[j].x, aTargetRegion[j].y, 
                                      aTargetRegion[i].x, aTargetRegion[i].y);
      if(ijDiatance > aDesiredDistance)
        return false;
      i++;
      j--;
    }
    return true;
  }
} //end namespace

PLUGINLIB_EXPORT_CLASS(fwing_decide_action_virtual_impl::FwingDecideActionVirtualImpl, decide_action_interface::DecideActionInterface)






