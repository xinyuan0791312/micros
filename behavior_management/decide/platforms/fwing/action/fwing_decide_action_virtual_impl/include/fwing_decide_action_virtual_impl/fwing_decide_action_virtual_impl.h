/* ==================================================================
* Copyright (c) 2018, micROS Group, TAIIC, NIIDT & HPCL.
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions
* are met:
*
* 1. Redistributions of source code must retain the above copyright
* notice, this list of conditions and the following disclaimer.
* 2. Redistributions in binary form must reproduce the above copyright
* notice, this list of conditions and the following disclaimer in the
* documentation and/or other materials provided with the
* distribution.
* 3. All advertising materials mentioning features or use of this software
* must display the following acknowledgement:
* This product includes software developed by the micROS Group. and
* its contributors.
* 4. Neither the name of the Group nor the names of its contributors may
* be used to endorse or promote products derived from this software
* without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY MICROS,GROUP AND CONTRIBUTORS
* ''AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT
* NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
* FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
* THE MICROS,GROUP OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
* OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
* HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
* OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
* IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
* ===================================================================
* Author: Ren Xiaoguang, Wu Yunlong, Li Jinghua, micROS-DA Team, NIIDT, TAIIC.
*/

#ifndef FWING_DECIDE_ACTION_VIRTUAL_IMPL_H
#define FWING_DECIDE_ACTION_VIRTUAL_IMPL_H

#include "message_types.h"
#include <pluginlib/class_list_macros.h>
#include "decide_softbus/decide_action_interface.h"

#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#include "common/param_precision.h"
#include "common/decide/action/decide_action_virtual_agent.h"
#include "common/decide/action/decide_action_virtual_agent_out.h"

/**
 * @brief Fwing Decide Action Virtual Impl Namespace.
 */
namespace fwing_decide_action_virtual_impl
{
  /**
   * @brief Fixed-wing virtual agent struct
   */
  #pragma pack (1)
  typedef struct __fwing_va
  {
    float x;  ///< This is the position in east direction
    float y;  ///< This is the position in north direction
  }fwing_va;
  #pragma pack()

  /**
   * @brief This is a class to compare self-defined struct variables.
   */
  class IsMatch
  {
    fwing_va v;
  public:
    IsMatch(fwing_va vv) : v(vv){}
    bool operator()(fwing_va tmp) const
    {
      return (fabs(v.x - tmp.x) <= 1e-5 && fabs(v.y - tmp.y) < 1e-5);
    }
  };

  /**
   * @brief This is a class to implement the algorithm plugin for algorithm FwingDecideActionVirtualImpl.
   */
  class FwingDecideActionVirtualImpl : public decide_action_interface::DecideActionInterface
  {
    public:

      FwingDecideActionVirtualImpl();

      ~FwingDecideActionVirtualImpl();

      /**
       * @brief This is the main function for the algorithms equipped in the action node of BT in the decide stage.
       * 
       * @param[in] pInputBuffer pointer to the input buffer
       * @param[out] pOutputBuffer pointer to the output buffer
       */
      virtual void getPlan( unified_msg::unified_message_t *pInputBuffer, 
                            unified_msg::unified_message_t *pOutputBuffer);

      /** @brief This is the function for setting the parameters for the algorithms.
       * 
       * @param[in] pParams pointer to the parameter setting buffer
       */
      virtual void initializeParam(unified_msg::unified_message_t *pParams);

    private:

      int _startPointNumber;

      std::vector<fwing_va> _deployPointsV;  ///< This is the set of position of VAs
      std::vector<fwing_va> _originRegionV;  ///< This is the set of vertices of origin_region (in order)
      std::vector<fwing_va> _multiRowRegionV;  ///< This is the set of vertices of multi_row_region (in order)
      
      std::vector< std::vector <fwing_va> > _singleRowRegionVV;  ///< This is the set of vertices of single row regions (in order)
      
      // decide_action_virtual_agent_t _payload;  ///< This is the struct saving input data 
      
      param_precision_t _paramPrecision;  ///< This is the struct saving parameters
      
      /**
       * @brief Find parallels of target line function
       * 
       * @param[in] aDesiredDistance specifies the desired distance of parallels from target line
       * @param[in] aStartPoint specifies the start point of target line
       * @param[in] anEndPoint specifies the end point of target line
       * @param[out] aPairParalines obtains vertices of parallels 
       */
      void getParallels(const double &aDesiredDistance, 
                        const fwing_va &aStartPoint, 
                        const fwing_va &anEndPoint, 
                              std::vector<fwing_va> &aPairParalines);
      
      /**
       * @brief Find the adjacent points function
       * 
       * @param[in] aDesiredDistance specifies the distance between adjacent point and target point
       * @param[in] anAngle specifies the angle of the longest boundary (x axis)
       * @param[in] aPoint specifies the position of target point
       * @param[out] anAdjacentsSet obtains 6 adjacent points of target point
       */
      void getAdjacentPoints(const double &aDesiredDistance, 
                             const double &anAngle, 
                             const fwing_va &aPoint, 
                                   std::vector<fwing_va> &anAdjacentsSet);
      
      /**
       * @brief Calculate midpoints of single row region
       * 
       * @param[in] aTargetRegion specifies the ordered vertices of single row region
       * @param[out] aMidpointsSet obtains midpoints of single row region
       */
      void calculateMidpoints(const std::vector<fwing_va> &aTargetRegion, 
                                    std::vector<fwing_va> &aMidpointsSet);
      
      /**
       * @brief Partition origional region into multirow region and single row regions
       * 
       * @param[in] aDesiredDistance specifies the radius of trjectory
       * @param[in] anOriginRegion specifies original region
       * @param[out] aMulitRowRegion obtains multirow region
       * @param[out] aSingleRowRegionSet obtains single row region
       */
      void partition(const double &aDesiredDistance, 
                     const std::vector<fwing_va> &anOriginRegion, 
                           std::vector<fwing_va> &aMulitRowRegion, 
                           std::vector<std::vector<fwing_va> > &aSingleRowRegionSet);
      
      /**
       * @brief Deploy virtual agents of original region function
       * 
       * @param[in] anOriginRegion specifies the input original region
       * @param[in] aDesiredDistance specifies desired radius of trajectory after deploy
       * @param[in] aQuantity specifies total quantity of MAVs
       */
      void deployVirtualAgents(const std::vector<fwing_va> &anOriginRegion, 
                               const float &aDesiredDistance, 
                               const int &aQuantity);   
      
      /**
       * @brief Deploy virtual agents of inner multirow region function
       * 
       * @param[in] aDesiredDistance specifies the radius of trajectory 
       * @param[in] aMultiRowRegion specifies multirow region
       * @param[out] aPointsSet obtains position of virtual agents of inner multirow region
       */
      void deployMultiRowInner(const double &aDesiredDistance, 
                               const std::vector<fwing_va> &aMultiRowRegion, 
                                     std::vector<fwing_va> &aPointsSet);
      
      /**
       * @brief Deploy virtual agents of remaining boundries of multirow region function
       * 
       * @param[in] aDesiredDistance specifies the radius of trajectory
       * @param[in] aStartPoint specifies the number of the longest boundry in the vertices of multirow region 
       * @param[in] aMultiRowRegion specifies multirow region
       * @param[out] aPointsSet obtains position of virtual agents of boundries of multirow region 
       */
      void deployMultiRowBoundaries(const double &aDesiredDistance, 
                                    const int &aStartPoint, 
                                    const std::vector<fwing_va> &aMultiRowRegion, 
                                          std::vector<fwing_va> &aPointsSet);
      
      /**
       * @brief Deploy virtual agents of original region function
       * 
       * @param[in] aDesiredDistance specifies the radius of trajectory
       * @param[in] aSegmentSet specifies segment to deploy virtual agent
       * @param[out] aPointsSet obtains position of virtual agents of target segment
       */
      void deploySegment(const double &aDesiredDistance, 
                         const std::vector<fwing_va> &aSegmentSet, 
                               std::vector<fwing_va> &aPointsSet);
      
      /**
       * @brief Deploy virtual agents of original region function
       * 
       * @param[in] aDesiredDistance specifies the radius of trajectory
       * @param[in] aSingleRowRegionSet specifies single rew region 
       * @param[out] aPointsSet obtains position of virtual agents of single row region
       */
      void deploySingleRow(const double &aDesiredDistance, 
                           const std::vector<std::vector<fwing_va>> &aSingleRowRegionSet, 
                                 std::vector<fwing_va> &aPointsSet);

      /**
       * @brief Get the Distance object
       * 
       * @param[in] aPointOneX specifies the position along x axis of point1
       * @param[in] aPointOneY specifies the position along y axis of point1
       * @param[in] aPointTwoX specifies the position along x axis of point2
       * @param[in] aPointTwoY specifies the position along y axis of point2
       * 
       * @return double distance between point1 and point2
       */
      double getDistance(const double &aPointOneX, 
                         const double &aPointOneY, 
                         const double &aPointTwoX, 
                         const double &aPointTwoY);

      /**
       * @brief Get the Line Para object
       * 
       * @param[in] aStartVertex specifies the start point of the targrt segement
       * @param[in] anEndVertex specifies the end point of the targrt segement
       * @param[out] aLinePara obtains the parameters, for instance intercept, slope
       * 
       * @return true demonstrates slope is finite
       * @return false demonstrates slope is infinite
       */ 
      bool getLinePara(const fwing_va &aStartVertex, 
                       const fwing_va &anEndVertex, 
                             fwing_va &aLinePara);
      
      /**
       * @brief Get the cross of 2 segments function
       * 
       * @param[in] aLineOneStart specifies start point of segment1
       * @param[in] aLineOneEnd specifies end point of segment1
       * @param[in] aLineTwoStart specifies start point of segment2
       * @param[in] aLineTwoEnd specifies end point of segment2
       * @param[out] aCrossPoint obtains the cross of segment1 and segment2
       * 
       * @return true demonstrates find the cross of segment1 and segment2
       * @return false demonstrates did NOT find the cross of segment1 and segment2
       */
      bool getCross(const fwing_va &aLineOneStart, 
                    const fwing_va &aLineOneEnd, 
                    const fwing_va &aLineTwoStart, 
                    const fwing_va &aLineTwoEnd, 
                          fwing_va &aCrossPoint);
      
      /**
       * @brief Discriminate whether the point is at current boundries function
       * 
       * @param[in] aPoint specifies the point to be discriminated
       * @param[in] aTargetRegion specifies current boundaries
       * 
       * @return true demonstrates the point is at boundries
       * @return false demonstrates the point is NOT at boundries
       */
      bool isAtBoundaries(const fwing_va &aPoint, 
                          const std::vector<fwing_va> &aTargetRegion);
     
      /**
       * @brief Discriminate whether the point is in current region function
       * 
       * @param[in] aPoint specifies specifies the point to be discriminated
       * @param[in] aTargetRegion specifies specifies current region
       * 
       * @return true demonstrates the point is in current region
       * @return false demonstrates the point is NOT in current region
       */
      bool isInRegion(const fwing_va &aPoint, 
                      const std::vector<fwing_va> &aTargetRegion);
      
      /**
       * @brief Discriminate whether the region is long & narrow region function
       * 
       * @param[in] aDesiredDistance specifies the radius of trajectory
       * @param[in] aTargetRegion specifies the region to be discriminated
       * 
       * @return true demonstrates the region is long & narrow region  
       * @return false demonstrates the region is NOT long & narrow region 
       */
      bool isLNArea(const double &aDesiredDistance, 
                    const std::vector<fwing_va> &aTargetRegion);
  };
};
#endif