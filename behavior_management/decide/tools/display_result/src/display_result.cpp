/* ==================================================================
* Copyright (c) 2018, micROS Group, TAIIC, NIIDT & HPCL.
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions
* are met:
*
* 1. Redistributions of source code must retain the above copyright
* notice, this list of conditions and the following disclaimer.
* 2. Redistributions in binary form must reproduce the above copyright
* notice, this list of conditions and the following disclaimer in the
* documentation and/or other materials provided with the
* distribution.
* 3. All advertising materials mentioning features or use of this software
* must display the following acknowledgement:
* This product includes software developed by the micROS Group. and
* its contributors.
* 4. Neither the name of the Group nor the names of its contributors may
* be used to endorse or promote products derived from this software
* without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY MICROS,GROUP AND CONTRIBUTORS
* ''AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT
* NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
* FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
* THE MICROS,GROUP OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
* OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
* HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
* OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
* IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
* ===================================================================
* Author: Shang Erke, Jiang Weizhong, Zhu Qi, micROS-DA Team, TAIIC.
*/

#include "display_result.h"

DisplayResult::DisplayResult() 
{
    nh_ = ros::NodeHandle();
    threshod_value_ = 30;

    received_guide_waypoints_ = false;
    received_goal_pose_local_ = false;
    received_costmap_ = false;
    received_safe_way_ = false;
    received_current_pose_local_ = false;
    received_count_value_ = false;

    costmap_.info.height = 250;
    costmap_.info.width = 250;
    costmap_.info.resolution = 1;
    costmap_.info.origin.position.x = 0;    costmap_.info.origin.position.y = 0;    costmap_.info.origin.position.z = 0;
    costmap_.info.origin.orientation = direction(0);
    for(int i = 0;i<costmap_.info.height;i++ )
    {
        for(int j = 0;j<costmap_.info.width;j++ )
        {
            costmap_.data.push_back(0);
        }
    }
    current_pose_local_.pose.position.x =  costmap_.info.width/2;
    current_pose_local_.pose.position.y = 1;
    current_pose_local_.pose.position.z = 0;
    current_pose_local_.pose.orientation = direction(90);
    count_value_ = 10;

    show_img_ = cv::Mat_<cv::Vec3b>(costmap_.info.height,costmap_.info.width, cv::Vec3b(255, 255, 255));

    subscribeRun();
}

void DisplayResult::displayRun() 
{
    if (received_guide_waypoints_) 
    {
        roadEdgeBuild(guide_waypoints_, threshod_value_);
    }

    if (received_guide_waypoints_ && 
        received_goal_pose_local_ && 
        received_costmap_ && 
        received_safe_way_ && 
        received_current_pose_local_ && 
        received_speed_value_ && 
        received_count_value_) 
    {
        showResult();
    }

    received_guide_waypoints_ = false;
    received_goal_pose_local_ = false;
    received_costmap_ = false;
    received_safe_way_ = false;
    received_current_pose_local_ = false;
    received_speed_value_ = false;
    received_count_value_ = false;
}

void DisplayResult::subscribeRun() 
{
    guide_waypoints_sub_ = nh_.subscribe("/sirius/astar_avoid_plugin/temp_guide_waypoints", 1, &DisplayResult::guideWaypointsCallback, this);
    goal_pose_local_sub_ = nh_.subscribe("/sirius/astar_avoid_plugin/temp_goal_pose_local", 1, &DisplayResult::goalPoseLocalCallback, this);
    costmap_sub_ = nh_.subscribe("/sirius/astar_avoid_plugin/temp_costmap", 1, &DisplayResult::costmapCallback, this);
    safe_way_sub_ = nh_.subscribe("/sirius/astar_avoid_plugin/temp_safe_way", 1, &DisplayResult::safeWayCallback, this);
    current_pose_local_sub_ = nh_.subscribe("/sirius/astar_avoid_plugin/temp_current_pose_local", 1, &DisplayResult::currentPoseLocalCallback, this);
    speed_value_sub_ = nh_.subscribe("/sirius/astar_avoid_plugin/temp_speed_value", 1, &DisplayResult::speedValueCallback, this);
    count_value_sub_ = nh_.subscribe("/sirius/astar_avoid_plugin/temp_count_value", 1, &DisplayResult::countValueCallback, this);
}

void DisplayResult::guideWaypointsCallback(const sirius_msgs::GuideTrajectoryL& guide_waypoints) 
{
    guide_waypoints_ = guide_waypoints;
    received_guide_waypoints_ = true;
}

void DisplayResult::goalPoseLocalCallback(const geometry_msgs::PoseStamped& goal_pose_local) 
{
    goal_pose_local_ = goal_pose_local;
    received_goal_pose_local_ = true;
}

void DisplayResult::costmapCallback(const nav_msgs::OccupancyGrid& costmap) 
{
    costmap_ = costmap;
    received_costmap_ = true;
}

void DisplayResult::safeWayCallback(const sirius_msgs::GuideTrajectoryL& safe_way) 
{
    safe_way_ = safe_way;
    received_safe_way_ = true;
}

void DisplayResult::currentPoseLocalCallback(const geometry_msgs::PoseStamped& current_pose_local) 
{
    current_pose_local_ = current_pose_local;
    received_current_pose_local_ = true;
}

void DisplayResult::speedValueCallback(const std_msgs::Float32& speed_value) 
{
    speed_value_ = speed_value.data;
    received_speed_value_ = true;
}

void DisplayResult::countValueCallback(const std_msgs::Float32& count_value) 
{
    count_value_ = count_value.data;
    received_count_value_ = true;
}

geometry_msgs::Quaternion DisplayResult::direction(double theta_)
{
    double theta = theta_*PI/180;
    double a[9] = {0};
    a[8] = 1.0;
    double q[4] = {0};
    q[0] = 1.0;
    a[0] = cos(theta);
     a[1] = sin(theta);
     a[3] = - a[1];
     a[4] = a[0];
     geometry_msgs::Quaternion result;
     q[0] = sqrt(a[0] + a[4] + a[8] + 1)/2;
     q[1] = (a[5] - a[7])/(4*q[0]);
     q[2] = (a[6] - a[2])/(4*q[0]);
     q[3] = (a[1] - a[3])/(4*q[0]);
     result.w = q[0];
     result.x = q[1];
     result.y = q[2];
     result.z = q[3];
     return result;
}

double DisplayResult::goalPoseDir(double x1,double y1,double x2,double y2)
{
    double goal_pose_dir_value;
    double dx = x2 - x1;
    double dy = y2 - y1;
    if (dx == 0)
         goal_pose_dir_value = 90;
    else
        goal_pose_dir_value  = atan(dy/dx)*180/PI;
    if(y2 >= y1)
        return(goal_pose_dir_value < 0 ? 180 + goal_pose_dir_value:goal_pose_dir_value);
    else
        return(goal_pose_dir_value > 0 ? 180 + goal_pose_dir_value:goal_pose_dir_value);
}

void DisplayResult::showResult()
{
    show_img_ = cv::Mat_<cv::Vec3b>(costmap_.info.height,costmap_.info.width, cv::Vec3b(255, 255, 255));
    cv::line(show_img_,cv::Point(0,50),cv::Point(costmap_.info.width - 1,50),CV_RGB(255,0,0));
    cv::line(show_img_,cv::Point(0,100),cv::Point(costmap_.info.width - 1,100),CV_RGB(255,0,0));
    cv::line(show_img_,cv::Point(0,150),cv::Point(costmap_.info.width - 1,150),CV_RGB(255,0,0));
    cv::line(show_img_,cv::Point(0,200),cv::Point(costmap_.info.width - 1,200),CV_RGB(255,0,0));
    cv::line(show_img_,cv::Point(costmap_.info.width/2,0),cv::Point(costmap_.info.width/2,costmap_.info.height - 1),CV_RGB(255,0,0));
    cv::line(show_img_,cv::Point(costmap_.info.width/2 - 50,0),cv::Point(costmap_.info.width/2 - 50,costmap_.info.height - 1),CV_RGB(255,0,0));
    cv::line(show_img_,cv::Point(costmap_.info.width/2 + 50,0),cv::Point(costmap_.info.width/2 + 50,costmap_.info.height - 1),CV_RGB(255,0,0));
    if(1)
    {
        for(int i = 0;i < guide_waypoints_.point_num;i++ )
        {
            int x = guide_waypoints_.points[i].x;
            int y = guide_waypoints_.points[i].y;
            int tx = (costmap_.info.height - y);
            int ty = x;
            if (tx > 0 && tx < costmap_.info.height && ty > 0 && ty < costmap_.info.width)
            {
                cv::circle(show_img_,cv::Point(ty,tx),1,cv::Scalar(255,0,0),1);
            }
        }
    }


    for(int y = 0;y < costmap_.info.height;y++ )
    {
        for(int x = 0;x < costmap_.info.width;x++ )
        {
            if(costmap_.data[y*costmap_.info.width + x] > 0 )//
            {
                int tx = (costmap_.info.height - y);
                int ty = x;
                if (tx > 0 && tx < costmap_.info.height && ty > 0 && ty < costmap_.info.width)
                {
                    if(costmap_.data[y*costmap_.info.width + x]  == 101)
                    {
                        show_img_.at<cv::Vec3b>(tx,ty) = cv::Vec3b(0,255,0);
                    }
                    else
                        show_img_.at<cv::Vec3b>(tx,ty) = cv::Vec3b(0,0,255);
                }
            }
        }
    }

    cv::String str;

    if(count_value_ == - 1)
    {
        for(int i = 0;i < safe_way_.point_num;i++ )
        {
            int x = safe_way_.points[i].x;
            int y = safe_way_.points[i].y;
            cv::circle(show_img_,cv::Point(x,costmap_.info.height - y),1,cv::Scalar(255,255,0),1);
        }
        std::string tmp = std::string("org road!spd: ") + std::to_string(speed_value_);
        str = tmp.c_str();
        cv::putText(show_img_,str,cv::Point(20,20),cv::FONT_HERSHEY_SIMPLEX,0.6,
                    CV_RGB(255, 0, 180), 2, 8, false);
    }
    else if(count_value_ == - 2)
    {
        for(int i = 0;i < safe_way_.point_num;i++ )
        {
            int x = safe_way_.points[i].x;
            int y = safe_way_.points[i].y;
            cv::circle(show_img_,cv::Point(x,costmap_.info.height - y),1,cv::Scalar(0,0,255),1);
        }
        std::string tmp = std::string("org plan!spd: ") + std::to_string(speed_value_);
        str = tmp.c_str();
        cv::putText(show_img_,str,cv::Point(20,20),cv::FONT_HERSHEY_SIMPLEX,0.6,
                    CV_RGB(255, 0, 180), 1, 8, false);
    }
    else if(count_value_ < 10)
    {
        int x = current_pose_local_.pose.position.x;
        int y = current_pose_local_.pose.position.y;
        cv::circle(show_img_,cv::Point(x,costmap_.info.height - y),5,cv::Scalar(255,0,0),1);
        x = goal_pose_local_.pose.position.x;
        y = goal_pose_local_.pose.position.y;
        cv::circle(show_img_,cv::Point(x,costmap_.info.height - y),5,cv::Scalar(0,255,0),1);


        if(count_value_ == 0)
        {
            std::string tmp = std::string("AStar success!spd: ") + std::to_string(speed_value_);
            str = tmp.c_str();
            cv::putText(show_img_,str,cv::Point(20,20),cv::FONT_HERSHEY_SIMPLEX,0.6,
                        CV_RGB(255, 0, 180), 1, 8, false);
            for(int i = 0;i < safe_way_.point_num;i++ )
            {
                int x = safe_way_.points[i].x;
                int y = safe_way_.points[i].y;
                cv::circle(show_img_,cv::Point(x,costmap_.info.height - y),1,cv::Scalar(0,255,0),1);
            }
        }
        else
        {
            std::string tmp = std::string("AStar keeping!spd: ") + std::to_string(speed_value_);
            str = tmp.c_str();
            cv::putText(show_img_,str,cv::Point(20,20),cv::FONT_HERSHEY_SIMPLEX,0.6,
                        CV_RGB(255, 0, 180), 1, 8, false);
            for(int i = 0;i < safe_way_.point_num;i++ )
            {
                int x = safe_way_.points[i].x;
                int y = safe_way_.points[i].y;
                cv::circle(show_img_,cv::Point(x,costmap_.info.height - y),1,cv::Scalar(255,255,0),1);
            }
        }
     } else 
     {
        int x = current_pose_local_.pose.position.x;
        int y = current_pose_local_.pose.position.y;
        cv::circle(show_img_,cv::Point(x,costmap_.info.height - y),5,cv::Scalar(255,0,0),1);
        x = goal_pose_local_.pose.position.x;
        y = goal_pose_local_.pose.position.y;
        cv::circle(show_img_,cv::Point(x,costmap_.info.height - y),5,cv::Scalar(0,255,0),1);

        std::string tmp = std::string("AStar false!spd: ") + std::to_string(speed_value_);
        str = tmp.c_str();
        cv::putText(show_img_,str,cv::Point(20,20),cv::FONT_HERSHEY_SIMPLEX,0.6,
                    CV_RGB(255, 0, 180), 1, 8, false);
    }
    cv::namedWindow("map_astar",CV_WINDOW_NORMAL);
    cv::imshow("map_astar",show_img_);
    cvWaitKey(10);

}


void DisplayResult::roadEdgeBuild(sirius_msgs::GuideTrajectoryL guide_way,double threshod)
{
    cv::Mat img_baseWaypoints = cv::Mat_<cv::Vec3b>(costmap_.info.height,costmap_.info.width, cv::Vec3b(255, 255, 255));
    cv::line(img_baseWaypoints,cv::Point(0,50),cv::Point(costmap_.info.width - 1,50),CV_RGB(255,0,0));
    cv::line(img_baseWaypoints,cv::Point(0,100),cv::Point(costmap_.info.width - 1,100),CV_RGB(255,0,0));
    cv::line(img_baseWaypoints,cv::Point(0,150),cv::Point(costmap_.info.width - 1,150),CV_RGB(255,0,0));
    cv::line(img_baseWaypoints,cv::Point(0,200),cv::Point(costmap_.info.width - 1,200),CV_RGB(255,0,0));
    cv::line(img_baseWaypoints,cv::Point(costmap_.info.width/2,0),cv::Point(costmap_.info.width/2,costmap_.info.height - 1),CV_RGB(255,0,0));

    sirius_msgs::GuideTrajectoryL l_way,r_way;
    l_way = guide_way;
    r_way = guide_way;

    for(int i = 0;i < guide_way.point_num - 1;i++ )
    {
        double x1 = guide_way.points[i].x;
        double y1 = guide_way.points[i].y;
        double x2 = guide_way.points[i + 1].x;
        double y2 = guide_way.points[i + 1].y;
        if(std::fabs(x2 - x1) < 0.1)
        {
            l_way.points[i].x = x1 - threshod;
            l_way.points[i].y = y1;
            r_way.points[i].x = x1 + threshod;
            r_way.points[i].y = y1;
        }
        else
        {
          double theta = goalPoseDir(x1,y1,x2,y2);

          double k1 = - 1/tan(theta*PI/180);
          double theta1 = theta > 90 ? theta - 90:90 - theta;
          //double theta1 = 90 - theta;
          double dis = threshod*cos(theta1*PI/180);
          l_way.points[i].x = x1 - dis;
          r_way.points[i].x = x1 + dis;
          l_way.points[i].y = y1 + k1*( l_way.points[i].x - x1);
          r_way.points[i].y = y1 + k1*( r_way.points[i].x - x1);
        }
    }
    for(int i = 0;i < guide_way.point_num - 1;i++ )
    {
        int x = guide_way.points[i].x;
        int y = guide_way.points[i].y;
        int tx = (costmap_.info.height - y);
        int ty = x;
        if (tx > 0 && tx < costmap_.info.height && ty > 0 && ty < costmap_.info.width)
            img_baseWaypoints.at<cv::Vec3b>(tx,ty) = cv::Vec3b(255,0,0);
         x = l_way.points[i].x;
         y = l_way.points[i].y;
         tx = (costmap_.info.height - y);
         ty = x;
        if (tx > 0 && tx < costmap_.info.height && ty > 0 && ty < costmap_.info.width)
            img_baseWaypoints.at<cv::Vec3b>(tx,ty) = cv::Vec3b(0,255,0);
        x = r_way.points[i].x;
        y = r_way.points[i].y;
        tx = (costmap_.info.height - y);
        ty = x;
       if (tx > 0 && tx < costmap_.info.height && ty>0 && ty < costmap_.info.width)
           img_baseWaypoints.at<cv::Vec3b>(tx,ty) = cv::Vec3b(0,0,255);
    }

    cv::namedWindow("map_guide_path",CV_WINDOW_NORMAL);
    cv::imshow("map_guide_path",img_baseWaypoints);
    cvWaitKey(1);
}