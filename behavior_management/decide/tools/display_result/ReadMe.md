一、配置新版 Fast-RTPS (版本为1.9)
1、 删除之前安装的 FAST-RTPS
        sudo rm -r /usr/local/include/fast*
        sudo rm -r /usr/local/lib/libfast*
        sudo rm -r /usr/local/share/fast*
        sudo rm -r /usr/local/bin/fast*
        
2、 配置 Fast-RTPS-1.9
        gedit ~/.bashrc
        写入：
            # fastrtps settings
            export FASTRTPS_ROOT=/home/jwz/softwares/Fast-RTPS-1.9/usr/local
            export PATH=$FASTRTPS_ROOT/bin:$PATH
            export LD_LIBRARY_PATH=$FASTRTPS_ROOT/lib:$LD_LIBRARY_PATH
            export LIBRARY_PATH=$FASTRTPS_ROOT/lib:$LIBRARY_PATH
            export C_INCLUDE_PATH=$FASTRTPS_ROOT/include:$C_INCLUDE_PATH
            export CPLUS_INCLUDE_PATH=$FASTRTPS_ROOT/include:$CPLUS_INCLUDE_PATH
            
3、 修改 /Fast-RTPS-1.9/build/CMakeCache.txt 中的相关内容:
        搜索关键字 "dibin"，将相关的而路径改为自己的路径
        
4、 编译
        cd Fast-RTPS-1.9/build
        cmake ..
        make -j12
        
5、 重启计算机

6、 sudo ldconfig

7、 ldd /home/jwz/softwares/Fast-RTPS-1.9/usr/local/lib/libfastrtps.so


8、 运行插件
###    
# 
    source devel/setup.bash
    roslaunch actor_daemon_node actor_daemon_vehicle_astar_avoid.launch
    
# 
    source devel/setup.bash
    roslaunch g_station astar_avoid_ground_station.launch
    
### path of task.xml
    astar_avoid_task.xml
    

9、 运行可视化节点

    roslaunch display_result display_result_node.launch
    
    
10、运行 rosbag 包

    rosbag play /home/wyl/Desktop/micROS_test/20191203/src/micros/behavior_management/decide/tools/display_result/2019-10-09-15-38-31.bag


### 查看插件包
rospack plugins --attrib=plugin general_plugin




    
