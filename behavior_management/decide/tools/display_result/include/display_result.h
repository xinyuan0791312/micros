/* ==================================================================
* Copyright (c) 2018, micROS Group, TAIIC, NIIDT & HPCL.
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions
* are met:
*
* 1. Redistributions of source code must retain the above copyright
* notice, this list of conditions and the following disclaimer.
* 2. Redistributions in binary form must reproduce the above copyright
* notice, this list of conditions and the following disclaimer in the
* documentation and/or other materials provided with the
* distribution.
* 3. All advertising materials mentioning features or use of this software
* must display the following acknowledgement:
* This product includes software developed by the micROS Group. and
* its contributors.
* 4. Neither the name of the Group nor the names of its contributors may
* be used to endorse or promote products derived from this software
* without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY MICROS,GROUP AND CONTRIBUTORS
* ''AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT
* NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
* FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
* THE MICROS,GROUP OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
* OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
* HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
* OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
* IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
* ===================================================================
* Author: Shang Erke, Jiang Weizhong, Zhu Qi, micROS-DA Team, TAIIC.
*/

#ifndef DISPLAY_RESULT_H
#define DISPLAY_RESULT_H

#include <iostream>
#include <opencv2/opencv.hpp>

#include <ros/ros.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/TwistStamped.h>
#include <nav_msgs/OccupancyGrid.h>
#include <geometry_msgs/PoseArray.h>
#include <nav_msgs/Path.h>
#include <std_msgs/Float32.h>

#include "sirius_msgs/Lane.h"
#include "sirius_msgs/GuideTrajectoryL.h"

#define PI 3.141592657

/**
 * @brief Decide Display Results of Astar Avoid
 */
class DisplayResult 
{
  public:
    /**
     * @brief Construct a new Display Result object
     * 
     */
    DisplayResult();

    /**
     * @brief Subscribe topics from astar_avoid package
     */
    void subscribeRun();

    /**
     * @brief Display results of astar avoid package
     */
    void displayRun();

    /**
     * @brief Subscribe guide_waypoints message
     */
    void guideWaypointsCallback(const sirius_msgs::GuideTrajectoryL& guide_waypoints);
    
    /**
     * @brief Subscribe target point message in local
     */
    void goalPoseLocalCallback(const geometry_msgs::PoseStamped& goal_pose_local);
    
    /**
     * @brief Subscribe costmap message
     */
    void costmapCallback(const nav_msgs::OccupancyGrid& costmap);
    
    /**
     * @brief Subscribe safe_way message
     */
    void safeWayCallback(const sirius_msgs::GuideTrajectoryL& safe_way);
    
    /**
     * @brief Subscribe current pose message in local
     */
    void currentPoseLocalCallback(const geometry_msgs::PoseStamped& current_pose_local);
    
    /**
     * @brief Subscribe speed value message
     */
    void speedValueCallback(const std_msgs::Float32 &speed_value);
    
    /**
     * @brief Subscribe count value message
     */
    void countValueCallback(const std_msgs::Float32 &count_value);

    /**
     * @brief Convert axis angle to quaternion format
     */
    geometry_msgs::Quaternion direction(double theta_);
    
    /**
     * @brief calculate yaw angle of the car at target point
     */
    double goalPoseDir(double x1,double y1,double x2,double y2);
    
    /**
     * @brief Display results of astar avoid
     */
    void showResult();
    
    /**
     * @brief Set two edges of planned path
     */
    void roadEdgeBuild(sirius_msgs::GuideTrajectoryL guide_way,double threshod);

  private:

    sirius_msgs::GuideTrajectoryL guide_waypoints_;
    sirius_msgs::GuideTrajectoryL safe_way_;

    geometry_msgs::PoseStamped goal_pose_local_;
    geometry_msgs::PoseStamped current_pose_local_;

    nav_msgs::OccupancyGrid costmap_;

    double speed_value_;
    double count_value_;
    double threshod_value_;
    
    cv::Mat show_img_;

    //! bool
    bool received_costmap_;
    bool received_safe_way_;
    bool received_speed_value_;
    bool received_count_value_;
    bool received_guide_waypoints_;
    bool received_goal_pose_local_;
    bool received_current_pose_local_;
    
    //! ros
    ros::NodeHandle nh_;
    ros::Subscriber costmap_sub_;
    ros::Subscriber safe_way_sub_;
    ros::Subscriber speed_value_sub_;
    ros::Subscriber count_value_sub_;
    ros::Subscriber guide_waypoints_sub_;
    ros::Subscriber goal_pose_local_sub_;
    ros::Subscriber current_pose_local_sub_;
};

#endif // DISPLAY_RESULT_H
