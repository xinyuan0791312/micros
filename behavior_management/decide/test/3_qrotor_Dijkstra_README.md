﻿###    DESCRIPTION    ### 
  This is a global path planning test script for quadrotor plane.

  The details are as follows.

###  EXPECTED RESULTS ###
  All the computational results are shown in the terminal window. 
  The computational results include:
  1. The original map of barriers(once);
  2. The map of barriers and planned path(once);
  3. The details of the planned path(continuous).

###  OPERATING STEPS  ###
#****************************************************************#
***                        [RUN with micROS]                   ***

## Note ##
  Change the value of parameter "map_filepath_" in dijkstra_task.xml
to your own directory before running !!!


# Source the workspace
  source XXX/devel/setup.bash

# Run the daemon node:   
  roslaunch decide_dijkstra actor_daemon_qrotor_dijkstra.launch
    
# Run the g_station:
  roslaunch decide_dijkstra dijkstra_ground_station.launch
    
### Input description file of task:
                                   dijkstra_task.xml
    
### Input task name to start:
                                   DJ                          
#****************************************************************#


