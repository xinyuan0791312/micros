###    DESCRIPTION    ### 
  This is a global path planning test script for vehicle 
  to achieve real-time obstacle avoidance function.

###  EXPECTED RESULTS ###
  The computational results are shown in two windows entitled 
  map_astar and map_guide_path respectively. 
  1. Window map_astar:
     Blue path: global path planned.
     Green path: real-time path planned.
  2. Window map_guide_path:
     Blue path: 
     Green path: 
     Red path: 

###  OPERATING STEPS  ###
#****************************************************************#
***                        [RUN with micROS]                   ***

# Source the workspace
  source (workspace directory)/devel/setup.bash

# Run the daemon node:
  roslaunch decide_vehicle_astar_avoid actor_daemon_vehicle_astar_avoid.launch
    
# Run the g_station:
  roslaunch decide_vehicle_astar_avoid astar_avoid_ground_station.launch
    
## Input description file of task:
                                  astar_avoid_task.xml
    
## Input task name to start:
                                  ASTAR

# Run the visualization node:
  roslaunch display_result display_result_node.launch
    
    
# Display the rosbag:
  rosbag play (directory to src)/src/micros/behavior_management/decide/tools/display_result/2019-10-09-15-38-31.bag
  eg: 
    rosbag play /home/ljh/Desktop/micros_da/src/micros/behavior_management/decide/tools/display_result/2019-10-09-15-38-31.bag
#****************************************************************#






    
