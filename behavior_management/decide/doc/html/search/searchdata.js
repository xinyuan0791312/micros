var indexSectionsWithContent =
{
  0: "abcdefghijklmnopqrstuvwxyz~",
  1: "abcdfgijlmopsvw",
  2: "dfgmr",
  3: "abcdfgijmorstvw",
  4: "abcdfgijlmopqrstuvw~",
  5: "abcdefghijklmnopqrstuvwxyz",
  6: "bnu",
  7: "mpt"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "typedefs",
  7: "defines"
};

var indexSectionLabels =
{
  0: "全部",
  1: "类",
  2: "命名空间",
  3: "文件",
  4: "函数",
  5: "变量",
  6: "类型定义",
  7: "宏定义"
};

