/* ==================================================================
* Copyright (c) 2018, micROS Group, TAIIC, NIIDT & HPCL.
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions
* are met:
*
* 1. Redistributions of source code must retain the above copyright
* notice, this list of conditions and the following disclaimer.
* 2. Redistributions in binary form must reproduce the above copyright
* notice, this list of conditions and the following disclaimer in the
* documentation and/or other materials provided with the
* distribution.
* 3. All advertising materials mentioning features or use of this software
* must display the following acknowledgement:
* This product includes software developed by the micROS Group. and
* its contributors.
* 4. Neither the name of the Group nor the names of its contributors may
* be used to endorse or promote products derived from this software
* without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY MICROS,GROUP AND CONTRIBUTORS
* ''AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT
* NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
* FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
* THE MICROS,GROUP OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
* OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
* HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
* OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
* IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
* ===================================================================
* Author: Ren Xiaoguang, Wu Yunlong, Li Jinghua, micROS-DA Team, NIIDT, TAIIC.
*/

#ifndef DECIDE_CONDITION_NODE_H
#define DECIDE_CONDITION_NODE_H

#include <string>
#include <pluginlib/class_loader.h>

#include "message_types.h"
#include "condition_node.h"
#include "decide_softbus/decide_condition_interface.h"

/**
 * @brief Behavior Tree Namespace.
 */
namespace BT
{
  template <class T>
  /**
   * @brief This is a class for Decide Condition Node.
   */
  class DecideConditionNode : public ConditionNode
  {
    public:
    
      // Constructor
      explicit DecideConditionNode(std::string aName);
      ~DecideConditionNode();
  
      /** 
       * @brief Set Method
       * 
       * @param[in] aBaseClassPkgName demonstrates name of package of interface base class 
       * @param[in] anInterfaceClassName demonstrates name of interface base class 
       * @param[in] anInterfaceImplName demonstrates name of interface subclass(allgorithm) 
       */
      void setMethod(const std::string aBaseClassPkgName,
                     const std::string anInterfaceClassName,
                     const std::string anInterfaceImplName);
  
      /** 
       * @brief Set Buffer
       * 
       * @param[in] pBuffer demonstrates structure pointer containing input parameters 
       */
      void setBuffer(unified_msg::unified_message_t *pBuffer);

      /** @brief Set Param
       * 
       * @param[in] pParams demonstrates structure pointer containing parameters to be initialized
       */
      void setParam(unified_msg::unified_message_t *pParams);

      BT::ReturnStatus Tick();


    private:
      
      bool _booleanValue;
      bool _conditionReturnValue;
      bool _paramSet;

      pluginlib::ClassLoader<T> *_testLoader;
      boost::shared_ptr<T> _testCal;

      unified_msg::unified_message_t *_inputBuffer;
      unified_msg::unified_message_t *_inputParamBuffer;
};

  template <class T>
  DecideConditionNode<T>::DecideConditionNode(std::string name) : ConditionNode::ConditionNode(name)
  {
    type_ = BT::CONDITION_NODE;
    _paramSet = false;
  }

  template <class T>
  DecideConditionNode<T>::~DecideConditionNode() 
  {
    _testCal.reset();
    delete _testLoader;
    delete _inputBuffer;
    delete _inputParamBuffer;
  }

  template <class T>
  BT::ReturnStatus DecideConditionNode<T>::Tick()
  {
    if (get_status() == BT::EXIT)
    {
      // The behavior tree is going to be destroyed
      return BT::EXIT;
    }
    
    std::cout << " Condition to be judged! " << std::endl;

    if(_paramSet)
    {
      _testCal->initializeParam(_inputParamBuffer); 
      _paramSet = false;
    }
    _conditionReturnValue = _testCal->judgeCondition(_inputBuffer); 

    if (_conditionReturnValue)
    {
      set_status(BT::SUCCESS);
      std::cout << get_name() << " returning Success" << BT::SUCCESS << "!" << std::endl;
      return BT::SUCCESS;
    }
    else
    {
      set_status(BT::FAILURE);
      std::cout << get_name() << " returning Failure" << BT::FAILURE << "!" << std::endl;
      return BT::FAILURE;
    }

  }

  template <class T>
  void DecideConditionNode<T>::setMethod(const std::string aBaseClassPkgName,
                                         const std::string anInterfaceClassName,
                                         const std::string anInterfaceImplName)
  {
    _testLoader = new pluginlib::ClassLoader<T>(aBaseClassPkgName, anInterfaceClassName);
    _testCal = _testLoader->createInstance(anInterfaceImplName);
  }

  template <class T>
  void DecideConditionNode<T>::setBuffer(unified_msg::unified_message_t *pBuffer)
  {
    _inputBuffer = pBuffer;
  }

  template <class T>
  void DecideConditionNode<T>::setParam(unified_msg::unified_message_t *pParams)
  {
    _inputParamBuffer = pParams;
    _paramSet = true;
  }

}  // namespace BT

#endif  // CONDITIONS_CONDITION_TEST_BATTERY_NODE_H
