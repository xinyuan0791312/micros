/* ==================================================================
* Copyright (c) 2018, micROS Group, TAIIC, NIIDT & HPCL.
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions
* are met:
*
* 1. Redistributions of source code must retain the above copyright
* notice, this list of conditions and the following disclaimer.
* 2. Redistributions in binary form must reproduce the above copyright
* notice, this list of conditions and the following disclaimer in the
* documentation and/or other materials provided with the
* distribution.
* 3. All advertising materials mentioning features or use of this software
* must display the following acknowledgement:
* This product includes software developed by the micROS Group. and
* its contributors.
* 4. Neither the name of the Group nor the names of its contributors may
* be used to endorse or promote products derived from this software
* without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY MICROS,GROUP AND CONTRIBUTORS
* ''AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT
* NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
* FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
* THE MICROS,GROUP OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
* OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
* HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
* OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
* IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
* ===================================================================
* Author: Ren Xiaoguang, Wu Yunlong, Li Jinghua, micROS-DA Team, NIIDT, TAIIC.
*/

#ifndef DECIDE_SOFTBUS_H
#define DECIDE_SOFTBUS_H

// C and C++ headers
#include <map>
#include <string>

// ROS headers
#include <ros/ros.h>
#include <pluginlib/class_loader.h>

// Added in new version
#include "behavior_tree.h"
#include "decide_softbus/decide_action_node.h"
#include "decide_softbus/decide_condition_node.h"
#include "decide_softbus/decide_action_interface.h"
#include "decide_softbus/decide_condition_interface.h"

/**
 * @brief Decide Softbus Template Namespace.
 */
namespace decide_softbus
{
  /**
   * @brief This is a class for Decide Softbus Template Base.
   */
  class DecideSoftbus
  {
    public:

      /**
       * @brief Construct a new DecideSoftbus object
       */
      DecideSoftbus();

      /**
       * @brief Destroy the DecideSoftbus object
       */
      ~DecideSoftbus();

      /**
       * @brief Interface for initializing NodeHandle, Subscriber, Publisher, etc.  
       */
      virtual void initArgs() = 0;

      /**
       * @brief .  
       */
      virtual unified_msg::unified_message_t * addParamBuffer();
      
      /**
       * @brief .  
       */
      virtual unified_msg::unified_message_t * addConditionBuffer();
      
            
      unified_msg::unified_message_t *_inputGlobalBuffer;
      unified_msg::unified_message_t *_outputGlobalBuffer;
      std::vector<unified_msg::unified_message_t* > _conditionBuffer;
      std::vector<unified_msg::unified_message_t* > _paramBuffer;

      BT::SequenceNode* _root; 

    private:
        
  };
}

#endif