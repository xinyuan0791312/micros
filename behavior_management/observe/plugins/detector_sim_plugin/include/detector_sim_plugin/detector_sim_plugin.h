/*=====================================================================================
* Copyright (c) 2019, micROS Group, NIIDT, TAIIC.
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without modification, are permitted
*  provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice, this list of conditions and
*      the following disclaimer.
* 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions
*      and the following disclaimer in the documentation and/or other materials provided with the
*      distribution.
* 3. All advertising materials mentioning features or use of this software must display the following
*      acknowledgement: This product includes software developed by the micROS Group and its
*      contributors.
* 4. Neither the name of the Group nor the names of contributors may be used to endorse or promote
*     products derived from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY MICROS GROUP AND CONTRIBUTORS ''AS IS''AND ANY EXPRESS OR
* IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR  PURPOSE ARE DISCLAIMED. IN NO EVENT
* SHALL THE MICROS, GROUP OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL,  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
*  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
* BUSINESS INTERRUPTION). HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
* WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
=====================================================================================
* Author: Naiyang Guan, Longfei Su.
*/



#ifndef __OBSERVE_PLUGIN__
#define __OBSERVE_PLUGIN__


/*#include <opencv2/opencv.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/core/utility.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/tracking.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/highgui.hpp>

#include <opencv2/dnn.hpp>
#include <opencv2/dnn/shape_utils.hpp>*/
#include <nav_msgs/Odometry.h>

#include <ros/ros.h>
#include <geodesy/utm.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/NavSatFix.h>
#include <sensor_msgs/CameraInfo.h>
//#include "observe_msgs/MovObject.h"
//#include "observe_msgs/VisibleImage.h"
//#include "observe_msgs/Gnss.h"
//#include "observe_msgs/GetImages.h"
//#include "observe_msgs/TotalMovObject.h"
//#include "observe_msgs/Velocity.h"
#include "observe_msgs/SingleObjectDetectionInfo.h"
#include "observe_msgs/ObjectDetectionInfo.h"
#include "observe_msgs/Location.h"
#include "observe_msgs/EvidenceConfirm.h"

#include <vector>
#include <iostream>
#include <stdio.h>
#include <cstring>
#include <stdint.h>
#include <fstream>
#include <string>
#include <time.h>
#include <thread>
#include <algorithm>
#include <ros/callback_queue.h>
//#include <nav_msgs/Odometry.h>
//#include <Eigen/Dense>
#include <image_transport/image_transport.h>

#include "general_plugin/general_plugin.h"
//#include "cal_assistant/coordConvert.h"
#include "rosplane_msgs/State.h"

using std::vector;
using std::string;
//using cv::Mat;
//using cv::MatND;
//using cv::Rect;
//using cv::Rect2d;
//using cv::Point;
//using cv::Scalar;

namespace general_bus
{
  class DetectorSimPlugin: public GeneralPlugin
  {
  public:
    DetectorSimPlugin();
    ~DetectorSimPlugin();

    virtual void start();
    //void grabImageCallback(const sensor_msgs::Image::ConstPtr& img_msg);
    //void callbacklater1(const sensor_msgs::CameraInfo::ConstPtr & msg1);
    //void callbacklater2(const rosplane_msgs::State::ConstPtr & msg2);
    
    void confirmCallback(const observe_msgs::EvidenceConfirm::ConstPtr & msg);
    //void callbacklater3(const nav_msgs::Odometry & msg3)
    //ros::NodeHandle _nodeHandler;
    //void PixelToCamera( double K[3][3],double R[3][3],double height,double xu,double yu, double& xb,double& yb,double& zb);
    //static bool biggerSort(vector<Point> v1, vector<Point> v2);


   void goalCallback(const nav_msgs::Odometry::ConstPtr &msg);
   void stateCallback(const rosplane_msgs::State::ConstPtr &msg);

   void publishUpdate();

  private:
    // Publishers and subscribers
    ros::Publisher _visibleImagePublisher;
    //ros::Publisher total_objects_publisher;
    ros::Publisher _locationPublisher;
    ros::Publisher _objectPublisher;

    ros::Subscriber _imageSubscriber;
    ros::Publisher _velocityPublisher;
    //ros::NodeHandle _nh1;
    //ros::Subscriber _sub1, _sub2;  //坐标变换需要订阅信息
    ros::Subscriber _subConfirm;   //判断组确认信息

    ros::Subscriber _stateSub;   // subscriber for UAV state
    ros::Subscriber _targetSub;  // subscriber for target position



    observe_msgs::Location _direction; 

    // Tracking related members
    //Rect2d ssd_bbox;
    int _frameNum;
    //Mat _currentFrame;
    //Mat hsv;                          //HSV颜色空间，在色调H上跟踪目标（camshift是基于颜色直方图的算法）
    //MatND hist;                       //直方图数组
    //float _centX;
    //float _centY;
    //float _ratioX;
    //float _ratioY;
    //Rect rt;                          //目标外接框、生成结构元素（用于连接断开的小目标）
    //int _no_target_num;
    //double _K[3][3];                       //相机标定参数K
    //double _R[3][3];                       //相机标定参数R
    double _theta, _phi, _psi;
    double _flyX = 200, _flyY = 0, _height = 50;          //无人机和相机高度
    double _xb, _yb, _zb;
    double _xo,_yo,_bo;                      //
    bool _uavReceieved, _targetReceieved;
    string _recievedClassname;

    rosplane_msgs::State _uavState;
    nav_msgs::Odometry _goalState;

  };
}

#endif
