#ifndef LOCALIZATION_BASE_H
#define LOCALIZATION_BASE_H

#include <Eigen/Core>
#include "geometry_msgs/PoseWithCovarianceStamped.h"

namespace micros{

/**
 * @brief The observe base class, originally the  Localization_Base class
 * Localization plugins should inherit this base class, and implement pluginlib-required methods
 */
class observe_Base
{
public:
  observe_Base() {}
  virtual ~observe_Base() {}

  /**
   * @brief activate Activate a localization plugin
   */
  virtual void activate() {}
  /**
   * @brief pause Pause a localization plugin
   */
  virtual void pause() {}
  /**
   * @brief go on Resume a localization plugin
   */
  virtual void resume() {}
  /**
   * @brief reset The localization plugin should be able reset itself based on an
   * initial pose, upon requests from the Decision softbus
   */
  virtual void reset() {}
  /**
   * @brief getLocalization Plugins should provide access to localization result
   */
  virtual void getLocalization() {}
  /**
   * @brief saveLocalization Method to save localization result to filesystem/database
   */
  virtual void saveLocalization() {}
  /**
   * @brief publishLocalization Publish localization result via micROS topic
   */
  virtual void publishLocalization() {}
  /**
   * @brief setInitialPose Set initial pose before the plugin is activated
   * @param trans Translation
   * @param rot Rotation matrix
   */
  virtual void setInitialPose(const geometry_msgs::PoseWithCovarianceStampedConstPtr& pose) {}
  virtual void setInitialPose(Eigen::Vector3d trans, Eigen::Matrix3d rot) {}

private:
//  Eigen::Vector3d translation3d_;
//  Eigen::Matrix3d rotation3d_;
//  Eigen::Vector2d translation2d_;
//  Eigen::Matrix2d rotation2d_;
//  std::string localizationTopicName_;
};
} // end namespace
#endif // LOCALIZATION_BASE_H
