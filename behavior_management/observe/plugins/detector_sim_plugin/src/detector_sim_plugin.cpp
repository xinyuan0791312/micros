/*=====================================================================================
* Copyright (c) 2019, micROS Group, NIIDT, TAIIC.
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without modification, are permitted
*  provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice, this list of conditions and
*      the following disclaimer.
* 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions
*      and the following disclaimer in the documentation and/or other materials provided with the
*      distribution.
* 3. All advertising materials mentioning features or use of this software must display the following
*      acknowledgement: This product includes software developed by the micROS Group and its
*      contributors.
* 4. Neither the name of the Group nor the names of contributors may be used to endorse or promote
*     products derived from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY MICROS GROUP AND CONTRIBUTORS ''AS IS''AND ANY EXPRESS OR
* IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR  PURPOSE ARE DISCLAIMED. IN NO EVENT
* SHALL THE MICROS, GROUP OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL,  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
*  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
* BUSINESS INTERRUPTION). HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
* WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
=====================================================================================
* Author: Naiyang Guan, Longfei Su.
*/

#include "detector_sim_plugin/detector_sim_plugin.h"
#include <pluginlib/class_list_macros.h>


PLUGINLIB_EXPORT_CLASS(general_bus::DetectorSimPlugin,general_bus::GeneralPlugin)

namespace general_bus
{
    void DetectorSimPlugin::start()
    {
	std::cout<<"[observe plugin]: 11111111111111111111"<<std::endl;

        _targetReceieved = false;
        _uavReceieved = false;
        ros::Time time_now = ros::Time::now();
        ROS_INFO("Object Detector is running at %f.!!!!!!!!!!!!!!!!!!!!!!!!!!!!!",time_now.toSec());
	// TODO: Salomon
        _targetSub = pluginSubscribe("/enemy/ground_truth/state", 100, &DetectorSimPlugin::goalCallback, this);
        _stateSub = pluginSubscribe("/fixedwing/truth", 100, &DetectorSimPlugin::stateCallback, this);
        _locationPublisher = pluginAdvertise<observe_msgs::Location>("/enemy_location", 1000);
        _objectPublisher = pluginAdvertise<observe_msgs::ObjectDetectionInfo>("orient_softbus_msgs/evidenceFromObserve", 1000);
        _subConfirm = pluginSubscribe("orient_soft_msgs/evidenceConfirmToObserve", 1000, &DetectorSimPlugin::confirmCallback, this);
       //image_subscriber = pluginSubscribe<sensor_msgs::Image>("/downward_cam/camera/image", 1000, &DetectorSimPlugin::grabImageCallback, this);
        std::string tempStr;
        if(getActorName(_actorID,tempStr))
        {
            //ROS_INFO("ACTOR ID %lld, actorName %s",_actorID,tempStr.c_str());
        }
        else
        {
            ROS_ERROR("CANNOT GET ACTORNAME");
        }
         while (true)
        {
            GOON_OR_RETURN;
            publishUpdate();
            usleep(20000);
        }
    }

    DetectorSimPlugin::DetectorSimPlugin()
    {
        //frame_num = 0;
    }

    DetectorSimPlugin::~DetectorSimPlugin()
    {
    }


void DetectorSimPlugin::stateCallback(const rosplane_msgs::State::ConstPtr &msg)
{
        _uavState = *msg;
        _uavReceieved = true;
}

void DetectorSimPlugin::goalCallback(const nav_msgs::Odometry::ConstPtr &msg)
{
        _goalState = *msg;
        _targetReceieved = true;
}


void DetectorSimPlugin::publishUpdate()
{
        observe_msgs::SingleObjectDetectionInfo singleobjectinfo;
        observe_msgs::ObjectDetectionInfo objectinfo;

        singleobjectinfo.labelName = "car";
        singleobjectinfo.confidence = 1;
        //singleobjectinfo.leftTopX = 0;
        //singleobjectinfo.leftTopY = 0;
        //singleobjectinfo.rightBottomX = 0;
        //singleobjectinfo.rightBottomY = 0;
        singleobjectinfo.related_evidence = "NAN";
        ROS_INFO("00000000000000000000000000000000000000000000000000000000000");

        if (_targetReceieved && _uavReceieved)
        {
            float x_ = _uavState.position[0] - _goalState.pose.pose.position.x;
            float y_ = _uavState.position[1] - (-_goalState.pose.pose.position.y);
            float r = sqrt(x_ * x_ + y_ * y_);
            ROS_INFO("11111111111111111111111111111111111111111111111111111111111");
            if (r < fabs(_uavState.position[2]) * 100.73)
            {
               objectinfo.detectionInfo.push_back(singleobjectinfo);
               /*cv_bridge::CvImage cvi;
               cvi.header.stamp = ros::Time::now();
               cvi.header.frame_id = "image";
               cvi.encoding = "bgr8";
               current_frame.copyTo(cvi.image);
               cvi.toImageMsg(objectinfo.image);*/
               //cv::imencode(".jpg", current_frame, objectinfo.image);
               _objectPublisher.publish(objectinfo);
               ROS_INFO("222222222222222222222222222222222222222222222222222222222222"); 
            }
        }
        
        if(_recievedClassname == "car")
        {
            ROS_INFO("Observe is Sending to DA????????????????????????????????????");
            //location_publisher.publish(direction);
        }

}

 void  DetectorSimPlugin::confirmCallback(const observe_msgs::EvidenceConfirm::ConstPtr& msg)
 {
       ROS_INFO("Accepted !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
       _recievedClassname = msg->category;
 }

/*void DetectorSimPlugin::grabImageCallback(const sensor_msgs::Image::ConstPtr& img_msg)
{
	ROS_INFO("Video capture!!!!!!!!!!!!!!!!!!!!!!!!");
        sensor_msgs::Image msg = *img_msg;	

        cv_bridge::CvImageConstPtr cv_ptr;
        try
        {
            cv_ptr = cv_bridge::toCvCopy(msg,"bgr8");
        }
        catch (cv_bridge::Exception& expt)
        {
            ROS_ERROR("cv_bridge exception: %s", expt.what());
            return;
        }

        cv_ptr->image.copyTo(_currentFrame);
        
        return;
}*/
    
}
