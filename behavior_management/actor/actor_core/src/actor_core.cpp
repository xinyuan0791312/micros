/* ==================================================================
*
* Copyright (c) 2016-2019, micROS Group, NIIDT, TAIIC, HPCL.
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions 
* are met: 
*
* 1. Redistributions of source code must retain the above copyright 
*     notice, this list of conditions and the following disclaimer. 
* 2. Redistributions in binary form must reproduce the above copyright 
*     notice, this list of conditions and the following disclaimer in the 
*     documentation and/or other materials provided with the 
*     distribution. 
* 3. All advertising materials mentioning features or use of this software 
*     must display the following acknowledgement: 
*     This product includes software developed by the micROS Group. and 
*     its contributors. 
* 4. Neither the name of the Group nor the names of its contributors may 
*     be used to endorse or promote products derived from this software 
*     without specific prior written permission. 
*
* THIS SOFTWARE IS PROVIDED BY MICROS,GROUP AND CONTRIBUTORS
* ''AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT
* NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
* FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL 
* THE MICROS,GROUP OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS 
* OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) 
* HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
* OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN 
* IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
* ===================================================================
*
* Author: Bin Di, Chunlian Fu, Junling Gao, Ruihao Li, Wei Yi.
*         (in alphabetical order)
*
*/

#include "actor_core/ACB.h"
#include "actor_core/actor_types.h"
#include <tinyxml.h>
#include <stdlib.h>
#include "ros/ros.h"
//#include "actor_core/actor_core.h"


void getPlatformResources(std::string xmlContent,std::vector<SensorActuatorInfo>& platformSensors, 
                       std::vector<SensorActuatorInfo>& platformActuators) {

    platformSensors.clear();
    platformActuators.clear();
    //todo replace the function by interpreting the XML file
    //fake a resource 
    ROS_WARN("local resources reading incompleted");
    SensorActuatorInfo sensorInfo1;
    sensorInfo1.id=112;
	sensorInfo1.type="visible";
	sensorInfo1.properties.clear();
    sensorInfo1.params.clear();
    sensorInfo1.params["resolution_x"]="1024";
    sensorInfo1.params["resolution_y"]="768";
    SensorActuatorInfo sensorInfo2;
    sensorInfo2.id=121;
	sensorInfo2.type="distancer";
	sensorInfo2.properties.clear();
    sensorInfo2.params.clear();
    sensorInfo2.properties["resolution"]="0.5";
    platformSensors.push_back(sensorInfo2);
    SensorActuatorInfo actuatorInfo1;
    actuatorInfo1.id=201;
	actuatorInfo1.type="motor";
	actuatorInfo1.properties.clear();
    actuatorInfo1.params.clear();
    platformActuators.push_back(actuatorInfo1);  
}


bool createACB(TaskInfo* pTask,const ResMatchResult* pMatchResult,int64_t actorID,ACB* pACB) {
	
	//pACB->id=???
	pACB->name=pTask->actorName;
	pACB->taskID=pTask->taskID;
	//pACB->pTaskInfo=pTask;
	
	//resources
	// std::vector<SensorActuatorInfo> sensors;
	// std::vector<SensorActuatorInfo> actuators;
	// PlatformInfo* pPlatformInfo;
	// SwarmInfo* pSwarmInfo;
	return true;
}
