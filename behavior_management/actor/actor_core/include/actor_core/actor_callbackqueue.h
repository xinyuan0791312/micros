/* ==================================================================
*
* Copyright (c) 2016-2019, micROS Group, NIIDT, TAIIC, HPCL.
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions 
* are met: 
*
* 1. Redistributions of source code must retain the above copyright 
*     notice, this list of conditions and the following disclaimer. 
* 2. Redistributions in binary form must reproduce the above copyright 
*     notice, this list of conditions and the following disclaimer in the 
*     documentation and/or other materials provided with the 
*     distribution. 
* 3. All advertising materials mentioning features or use of this software 
*     must display the following acknowledgement: 
*     This product includes software developed by the micROS Group. and 
*     its contributors. 
* 4. Neither the name of the Group nor the names of its contributors may 
*     be used to endorse or promote products derived from this software 
*     without specific prior written permission. 
*
* THIS SOFTWARE IS PROVIDED BY MICROS,GROUP AND CONTRIBUTORS
* ''AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT
* NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
* FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL 
* THE MICROS,GROUP OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS 
* OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) 
* HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
* OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN 
* IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
* ===================================================================
*
* Author: Bin Di, Chunlian Fu, Junling Gao, Ruihao Li, Wei Yi.
*         (in alphabetical order)
*
*/

#ifndef _ACTOR_CALLBACK_QUEUE_H_
#define _ACTOR_CALLBACK_QUEUE_H_
#include <ros/callback_queue.h>
#include <ros/subscription_queue.h>
#include <ros/spinner.h>
#include <string>

// ActorCallbackQueue implement from CallbackQueue,
// this is used for clear callbackQueue when restart the subscriber thread
class ActorCallbackQueue : public ros::CallbackQueue{
    public:
    void clearQueue(){
        while(1){
            uint64_t removalID = -1;
            {
                boost::mutex::scoped_lock lock(mutex_);
                if(callbacks_.empty())break;
                removalID = callbacks_.begin()->removal_id;
                boost::shared_ptr<ros::SubscriptionQueue> qPtr = boost::dynamic_pointer_cast<ros::SubscriptionQueue>(callbacks_.begin()->callback);
                qPtr->clear();
            }
            removeByID(removalID);
        }
    }
};

// every Actor related to one ActorCallbackQueueInfo
// this is used to stop subscribe msgs when pause actor and start subscribe msgs when start actor
class ActorCallbackQueueInfo{
public:
    std::string _actorName;
    boost::shared_ptr<ActorCallbackQueue> _callbackQueue;
    boost::shared_ptr<ros::AsyncSpinner> _asyncSpinner;
    ActorCallbackQueueInfo(std::string actorName):_actorName(actorName){
        _callbackQueue.reset(new ActorCallbackQueue());
        _asyncSpinner.reset(new ros::AsyncSpinner(1, _callbackQueue.get()));
    }
    ~ActorCallbackQueueInfo(){
        _asyncSpinner.reset(); // _asyncSpinner will be auto stoped 
        _callbackQueue.reset();  
    }

    // call this before stop thread
    void stopCallbackQueue(){
        _asyncSpinner->stop();
    }

    // call this before start thread
    void startCallbackQueue(){
        _callbackQueue->clearQueue();
        if(_asyncSpinner->canStart())
            _asyncSpinner->start(); 
    }
};
#endif

