/* ==================================================================
*
* Copyright (c) 2016-2019, micROS Group, NIIDT, TAIIC, HPCL.
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions 
* are met: 
*
* 1. Redistributions of source code must retain the above copyright 
*     notice, this list of conditions and the following disclaimer. 
* 2. Redistributions in binary form must reproduce the above copyright 
*     notice, this list of conditions and the following disclaimer in the 
*     documentation and/or other materials provided with the 
*     distribution. 
* 3. All advertising materials mentioning features or use of this software 
*     must display the following acknowledgement: 
*     This product includes software developed by the micROS Group. and 
*     its contributors. 
* 4. Neither the name of the Group nor the names of its contributors may 
*     be used to endorse or promote products derived from this software 
*     without specific prior written permission. 
*
* THIS SOFTWARE IS PROVIDED BY MICROS,GROUP AND CONTRIBUTORS
* ''AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT
* NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
* FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL 
* THE MICROS,GROUP OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS 
* OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) 
* HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
* OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN 
* IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
* ===================================================================
*
* Author: Bin Di, Chunlian Fu, Junling Gao, Ruihao Li, Wei Yi.
*         (in alphabetical order)
*
*/

#ifndef __ACTOR_TYPES__
#define __ACTOR_TYPES__
#include<string>
#include<stdint.h>
#include<map>
#include<vector>
#include<string>

class SensorActuatorInfo {
public:
	int64_t id;
	std::string type;
	//static properties of the sensor
	std::map<std::string,std::string> properties;
	//configurable parameters of the sensor
	std::map<std::string,std::string> params;
};

class ResMatchResult {
public:
	//key to resource idx
	std::map<std::string,SensorActuatorInfo> keyToRes;
};

class PluginInfo {
public:
	std::string busName;
	std::string name;
	PluginInfo(const std::string aName,const std::string aBusName):busName(aBusName),name(aName) {}
};

class TaskInfo {
public:
	int64_t taskID;
	std::string actorName;
	std::string taskXMLStr;
	std::vector<PluginInfo> plugins;
};

class SwarmInfo {
public:
	std::string swarmName;
	int64_t swarmID;
	//DB 180601
	int32_t swarmPrio;
	//state
	int16_t state;
	//local platform name
	std::string platformName;
	//actors in the swarm
	std::vector<std::string> generalActors;
	std::map<std::string,std::vector<std::string> > excluActors;
	std::vector<std::string> dynamicActors; 	// [DMY] 20180711
	//platform-->actors in the swarm
	std::map<std::string,std::vector<std::string> > platformActors;

};

class PlatformInfo {
public:
	int64_t id;
	std::string name;
	std::string type;
	std::map<std::string,std::string> properties;	
};



#endif

