/* ==================================================================
*
* Copyright (c) 2016-2019, micROS Group, NIIDT, TAIIC, HPCL.
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions 
* are met: 
*
* 1. Redistributions of source code must retain the above copyright 
*     notice, this list of conditions and the following disclaimer. 
* 2. Redistributions in binary form must reproduce the above copyright 
*     notice, this list of conditions and the following disclaimer in the 
*     documentation and/or other materials provided with the 
*     distribution. 
* 3. All advertising materials mentioning features or use of this software 
*     must display the following acknowledgement: 
*     This product includes software developed by the micROS Group. and 
*     its contributors. 
* 4. Neither the name of the Group nor the names of its contributors may 
*     be used to endorse or promote products derived from this software 
*     without specific prior written permission. 
*
* THIS SOFTWARE IS PROVIDED BY MICROS,GROUP AND CONTRIBUTORS
* ''AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT
* NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
* FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL 
* THE MICROS,GROUP OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS 
* OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) 
* HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
* OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN 
* IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
* ===================================================================
*
* Author: Bin Di, Chunlian Fu, Junling Gao, Ruihao Li, Wei Yi.
*         (in alphabetical order)
*
*/



#ifndef __ACTOR_API__
#define __ACTOR_API__


#include <string>
#include "boost/shared_ptr.hpp"
#include "actor_core/barrier.h"
//#include "actor_msgs/UTOEvent.h"

// extern int SHARED_DATA_SIZE;

/*class declration*/
// class ActorScheduler;
// class SwarmMasterCmd;
namespace general_bus {
    class GeneralPlugin;
};

/*global variable and functions*/
//extern ActorScheduler *gPActorScheduler;
//extern SwarmMasterCmd *gSwarmMasterCmd;

/**
 * @brief Get the Actor Name by actor ID. 
 * 
 * @param[in]  anActorID    Actor ID
 * @param[out] anActorName  Actor name
 * @return true  Success
 * @return false Failure
 */
extern bool getActorName(int64_t anActorID, std::string &anActorName);

/**
 * @brief Get shared_ptr of the plugin by actor ID.
 * 
 * @param[in] anActorID   Actor ID
 * @param[in] aPluginName Plugin name
 * @param[out] aPluginStr Shared_ptr of the plugin
 * @return true  Success
 * @return false Failure
 */
extern bool getActorPlugin(int64_t anActorID, std::string aPluginName, boost::shared_ptr<general_bus::GeneralPlugin> &aPluginStr);

/**
 * @brief Get the Actor Pause Count
 * 
 * @param anActorID Actor ID
 * @return int      pause count
 */
extern int getActorPauseCount(int64_t anActorID);

/**
 * @brief Robot barrier api, for all robot step into next state in the same time
 * 
 * @param[in] aBarrierKey  Global barrier key
 * @param[in] aWaitCount   Number of synchronization plugins
 * @param[in] aTimeout     Waiting seconds 
 * @return BarrierResult 
 */ 
extern BarrierResult pluginBarrierApi(GlobalBarrierKey& aBarrierKey, int aWaitCount, const short aTimeout);

/**
 * @brief data distribute api
 * 
 * @param aBarrierKey   barrierKey, should be unique
 * @param anActorName   the actor which call this method, diffirent actor name means diffirent request client.
 * @param aWaitCount    when the number of robots reached to the barrier >= aWaitCount, the caller will go on. 
 * @param aTimeout      seconds
 * @param[out] anIndex  data index, start with 1, avaliable when BarrierResult is YES.
 * @return BarrierResult 
 */
extern BarrierResult actorDataDistributeBarrierApi(GlobalBarrierKey& aBarrierKey, std::string anActorName, int aWaitCount, const short aTimeout, int &anIndex);

/**
 * @brief Get the Shared Data from shared space amongst actors
 * 
 * @param[out] aPData  Object used to store shared data
 * @param[int] aSize   Size of data you want to acquire
 * @return int  Size of data acquired
 */
extern int getSharedData(char* aPData, int aSize);

/**
 * @brief Set the Shared Data to shared space amongst actors
 * 
 * @param[in] aPData  Source data
 * @param[in] aSize   Source data size
 * @return int  Size of data stored
 */
extern int setSharedData(char* aPData, int aSize);

/**
 * @attention Deprecated
 * @brief Switch one actor to another
 * 
 * @param[in] anActorID   Source actor ID
 * @param[in] anActorName Target actor name
 */
extern void switchToActor(int64_t anActorID, std::string anActorName);

/**
 * @attention Deprecated
 * @brief Activate an actor
 *  
 * @param[in] anActorName The actor name
 */
extern void activateActor(std::string anActorName);

/**
 * @attention Deprecated
 * @brief Pause an actor
 * 
 * @param[in] anActorID The actor ID
 */
extern void pauseActorApi(int64_t anActorID);

/**
 * @attention Deprecated
 * @brief Quit an actor
 * 
 * @param[in] anActorID The actor ID
 */
extern void quitActor(int64_t anActorID);

/**
 * @attention Deprecated
 * @brief Get the Formation of current platfrom
 * 
 * @param[in] anActorID       The actor ID
 * @param[out] aFormationType Formation type of current platform
 * @param[out] aFormationPos  Formation position of current platform
 * @return true   Success
 * @return false  Failure
 */
extern bool getFormation(int64_t anActorID, std::string &aFormationType, std::string &aFormationPos);

/**
 * @attention Deprecated
 * @brief Request master arbitration for activating an actor
 * 
 * @param[in] aSendActorName Applicant's actor name
 * @param[in] anActorName        Target actor name
 */
extern void requestActivateActorFromMaster(const std::string& aSendActorName,const std::string& anActorName);

/**
 * @attention Deprecated
 * @brief Request master arbitration for switching one actor to another
 * 
 * @param[in] aSendActorName Applicant's actor name
 * @param[in] anActorName    Target actor name
 */
extern void requestSwitchActorFromMaster(const std::string& aSendActorName,const std::string& anActorName);

//TODO temp add formation content for DA
extern short getFormationPos(const int aRobotID = -1);

#endif