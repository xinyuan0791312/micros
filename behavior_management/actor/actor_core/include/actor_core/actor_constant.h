/* ==================================================================
*
* Copyright (c) 2016-2019, micROS Group, NIIDT, TAIIC, HPCL.
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions 
* are met: 
*
* 1. Redistributions of source code must retain the above copyright 
*     notice, this list of conditions and the following disclaimer. 
* 2. Redistributions in binary form must reproduce the above copyright 
*     notice, this list of conditions and the following disclaimer in the 
*     documentation and/or other materials provided with the 
*     distribution. 
* 3. All advertising materials mentioning features or use of this software 
*     must display the following acknowledgement: 
*     This product includes software developed by the micROS Group. and 
*     its contributors. 
* 4. Neither the name of the Group nor the names of its contributors may 
*     be used to endorse or promote products derived from this software 
*     without specific prior written permission. 
*
* THIS SOFTWARE IS PROVIDED BY MICROS,GROUP AND CONTRIBUTORS
* ''AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT
* NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
* FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL 
* THE MICROS,GROUP OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS 
* OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) 
* HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
* OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN 
* IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
* ===================================================================
*
* Author: Bin Di, Chunlian Fu, Junling Gao, Ruihao Li, Wei Yi.
*         (in alphabetical order)
*
*/

#ifndef __ACB_CONSTANT__
#define __ACB_CONSTANT__


/**
 * @brief Auto run task and execute actor switch
 */
// #define AUTO_RUN_TASK


const int MAX_PRIORITY=32768;

const int ACTOR_STATE_INIT=0;
const int ACTOR_STATE_RUNNING=1;
const int ACTOR_STATE_PAUSE=2;
const int ACTOR_STATE_READY=3;
const int ACTOR_STATE_FINISH=4;
const int ACTOR_STATE_INIT_DELAY=5;

/**
 * @brief Event and msg type for communication in state machine
 */
enum EEventType {
    SWITCH_EVENT_MSG = 2,  ///< Original transition event to switch actors
    SWITCH_EVENT_MSG_GS_SYNED = 3,  ///< Synchronized transition event from gstation and it is ready to be handled
    START_TASK_MSG = 4,  ///< Msg to start the initial actors for a new task
    ACTIVATE_ACTOR_MSG = 5,  ///< Msg to to active an actor, e.g., used in master
    PAUSE_ACTOR_MSG = 6,  ///< Msg to pause an actor, e.g., used in master
    SWITCH_ACTOR_MSG = 7  ///< Msg to switch an actor, e.g., used in master
};

#endif