/* ==================================================================
*
* Copyright (c) 2016-2019, micROS Group, NIIDT, TAIIC, HPCL.
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions 
* are met: 
*
* 1. Redistributions of source code must retain the above copyright 
*     notice, this list of conditions and the following disclaimer. 
* 2. Redistributions in binary form must reproduce the above copyright 
*     notice, this list of conditions and the following disclaimer in the 
*     documentation and/or other materials provided with the 
*     distribution. 
* 3. All advertising materials mentioning features or use of this software 
*     must display the following acknowledgement: 
*     This product includes software developed by the micROS Group. and 
*     its contributors. 
* 4. Neither the name of the Group nor the names of its contributors may 
*     be used to endorse or promote products derived from this software 
*     without specific prior written permission. 
*
* THIS SOFTWARE IS PROVIDED BY MICROS,GROUP AND CONTRIBUTORS
* ''AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT
* NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
* FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL 
* THE MICROS,GROUP OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS 
* OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) 
* HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
* OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN 
* IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
* ===================================================================
*
* Author: Bin Di, Chunlian Fu, Junling Gao, Ruihao Li, Wei Yi.
*         (in alphabetical order)
*
*/

#ifndef __BARRIER_H__
#define __BARRIER_H__
#include <string>

enum BarrierResult{NO=0, YES, OPTIONALYES, TIMEOUT};

struct GlobalBarrierKey{
    private:
    std::string _swarmName; 
    std::string _actorName;
    std::string _pluginName ;   // this param is invalid in actor barrier process
    short _barrierKey;          // unique key
    public:
    short getBarrierKey(){return _barrierKey;}
    std::string getPluginName(){return _pluginName;}
    std::string getActorName(){return _actorName;}
    std::string getSwarmName(){return _swarmName;}
    void setSwarmName(const std::string& aSwarmName){ _swarmName = aSwarmName;}
    // only _barrierKey is avalible
    GlobalBarrierKey(const short aKey)
    :_swarmName(""), _actorName(""), _pluginName(""), _barrierKey(aKey){}

     // only _pluginName and _barrierKey is avalible
    GlobalBarrierKey(const std::string& aPluginName, const short aKey)
        :_swarmName(""), _actorName(""), _pluginName(aPluginName), _barrierKey(aKey){}

    // only _actorName _pluginName and _barrierKey is avalible
    GlobalBarrierKey(const std::string& anActorName, const std::string& aPluginName, const short aKey)
        :_swarmName(""), _actorName(anActorName), _pluginName(aPluginName), _barrierKey(aKey){}

    // all params is avalible
    GlobalBarrierKey(const std::string& aSwarmName, const std::string& anActorName, const std::string& aPluginName, const short aKey)
        :_swarmName(aSwarmName), _actorName(anActorName), _pluginName(aPluginName), _barrierKey(aKey){}
    
    friend bool operator <(const GlobalBarrierKey& key1, const GlobalBarrierKey& key2){
        if(key1._swarmName < key2._swarmName) return true;
        if(key1._swarmName == key2._swarmName){
            if(key1._actorName < key2._actorName) return true;
            if(key1._actorName == key2._actorName){
                if(key1._pluginName < key2._pluginName) return true;
                if(key1._pluginName == key2._pluginName) return key1._barrierKey < key2._barrierKey;
            }
        }
        return false;
    }
};

#endif