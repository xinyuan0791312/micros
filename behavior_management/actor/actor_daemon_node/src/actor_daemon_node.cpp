/* ==================================================================
*
* Copyright (c) 2016-2019, micROS Group, NIIDT, TAIIC, HPCL.
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions 
* are met: 
*
* 1. Redistributions of source code must retain the above copyright 
*     notice, this list of conditions and the following disclaimer. 
* 2. Redistributions in binary form must reproduce the above copyright 
*     notice, this list of conditions and the following disclaimer in the 
*     documentation and/or other materials provided with the 
*     distribution. 
* 3. All advertising materials mentioning features or use of this software 
*     must display the following acknowledgement: 
*     This product includes software developed by the micROS Group. and 
*     its contributors. 
* 4. Neither the name of the Group nor the names of its contributors may 
*     be used to endorse or promote products derived from this software 
*     without specific prior written permission. 
*
* THIS SOFTWARE IS PROVIDED BY MICROS,GROUP AND CONTRIBUTORS
* ''AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT
* NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
* FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL 
* THE MICROS,GROUP OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS 
* OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) 
* HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
* OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN 
* IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
* ===================================================================
*
* Author: Bin Di, Chunlian Fu, Junling Gao, Ruihao Li, Wei Yi.
*         (in alphabetical order)
*
*/

#include "actor_core/actor_types.h"
#include "actor_match/actor_match.h"
#include "task_bind/task_bind.h"
//#include "actor_manager/actor_manager.h"
#include "actor_schedule/actor_schedule.h"
#include "ros/ros.h"

//#include "actor_election/actor_election.h"
#include "swarm_data_storage/swarm_data_storage.h"
#include "swarm_robot_heartbeat_handler/swarm_robot_heartbeat_handler_simple.h"
#include "swarm_robot_heartbeat/swarm_robot_heartbeat_strategy_simple.h"
// #include "swarm_robot_heartbeat/swarm_robot_heartbeat_strategy_latency.h"
// #include "swarm_robot_heartbeat_handler/swarm_robot_heartbeat_handler_for_electionbids.h"

#ifdef USE_STIGMERGY
    //consensus
    #include "micros_swarm/micros_swarm.h"//vs only needed
    #include "micros_swarm/serialize.h"
    #include "micros_swarm/singleton.h"
    #include "micros_swarm/runtime_handle.h"//rth 
    #include "micros_swarm/runtime_core.h"
    #include "swarm_robot_heartbeat/swarm_robot_heartbeat_stigmergy.h"
#else
    #include "swarm_robot_heartbeat/swarm_robot_heartbeat_fastrtps.h"
    //#include "swarm_robot_heartbeat/swarm_robot_heartbeat_timeslot.h"
#endif


//#define DEBUG_FLAG   
#include "actor_tag_store/ActorTagStore.h"
#include "actor_state_machine/actor_state_machine.h"

#include "RTPSPublisher.h"
#include "RTPSSubscriber.h"
#include "micROSRTPSExt.h"

class ServiceInterface {
    virtual void startService()=0;
};

class DaemonApp: public ServiceInterface{
public:
    DaemonApp(ros::NodeHandle& nh,std::string& platformResourcesXMLContent,std::vector<boost::shared_ptr<general_bus::GeneralBus>> aBusVec,int32_t aPlatID) {
        this->_nh=nh;
        _pActorMatcher = new ActorMatcher(platformResourcesXMLContent);
        _pActorScheduler = boost::shared_ptr<ActorScheduler>(new ActorScheduler(aBusVec));

        _pActorStateMachine = new ActorStateMachine(_pActorScheduler.get(), aPlatID);
        _pTaskBinder = new TaskBinder(_pActorMatcher, _pActorScheduler.get(), _pActorStateMachine, aPlatID);
       
        _pHeartbeatHandler = new SwarmRobotHeartbeatHandlerSimple(_pActorScheduler);
        _pHeartbeatStrategy = new SwarmRobotHeartbeatStrategySimple();
        _pHeartbeatStrategy->initHeatbeatHandler(_pHeartbeatHandler);
#ifdef USE_STIGMERGY
        _pHeartbeat = new SwarmRobotHeartbeat_Stigmergy();
#else
        _pHeartbeat = new SwarmRobotHeartbeat_RTPS();
        //_pHeartbeat = new SwarmRobotHeartbeat_TimeSlot();
#endif
        _pHeartbeat->initHeartbeatStrategy(_pHeartbeatStrategy);

        // _pHeartbeatHandler = new SwarmRobotHeartbeatHandler_ForElectionBids(_pActorScheduler);
        // _pHeartbeatStrategy = new SwarmRobotHeartbeatStrategyLatency();
        // _pHeartbeatStrategy->initHeatbeatHandler(_pHeartbeatHandler);
        // _pHeartbeat = new SwarmRobotHeartbeat_RTPS();
        // _pHeartbeat->initHeartbeatStrategy(_pHeartbeatStrategy);

        //_pActorManager = new ActorManager(_pActorScheduler,_pActorElection,aPlatID);
        //_pActorManager = new ActorManager(_pActorScheduler.get());
        
    }

    ~DaemonApp() {
        if(_pActorStateMachine) {
            delete _pActorStateMachine;
           _pActorStateMachine = NULL;
        }
        if (_pTaskBinder) {
            delete _pTaskBinder;
           _pTaskBinder=NULL;
        }
        // if (_pActorManager) {
        //     delete _pActorManager;
        //     _pActorManager=NULL;
        // }
        // if (_pActorElection) {
        //     delete _pActorElection;
        //     _pActorElection=NULL;
        // }
        if (_pActorScheduler) {
            //delete _pActorScheduler;
            //_pActorScheduler=NULL;
            _pActorScheduler.reset();
        }
        if (_pActorMatcher) {
            delete _pActorMatcher;
            _pActorMatcher=NULL;
        }
        //
        if(_pHeartbeatHandler){
            delete _pHeartbeatHandler;
            _pHeartbeatHandler = NULL;
        }
        if(_pHeartbeatStrategy){
            delete _pHeartbeatStrategy;
            _pHeartbeatStrategy = NULL;
        }
        if(_pHeartbeat){
            delete _pHeartbeat;
            _pHeartbeat = NULL;
        }
        
    }

    void startService() {
        _pActorScheduler->start();
        #ifndef DEBUG_FLAG
        _pActorMatcher->startService();
        #endif
        _pTaskBinder->start0();
        _pActorStateMachine->start();
        //
        //_pActorElection->start();
        _pHeartbeat->run();
        //_pActorManager->start();
    #ifdef AUTO_RUN_TASK
        _pTaskBinder->startAutoTask();
    #endif
    }

    ros::NodeHandle _nh;
    ActorMatcher* _pActorMatcher;
    TaskBinder* _pTaskBinder;
    ActorStateMachine *_pActorStateMachine;
    //ActorManager* _pActorManager;
    // ActorScheduler* _pActorScheduler;
    boost::shared_ptr<ActorScheduler> _pActorScheduler;
    //election
    //ActorElection* _pActorElection;

    SwarmRobotHeartbeat* _pHeartbeat;
    SwarmRobotHeartbeatStrategy* _pHeartbeatStrategy;
    SwarmRobotHeartbeatHandler* _pHeartbeatHandler;

};

//consensus
/* ros::Timer timer1,timer2;
micros_swarm::VirtualStigmergy vs;
boost::shared_ptr<micros_swarm::RuntimeHandle> rth;

double x = 0;
double y = 0;


    void loop_puts(const ros::TimerEvent&)
    {
        std::string robot_id_string = "robot_"+boost::lexical_cast<std::string>(rth->getRobotID());

        static int count = 0;
        std_msgs::Int32 pval;
        //pval.data = get_id() + count;
        pval.data = rth->getRobotID();
        vs.put<std_msgs::Int32>(robot_id_string, pval);

        count=count+rth->getRobotID();
        if (count==101)
        	count=0;

        micros_swarm::Base l(count, count, count, 0, 0, 0, 1);
        rth->setRobotBase(l);
        //std::cout<<robot_id_string<<": "<<vs.size()<<std::endl;
        //vs.print();
        /*static bool flag = true;
        if(flag) {
            if(vs.size() == 60) {
                std::cout<<get_id()<<" get all tuple"<<std::endl;
                flag = false;
            }
        }*/ /*
    }

    void loop_gets(const ros::TimerEvent&)
    {
        std::string robot_id_string = "robot_" + boost::lexical_cast<std::string>(rth->getRobotID());
        std_msgs::Int32 gval;
        //int lamport_clock;
        if(!vs.get<std_msgs::Int32>(robot_id_string,gval))
        	return;
        //std::cout << robot_id_string << ": " << vs.size() << ", " << gval.data << ", " << lamport_clock << std::endl;
        vs.print();
        std::map<std::string, micros_swarm::VirtualStigmergyTuple> vs_map;
        vs.get_vs(vs_map);

        std_msgs::Int32 temp=micros_swarm::deserialize_ros<std_msgs::Int32>(vs_map[robot_id_string].vstig_value);
        std::cout << robot_id_string << ": " <<  vs_map[robot_id_string].lamport_clock << ", " <<temp.data << std::endl;
    }
 */

int main(int argc,char** argv) {
    //micROS::init();
    ros::init(argc,argv,"actor_main");
    ros::NodeHandle nh;
    ros::NodeHandle privNh("~");
#ifdef DDS_STATIC_DISCOVERY
    std::string strDDSXmlProfile = "";
    if (nh.hasParam("/dds_static_profile_xml")) {
        nh.getParam("/dds_static_profile_xml",strDDSXmlProfile); 
    } else {
        ROS_WARN("cannot find /dds_static_profile_xml in the parameter server");
    }
    micROS::init(strDDSXmlProfile);
#else    
    // fastrtps participant discovery list 
    std::string strRobotListFile="";
    if (nh.hasParam("/robotListFile")) {
        nh.getParam("/robotListFile",strRobotListFile); 
    } else {
        ROS_WARN("cannot find /robotListFile in the parameter server");
    }
    
    // fastrtps domain_id
    int domainId = 0;
    if(nh.hasParam("/dds_domainid")){
        nh.getParam("/dds_domainid", domainId);
    } else {
        ROS_WARN("cannot find /dds_domainid in the parameter server, default 0");
    }
    ROS_WARN("DDS DomainId is %d", domainId);
    micROS::init(domainId, strRobotListFile);
#endif

    // tag list directory
    std::string strTagListFile = "";
    if (nh.hasParam("/taglistdict")) {
        nh.getParam("/taglistdict",strTagListFile); 
    } else {
        ROS_WARN("cannot find /taglistdict in the parameter server");
    }
    if (!gActorTagStore.init(strTagListFile)) {
		std::cout<<"FATAL: unable to get the actorName to tag mapping!"<<std::endl;
		return 1;
	}    

    std::string platformResourcesFileName;
    platformResourcesFileName = "/home/gjl/micros_ws/micros_actor/src/micros/behavior_management/actor/test/data/robot_0.xml";
    //platformResourcesFileName = "/home/captain/code_server/phy_code_server/micros_actor/src/micros/behavior_management/actor/test/data/robot_0.xml";
    
    if (nh.hasParam("PlatformResourcesFile")) {
        nh.getParam("PlatformResourcesFile",platformResourcesFileName); 
    } else {
        ROS_WARN("cannot find PlatformResourcesFile in the parameter server");
    }

    //platID
    int32_t platID = -1;
    if (privNh.hasParam("robot_id")) {
        privNh.getParam("robot_id",platID); 
    } else {
        ROS_WARN("[Daemon Node]cannot find robot ID in the parameter server, return..");
        return 0;
    }

    std::string logFilePath = "";
    if (nh.hasParam("/ActorLogFileDir")) {
        nh.getParam("/ActorLogFileDir",logFilePath); 
    } else {
        std::cout<<"WARNING:cannot find actor election data file dir in the parameter server"<<std::endl;
    }
    
    //SwarmDataStorage::instance()->setLocalRobotID(platID);
    SwarmDataStorage::instance()->init(platID, logFilePath);
    ROS_INFO("[Daemon Node] robotID=%d", platID);
    
    //TODO: move to DaemaonApp constructor
    boost::shared_ptr<general_bus::GeneralBus> pObserveBus(new general_bus::GeneralBus("observe_bus"));
    boost::shared_ptr<general_bus::GeneralBus> pOrientBus(new general_bus::GeneralBus("orient_bus"));
    boost::shared_ptr<general_bus::GeneralBus> pDecideBus(new general_bus::GeneralBus("decide_bus"));
    boost::shared_ptr<general_bus::GeneralBus> pActBus(new general_bus::GeneralBus("act_bus"));
    std::vector<boost::shared_ptr<general_bus::GeneralBus>> busVec;
    busVec.push_back(pObserveBus);
    busVec.push_back(pOrientBus);
    busVec.push_back(pDecideBus);
    busVec.push_back(pActBus);
    
    /*  
    TiXmlDocument doc;
    std::string xmlStr;
    if (!doc.LoadFile(platformResourcesFileName)) {
	    ROS_FATAL("[Actor Daemon Node]Error while open the platform resource XML file!");
    } else {
        TiXmlPrinter printer;
        doc.Accept(&printer);
        xmlStr = printer.CStr();
    } 
    std::string platformResourcesXMLContent;
    platformResourcesXMLContent = xmlStr;


    */
    //consensus information
#ifdef USE_STIGMERGY
    boost::shared_ptr<micros_swarm::RuntimeCore> rtp_core_;

    rtp_core_.reset(new micros_swarm::RuntimeCore());
    rtp_core_->initialize();
#endif
    //micros_swarm::VirtualStigmergy vs;
    
	//vs=micros_swarm::VirtualStigmergy(3);
    //may be global
    //boost::shared_ptr<micros_swarm::RuntimeHandle> rth;
	//rth = micros_swarm::Singleton<micros_swarm::RuntimeHandle>::getSingleton();   

    //timer1 = nh.createTimer(ros::Duration(2), &loop_puts);
    //timer2 = nh.createTimer(ros::Duration(3), &loop_gets);
    

    DaemonApp daemonApp(nh,platformResourcesFileName,busVec,platID);
    daemonApp.startService();
    //ros::spin();
    boost::thread t = boost::thread(boost::bind(&ros::spin));
    t.join();    

    for(std::vector<boost::shared_ptr<general_bus::GeneralBus> >::iterator it= busVec.begin();it!=busVec.end();it++){
        (*it).reset();
    }


    micROS::finish();
    return 0;
}
