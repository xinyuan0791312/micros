/* ==================================================================
*
* Copyright (c) 2016-2019, micROS Group, NIIDT, TAIIC, HPCL.
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions 
* are met: 
*
* 1. Redistributions of source code must retain the above copyright 
*     notice, this list of conditions and the following disclaimer. 
* 2. Redistributions in binary form must reproduce the above copyright 
*     notice, this list of conditions and the following disclaimer in the 
*     documentation and/or other materials provided with the 
*     distribution. 
* 3. All advertising materials mentioning features or use of this software 
*     must display the following acknowledgement: 
*     This product includes software developed by the micROS Group. and 
*     its contributors. 
* 4. Neither the name of the Group nor the names of its contributors may 
*     be used to endorse or promote products derived from this software 
*     without specific prior written permission. 
*
* THIS SOFTWARE IS PROVIDED BY MICROS,GROUP AND CONTRIBUTORS
* ''AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT
* NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
* FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL 
* THE MICROS,GROUP OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS 
* OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) 
* HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
* OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN 
* IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
* ===================================================================
*
* Author: Bin Di, Chunlian Fu, Junling Gao, Ruihao Li, Wei Yi.
*         (in alphabetical order)
*
*/

#ifndef __ACTOR_STATE_MACHINE__
#define __ACTOR_STATE_MACHINE__

#include "actor_schedule/actor_schedule.h"
#include "actor_msgs/UTOEvent.h"
#include "actor_msgs/UTOEventTransition.h"
#include "std_msgs/String.h"
#include <ros/ros.h>
#include <tinyxml.h>
#include <boost/thread/thread_pool.hpp>
#include "RTPSPublisher.h"
#include "RTPSSubscriber.h"
#include "micROSRTPSExt.h"
#include "UTOEventTransition.h"
#include "UTOEventWithGStation.h"
#include "StringMsg.h"
#include "actor_core/actor_callbackqueue.h"

/**
 * @brief Compiler toggle
 * 
 * If defined, it is FastRTPS communication between UTO and gstation/Qt. Otherwise, it's ROS.
 */
#define EVENT_GS_RTPS_COMM

/**
 * @brief Compiler toggle
 * 
 * If defined, it is master barrier synchronization for UTO. Otherwise, it's gstation/Qt synchronization.
 * 
 * @note If ground station wants to pub event msg to control actor switching, it should be defined.
 */
#define EVENT_BARRIER_SYNC

/**
 * @brief Master synchronization default waiting duration
 */
#define DEFAULT_SUSPEND_DURATION 60

/**
 * @brief Event synchronize status for gstation/Qt synchronization
 */
enum EEventSyncStatus {
    EVENT_SYNC_BLOCK = 1,  ///< UTO event transition message has not yet been synchronized
    EVENT_SYNC_READY = 2   ///< UTO event transition message has been synchronized 
};

/**
 * @brief Actor parameter type
 */
enum EActorParamType {
    PARAM_TYPE_NORMAL = 0,  ///< Normal parameter of the actor
    PARAM_TYPE_DISTRIBUTE = 1  ///< Distribute parameter of the actor
};

/**
 * @brief Enum operate type for managing running actors list
 */
enum EOperateType {
    TYPE_DELETE = 0,  ///< Delete member
    TYPE_REPLACE = 1,  ///< Replace member
    TYPE_ADD = 2,   ///< Add member
    TYPE_CLEAR = 3   ///< Clear all member
};

/**
 * @brief UTOTransition struct to save event data
 */
struct UTOTransition {
    std::string _eventName;  ///< Transition event name
    std::string _nextActor;  ///< Transition target actor name
    int _sysNum;  ///< Number of platforms for synchronization at the same time
    short _barrierKey;   ///< Key value for barrier settings of master synchronization
    short _exeCount;  ///< Number of event transition execution circularly
    short _timeout;  ///< Synchronization timeout in seconds
    void clear() {
        _eventName.clear();
        _nextActor.clear();
        _sysNum = 0;
        _barrierKey = -1;
        _exeCount = 0;
        _timeout = DEFAULT_SUSPEND_DURATION;
    }
};

/**
 * @brief ActorParam　struct to save parameters in actor
 */
struct ActorParam {
    std::string _key;  ///< Actor Parameter key
    std::string _value;  ///< Actor Parameter value
    EActorParamType _type;  ///< Actor parameter type
    void clear() {
        _key.clear();
        _value.clear();
        _type = PARAM_TYPE_NORMAL;
    }
};

/**
 * @brief UTOTransition struct to save actor data
 */
struct UTOActorNode {
    std::string _actorName;  ///< Current actor name
    std::vector<ActorParam> _paramList;  ///< Parameters for the actor
    std::vector<UTOTransition> _eventList;  ///< Event list for the actor
    void clear() {
        _actorName.clear();
        _paramList.clear();
        _eventList.clear();
    }
};

/**
 * @brief UTOSuspendEvent struct for event gstation/Qt synchronization
 */
struct UTOSuspendEvent {
    actor_msgs::UTOEvent _eventMsg;  ///< Event needed to synchronize
    time_t _suspendTime;  ///< Startup synchronization time
    int32_t _suspendDuration;  ///< Synchronize waiting period
};

/**
 * @brief ActorStateMachine class
 */
class ActorStateMachine {
public:
    ActorStateMachine(ActorScheduler *aPActorScheduler, int64_t aPlatformID);
    ~ActorStateMachine();

    /**
     * @brief Class initialize method
     */
    void start();

    /**
     * @brief Set UTO data after receiving an UTO task
     * 
     * @param[in] aUTOList Parsed UTO data
     */
    void setUTOList(const std::vector<UTOActorNode>& aUTOList);

    /**
     * @brief Set the initial actors after receiving an UTO task
     * 
     * @param[in] aSwarmName Swarm name for the UTO task
     * @param[in] anInitialActors Vector for initial actors, int in pair is platform count and string in pair is actor name
     */
    void setInitialActors(const std::string& aSwarmName, const std::vector<std::pair<int, std::string> >& anInitialActors);

    /**
     * @brief Set the Task Auto Process Config object
     * 
     * @param[in] anActorIntervalMap key is actor, value is the actor auto switching interval time (s)
     */
    void setTaskAutoProcessConfig(const std::map<std::string, int>& anActorIntervalMap);

private:
    /* UTO initial */
    int64_t _platformID;  
    std::string _swarmName;
    ActorScheduler *_pActorScheduler;


    /* UTO Event communication between UTO and gstation */
    RTPSSubscriber<UTOEventWithGStation> *_rtpEventGSSub;
    void eventGSCallback(UTOEventWithGStation &eventGSMsg); 
    

    /* UTO task list */
    boost::shared_mutex _utoListMutex;  ///< Mutex for _swarmNameToInitialActors and _utoList
    std::vector<UTOActorNode> _utoList;
    std::map<std::string, std::vector<std::pair<int, std::string> > > _swarmNameToInitialActors;
    std::map<std::string, bool> _swarmStarted;  ///< Map key is swarm name, value is whether swarm is started

    /* Message communication between UTO and plugins */
    ros::Subscriber _eventSub;
    /** 
     * @brief Transition event msg callback of ROS topic
     * 
     * @param[in] msg UTOEvent msg of ROS
     */
    void eventCallback(const actor_msgs::UTOEvent::ConstPtr &msg);
    

    /* Event message communication for synchronization between UTO and gstation/Qt */
#if defined(EVENT_GS_RTPS_COMM)
    // FastRTPS communication with Qt/gstation
    RTPSPublisher<UTOEventTransition> *_rtpEventPub;
    RTPSSubscriber<UTOEventTransition> *_rtpEventSub;
    // Event transition callback from gstation/Qt on FastRTPS
    void eventRTPSCallback(UTOEventTransition &msg);
#else
    ros::Publisher _eventSyncPub;
    ros::Subscriber _eventSyncSub;

    /**
     * @brief Event synchronization msg callback from gstation/Qt of ROS topic
     * 
     * @param[in] msg UTOEventTransition msg of ROS
     */
    void eventSyncCallback(const actor_msgs::UTOEventTransition &msg);
#endif
    // Event message synchronization with gstation/Qt
    boost::mutex _msgSyncMutex;
    boost::condition_variable _msgSyncCV;
    std::vector<UTOSuspendEvent> _msgSyncList;
    boost::shared_ptr<boost::thread> _pSyncQueueThread;

    /**
     * @brief Synchronization event process mthod
     * 
     * Detection whether event msg time out occurred while waiting for Qt/gstation synchronization
     */
    void eventSyncTimeoutDetection();

    /* UTO event transition handling */
    boost::mutex _qMutex;
    boost::condition_variable _cv;
    boost::shared_ptr<boost::thread> _pEventProcessThread;
    boost::shared_mutex _runMutex;
    std::vector<std::string> _utoRunningActorsList;  ///< Current running actors' list

    boost::shared_ptr<boost::thread> _pAutoRunTaskThd;
    boost::mutex _autoRunMutex;
    boost::condition_variable _autoRunCond;

    /**
     * @brief This queue is used for handle msgs
     * @details Msgs contain these categories
     * <BR> SWITCH_EVENT_MSG, ROS callback method is eventCallback. It is from plugins to switch actor.
     * <BR> SWITCH_EVENT_MSG_GS_SYNED, generated from SWITCH_EVENT_MSG after ground station synchronization confirmation
     *           and timeout handling. Msg callback methods interact with ground station for synchronization control 
     *              are eventRTPSCallback (FastRTPS) and eventSyncCallback (ROS).
     * <BR> START_TASK_MSG, FastRTPS callback method is eventGSCallback. It is from ground station to start UTO task.
     * <BR> ACTIVATE_ACTOR_MSG, ROS callback method is eventCallback. It is called by group manager to activate an actor.
     * <BR> PAUSE_ACTOR_MSG, ROS callback method is eventCallback. It is called by group manager to pause an actor.
     * <BR> SWITCH_ACTOR_MSG, ROS callback method is eventCallback. It is called by group manager to switch an actor to another.
     */
    std::queue<actor_msgs::UTOEvent> _eventMsgQueue;

    /**
     * @brief Event process method
     * 
     * _pEventProcessThread bind the method to process event in _eventMsgQueue
     */
    void eventProcessing();

    /**
     * @brief Handle transition event method, called in eventProcessing method and msgs are from _eventMsgQueue.
     * 
     * @param[in] UTOEvent msg of ROS
     */
    void handleEventTransition(actor_msgs::UTOEvent &eventMsg);

    /**
     * @brief Check whether the request actor is running
     * 
     * @param[in] anActorName The actor name
     * @return true The actor is valid
     * @return false The actor is invalid
     */
    bool isActorAvailable(const std::string &anActorName);

    /**
     * @brief Request barrier synchronization for transition event
     * 
     * @param[in] syncNum Number of platforms for event synchronization
     * @param[in] key Barrier key for event synchronization
     * @param[in] anActorName Actor for log
     * @param[in] timeout Timeout in seconds for event synchronization
     * @param[out] anIndex Distribute parameter index
     * @return true The event after synchronization needs to be executed
     * @return false Events after synchronization don't need to be executed
     */
    bool requestBarrierSync(int aSyncNum, short aKey, const std::string &anActorName, short timeout, int &anIndex);

    /**
     * @brief Publish transition event msg or actor starts running msg to gstation
     * 
     * @param[in] anActorName The actor name
     * @param[in] anEvent The event message
     * @param[in] aStamp The time stamp
     */
    void pubEventMsgToGSTation(const std::string &anActorName, const std::string &anEvent, const time_t &aStamp);

    /* UTO event synchronization with actor barrier */
    boost::shared_ptr<boost::executors::basic_thread_pool> _poolPtr;  ///< Use thread pool to run event

    /**
     * @brief Parse distribute param in to ways, one is splitting by ',' and the other is Arithmetic sequence such as (1,5,17). e.g. 1 is initial value, 5 is D-value, 17 is max value.
     * 
     * @param[in] aSourceStr Origin distribute param
     * @param[out] aDestStr Param value in the index of 'anIndex'
     * @param[in] aSysNum Barrier synchronization number
     * @param[in] anIndex Index of distribution param from barrier synchronization
     */
    void distributeParamParse(std::string aSourceStr, std::string& aDestStr, int aSysNum, int anIndex);

    /**
     * @brief Check whether all of characters in string are digit
     * 
     * @param[in] aSourceStr Source string 
     * @return true All of characters in string are digit
     * @return false Not all of characters in string are digit
     */
    bool isAllNum(const std::string& aSourceStr);

    /**
     * @brief Create individual callback queue for ROS　message
     */
    boost::shared_ptr<ActorCallbackQueueInfo> _rosCallbackQueue;

    /**
     * @brief ROS subscribe with individual callback queue
     * 
     * @tparam[in] M The callback parameter type, not the message type, and should almost always be deduced
     * @tparam[in] T Class template
     * @param[in] topic Topic to subscribe to 
     * @param[in] queue_size Number of incoming messages to queue up for processing
     * @param[in] fp Member function pointer to call when a message has arrived
     * @param[in] obj Object to call fp on
     * @param[in] transport_hints A TransportHints structure which defines various transport-related options 
     * @return ros::Subscriber ROS subscriber
     */
    template<class M, class T>
    ros::Subscriber rosSubscribe(const std::string& topic, uint32_t queue_size, void(T::*fp)(const boost::shared_ptr<M const>&), T* obj, const ros::TransportHints& transport_hints = ros::TransportHints());

    /**
     * @brief ROS handle
     */
    ros::NodeHandle _handle;

    /**
     * @brief Update running actors' list
     * 
     * @param[in] type Operate type
     * @param[in] aSrc Source actor
     * @param[in] aTarget Target actor
     */
    void updateRunningActors(const EOperateType type, const std::string& aSrc="", const std::string& aTarget="");

    /**
     * @brief key is actor, value is the actor auto switching interval time (s), used in task auto process
     */
    std::map<std::string, int> _actor2IntervalMap;

    /**
     * @brief Auto running task and switch actor in interval time
     */
    void autoRunTaskProcedure();

    /**
     * @brief Push event msg to _eventMsgQueue
     * 
     * @param[in] msg actor_msgs::UTOEvent msg
     */
    void pushEventMsgToQueque(const actor_msgs::UTOEvent &msg);

};

#endif
