/* ==================================================================
*
* Copyright (c) 2016-2019, micROS Group, NIIDT, TAIIC, HPCL.
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions 
* are met: 
*
* 1. Redistributions of source code must retain the above copyright 
*     notice, this list of conditions and the following disclaimer. 
* 2. Redistributions in binary form must reproduce the above copyright 
*     notice, this list of conditions and the following disclaimer in the 
*     documentation and/or other materials provided with the 
*     distribution. 
* 3. All advertising materials mentioning features or use of this software 
*     must display the following acknowledgement: 
*     This product includes software developed by the micROS Group. and 
*     its contributors. 
* 4. Neither the name of the Group nor the names of its contributors may 
*     be used to endorse or promote products derived from this software 
*     without specific prior written permission. 
*
* THIS SOFTWARE IS PROVIDED BY MICROS,GROUP AND CONTRIBUTORS
* ''AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT
* NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
* FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL 
* THE MICROS,GROUP OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS 
* OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) 
* HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
* OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN 
* IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
* ===================================================================
*
* Author: Bin Di, Chunlian Fu, Junling Gao, Ruihao Li, Wei Yi.
*         (in alphabetical order)
*
*/

#include "actor_state_machine/actor_state_machine.h"
#include "swarm_master_cmd/swarm_master_cmd_api.h"

/**
 * @brief Synchronization barrier key (zero) for initial actors in an task
 * 
 * @note Zero of barrier key is illegal for plugin user.
 */
#define INITIAL_ACTOR_SYNC_KEY 0

ActorStateMachine::ActorStateMachine(ActorScheduler *aPActorScheduler, int64_t aPlatformID) {
    this->_pActorScheduler = aPActorScheduler;
    _platformID = aPlatformID;
}

ActorStateMachine::~ActorStateMachine() {
    if(_pEventProcessThread) {
        _pEventProcessThread->interrupt();
        _pEventProcessThread->join();
        _pEventProcessThread.reset();
    }
#ifndef EVENT_BARRIER_SYNC
    if(_pSyncQueueThread) {
        _pSyncQueueThread->interrupt();
        _pSyncQueueThread->join();
        _pSyncQueueThread.reset();
    }
#endif
    if(_poolPtr){
        _poolPtr->join();
        _poolPtr.reset();
    }
    if(_pActorScheduler) delete _pActorScheduler;
#ifdef EVENT_GS_RTPS_COMM
    if(_rtpEventPub) {
        delete _rtpEventPub;
    }
    if(_rtpEventSub) {
        delete _rtpEventSub;
    }
#endif
    if(_rtpEventGSSub) {
        delete _rtpEventGSSub;
    }
    if(_rosCallbackQueue) {
        _rosCallbackQueue->stopCallbackQueue();
        _rosCallbackQueue.reset();
    }

#ifdef AUTO_RUN_TASK
    if(_pAutoRunTaskThd) {
        _pAutoRunTaskThd->interrupt();
        _pAutoRunTaskThd->join();
        _pAutoRunTaskThd.reset();
    }
#endif
}


void ActorStateMachine::start() {
    _rosCallbackQueue.reset(new ActorCallbackQueueInfo(""));
    // Subscribe msgs from plugins, including response to receive params and requirement for event transition.
    _eventSub = rosSubscribe<actor_msgs::UTOEvent>("uto_event_msg", 10000, &ActorStateMachine::eventCallback, this);
    _rosCallbackQueue->startCallbackQueue();

    // Subscribe msgs from GS through FastRTPS: 1. active actors; 2. transimit actors.
    _rtpEventGSSub = new RTPSSubscriber<UTOEventWithGStation>("/uto_event_msg", &ActorStateMachine::eventGSCallback, this);

#ifdef EVENT_GS_RTPS_COMM
    // Initial FastRTPS pub＆sub to communicate with gstation/Qt
    _rtpEventPub = new RTPSPublisher<UTOEventTransition>("/uto_event_transition");
    _rtpEventSub = new RTPSSubscriber<UTOEventTransition>("/uto_event_sync", &ActorStateMachine::eventRTPSCallback, this);
#else
    // Publish event to gstation in order to event synchronization
    _eventSyncPub = nh.advertise<actor_msgs::UTOEventTransition>("/uto_event_transition", 10000);
    // Subscribe msgs from gstation for event transition cmd
    _eventSyncSub = nh.subscribe("/uto_event_sync", 10000, &ActorStateMachine::eventSyncCallback, this);
#endif

    // Create a thread to monitor event message queue and handle event transition
    _pEventProcessThread = boost::shared_ptr<boost::thread>(new boost::thread(boost::bind(&ActorStateMachine::eventProcessing, this)));
    // _pEventProcessThread->detach();

#ifndef EVENT_BARRIER_SYNC
    // Create a thread to monitor event synchronize message queue
    _pSyncQueueThread = boost::shared_ptr<boost::thread>(new boost::thread(boost::bind(&ActorStateMachine::eventSyncTimeoutDetection, this)));
    // _pSyncQueueThread->detach();
#endif

    _poolPtr = boost::shared_ptr<boost::executors::basic_thread_pool>(new boost::executors::basic_thread_pool(4));
}

void ActorStateMachine::eventProcessing() {
    actor_msgs::UTOEvent eventMsg;
    while(true) {
        {
            // Wait and hang-up the thread when _eventMsgQueue is empty.
            boost::unique_lock<boost::mutex> lock(_qMutex);
            if(_eventMsgQueue.empty()) {
                _cv.wait(lock);
            }
            eventMsg = _eventMsgQueue.front();
            _eventMsgQueue.pop();
        }

        // ROS_INFO("[actor_state_machine] _eventMsgQueue loop %d, actor:%s, event:%s, type:%d", i, eventMsg.currentActor.c_str(), eventMsg.eventName.c_str(),eventMsg.type);                
        if(eventMsg.type == SWITCH_EVENT_MSG) 
        {
            if(!isActorAvailable(eventMsg.currentActor))
                continue;
            time_t stamp = time(NULL);
            pubEventMsgToGSTation(eventMsg.currentActor, eventMsg.eventName, stamp);

        #ifndef EVENT_BARRIER_SYNC
            // Synchronization depends on gstation control
            UTOSuspendEvent se;
            se._eventMsg = eventMsg;
            se._suspendTime = stamp;
            se._suspendDuration = DEFAULT_SUSPEND_DURATION;
            boost::unique_lock<boost::mutex> lock(_msgSyncMutex);
            _msgSyncList.push_back(se);
            _msgSyncCV.notify_one();
            continue;
        #endif
        }

        // ROS_INFO("[actor_state_machine] handle event on %d, actor %s, event %s", _platformID, eventMsg.currentActor.c_str(), eventMsg.eventName.c_str());
        // Use thread pool to handle events
        _poolPtr->submit(boost::bind(&ActorStateMachine::handleEventTransition, this, eventMsg));
    }
}

void ActorStateMachine::pushEventMsgToQueque(const actor_msgs::UTOEvent &msg) {
    // Wakeup monitor loop to handle event
    boost::unique_lock<boost::mutex> lock(_qMutex);
    _eventMsgQueue.push(msg);
    _cv.notify_one();
}

void ActorStateMachine::autoRunTaskProcedure() {
    usleep(100000);   ///< Wait for actor scheduler finishing first schedule action

    actor_msgs::UTOEvent eventMsg;
    eventMsg.type = START_TASK_MSG; 
    {
        boost::shared_lock<boost::shared_mutex> rlock(_utoListMutex);
        eventMsg.currentActor = _swarmName;
    }
    pushEventMsgToQueque(eventMsg);

    std::string currentActor;
    int interval = 120;
    bool eventSended = true;
    while (true)
    {
        boost::unique_lock<boost::mutex> lock(_autoRunMutex);
        if(!_autoRunCond.timed_wait(lock, boost::get_system_time() + boost::posix_time::seconds(interval))) {
            if(!eventSended && !currentActor.empty()) {
                ///< Switch actor
                eventMsg.type = SWITCH_EVENT_MSG; 
                eventMsg.currentActor = currentActor;
                eventMsg.eventName = "finish_event";
                pushEventMsgToQueque(eventMsg);
                eventSended = true;
                interval = 120;
            }
        } else {    ///< eventSended false, plugin switch actor by itself; eventSended true, actor state machine finish switching actor;
            eventSended = false;
            
            ///< Get current actor
            {
                boost::shared_lock<boost::shared_mutex> lock(_runMutex);
                if(!_utoRunningActorsList.empty()) {
                    currentActor = *(_utoRunningActorsList.begin());
                } else {
                    currentActor.clear();
                }
            }

            ///< Get current actor switch interval
            interval = currentActor.empty() ? 120 : _actor2IntervalMap[currentActor];
        }
    }
}

void ActorStateMachine::setTaskAutoProcessConfig(const std::map<std::string, int>& anActorIntervalMap) {
    _actor2IntervalMap = anActorIntervalMap;
    if(!_pAutoRunTaskThd)
        _pAutoRunTaskThd = boost::shared_ptr<boost::thread>(new boost::thread(boost::bind(&ActorStateMachine::autoRunTaskProcedure, this)));
}

/* Event synchronization from gstation/Qt */
void ActorStateMachine::eventSyncTimeoutDetection() {
    UTOSuspendEvent susEvent;
    time_t ctime;
    bool flag;
    while(true) {
        flag = false;
        // Wait and hang-up the thread when _msgSyncList is empty.
        {
            boost::unique_lock<boost::mutex> lock(_msgSyncMutex);
            if(_msgSyncList.empty()) {
                _msgSyncCV.wait(lock);
            }
            susEvent = _msgSyncList.front();
            ctime = time(NULL);
            if((ctime-susEvent._suspendTime) >= susEvent._suspendDuration) {
                flag = true;
                _msgSyncList.erase(_msgSyncList.begin());
                ROS_INFO("[actor_state_machine] duration over time : %d", susEvent._suspendDuration);
            }
        }

        // If a timeout occurred while event waiting for gstation response, event will be moved to _eventMsgQueue to be handled
        if(flag) {
            susEvent._eventMsg.type = SWITCH_EVENT_MSG_GS_SYNED;
            pushEventMsgToQueque(susEvent._eventMsg);
        } else {
            usleep(100000);
        }
    }
}

#ifdef EVENT_GS_RTPS_COMM
// Event transition callback from gstation/Qt on FastRTPS
void ActorStateMachine::eventRTPSCallback(UTOEventTransition &msg)
{
    boost::unique_lock<boost::mutex> lock(_msgSyncMutex);
    for(std::vector<UTOSuspendEvent>::iterator it=_msgSyncList.begin(); it!=_msgSyncList.end(); it++) {
        if(msg.timestamp() == it->_suspendTime && msg.currentActor() == it->_eventMsg.currentActor) {
            if(msg.syncStatus() == EVENT_SYNC_READY) {
                // When event's suspend duration is zero, the event transition will be handled
                it->_suspendDuration = 0;
                ROS_INFO("[actor_state_machine] Receive 'Agree to state transition' from g_station, currentActor:%s, eventName:%s", msg.currentActor().c_str(), msg.eventName().c_str());            
            } else if(msg.syncStatus() == EVENT_SYNC_BLOCK) {
                // Waiting longer for message synchronization
                it->_suspendDuration = 3 * it->_suspendDuration;
                ROS_INFO("[actor_state_machine] Receive 'Waiting' from g_station, currentActor:%s, eventName:%s", msg.currentActor().c_str(), msg.eventName().c_str());
            }
            break;
        }
    }
}
#else

/* Event transition synchronization callback from gstation/Qt on ros topic */
void ActorStateMachine::eventSyncCallback(const actor_msgs::UTOEventTransition &msg)
{
    boost::unique_lock<boost::mutex> lock(_msgSyncMutex);
    for(std::vector<UTOSuspendEvent>::iterator it=_msgSyncList.begin(); it!=_msgSyncList.end(); it++) {
        if(msg.timestamp == it->_suspendTime && msg.currentActor == it->_eventMsg.currentActor) {
            if(msg.syncStatus == EVENT_SYNC_READY) {
                // When event's suspend duration is zero, the event transition will be handled
                it->_suspendDuration = 0;
                ROS_INFO("[actor_state_machine] Receive 'Agree to state transition' from g_station, currentActor:%s, eventName:%s", msg.currentActor.c_str(), msg.eventName.c_str());            
            } else if(msg.syncStatus == EVENT_SYNC_BLOCK) {
                // Waiting longer for message synchronization
                it->_suspendDuration = 3 * it->_suspendDuration;
                ROS_INFO("[actor_state_machine] Receive 'Waiting' from g_station, currentActor:%s, eventName:%s", msg.currentActor.c_str(), msg.eventName.c_str());
            }
            break;
        }
    }
}
#endif

void ActorStateMachine::eventCallback(const actor_msgs::UTOEvent::ConstPtr &msg) {
    ROS_INFO("[actor_state_machine] receive msg request through ROS: eventName:%s, currentActor:%s, targetActor:%s", msg->eventName.c_str(), msg->currentActor.c_str(), msg->targetActor.c_str());
    // Wakeup monitor loop to handle event
    boost::unique_lock<boost::mutex> lock(_qMutex);
    _eventMsgQueue.push(*msg);
    _cv.notify_one();
}

// Event transition/activation callback from GS through FastRTPS
void ActorStateMachine::eventGSCallback(UTOEventWithGStation &eventGSMsg)
{
    actor_msgs::UTOEvent eventMsg;
    eventMsg.currentActor = eventGSMsg.currentActor();
    eventMsg.eventName = eventGSMsg.eventName();  
    eventMsg.type = eventGSMsg.type();  
    ROS_INFO("[actor_state_machine] receive event transition request from GS: currentActor:%s eventName:%s", eventGSMsg.currentActor().c_str(), eventGSMsg.eventName().c_str());
    pushEventMsgToQueque(eventMsg);
}

void ActorStateMachine::setUTOList(const std::vector<UTOActorNode> &aUTOList)
{    
    boost::lock_guard<boost::shared_mutex> lock(_utoListMutex);
    _utoList = aUTOList;
}

void ActorStateMachine::setInitialActors(const std::string &aSwarmName, const std::vector<std::pair<int, std::string> > &anInitialActors)
{   
    {
        boost::lock_guard<boost::shared_mutex> wlock(_utoListMutex);
        _swarmNameToInitialActors[aSwarmName] = anInitialActors;
        if(_swarmStarted.count(_swarmName) > 0)
            _swarmStarted[_swarmName] = false;  ///< Clear started swarm
        _swarmName = aSwarmName;
    }
    updateRunningActors(TYPE_CLEAR);
}

void ActorStateMachine::handleEventTransition(actor_msgs::UTOEvent &eventMsg) 
{
#ifdef EVENT_BARRIER_SYNC
    if(eventMsg.type == SWITCH_EVENT_MSG)
#else
    if(eventMsg.type == SWITCH_EVENT_MSG_GS_SYNED)
#endif
    {
        if(!isActorAvailable(eventMsg.currentActor)) return;

        //Get next actor name
        std::string nextActor;
    #ifdef EVENT_BARRIER_SYNC
        int sysNum = -1;
        // High 6 bit respresent event index and low 10 bit respresent number of execution circularly
        short key = -1;
        short timeout = DEFAULT_SUSPEND_DURATION;
        bool hasBarrier = false;
    #endif
        
        {
            boost::lock_guard<boost::shared_mutex> wlock(_utoListMutex);
            for(std::vector<UTOActorNode>::iterator it = _utoList.begin(); it != _utoList.end(); it++) {
                if(it->_actorName == eventMsg.currentActor) {
                    for(std::vector<UTOTransition>::iterator vit = it->_eventList.begin(); vit != it->_eventList.end(); vit++) {
                        if(vit->_eventName == eventMsg.eventName) {
                            nextActor = vit->_nextActor;
                        #ifdef EVENT_BARRIER_SYNC
                        #ifndef AUTO_RUN_TASK
                            if(vit->_barrierKey > -1) {
                                hasBarrier = true;
                                sysNum = vit->_sysNum;
                                timeout = vit->_timeout;
                                key = vit->_barrierKey;
                                key = key << 8;
                                key += (vit->_exeCount)++;
                            }
                        #endif
                        #endif
                            break;
                        }
                    }
                    break;
                }
            }
        }

        if(!nextActor.empty())
        {
            bool flag = false;
            {
                boost::shared_lock<boost::shared_mutex> rlock(_utoListMutex);
                //Get next actor parameters and switch
                for(std::vector<UTOActorNode>::iterator it = _utoList.begin(); it != _utoList.end(); it++) {
                    if(it->_actorName == nextActor) {
                        int index = -1;
                    #ifdef EVENT_BARRIER_SYNC
                        if(hasBarrier && !requestBarrierSync(sysNum, key, eventMsg.currentActor, timeout, index)) 
                            return;
                    #endif
                        std::string value;
                        std::map<std::string, std::string> paramMap;
                        for(std::vector<ActorParam>::iterator pit=it->_paramList.begin(); pit!=it->_paramList.end(); pit++) {
                            if(pit->_type == PARAM_TYPE_DISTRIBUTE && index > -1) {  ///< Get distribute parameter
                                distributeParamParse(pit->_value, value, sysNum, index);
                                if(!value.empty()) paramMap[pit->_key] = value;
                            } else if(pit->_type == PARAM_TYPE_NORMAL) {
                                paramMap[pit->_key] = pit->_value;
                            }
                        }
                        //Set actor's params
                        _pActorScheduler->setActorParams(_swarmName, nextActor, paramMap);
                        _pActorScheduler->switchToActor(_swarmName, eventMsg.currentActor, nextActor);
                        flag = true;
                        break;
                    }
                }
            }

            if(flag) {
                pubEventMsgToGSTation(nextActor, "I am starting", time(NULL));
                updateRunningActors(TYPE_REPLACE, eventMsg.currentActor, nextActor);
            #ifdef AUTO_RUN_TASK
                _autoRunCond.notify_one();
            #endif
            }
        }
    }
    else if (eventMsg.type == ACTIVATE_ACTOR_MSG) 
    {
        if(!eventMsg.targetActor.empty())
        {
            bool flag = false;
            {
                boost::shared_lock<boost::shared_mutex> rlock(_utoListMutex);
                //Get next actor parameters and switch
                for(std::vector<UTOActorNode>::iterator it = _utoList.begin(); it != _utoList.end(); it++) {
                    if(it->_actorName == eventMsg.targetActor) {
                        std::map<std::string, std::string> paramMap;
                        for(std::vector<ActorParam>::iterator pit=it->_paramList.begin(); pit!=it->_paramList.end(); pit++) {
                            if(pit->_type == PARAM_TYPE_NORMAL) {
                                paramMap[pit->_key] = pit->_value;
                            }
                        }
                        //Set actor's params
                        _pActorScheduler->setActorParams(_swarmName, eventMsg.targetActor, paramMap);
                        _pActorScheduler->activateActor(_swarmName, eventMsg.targetActor);
                        flag = true;
                        break;
                    }
                }
            }

            if(flag) {
                pubEventMsgToGSTation(eventMsg.targetActor, "I am starting", time(NULL));
                updateRunningActors(TYPE_ADD, "", eventMsg.targetActor);
            }
        }
    }
    else if (eventMsg.type == START_TASK_MSG)  ///< Initial actors will be synchronized with barrierKey(0) before starting
    {
        bool flag;
        int index = -1;
    #ifdef EVENT_BARRIER_SYNC
        // int barrierkey = INITIAL_ACTOR_SYNC_KEY;
        // int keyCircle = 0;
    #endif
        boost::shared_lock<boost::shared_mutex> rlock(_utoListMutex);
        ///< Check if the swarm is valid
        if(eventMsg.currentActor != _swarmName) {
            ROS_WARN("[actor_state_machine] the task file of swarm %s hasn't received yet!", eventMsg.currentActor.c_str());
            return;
        }
        ///< Check if the swarm needs to start
        if(_swarmStarted.count(_swarmName) > 0 && _swarmStarted[_swarmName]) {
            ROS_WARN("[actor_state_machine] the swarm %s is already started!", eventMsg.currentActor.c_str());
            return;
        }
        ///< Setting the current swarm started
        _swarmStarted[_swarmName] = true;
        for (std::vector<std::pair<int, std::string> >::iterator vit = _swarmNameToInitialActors[_swarmName].begin(); vit != _swarmNameToInitialActors[_swarmName].end(); vit++) {
            flag = false;
            //Get actor parameters and switch
            for(std::vector<UTOActorNode>::iterator it = _utoList.begin(); it != _utoList.end(); it++) {
                if(it->_actorName == vit->second) {
                // #ifdef EVENT_BARRIER_SYNC
                //     barrierkey = INITIAL_ACTOR_SYNC_KEY;
                //     barrierkey = barrierkey << 8;
                //     barrierkey += keyCircle;
                //     if(index < 0 && !requestBarrierSync(vit->first, barrierkey, vit->second, DEFAULT_SUSPEND_DURATION, index))
                //         return;
                //     keyCircle++;
                // #endif
                    std::string value;
                    std::map<std::string, std::string> paramMap;
                    for(std::vector<ActorParam>::iterator pit=it->_paramList.begin(); pit!=it->_paramList.end(); pit++) {
                        if(pit->_type == PARAM_TYPE_DISTRIBUTE && index > -1) {  ///< Get distribute parameter
                            distributeParamParse(pit->_value, value, vit->first, index);
                            if(!value.empty()) paramMap[pit->_key] = value;
                        } else if(pit->_type == PARAM_TYPE_NORMAL) {
                            paramMap[pit->_key] = pit->_value;
                        }
                    }
                    //Set actor's params
                    _pActorScheduler->setActorParams(_swarmName, vit->second, paramMap);
                    _pActorScheduler->activateActor(_swarmName, vit->second);
                    flag = true;
                    break;
                }
            }

            if(flag) {
                rlock.unlock();
                pubEventMsgToGSTation(vit->second, "I am starting", time(NULL));
                updateRunningActors(TYPE_ADD, "", vit->second);
            #ifdef AUTO_RUN_TASK
                _autoRunCond.notify_one();
            #endif
                rlock.lock();
            }
        }
    }
    else if(eventMsg.type == PAUSE_ACTOR_MSG) {
        if(!isActorAvailable(eventMsg.currentActor))
            return;
        _pActorScheduler->pauseActor(_swarmName, eventMsg.currentActor);
        pubEventMsgToGSTation(eventMsg.currentActor, "I am pausing", time(NULL));
        updateRunningActors(TYPE_DELETE, eventMsg.currentActor);
    }
    else if(eventMsg.type == SWITCH_ACTOR_MSG) {
        if(!isActorAvailable(eventMsg.currentActor))
            return;
        if(!eventMsg.targetActor.empty()) {
            bool flag = false;
            {
                boost::shared_lock<boost::shared_mutex> rlock(_utoListMutex);
                //Get next actor parameters and switch
                for(std::vector<UTOActorNode>::iterator it = _utoList.begin(); it != _utoList.end(); it++) {
                    if(it->_actorName == eventMsg.targetActor) {
                        std::map<std::string, std::string> paramMap;
                        for(std::vector<ActorParam>::iterator pit=it->_paramList.begin(); pit!=it->_paramList.end(); pit++) {
                            if(pit->_type == PARAM_TYPE_NORMAL) {
                                paramMap[pit->_key] = pit->_value;
                            }
                        }
                        //Set actor's params
                        _pActorScheduler->setActorParams(_swarmName, eventMsg.targetActor, paramMap);
                        _pActorScheduler->switchToActor(_swarmName, eventMsg.currentActor, eventMsg.targetActor);
                        flag = true;
                        break;
                    }
                }
            }

            if(flag) {
                std::string str = "I am switching to " + eventMsg.targetActor;
                pubEventMsgToGSTation(eventMsg.currentActor, str, time(NULL));
                updateRunningActors(TYPE_REPLACE, eventMsg.currentActor, eventMsg.targetActor);
            }
        }
    }
}

bool ActorStateMachine::isActorAvailable(const std::string &anActorName) {
    boost::shared_lock<boost::shared_mutex> lock(_runMutex);
    std::vector<std::string>::iterator runit = find(_utoRunningActorsList.begin(), _utoRunningActorsList.end(), anActorName);
    if(runit == _utoRunningActorsList.end()) {
        return false;
    }
    return true;
}

bool ActorStateMachine::requestBarrierSync(int aSyncNum, short aKey, const std::string &anActorName, short timeout, int &anIndex) {
    bool rtn = true;
    ROS_INFO("[actor_state_machine] actor synchronization num is %d, key is %d", aSyncNum, aKey);
    // Master controls synchronization
    GlobalBarrierKey barrier = GlobalBarrierKey(aKey);
    BarrierResult result = actorDataDistributeBarrierApi(barrier, anActorName, aSyncNum, timeout, anIndex);
    if(result == YES) {
        anIndex--;  ///< The returned value of barrier index is positive number
        ROS_INFO("[actor_state_machine] actor %s completes synchronization on platform %ld, key is %d", anActorName.c_str(), _platformID, aKey);
    } else if(result == TIMEOUT) {
        ROS_INFO("[actor_state_machine] actor %s waits timeout on platform %ld, key is %d", anActorName.c_str(), _platformID, aKey);
    } else if(result == OPTIONALYES) {
        ROS_INFO("[actor_state_machine] actor %s gets optinal YES on platform %ld, key is %d", anActorName.c_str(), _platformID, aKey);
        rtn = false;
    } else{
        ROS_INFO("[actor_state_machine] actor %s result %d on platform %ld, key is %d", anActorName.c_str(), result, _platformID, aKey);
        rtn = false;
    }
    return rtn;
}


void ActorStateMachine::pubEventMsgToGSTation(const std::string &anActorName, const std::string &anEvent, const time_t &aStamp) {
#ifdef EVENT_GS_RTPS_COMM
    // Publish event transition message to gstation on FastRTPS
    UTOEventTransition transition;
    transition.currentActor(anActorName);
    transition.eventName(anEvent);
    transition.syncStatus(-1);
    transition.timestamp(aStamp);
    transition.robotId(_platformID);  
    _rtpEventPub->publish(transition);
#else
    // Publish event transition message to gstation on ros
    actor_msgs::UTOEventTransition rosEvent;
    rosEvent.currentActor = anActorName;
    rosEvent.eventName = anEvent;
    rosEvent.syncStatus = -1;
    rosEvent.timestamp = aStamp;
    rosEvent.robotId = _platformID;
    _eventSyncPub.publish(rosEvent);
#endif
}

void ActorStateMachine::distributeParamParse(std::string aSourceStr, std::string& aDestStr, int aSysNum, int anIndex) {
    aDestStr.clear();
    std::vector<std::string> vc;
    char splitChar = ';';
    char arithmeticChar = '(';
    std::size_t begpos, endpos;
    begpos = endpos = 0;
    std::string substr, subpre;
    if(aSourceStr.find_first_of(splitChar) != std::string::npos) {  ///< Find ';' to split parameter
        begpos = aSourceStr.find_first_not_of(splitChar, begpos);
        while (begpos != std::string::npos) {
            endpos = aSourceStr.find_first_of(splitChar, begpos);
            if(endpos == std::string::npos) {
                substr = aSourceStr.substr(begpos);
            } else {
                substr = aSourceStr.substr(begpos, endpos-begpos);
            }
            if(!substr.empty()) {
                vc.push_back(substr);
            }
            begpos = aSourceStr.find_first_not_of(splitChar, endpos);
            if(vc.size() > anIndex) break;
        }
        
        if(vc.size() <= anIndex) {
            ROS_INFO("Distribute parameter count is smaller than the syschronization number");
            return;
        }
    }
    else if(aSourceStr.find_first_of(arithmeticChar) != std::string::npos) {
        splitChar = ',';
        begpos = aSourceStr.find_first_not_of(arithmeticChar, begpos);
        subpre = aSourceStr;
        aSourceStr = aSourceStr.substr(begpos, aSourceStr.size()-begpos-1);  ///< aSourceStr removes the brackets
        begpos = 0;
        int startv, dv, endv;
        startv = dv = 0;
        endv = -1;
        for(int i=0; (begpos != std::string::npos) && (i<3); i++) {
            endpos = aSourceStr.find_first_of(splitChar, begpos);
            if(endpos == std::string::npos) {
                substr = aSourceStr.substr(begpos);
            } else {
                substr = aSourceStr.substr(begpos, endpos-begpos);
            }
            if(!substr.empty() && isAllNum(substr)) {
                if(i==0) startv = atoi(substr.c_str());
                else if(i==1) dv = atoi(substr.c_str());
                else endv = atoi(substr.c_str());
            }
            begpos = aSourceStr.find_first_not_of(splitChar, endpos);
        }
        if(begpos != std::string::npos) {
            ROS_WARN("The actor parameter %s is incorrectly formatted", subpre.c_str());
            return;
        }

        for(int j=0; j<=anIndex; j++) {
            vc.push_back(std::to_string(startv));
            startv += dv;
            if(endv != -1) {
                if(dv > 0)
                    startv = startv > endv ? endv : startv;
                else if(dv < 0)
                    startv = startv < endv ? endv : startv;
            }   
        }
    }

    aDestStr = vc[anIndex];
}

bool ActorStateMachine::isAllNum(const std::string &aSourceStr) {
    bool flag = true;
    for(int i=0; i<aSourceStr.size(); i++) {
        if(!isdigit(aSourceStr[i]) && aSourceStr[i] != '-') {
            flag = false;
            break;
        }
    }
    return flag;
}


template<class M, class T>
ros::Subscriber ActorStateMachine::rosSubscribe(const std::string& topic, uint32_t queue_size, void(T::*fp)(const boost::shared_ptr<M const>&), T* obj, const ros::TransportHints& transport_hints) {
    ros::SubscribeOptions ops;
    ops.template init<M>(topic, queue_size, boost::bind(fp, obj, _1));
    ops.transport_hints = transport_hints;
    ops.callback_queue = _rosCallbackQueue->_callbackQueue.get();
    return _handle.subscribe(ops);
}

void ActorStateMachine::updateRunningActors(const EOperateType type, const std::string& aSrc, const std::string& aTarget) {
    std::vector<std::string>::iterator runit;
    switch (type)
    {
    case TYPE_ADD:
        {
            boost::lock_guard<boost::shared_mutex> lck(_runMutex);
            runit = find(_utoRunningActorsList.begin(), _utoRunningActorsList.end(), aTarget);
            if(runit == _utoRunningActorsList.end())
                _utoRunningActorsList.push_back(aTarget);
            break;
        }
    case TYPE_DELETE:
        {
            boost::lock_guard<boost::shared_mutex> lck(_runMutex);
            runit = find(_utoRunningActorsList.begin(), _utoRunningActorsList.end(), aSrc);
            if(runit != _utoRunningActorsList.end())
                _utoRunningActorsList.erase(runit);
            break;
        }
    case TYPE_REPLACE:
        {
            updateRunningActors(TYPE_DELETE, aSrc, aTarget);
            updateRunningActors(TYPE_ADD, aSrc, aTarget);
            break;
        }
    case TYPE_CLEAR:
        {
            boost::lock_guard<boost::shared_mutex> lck(_runMutex);
            _utoRunningActorsList.clear();
            break;
        }
    default:
        break;
    }
}