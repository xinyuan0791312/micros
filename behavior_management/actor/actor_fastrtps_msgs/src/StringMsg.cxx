// Copyright 2016 Proyectos y Sistemas de Mantenimiento SL (eProsima).
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/*!
 * @file StringMsg.cpp
 * This source file contains the definition of the described types in the IDL file.
 *
 * This file was generated by the tool gen.
 */

#ifdef _WIN32
// Remove linker warning LNK4221 on Visual Studio
namespace { char dummy; }
#endif

#include "StringMsg.h"
#include <fastcdr/Cdr.h>

#include <fastcdr/exceptions/BadParamException.h>
using namespace eprosima::fastcdr::exception;

#include <utility>

StringMsg::StringMsg()
{
    m_sendID = 0;
    m_destID = 0;
    m_cmdNo = 0;
    m_reply = 0;


}

StringMsg::~StringMsg()
{





}

StringMsg::StringMsg(const StringMsg &x)
{
    m_sendID = x.m_sendID;
    m_destID = x.m_destID;
    m_cmdNo = x.m_cmdNo;
    m_reply = x.m_reply;
    m_data = x.m_data;
}

StringMsg::StringMsg(StringMsg &&x)
{
    m_sendID = x.m_sendID;
    m_destID = x.m_destID;
    m_cmdNo = x.m_cmdNo;
    m_reply = x.m_reply;
    m_data = std::move(x.m_data);
}

StringMsg& StringMsg::operator=(const StringMsg &x)
{

    m_sendID = x.m_sendID;
    m_destID = x.m_destID;
    m_cmdNo = x.m_cmdNo;
    m_reply = x.m_reply;
    m_data = x.m_data;

    return *this;
}

StringMsg& StringMsg::operator=(StringMsg &&x)
{

    m_sendID = x.m_sendID;
    m_destID = x.m_destID;
    m_cmdNo = x.m_cmdNo;
    m_reply = x.m_reply;
    m_data = std::move(x.m_data);

    return *this;
}

size_t StringMsg::getMaxCdrSerializedSize(size_t current_alignment)
{
    size_t initial_alignment = current_alignment;


    current_alignment += 2 + eprosima::fastcdr::Cdr::alignment(current_alignment, 2);


    current_alignment += 2 + eprosima::fastcdr::Cdr::alignment(current_alignment, 2);


    current_alignment += 8 + eprosima::fastcdr::Cdr::alignment(current_alignment, 8);


    current_alignment += 2 + eprosima::fastcdr::Cdr::alignment(current_alignment, 2);


    current_alignment += 4 + eprosima::fastcdr::Cdr::alignment(current_alignment, 4) + 65536 + 1;


    return current_alignment - initial_alignment;
}

size_t StringMsg::getCdrSerializedSize(const StringMsg& data, size_t current_alignment)
{
    (void)data;
    size_t initial_alignment = current_alignment;


    current_alignment += 2 + eprosima::fastcdr::Cdr::alignment(current_alignment, 2);


    current_alignment += 2 + eprosima::fastcdr::Cdr::alignment(current_alignment, 2);


    current_alignment += 8 + eprosima::fastcdr::Cdr::alignment(current_alignment, 8);


    current_alignment += 2 + eprosima::fastcdr::Cdr::alignment(current_alignment, 2);


    current_alignment += 4 + eprosima::fastcdr::Cdr::alignment(current_alignment, 4) + data.data().size() + 1;


    return current_alignment - initial_alignment;
}

void StringMsg::serialize(eprosima::fastcdr::Cdr &scdr) const
{

    scdr << m_sendID;
    scdr << m_destID;
    scdr << m_cmdNo;
    scdr << m_reply;
    scdr << m_data;
}

void StringMsg::deserialize(eprosima::fastcdr::Cdr &dcdr)
{

    dcdr >> m_sendID;
    dcdr >> m_destID;
    dcdr >> m_cmdNo;
    dcdr >> m_reply;
    dcdr >> m_data;
}

/*!
 * @brief This function sets a value in member sendID
 * @param _sendID New value for member sendID
 */
void StringMsg::sendID(int16_t _sendID)
{
m_sendID = _sendID;
}

/*!
 * @brief This function returns the value of member sendID
 * @return Value of member sendID
 */
int16_t StringMsg::sendID() const
{
    return m_sendID;
}

/*!
 * @brief This function returns a reference to member sendID
 * @return Reference to member sendID
 */
int16_t& StringMsg::sendID()
{
    return m_sendID;
}
/*!
 * @brief This function sets a value in member destID
 * @param _destID New value for member destID
 */
void StringMsg::destID(int16_t _destID)
{
m_destID = _destID;
}

/*!
 * @brief This function returns the value of member destID
 * @return Value of member destID
 */
int16_t StringMsg::destID() const
{
    return m_destID;
}

/*!
 * @brief This function returns a reference to member destID
 * @return Reference to member destID
 */
int16_t& StringMsg::destID()
{
    return m_destID;
}
/*!
 * @brief This function sets a value in member cmdNo
 * @param _cmdNo New value for member cmdNo
 */
void StringMsg::cmdNo(int64_t _cmdNo)
{
m_cmdNo = _cmdNo;
}

/*!
 * @brief This function returns the value of member cmdNo
 * @return Value of member cmdNo
 */
int64_t StringMsg::cmdNo() const
{
    return m_cmdNo;
}

/*!
 * @brief This function returns a reference to member cmdNo
 * @return Reference to member cmdNo
 */
int64_t& StringMsg::cmdNo()
{
    return m_cmdNo;
}
/*!
 * @brief This function sets a value in member reply
 * @param _reply New value for member reply
 */
void StringMsg::reply(int16_t _reply)
{
m_reply = _reply;
}

/*!
 * @brief This function returns the value of member reply
 * @return Value of member reply
 */
int16_t StringMsg::reply() const
{
    return m_reply;
}

/*!
 * @brief This function returns a reference to member reply
 * @return Reference to member reply
 */
int16_t& StringMsg::reply()
{
    return m_reply;
}
/*!
 * @brief This function copies the value in member data
 * @param _data New value to be copied in member data
 */
void StringMsg::data(const std::string &_data)
{
m_data = _data;
}

/*!
 * @brief This function moves the value in member data
 * @param _data New value to be moved in member data
 */
void StringMsg::data(std::string &&_data)
{
m_data = std::move(_data);
}

/*!
 * @brief This function returns a constant reference to member data
 * @return Constant reference to member data
 */
const std::string& StringMsg::data() const
{
    return m_data;
}

/*!
 * @brief This function returns a reference to member data
 * @return Reference to member data
 */
std::string& StringMsg::data()
{
    return m_data;
}

size_t StringMsg::getKeyMaxCdrSerializedSize(size_t current_alignment)
{
	size_t current_align = current_alignment;








    return current_align;
}

bool StringMsg::isKeyDefined()
{
   return false;
}

void StringMsg::serializeKey(eprosima::fastcdr::Cdr &scdr) const
{
	(void) scdr;
	 
	 
	 
	 
	 
}
