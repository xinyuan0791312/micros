// Copyright 2016 Proyectos y Sistemas de Mantenimiento SL (eProsima).
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/*!
 * @file MasterCmd.h
 * This header file contains the declaration of the described types in the IDL file.
 *
 * This file was generated by the tool gen.
 */

#ifndef _MASTERCMD_H_
#define _MASTERCMD_H_

// TODO Poner en el contexto.

#include <stdint.h>
#include <array>
#include <string>
#include <vector>
#include <map>
#include <bitset>

#if defined(_WIN32)
#if defined(EPROSIMA_USER_DLL_EXPORT)
#define eProsima_user_DllExport __declspec( dllexport )
#else
#define eProsima_user_DllExport
#endif
#else
#define eProsima_user_DllExport
#endif

#if defined(_WIN32)
#if defined(EPROSIMA_USER_DLL_EXPORT)
#if defined(MasterCmd_SOURCE)
#define MasterCmd_DllAPI __declspec( dllexport )
#else
#define MasterCmd_DllAPI __declspec( dllimport )
#endif // MasterCmd_SOURCE
#else
#define MasterCmd_DllAPI
#endif
#else
#define MasterCmd_DllAPI
#endif // _WIN32

namespace eprosima
{
    namespace fastcdr
    {
        class Cdr;
    }
}

/*!
 * @brief This class represents the structure MasterCmd defined by the user in the IDL file.
 * @ingroup MASTERCMD
 */
class MasterCmd
{
public:

    /*!
     * @brief Default constructor.
     */
    eProsima_user_DllExport MasterCmd();

    /*!
     * @brief Default destructor.
     */
    eProsima_user_DllExport virtual ~MasterCmd();

    /*!
     * @brief Copy constructor.
     * @param x Reference to the object MasterCmd that will be copied.
     */
    eProsima_user_DllExport MasterCmd(const MasterCmd &x);

    /*!
     * @brief Move constructor.
     * @param x Reference to the object MasterCmd that will be copied.
     */
    eProsima_user_DllExport MasterCmd(MasterCmd &&x);

    /*!
     * @brief Copy assignment.
     * @param x Reference to the object MasterCmd that will be copied.
     */
    eProsima_user_DllExport MasterCmd& operator=(const MasterCmd &x);

    /*!
     * @brief Move assignment.
     * @param x Reference to the object MasterCmd that will be copied.
     */
    eProsima_user_DllExport MasterCmd& operator=(MasterCmd &&x);

    /*!
     * @brief This function sets a value in member sendID
     * @param _sendID New value for member sendID
     */
    eProsima_user_DllExport void sendID(int16_t _sendID);

    /*!
     * @brief This function returns the value of member sendID
     * @return Value of member sendID
     */
    eProsima_user_DllExport int16_t sendID() const;

    /*!
     * @brief This function returns a reference to member sendID
     * @return Reference to member sendID
     */
    eProsima_user_DllExport int16_t& sendID();
    /*!
     * @brief This function sets a value in member receID
     * @param _receID New value for member receID
     */
    eProsima_user_DllExport void receID(int16_t _receID);

    /*!
     * @brief This function returns the value of member receID
     * @return Value of member receID
     */
    eProsima_user_DllExport int16_t receID() const;

    /*!
     * @brief This function returns a reference to member receID
     * @return Reference to member receID
     */
    eProsima_user_DllExport int16_t& receID();
    /*!
     * @brief This function sets a value in member cmdNo
     * @param _cmdNo New value for member cmdNo
     */
    eProsima_user_DllExport void cmdNo(int16_t _cmdNo);

    /*!
     * @brief This function returns the value of member cmdNo
     * @return Value of member cmdNo
     */
    eProsima_user_DllExport int16_t cmdNo() const;

    /*!
     * @brief This function returns a reference to member cmdNo
     * @return Reference to member cmdNo
     */
    eProsima_user_DllExport int16_t& cmdNo();
    /*!
     * @brief This function sets a value in member type
     * @param _type New value for member type
     */
    eProsima_user_DllExport void type(int16_t _type);

    /*!
     * @brief This function returns the value of member type
     * @return Value of member type
     */
    eProsima_user_DllExport int16_t type() const;

    /*!
     * @brief This function returns a reference to member type
     * @return Reference to member type
     */
    eProsima_user_DllExport int16_t& type();
    /*!
     * @brief This function sets a value in member param1
     * @param _param1 New value for member param1
     */
    eProsima_user_DllExport void param1(int16_t _param1);

    /*!
     * @brief This function returns the value of member param1
     * @return Value of member param1
     */
    eProsima_user_DllExport int16_t param1() const;

    /*!
     * @brief This function returns a reference to member param1
     * @return Reference to member param1
     */
    eProsima_user_DllExport int16_t& param1();
    /*!
     * @brief This function sets a value in member param2
     * @param _param2 New value for member param2
     */
    eProsima_user_DllExport void param2(int16_t _param2);

    /*!
     * @brief This function returns the value of member param2
     * @return Value of member param2
     */
    eProsima_user_DllExport int16_t param2() const;

    /*!
     * @brief This function returns a reference to member param2
     * @return Reference to member param2
     */
    eProsima_user_DllExport int16_t& param2();
    /*!
     * @brief This function sets a value in member param3
     * @param _param3 New value for member param3
     */
    eProsima_user_DllExport void param3(int16_t _param3);

    /*!
     * @brief This function returns the value of member param3
     * @return Value of member param3
     */
    eProsima_user_DllExport int16_t param3() const;

    /*!
     * @brief This function returns a reference to member param3
     * @return Reference to member param3
     */
    eProsima_user_DllExport int16_t& param3();
    /*!
     * @brief This function sets a value in member param4
     * @param _param4 New value for member param4
     */
    eProsima_user_DllExport void param4(int16_t _param4);

    /*!
     * @brief This function returns the value of member param4
     * @return Value of member param4
     */
    eProsima_user_DllExport int16_t param4() const;

    /*!
     * @brief This function returns a reference to member param4
     * @return Reference to member param4
     */
    eProsima_user_DllExport int16_t& param4();
    /*!
     * @brief This function sets a value in member param5
     * @param _param5 New value for member param5
     */
    eProsima_user_DllExport void param5(int16_t _param5);

    /*!
     * @brief This function returns the value of member param5
     * @return Value of member param5
     */
    eProsima_user_DllExport int16_t param5() const;

    /*!
     * @brief This function returns a reference to member param5
     * @return Reference to member param5
     */
    eProsima_user_DllExport int16_t& param5();
    /*!
     * @brief This function copies the value in member param6
     * @param _param6 New value to be copied in member param6
     */
    eProsima_user_DllExport void param6(const std::vector<int16_t> &_param6);

    /*!
     * @brief This function moves the value in member param6
     * @param _param6 New value to be moved in member param6
     */
    eProsima_user_DllExport void param6(std::vector<int16_t> &&_param6);

    /*!
     * @brief This function returns a constant reference to member param6
     * @return Constant reference to member param6
     */
    eProsima_user_DllExport const std::vector<int16_t>& param6() const;

    /*!
     * @brief This function returns a reference to member param6
     * @return Reference to member param6
     */
    eProsima_user_DllExport std::vector<int16_t>& param6();

    /*!
     * @brief This function returns the maximum serialized size of an object
     * depending on the buffer alignment.
     * @param current_alignment Buffer alignment.
     * @return Maximum serialized size.
     */
    eProsima_user_DllExport static size_t getMaxCdrSerializedSize(size_t current_alignment = 0);

    /*!
     * @brief This function returns the serialized size of a data depending on the buffer alignment.
     * @param data Data which is calculated its serialized size.
     * @param current_alignment Buffer alignment.
     * @return Serialized size.
     */
    eProsima_user_DllExport static size_t getCdrSerializedSize(const MasterCmd& data, size_t current_alignment = 0);


    /*!
     * @brief This function serializes an object using CDR serialization.
     * @param cdr CDR serialization object.
     */
    eProsima_user_DllExport void serialize(eprosima::fastcdr::Cdr &cdr) const;

    /*!
     * @brief This function deserializes an object using CDR serialization.
     * @param cdr CDR serialization object.
     */
    eProsima_user_DllExport void deserialize(eprosima::fastcdr::Cdr &cdr);



    /*!
     * @brief This function returns the maximum serialized size of the Key of an object
     * depending on the buffer alignment.
     * @param current_alignment Buffer alignment.
     * @return Maximum serialized size.
     */
    eProsima_user_DllExport static size_t getKeyMaxCdrSerializedSize(size_t current_alignment = 0);

    /*!
     * @brief This function tells you if the Key has been defined for this type
     */
    eProsima_user_DllExport static bool isKeyDefined();

    /*!
     * @brief This function serializes the key members of an object using CDR serialization.
     * @param cdr CDR serialization object.
     */
    eProsima_user_DllExport void serializeKey(eprosima::fastcdr::Cdr &cdr) const;

 eProsima_user_DllExport static std::string getName() { return "MasterCmd"; } 
private:
    int16_t m_sendID;
    int16_t m_receID;
    int16_t m_cmdNo;
    int16_t m_type;
    int16_t m_param1;
    int16_t m_param2;
    int16_t m_param3;
    int16_t m_param4;
    int16_t m_param5;
    std::vector<int16_t> m_param6;
};

#endif // _MASTERCMD_H_