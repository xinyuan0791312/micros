/* ==================================================================
*
* Copyright (c) 2016-2019, micROS Group, NIIDT, TAIIC, HPCL.
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions 
* are met: 
*
* 1. Redistributions of source code must retain the above copyright 
*     notice, this list of conditions and the following disclaimer. 
* 2. Redistributions in binary form must reproduce the above copyright 
*     notice, this list of conditions and the following disclaimer in the 
*     documentation and/or other materials provided with the 
*     distribution. 
* 3. All advertising materials mentioning features or use of this software 
*     must display the following acknowledgement: 
*     This product includes software developed by the micROS Group. and 
*     its contributors. 
* 4. Neither the name of the Group nor the names of its contributors may 
*     be used to endorse or promote products derived from this software 
*     without specific prior written permission. 
*
* THIS SOFTWARE IS PROVIDED BY MICROS,GROUP AND CONTRIBUTORS
* ''AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT
* NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
* FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL 
* THE MICROS,GROUP OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS 
* OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) 
* HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
* OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN 
* IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
* ===================================================================
*
* Author: Bin Di, Chunlian Fu, Junling Gao, Ruihao Li, Wei Yi.
*         (in alphabetical order)
*
*/

#include "general_plugin/general_plugin.h"
#include "actor_msgs/UTOEvent.h"

#define MIN_SLEEP_DURATION 10000 // 10ms

namespace general_bus {
	boost::mutex GeneralPlugin::_rosCreatePubSubMutex;
	bool GeneralPlugin::initialize(int64_t aActorID,std::string aPluginName,boost::shared_ptr<ActorCallbackQueue> aCallbackQueue){
		//_actorName=aActorName;
		_actorID = aActorID;
		_name = aPluginName;
		_state = ACTOR_STATE_PAUSE;
		_duration = 1;
		_callbackQueue = aCallbackQueue;
		// Publish event msg to actor state machine
		_pub = pluginAdvertise<actor_msgs::UTOEvent>("uto_event_msg", 10000);
	}

	void GeneralPlugin::pause() {
		boost::mutex::scoped_lock lock(_mutex);
		if(_state==ACTOR_STATE_FINISH){
			ROS_INFO("[General Plugin] Actor %ld, plugin %s Stoped",_actorID, _name.c_str());
			return;
		}
		if(_state==ACTOR_STATE_PAUSE){
			ROS_INFO("[General Plugin] Actor %ld, plugin %s Paused",_actorID,_name.c_str());
			return;

		}
		setState(ACTOR_STATE_PAUSE);
		_cond_pause.wait(lock);
		ROS_INFO("[General Plugin] Actor %ld, plugin %s Paused",_actorID,_name.c_str());
	}

	void GeneralPlugin::resume() {
		boost::mutex::scoped_lock lock(_mutex);	
		setState(ACTOR_STATE_RUNNING);
		_cond.notify_one();
	}

	void GeneralPlugin::stop(){
		boost::mutex::scoped_lock lock(_mutex);
		if(_state==ACTOR_STATE_FINISH){
			ROS_INFO("[General Plugin] Actor %ld, plugin %s Stoped",_actorID,_name.c_str());
			return;
		}
		setState(ACTOR_STATE_FINISH);
		_cond_stop.wait(lock);
		ROS_INFO("[General Plugin] Actor %ld, plugin %s Stoped",_actorID,_name.c_str());
	}

	/*  void GeneralPlugin::terminate() {
		{
			boost::mutex::scoped_lock lock(_mutex);
			setState(ACTOR_STATE_FINISH); //use constants like ACTOR_STATE_STOP;
			_cond_pause.notify_one();
			//the same as _cond_pause
			_cond_stop.notify_one();
		}
		//notify the schedule
		ROS_INFO("[General Plugin] Actor actor ID %ld ,plugin %s call terminate, before scheduler lock",_actorID,_name.c_str());
		/*
		if((_pMutex==NULL)||(!_pMutex)){
			ROS_ERROR("[General Plugin] pMutex = NULL");
		}
		boost::mutex::scoped_lock scheLock(*_pMutex);
		_pCond->notify_one(); */
		/*
		quitActor(_actorID);
		
		ROS_INFO("[General Plugin] Actor %ld , plugin %s Terminated",_actorID,_name.c_str());
		pthread_exit(0);	
	}  
	*/

	void GeneralPlugin::insertInterruptablePoint(){
		boost::mutex::scoped_lock lock(_mutex);
		if (_state==ACTOR_STATE_PAUSE) { 
			_cond_pause.notify_one();
			_cond.wait(lock);
		}
		if(_state==ACTOR_STATE_FINISH){
			_cond_stop.notify_one();
			pthread_exit(0);
		}
		
	}

	void GeneralPlugin::pubEventMsg(const std::string &anEventName){
		std::string actor;
		getActorName(_actorID, actor);
		actor_msgs::UTOEvent event;
		event.type = SWITCH_EVENT_MSG;
		event.currentActor = actor;
		event.eventName = anEventName;
		_pub.publish(event);
	}
	
	// This method will be called to transmit params to plugins before plugins starting
	void GeneralPlugin::setParams(const std::map<std::string, std::string> &aMap) {
		if(aMap.empty()) return;
		_paramsMap.insert(aMap.begin(), aMap.end());
	}

	std::string GeneralPlugin::getParam(const std::string &paramName){
		if(paramName.empty() || _paramsMap.count(paramName)==0) return "";
		return _paramsMap[paramName];
	}

	void GeneralPlugin::pluginSleep(const int aSeconds){
		pluginUSleep(aSeconds * 1000 * 1000);
	}

	void GeneralPlugin::pluginUSleep(const int aUSeconds){
		boost::posix_time::ptime time_start = boost::posix_time::microsec_clock::universal_time(); 
		boost::posix_time::ptime time_now; 
		int leftUSconds = MIN_SLEEP_DURATION; 
		while(true){
			GOON_OR_RETURN;
			time_now = boost::posix_time::microsec_clock::universal_time(); 
			leftUSconds = std::min(MIN_SLEEP_DURATION, aUSeconds - (int)((time_now - time_start).ticks()));
			if(leftUSconds <= 0) break;

			usleep(leftUSconds);
		}
	}
}

