// Copyright 2016 Proyectos y Sistemas de Mantenimiento SL (eProsima).
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/**
 * @file main_ClientServerTest.cpp
 *
 */



#include <stdio.h>
#include <string>
#include <iostream>
#include <iomanip>
#include <bitset>
#include <cstdint>
#include <sstream>

#include "fastrtps/utils/eClock.h"
#include "fastrtps/log/Log.h"

#include "fastrtps/Domain.h"

#include "RTPSServer.h"
#include "RTPSClient.h"
#include "AddRequest.h"
#include "AddReply.h"
#include "MultiplyRequest.h"
#include "MultiplyReply.h"


#include "micROSRTPSExt.h"

using namespace eprosima;
using namespace fastrtps;
using std::cout;
using std::endl;

#if defined(__LITTLE_ENDIAN__)
const Endianness_t DEFAULT_ENDIAN = LITTLEEND;
#elif defined (__BIG_ENDIAN__)
const Endianness_t DEFAULT_ENDIAN = BIGEND;
#endif

#if defined(_WIN32)
#define COPYSTR strcpy_s
#else
#define COPYSTR strcpy
#endif

enum E_SIDE
{
	CLIENT,
	SERVER
};

void addCallback(AddRequest& aRequest,AddReply& aReply) {
	aReply.result(aRequest.a()+aRequest.b());
}

void multiplyCallback(MultiplyRequest& aRequest,MultiplyReply& aReply) {
	aReply.result(aRequest.a()*aRequest.b()*aRequest.c());
}

int main(int argc, char** argv){
	cout << "Starting "<< endl;
	E_SIDE side;
	int samples = 10000;
	if(argc > 1)
	{
		if(strcmp(argv[1],"client")==0)
			side = CLIENT;
		else if(strcmp(argv[1],"server")==0)
			side = SERVER;
		else
		{
			cout << "Argument 1 needs to be client OR server"<<endl;
			return 0;
		}
		if(argc > 3)
		{
			std::istringstream iss( argv[3] );
			if (!(iss >> samples))
			{
				cout << "Problem reading samples number,using default 10000 samples "<< endl;
				samples = 10000;
			}
		}
	}
	else
	{
		cout << "Client Server Test needs 1 arguments: (client/server)"<<endl;
		return 0;
	}

	micROS::init();

	if(side == SERVER)
	{
		RTPSServer<AddRequest,AddReply> server1("AddService",addCallback);
		RTPSServer<MultiplyRequest,MultiplyReply> server2("MultiplyService",multiplyCallback);
		server1.init();
		server2.init();
		while (true) {
			sleep(1);
		}
	}
	if(side == CLIENT)
	{
		RTPSClient<AddRequest,AddReply> client1("AddService");
		RTPSClient<MultiplyRequest,MultiplyReply> client2("MultiplyService");
		client1.init();
		client2.init();
		while(!client1.isReady() || !client2.isReady())
    		{
        		eClock::my_sleep(100);
   		}
    		for(int i=0;i<10;i++)
    		{
			AddRequest request1;
			request1.a(i);request1.b(i);
			AddReply reply1;
        		if(client1.call(request1,reply1)) 
				cout<<"Add Result="<<reply1.result()<<endl;
			else 
				cout<<"Add call service failed"<<endl;
  			MultiplyRequest request2;
			request2.a(i);request2.b(i);request2.c(i);
			MultiplyReply reply2;
        		if(client2.call(request2,reply2)) 
				cout<<"Multiply Result="<<reply2.result()<<endl;
			else 
				cout<<"Multiply call service failed"<<endl;
			sleep(1);
		}
	}

	
	cout << "EVERYTHING STOPPED FINE"<<endl;
	
	micROS::finish();
	return 0;
}




