/* ==================================================================
*
* Copyright (c) 2016-2019, micROS Group, NIIDT, TAIIC, HPCL.
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions 
* are met: 
*
* 1. Redistributions of source code must retain the above copyright 
*     notice, this list of conditions and the following disclaimer. 
* 2. Redistributions in binary form must reproduce the above copyright 
*     notice, this list of conditions and the following disclaimer in the 
*     documentation and/or other materials provided with the 
*     distribution. 
* 3. All advertising materials mentioning features or use of this software 
*     must display the following acknowledgement: 
*     This product includes software developed by the micROS Group. and 
*     its contributors. 
* 4. Neither the name of the Group nor the names of its contributors may 
*     be used to endorse or promote products derived from this software 
*     without specific prior written permission. 
*
* THIS SOFTWARE IS PROVIDED BY MICROS,GROUP AND CONTRIBUTORS
* ''AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT
* NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
* FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL 
* THE MICROS,GROUP OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS 
* OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) 
* HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
* OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN 
* IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
* ===================================================================
*
* Author: Bin Di, Chunlian Fu, Junling Gao, Ruihao Li, Wei Yi.
*         (in alphabetical order)
*
*/

#include "test_queue_pub_plugin/test_queue_pub_plugin.h"
#include <pluginlib/class_list_macros.h>
#include "ros/ros.h"
#include "std_msgs/String.h"
//#include "test3_plugin/actor_api.h"

PLUGINLIB_EXPORT_CLASS(general_bus::TestQueuePubPlugin,general_bus::GeneralPlugin)

namespace general_bus {
	void TestQueuePubPlugin::connectCallback(const ros::SingleSubscriberPublisher& pub){
			ROS_INFO("[PubPlugin] connect from %s", pub.getSubscriberName().c_str());
	}

	void TestQueuePubPlugin::disconnectCallback(const ros::SingleSubscriberPublisher& pub){
		ROS_INFO("[PubPlugin] disconnect from %s", pub.getSubscriberName().c_str());
	}

	void TestQueuePubPlugin::start(){
		GOON_OR_RETURN;
		
		std::string actorName;
		getActorName(_actorID,actorName);
		ROS_INFO("[PubPlugin] %s start", actorName.c_str());

		// ros create pub, this is depecated.
		/*ros::NodeHandle n;
		ros::Publisher chatter_pub = n.advertise<std_msgs::String>("/chatter", 1000,
		 boost::bind(&TestQueuePubPlugin::connectCallback, this, _1),
		 boost::bind(&TestQueuePubPlugin::disconnectCallback, this, _1)
		);*/

		// user create ros publisher, test the method in general_plugin
		ros::Publisher chatter_pub = pluginAdvertise<std_msgs::String>("/chatter", 1000);

		ros::Rate loop_rate(1);
		int count = 0;
  		while (ros::ok()){
			GOON_OR_RETURN;
			std_msgs::String msg;

    		std::stringstream ss;
    		ss << "Pushlish: " << count;
    		msg.data = ss.str();
    
    		ROS_INFO("[PubPlugin]subNum%d, %s", chatter_pub.getNumSubscribers(), msg.data.c_str());
			chatter_pub.publish(msg);

    		//ros::spinOnce();

    		loop_rate.sleep();
    		++count;
			if(count >=100)sleep(20);
		}

		GeneralPlugin::start();
	}

};
