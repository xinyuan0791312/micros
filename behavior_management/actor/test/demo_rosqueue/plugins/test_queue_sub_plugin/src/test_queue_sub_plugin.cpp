/* ==================================================================
*
* Copyright (c) 2016-2019, micROS Group, NIIDT, TAIIC, HPCL.
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions 
* are met: 
*
* 1. Redistributions of source code must retain the above copyright 
*     notice, this list of conditions and the following disclaimer. 
* 2. Redistributions in binary form must reproduce the above copyright 
*     notice, this list of conditions and the following disclaimer in the 
*     documentation and/or other materials provided with the 
*     distribution. 
* 3. All advertising materials mentioning features or use of this software 
*     must display the following acknowledgement: 
*     This product includes software developed by the micROS Group. and 
*     its contributors. 
* 4. Neither the name of the Group nor the names of its contributors may 
*     be used to endorse or promote products derived from this software 
*     without specific prior written permission. 
*
* THIS SOFTWARE IS PROVIDED BY MICROS,GROUP AND CONTRIBUTORS
* ''AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT
* NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
* FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL 
* THE MICROS,GROUP OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS 
* OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) 
* HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
* OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN 
* IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
* ===================================================================
*
* Author: Bin Di, Chunlian Fu, Junling Gao, Ruihao Li, Wei Yi.
*         (in alphabetical order)
*
*/

#include "test_queue_sub_plugin/test_queue_sub_plugin.h"
#include <pluginlib/class_list_macros.h>
#include "ros/ros.h"
#include <iostream>
#include <XmlRpcValue.h>
#include <ros/publication.h>
#include <ros/topic_manager.h>
//#include "test3_plugin/actor_api.h"

PLUGINLIB_EXPORT_CLASS(general_bus::TestQueueSubPlugin,general_bus::GeneralPlugin)

namespace general_bus {
	void chatterCallback_g1(const std_msgs::String::ConstPtr& msg){
		ROS_INFO("[SubPlugin] global1 chatterCallback:heard: [%s]",msg->data.c_str());
	}

	void chatterCallback_g2(const std_msgs::String::ConstPtr& msg){
		ROS_INFO("[SubPlugin] global2 chatterCallback:heard: [%s]",msg->data.c_str());
	}

	void TestQueueSubPlugin::chatterCallback(const std_msgs::String::ConstPtr& msg){
		ROS_INFO("[SubPlugin] member1 chatterCallback:%ld heard: [%s]", _actorID,msg->data.c_str());
	}

	void TestQueueSubPlugin::chatterCallback2(const std_msgs::String::ConstPtr& msg){
		ROS_INFO("[SubPlugin] member2 chatterCallback:%ld heard: [%s]", _actorID,msg->data.c_str());
	}

	void TestQueueSubPlugin::start(){
		std::string actorName;
		getActorName(_actorID,actorName);
		ROS_INFO("[SubPlugin] %s start", actorName.c_str());
		GOON_OR_RETURN;

		// depecated, used to test ros bug
		/*ros::NodeHandle n;
		ros::SubscribeOptions ops_1;
		ops_1.template initByFullCallbackType<std_msgs::String::ConstPtr>("/chatter", 100, boost::bind(&TestQueueSubPlugin::chatterCallback, this, _1));
		ops_1.callback_queue = getCallbackQueue().get();
		ros::Subscriber sub1 = n.subscribe(ops_1);
		ROS_INFO("[SubPlugin] %s on thread %lu pubNum:%d", actorName.c_str(), pthread_self(), sub1.getNumPublishers());
	
		// depecated, used to test ros bug
		// ros::Subscriber sub1 = n.subscribe("/chatter",100, &TestQueueSubPlugin::chatterCallback, this);
		// ROS_INFO("[SubPlugin] %s on thread %lu pubNum:%d", actorName.c_str(), pthread_self(), sub1.getNumPublishers());

		// depecated, used to test ros bug
		ros::SubscribeOptions ops_2 =
    	ros::SubscribeOptions::create<std_msgs::String>(
			"/chatter",        			// topic name
			100,              			// queue length
			chatterCallback_g1, 		// callback function
			ros::VoidPtr(),    			// tracked object, we don't need one thus NULL
			getCallbackQueue().get()   	// pointer to callback queue object
		);
		ros::Subscriber sub2 = n.subscribe(ops_2);
		ROS_INFO("[SubPlugin] %s on thread %lu pubNum:%d", actorName.c_str(), pthread_self(), sub2.getNumPublishers());
		
		// depecated, used to test ros bug
		ros::SubscribeOptions ops_3;
		ops_3.template initByFullCallbackType<std_msgs::String::ConstPtr>("/chatter", 100, boost::bind(&TestQueueSubPlugin::chatterCallback2, this, _1));
		ops_3.callback_queue = getCallbackQueue().get();
		ros::Subscriber sub3 = n.subscribe(ops_3);
		ROS_INFO("[SubPlugin] %s on thread %lu pubNum:%d", actorName.c_str(), pthread_self(), sub3.getNumPublishers());

		// depecated, used to test ros bug
		ros::SubscribeOptions ops_4 =
    	ros::SubscribeOptions::create<std_msgs::String>(
			"/chatter",        			// topic name
			100,              			// queue length
			chatterCallback_g2, 		// callback function
			ros::VoidPtr(),    			// tracked object, we don't need one thus NULL
			getCallbackQueue().get()	// pointer to callback queue object
		);
		ros::Subscriber sub4 = n.subscribe(ops_4);
		ROS_INFO("[SubPlugin] %s on thread %lu pubNum:%d", actorName.c_str(), pthread_self(), sub4.getNumPublishers());
		*/

		// user create ros subscriber, test the method in general_plugin
		ros::Subscriber sub5 = pluginSubscribe<std_msgs::String>("/chatter", 100, &TestQueueSubPlugin::chatterCallback2, this);
		//ROS_INFO("[SubPlugin] %s on thread %lu pubNum:%d", actorName.c_str(), pthread_self(), sub5.getNumPublishers());
		
		
		std::string switchStr = "Transmitter";
		if(actorName.compare(switchStr) != 0){
			sleep(10);
			activateActor(switchStr);
			sleep(20);
			activateActor(switchStr);
		}else{
			sleep(10);
			yieldActor();
		}

		sleep(50);
		GeneralPlugin::start();
	}

};
