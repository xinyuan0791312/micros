/* ==================================================================
*
* Copyright (c) 2016-2019, micROS Group, NIIDT, TAIIC, HPCL.
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions 
* are met: 
*
* 1. Redistributions of source code must retain the above copyright 
*     notice, this list of conditions and the following disclaimer. 
* 2. Redistributions in binary form must reproduce the above copyright 
*     notice, this list of conditions and the following disclaimer in the 
*     documentation and/or other materials provided with the 
*     distribution. 
* 3. All advertising materials mentioning features or use of this software 
*     must display the following acknowledgement: 
*     This product includes software developed by the micROS Group. and 
*     its contributors. 
* 4. Neither the name of the Group nor the names of its contributors may 
*     be used to endorse or promote products derived from this software 
*     without specific prior written permission. 
*
* THIS SOFTWARE IS PROVIDED BY MICROS,GROUP AND CONTRIBUTORS
* ''AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT
* NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
* FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL 
* THE MICROS,GROUP OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS 
* OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) 
* HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
* OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN 
* IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
* ===================================================================
*
* Author: Bin Di, Chunlian Fu, Junling Gao, Ruihao Li, Wei Yi.
*         (in alphabetical order)
*
*/

#include "test_barrier_plugin/test_barrier_plugin.h"
#include <pluginlib/class_list_macros.h>
#include "std_msgs/Empty.h"
#include "ros/ros.h"
#include "actor_core/actor_api.h"

PLUGINLIB_EXPORT_CLASS(general_bus::TestBarrierPlugin,general_bus::GeneralPlugin)
// start 4 daemonnode, robotid between 1~4
namespace general_bus {
	void TestBarrierPlugin::start(){
        int robotID = -1;
        ros::NodeHandle privNh("~");
        if (privNh.hasParam("robot_id")) {
            privNh.getParam("robot_id", robotID); 
        } else {
            ROS_WARN("[TestBarrierPlugin]cannot find robot ID in the parameter server, return..");
        }

        ROS_INFO("[TestBarrierPlugin] robotID=%d", robotID);
		GOON_OR_RETURN;
        std::string actorName = "";
		getActorName(_actorID, actorName);
		ROS_INFO("[TestBarrierPlugin] start in actor:%s", actorName.c_str());

        ROS_INFO("[TestBarrierPlugin] NO1. test master request, key=1, count=1");
        GlobalBarrierKey barrierKey("test_barrier_plugin", 1);
        BarrierResult ret = NO;
        if(robotID == 1){
            ret = pluginBarrierApi(barrierKey, 1, 5);
            ROS_INFO("[TestBarrierPlugin] key=%d, ret=%d", barrierKey.getBarrierKey(), ret);
        }
        pluginSleep(5);

        ROS_INFO("[TestBarrierPlugin] NO2. test robot request, key=2, count=1");
        //barrierKey.getBarrierKey() = 2;
        GlobalBarrierKey barrierKey2("test_barrier_plugin", 2);
         if(robotID == 2){
            ret = pluginBarrierApi(barrierKey2, 1, 5);
            ROS_INFO("[TestBarrierPlugin] key=%d, ret=%d", barrierKey2.getBarrierKey(), ret);
        }
        pluginSleep(5);

        ROS_INFO("[TestBarrierPlugin] NO3. test 2 robot request, key=3, count=2");
         //barrierKey.getBarrierKey() = 3;
         GlobalBarrierKey barrierKey3("test_barrier_plugin", 3);
         if(robotID == 2 || robotID == 3){
            for(int i=0; i<5; i++){
                ret = pluginBarrierApi(barrierKey3, 2, 5);
                ROS_INFO("[TestBarrierPlugin] key=%d, ret=%d", barrierKey3.getBarrierKey(), ret);
            } 
        }
        pluginSleep(5);

        ROS_INFO("[TestBarrierPlugin] NO4. test 2 master/robot request, key=4, count=2");
        //barrierKey.getBarrierKey() = 4;
        GlobalBarrierKey barrierKey4("test_barrier_plugin", 4);
         if(robotID == 1 || robotID == 2){
            for(int i=0; i<5; i++){
                ret = pluginBarrierApi(barrierKey4, 2, 5);
                ROS_INFO("[TestBarrierPlugin] key=%d, ret=%d", barrierKey4.getBarrierKey(), ret);
            } 
        }
        pluginSleep(5);

        ROS_INFO("[TestBarrierPlugin] NO5. test 3 robot request, key=5, count=2");
        //barrierKey.getBarrierKey() = 5;
        GlobalBarrierKey barrierKey5("test_barrier_plugin", 5);
        if(robotID != 1){
            ret = pluginBarrierApi(barrierKey5, 2, 5);
            ROS_INFO("[TestBarrierPlugin] key=%d, ret=%d", barrierKey5.getBarrierKey(), ret);
        }
        pluginSleep(5);

        ROS_INFO("[TestBarrierPlugin] NO6. test 3 master/robot request, key=6, count=2");
        // barrierKey.getBarrierKey() = 6;
        GlobalBarrierKey barrierKey6("test_barrier_plugin", 6);
        if(robotID != 4){
            ret = pluginBarrierApi(barrierKey6, 2, 5);
            ROS_INFO("[TestBarrierPlugin] key=%d, ret=%d", barrierKey6.getBarrierKey(), ret);
        }
        pluginSleep(5);

        ROS_INFO("[TestBarrierPlugin] test completed");
    }
}    
