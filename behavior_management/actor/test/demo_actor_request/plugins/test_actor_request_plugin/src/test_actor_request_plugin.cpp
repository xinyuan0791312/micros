/* ==================================================================
*
* Copyright (c) 2016-2019, micROS Group, NIIDT, TAIIC, HPCL.
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions 
* are met: 
*
* 1. Redistributions of source code must retain the above copyright 
*     notice, this list of conditions and the following disclaimer. 
* 2. Redistributions in binary form must reproduce the above copyright 
*     notice, this list of conditions and the following disclaimer in the 
*     documentation and/or other materials provided with the 
*     distribution. 
* 3. All advertising materials mentioning features or use of this software 
*     must display the following acknowledgement: 
*     This product includes software developed by the micROS Group. and 
*     its contributors. 
* 4. Neither the name of the Group nor the names of its contributors may 
*     be used to endorse or promote products derived from this software 
*     without specific prior written permission. 
*
* THIS SOFTWARE IS PROVIDED BY MICROS,GROUP AND CONTRIBUTORS
* ''AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT
* NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
* FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL 
* THE MICROS,GROUP OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS 
* OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) 
* HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
* OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN 
* IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
* ===================================================================
*
* Author: Bin Di, Chunlian Fu, Junling Gao, Ruihao Li, Wei Yi.
*         (in alphabetical order)
*
*/

#include "test_actor_request_plugin/test_actor_request_plugin.h"
#include <pluginlib/class_list_macros.h>
#include "std_msgs/String.h"
#include "ros/ros.h"

PLUGINLIB_EXPORT_CLASS(general_bus::TestActorRequestPlugin,general_bus::GeneralPlugin)

namespace general_bus {
	void TestActorRequestPlugin::callback(const std_msgs::String::ConstPtr& msg){
		ROS_INFO("[TestActorRequestPlugin] /swarmactor_request heard: [%s], and reqeustSwarmActorFromMaster.", msg->data.c_str());
		std::string sendActorName;
		getActorName(_actorID, sendActorName);
		requestActivateActorFromMaster(sendActorName,msg->data);
	}

	void TestActorRequestPlugin::start(){
		GOON_OR_RETURN;

		std::string actorName = "";
		getActorName(_actorID, actorName);
		ROS_INFO("[TestActorRequestPlugin] start in actor %s:", actorName.c_str());
	    
		ros::NodeHandle n;
		ros::Subscriber sub = n.subscribe("/swarmactor_request", 100, &TestActorRequestPlugin::callback, this);
		ros::Publisher pub = n.advertise<std_msgs::String>("/swarmactor_request", 100);

		int i=0;
		while(ros::ok()){
			GOON_OR_RETURN;
			ROS_INFO("[TestActorRequestPlugin] in actorID %ld actorName %s running for %d time",_actorID,actorName.c_str(),i++);
			sleep(_duration);
			ros::spinOnce();
		}

	}

};
