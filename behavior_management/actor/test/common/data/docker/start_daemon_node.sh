#!/bin/bash
sharedir=$(cd $(dirname $0); pwd)
echo -e "export FASTRTPS_ROOT=${sharedir}/Fast-RTPS/usr/local/\nexport PATH=\$FASTRTPS_ROOT/bin:\$PATH\nexport LD_LIBRARY_PATH=\$FASTRTPS_ROOT/lib:\$LD_LIBRARY_PATH\nexport LIBRARY_PATH=\$FASTRTPS_ROOT/lib:\$LIBRARY_PATH\nexport C_INCLUDE_PATH=\$FASTRTPS_ROOT/include:\$C_INCLUDE_PATH\nexport CPLUS_INCLUDE_PATH=\$FASTRTPS_ROOT/include:\$CPLUS_INCLUDE_PATH" >> ~/.bashrc

export FASTRTPS_ROOT=${sharedir}/3rd_software/Fast-RTPS/usr/local/
export PATH=$FASTRTPS_ROOT/bin:$PATH
export LD_LIBRARY_PATH=$FASTRTPS_ROOT/lib:$LD_LIBRARY_PATH
export LIBRARY_PATH=$FASTRTPS_ROOT/lib:$LIBRARY_PATH
export C_INCLUDE_PATH=$FASTRTPS_ROOT/include:$C_INCLUDE_PATH
export CPLUS_INCLUDE_PATH=$FASTRTPS_ROOT/include:$CPLUS_INCLUDE_PATH

source ${sharedir}/micros_actor/devel/setup.bash

roslaunch actor_daemon_node actor_daemon_node.launch
