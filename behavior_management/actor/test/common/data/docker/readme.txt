1. 生成镜像
docker build --network=host -t demo/micros_actor:v1.0 .

2.本地安装FastRTPS，目录 docker_share/3rd_software/Fast-RTPS
cd Fast-RTPS
mkdir build & cd build
cmake -DTHIRDPARTY=ON -DPERFORMANCE_TESTS=ON..
make
sudo make install ..

3.本地编译micros_actor,目录docker_share/micros_actor
catkin_make -j1

4.启动容器（先启动容器，防止没有stop之前的容器）
actor_daemon_node.launch 中将group ns和robotid配置成环境变量。
./docker_start.sh -c 10

如果需要单独启动某一个docker,可执行：
./docker_start.sh -n 10 

5.启动actor_tag_store
roslaunch actor_tag_store taglist_fordemo.launch 

6.启动g_station
roslaunch g_station ground_station_for1platform.launch 

7.测试
7.1	简单启动
	任务文件：/home/yiwei/demo/docker_share/micros_actor/src/micros/behavior_management/actor/demo/start_one_actor/demo1_task.xml
	任务名称：DEMO
	判断：插件Demo1Plugin启动成功
7.2 简单同步与切换
	修改任务文件中<transition部分的sysNum,改为启动docker的个数，如5/10/20/30。
	任务文件：/home/yiwei/demo/docker_share/micros_actor/src/micros/behavior_management/actor/demo/start_one_actor_and_barrier/demo2_task.xml
	任务名称：DEMO
	判断：Actor1先启动，10s后Actor2启动。
7.3 基于地面站消息同步与切换
	任务文件：/home/yiwei/demo/docker_share/micros_actor/src/micros/behavior_management/actor/demo/start_one_actor_and_pubevent/demo3_task.xml
	任务名称：DEMO
	判断：Actor1先启动，g_station发布任务后，Actor2启动。


