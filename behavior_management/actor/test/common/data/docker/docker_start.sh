#!/bin/bash
if [ $# -lt 1 ]; then
	echo 'error: docker_start.sh [-c "docker count"] [-n "docker number"]'
	exit 0
fi

# micros_actor source will be compiled here, and load this folder to docker containers
sharedir='/home/yiwei/demo/docker_share'
N=0
No=0
while [ $# -ge 2 ]; do
	case "$1" in
		-c) N=$2;shift 2;;
		-n) No=$2;shift 2;;
		*) echo "invalid param $1";exit 1;break;;
	esac
done

startDockerWithID(){
	container_name="robot_$1"
	#stop and remove container
	#docker stop ${container_name}
 	#docker rm ${container_name}

	#start container and actor daemon node
	docker run -itd --name ${container_name} --cap-add=NET_ADMIN NAMESPACE=${container_name} --env NUM=$1 -v ${sharedir}:${sharedir} -v /etc/localtime:/etc/localtime:ro demo/micros_actor:v1.0 /bin/sh -c "${sharedir}/start_daemon_node.sh"
}

# only start docker with number
if [ $No -gt 0 ]; then
	container_name="robot_$No"
	echo "stop and rm docker containers [${container_name}]..."
	docker stop ${container_name}
 	docker rm ${container_name}
	echo "start docker containers [${container_name}]..."
	startDockerWithID $No

# start docker with count
elif [ $N -gt 0 ]; then
	ctl_time=600
	echo "stop and rm all docker containers..."
	docker stop $(docker ps -aq)
	docker rm $(docker ps -aq)
	echo "start $N docker containers..."
	for ((i=1;i<=${N};i++)); do
		startDockerWithID $i
	done
fi


