/* ==================================================================
*
* Copyright (c) 2016-2019, micROS Group, NIIDT, TAIIC, HPCL.
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions 
* are met: 
*
* 1. Redistributions of source code must retain the above copyright 
*     notice, this list of conditions and the following disclaimer. 
* 2. Redistributions in binary form must reproduce the above copyright 
*     notice, this list of conditions and the following disclaimer in the 
*     documentation and/or other materials provided with the 
*     distribution. 
* 3. All advertising materials mentioning features or use of this software 
*     must display the following acknowledgement: 
*     This product includes software developed by the micROS Group. and 
*     its contributors. 
* 4. Neither the name of the Group nor the names of its contributors may 
*     be used to endorse or promote products derived from this software 
*     without specific prior written permission. 
*
* THIS SOFTWARE IS PROVIDED BY MICROS,GROUP AND CONTRIBUTORS
* ''AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT
* NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
* FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL 
* THE MICROS,GROUP OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS 
* OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) 
* HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
* OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN 
* IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
* ===================================================================
*
* Author: Bin Di, Chunlian Fu, Junling Gao, Ruihao Li, Wei Yi.
*         (in alphabetical order)
*
*/

#include "example1_plugin/example1_plugin.h"
#include <pluginlib/class_list_macros.h>
#include "ros/ros.h"

PLUGINLIB_EXPORT_CLASS(general_bus::Example1Plugin,general_bus::GeneralPlugin)

namespace general_bus {
	Example1Plugin::Example1Plugin(){
		_sub = new RTPSSubscriber<UTOEventTransition>("/uto_event_transition", &Example1Plugin::eventTransitionCallback, this);
		// _pub = new RTPSPublisher<UTOEventTransition>("/uto_event_sync");
	}

	Example1Plugin::~Example1Plugin(){
		if(_sub){
			delete _sub;
		}
	}

	void Example1Plugin::eventTransitionCallback(UTOEventTransition &msg){
		ROS_INFO("[Example1Plugin] Receive uto msg by FastRTPS: currentActor:%s eventName:%s on robot %ld", msg.currentActor().c_str(), msg.eventName().c_str(), msg.robotId());
	}

	void Example1Plugin::start(){
		//GOON_OR_RETURN;
		i = 0;
		std::string tempStr;
		getActorName(_actorID,tempStr);	
		while(ros::ok()){
			GOON_OR_RETURN;
			i++;
			// UTOEventTransition transition;
            // transition.currentActor(tempStr);
            // transition.eventName("default");
            // transition.syncStatus(-1);
            // transition.robotId(i);
			// _pub->publish(transition);
			ROS_INFO("[Example1 Plugin] in actorID %ld actorName %s running on cpu %d for %d time",_actorID,tempStr.c_str(),sched_getcpu(),i);
			sleep(_duration);
		}

		// std::string switchStr = "Transmitter";
		// activateActor(_actorID, switchStr);

		// while(i < 10){
		// 	GOON_OR_RETURN;
		// 	i++;
		// 	ROS_INFO("[Example1 Plugin] in actorID %ld actorName %s running on cpu %d for %d time",_actorID,tempStr.c_str(),sched_getcpu(),i);
		// 	sleep(_duration);
		// }

		// switchStr = "Follower";
		// switchToActor(_actorID, switchStr);

		// while(i < 15){
		// 	GOON_OR_RETURN;
		// 	i++;
		// 	ROS_INFO("[Example1 Plugin] in actorID %ld actorName %s running on cpu %d for %d time",_actorID,tempStr.c_str(),sched_getcpu(),i);
		// 	sleep(_duration);
		// }

		// switchStr = "Transmitter";
		// switchToActor(_actorID, switchStr);

		// switchStr = "Follower";
		// while (ros::ok()) {
		// 	i++;
		// 	GOON_OR_RETURN;
		// 	ROS_INFO("[Example1 Plugin] in actorID %ld actorName %s running on cpu %d for %d time",_actorID,tempStr.c_str(),sched_getcpu(),i);
		// 	if(i%5==0) activateActor(_actorID, switchStr);
		// 	sleep(_duration);
		// }
	}
};
