#Test
    test actor structure in docker containers, simply start Actor1 on all nodes

#Configs
    docker build --network=host -t demo/micros_actor:v1.0 -f ../common/data/docker/Dockerfile .
    install Fast-RTPS to directory: docker_share/Fast-RTPS
    catkin_make actor source code to directory: docker_share/micros_actor
    copy ../common/data/docker/start_daemon_node.sh to directory: docker_share
    modify sharedir param in ../common/data/docker/docker_start.sh
    modify taglistdict param in actor_daemon_node.launch
    modify task_xml_path param in actor_daemon_node.launch and g_station.launch
    modify staticEndpointXMLFilename param in actor_daemon_node_dds_profile.xml and g_station_dds_profile.xml

#Steps
    ../common/data/docker/docker_start.sh -c 30
    roslaunch g_station ground_station.launch 

    g_station publish task file: demo1_task.xml
    g_station startup task with task name: DEMO
    start test: 30 actor_daemon_node will start Actor1

#Reference
    ../common/data/docker/readme.txt

#Notice
    prepare docker environment in advance

