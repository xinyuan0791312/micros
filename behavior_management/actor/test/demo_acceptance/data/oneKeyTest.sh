#! /bin/bash
#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# global params defination
#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# the username and password to login remote robots
g_user="odroid"
g_password="odroid"

# workspace path
g_workdir="/home/${g_user}/micros_ws/"

# the actor_daemon_node.launch file path on tx2, relative path
g_launchPath="src/behavior_management/actor/test/demo_acceptance/data/actor_daemon_node_acceptance.launch"

# netcard, used to read local ip address, and communicate with other platforms
g_nic="enp4s0"

# these files will be automatically generate ,and copid to remote platforms
g_timesyncshell="time_sync.sh"
g_makerunshell="makerun.sh"

# this shell will read this file to estiblish connection with remote robots,if null, it will scan the LAN
g_robotListPath="${PWD}/robotList.txt"

# this compressed file includs build,devel,src etc.
g_srctar="${PWD}/../micros_actor_alpha_odroid_191129.tar.gz"

# this directory will be used to download remote robots' logs
g_localLogPath="${PWD}/../log/"

# latency time to start up daemon node, s
g_latency=10

# echo color
RED='\e[1;31m'
GREEN='\e[1;32m'
YELLOW='\e[1;33m'
BLUE='\e[1;34m'
PINK='\e[1;35m'
RES='\e[0m'

#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# check params
#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
if [ ! -f ${g_robotListPath} ]; then
	echo -e "${RED}file ${g_robotListPath} not exists.${RES}"
fi
if [ ! -f ${g_srctar} ]; then
	echo -e "${RED}file ${g_srctar} not exists.${RES}"
	exit 0
fi

#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# check if commands installed: ssh,nmap,ntp,expect 
#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# param: $1-cmd, eg.ssh
checkCmdFunc(){
	if [ $# -ge 1 ]; then
		cmd=$1
		msg=$( which $cmd )
		if [[ -z ${msg} ]]; then 
			echo -e "${RED}check $cmd command failed, $cmd not exist, please install it.\nexit.${RES}"
			exit 0
		else 
			echo "check $cmd command success."
		fi
	fi
}

# ---------------------------------------------------------------
## check commands
cmdArray=("ssh" "nmap" "ntpq" "expect")
echo -e "${GREEN} 1. start check local commands... ${RES}"
for cmd in ${cmdArray[@]}; do
	checkCmdFunc ${cmd}
done

# ---------------------------------------------------------------
## check ntp service
echo -e "\n${GREEN} 2. start check ntp service... ${RES}"
ret=$(service ntp status | grep "running")
if [ ${#ret} -eq 0 ]; then
	echo -e "${RED}ntp service is not running.${RES}"
	exit 0
else echo "ntp service is running..."
fi

#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# read IP List
#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
echo -e "\n${GREEN} 3. start read ip address... ${RES}"
# ---------------------------------------------------------------
## read local IP
localIP=`LANG=C ifconfig ${g_nic} | awk '/inet addr:/{ print $2 }' | awk -F: '{print $2 }'`
localMASK=`LANG=C ifconfig ${g_nic} | awk -F: '/Mask/{print $4}'`
if [ ${#localIP} -eq 0 ] || [ ${#localMASK} -eq 0 ]; then
	echo -e "${RED}read local ip failed.${RES}"
	exit 0
fi
echo -e "dev:\t${g_nic}"
echo -e "ip:\t${localIP}"
echo -e "mask:\t${localMASK}"

# ---------------------------------------------------------------
## compute net address,eg.192.168.1.0/24
# $1-mask,eg.255.255.255.0 $2-ip,eg.192.168.1.5
computeNetAddr(){
	if [ $# -ne "2" ];then 
		echo "computeNetAddr function need 2 params."
		exit 0
	fi
	maskArr=(${1//./ })
	ipArr=(${2//./ })
	netAddr=""
	netBit=0
	for i in {0..3}; do
		if [ $i -ne 0 ]; then
			netAddr=${netAddr}.
		fi
		netAddr=${netAddr}$[${ipArr[i]}&${maskArr[i]}]
	
		b=$(echo "obase=2;ibase=10;${maskArr[i]}"|bc)
		j=0
		while [ $j -lt ${#b} ]; do
			if [ ${b:0:1} -eq 1 ]; then
				j=$((j+1))
			else break
			fi
		done
		netBit=$((netBit+j))
	done

	echo ${netAddr}/${netBit}
}

# ---------------------------------------------------------------
## read remote ip list, read from robotList file or scan the LAN
if [ -f ${g_robotListPath} ]; then
	g_remoteIPs=($(cat ${g_robotListPath} | grep  -o -P "^(\d+\.)(\d+\.)(\d+\.)\d+" | uniq))  
else
	netAddr=$(computeNetAddr "${localMASK}" "${localIP}")
	echo -e "LAN:\t${netAddr}"
	g_remoteIPs=($(nmap -sP -e ${g_nic} ${netAddr} | grep -o -P "(\d+\.)(\d+\.)(\d+\.)\d+"))
fi

echo "IP List:"
for i in "${!g_remoteIPs[@]}"; do
	if [ ${g_remoteIPs[$i]} == ${localIP} ]; then
		unset g_remoteIPs[$i]
	else echo ${g_remoteIPs[$i]}
	fi
done

#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# generate shells
#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

start_time=`date +%Y%m%d%H%M%S`
logdir=${g_workdir}log/${start_time}/
#logdir=${g_workdir}log/20190919113056/
generateTimesyncSH(){
	echo -e "${GREEN} start generate ${g_timesyncshell}... ${RES}"
	echo "#! /bin/bash
	
	# config network
	sudo ifconfig eth0 multicast
	sudo route add -net 224.0.0.0 netmask 240.0.0.0 dev eth0
	sudo route add -host 255.255.255.255 dev eth0

	# time sync
	sudo service ntp stop
	timedatectl set-timezone Asia/Shanghai
	ps -ef | grep ntpd | grep -v grep | cut -c 9-15 | xargs kill -9
	sudo ntpdate -u ${localIP}

	">${g_timesyncshell}
	chmod +x ${g_timesyncshell}
}

generateMakerunSH(){
	echo -e "${GREEN} start generate ${g_makerunshell}... ${RES}"
	firstLevelDir=$(tar -ztf ${g_srctar} | awk -F "/" '{print $1}' | sort | uniq )
	tarfileName=$(basename ${g_srctar})

	echo "#!/bin/bash
	
	# check thirdparty software enviroment
	# copy env to ~/.profile
	if [ -z \"\$(cat ~/.profile | grep 'FASTRTPS_ROOT')\" ]; then
		cat ~/.bashrc | grep \"^export\"  >> ~/.profile
	fi

	# create log dir
	mkdir -p ${logdir}

	# kill old process
	pid=\$(ps x | grep 'actor_daemon_node' | sed '/grep/d' | awk 'NR==1{print \$1}')
	if [[ -n \${pid} ]]; then
		kill -9 \${pid}	
	fi
	pid=\$(ps x | grep 'actor_daemon_node' | sed '/grep/d' | awk 'NR==1{print \$1}')
	if [[ -n \${pid} ]]; then
		kill -9 \${pid}	
	fi

	# uncompress
	cd ${g_workdir}
	#if [ ! -f ${tarfileName} ]; then
	#	exit 1
	#fi
    #rm -r ${firstLevelDir}
	#tar -xvzf ${tarfileName} 

	# robot id(ip address)
	ip=\$(ifconfig | awk '/inet addr:/{ print \$2 }' | awk -F: '{print \$2 }' | grep \"${localIP%.*}\")
	robot_id=\${ip##*.}
	echo \"robot_id:\${robot_id}\"

	# config launch file
	cd ${firstLevelDir}
	sed -i \"/<group/s#ns=\\\".*\\\"#ns=\\\"robot_\${robot_id}\\\"#\" ${g_launchPath}
	sed -i \"/\\\"robot_id\\\"/s#value=\\\".*\\\"#value=\\\"\${robot_id}\\\"#\" ${g_launchPath}
	sed -i \"/\\\"ActorLogFileDir\\\"/s#value=\\\".*\\\"#value=\\\"${logdir}\\\"#\" ${g_launchPath}

	# config static discovery
	profilePath=${g_launchPath%/launch/*}/staticprofile/actor_daemon_node_dds_profile.xml
	profileMatchPath=${g_workdir}${firstLevelDir}/${g_launchPath%/launch/*}/staticprofile/actor_daemon_node_dds_discovery_profile.xml
	sed -i \"/<staticEndpointXMLFilename/s#>.*<#>\${profileMatchPath}<#\" \${profilePath}

	# latency
	sleep ${g_latency}s

	# run actor_daemon_node
	source devel/setup.bash
	roslaunch actor_daemon_node ${g_launchPath#*/launch/} 
	"> ${g_makerunshell}
	chmod +x ${g_makerunshell}
}

#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# exec shells
#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ 
pipe=/tmp/testpipe	#used to communicate with main process

# $1--ip address, $2--shell file, $3--nohup? $4--index
doShellFunc() {
	cmd=$2
	if [ $3 -eq 1 ]; then
		cmd="nohup $2>/dev/null 2>&1 &"
	fi
expect << remoteEOF
	set timeout 30
	spawn ssh ${g_user}@$1
	expect {
		"lost connection" {exit 1;}
		-re ".*Connection refused.*" {exit 1;}
		-re ".*Permission denied.*" {exit 1;}
		-re ".*No route to host.*" {exit 1;}
	    "*assword" {set timeout 300; send "${g_password}\r";}
	    "yes/no" {send "yes\r"; exp_continue;}
	}

	expect "${g_user}@*"
	send "${cmd}\r"

	expect {
		"*assword" {set timeout 300; send "${g_password}\r"; exp_continue;}
		"*authenticate*" {set timeout 300; send "2\r"; exp_continue;}
		"*assword*" {set timeout 300; send "${g_password}\r"; exp_continue;;}
		"${g_user}@*" {send "exit\r"; exit 0;}
	}

	expect {
		-re ".*\[0-9\]+\r\n.*" {send "exit\r"; exit 0;}
		"${g_user}@*" {send "exit\r"; exit 0;}
	}

	expect {
		"${g_user}@*" {send "exit\r"; exit 0;}
	}
	interact       
	expect eof
				
remoteEOF
	echo "$4 $1 $?" >$pipe
}

# $1--cmd,  $2-nohup? $3-remove invalid
sshShellFunc() {
	invalidIndex=("${!g_remoteIPs[@]}")

	trap "rm -f $pipe" EXIT
	if [[ ! -p $pipe ]]; then
		mkfifo $pipe
	fi

	while [ ${#invalidIndex[@]} -gt 0 ]; do
		for i in "${!invalidIndex[@]}"; do
			index="${invalidIndex[$i]}"
			doShellFunc ${g_remoteIPs[$index]} ${g_workdir}$1 $2 ${i} >/dev/null 2>&1 &
		done 
		
		count=${#invalidIndex[@]}
		while [[ $count -gt 0 ]]; do
		  	if read line <$pipe; then
		  		count=$((count - 1 ))
				array=(${line// / })  
				if [[ ${array[2]} -eq 0 ]]; then
					echo "[${array[1]}] exec success."
					unset invalidIndex[${array[0]}]
				else
					echo -e "${YELLOW}[${array[1]}] exec failed.${RES}"
				fi
			fi
		done

		if [ ${#invalidIndex[@]} -gt 0 ]; then
			echo -e "${PINK}exec failedCount:${#invalidIndex[@]}, retry this step (1 or 2)?${RES}"
			select sel in "Yes" "No"; do 	
			break; done
			if [ "${sel}" != "Yes" ]; then
				if [ $3 -eq 1 ]; then
					for i in ${invalidIndex[*]}; do
						echo -e "${YELLOW}remove invalid remote ip:[${g_remoteIPs[$i]}].${RES}"
						unset g_remoteIPs[$i]
					done
				fi
				break
			fi
		fi

	done
}

#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# exec remote cmds
#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ 
# params:$1-cmd, $2--cmd success code, $3-remove invalid ip?0-no,1-yes
sshCmdFunc(){
	invalidIndex=("${!g_remoteIPs[@]}")

	while [ ${#invalidIndex[@]} -gt 0 ]; do
		for i in "${!invalidIndex[@]}"; do
			index="${invalidIndex[$i]}"
			ip=${g_remoteIPs[$index]}
			cmd=$(echo ${1/@host/@${ip}})
			(expect << remoteEOF
				set timeout 10
				spawn ${cmd}
				set output [open "/tmp/expect_result" "w"]
				set ret 0
				expect {
						"lost connection" {exit 1;}
						-re ".*Connection refused.*" {exit 1;}
						-re ".*Permission denied.*" {exit 1;}
						-re ".*No route to host.*" {exit 1;}
						-re ".*No such file or directory.*"  {exit 1;}
						"yes/no" {send "yes\r"; exp_continue;}
						"*assword:" {set timeout 300; send "${g_password}\r";exp_continue;}
						-re "\[0-9\]{4}-\[0-9\]{2}-\[0-9\]{2} \[0-9\]{2}:\[0-9\]{2}:\[0-9\]{2}" {puts \$output "\$expect_out(buffer)"; exit 101;}
						-re ".*100%.*" {puts \$output \$expect_out(buffer);  set ret 102; exp_continue;}
						-re "\[0-9\]+\r\n" {puts \$output \$expect_out(buffer); set ret 100; exp_continue;}
						timeout {exit 1;}
						eof { exit \${ret};}
					   }
				close $output
remoteEOF
			) >/dev/null 2>&1

			if [ $? -ne $2 ]; then
				echo -e "${YELLOW}[${ip}] exec failed.${RES}"
			else 
				result=$(cat "/tmp/expect_result" | sed -r 's#.*([0-9]{3,}[%]).*#\1#g' | grep -E '^[0-9]+' | sed ':a;N;$!ba;s/\n/ /g')
				echo -e "[${ip}] exec result: ${result}"
				unset invalidIndex[$i]
			fi

		done 
		if [ ${#invalidIndex[@]} -gt 0 ]; then
			echo -e "${PINK}exec failedCount:${#invalidIndex[@]}, retry this step (1 or 2)?${RES}"
			select sel in "Yes" "No"; do 	
			break; done
			if [ "${sel}" != "Yes" ]; then
				if [ $3 -eq 1 ]; then
					for i in ${invalidIndex[*]}; do
						echo -e "${YELLOW}remove invalid remote ip:[${g_remoteIPs[$i]}].${RES}"
						unset g_remoteIPs[$i]
					done
				fi
				break
			fi
		fi

	done
}

echo -e "${BLUE}-----------------[COMMAND MEUN]----------------${RES}"	
#PS3="[select]:"
select sel in "generate sh" "ssh copy sh(no tar)" "ssh copy sh(with tar)" "run actor_daemon_node" "check actor_daemon_node" "time sync" "check date time" "save log" "stop test"; do 
	cmdNo=${REPLY}
	case ${sel} in
	"generate sh") 
		generateTimesyncSH
		generateMakerunSH;;
	"ssh copy sh(no tar)") 
		sshCmdFunc "scp -o ConnectTimeout=10 ${g_timesyncshell} ${g_makerunshell} ${g_user}@host:${g_workdir}" 102 1;;
	"ssh copy sh(with tar)")
		sshCmdFunc "scp -o ConnectTimeout=10 ${g_makerunshell} ${g_srctar} ${g_user}@host:${g_workdir}" 102 1;;
	"run actor_daemon_node")
		sshShellFunc ${g_makerunshell} 1 1;;
	"check actor_daemon_node")
		sshCmdFunc "ssh -o ConnectTimeout=10 -t ${g_user}@host \"ps x | grep 'actor_daemon_node' | sed '/grep/d' | awk '{print \\\$1}'\"" 100 0;;
	"time sync")
		sshShellFunc ${g_timesyncshell} 0 1;;
	"check date time")
		sshCmdFunc "ssh -o ConnectTimeout=10 -t ${g_user}@host \"date +'%Y-%m-%d %H:%M:%S'\"" 101 0;;
	"save log")
		mkdir -p ${g_localLogPath}${start_time}
		sshCmdFunc "scp -o ConnectTimeout=10 -r ${g_user}@host:${logdir} ${g_localLogPath}${start_time}/" 102 0;;
	"stop test")
		sshCmdFunc "ssh -o ConnectTimeout=10 -t ${g_user}@host \"ps x | grep 'actor_daemon_node' | sed '/grep/d' | awk '{print \\\$1}' | xargs kill -9\"" 0 0;;
	*) echo "no such choice.";;
	esac
echo -e "${BLUE}--------------[COMMAND ${cmdNo} COMPLETED]------------${RES}"
done

