#ifndef __TEMPLATE_PLUGIN__
#define __TEMPLATE_PLUGIN__

#include "general_plugin/general_plugin.h"
namespace general_bus {
	class TemplatePlugin: public GeneralPlugin {	
	public:
		TemplatePlugin();
		~TemplatePlugin();
		virtual void start();
	};
}
#endif
