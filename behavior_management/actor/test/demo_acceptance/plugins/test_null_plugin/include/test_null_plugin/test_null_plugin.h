#ifndef __TESTNULL_PLUGIN__
#define __TESTNULL_PLUGIN__

#include "general_plugin/general_plugin.h"
namespace general_bus {
	class TestNullPlugin: public GeneralPlugin {	
	public:
		TestNullPlugin();
		~TestNullPlugin();
		virtual void start();
	};
}
#endif
