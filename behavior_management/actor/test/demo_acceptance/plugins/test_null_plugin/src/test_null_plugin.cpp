#include "test_null_plugin/test_null_plugin.h"
#include <pluginlib/class_list_macros.h>
#include "ros/ros.h"

PLUGINLIB_EXPORT_CLASS(general_bus::TestNullPlugin,general_bus::GeneralPlugin)

namespace general_bus {
	TestNullPlugin::TestNullPlugin(){}

	TestNullPlugin::~TestNullPlugin(){}

	void TestNullPlugin::start(){
		std::string actorName = "";
		getActorName(_actorID, actorName);	
		
		int sleepTime = actorName.compare("Actor1") == 0? 1e6 : 1e5;
		std::string nextActorName = actorName.compare("Actor1") == 0? "Actor2":"Actor1";
		int i = 0;
		while(ros::ok()){
			GOON_OR_RETURN;
			i++;

			ROS_INFO("[TestNullPlugin] %d", i);
			if(sleepTime == 1e5 || i >= 10){
				sleepTime = 1e5;
				ROS_INFO("[TestNullPlugin] start switch actor %s", nextActorName.c_str());
				switchToActor(_actorID, nextActorName);
			}

			usleep(sleepTime);
			//pluginUSleep(sleepTime);
		}
	}
};
