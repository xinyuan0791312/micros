#ifndef __TESTACTIVATEACTOR_PLUGIN__
#define __TESTACTIVATEACTOR_PLUGIN__

#include "general_plugin/general_plugin.h"
namespace general_bus {
	class TestActivateActorPlugin: public GeneralPlugin {	
	public:
		TestActivateActorPlugin();
		~TestActivateActorPlugin();
		virtual void start();
	};
}
#endif
