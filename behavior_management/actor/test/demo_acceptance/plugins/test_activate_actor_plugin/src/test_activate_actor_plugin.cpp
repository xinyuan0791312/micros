#include "test_activate_actor_plugin/test_activate_actor_plugin.h"
#include <pluginlib/class_list_macros.h>
#include "ros/ros.h"

PLUGINLIB_EXPORT_CLASS(general_bus::TestActivateActorPlugin,general_bus::GeneralPlugin)

namespace general_bus {
	TestActivateActorPlugin::TestActivateActorPlugin(){}

	TestActivateActorPlugin::~TestActivateActorPlugin(){}

	void TestActivateActorPlugin::start(){
		std::string actorName = "";
		getActorName(_actorID, actorName);	
		int i=0;
		while(ros::ok()){
			GOON_OR_RETURN;
			i++;
			ROS_INFO("[TestActivateActorPlugin] in actorID %lld actorName %s running for %d time", _actorID, actorName.c_str(), i);
			if(i == 30 && actorName.compare("Actor1") == 0){
				activateActor("Actor2");
			}
			pluginSleep(1);
		}
	}
};
