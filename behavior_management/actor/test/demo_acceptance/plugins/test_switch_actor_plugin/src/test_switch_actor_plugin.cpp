#include "test_switch_actor_plugin/test_switch_actor_plugin.h"
#include <pluginlib/class_list_macros.h>
#include "ros/ros.h"

PLUGINLIB_EXPORT_CLASS(general_bus::TestSwitchActorPlugin,general_bus::GeneralPlugin)

namespace general_bus {
	TestSwitchActorPlugin::TestSwitchActorPlugin(){}

	TestSwitchActorPlugin::~TestSwitchActorPlugin(){}

	void TestSwitchActorPlugin::start(){
		std::string actorName;
		getActorName(_actorID, actorName);	
		int i=0;
		while(ros::ok()){
			GOON_OR_RETURN;
			i++;
			ROS_INFO("[TestSwitchActorPlugin] in actorID %lld actorName %s running for %d time", _actorID, actorName.c_str(), i);
			if(i == 30 && actorName.compare("Actor2") == 0){
				switchToActor(_actorID, "Actor3");
			}
			pluginSleep(1);
		}
	}
};
