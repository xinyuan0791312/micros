#ifndef __TESTSWITCHACTOR_PLUGIN__
#define __TESTSWITCHACTOR_PLUGIN__

#include "general_plugin/general_plugin.h"
namespace general_bus {
	class TestSwitchActorPlugin: public GeneralPlugin {	
	public:
		TestSwitchActorPlugin();
		~TestSwitchActorPlugin();
		virtual void start();
	};
}
#endif
