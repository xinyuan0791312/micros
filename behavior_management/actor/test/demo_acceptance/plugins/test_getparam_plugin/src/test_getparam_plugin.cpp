#include "test_getparam_plugin/test_getparam_plugin.h"
#include <pluginlib/class_list_macros.h>
#include "ros/ros.h"

PLUGINLIB_EXPORT_CLASS(general_bus::TestGetParamPlugin,general_bus::GeneralPlugin)

namespace general_bus {
	TestGetParamPlugin::TestGetParamPlugin(){}

	TestGetParamPlugin::~TestGetParamPlugin(){}

	void TestGetParamPlugin::start(){
		std::string actorName = "";
		getActorName(_actorID, actorName);
		std::string paramValue = getParam("param");	
		int i=0;
		while(ros::ok()){
			GOON_OR_RETURN;
			i++;
			ROS_INFO("[TestGetParamPlugin] in actorID %lld actorName %s running for %d time, paramvalue %s", _actorID, actorName.c_str(), i, paramValue.c_str());
			pluginSleep(1);
		}
	}
};
