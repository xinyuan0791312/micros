#ifndef __TESTGETPARAM_PLUGIN__
#define __TESTGETPARAM_PLUGIN__

#include "general_plugin/general_plugin.h"
namespace general_bus {
	class TestGetParamPlugin: public GeneralPlugin {	
	public:
		TestGetParamPlugin();
		~TestGetParamPlugin();
		virtual void start();
	};
}
#endif
