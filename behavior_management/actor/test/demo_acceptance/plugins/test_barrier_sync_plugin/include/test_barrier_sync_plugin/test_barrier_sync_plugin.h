#ifndef __TESTBARRIERSYNC_PLUGIN__
#define __TESTBARRIERSYNC_PLUGIN__

#include "general_plugin/general_plugin.h"
namespace general_bus {
	class TestBarrierSyncPlugin: public GeneralPlugin {	
	public:
		TestBarrierSyncPlugin();
		~TestBarrierSyncPlugin();
		virtual void start();
	};
}
#endif
