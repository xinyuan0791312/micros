#include "test_barrier_sync_plugin/test_barrier_sync_plugin.h"
#include <pluginlib/class_list_macros.h>
#include "ros/ros.h"

PLUGINLIB_EXPORT_CLASS(general_bus::TestBarrierSyncPlugin,general_bus::GeneralPlugin)

namespace general_bus {
	TestBarrierSyncPlugin::TestBarrierSyncPlugin(){}

	TestBarrierSyncPlugin::~TestBarrierSyncPlugin(){}

	void TestBarrierSyncPlugin::start(){
		std::string actorName = "";
		getActorName(_actorID, actorName);	
		int i=0;
		while(ros::ok()){
			GOON_OR_RETURN;
			i++;
			ROS_INFO("[TestBarrierSyncPlugin] in actorID %lld actorName %s running for %d time", _actorID, actorName.c_str(), i);
			if(i==30){
				pubEventMsg("finish_event");
			}
			pluginSleep(1);
		}
	}
};
