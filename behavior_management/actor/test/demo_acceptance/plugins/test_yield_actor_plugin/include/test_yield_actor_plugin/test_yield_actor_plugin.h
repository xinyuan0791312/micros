#ifndef __TESTYIELDACTOR_PLUGIN__
#define __TESTYIELDACTOR_PLUGIN__

#include "general_plugin/general_plugin.h"
namespace general_bus {
	class TestYieldActorPlugin: public GeneralPlugin {	
	public:
		TestYieldActorPlugin();
		~TestYieldActorPlugin();
		virtual void start();
	};
}
#endif
