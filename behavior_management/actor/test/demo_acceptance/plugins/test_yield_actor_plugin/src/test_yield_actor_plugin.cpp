#include "test_yield_actor_plugin/test_yield_actor_plugin.h"
#include <pluginlib/class_list_macros.h>
#include "ros/ros.h"

PLUGINLIB_EXPORT_CLASS(general_bus::TestYieldActorPlugin,general_bus::GeneralPlugin)

namespace general_bus {
	TestYieldActorPlugin::TestYieldActorPlugin(){}

	TestYieldActorPlugin::~TestYieldActorPlugin(){}

	void TestYieldActorPlugin::start(){
		std::string actorName;
		getActorName(_actorID, actorName);	
		int i=0;
		while(ros::ok()){
			GOON_OR_RETURN;
			i++;
			ROS_INFO("[TestYieldActorPlugin] in actorID %lld actorName %s running for %d time", _actorID, actorName.c_str(), i);
			if(i == 30 ){
				yieldActor();
			}
			pluginSleep(1);
		}
	}
};
