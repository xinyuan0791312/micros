#ifndef __EXAMPLE_PLUGIN__
#define __EXAMPLE_PLUGIN__

#include "general_plugin/general_plugin.h"
namespace general_bus {
	class ExamplePlugin: public GeneralPlugin {	
	public:
		ExamplePlugin();
		~ExamplePlugin();
		virtual void start();
	};
}
#endif
