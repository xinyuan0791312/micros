#include "example_plugin/example_plugin.h"
#include <pluginlib/class_list_macros.h>
#include "ros/ros.h"

PLUGINLIB_EXPORT_CLASS(general_bus::ExamplePlugin,general_bus::GeneralPlugin)

namespace general_bus {
	ExamplePlugin::ExamplePlugin(){}

	ExamplePlugin::~ExamplePlugin(){}

	void ExamplePlugin::start(){
		std::string actorName = "";
		getActorName(_actorID, actorName);	
		int i=0;
		while(ros::ok()){
			GOON_OR_RETURN;
			i++;
			ROS_INFO("[ExamplePlugin] in actorID %lld actorName %s running for %d time", _actorID, actorName.c_str(), i);
			pluginSleep(1);
		}
	}
};
