#Test
    actor structure acceptance test 

#Configs
  copy source file to DG machine, catkin_make and compress the folder;
  copy compressed folder to all DG machines;
  copy compressed folder to local machine directory: test1912;
  copy oneKeyTest.sh and robotList.txt to directory: test1912;

#Steps
## Test1: Test on single-machine
  start actor_daemon_node on a DG machine: roslaunch src/behavior_management/actor/test/demo_acceptance/data/actor_daemon_node_acceptance.launch
  start g_station on local machine: roslaunch src/behavior_management/actor/test/demo_acceptance/data/g_station_acceptance.launch
  g_station publish task file: task1.xml;
  g_station startup task with task name: DEMO;
  g_station publish event: Actor1 finish_event;

## Test2: Test performance on single_machine
  start actor_daemon_node on a DG machine: roslaunch src/behavior_management/actor/test/demo_acceptance/data/actor_daemon_node_acceptance.launch
  start g_station on local machine: roslaunch src/behavior_management/actor/test/demo_acceptance/data/g_station_acceptance.launch
  g_station publish task file: task_performance.xml;
  g_station startup task with task name: DEMO;
  use top command to check %CPU of actor_daemon_node process; 

## Test3: Test on multi-machine
  modify params in oneKeyTest.sh;
  execute shell file: oneKeyTest.sh;
  start g_station on local machine: roslaunch src/behavior_management/actor/test/demo_acceptance/data/g_station_acceptance.launch
  g_station publish task file: task10.xml;
  g_station startup task with task name: DEMO; 
  g_station publish event: Actor2 finish_event;
