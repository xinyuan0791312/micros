/* ==================================================================
*
* Copyright (c) 2016-2019, micROS Group, NIIDT, TAIIC, HPCL.
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions 
* are met: 
*
* 1. Redistributions of source code must retain the above copyright 
*     notice, this list of conditions and the following disclaimer. 
* 2. Redistributions in binary form must reproduce the above copyright 
*     notice, this list of conditions and the following disclaimer in the 
*     documentation and/or other materials provided with the 
*     distribution. 
* 3. All advertising materials mentioning features or use of this software 
*     must display the following acknowledgement: 
*     This product includes software developed by the micROS Group. and 
*     its contributors. 
* 4. Neither the name of the Group nor the names of its contributors may 
*     be used to endorse or promote products derived from this software 
*     without specific prior written permission. 
*
* THIS SOFTWARE IS PROVIDED BY MICROS,GROUP AND CONTRIBUTORS
* ''AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT
* NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
* FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL 
* THE MICROS,GROUP OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS 
* OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) 
* HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
* OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN 
* IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
* ===================================================================
*
* Author: Bin Di, Chunlian Fu, Junling Gao, Ruihao Li, Wei Yi.
*         (in alphabetical order)
*
*/

#include "test_data_distribute_plugin/test_data_distribute_plugin.h"
#include <pluginlib/class_list_macros.h>
#include "std_msgs/Empty.h"
#include "ros/ros.h"
#include "actor_core/actor_api.h"

PLUGINLIB_EXPORT_CLASS(general_bus::TestDataDistributePlugin,general_bus::GeneralPlugin)
// start 4 daemonnode, robotid between 1~4
namespace general_bus {
	void TestDataDistributePlugin::start(){
        int robotID = -1;
        ros::NodeHandle privNh("~");
        if (privNh.hasParam("robot_id")) {
            privNh.getParam("robot_id", robotID); 
        } else {
            ROS_WARN("[TestDataDistributePlugin]cannot find robot ID in the parameter server, return..");
        }

        ROS_INFO("[TestDataDistributePlugin] robotID=%d", robotID);
		GOON_OR_RETURN;
        std::string actorName = "";
		getActorName(_actorID, actorName);
		ROS_INFO("[TestDataDistributePlugin] start in actor:%s", actorName.c_str());

        BarrierResult ret = NO;
        int index = 0;

        if(actorName.compare("TakeOff") == 0){
            ROS_INFO("[TestDataDistributePlugin] NO1. test master single actor request, key=1, count=1");
            GlobalBarrierKey barrierKey(1);
            if(robotID == 1){
                ret = actorDataDistributeBarrierApi(barrierKey, actorName, 1, 5, index);
                ROS_INFO("[TestDataDistributePlugin] key=%d, actor=%s, ret=%d, index=%d", barrierKey.getBarrierKey(), actorName.c_str(), ret, index);
            }
            pluginSleep(5);

            ROS_INFO("[TestDataDistributePlugin] NO2. test robot single actor request, key=2, count=1");
            GlobalBarrierKey barrierKey2(2);
            if(robotID == 2){
                ret = actorDataDistributeBarrierApi(barrierKey2, actorName, 1, 5, index);
                ROS_INFO("[TestDataDistributePlugin] key=%d, actor=%s, ret=%d, index=%d", barrierKey2.getBarrierKey(), actorName.c_str(), ret, index);
            }
            pluginSleep(5);

            ROS_INFO("[TestDataDistributePlugin] NO3. test 2 robot request, key=3, count=2");
            GlobalBarrierKey barrierKey3(3);
            if(robotID == 2 || robotID == 3){
                for(int i=0; i<5; i++){
                    ret = actorDataDistributeBarrierApi(barrierKey3, actorName, 2, 5, index);
                    ROS_INFO("[TestDataDistributePlugin] key=%d, actor=%s, ret=%d, index=%d", barrierKey3.getBarrierKey(), actorName.c_str(), ret, index);
                } 
            }
            pluginSleep(5);

            ROS_INFO("[TestDataDistributePlugin] NO4. test 2 master/robot request, key=4, count=2");
            GlobalBarrierKey barrierKey4(4);
            if(robotID == 1 || robotID == 2){
                for(int i=0; i<5; i++){
                    ret = actorDataDistributeBarrierApi(barrierKey4, actorName, 2, 5, index);
                    ROS_INFO("[TestDataDistributePlugin] key=%d, actor=%s, ret=%d, index=%d", barrierKey4.getBarrierKey(), actorName.c_str(), ret, index);
                } 
            }
            pluginSleep(5);

            ROS_INFO("[TestDataDistributePlugin] NO5. test 3 robot request, key=5, count=2");
            GlobalBarrierKey barrierKey5( 5);
            if(robotID != 1){
                ret = actorDataDistributeBarrierApi(barrierKey5, actorName, 2, 5, index);
                ROS_INFO("[TestDataDistributePlugin] key=%d, actor=%s, ret=%d, index=%d", barrierKey5.getBarrierKey(), actorName.c_str(), ret, index);
            }
            pluginSleep(5);

            ROS_INFO("[TestDataDistributePlugin] NO6. test 3 master/robot request, key=6, count=2");
            GlobalBarrierKey barrierKey6(6);
            if(robotID != 4){
                ret = actorDataDistributeBarrierApi(barrierKey6, actorName, 2, 5, index);
                ROS_INFO("[TestDataDistributePlugin] key=%d, actor=%s, ret=%d, index=%d", barrierKey6.getBarrierKey(), actorName.c_str(), ret, index);
            }
            pluginSleep(5);

            // start actor 
            activateActor("Egress");
        }

        ROS_INFO("[TestDataDistributePlugin] NO7. test 4 master/robot request,multi actor, key=6, count=5");
        GlobalBarrierKey barrierKey7(7);
        for(int i=0; i<3; i++){
            ret = actorDataDistributeBarrierApi(barrierKey7, actorName, 5, 5, index);
            ROS_INFO("[TestDataDistributePlugin] key=%d, actor=%s, ret=%d, index=%d", barrierKey7.getBarrierKey(), actorName.c_str(), ret, index);
        }
        pluginSleep(5);

        ROS_INFO("[TestDataDistributePlugin] test completed");
    }
}    
