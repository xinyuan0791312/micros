/* ==================================================================
*
* Copyright (c) 2016-2019, micROS Group, NIIDT, TAIIC, HPCL.
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions 
* are met: 
*
* 1. Redistributions of source code must retain the above copyright 
*     notice, this list of conditions and the following disclaimer. 
* 2. Redistributions in binary form must reproduce the above copyright 
*     notice, this list of conditions and the following disclaimer in the 
*     documentation and/or other materials provided with the 
*     distribution. 
* 3. All advertising materials mentioning features or use of this software 
*     must display the following acknowledgement: 
*     This product includes software developed by the micROS Group. and 
*     its contributors. 
* 4. Neither the name of the Group nor the names of its contributors may 
*     be used to endorse or promote products derived from this software 
*     without specific prior written permission. 
*
* THIS SOFTWARE IS PROVIDED BY MICROS,GROUP AND CONTRIBUTORS
* ''AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT
* NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
* FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL 
* THE MICROS,GROUP OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS 
* OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) 
* HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
* OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN 
* IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
* ===================================================================
*
* Author: Bin Di, Chunlian Fu, Junling Gao, Ruihao Li, Wei Yi.
*         (in alphabetical order)
*
*/

#ifndef ACTOR_MATCH_H
#define ACTOR_MATCH_H

#include <QtCore>
#include <QtXml>
#include <QFile>
#include <string>
#include <iostream>
#include <sstream>
#include <mutex>
#include <ros/ros.h>
#include <actor_msgs/actor_match.h>
#include <actor_msgs/swarm_task.h>
#include "hungary_match.h"


//namespace actor_management
//{

//match relation/edge between main nodes, <sensor> <platform> 
struct match_edge
{
    match_edge()
    {
    }
    bool operator==(const match_edge &x)
    {
        return (this->fromID == x.fromID)&&(this->toID == x.toID)&&(this->fromString == x.fromString)&&(this->toString == x.toString);
    }
    int fromID;
    int toID;
    std::string fromString;
    std::string toString;
};
//actor match information 
struct actor_match_info
{
    actor_match_info(){match = false;error_index = -1;}
    actor_match_info& operator=(const actor_match_info &x)
    {
        this->name_str = x.name_str;
        this->match = x.match;
        this->error_index = x.error_index;
        this->node_match_info = x.node_match_info;
        return *this;
    }
    //actor name
    std::string name_str;
    //match or not
    bool match;
    //error index, 0-true,1-platform match error,2-sensor match error,3-hungary match error
    int error_index;
    //node match information,if match==false,node_match_info is empty
    std::vector<match_edge> node_match_info;
};
/*
provide actor match service
request: a string contains the actor xml
response: ture or false
input: local platform xml file
*/
/*
provide actor match information by public function
input: actor name and/or sensor name
output: actor match error index, actor sensor match informations
*/
class ActorMatcher
{
public:
    ActorMatcher(std::string platformXMLFullPath);
    ~ActorMatcher();
    //start Service
    void startService();
    //get platformID,if platformID==-1,not initialized
    int64_t getPlatformID();
    //get sensor match information, return error index, return the match information
    int getActorMatchInfo(std::string actor_name,std::string sensor_name,std::string &match_info);
    int getActorMatchInfo(std::string actor_name,std::map<std::string,std::string> &match_info_map);
    //if reset platform xml file needed, aPlatformXMLFile includes path and name
    bool resetPlatform(std::string aPlatformXMLFile);
//    //actor match callback
//    bool callActorMatch(actor_msgs::actor_match::Request  &req, actor_msgs::actor_match::Response &res);
   

private:
    ros::NodeHandle nh_;
    //platform ID
    int64_t _platformID;
    //platform xml available or not, if false, cannot start service
    bool _platformAvai;
    //publisher and subscirber
    ros::Publisher _publisher;
    ros::Subscriber _subscriber;
    //server remains
    ros::ServiceServer actor_match_server;
    //xml document for actor and platform
    QDomDocument document_platform;
    //current actor document call for match service and its name/ID
    QDomDocument document_actor_curr;
    std::string actor_name_curr;
    //save actors which have called the match service
    std::map<std::string,std::string> actors_document_map;
    //save actors match info, saved as map, easy to replace
    std::map<std::string,actor_match_info> actor_match_info_map;
    //mutex for actor match info map
    std::mutex mutex_info_map;
    //platform xml file name and path
    std::string platform_xml_name;
    std::string platform_xml_path;

    // temp platform xml document for TASK_ARRANGE
    QDomDocument document_temp_platform;
    // temp current actor document for TASK_ARRANGE
    QDomDocument document_temp_actor_curr;
    std::string temp_actor_name_curr;
    // save actors which have called the match service, temp data for TASK_ARRANGE, changing after each call
    std::map<std::string,std::string> temp_actors_document_map;
    // save actors match info, saved as map, easy to replace, temp data for TASK_ARRANGE, changing after each call
    std::map<std::string,actor_match_info> temp_actor_match_info_map;

    //actor match callback
    bool callActorMatch(actor_msgs::actor_match::Request  &req, actor_msgs::actor_match::Response &res);
    //swarm task msg callback function
    void subSwarmTask(const actor_msgs::swarm_task aSwarmTaskMsg);
    //input str: anActorStr
    bool callActorMatch(std::string anActorStr,std::string &anActorNameStr);
    
    //actor match main function
    bool matchActorXML();
    //actor main node match function
    bool matchActorNode(QDomElement &node_actor , const QDomElement &node_platform );
    //property tag match function
    bool propTagMatch(QDomElement &node_actor ,const QDomElement &node_platform,  std::string str_text);
    //usual (non-property) tag match function
    bool usualTagMatch(QDomElement &node_actor, const QDomElement &node_platform);


    //get the platform node text according to the tagname and fullpath
    std::string getPlatformNodeText(const QDomElement &root_platform , std::string tagname , std::string full_path);
    //get the full path of the leaf node
    std::string getLeafFullpath(const QDomElement &leaf );
    //judge if the tag is a property tag according to the tagname
    bool judgePropTagname(const std::string &tagname);


    //check actor and platform xml,if tag "_P_NAME" is exclusive in the root and "_SENSOR" nodes for platform
    //actor_or_platform true represents actor, false represents platform
    bool checkActorPlatformXML(bool actor_or_platform);

    // check actor and platform xml for TASK_ARRANGE, if tag "_P_NAME" is exclusive in the root and "_SENSOR" nodes for platform
    // actor_or_platform true represents actor, false represents platform
    bool checkTempActorPlatformXML(bool actor_or_platform);
    // actor match main function for TASK_ARRANGE
    bool matchTempActorXML();
    // load xml document from file
public://for test
    bool loadXMLFile(QDomDocument &document_platform , QDomDocument &document_actor);
    bool loadXMLFile(QDomDocument &document_platform , std::string fullPath);
    // actor_match for task_arrange
    bool tempActorMatch(actor_msgs::actor_match::Request  &req, actor_msgs::actor_match::Response &res);    
    //if reset platform xml file needed, aPlatformXMLFile includes path and name
    bool resetTempPlatform(std::string aPlatformXMLFile); 

};

//}




#endif
