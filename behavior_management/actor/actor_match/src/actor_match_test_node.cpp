/* ==================================================================
*
* Copyright (c) 2016-2019, micROS Group, NIIDT, TAIIC, HPCL.
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions 
* are met: 
*
* 1. Redistributions of source code must retain the above copyright 
*     notice, this list of conditions and the following disclaimer. 
* 2. Redistributions in binary form must reproduce the above copyright 
*     notice, this list of conditions and the following disclaimer in the 
*     documentation and/or other materials provided with the 
*     distribution. 
* 3. All advertising materials mentioning features or use of this software 
*     must display the following acknowledgement: 
*     This product includes software developed by the micROS Group. and 
*     its contributors. 
* 4. Neither the name of the Group nor the names of its contributors may 
*     be used to endorse or promote products derived from this software 
*     without specific prior written permission. 
*
* THIS SOFTWARE IS PROVIDED BY MICROS,GROUP AND CONTRIBUTORS
* ''AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT
* NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
* FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL 
* THE MICROS,GROUP OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS 
* OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) 
* HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
* OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN 
* IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
* ===================================================================
*
* Author: Bin Di, Chunlian Fu, Junling Gao, Ruihao Li, Wei Yi.
*         (in alphabetical order)
*
*/

// ros node for actor match test
#include "actor_match/actor_match.h"
#include <thread>
#include <unistd.h>
#include <boost/thread.hpp>

//declare global actor match object
ActorMatcher *actor_match;

//actor match server
void pubActorMatchSrv()
{
//	ros::init(argc_,argv_,"actor_match");
	actor_match->startService();
	ros::spin();

}

void subActorMatchSrv()
{
	
	//sleep(3);
	//ros::init(argc_,argv_,"actor_match_test");
	ros::service::waitForService("/actor_match_test/actor_match");
	ros::NodeHandle nh;
	ros::ServiceClient actor_match_client = nh.serviceClient<actor_msgs::actor_match>("/actor_match_test/actor_match");
	//load actor xml file
	std::string file_str[5] = {"1","2","3","4","5"};//{"1"};////,"6","7","8","9","10"};
	for(int i=0;i<5;i++)
	{
		ROS_INFO("******************************************");
		QDomDocument actor_document;
		std::string file_name = "actor"+file_str[i];
		file_name += ".xml";
		if(!actor_match->loadXMLFile(actor_document,"/home/dibin/actor_management_guo/actor_data/actor_match_test_db/"+file_name))
		{
			ROS_INFO("Load actor xml %s file fail...",file_name.c_str());
			//ROS_INFO("%s",file_name);

		}else
		{
			ROS_INFO("Load actor xml %s file successfully...",file_name.c_str());
		}
		//str is the actor XML information that will send to the platform
		std::string str = actor_document.toString().toStdString();
		//
		actor_msgs::actor_match actor_match_srv;
		actor_match_srv.request.str = str;

		if(actor_match_client.call(actor_match_srv))
		{
			ROS_INFO("Actor %s match service called successfully...",file_name.c_str());
			//ROS_INFO("%s",file_name);
			if(actor_match_srv.response.match == true)
			{
				ROS_INFO("Actor %s match successfully",file_name.c_str());
				//ROS_INFO("%s",file_name);
			}else
			{
				ROS_INFO("Actor %s match fails",file_name.c_str());
			}
		}else
		{
			ROS_INFO("Actor %s match service called failed...",file_name.c_str());
		}

		ROS_INFO("------------------------------------------");
	}
	
	/*TEST test the actor match getActorMatchInfo function */
	std::string actor_str[5] = {"1","2","3","4","5"};//,"6","7","8","9","10"};{"5"};
	std::map<std::string,std::string> match_info_map;
	for(int i=0;i<5;i++)
	{
		match_info_map.clear();
		std::string actor_name = "publisher_"+actor_str[i];	
		ROS_INFO("Call getActorMatchInfo,Actor name: %s",actor_name.c_str());
		//call the getActorMatchInfo
		int actor_result = actor_match->getActorMatchInfo(actor_name,match_info_map);
		ROS_INFO("Actor match error index = %d",actor_result);
		if(!match_info_map.empty())
		{
			ROS_INFO("Match_info_map:");
			for(std::map<std::string,std::string>::iterator iter = match_info_map.begin();iter!=match_info_map.end();iter++)
			{
				std::cout<<iter->first<<"---"<<iter->second<<std::endl;
			}
		}

	}


}


int main(int argc, char **argv)
{
	ros::init(argc, argv, "actor_match_test");

	actor_match = new ActorMatcher("/home/dibin/micros_actor/src/micros/behavior_management/actor/test/data/robot_0.xml");

	//new the pub and sub srv thread
	// std::thread *pub_srv = new std::thread(&pubActorMatchSrv);
	// std::thread *sub_srv = new std::thread(&subActorMatchSrv);
	/*TEST test the actor match service */
	boost::thread pub_srv = boost::thread(boost::bind(&pubActorMatchSrv));
    boost::thread sub_srv = boost::thread(boost::bind(&subActorMatchSrv));
	// while(ros::ok()) ;
    pub_srv.join();
    sub_srv.join();

    if(actor_match) {
        delete actor_match;
    }
//	ros::spin();
	return 0;
}
