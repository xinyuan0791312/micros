/* ==================================================================
*
* Copyright (c) 2016-2019, micROS Group, NIIDT, TAIIC, HPCL.
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions 
* are met: 
*
* 1. Redistributions of source code must retain the above copyright 
*     notice, this list of conditions and the following disclaimer. 
* 2. Redistributions in binary form must reproduce the above copyright 
*     notice, this list of conditions and the following disclaimer in the 
*     documentation and/or other materials provided with the 
*     distribution. 
* 3. All advertising materials mentioning features or use of this software 
*     must display the following acknowledgement: 
*     This product includes software developed by the micROS Group. and 
*     its contributors. 
* 4. Neither the name of the Group nor the names of its contributors may 
*     be used to endorse or promote products derived from this software 
*     without specific prior written permission. 
*
* THIS SOFTWARE IS PROVIDED BY MICROS,GROUP AND CONTRIBUTORS
* ''AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT
* NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
* FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL 
* THE MICROS,GROUP OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS 
* OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) 
* HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
* OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN 
* IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
* ===================================================================
*
* Author: Bin Di, Chunlian Fu, Junling Gao, Ruihao Li, Wei Yi.
*         (in alphabetical order)
*
*/


#include "actor_match/actor_match.h"
#define ACTOR_MATCH_TEST
//using namespace actor_management;

//#include "debug_tools/debug_constant.h"

ActorMatcher::ActorMatcher(std::string platformXMLFullPath)
{
    //initialize platformID
    _platformID = -1;
    #ifndef DEBUG_FLAG
    nh_ = ros::NodeHandle("~/");
    actors_document_map.clear();
    //get local platform xml document
    if(!loadXMLFile(document_platform,platformXMLFullPath))
    {
        ROS_FATAL("[Actor Match]Failed to load local platform xml document...");
        _platformAvai = false;
        return;
    }
    //check platform xml
     if(!checkActorPlatformXML(false))
    {
        ROS_FATAL("[Actor Match]Check platform xml failed...");
        _platformAvai = false;
        return;
    }
    _platformAvai = true;
    #endif
 
}

ActorMatcher::~ActorMatcher()
{

}

bool ActorMatcher::resetPlatform(std::string aPlatformXMLFile)
{
    document_platform.clear();
    if(!loadXMLFile(document_platform,aPlatformXMLFile))
    {
        ROS_FATAL("[Actor Match]Failed to load local platform xml document...");
        _platformAvai = false;
        return false;
    }
    //check platform xml
     if(!checkActorPlatformXML(false))
    {
        ROS_FATAL("[Actor Match]Check platform xml failed...");
        _platformAvai = false;
        return false;
    }
    _platformAvai = true;
    return true;
    
}

void ActorMatcher::startService()
{
   //advertise action match sevice
    if(_platformAvai){
        actor_match_server = nh_.advertiseService("actor_match", &ActorMatcher::callActorMatch, this);
        //pub sub swarm task msg
        _publisher = nh_.advertise<actor_msgs::swarm_task>("/swarm_task_response",10);
        _subscriber = nh_.subscribe("/swarm_task_requirements",10,&ActorMatcher::subSwarmTask,this);
    }else{
        ROS_ERROR("[Actor Match]Cannot start service, check if the platform xml file available...");
    }
    
    
    //TEST
    /*
    int m = 3;
    int n = 3;

    int **matrix = new int *[m];
    for(int i=0;i<m;i++) matrix[i] = new int [n];
    for(int i=0;i<m;i++)
    {
        for(int j=0;j<n;j++)
        {
            matrix[i][j]=0;
            std::cout<<matrix[i][j]<<",";
        }
        std::cout<<std::endl;
    }
    matrix[0][0] = 1;
    matrix[0][1] = 1;
    matrix[1][0] = 1;
    matrix[2][1] = 1;
    

    HungaryMatch a(m,n,matrix);// = new HungaryMatch
    int *match_result; //= new int [n];
    int match_num = a.doHungaryMatch(match_result);
    std::cout<<"match_num="<<match_num<<std::endl;
    for(int i=0;i<n;i++) std::cout<<match_result[i]<<",";
    std::cout<<std::endl;
    //    delete []match_result;
    //delete the matrix
            for(int i=0;i<m;i++)
            {
                #ifdef ACTOR_MATCH_TEST
                ROS_INFO("%d",i);
                #endif
                delete []matrix[i];
            }
            #ifdef ACTOR_MATCH_TEST
            ROS_INFO("delete matrix");
            #endif
            delete []matrix;
    
    //TEST
    */
}

int64_t ActorMatcher::getPlatformID(){
    return _platformID;
}

int ActorMatcher::getActorMatchInfo(std::string actor_name,std::string sensor_name,std::string &match_info)
{
    //mutex lock
    mutex_info_map.lock();
    //find the information in the match info map
    std::map<std::string,actor_match_info>::iterator iter;
    iter = actor_match_info_map.find(actor_name);
    //if not found
    if(iter==actor_match_info_map.end()) return -1;

    //get the match information indexed by actor name
    actor_match_info temp_actor_info = iter->second;
    //unlock
    mutex_info_map.unlock();
    //if error index != 1, platform tag match success
    if(temp_actor_info.error_index!=1)
    {
        for(std::vector<match_edge>::iterator v_iter=temp_actor_info.node_match_info.begin();v_iter!=temp_actor_info.node_match_info.end();v_iter++)
        {
            std::string str = v_iter->fromString;
            if(str.compare(sensor_name)==0)
            {
                match_info = v_iter->toString;
                break;
            }
        }

    }
    
    return temp_actor_info.error_index;
}

int ActorMatcher::getActorMatchInfo(std::string actor_name,std::map<std::string,std::string> &match_info_map)
{
    //lock 
    mutex_info_map.lock();
    //find the information in the match info map
    std::map<std::string,actor_match_info>::iterator iter;
    #ifdef ACTOR_MATCH_TEST
    if(actor_match_info_map.empty()) ROS_WARN("[Actor Match]Actor match info map is empty...");
    if(actors_document_map.empty()) ROS_WARN("[Actor Match]Actor document map is empty...");//
   
    #endif
    iter = actor_match_info_map.find(actor_name);
    //if not found
    if(iter==actor_match_info_map.end()) return -1;

    //get the match information indexed by actor name
    actor_match_info temp_actor_info = iter->second;
    //unlock
    mutex_info_map.unlock();
    //if error index != 1, platform tag match success
    if(temp_actor_info.error_index!=1)
    {
        for(std::vector<match_edge>::iterator v_iter=temp_actor_info.node_match_info.begin();v_iter!=temp_actor_info.node_match_info.end();v_iter++)
        {
            std::string from_str = v_iter->fromString;
            std::string to_str = v_iter->toString;
            match_info_map[from_str] = to_str;
        }

    }
    
    return temp_actor_info.error_index;
}

void ActorMatcher::subSwarmTask(const actor_msgs::swarm_task aSwarmTaskMsg){
    ROS_INFO("[Actor Match] receive swarm task match msg");
    std::string actorsStr = aSwarmTaskMsg.swarmMatchXmlStr;
    std::vector<std::string> actorNameVec;
    //get single actor str
    std::size_t foundHead = 0;
    std::size_t foundTail = 0;
    while(true){
        foundHead = actorsStr.find("<_ACTOR");
        foundTail = actorsStr.find("</_ACTOR>");
        if((foundHead==std::string::npos)||(foundHead==std::string::npos))break;
        std::string tempStr = actorsStr.substr(foundHead,foundTail-foundHead+9);
       /*  #ifdef ACTOR_MATCH_TEST
        ROS_WARN("[Actor Match] test actorstr: %s",tempStr.c_str());
        #endif */
        std::string tempActorName;
        if(callActorMatch(tempStr,tempActorName)){
            actorNameVec.push_back(tempActorName);
        }
        //erase actor str
        actorsStr.erase(foundHead,foundTail-foundHead+9);
    }
    //response to the station
    actor_msgs::swarm_task responseMsg;
    responseMsg.platformID = _platformID;
    #ifdef ACTOR_MATCH_TEST
    ROS_INFO("[Actor Match] test get platformID:%ld",responseMsg.platformID);
    #endif
    std::map<std::string,std::string> tempMatchMap;
    for(int i=0;i<actorNameVec.size();i++){
        int32_t tempBid = getActorMatchInfo(actorNameVec[i],tempMatchMap);
        responseMsg.actorName.push_back(actorNameVec[i]);
        responseMsg.bidActor.push_back(tempBid);
        #ifdef ACTOR_MATCH_TEST
        ROS_INFO("[Actor Match] test actor match msg response,actorName: %s,actorBid:%d",actorNameVec[i].c_str(),tempBid);
        #endif
    }
    _publisher.publish(responseMsg);
    
}

bool ActorMatcher::callActorMatch(std::string anActorStr,std::string &anActorNameStr){
    //get actor xml document from request
     document_actor_curr.clear();
    if(!document_actor_curr.setContent(QString::fromStdString(anActorStr)))
    {
        ROS_ERROR("[Actor Match]Failed to get actor xml document from request...");
        return false;
    }
    
    //check the actor xml
    if(!checkActorPlatformXML(true))
    {
        ROS_ERROR("[Actor Match]Check actor xml failed...");
        return false;
    }
   
    //get actor name/ID from request string
    QDomElement root_platform = document_platform.firstChildElement();
    if(root_platform.isNull())
    {
        ROS_ERROR("[Actor Match]Root node of the platform is null, the platform file may not be loaded successfully...");
        return false;
    }
    QDomElement root_actor = document_actor_curr.firstChildElement();
    //Returns the first direct child node for which nodeName() equals name. If no such direct child exists, a null node is returned.
    QDomNode tempnode_actor_name = root_actor.namedItem("_P_NAME");
    QDomElement temp_element = tempnode_actor_name.toElement();
    actor_name_curr = temp_element.text().toStdString();
    //get actor name
    anActorNameStr = actor_name_curr;
    
    std::pair<std::map<std::string, std::string>::iterator, bool> tempinsert_pair;
    tempinsert_pair = actors_document_map.insert(std::pair<std::string, std::string>(actor_name_curr, anActorStr));
    //if insert successfully, then match,else find the match results
    if(tempinsert_pair.second == true)
    {
        //call the actor match function
        /*FOR TEST*/
        #ifdef ACTOR_MATCH_TEST
        ROS_INFO("[Actor Match]Actor xml has not been matched, insert map, call matchActorXML ...");
        #endif
        /*FOR TEST*/
        matchActorXML();

    }else
    {
        //compare the xml string, if the xml string equals
        std::string tempactor_xml = actors_document_map[actor_name_curr];
        if(tempactor_xml.compare(anActorStr)==0)
        {
            #ifdef ACTOR_MATCH_TEST
            ROS_INFO("[Actor Match]The actor has been matched before, return...");
            #endif            
        }else
        {
            /*FOR TEST*/
            #ifdef ACTOR_MATCH_TEST
            ROS_INFO("[Actor Match]Actor xml has been matched, will replace, call matchActorXML ...");
            #endif
            /*FOR TEST*/
            //replace the xml file 
            actors_document_map[actor_name_curr] = anActorStr;
            //call the actor match function
            matchActorXML();
        }
        

    }  

    return true;

}

bool ActorMatcher::callActorMatch(actor_msgs::actor_match::Request  &req, actor_msgs::actor_match::Response &res)
{
    ROS_INFO("[Actor Match] Service confirmed!");
    //get actor xml document from request
     document_actor_curr.clear();
    if(!document_actor_curr.setContent(QString::fromStdString(req.str)))
    {
        ROS_ERROR("[Actor Match]Failed to get actor xml document from request...");
        res.match = false;
        return false;
    }
    
    //check the actor xml
    if(!checkActorPlatformXML(true))
    {
        ROS_ERROR("[Actor Match]Check actor xml failed...");
        res.match = false;
        return false;
    }
   
    //get actor name/ID from request string
    QDomElement root_platform = document_platform.firstChildElement();
    if(root_platform.isNull())
    {
        ROS_ERROR("[Actor Match]Root node of the platform is null, the platform file may not be loaded successfully...");
        res.match = false;
        return false;
    }
    QDomElement root_actor = document_actor_curr.firstChildElement();
    //Returns the first direct child node for which nodeName() equals name. If no such direct child exists, a null node is returned.
    QDomNode tempnode_actor_name = root_actor.namedItem("_P_NAME");
    QDomElement temp_element = tempnode_actor_name.toElement();
    actor_name_curr = temp_element.text().toStdString();

    std::pair<std::map<std::string, std::string>::iterator, bool> tempinsert_pair;
    tempinsert_pair = actors_document_map.insert(std::pair<std::string, std::string>(actor_name_curr, req.str));
    //if insert successfully, then match,else find the match results
    if(tempinsert_pair.second == true)
    {
        //call the actor match function
        /*FOR TEST*/
        //#ifdef ACTOR_MATCH_TEST
        ROS_INFO("[Actor Match]Actor xml has not been matched, insert map, call matchActorXML ...");
        //#endif
        /*FOR TEST*/
        res.match = matchActorXML();

    }else
    {
        //compare the xml string, if the xml string equals
        std::string tempactor_xml = actors_document_map[actor_name_curr];
        if(tempactor_xml.compare(req.str)==0)
        {
            //find the match result
            ROS_INFO("[Actor Match]The actor has been matched before, return...");
            //lock info map
            mutex_info_map.lock();
            res.match = actor_match_info_map[actor_name_curr].match;
            //unlock
            mutex_info_map.unlock();
            return true;
        }else
        {
            /*FOR TEST*/
            //#ifdef ACTOR_MATCH_TEST
            ROS_INFO("[Actor Match]Actor xml has been matched, will replace, call matchActorXML ...");
            //#endif
            /*FOR TEST*/
            //replace the xml file 
            actors_document_map[actor_name_curr] = req.str;
            //call the actor match function
            res.match = matchActorXML();
        }
        

    }  


    return true;
}

bool ActorMatcher::matchActorXML()
{
    //get root node element
    QDomElement root_platform = document_platform.firstChildElement();
    QDomElement root_actor = document_actor_curr.firstChildElement();
    bool match_result = true;
    //son_node_num is the number of the son nodes of the root,son_node_num>=main_node_count
    int son_node_num = root_actor.childNodes().count();
    bool *result = new bool[son_node_num];
    for(int i=0;i<son_node_num;++i) result[i] = false;
    //main_node represents the <platform> <sensor> node
    int main_node_count = 0;
    //sensor_node represents the <_SENSOR> node
    int sensor_node_count = 0;
    //saves the actor match information and temp node match information
    actor_match_info tempactor_match_info;
    tempactor_match_info.name_str = actor_name_curr;
    tempactor_match_info.error_index = 0;
    std::vector<match_edge> tempnode_match_info;
    //for each son node of the root in actor xml
    for(QDomNode n_actor = root_actor.firstChild();(!n_actor.isNull());n_actor = n_actor.nextSibling())
    {
        QDomElement e_actor = n_actor.toElement();
        QString tagname = e_actor.tagName();

        QDomNodeList nodelist = root_platform.elementsByTagName(tagname);
        if(tagname=="_PLATFORM")
        {
            QDomElement new_platform = nodelist.at(0).toElement();
            //match the _PLATFORM node between actor and platform xml, only one _PLATFORM tag exists 
            result[main_node_count] = matchActorNode(e_actor,new_platform);
            if(!result[main_node_count])
            {
                match_result = false;
                //set error index
                tempactor_match_info.error_index = 1;
                /*FOR TEST*/
                #ifdef ACTOR_MATCH_TEST
                ROS_INFO("[Actor Match]Platform do not match ...");
                #endif
                /*FOR TEST*/
                break;
            }
            main_node_count++;
        }else if(tagname=="_SENSOR")
        {
            //more than one _SENSOR tags may exist, for each _SENSOR tag in actor, compare with 
            //all the _SENSOR tags in platform, the results would be used in the Hungary match
            //sensor node count, used for fromID
            sensor_node_count++;
            bool *sensor_result = new bool[nodelist.count()];
            //for each _SENSOR i(the ith sensor) found in platform
            for(int i = 0 ; i < nodelist.count() ; i++)
            {
                sensor_result[i] = false;
                QDomElement new_sensor = nodelist.at(i).toElement();
                match_edge temp_edge;
                sensor_result[i] = matchActorNode(e_actor,new_sensor);
                
                if(sensor_result[i])
                {
                    temp_edge.fromID = sensor_node_count;//main_node_count;
                    temp_edge.toID = i+1;
                    //get the _SENSOR name
                    QDomNode tempnode_actor_topic = n_actor.namedItem("_P_TOPIC");
                    temp_edge.fromString = tempnode_actor_topic.toElement().text().toStdString();

                    QDomNode tempnode_plat_name = new_sensor.namedItem("_P_NAME");
                    temp_edge.toString = tempnode_plat_name.toElement().text().toStdString();
                    tempnode_match_info.push_back(temp_edge);
                }
                result[main_node_count] = result[main_node_count] || sensor_result[i];
            }
            delete []sensor_result;
            sensor_result = NULL;
            if(!result[main_node_count])
            {
                match_result = false;
                //set error index
                tempactor_match_info.error_index = 2;
                /*FOR TEST*/
                #ifdef ACTOR_MATCH_TEST
                ROS_INFO("[Actor Match]Sensor %d do not match ...",main_node_count);
                #endif
                /*FOR TEST*/
                break;
            }
            main_node_count++;
        }

    }

    delete []result;
    result = NULL;
   
    //if match fails
    if(!match_result)
    {
        //tempactor_match_info.match = false;
        //if the match fails, also saves the node match info, it contains node match information
        tempactor_match_info.node_match_info.insert(tempactor_match_info.node_match_info.end(),tempnode_match_info.begin(),tempnode_match_info.end());

    }else//if match successfully, call hungary match 
    {
        //when the match_result is true and the tempnode_match_info is empty, means the actor has no "_SENSOR" tags
        if(!tempnode_match_info.empty())
        {
             /*FOR TEST*/
            #ifdef ACTOR_MATCH_TEST
            ROS_INFO("[Actor Match]Before hungary match...");
            #endif
            /*FOR TEST*/
            //get actor sensor num m, and platform sensor num n
            int m = 0;
            int n = 0;
            for(int i = 0; i < tempnode_match_info.size() ; i++)
            {
                m = m > tempnode_match_info[i].fromID ? m : tempnode_match_info[i].fromID;
                n = n > tempnode_match_info[i].toID ? n : tempnode_match_info[i].toID;
            }
            /*FOR TEST*/
            #ifdef ACTOR_MATCH_TEST
            ROS_INFO("[Actor Match]m=%d,n=%d",m,n);
            #endif
             /*FOR TEST*/
            #ifdef ACTOR_MATCH_TEST
            ROS_INFO("[Actor Match]Before new matrix...");
            #endif
            /*FOR TEST*/
            int **matrix = new int* [m];
            for(int i=0;i<m;i++) matrix[i] = new int [n];
            for(int i=0;i<m;i++)
            {
                for(int j=0;j<n;j++)
                {
                    matrix[i][j]=0;
                }
            }
            //get the actor-plat sensor matrix
            for(int i = 0 ; i < m ; i++)
            {
                for(int j = 0 ; j < n ; j++)
                {
                    for(int k=0; k<tempnode_match_info.size(); k++)
                    {
                        if((tempnode_match_info[k].fromID==(i+1))&&(tempnode_match_info[k].toID==(j+1)))
                        {
                            matrix[i][j] = 1;
                            break;
                        }
                    }
                }
            }
            /*FOR TEST*/
            #ifdef ACTOR_MATCH_TEST
            /* for(int i=0;i<m;i++)
            {
                for(int j=0;j<n;j++)
                {
                    //matrix[i][j]=0;
                    std::cout<<matrix[i][j]<<",";
                }
                std::cout<<std::endl;
            } */
            ROS_INFO("[Actor Match]Before declare hungary match object...");
            #endif
            /*FOR TEST*/
            //call the hungary match
            HungaryMatch a(m,n,matrix);
            int *hungary_match_result; 
            int match_num = a.doHungaryMatch(hungary_match_result);
            /*FOR TEST*/
            #ifdef ACTOR_MATCH_TEST
            ROS_INFO("[Actor Match]After call dohungarymatch...");
            #endif
            /*FOR TEST*/
            match_edge temp;
            for(int i = 0 ; i < n ; i++)
            {
                if(hungary_match_result[i]!=-1)
                {
                    temp.fromID = hungary_match_result[i]+1;
                    temp.toID = i+1;
                    //get the actor sensor topic and platform sensor name
                    for(int k=0; k<tempnode_match_info.size(); k++)
                    {
                        if((tempnode_match_info[k].fromID==temp.fromID)&&(tempnode_match_info[k].toID==temp.toID))
                        {
                            temp.fromString = tempnode_match_info[k].fromString;
                            temp.toString = tempnode_match_info[k].toString;
                            break;
                        }
                    }
                    tempactor_match_info.node_match_info.push_back(temp);
                }
            }
            /*FOR TEST*/
            #ifdef ACTOR_MATCH_TEST
            ROS_INFO("[Actor Match]Get the Hungary match result...");
            #endif
            /*FOR TEST*/
            //if the match_num < actor sensor number
            if(match_num < m)
            {
                match_result = false;
                //set error index
                tempactor_match_info.error_index = 3;
                /*FOR TEST*/
                #ifdef ACTOR_MATCH_TEST
                ROS_INFO("[Actor Match]Hungary match fails ...");
                #endif
                /*FOR TEST*/
            } 
             /*FOR TEST*/
            #ifdef ACTOR_MATCH_TEST
            ROS_INFO("[Actor Match]Hungary match success...");
            #endif
            /*FOR TEST*/
            //delete the matrix
            for(int i=0;i<m;i++)
            {
                #ifdef ACTOR_MATCH_TEST
                //ROS_INFO("%d",i);
                #endif
                delete []matrix[i];
            }
            #ifdef ACTOR_MATCH_TEST
            ROS_INFO("[Actor Match]Delete matrix");
            #endif
            delete []matrix;
            matrix = NULL;
        
        }

    }

    #ifdef ACTOR_MATCH_TEST
    ROS_INFO("[Actor Match]Save match result...");
    #endif

    tempactor_match_info.match = match_result;
    //save actor match info, lock first
    mutex_info_map.lock();
    actor_match_info_map[actor_name_curr] = tempactor_match_info;
    mutex_info_map.unlock();
    return match_result;


}

bool ActorMatcher::matchActorNode(QDomElement &node_actor , const QDomElement &node_platform )
{

    //get the child node num of the node_actor
    int node_size = node_actor.childNodes().count();
    bool *result = new bool[node_size];
    for(int i=0;i<node_size;i++)result[i] = false;
    int son_node_count = 0;
    for(QDomNode n_actor = node_actor.firstChild();(!n_actor.isNull())&&n_actor.isElement();n_actor = n_actor.nextSibling())
    {
        QDomElement e_actor = n_actor.toElement();
        result[son_node_count++] = matchActorNode(e_actor,node_platform);
    }

    //conditions in one layer, exit for recursive 
    if(node_actor.tagName()=="_LOGIC_OR")
    {
        bool logicOr_match_result = false;
        for(int i = 0 ; i < son_node_count ; i++)
        {
            logicOr_match_result = logicOr_match_result || result[i];
        }
        delete []result;
        result = NULL;
        return logicOr_match_result;
    }else if(node_actor.tagName()=="_LOGIC_AND")
    {
        bool logicAnd_match_result = true;
        for(int i = 0 ; i < son_node_count ; i++)
        {
            logicAnd_match_result = logicAnd_match_result && result[i];
        }
        delete []result;
        result = NULL;
        return logicAnd_match_result;
    }else if(node_actor.tagName()=="_P_NAME" || node_actor.tagName()=="_P_TOPIC" ||
           node_actor.tagName()=="_LARGER_THAN" || node_actor.tagName()=="_SMALLER_THAN" ||
           node_actor.tagName()=="_P_BIND")
    {
        delete []result;
        result = NULL;
        return true;
    }else
    {
        //the child node elements are logicAnd by default
        bool logicAnd_match_result = true;
        if(judgePropTagname(node_actor.tagName().toStdString()))
        {
            logicAnd_match_result = propTagMatch(node_actor,node_platform,node_actor.text().toStdString());
        }else
        {
            if(node_actor.parentNode().isNull())
            {
                for(int i = 0 ; i < son_node_count ; i++)
                {
                    logicAnd_match_result = logicAnd_match_result && result[i];
                }
            }else
            {
                logicAnd_match_result = usualTagMatch(node_actor,node_platform);
                for(int i = 0 ; i < son_node_count ; i++)
                {
                    logicAnd_match_result = logicAnd_match_result && result[i];
                }
            }
        }
        delete []result;
        result = NULL;
        //qDebug() <<node_actor.tagName() << "________" <<logicAnd_match_result;
        return logicAnd_match_result;
    }
}

//property tag match function,
bool ActorMatcher::propTagMatch(QDomElement &node_actor ,const QDomElement &node_platform,  std::string str_text)
{
    bool result = false;
    //get the full path of the actor node
    std::string full_path = getLeafFullpath(node_actor);
    //according to the tagname and fullpath, get the tag text from the platform node
    std::string str = getPlatformNodeText(node_platform,node_actor.tagName().toStdString(),full_path);
    //if tag text is ""
    if(str == "")
    {
        return false;
    }

    QDomNode n_actor = node_actor.firstChild();
    if((!n_actor.isNull()))
    {
        QDomElement e_actor = n_actor.toElement();
        if(e_actor.tagName()=="_LARGER_THAN")
        {
            result = atof(str.c_str()) > atof(str_text.c_str()) ? true : false;
            return result;
        }else if(e_actor.tagName()=="_SMALLER_THAN")
        {
            result = atof(str.c_str()) < atof(str_text.c_str()) ? true : false;
            return result;
        }else if(e_actor.tagName()=="_SMALLER_OR_EQUAL")
        {
            result = atof(str.c_str()) <= atof(str_text.c_str()) ? true : false;
            return result;
        }else if(e_actor.tagName()=="_LARGER_OR_EQUAL")
        {
            result = atof(str.c_str()) >= atof(str_text.c_str()) ? true : false;
            return result;
        }else
        {
            result = str == str_text ? true : false;
            return result;
        }
    }else
    {
        return false;
    }
}

//usual(non-property) tag match function
bool ActorMatcher::usualTagMatch(QDomElement &node_actor, const QDomElement &node_platform)
{
    if(!(node_actor.hasChildNodes()&&node_actor.firstChildElement().isElement()))
    {
        //get the full path of the actor node
        std::string full_path = getLeafFullpath(node_actor);
        //according to the tagname and fullpath, get the tag text from the platform node
        std::string str = getPlatformNodeText(node_platform,node_actor.tagName().toStdString(),full_path);
        //if tag text is ""
        if(str == "")
        {
            return false;
        }else//non empty, should return "NoValueNeeded"
        {
            return true;
        }
    }else
    {
        return true;
    }
}

//get the platform node text according to the tagname and fullpath, if the tag is not a property tag, return "NoValueNeeded"
std::string ActorMatcher::getPlatformNodeText(const QDomElement &root_platform , std::string tagname , std::string full_path)
{
    std::string str;
    //get the first tag of platform
    if((tagname == root_platform.tagName().toStdString())&&(full_path == getLeafFullpath(root_platform)))
    {
        if(!judgePropTagname(tagname))
        {
            str = "NoValueNeeded";
            //return str;
        }else
        {
            str = root_platform.text().toStdString();
        }
        
        return str;
    }
    for(QDomNode n_actor = root_platform.firstChild();!n_actor.isNull()&&n_actor.isElement();n_actor = n_actor.nextSibling())
    {
        QDomElement e_actor = n_actor.toElement();
        str = getPlatformNodeText(e_actor,tagname,full_path);
        if(str != "")
        {
            return str;
        }
    }
}

//get the full path of the leaf node
std::string ActorMatcher::getLeafFullpath(const QDomElement &leaf)
{
    std::string full_path = "";
    for(QDomNode node = leaf;!node.parentNode().parentNode().isNull();node = node.parentNode())
    {
        if(node.toElement().tagName()=="_LOGIC_OR" || node.toElement().tagName()=="_LOGIC_AND"
            || node.toElement().tagName()=="_SMALLER_THAN" || node.toElement().tagName()=="_LARGER_THAN")
        {

        }else
        {
            full_path = full_path.append("/").append(node.toElement().tagName().toStdString());
        }
    }
    return full_path;
}

//judge if the tag is a property tag according to the tagname
bool ActorMatcher::judgePropTagname(const std::string &tagname)
{
    std::string str = QString::fromStdString(tagname).left(3).toStdString();
    //if the tagname starts with "_P_"
    if(str=="_P_")
    {
        return true;
    }else
    {
        return false;
    }
}

//check actor and platform xml,if tag "_P_NAME" is exclusive in the root and "_SENSOR" nodes for platform
//actor_or_platform true represents actor, false represents platform
//call it after load xml file
bool ActorMatcher::checkActorPlatformXML(bool actor_or_platform)
{
    //get root node element
    QDomElement root_platform;
    std::string actor_platform_str;
    if(actor_or_platform)//actor xml
    {
        root_platform = document_actor_curr.firstChildElement();
        actor_platform_str = "_P_TOPIC";
    }else
    {
        root_platform = document_platform.firstChildElement();
        actor_platform_str = "_P_NAME";
    }
    
    if(root_platform.isNull()) return false;

    //saves the number of the tags and the tags contains text
    int root_name_num = 0;
    int root_name_num_te = 0;
    for(QDomNode n_plat = root_platform.firstChild();(!n_plat.isNull());n_plat = n_plat.nextSibling())
    {
        QDomElement e_plat = n_plat.toElement();
        if(e_plat.tagName()=="_P_NAME" )//|| node_actor.tagName()=="_P_TOPIC" 
        {
            root_name_num++;
            //get text
            std::string root_str = e_plat.text().toStdString();
            if(!root_str.empty()) {
                root_name_num_te++;
                if(!actor_or_platform){
                    //get platformID
                    std::stringstream ss;
                    ss.clear();
                    ss.str(root_str);
                    ss>>_platformID;
                }
            }
        }

        if(e_plat.tagName()=="_SENSOR")
        {
            int sensor_name_num = 0;
            int sensor_name_num_te = 0;
            for(QDomNode n_sensor = n_plat.firstChild();(!n_sensor.isNull());n_sensor = n_sensor.nextSibling())
            {
                std::string tagname = n_sensor.toElement().tagName().toStdString();
                if(tagname.compare(actor_platform_str)==0)
                {
                    sensor_name_num++;
                    std::string sensor_str = n_sensor.toElement().text().toStdString();
                    if(!sensor_str.empty()) sensor_name_num_te++;
                }
            }

            if((sensor_name_num!=1)||(sensor_name_num_te!=1)) return false;
        }
    }
    if((root_name_num!=1)||(root_name_num_te!=1)) return false;

    return true;

}

//read local given platform and actor xml file, for test 
bool ActorMatcher::loadXMLFile(QDomDocument &document_platform , QDomDocument &document_actor)
{
    //Load file
    QFile file_limi("/home/dibin/actor_management_guo/actor_data/platform_1.xml");
    if(!file_limi.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        ROS_ERROR("[Actor Match]Error opening platform file...");
        return false;
    }
    else
    {
        //This function parses the XML document from the byte array data and sets it as the content of the document.
        //It tries to detect the encoding of the document as required by the XML specification.
        if(!document_platform.setContent(&file_limi))
        {
            ROS_ERROR("[Actor Match]Failed to load platform document...");
            return false;
        }
        file_limi.close();
    }

    QFile file("/home/dibin/actor_management_guo/actor_data/actor1.xml");
    if(!file.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        ROS_ERROR("[Actor Match]Error opening actor file...");
        return false;
    }
    else
    {
        //This function parses the XML document from the byte array data and sets it as the content of the document.
        //It tries to detect the encoding of the document as required by the XML specification.
        if(!document_actor.setContent(&file))
        {
            ROS_ERROR("[Actor Match]Failed to load actor document...");
            return false;
        }
        file.close();
    }
    return true;
}

//read local platform xml file, saved in document_platforms
bool ActorMatcher::loadXMLFile(QDomDocument &document_platform , std::string fullPath)
{
    /*
    //load file
    QString str = ".xml";
    QString path = QString::fromStdString(currentPath) + QString::fromStdString(name)+str;
    */
    QString path=QString::fromStdString(fullPath);
    QFile file_limi(path);
    if(!file_limi.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        ROS_ERROR("[Actor Match]Error opening platform xml file...");
        return false;
    }
    else
    {
        //This function parses the XML document from the byte array data and sets it as the content of the document.
        //It tries to detect the encoding of the document as required by the XML specification.
        if(!document_platform.setContent(&file_limi))
        {
            ROS_ERROR("[Actor Match]Failed to get from platform document...");
            return false;
        }
        file_limi.close();
    }
    return true;

}

/********************************* split line *********************************/
/*********************** new functions for task_arrange ***********************/
/********************************** 20180528 **********************************/
/*deprecated*/

bool ActorMatcher::tempActorMatch(actor_msgs::actor_match::Request  &req, actor_msgs::actor_match::Response &res)
{
    ROS_INFO("[Actor Match] Service confirmed!");
    //get actor xml document from request
     document_temp_actor_curr.clear();
    if(!document_temp_actor_curr.setContent(QString::fromStdString(req.str)))
    {
        ROS_ERROR("[Actor Match] Failed to get actor xml document from request...");
        res.match = false;
        return false;
    }
    
    //check the actor xml
    if(!checkTempActorPlatformXML(true))
    {
        ROS_ERROR("[Actor Match]Check actor xml failed...");
        res.match = false;
        return false;
    }
   
    //get actor name/ID from request string
    QDomElement root_platform = document_temp_platform.firstChildElement();
    if(root_platform.isNull())
    {
        ROS_ERROR("[Actor Match]Root node of the platform is null, the platform file may not be loaded successfully...");
        res.match = false;
        return false;
    }
    QDomElement root_actor = document_temp_actor_curr.firstChildElement();
    //Returns the first direct child node for which nodeName() equals name. If no such direct child exists, a null node is returned.
    QDomNode tempnode_actor_name = root_actor.namedItem("_P_NAME");
    QDomElement temp_element = tempnode_actor_name.toElement();
    temp_actor_name_curr = temp_element.text().toStdString();

    std::pair<std::map<std::string, std::string>::iterator, bool> tempinsert_pair;
    tempinsert_pair = temp_actors_document_map.insert(std::pair<std::string, std::string>(temp_actor_name_curr, req.str));
    //if insert successfully, then match,else find the match results
    if(tempinsert_pair.second == true)
    {
        //call the actor match function
        /*FOR TEST*/
        //#ifdef ACTOR_MATCH_TEST
        ROS_INFO("[Actor Match]Actor xml has not been matched, insert map, call matchActorXML ...");
        //#endif
        /*FOR TEST*/
        res.match = matchTempActorXML();
    }else
    {
        //compare the xml string, if the xml string equals
        std::string tempactor_xml = temp_actors_document_map[temp_actor_name_curr];
        if(tempactor_xml.compare(req.str)==0)
        {
            //find the match result
            ROS_INFO("[Actor Match]The actor has been matched before, return...");
            res.match = temp_actor_match_info_map[temp_actor_name_curr].match;
            return true;
        }else
        {
            /*FOR TEST*/
            //#ifdef ACTOR_MATCH_TEST
            ROS_INFO("[Actor Match]Actor xml has been matched, will replace, call matchActorXML ...");
            //#endif
            /*FOR TEST*/
            //replace the xml file 
            temp_actors_document_map[temp_actor_name_curr] = req.str;
            //call the actor match function
            res.match = matchTempActorXML();
        }
    }  
    return true;
}

bool ActorMatcher::resetTempPlatform(std::string aPlatformXMLFile)
{
    document_temp_platform.clear();
    if(!loadXMLFile(document_temp_platform, aPlatformXMLFile))
    {
        ROS_FATAL("[Actor Match] Failed to load local platform xml document...");
        return false;
    }
    //check platform xml
     if(!checkTempActorPlatformXML(false))
    {
        ROS_FATAL("[Actor Match] Check platform xml failed...");
        return false;
    }
    return true;
    
}

//check actor and platform xml,if tag "_P_NAME" is exclusive in the root and "_SENSOR" nodes for platform
//actor_or_platform true represents actor, false represents platform
//call it after load xml file
bool ActorMatcher::checkTempActorPlatformXML(bool actor_or_platform)
{
    //get root node element
    QDomElement root_platform;
    std::string actor_platform_str;
    if(actor_or_platform)//actor xml
    {
        root_platform = document_temp_actor_curr.firstChildElement();
        actor_platform_str = "_P_TOPIC";
    }else
    {
        root_platform = document_temp_platform.firstChildElement();
        actor_platform_str = "_P_NAME";
    }
    
    if(root_platform.isNull()) return false;

    //saves the number of the tags and the tags contains text
    int root_name_num = 0;
    int root_name_num_te = 0;
    for(QDomNode n_plat = root_platform.firstChild();(!n_plat.isNull());n_plat = n_plat.nextSibling())
    {
        QDomElement e_plat = n_plat.toElement();
        if(e_plat.tagName()=="_P_NAME" )//|| node_actor.tagName()=="_P_TOPIC" 
        {
            root_name_num++;
            //get text
            std::string root_str = e_plat.text().toStdString();
            if(!root_str.empty()) root_name_num_te++;
        }

        if(e_plat.tagName()=="_SENSOR")
        {
            int sensor_name_num = 0;
            int sensor_name_num_te = 0;
            for(QDomNode n_sensor = n_plat.firstChild();(!n_sensor.isNull());n_sensor = n_sensor.nextSibling())
            {
                std::string tagname = n_sensor.toElement().tagName().toStdString();
                if(tagname.compare(actor_platform_str)==0)
                {
                    sensor_name_num++;
                    std::string sensor_str = n_sensor.toElement().text().toStdString();
                    if(!sensor_str.empty()) sensor_name_num_te++;
                }
            }
            if((sensor_name_num!=1)||(sensor_name_num_te!=1)) return false;
        }
    }
    if((root_name_num!=1)||(root_name_num_te!=1)) return false;

    return true;

}

bool ActorMatcher::matchTempActorXML()
{
    //get root node element
    QDomElement root_platform = document_temp_platform.firstChildElement();
    QDomElement root_actor = document_temp_actor_curr.firstChildElement();
    bool match_result = true;
    //son_node_num is the number of the son nodes of the root,son_node_num>=main_node_count
    int son_node_num = root_actor.childNodes().count();
    bool *result = new bool[son_node_num];
    for(int i=0;i<son_node_num;++i) result[i] = false;
    //main_node represents the <platform> <sensor> node
    int main_node_count = 0;
    //sensor_node represents the <_SENSOR> node
    int sensor_node_count = 0;
    //saves the actor match information and temp node match information
    actor_match_info temp_actor_match_info;
    temp_actor_match_info.name_str = temp_actor_name_curr;
    temp_actor_match_info.error_index = 0;
    std::vector<match_edge> temp_node_match_info;
    //for each son node of the root in actor xml
    for(QDomNode n_actor = root_actor.firstChild();(!n_actor.isNull());n_actor = n_actor.nextSibling())
    {
        QDomElement e_actor = n_actor.toElement();
        QString tagname = e_actor.tagName();

        QDomNodeList nodelist = root_platform.elementsByTagName(tagname);
        if(tagname=="_PLATFORM")
        {
            QDomElement new_platform = nodelist.at(0).toElement();
            //match the _PLATFORM node between actor and platform xml, only one _PLATFORM tag exists 
            result[main_node_count] = matchActorNode(e_actor,new_platform);
            if(!result[main_node_count])
            {
                match_result = false;
                //set error index
                temp_actor_match_info.error_index = 1;
                /*FOR TEST*/
                #ifdef ACTOR_MATCH_TEST
                ROS_INFO("[Actor Match] Platform do not match ...");
                #endif
                /*FOR TEST*/
                break;
            }
            main_node_count++;
        }else if(tagname=="_SENSOR")
        {
            //more than one _SENSOR tags may exist, for each _SENSOR tag in actor, compare with 
            //all the _SENSOR tags in platform, the results would be used in the Hungary match
            //sensor node count, used for fromID
            sensor_node_count++;
            bool *sensor_result = new bool[nodelist.count()];
            //for each _SENSOR i(the ith sensor) found in platform
            for(int i = 0 ; i < nodelist.count() ; i++)
            {
                sensor_result[i] = false;
                QDomElement new_sensor = nodelist.at(i).toElement();
                match_edge temp_edge;
                sensor_result[i] = matchActorNode(e_actor,new_sensor);
                
                if(sensor_result[i])
                {
                    temp_edge.fromID = sensor_node_count;//main_node_count;
                    temp_edge.toID = i+1;
                    //get the _SENSOR name
                    QDomNode tempnode_actor_topic = n_actor.namedItem("_P_TOPIC");
                    temp_edge.fromString = tempnode_actor_topic.toElement().text().toStdString();

                    QDomNode tempnode_plat_name = new_sensor.namedItem("_P_NAME");
                    temp_edge.toString = tempnode_plat_name.toElement().text().toStdString();
                    temp_node_match_info.push_back(temp_edge);
                }
                result[main_node_count] = result[main_node_count] || sensor_result[i];
            }
            delete []sensor_result;
            sensor_result = NULL;
            if(!result[main_node_count])
            {
                match_result = false;
                //set error index
                temp_actor_match_info.error_index = 2;
                /*FOR TEST*/
                #ifdef ACTOR_MATCH_TEST
                ROS_INFO("[Actor Match] Sensor %d do not match ...",main_node_count);
                #endif
                /*FOR TEST*/
                break;
            }
            main_node_count++;
        }

    }

    delete []result;
    result = NULL;
   
    //if match fails
    if(!match_result)
    {
        //temp_actor_match_info.match = false;
        //if the match fails, also saves the node match info, it contains node match information
        temp_actor_match_info.node_match_info.insert(temp_actor_match_info.node_match_info.end(),temp_node_match_info.begin(),temp_node_match_info.end());

    }else//if match successfully, call hungary match 
    {
        //when the match_result is true and the temp_node_match_info is empty, means the actor has no "_SENSOR" tags
        if(!temp_node_match_info.empty())
        {
             /*FOR TEST*/
            #ifdef ACTOR_MATCH_TEST
            ROS_INFO("[Actor Match] Before hungary match...");
            #endif
            /*FOR TEST*/
            //get actor sensor num m, and platform sensor num n
            int m = 0;
            int n = 0;
            for(int i = 0; i < temp_node_match_info.size() ; i++)
            {
                m = m > temp_node_match_info[i].fromID ? m : temp_node_match_info[i].fromID;
                n = n > temp_node_match_info[i].toID ? n : temp_node_match_info[i].toID;
            }
            /*FOR TEST*/
            #ifdef ACTOR_MATCH_TEST
            ROS_INFO("[Actor Match] m=%d,n=%d",m,n);
            #endif
             /*FOR TEST*/
            #ifdef ACTOR_MATCH_TEST
            ROS_INFO("[Actor Match] Before new matrix...");
            #endif
            /*FOR TEST*/
            int **matrix = new int* [m];
            for(int i=0;i<m;i++) matrix[i] = new int [n];
            for(int i=0;i<m;i++)
            {
                for(int j=0;j<n;j++)
                {
                    matrix[i][j]=0;
                }
            }
            //get the actor-plat sensor matrix
            for(int i = 0 ; i < m ; i++)
            {
                for(int j = 0 ; j < n ; j++)
                {
                    for(int k=0; k<temp_node_match_info.size(); k++)
                    {
                        if((temp_node_match_info[k].fromID==(i+1))&&(temp_node_match_info[k].toID==(j+1)))
                        {
                            matrix[i][j] = 1;
                            break;
                        }
                    }
                }
            }
            /*FOR TEST*/
            #ifdef ACTOR_MATCH_TEST
            /* for(int i=0;i<m;i++)
            {
                for(int j=0;j<n;j++)
                {
                    //matrix[i][j]=0;
                    std::cout<<matrix[i][j]<<",";
                }
                std::cout<<std::endl;
            } */
            ROS_INFO("[Actor Match] Before declare hungary match object...");
            #endif
            /*FOR TEST*/
            //call the hungary match
            HungaryMatch a(m,n,matrix);
            int *hungary_match_result; 
            int match_num = a.doHungaryMatch(hungary_match_result);
            /*FOR TEST*/
            #ifdef ACTOR_MATCH_TEST
            ROS_INFO("[Actor Match]After call dohungarymatch...");
            #endif
            /*FOR TEST*/
            match_edge temp;
            for(int i = 0 ; i < n ; i++)
            {
                if(hungary_match_result[i]!=-1)
                {
                    temp.fromID = hungary_match_result[i]+1;
                    temp.toID = i+1;
                    //get the actor sensor topic and platform sensor name
                    for(int k=0; k<temp_node_match_info.size(); k++)
                    {
                        if((temp_node_match_info[k].fromID==temp.fromID)&&(temp_node_match_info[k].toID==temp.toID))
                        {
                            temp.fromString = temp_node_match_info[k].fromString;
                            temp.toString = temp_node_match_info[k].toString;
                            break;
                        }
                    }
                    temp_actor_match_info.node_match_info.push_back(temp);
                }
            }
            /*FOR TEST*/
            #ifdef ACTOR_MATCH_TEST
            ROS_INFO("[Actor Match] Get the Hungary match result...");
            #endif
            /*FOR TEST*/
            //if the match_num < actor sensor number
            if(match_num < m)
            {
                match_result = false;
                //set error index
                temp_actor_match_info.error_index = 3;
                /*FOR TEST*/
                #ifdef ACTOR_MATCH_TEST
                ROS_INFO("[Actor Match] Hungary match fails ...");
                #endif
                /*FOR TEST*/
            } 
             /*FOR TEST*/
            #ifdef ACTOR_MATCH_TEST
            ROS_INFO("[Actor Match] Hungary match success...");
            #endif
            /*FOR TEST*/
            //delete the matrix
            for(int i=0;i<m;i++)
            {
                #ifdef ACTOR_MATCH_TEST
                //ROS_INFO("%d",i);
                #endif
                delete []matrix[i];
            }
            #ifdef ACTOR_MATCH_TEST
            ROS_INFO("[Actor Match] Delete matrix");
            #endif
            delete []matrix;
            matrix = NULL;
        
        }

    }

    #ifdef ACTOR_MATCH_TEST
    ROS_INFO("[Actor Match] Save match result...");
    #endif

    temp_actor_match_info.match = match_result;
    temp_actor_match_info_map[temp_actor_name_curr] = temp_actor_match_info;
    return match_result;
}