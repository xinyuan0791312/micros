/* ==================================================================
*
* Copyright (c) 2016-2019, micROS Group, NIIDT, TAIIC, HPCL.
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions 
* are met: 
*
* 1. Redistributions of source code must retain the above copyright 
*     notice, this list of conditions and the following disclaimer. 
* 2. Redistributions in binary form must reproduce the above copyright 
*     notice, this list of conditions and the following disclaimer in the 
*     documentation and/or other materials provided with the 
*     distribution. 
* 3. All advertising materials mentioning features or use of this software 
*     must display the following acknowledgement: 
*     This product includes software developed by the micROS Group. and 
*     its contributors. 
* 4. Neither the name of the Group nor the names of its contributors may 
*     be used to endorse or promote products derived from this software 
*     without specific prior written permission. 
*
* THIS SOFTWARE IS PROVIDED BY MICROS,GROUP AND CONTRIBUTORS
* ''AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT
* NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
* FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL 
* THE MICROS,GROUP OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS 
* OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) 
* HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
* OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN 
* IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
* ===================================================================
*
* Author: Bin Di, Chunlian Fu, Junling Gao, Ruihao Li, Wei Yi.
*         (in alphabetical order)
*
*/

#include "actor_match/hungary_match.h"

HungaryMatch::HungaryMatch(int index_m, int index_n,int **matrix)
{
	m = index_m;
	n = index_n;
	matrix_match = matrix;
	v = new int [n];
	b = new int [n];
	for(int i = 0 ; i < n ; i++)
	{
		b[i] = -1;
	}

}

HungaryMatch::~HungaryMatch()
{
	delete []v;
	delete []b;
}


bool HungaryMatch::hungaryFind(int x)
{
	for (int j = 0 ; j < n ; ++j)
	{
		if (matrix_match[x][j] && !v[j])
		{
			v[j]=1;
			if (b[j]==-1 || hungaryFind(b[j]))
			{
				b[j] = x;
				return true;
			}
		}
	}
	return false;
}

int HungaryMatch::doHungaryMatch( int* &match_result)
{
	//TEST
	/*
	std::cout<<"matrix_match="<<std::endl;
	for(int i=0;i<m;i++)
    {
        for(int j=0;j<n;j++)
        {
           
            std::cout<<matrix_match[i][j]<<",";
        }
        std::cout<<std::endl;
    }
	*/
	//TEST
	int match_num = 0;
	for(int i = 0 ; i < m ; i++)
	{
	//	memset(v,0,sizeof(v));
		for(int j=0;j<n;j++) v[j] = 0;
		if(hungaryFind(i))
		{
			match_num++;
		}
	}
	//TEST
	/*
	std::cout<<"b="<<std::endl;
	for(int i=0;i<n;i++) std::cout<<b[i]<<",";
	*/
	//TEST
	match_result = b;
	return match_num;
}

/*

void hungaryMatch::hungaryMatchInit()
{
	for(int i = 0 ; i < sizeof(b) ; i++)
	{
		b[i] = -1;
	}
	ActorSensorCount = PlatformSensorCount = 0;
}

bool hungaryMatch::DoHungaryMatch(vector<matchEdge> &XML_matchInfo)
{
	qDebug() << "SENSORS match Issue:";
	qDebug() <<"actor_Sensors" << "     " << "Platform_Sensors";
//  ActorSensorCount = PlatformSensorCount = 0;
	qDebug() << XML_matchInfo.size();
	for(int i = 0; i < XML_matchInfo.size() ; i++)
	{
		qDebug() << XML_matchInfo[i].fromID <<"                " << XML_matchInfo[i].toID;
		ActorSensorCount = ActorSensorCount > XML_matchInfo[i].fromID ? ActorSensorCount : XML_matchInfo[i].fromID;
		PlatformSensorCount = PlatformSensorCount > XML_matchInfo[i].toID ? PlatformSensorCount : XML_matchInfo[i].toID;
	}
	int match_nums = 0;
	matchEdge temp;
	vector<matchEdge>::iterator it;
//  for(int i = 0 ; i < sizeof(b) ; i++)
//  {
//    b[i] = -1;
//  }
	//memset(b,-1,sizeof(b));
	for(int i = 0 ; i < ActorSensorCount ; i++)
	{
		for(int j = 0 ; j < PlatformSensorCount ; j++)
		{
			temp.fromID = i+1;
			temp.toID = j+1;
			it = std::find(XML_matchInfo.begin(),XML_matchInfo.end(),temp);
			if(it == XML_matchInfo.end())
			{
				Matrix_match[i][j] = 0;
			}else
			{
				Matrix_match[i][j] = 1;
			}
		}
	}
	for(int i = 0 ; i < ActorSensorCount ; i++)
	{
		memset(v,0,sizeof(v));
		if(Hungary_find(i))
		{
			match_nums++;
		}
	}
	//b[i]��¼�˽�ɫ��ID��iΪƽ̨��ID
	for(int i = 0 ; i < PlatformSensorCount ; i++)
	{
		qDebug() <<"b[i] = "<< b[i];
	}
	for(int i = 0 ; i < ActorSensorCount ; i++)
	{
		for(int j = 0 ; j < PlatformSensorCount ; j++)
		{
			cout << Matrix_match[i][j] << "   ";
			Matrix_match[i][j] = 0;
		}
		cout << "\n";
	}
	qDebug()<<ActorSensorCount<<" sensors have"<< match_nums <<" match ways";

	if(match_nums == ActorSensorCount && match_nums != 0)
	{
		XML_matchInfo.clear();
		matchEdge temp;
		qDebug() << "platform matched the actor successfully!";
		qDebug() << "the final sensors'match situation is:";
		qDebug() <<"actor_Sensors" << "     " << "Platform_Sensors";
		for(int i = 0 ; i < PlatformSensorCount ; i++)
		{
			if(b[i]!=-1)
			{
				qDebug()<< b[i]+1 << "                "<< i+1;
				temp.fromID = b[i]+1;
				temp.toID = i+1;
				XML_matchInfo.push_back(temp);
			}
		}

		//qDebug() << "DoHungaryMatch = true";


		return true;
	}
	else
	{
		qDebug() << "platform matched the actor failed!";
		return false;
	}

}
bool hungaryMatch::Hungary_find(int x)
{
	qDebug() << "x = " << x;
	for (int j = 0 ; j < PlatformSensorCount ; ++j)
	{
		if (Matrix_match[x][j] && !v[j])
		{
			v[j]=1;
			if (b[j]==-1 || Hungary_find(b[j]))
			{
				b[j] = x;
				return true;
			}
		}
	}
	return false;
}
*/
