/* ==================================================================
*
* Copyright (c) 2016-2019, micROS Group, NIIDT, TAIIC, HPCL.
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions 
* are met: 
*
* 1. Redistributions of source code must retain the above copyright 
*     notice, this list of conditions and the following disclaimer. 
* 2. Redistributions in binary form must reproduce the above copyright 
*     notice, this list of conditions and the following disclaimer in the 
*     documentation and/or other materials provided with the 
*     distribution. 
* 3. All advertising materials mentioning features or use of this software 
*     must display the following acknowledgement: 
*     This product includes software developed by the micROS Group. and 
*     its contributors. 
* 4. Neither the name of the Group nor the names of its contributors may 
*     be used to endorse or promote products derived from this software 
*     without specific prior written permission. 
*
* THIS SOFTWARE IS PROVIDED BY MICROS,GROUP AND CONTRIBUTORS
* ''AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT
* NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
* FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL 
* THE MICROS,GROUP OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS 
* OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) 
* HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
* OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN 
* IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
* ===================================================================
*
* Author: Bin Di, Chunlian Fu, Junling Gao, Ruihao Li, Wei Yi.
*         (in alphabetical order)
*
*/

#include "actor_tag_store/ActorTagStoreTools.h"


bool interpretTagXML(TiXmlDocument& aDoc,ACTOR_STR2TAG_TYPE& anActorName2Tag,
		SWARM_STR2TAG_TYPE& aSwarmName2Tag, PLUGIN_STR2TAG_TYPE& aPluginName2Tag) {
	for (TiXmlElement *pActor=aDoc.RootElement();pActor!=NULL;pActor=pActor->NextSiblingElement()) {
		bool nameFound=false;
		bool tagFound=false;
		std::string nameStr,tagStr;
		{
			TiXmlElement *iter=pActor->FirstChildElement("name");
			if (iter!=NULL) {
				nameFound=true;
				nameStr=iter->GetText();
			}
		}
		{
			TiXmlElement *iter=pActor->FirstChildElement("tag");
			if (iter!=NULL) {
				tagFound=true;
				tagStr=iter->GetText();
			}
		}
		int32_t tag;
		if (nameFound && tagFound && (tagStr!="") && (sscanf(tagStr.c_str(),"%d",&tag)>0))  {

			if (std::string(pActor->Value())=="actor") {
				if (anActorName2Tag.find(nameStr)==anActorName2Tag.end()) {
					anActorName2Tag[nameStr]=(ACTOR_TAG_TYPE)tag;
				}
				else {
					printf("ERROR: duplicate setting of tag for the same actor %s\n",nameStr.c_str());
					return false;
				}
			}
			else if (std::string(pActor->Value())=="swarm") {
				if (aSwarmName2Tag.find(nameStr)==aSwarmName2Tag.end())
					aSwarmName2Tag[nameStr]=(SWARM_TAG_TYPE)tag;
				else {
					printf("ERROR: duplicate setting of tag for the same swarm %s\n",nameStr.c_str());
					return false;
				}
			}
			else if (std::string(pActor->Value())=="plugin") {
				if (aPluginName2Tag.find(nameStr)==aPluginName2Tag.end())
					aPluginName2Tag[nameStr]=(PLUGIN_TAG_TYPE)tag;
				else {
					printf("ERROR: duplicate setting of tag for the same plugin %s\n",nameStr.c_str());
					return false;
				}
			}
		}
	}	
	return true;
}
