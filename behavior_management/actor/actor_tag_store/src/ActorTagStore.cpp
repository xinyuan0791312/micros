/* ==================================================================
*
* Copyright (c) 2016-2019, micROS Group, NIIDT, TAIIC, HPCL.
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions 
* are met: 
*
* 1. Redistributions of source code must retain the above copyright 
*     notice, this list of conditions and the following disclaimer. 
* 2. Redistributions in binary form must reproduce the above copyright 
*     notice, this list of conditions and the following disclaimer in the 
*     documentation and/or other materials provided with the 
*     distribution. 
* 3. All advertising materials mentioning features or use of this software 
*     must display the following acknowledgement: 
*     This product includes software developed by the micROS Group. and 
*     its contributors. 
* 4. Neither the name of the Group nor the names of its contributors may 
*     be used to endorse or promote products derived from this software 
*     without specific prior written permission. 
*
* THIS SOFTWARE IS PROVIDED BY MICROS,GROUP AND CONTRIBUTORS
* ''AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT
* NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
* FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL 
* THE MICROS,GROUP OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS 
* OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) 
* HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
* OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN 
* IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
* ===================================================================
*
* Author: Bin Di, Chunlian Fu, Junling Gao, Ruihao Li, Wei Yi.
*         (in alphabetical order)
*
*/

#include "actor_tag_store/ActorTagStore.h"
#include<stdio.h>
#include<unistd.h>
#include<ros/ros.h>
#include "actor_msgs/TagListResp.h"

ActorTagStore gActorTagStore;

ActorTagStore::ActorTagStore() {
	_actorTag2Name.clear();
	_actorName2Tag.clear();
	_swarmTag2Name.clear();
	_swarmName2Tag.clear();
	_pluginName2Tag.clear();
	_tagListReady=false;

	_pSub = NULL;	
	_pPub = NULL;
}

ActorTagStore::~ActorTagStore() {
	if(_pSub){
		delete _pSub;
		_pSub = NULL;
	}
	if(_pPub){
		delete _pPub;
		_pPub = NULL;
	}
}

bool ActorTagStore::doTagDictConflictTest() {
	std::map<std::string,ACTOR_TAG_TYPE>::iterator iter1;
	iter1=_actorName2Tag.begin();
	while (iter1!=_actorName2Tag.end()) {
			if (_actorTag2Name.find(iter1->second)!=_actorTag2Name.end()) {
			if (_actorTag2Name[iter1->second]!=iter1->first) {
				printf("FATAL: multiple actor names are map to the same tag\n");
				return false;
			}
			else {
				printf("WARNING: duplication in name2tag xml\n");
			}
		}
		_actorTag2Name[iter1->second]=iter1->first;
		iter1++;
	}

	std::map<std::string,SWARM_TAG_TYPE>::iterator iter2;
	iter2=_swarmName2Tag.begin();
	while (iter2!=_swarmName2Tag.end()) {
		if (_swarmTag2Name.find(iter2->second)!=_swarmTag2Name.end()) {
			if (_swarmTag2Name[iter2->second]!=iter2->first) {
				printf("FATAL: multiple swarm names are map to the same tag\n");
				return false;
			}
			else {
				printf("WARNING: duplication in name2tag xml\n");
			}
		}
		_swarmTag2Name[iter2->second]=iter2->first;
		iter2++;
	}
	return true;
}


//todo: using functions from ActorTagStoreTools.h
bool ActorTagStore::interpretXML(TiXmlDocument& aDoc) {

	for (TiXmlElement *pActor=aDoc.RootElement();pActor!=NULL;pActor=pActor->NextSiblingElement()) {
		bool nameFound=false;
		bool tagFound=false;
		std::string nameStr,tagStr;
		{
			TiXmlElement *iter=pActor->FirstChildElement("name");
			if (iter!=NULL) {
				nameFound=true;
				nameStr=iter->GetText();
			}
		}
		{
			TiXmlElement *iter=pActor->FirstChildElement("tag");
			if (iter!=NULL) {
				tagFound=true;
				tagStr=iter->GetText();
			}
		}
		int32_t tag;
		if (nameFound && tagFound && (tagStr!="") && (sscanf(tagStr.c_str(),"%d",&tag)>0))  {

			if (std::string(pActor->Value())=="actor") {
				if (_actorName2Tag.find(nameStr)==_actorName2Tag.end()) {
					_actorName2Tag[nameStr]=(ACTOR_TAG_TYPE)tag;
				}
				else {
					printf("ERROR: duplicate setting of tag for the same actor %s\n",nameStr.c_str());
					return false;
				}
			}
			else if (std::string(pActor->Value())=="swarm") {
				if (_swarmName2Tag.find(nameStr)==_swarmName2Tag.end())
					_swarmName2Tag[nameStr]=(SWARM_TAG_TYPE)tag;
				else {
					printf("ERROR: duplicate setting of tag for the same swarm %s\n",nameStr.c_str());
					return false;
				}
			}
			else if (std::string(pActor->Value())=="plugin") {
				if(_pluginName2Tag.find(nameStr) == _pluginName2Tag.end()){
					_pluginName2Tag[nameStr]=(PLUGIN_TAG_TYPE)tag;
				} else {
					printf("ERROR: duplicate setting of tag for the same plugin %s\n",nameStr.c_str());
					return false;
		}
	}	
		}
	}	
	if (!doTagDictConflictTest()) return false;
	return true;
}


//XML file is interpreted in the service-provider side
bool ActorTagStore::interpretBinary(TagListBinary& aTagListBinary) {
	std::vector<ACTOR_TAG_TYPE>& actorTags=aTagListBinary.actor_tags();
	std::vector<std::string>& actorNames=aTagListBinary.actor_names();
	int nActorType=actorTags.size();
	if (nActorType!=actorNames.size()) {
		printf("FATAL: the number of actor names is different from the number of actor tags\n");
		return false;
	}
	
	std::vector<SWARM_TAG_TYPE>& swarmTags=aTagListBinary.swarm_tags();
	std::vector<std::string>& swarmNames=aTagListBinary.swarm_names();

	int nSwarmType=swarmTags.size();
	if (nSwarmType!=swarmNames.size()) {
		printf("FATAL: the number of swarm names is different from the number of swarm tags\n");
		return false;
	}

	std::vector<PLUGIN_TAG_TYPE>& pluginTags = aTagListBinary.plugin_tags();
	std::vector<std::string>& pluginNames = aTagListBinary.plugin_names();
	int nPluginType = pluginTags.size();
	if(nPluginType != pluginNames.size()){
		printf("FATAL: the number of plugin names is different from the number of plugin tags\n");
		return false;
	}
	printf("ActorTagStore::interpretBinary:plugin tag size=%d", nPluginType);

	int i;	
	for (i=0;i<nActorType;i++) 
		if (_actorName2Tag.find(actorNames[i])==_actorName2Tag.end()) {
			_actorName2Tag[actorNames[i]]=(ACTOR_TAG_TYPE)actorTags[i];
		}
		else {
			printf("ERROR: duplicate setting of tag for the same actor %s\n",actorNames[i].c_str());
			return false;
		}
	for (i=0;i<nSwarmType;i++) 
		if (_swarmName2Tag.find(swarmNames[i])==_swarmName2Tag.end()) {
			_swarmName2Tag[swarmNames[i]]=(SWARM_TAG_TYPE)swarmTags[i];
		}
		else {
			printf("ERROR: duplicate setting of tag for the same swarm %s\n",swarmNames[i].c_str());
			return false;
		}

	for (i=0;i<nPluginType;i++) 
		if (_pluginName2Tag.find(pluginNames[i])==_pluginName2Tag.end()) {
			_pluginName2Tag[pluginNames[i]]=(PLUGIN_TAG_TYPE)pluginTags[i];
		} else {
			printf("ERROR: duplicate setting of tag for the same plugin %s\n",pluginNames[i].c_str());
			return false;
		}
	if (!doTagDictConflictTest()) return false;
	return true;
}



void ActorTagStore::tagListCallback(TagList& aTagList) {
	if (_tagListReady) return;
	std::string content=aTagList.data();
	TiXmlDocument doc;
	doc.Parse(content.c_str());
	if (interpretXML(doc)) _tagListReady=true;
}

void ActorTagStore::tagListBinaryCallback(TagListBinary& aTagListBinary) {
	if (_tagListReady) return;
	if (interpretBinary(aTagListBinary)) _tagListReady=true;
}



bool ActorTagStore::init(const std::string aFilePath) {
	//if local name2tag file is specified, use it as default
	if (aFilePath!="") { 

        //TODO add this for information group
        ros::NodeHandle nh;
        _pubTagListResp = nh.advertise<actor_msgs::TagListResp>("/taglist_resp", 10000);
        _subTagListReq = nh.subscribe("/taglist_req", 10, &ActorTagStore::tagListReqCallback, this);

		TiXmlDocument doc(aFilePath.c_str());
   	    	if (doc.LoadFile()) {
			if (!interpretXML(doc)) {
				printf("ERROR: error in the actor2tag xml file\n");	
				return false;
			}
			else 
				return true;
		}
		else {
			printf("ERROR: the actor2tag xml path is invalid\n");
			return false;
		}
	}
	//otherwise wait for the name server to response
	ros::NodeHandle nh;
	std::string tagListTopic,tagListReqTopic;
	if (!nh.getParam("/tag_list_req_topic", tagListReqTopic))  {
		tagListReqTopic="TagListRequest";
		printf("INFO: using default tag_list_req_topic\n");
	}
	if (!nh.getParam("/tag_list_topic",tagListTopic))  {
		tagListTopic="TagList";
		printf("INFO: using default tag_list_topic\n");
	}

	//_pSub=new RTPSSubscriber<TagList>(tagListTopic,&ActorTagStore::tagListCallback,this);
	//_pSub->init();
	if(!_pSub){
#ifndef USING_BINARY_TAGDICT
	_pSub = new RTPSSubscriber<TagList>(tagListTopic,&ActorTagStore::tagListCallback,this);
#else
	_pSub = new RTPSSubscriber<TagListBinary>(tagListTopic,&ActorTagStore::tagListBinaryCallback,this);
#endif
	}
	if (!_pPub)
		_pPub = new RTPSPublisher<TagListRequest>(tagListReqTopic);
	
//	pub.init();

	printf("INFO: waiting for the actor2tag service provider to response\n");
	int count=0;
	//todo: add lock for _tagListReady
	while (!_tagListReady) {
		TagListRequest req;
		_pPub->publish(req);
		count++;
		printf("INFO: trying to inform the service provider for %d times\n",count);
		if (count<32768)  
			sleep(1);
	//		usleep(5000);
		else return false;
	}
	return true;	
}


ACTOR_TAG_TYPE ActorTagStore::actorName2Tag(const std::string aSwarmName, const std::string& aName) {
	if (_actorName2Tag.find(aName)!=_actorName2Tag.end()) {
		return _actorName2Tag[aName];
	}
	else {
		printf("FATAL: unable to get tag for actor name: %s\n",aName.c_str());
		return -1;
	}
}

std::string ActorTagStore::actorTag2Name(const std::string aSwarmName, ACTOR_TAG_TYPE aTag) {
	if (_actorTag2Name.find(aTag)!=_actorTag2Name.end()) {
		return _actorTag2Name[aTag];
	}
	else {
		//potential output error when using int64_t for the tag
		printf("FATAL: unable to get actor name for tag: %d\n",aTag);
		return "error";
	}

}

SWARM_TAG_TYPE ActorTagStore::swarmName2Tag(const std::string& aName) {
	if (_swarmName2Tag.find(aName)!=_swarmName2Tag.end()) {
		return _swarmName2Tag[aName];
	}
	else {
		printf("FATAL: unable to get tag for swarm name: %s\n",aName.c_str());
		return -1;
	}
}

std::string ActorTagStore::swarmTag2Name(SWARM_TAG_TYPE aTag) {
	if (_swarmTag2Name.find(aTag)!=_swarmTag2Name.end()) {
		return _swarmTag2Name[aTag];
	}
	else {
		//potential output error when using int64_t for the tag
		printf("FATAL: unable to get swarm name for tag: %d\n",aTag);
		return "error";
	}

}

PLUGIN_TAG_TYPE ActorTagStore::pluginName2Tag(const std::string& aName){
	if(_pluginName2Tag.find(aName) != _pluginName2Tag.end()) {
		return _pluginName2Tag[aName];
	} else {
		printf("FATAL: unable to get tag for plugin name: %s\n",aName.c_str());
		return -1;
	}
}

std::string ActorTagStore::pluginTag2Name(PLUGIN_TAG_TYPE aTag){
	auto it = std::find_if(_pluginName2Tag.begin(),_pluginName2Tag.end(), 
		[aTag](const std::map<std::string, PLUGIN_TAG_TYPE>::value_type item){
        	return item.second == aTag;
    	});
	if(it != _pluginName2Tag.end()){
		return it->first;
	} else {
		printf("FATAL: unable to get plugin name for tag: %d\n",aTag);
		return "";
	}
}

//TODO add this for information group
void ActorTagStore::tagListReqCallback(const std_msgs::String::ConstPtr aMsg) {
    actor_msgs::TagListResp respMsg;
    if (_swarmName2Tag.count(aMsg->data) != 0) {
        respMsg.swarmName = aMsg->data;
        respMsg.swarmTag = _swarmName2Tag[aMsg->data];

        for(auto it = _actorName2Tag.begin(); it != _actorName2Tag.end(); it++) {
            respMsg.actorNames.push_back(it->first);
            respMsg.actorTags.push_back(it->second);
        }
    }

    _pubTagListResp.publish(respMsg);
}