/* ==================================================================
*
* Copyright (c) 2016-2019, micROS Group, NIIDT, TAIIC, HPCL.
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions 
* are met: 
*
* 1. Redistributions of source code must retain the above copyright 
*     notice, this list of conditions and the following disclaimer. 
* 2. Redistributions in binary form must reproduce the above copyright 
*     notice, this list of conditions and the following disclaimer in the 
*     documentation and/or other materials provided with the 
*     distribution. 
* 3. All advertising materials mentioning features or use of this software 
*     must display the following acknowledgement: 
*     This product includes software developed by the micROS Group. and 
*     its contributors. 
* 4. Neither the name of the Group nor the names of its contributors may 
*     be used to endorse or promote products derived from this software 
*     without specific prior written permission. 
*
* THIS SOFTWARE IS PROVIDED BY MICROS,GROUP AND CONTRIBUTORS
* ''AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT
* NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
* FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL 
* THE MICROS,GROUP OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS 
* OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) 
* HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
* OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN 
* IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
* ===================================================================
*
* Author: Bin Di, Chunlian Fu, Junling Gao, Ruihao Li, Wei Yi.
*         (in alphabetical order)
*
*/

#include "actor_tag_store/actor_tag_store.h"
#include <vector>

ActorTagStorage::ActorTagStorage(){}

ActorTagStorage::~ActorTagStorage(){
    _swarm2Actor.clear();
}

void ActorTagStorage::updateActorTagStoreFromTaskXML(TiXmlDocument& aDoc){
	if(aDoc.Error()) {
		ROS_WARN("[ActorTagStorage] updateActorTagStoreFromTaskXML:invalid task xml!");
	}

	TiXmlElement *pEle = aDoc.RootElement();
	std::string swarmName = pEle->FirstChildElement("_P_Name")->GetText();

	std::vector<std::string> actorNames;
	std::vector<std::string> pluginNames;
	pEle = pEle->FirstChildElement("_ActorsConfig");
	for (pEle = pEle->FirstChildElement(); pEle != NULL; pEle = pEle->NextSiblingElement()) {
		for(TiXmlElement* pActor = pEle->FirstChildElement("Actor"); pActor != NULL; pActor = pActor->NextSiblingElement("Actor")){
			actorNames.push_back(pActor->FirstChildElement("_P_Name")->GetText());
			for(TiXmlElement* pOODA = pActor->FirstChildElement("OODAConfig"); pOODA != NULL; pOODA = pOODA->NextSiblingElement("OODAConfig"))
				for(TiXmlElement* pBus = pOODA->FirstChildElement("Bus"); pBus != NULL; pBus = pBus->NextSiblingElement("Bus"))
					for(TiXmlElement* pPlugin = pBus->FirstChildElement("Plugin"); pPlugin != NULL; pPlugin = pPlugin->NextSiblingElement("Plugin"))
						pluginNames.push_back(pPlugin->FirstChildElement("_P_Name")->GetText());
			
		}
	}

	updateActorTagStore(swarmName, actorNames, pluginNames);
}

void ActorTagStorage::updateActorTagStore(SwarmInfo& aSwarmInfo, std::vector<ACB>& anACBList){
	std::vector<std::string> actorNames;
	std::vector<std::string> pluginNames;
	for(auto it = anACBList.begin(); it != anACBList.end(); it++){
		actorNames.push_back(it->name);
		for(auto it2 = it->taskInfo.plugins.begin(); it2 != it->taskInfo.plugins.end(); it2++){
			pluginNames.push_back(it2->name);
		}
	}
	updateActorTagStore(aSwarmInfo.swarmName, actorNames, pluginNames);
}

void ActorTagStorage::updateActorTagStore(const std::string& aSwarmName, const std::vector<std::string>& anActorNames, const std::vector<std::string>& aPluginNames){
	boost::unique_lock<boost::shared_mutex> wlock(_swarm2ActorMutex);
	// analyze swarm tag
	if(_swarm2Actor.find(aSwarmName) == _swarm2Actor.end()){
		SWARM_TAG_TYPE swarmTag = _swarm2Actor.size();
		_swarm2Actor[aSwarmName].first = swarmTag;
	} 

	// analyze actor tag
	for(auto it = anActorNames.begin(); it != anActorNames.end(); it++){
		if(_swarm2Actor[aSwarmName].second.find(*it) == _swarm2Actor[aSwarmName].second.end()){
			ACTOR_TAG_TYPE actorTag = _swarm2Actor[aSwarmName].second.size();
			_swarm2Actor[aSwarmName].second[*it] = actorTag;
		}
	}

	// analyze plugin tag
	for(auto it = aPluginNames.begin(); it != aPluginNames.end(); it++){
		if(_pluginName2Tag.find(*it) == _pluginName2Tag.end()) {
			PLUGIN_TAG_TYPE pluginTag = _pluginName2Tag.size();
			_pluginName2Tag[*it] = pluginTag;
		}
	}

	// print
    printf("-------------actor_tag_store---------------\n");
    for(auto it = _swarm2Actor.begin(); it != _swarm2Actor.end(); it++){
        printf("swarm:%s,%d\nactors:", it->first.c_str(), it->second.first);
        for(auto it2 = it->second.second.begin(); it2 != it->second.second.end(); it2++){
            printf("%s,%d;", it2->first.c_str(), it2->second);
        }
        printf("\n");
    }
    printf("plugin:");
    for(auto it = _pluginName2Tag.begin(); it != _pluginName2Tag.end(); it++){
         printf("%s,%d;", it->first.c_str(), it->second);
    }
    printf("\n");

}

ACTOR_TAG_TYPE ActorTagStorage::actorName2Tag(const std::string& aSwarmName, const std::string& anActorName){
    if(_swarm2Actor.find(aSwarmName) != _swarm2Actor.end() 
	&& _swarm2Actor[aSwarmName].second.find(anActorName) != _swarm2Actor[aSwarmName].second.end()){
		return _swarm2Actor[aSwarmName].second[anActorName];
	} 

	printf("FATAL: unable to get tag for actor name: %s\n",anActorName.c_str());
	return -1;
}

std::string ActorTagStorage::actorTag2Name(const std::string& aSwarmName, ACTOR_TAG_TYPE aTag){
    if(_swarm2Actor.find(aSwarmName) != _swarm2Actor.end()) {
		auto it = std::find_if(_swarm2Actor[aSwarmName].second.begin(),_swarm2Actor[aSwarmName].second.end(), 
			[aTag](const std::map<std::string, ACTOR_TAG_TYPE>::value_type item){
				return item.second == aTag;
			});
		if(it != _pluginName2Tag.end())
			return it->first;
	}
	
	printf("FATAL: unable to get actor name for tag: %d\n",aTag);
	return "";
}

SWARM_TAG_TYPE ActorTagStorage::swarmName2Tag(const std::string& aSwarmName){
    if(_swarm2Actor.find(aSwarmName) != _swarm2Actor.end()){
		return _swarm2Actor[aSwarmName].first;
	}

	printf("FATAL: unable to get tag for swarm name: %s\n", aSwarmName.c_str());
	return -1;
}

std::string ActorTagStorage::swarmTag2Name(SWARM_TAG_TYPE aTag){
    auto it = std::find_if(_swarm2Actor.begin(),_swarm2Actor.end(), 
		[aTag](const SWARM2ACTORMAP::value_type item){
        	return item.second.first == aTag;
    	});
	if(it != _swarm2Actor.end()){
		return it->first;
	}
	
	printf("FATAL: unable to get swarm name for tag: %d\n",aTag);
	return "";
}

PLUGIN_TAG_TYPE ActorTagStorage::pluginName2Tag(const std::string& aPluginName){
    if(_pluginName2Tag.find(aPluginName) != _pluginName2Tag.end()) {
		return _pluginName2Tag[aPluginName];
	}
	
	printf("FATAL: unable to get tag for plugin name: %s\n", aPluginName.c_str());
	return -1;
}

std::string ActorTagStorage::pluginTag2Name(PLUGIN_TAG_TYPE aTag){
    auto it = std::find_if(_pluginName2Tag.begin(),_pluginName2Tag.end(), 
		[aTag](const std::map<std::string, PLUGIN_TAG_TYPE>::value_type item){
        	return item.second == aTag;
    	});
	if(it != _pluginName2Tag.end()){
		return it->first;
	}
	
	printf("FATAL: unable to get plugin name for tag: %d\n",aTag);
	return "";
}