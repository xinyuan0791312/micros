/* ==================================================================
*
* Copyright (c) 2016-2019, micROS Group, NIIDT, TAIIC, HPCL.
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions 
* are met: 
*
* 1. Redistributions of source code must retain the above copyright 
*     notice, this list of conditions and the following disclaimer. 
* 2. Redistributions in binary form must reproduce the above copyright 
*     notice, this list of conditions and the following disclaimer in the 
*     documentation and/or other materials provided with the 
*     distribution. 
* 3. All advertising materials mentioning features or use of this software 
*     must display the following acknowledgement: 
*     This product includes software developed by the micROS Group. and 
*     its contributors. 
* 4. Neither the name of the Group nor the names of its contributors may 
*     be used to endorse or promote products derived from this software 
*     without specific prior written permission. 
*
* THIS SOFTWARE IS PROVIDED BY MICROS,GROUP AND CONTRIBUTORS
* ''AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT
* NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
* FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL 
* THE MICROS,GROUP OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS 
* OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) 
* HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
* OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN 
* IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
* ===================================================================
*
* Author: Bin Di, Chunlian Fu, Junling Gao, Ruihao Li, Wei Yi.
*         (in alphabetical order)
*
*/

#ifndef __ACTOR_DATA_STORAGE_H__
#define __ACTOR_DATA_STORAGE_H__
#include "actor_tag_store/actor_tag_store_options.h"
#include "actor_core/ACB.h"
#include "tinyxml.h"
#include <string>
#include <boost/thread/mutex.hpp>

class ActorTagStorage{
public:
    ActorTagStorage();
    ~ActorTagStorage();

    void updateActorTagStoreFromTaskXML(TiXmlDocument& aDoc);

    void updateActorTagStore(SwarmInfo& aSwarmInfo, std::vector<ACB>& anACBList);

    ACTOR_TAG_TYPE actorName2Tag(const std::string& aSwarmName, const std::string& anActorName);
	std::string actorTag2Name(const std::string& aSwarmName, ACTOR_TAG_TYPE aTag);

	SWARM_TAG_TYPE swarmName2Tag(const std::string& aSwarmName);
	std::string swarmTag2Name(SWARM_TAG_TYPE aTag);

	PLUGIN_TAG_TYPE pluginName2Tag(const std::string& aPluginName);
	std::string pluginTag2Name(PLUGIN_TAG_TYPE aTag);

private:
    void updateActorTagStore(const std::string& aSwarmName, const std::vector<std::string>& anActorNames, const std::vector<std::string>& aPluginNames);

private:
    typedef std::map<std::string, std::pair<SWARM_TAG_TYPE, std::map<std::string, ACTOR_TAG_TYPE> > >  SWARM2ACTORMAP;
    SWARM2ACTORMAP _swarm2Actor;// swarmName->{swarmTag, actorName->actorTag}
    std::map<std::string, PLUGIN_TAG_TYPE> _pluginName2Tag;
    boost::shared_mutex _swarm2ActorMutex;
};
ActorTagStorage gActorTagStore2;
#endif