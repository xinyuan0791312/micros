/* ==================================================================
*
* Copyright (c) 2016-2019, micROS Group, NIIDT, TAIIC, HPCL.
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions 
* are met: 
*
* 1. Redistributions of source code must retain the above copyright 
*     notice, this list of conditions and the following disclaimer. 
* 2. Redistributions in binary form must reproduce the above copyright 
*     notice, this list of conditions and the following disclaimer in the 
*     documentation and/or other materials provided with the 
*     distribution. 
* 3. All advertising materials mentioning features or use of this software 
*     must display the following acknowledgement: 
*     This product includes software developed by the micROS Group. and 
*     its contributors. 
* 4. Neither the name of the Group nor the names of its contributors may 
*     be used to endorse or promote products derived from this software 
*     without specific prior written permission. 
*
* THIS SOFTWARE IS PROVIDED BY MICROS,GROUP AND CONTRIBUTORS
* ''AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT
* NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
* FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL 
* THE MICROS,GROUP OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS 
* OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) 
* HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
* OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN 
* IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
* ===================================================================
*
* Author: Bin Di, Chunlian Fu, Junling Gao, Ruihao Li, Wei Yi.
*         (in alphabetical order)
*
*/

#ifndef __ACTOR_DATA_STORE__
#define __ACTOR_DATA_STORE__

#include <map>
#include <string>
#include <stdint.h>
#include "RTPSPublisher.h"
#include "RTPSSubscriber.h"
#include "TagList.h"
#include "TagListRequest.h"
#include "TagListBinary.h"
#include "tinyxml.h"
#include "actor_tag_store/actor_tag_store_options.h"

#include <ros/ros.h>
#include "std_msgs/String.h"

//note:
// member functions for ActorTagStore are not multi-thread safe
// data is initialized during the startup of actor_daemon_node and readonly thereafter


class ActorTagStore {
public:
	ActorTagStore();
	~ActorTagStore();
	bool init(std::string aFilePath="");

	ACTOR_TAG_TYPE actorName2Tag(const std::string aSwarmName, const std::string& aName);
	std::string actorTag2Name(const std::string aSwarmName, ACTOR_TAG_TYPE aTag);

	SWARM_TAG_TYPE swarmName2Tag(const std::string& aName);
	std::string swarmTag2Name(SWARM_TAG_TYPE aTag);

	PLUGIN_TAG_TYPE pluginName2Tag(const std::string& aName);
	std::string pluginTag2Name(PLUGIN_TAG_TYPE aTag);

private:
	bool doTagDictConflictTest();
	bool interpretXML(TiXmlDocument& aDoc);
	bool interpretBinary(TagListBinary& binary);
	void tagListCallback(TagList& aTagList);
	void tagListBinaryCallback(TagListBinary& aTagListBinary);

	std::map<ACTOR_TAG_TYPE,std::string> _actorTag2Name;
	std::map<std::string,ACTOR_TAG_TYPE> _actorName2Tag;
	std::map<SWARM_TAG_TYPE,std::string> _swarmTag2Name;
	std::map<std::string,SWARM_TAG_TYPE> _swarmName2Tag;
	std::map<std::string,PLUGIN_TAG_TYPE> _pluginName2Tag;
	bool _tagListReady;

#ifndef USING_BINARY_TAGDICT
	RTPSSubscriber<TagList> *_pSub;
#else
	RTPSSubscriber<TagListBinary> *_pSub;
#endif
	RTPSPublisher<TagListRequest> *_pPub;

    //TODO add this for information group
    void tagListReqCallback(const std_msgs::String::ConstPtr aMsg);
    ros::Publisher _pubTagListResp;
    ros::Subscriber _subTagListReq;

};

extern ActorTagStore gActorTagStore;

#endif
