/* ==================================================================
*
* Copyright (c) 2016-2019, micROS Group, NIIDT, TAIIC, HPCL.
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions 
* are met: 
*
* 1. Redistributions of source code must retain the above copyright 
*     notice, this list of conditions and the following disclaimer. 
* 2. Redistributions in binary form must reproduce the above copyright 
*     notice, this list of conditions and the following disclaimer in the 
*     documentation and/or other materials provided with the 
*     distribution. 
* 3. All advertising materials mentioning features or use of this software 
*     must display the following acknowledgement: 
*     This product includes software developed by the micROS Group. and 
*     its contributors. 
* 4. Neither the name of the Group nor the names of its contributors may 
*     be used to endorse or promote products derived from this software 
*     without specific prior written permission. 
*
* THIS SOFTWARE IS PROVIDED BY MICROS,GROUP AND CONTRIBUTORS
* ''AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT
* NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
* FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL 
* THE MICROS,GROUP OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS 
* OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) 
* HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
* OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN 
* IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
* ===================================================================
*
* Author: Bin Di, Chunlian Fu, Junling Gao, Ruihao Li, Wei Yi.
*         (in alphabetical order)
*
*/

#include "TagDictServiceProvider.h"

TagDictServiceProvider::TagDictServiceProvider() {
	_count=0;
}

//broadcast once when receiving a TagListReq, limited frequency 1Hz
void TagDictServiceProvider::tagListRequestCallback(TagListRequest& aMsg) {
	//calc time interval between last bcast and the current req in microsec
	double now = time(NULL);
	double duration=difftime(now,_lasttime);
	if (duration>=1) {
		_count++;
		printf("INFO: publishing the taglist for %d times\n",count);
		TagList tagList;
		tagList.data(gTagListStr.c_str());
		gPPub->publish(tagList);
		lasttime=now;
	}
}

void init() {
	gTagListStr="\
<actor><name>observer</name><tag>1</tag></actor>\
<actor><name>leader</name><tag>2</tag></actor>\
<actor><name>follower</name><tag>3</tag></actor>\
<actor><name>attacker</name><tag>4</tag></actor>\
		";
	std::string filePath;
	ros::NodeHandle nh;
	if (nh.getParam("taglistdict", filePath)) {
		TiXmlDocument doc(filePath);
		printf("try to get taglistdict from %s\n",filePath.c_str());
		if (doc.LoadFile()) {
			TiXmlPrinter printer;
			//printer.SetIndent( "    " );
			doc.Accept( &printer );
			gTagListStr = printer.CStr();
		}
	}
}

int main(int argc, char** argv)
{
	ros::init(argc,argv,"taglist_service_provider");
	micROS::init();
	
	init();

	gPPub=new RTPSPublisher<TagList>("TagList");
	gPPub->init();
	lasttime=time(NULL);
	RTPSSubscriber<TagListRequest> mysub("TagListRequest",tagListRequestCallback);
	mysub.init();
	while (true) {
		usleep(1000);
	}
	delete gPPub;

	micROS::finish();
	return 0;
}
