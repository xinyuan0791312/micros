/* ==================================================================
*
* Copyright (c) 2016-2019, micROS Group, NIIDT, TAIIC, HPCL.
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions 
* are met: 
*
* 1. Redistributions of source code must retain the above copyright 
*     notice, this list of conditions and the following disclaimer. 
* 2. Redistributions in binary form must reproduce the above copyright 
*     notice, this list of conditions and the following disclaimer in the 
*     documentation and/or other materials provided with the 
*     distribution. 
* 3. All advertising materials mentioning features or use of this software 
*     must display the following acknowledgement: 
*     This product includes software developed by the micROS Group. and 
*     its contributors. 
* 4. Neither the name of the Group nor the names of its contributors may 
*     be used to endorse or promote products derived from this software 
*     without specific prior written permission. 
*
* THIS SOFTWARE IS PROVIDED BY MICROS,GROUP AND CONTRIBUTORS
* ''AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT
* NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
* FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL 
* THE MICROS,GROUP OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS 
* OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) 
* HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
* OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN 
* IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
* ===================================================================
*
* Author: Bin Di, Chunlian Fu, Junling Gao, Ruihao Li, Wei Yi.
*         (in alphabetical order)
*
*/

#include "RTPSPublisher.h"
#include "RTPSSubscriber.h"

#include "micROSRTPSExt.h"
#include "TagListRequest.h"
#include "TagList.h"
#include "TagListBinary.h"
#include "tinyxml.h"
#include <time.h>
#include "ros/ros.h"
#include "actor_tag_store/actor_tag_store_options.h"
#include "actor_tag_store/ActorTagStoreTools.h"
//broadcast once when receiving a TagListReq, limited frequency 1Hz

#ifndef USING_BINARY_TAGDICT
RTPSPublisher<TagList>* gPPub;
#else
RTPSPublisher<TagListBinary>* gPPub;
#endif
std::string gTagListStr;
double  lasttime;
int count=0;

void tagListRequestCallback(TagListRequest& aMsg) {
	//calc time interval between last bcast and the current req in microsec
	double now = time(NULL);
	double duration=difftime(now,lasttime);
	if (duration>=1) {
		count++;
		printf("INFO: publishing the taglist for %d times\n",count);
#ifndef USING_BINARY_TAGDICT
		TagList tagList;
		tagList.data(gTagListStr.c_str());
#else 
		TagListBinary tagList;
		TagListBinary& tagListBinary=tagList;
		//todo: fill the content from interpreting the XML,
		//todo: do it only once
		TiXmlDocument doc;
		doc.Parse(gTagListStr.c_str());
		ACTOR_STR2TAG_TYPE actorName2Tag;
		actorName2Tag.clear();
		SWARM_STR2TAG_TYPE swarmName2Tag;
		swarmName2Tag.clear();
		PLUGIN_STR2TAG_TYPE pluginName2Tag;
		pluginName2Tag.clear();

		std::vector<ACTOR_TAG_TYPE>& actorTags=tagListBinary.actor_tags();
		std::vector<std::string>& actorNames=tagListBinary.actor_names();
			       
		std::vector<SWARM_TAG_TYPE>& swarmTags=tagListBinary.swarm_tags();
		std::vector<std::string>& swarmNames=tagListBinary.swarm_names();

		std::vector<PLUGIN_TAG_TYPE>& pluginTags=tagListBinary.plugin_tags();
		std::vector<std::string>& pluginNames=tagListBinary.plugin_names();

		if (interpretTagXML(doc,actorName2Tag,swarmName2Tag, pluginName2Tag)) {
			
			for (ACTOR_STR2TAG_TYPE::iterator iter1=actorName2Tag.begin();iter1!=actorName2Tag.end();iter1++) {
				actorNames.push_back(iter1->first);
				actorTags.push_back(iter1->second);
				//todo: test
			}
			for (SWARM_STR2TAG_TYPE::iterator iter2=swarmName2Tag.begin();iter2!=swarmName2Tag.end();iter2++) {
				swarmNames.push_back(iter2->first);
				swarmTags.push_back(iter2->second);	
				//todo: test
			}
			for (PLUGIN_STR2TAG_TYPE::iterator iter3=pluginName2Tag.begin();iter3!=pluginName2Tag.end();iter3++) {
				pluginNames.push_back(iter3->first);
				pluginTags.push_back(iter3->second);
				//todo: test
			}
			
		}
		else  {
			printf("ERROR: fail to interpret the tagListDict\n");
			return;
		}
#endif
		gPPub->publish(tagList);
		lasttime=now;
	}
}

void init() {
	gTagListStr="\
<actor><name>observer</name><tag>1</tag></actor>\
<actor><name>leader</name><tag>2</tag></actor>\
<actor><name>follower</name><tag>3</tag></actor>\
<actor><name>attacker</name><tag>4</tag></actor>\
		";
	std::string filePath;
	ros::NodeHandle nh;
	if (nh.getParam("taglistdict", filePath)) {
		TiXmlDocument doc(filePath);
		printf("try to get taglistdict from %s\n",filePath.c_str());
		if (doc.LoadFile()) {
			TiXmlPrinter printer;
			//printer.SetIndent( "    " );
			doc.Accept( &printer );
			gTagListStr = printer.CStr();

		}
	}
	assert(gTagListStr.size()<1024);
}

int main(int argc, char** argv)
{
	ros::init(argc,argv,"taglist_service_provider");
	ros::NodeHandle nh;
	
	 std::string strRobotListFile="";
    if (nh.hasParam("/robotListFile")) {
        nh.getParam("/robotListFile",strRobotListFile); 
    } else {
        ROS_WARN("cannot find /robotListFile in the parameter server");
    }
    
	int domainId = 0;
    if(nh.hasParam("/dds_domainid")){
        nh.getParam("/dds_domainid", domainId);
    } else {
        ROS_WARN("cannot find /dds_domainid in the parameter server, default 0");
    }
	ROS_WARN("DDS DomainId is %d", domainId);

#ifdef DDS_STATIC_DISCOVERY
	micROS::init();
#else
    micROS::init(domainId, strRobotListFile);
#endif
	
	init();
		
	std::string tagListTopic,tagListReqTopic;

	if (!nh.getParam("tag_list_req_topic", tagListReqTopic)) 
		tagListReqTopic="TagListRequest";
	if (!nh.getParam("tag_list_topic",tagListTopic))
		tagListTopic="TagList";

#ifndef USING_BINARY_TAGDICT
	gPPub=new RTPSPublisher<TagList>(tagListTopic);
#else
	gPPub=new RTPSPublisher<TagListBinary>(tagListTopic);
#endif
	lasttime=time(NULL);
	RTPSSubscriber<TagListRequest> mysub(tagListReqTopic,tagListRequestCallback);
	while (true) {
		usleep(1000);
	}
	delete gPPub;

	micROS::finish();
	return 0;
}
