/* ==================================================================
*
* Copyright (c) 2016-2019, micROS Group, NIIDT, TAIIC, HPCL.
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions 
* are met: 
*
* 1. Redistributions of source code must retain the above copyright 
*     notice, this list of conditions and the following disclaimer. 
* 2. Redistributions in binary form must reproduce the above copyright 
*     notice, this list of conditions and the following disclaimer in the 
*     documentation and/or other materials provided with the 
*     distribution. 
* 3. All advertising materials mentioning features or use of this software 
*     must display the following acknowledgement: 
*     This product includes software developed by the micROS Group. and 
*     its contributors. 
* 4. Neither the name of the Group nor the names of its contributors may 
*     be used to endorse or promote products derived from this software 
*     without specific prior written permission. 
*
* THIS SOFTWARE IS PROVIDED BY MICROS,GROUP AND CONTRIBUTORS
* ''AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT
* NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
* FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL 
* THE MICROS,GROUP OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS 
* OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) 
* HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
* OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN 
* IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
* ===================================================================
*
* Author: Bin Di, Chunlian Fu, Junling Gao, Ruihao Li, Wei Yi.
*         (in alphabetical order)
*
*/

#include "task_bind/task_bind.h"
#include "swarm_data_storage/swarm_data_storage.h"
#include "actor_tag_store/ActorTagStore.h"

TaskBinder::TaskBinder(ActorMatcher *aPActorMatcher, ActorScheduler *aPActorScheduler)
{
	this->_pActorMatcher = aPActorMatcher;
	this->_pActorScheduler = aPActorScheduler;
	this->_nh = ros::NodeHandle("~/");
}

// 20181113 For GStation Daemon Node
TaskBinder::TaskBinder(ActorScheduler *aPActorScheduler, string aGStationInitXmlFilePath)
{
	this->_pActorScheduler = aPActorScheduler;
	this->_nh = ros::NodeHandle("~/");
	this->_gstationInitXmlFilePath = aGStationInitXmlFilePath;
}

void TaskBinder::startService()
{
	_task_bind_server = _nh.advertiseService("task_bind_service", &TaskBinder::bindCallback, this);
	ROS_INFO("[Task Bind] Service initialized.");
}

// decompose GStation INI XML file and start GStation Actor
bool TaskBinder::startGStation()
{
	TiXmlDocument doc1;
	string xmlStr1;
	if ( !doc1.LoadFile(_gstationInitXmlFilePath) )
	{
		cout << "[Ground Station] Error while open GStation Initialization XML file!" << endl;
		return false;
	}
	else
	{
		TiXmlPrinter printer1;
		doc1.Accept(&printer1);
		xmlStr1 = printer1.CStr();
		pushSwarmTaskAcbs(xmlStr1);
		return true;
	}
}

bool TaskBinder::bindCallback(actor_msgs::task_bind::Request &aReq, actor_msgs::task_bind::Response &aRes)
{
	//check the state of platform, if Available for bind, then bind and return true
	ROS_INFO("[Task Bind] Service Confirmed.");

	// inquiry matcher for actor_match information
	// std::map<std::string, std::string> match_info_map;
	// int flag = _pActorMatcher->getActorMatchInfo(aReq.actor_name, match_info_map);

	// push back Swarm ACB
	if (pushSwarmTaskAcbs(aReq.str))
	{
		ROS_INFO("[Task Bind] Bind platform and generate ACB!");
		aRes.bind = true;
		return true;
	}
	else
	{
		// TODO: Process errors.
		ROS_ERROR("[Task Bind] Could not decode request XML data, no ACB generated!");
		aRes.bind = false;
		return false;
	}
}

//deprecated
//Generate ACB for a single actor
bool TaskBinder::decodeXML(const std::string aTaskXMLStr)
{
	// [DMY]: ToDo. Set ACB as inputs. Which requires that the task file only contains one actor.
	// decodeXML(const std::string aTaskXMLStr, ACB &oneACB)
	TiXmlDocument doc;
	// receive XML file as string
	if (aTaskXMLStr.size()==0) {
		ROS_FATAL("[Task Bind] Input XML string is empty!");
		return false;
	}
	doc.Parse(aTaskXMLStr.c_str());
	if (doc.Error()!=0) {
		ROS_FATAL("[Task Bind] Input XML file is not in correct format!");
		return false;
	}
	TiXmlElement *pEle;
	TiXmlHandle hDoc(&doc);
	pEle = hDoc.FirstChildElement().Element();	// FirstChildElement of XML is discarded
	int64_t taskID;
	string taskName;
	int cntTag[20];		// Used for ROS_ERROR
	for (int ii=0; ii<20; ii++) {
		cntTag[ii] = 0;
	}
	for (pEle = pEle->FirstChildElement(); pEle != NULL; pEle = pEle->NextSiblingElement()) {
		string tagName = pEle->Value();
		if (tagName == "_P_ID") {
			taskID = atoll(pEle->GetText());
			cntTag[0]++;
		} else if (tagName == "_P_Name") {
			taskName = pEle->GetText();
			cntTag[1]++;
		} else if (tagName == "ActorsConfig") {
			cntTag[2]++;
			// Iteration among actors
			for (TiXmlElement *pActors = pEle->FirstChildElement(); pActors != NULL; pActors = pActors->NextSiblingElement()) {
				ACB myACB;
				TaskInfo myTaskInfo;
				myACB.taskID = taskID;
				myACB.taskName = taskName;
				myTaskInfo.taskID = myACB.taskID;
				myTaskInfo.taskXMLStr = aTaskXMLStr;
				for (TiXmlElement *pAct = pActors->FirstChildElement(); pAct != NULL; pAct = pAct->NextSiblingElement()) {
					string actorTagName = pAct->Value();
					if (actorTagName == "_P_ID") {
						cntTag[3]++;
						myACB.actorID = atoll(pAct->GetText());
					} else if (actorTagName == "_P_Name") {
						cntTag[4]++;
						myACB.name = pAct->GetText();
						myTaskInfo.actorName = myACB.name;
					} else if (actorTagName == "_P_Prio") {
						cntTag[5]++;
						myACB.prio = atoll(pAct->GetText());
					} else if (actorTagName == "SensorsConfig")
					{ //sensors
						cntTag[6]++;
						for (TiXmlElement *pSensor = pAct->FirstChildElement(); pSensor != NULL; pSensor = pSensor->NextSiblingElement()) {
							SensorActuatorInfo saInfo;
							for (TiXmlElement *pSATag = pSensor->FirstChildElement(); pSATag != NULL; pSATag = pSATag->NextSiblingElement()) {
								string sensorTagName = pSATag->Value();
								if (sensorTagName == "_P_ID") {
									cntTag[7]++;
									saInfo.id = atoll(pSATag->GetText());
								} else if (sensorTagName == "_P_Type") {
									cntTag[8]++;
									saInfo.type = pSATag->GetText();
								} else if (sensorTagName == "Property") {
									cntTag[9]++;
									for (TiXmlElement *pProperty = pSATag->FirstChildElement(); pProperty != NULL; pProperty = pProperty->NextSiblingElement()) {
										saInfo.properties.insert(std::pair<std::string,std::string>(pProperty->Value(),pProperty->GetText()));
									}
								} else if (sensorTagName == "Param") {
									cntTag[10]++;
									for (TiXmlElement *pParam = pSATag->FirstChildElement(); pParam != NULL; pParam = pParam->NextSiblingElement()) {
										saInfo.params.insert(std::pair<std::string,std::string>(pParam->Value(),pParam->GetText()));
									}
								} else {
									ROS_ERROR("[Task Bind] Unknown Tag Name in SensorsConfig!");
								}
							}
							myACB.sensors.push_back(saInfo);
						}
					} else if (actorTagName == "ActuatorsConfig") {	//actuators
						cntTag[11]++;
						for (TiXmlElement *pActuator = pAct->FirstChildElement(); pActuator != NULL; pActuator = pActuator->NextSiblingElement()) {
							SensorActuatorInfo saInfo;
							for (TiXmlElement *pSATag = pActuator->FirstChildElement(); pSATag != NULL; pSATag = pSATag->NextSiblingElement()) {
								string actuatorTagName = pSATag->Value();
								if (actuatorTagName == "_P_ID") {
									cntTag[12]++;
									saInfo.id = atoll(pSATag->GetText());
								} else if (actuatorTagName == "_P_Type") {
									cntTag[13]++;
									saInfo.type = pSATag->GetText();
								} else if (actuatorTagName == "Property") {
									cntTag[14]++;
									for (TiXmlElement *pProperty = pSATag->FirstChildElement(); pProperty != NULL; pProperty = pProperty->NextSiblingElement()) {
										saInfo.properties.insert(std::pair<std::string,std::string>(pProperty->Value(),pProperty->GetText()));
									}
								} else if (actuatorTagName == "Param") {
									cntTag[15]++;
									for (TiXmlElement *pParam = pSATag->FirstChildElement(); pParam != NULL; pParam = pParam->NextSiblingElement()) {
										saInfo.properties.insert(std::pair<std::string,std::string>(pParam->Value(),pParam->GetText()));
									}
								} else {
									ROS_ERROR("[Task Bind] Unknown Tag Name in ActuatorsConfig!");
								}
							}
							myACB.actuators.push_back(saInfo);
						}
					} else if (actorTagName == "OODAConfig") {
						cntTag[16]++;
						string busName;
						string pluginName;
						for (TiXmlElement *pBus = pAct->FirstChildElement(); pBus != NULL; pBus = pBus->NextSiblingElement()) {	//Bus
							for (TiXmlElement *pBusTag = pBus->FirstChildElement(); pBusTag != NULL; pBusTag = pBusTag->NextSiblingElement()) {
								string oodaTagName = pBusTag->Value();
								if (oodaTagName == "_P_Name") {
									cntTag[17]++;
									busName = pBusTag->GetText();
								} else if (oodaTagName == "Plugin") {	//Plugins
									cntTag[18]++;
									for (TiXmlElement *pPlugin = pBusTag->FirstChildElement(); pPlugin != NULL; pPlugin = pPlugin->NextSiblingElement()) {
										pluginName = pPlugin->GetText();
										PluginInfo plugin(pluginName, busName);
										myTaskInfo.plugins.push_back(plugin);
									}
								} else {
									ROS_ERROR("[Task Bind] Unknown Tag Name in OODAConfig!");
								}
							}							
						}
						myACB.taskInfo = myTaskInfo;
					} else {
						ROS_ERROR("[Task Bind] Unknown Tag Name in ActorsConfig!");
					}
				}
				myACB.pParent = NULL;		// ToDo: Just for test!!!
				myACB.pChild = NULL;		// ToDo: Just for test!!!
				myACB.pSibling = NULL;		// ToDo: Just for test!!!
				myACB.state = ACTOR_STATE_INIT;			// ToDo: Just for test!!!
				myACB.pPlatformInfo = NULL;	// ToDo: Just for test!!!
				myACB.pSwarmInfo = NULL;	// ToDo: Just for test!!!
				
				//---------------------*** Error Indicator ***---------------------//
				if (cntTag[0]==0) {
					ROS_ERROR("[Task Bind] Task ID missing!");	return false;
				}
				if (cntTag[1]==0) {
					ROS_ERROR("[Task Bind] Task Name missing!");	return false;
				}
				if (cntTag[3]==0) {
					ROS_ERROR("[Task Bind] Actor ID missing!");	return false;
				}
				if (cntTag[4]==0) {
					ROS_ERROR("[Task Bind] Actor Name missing!");	return false;
				}
				if (cntTag[6]==0) ROS_ERROR("[Task Bind] No Sensor!");
				if (cntTag[11]==0) ROS_ERROR("[Task Bind] No Actuator!");
				if (cntTag[16]==0) ROS_ERROR("[Task Bind] OODA info missing!");
				if (cntTag[17]==0) ROS_ERROR("[Task Bind] Softbus info missing!");
				if (cntTag[18]==0) ROS_ERROR("[Task Bind] Plugin info missing!");
			
				//---------------------***** Append  ACB *****---------------------//
				_pActorScheduler->appendACB(myACB);
			}

		} else {
			ROS_ERROR("[Task Bind] Unknown Tag Name!");
		}
	}
	return true;
}

//deprecated
//Coded by YW
bool TaskBinder::initTaskFromXML(const std::string aTaskXMLStr, TaskInfo *pTask)
{
	ROS_ASSERT(pTask != NULL);
	pTask->taskXMLStr = aTaskXMLStr;
	pTask->taskID = 0; //initialize to unknown task
	TiXmlDocument doc;
	doc.Parse(aTaskXMLStr.c_str());
	TiXmlHandle docHandle(&doc);
	TiXmlElement *pBasicElement = docHandle.FirstChild("Basic").ToElement();
	if (!pBasicElement)
	{
		ROS_FATAL("[Task Bind] Basic tag missing in task XML");
	}
	const char *idStr = pBasicElement->Attribute("id");
	if (!idStr)
	{
		ROS_FATAL("[Task Bind] Basic/id tag missing in task XML");
	}
	pTask->taskID = atoll(idStr);

	const char *actorName = pBasicElement->Attribute("actorName");
	if (!actorName)
	{
		ROS_FATAL("[Task Bind] Basic/actorName tag missing in task XML");
	}
	pTask->actorName = actorName;

	TiXmlElement *pOODAElement = docHandle.FirstChild("OODAConfig").ToElement();
	if (!pOODAElement)
	{
		ROS_FATAL("[Task Bind] OODAConfig tag missing in task XML");
	}
	pTask->plugins.clear();
	for (TiXmlElement *pBusElement = pOODAElement->FirstChildElement("Bus"); pBusElement; pBusElement = pBusElement->NextSiblingElement("Bus"))
	{
		const char *busName = pBusElement->Attribute("name");
		ROS_INFO("[Task Bind] start Reading plugins on %s", busName);
		for (TiXmlElement *pPluginElement = pBusElement->FirstChildElement("Plugin"); pPluginElement; pPluginElement = pPluginElement->NextSiblingElement("Plugin"))
		{
			const char *pluginName = pPluginElement->Attribute("name");
			ROS_INFO("[Task Bind] found %s plugin", pluginName);
			pTask->plugins.push_back(PluginInfo(std::string(pluginName), std::string(busName)));
		}
	}
	return true;
}

//---------------------********************* split line *********************---------------------//
//---------------------********* new functions based on ROS pub&sub *********---------------------//
//---------------------********************** 20180713 **********************---------------------//
TaskBinder::TaskBinder(int64_t aPlatformID)
{
	this->_pActorMatcher = NULL;
	this->_pActorScheduler = NULL;
	this->_platformID = aPlatformID;
	this->_curSwarmTaskXmlStr = "";
	this->_curSwarmMatchXmlStr = "";
	this->_localTaskAssignment.actorName.clear();
	this->_localTaskAssignment.platformID.clear();
	cout << "[Task Bind] Task Binder constructed!" << endl;
}

void TaskBinder::start()
{
	ros::Subscriber sub1 = _nh.subscribe("/swarm_task_requirements", 10000, &TaskBinder::swarmMatchCallback, this);
	ros::Subscriber sub2 = _nh.subscribe("/swarm_task_assignments", 10000, &TaskBinder::swarmTaskCallback, this);
	ROS_INFO("[Task Bind] Swarm Task Assignment Initialized.");
	ros::spin();
}

void TaskBinder::swarmMatchCallback(const actor_msgs::swarm_task& msg)
{
	// only process new assignments
	if (msg.swarmMatchXmlStr != _curSwarmMatchXmlStr) {
		ROS_INFO("[Task Bind] Platform %ld received /swarm_task_requirements", _platformID);
		_curSwarmMatchXmlStr = msg.swarmMatchXmlStr;
		// match and response
		actor_msgs::swarm_task resp;
		resp.platformID = _platformID;
		ros::Publisher pub = _nh.advertise<actor_msgs::swarm_task>("/swarm_task_response", 10000);
		ROS_INFO("[Task Bind] Platform %ld About to publish /swarm_task_response.", _platformID);
		for (int i = 0; i < 5; i++)
		{
			pub.publish(resp);
			sleep(1);
		}
		ROS_INFO("[Task Bind] Platform %ld Publish /swarm_task_response.", _platformID);
	}
	else {
		ROS_INFO("[Task Bind] Platform %ld: /swarm_task_requirements received before.", _platformID);
		return;
	}
}

void TaskBinder::swarmTaskCallback(const actor_msgs::swarm_task& msg)
{
	bool flag = false;
	for (int i=0; i<msg.taskAssignment.size(); i++) {
		if (msg.taskAssignment[i] == _platformID) {
			flag = true;
			break;
		}
	}
	// only process new assignments
	if ( flag && (msg.swarmTaskXmlStr != _curSwarmTaskXmlStr) ) {
		ROS_INFO("[Task Bind] Platform %ld received /swarm_task_assignments", _platformID);

		int actorNum = msg.actorName.size();
		for (int i = 0; i < actorNum; i++)
		{
			string tempStr = msg.actorName[i];
			int64_t tempID = msg.taskAssignment[i];
			ROS_INFO("[Task Bind] Platform %ld Assignments: %s, %ld", _platformID, tempStr.c_str(), tempID);
		}

		_curSwarmTaskXmlStr = msg.swarmTaskXmlStr;
		// push swarm ACBs
		pushSwarmTaskAcbs(_curSwarmTaskXmlStr);
		// publish confirmation
		actor_msgs::swarm_task resp;
		resp.platformID = _platformID;
		resp.confirmAssignment = 1;
		ros::Publisher pub = _nh.advertise<actor_msgs::swarm_task>("/task_assignment_confirmation", 10000);
		ROS_INFO("[Task Bind] Platform %ld About to publish /task_assignment_confirmation.", _platformID);
		for (int i = 0; i < 5; i++)
		{
			pub.publish(resp);
			sleep(1);
		}
		ROS_INFO("[Task Bind] Platform %ld Publish /task_assignment_confirmation.", _platformID);
	}
	else {
		ROS_INFO("[Task Bind] Platform %ld: /swarm_task_assignments received before.", _platformID);
		return;
	}
}

//---------------------************************ split line ************************---------------------//
//---------------------************* new functions based on FAST-RTPS *************---------------------//
//---------------------************************* 20180730 *************************---------------------//
TaskBinder::TaskBinder(ActorMatcher *aPActorMatcher, ActorScheduler *aPActorScheduler, int64_t aPlatformID)
{
	this->_pActorMatcher = aPActorMatcher;
	this->_pActorScheduler = aPActorScheduler;
	this->_nh = ros::NodeHandle("~/");
	this->_platformID = aPlatformID;
	this->_curSwarmTaskXmlStr = "";
	this->_curSwarmMatchXmlStr = "";
	this->_localTaskAssignment.actorName.clear();
	this->_localTaskAssignment.platformID.clear();
	_newTaskArrived=false;
	_currentSwarmID = -1;

	//_pSubReq = new RTPSSubscriber<SwarmTask>("/swarm_task_requirements", &TaskBinder::swarmMatchCallback0, this);
	//_pSubAssign = new RTPSSubscriber<SwarmTask>("/swarm_task_assignments", &TaskBinder::swarmTaskCallback0, this);
	//_pPubResp = new RTPSPublisher<SwarmTask>("/swarm_task_response");
	//_pPubConfirm = new RTPSPublisher<SwarmTask>("/task_assignment_confirmation");

	// add by gjl
	_pSubTaskXml = new RTPSSubscriber<StringMsg>("/swarm_task_xml", &TaskBinder::gStationSwarmTaskCallback, this);
	_pPubTaskXmlResponse = new RTPSPublisher<StringMsg>("/swarm_task_xml_resp");

	ros::NodeHandle nodeH;
  if(nodeH.hasParam("/task_xml_path")){
      nodeH.getParam("/task_xml_path", _task_path);
  } else {
      ROS_ERROR("cannot find /task_xml_path in the parameter server!!!");
  }

  _subRosTaskXml = nodeH.subscribe("swarm_task_xml", 10000, &TaskBinder::swarmTaskXmlCallback, this);
}

TaskBinder::TaskBinder(ActorMatcher *aPActorMatcher, ActorScheduler *aPActorScheduler, ActorStateMachine* anActorStateMachine, int64_t aPlatformID)
:TaskBinder(aPActorMatcher, aPActorScheduler, aPlatformID){
	_pActorStateMachine = anActorStateMachine;
}

TaskBinder::~TaskBinder() {
	if (_pSubReq != NULL)
	{
		delete _pSubReq;
		_pSubReq = NULL;
	}
	if (_pSubAssign != NULL)
	{
		delete _pSubAssign;
		_pSubAssign = NULL;
	}
	if (_pPubResp != NULL)
	{
		delete _pPubResp;
		_pPubResp = NULL;
	}
	if (_pPubConfirm != NULL)
	{
		delete _pPubConfirm;
		_pPubConfirm = NULL;
	}
}

void TaskBinder::startAutoTask() {
#ifdef AUTO_RUN_TASK
    ros::NodeHandle nodeH;
    if(nodeH.hasParam("/task_xml_name")){
        std::string name;
        nodeH.getParam("/task_xml_name", name);        
        ///< Setting StringMsg
        StringMsg strMsg;
		strMsg.sendID(-1);
		strMsg.destID(-1);
		strMsg.cmdNo(0);
		strMsg.reply(0);
        strMsg.data(name);
        ///< Call task xml parse
        gStationSwarmTaskCallback(strMsg);
    } else {
        ROS_ERROR("cannot find /task_xml_name in the parameter server!!!");
    }
#endif
}

void TaskBinder::start0()
{
	if (!_pSubReq->init()) {
		cout << "[Ground Station] Failed to initialize RTPSSubscriber for /swarm_task_requirements!" << endl;
		return;
	}
	if (!_pPubResp->init()) {
		cout << "[Ground Station] Failed to initialized RTPSPublisher for /swarm_task_response!" << endl;
		return;
	}
	if (!_pSubAssign->init()) {
		cout << "[Ground Station] Failed to initialized RTPSSubscriber for /swarm_task_assignments!" << endl;
		return;
	}
	if (!_pPubConfirm->init()) {
		cout << "[Ground Station] Failed to initialized RTPSPublisher for /task_assignment_confirmation!" << endl;
		return;
	}

	cout << "[Task Bind] Task Binder constructed!" << endl;
	// subscribeRequirement();
	// publishResponse();
	// subscriberAssignment();
	// publishConfirmation();
}

void TaskBinder::subscribeRequirement()
{
	// RTPSSubscriber<SwarmTask> sub("/swarm_task_requirements", &TaskBinder::swarmMatchCallback0, this);
	cout << "[Task Bind] Wait for subscribing /swarm_task_requirements." << endl;
	for (int i = 0; i < 5; i++)
	{
		sleep(1);
	}
}

void TaskBinder::subscriberAssignment()
{
	// RTPSSubscriber<SwarmTask> sub("/swarm_task_assignments", &TaskBinder::swarmTaskCallback0, this);
	cout << "[Task Bind] Wait for subscribing /swarm_task_assignments." << endl;
	for (int i = 0; i < 5; i++)
	{
		sleep(1);
	}
}

void TaskBinder::publishResponse()
{
	SwarmTask resp;
	resp.platformID(_platformID);
	// RTPSPublisher<SwarmTask> pub("/swarm_task_response");
		
	cout << "[Task Bind] Platform " << _platformID << " is about to publish /swarm_task_response." << endl;

	for (int i = 0; i < 5; i++)
	{
		cout << "[Task Bind] Publishing /swarm_task_response!" << endl;
		_pPubResp->publish(resp);
		sleep(1);
	}
}

void TaskBinder::publishConfirmation()
{
	SwarmTask resp;
	resp.platformID(_platformID);
	resp.confirmAssignment(1);
	// RTPSPublisher<SwarmTask> pub("/task_assignment_confirmation");
		
	cout << "[Task Bind] Platform " << _platformID << " is about to publish /task_assignment_confirmation." << endl;

	for (int i = 0; i < 5; i++)
	{
		cout << "[Task Bind] Publishing /task_assignment_confirmation!" << endl;
		_pPubConfirm->publish(resp);
		sleep(1);
	}
}

void TaskBinder::swarmMatchCallback0(SwarmTask& msg)
{
	cout << "[Task Bind] in /swarm_task_requirements callbacks!" << endl;
	// only process new assignments
	if (msg.swarmMatchXmlStr() != _curSwarmMatchXmlStr) {
		// ROS_INFO("[Task Bind] Platform %ld received /swarm_task_requirements", _platformID);
		cout << "[Task Bind] Platform " << _platformID << " received /swarm_task_requirements" << endl;
		_curSwarmMatchXmlStr = msg.swarmMatchXmlStr();
		_newTaskArrived=true;
		publishResponse();
	}
	else {
		// ROS_INFO("[Task Bind] Platform %ld: /swarm_task_requirements received before.", _platformID);
		cout << "[Task Bind] Platform " << _platformID << " : /swarm_task_requirements received before." << endl;
		return;
	}
}

void TaskBinder::swarmTaskCallback0(SwarmTask& msg)
{
	cout << "[Task Bind] in /swarm_task_assignments callbacks!" << endl;

	bool flag = false;
	
	// original idl use sequence, commented for debug, remember to uncommmented after debug
	// std::vector<int64_t>tempTaskAssignment = msg.taskAssignment();
	// for (int i=0; i<tempTaskAssignment.size(); i++) {
	// 	if (tempTaskAssignment[i] == _platformID) {
	// 		flag = true;
	// 		break;
	// 	}
	// }
	// debug only: task assignment
	string tempTaskAssignmentListStr = msg.taskAssignmentList();
	stringstream ss;
	string platformIDStr;
	ss.clear();
	ss << _platformID;
	ss >> platformIDStr;
	if (tempTaskAssignmentListStr.find(platformIDStr) != std::string::npos) {
		flag = true;
	}
	std::vector<int64_t>tempTaskAssignment;
	string tempStr1, tempStr2;
	tempStr1 = tempTaskAssignmentListStr;
	while (tempStr1.find(";") != std::string::npos) {
		std::size_t pos1 = tempStr1.find(";");
		tempStr2 = tempStr1.substr(0, pos1);
		tempStr1 = tempStr1.substr(pos1+1);
		ss.clear();
		ss << tempStr2;
		int64_t tempID;
		ss >> tempID;
		tempTaskAssignment.push_back(tempID);
	}
	// only for debug: actor names
	std::vector<string> tempActorName;
	string tempActorNameListStr = msg.actorNameList();
	tempStr1 = tempActorNameListStr;
	while (tempStr1.find(";") != std::string::npos) {
		std::size_t pos1 = tempStr1.find(";");
		tempStr2 = tempStr1.substr(0, pos1);
		tempStr1 = tempStr1.substr(pos1+1);
		tempActorName.push_back(tempStr2);
	}

	// only process new assignments
	if ( flag && (msg.swarmTaskXmlStr() != _curSwarmTaskXmlStr) ) {
		// ROS_INFO("[Task Bind] Platform %ld received /swarm_task_assignments", _platformID);
		cout << "[Task Bind] Platform " << _platformID << " received /swarm_task_assignments" << endl;
		// commented only for debug, remember to uncommented after debug
		// std::vector<string> tempActorName = msg.actorName();
		// only for debug
		cout << "[Task Bind] Actor List:" << endl;
		cout << tempActorNameListStr << endl;
		cout << "[Task Bind] Task Assignment:" << endl;
		cout << tempTaskAssignmentListStr << endl;
		int actorNum = tempActorName.size();
		for (int i = 0; i < actorNum; i++)
		{
			string tempStr = tempActorName[i];
			int64_t tempID = tempTaskAssignment[i];
			// ROS_INFO("[Task Bind] Platform %ld Assignments: %s, %ld", _platformID, tempStr.c_str(), tempID);
			cout << "[Task Bind] Platform " << _platformID << " Assignments: " << tempStr << ", " << tempID << endl;
		}
		_localTaskAssignment.actorName = tempActorName;
		_localTaskAssignment.platformID = tempTaskAssignment;

		_curSwarmTaskXmlStr = msg.swarmTaskXmlStr();
		// push swarm ACBs
		pushSwarmTaskAcbs(_curSwarmTaskXmlStr);
		// publish confirmation
		publishConfirmation();
	}
	else {
		// ROS_INFO("[Task Bind] Platform %ld: /swarm_task_assignments received before.", _platformID);
		cout << "[Task Bind] Platform " << _platformID << " : /swarm_task_assignments received before." << endl;
		return;
	}
}

//---------------------************************ split line ************************---------------------//
//---------------------************* Functions to Resolve XML Strings *************---------------------//
//---------------------************************* 20180822 *************************---------------------//
void TaskBinder::processSwarmInfo(TiXmlElement* aPEle, SwarmInfo& aSwarmInfo)
{
	TiXmlElement* pEle = aPEle;
	cout << "[Task Bind] Start processing SwarmInfo!" << endl;

	string swarmName, excluKey;
	int32_t swarmPrio;
	std::vector<string> excluActorName;

	for (pEle = pEle->FirstChildElement(); pEle != NULL; pEle = pEle->NextSiblingElement()) {
		string tagName = pEle->Value();
		if (tagName == "_P_SwarmName") {
			swarmName = pEle->GetText();
			aSwarmInfo.swarmName = swarmName;
		}
		if (tagName == "_P_SwarmTaskPrio") {
			swarmPrio = atol(pEle->GetText());
			aSwarmInfo.swarmPrio = swarmPrio;
		}
		if (tagName == "_ActorsConfig") {
			for (TiXmlElement *pActorType = pEle->FirstChildElement(); pActorType != NULL; pActorType = pActorType->NextSiblingElement()) {
				string actorType = pActorType->Value();
				if (actorType=="_GeneralActors") {
					// Iteration among actors
					for (TiXmlElement *pActors = pActorType->FirstChildElement(); pActors != NULL; pActors = pActors->NextSiblingElement())
					{
						for (TiXmlElement *pAct = pActors->FirstChildElement(); pAct != NULL; pAct = pAct->NextSiblingElement())
						{
							string actorTagName = pAct->Value();
							if (actorTagName == "_P_Name")
							{
								aSwarmInfo.generalActors.push_back( pAct->GetText() );
							}
						}
					} // done with all general Actors
				} else if (actorType=="_ExcluActors") {
					for (TiXmlElement *pExcluTag = pActorType->FirstChildElement(); pExcluTag != NULL; pExcluTag = pExcluTag->NextSiblingElement()) {
						string excluTag = pExcluTag->Value();
						if (excluTag == "_P_Name") {
							excluKey = pExcluTag->GetText();
						}
						if (excluTag == "Actor") {
							for (TiXmlElement *pAct = pActorType->FirstChildElement(); pAct != NULL; pAct = pAct->NextSiblingElement())
							{
								string actorTagName = pAct->Value();
								if (actorTagName == "_P_Name")
								{
									excluActorName.push_back( pAct->GetText() );
								}
							}
						}
					} // done with all exclusive Actors
					aSwarmInfo.excluActors.insert(std::pair<std::string,std::vector<std::string> >(excluKey, excluActorName));
				} else if (actorType=="_DynamicActors") {
					for (TiXmlElement *pDynamicTag = pActorType->FirstChildElement(); pDynamicTag != NULL; pDynamicTag = pDynamicTag->NextSiblingElement()) {
						string dynamicTag = pDynamicTag->Value();
						for (TiXmlElement *pAct = pActorType->FirstChildElement(); pAct != NULL; pAct = pAct->NextSiblingElement())
						{
							string actorTagName = pAct->Value();
							if (actorTagName == "_P_Name")
							{
								aSwarmInfo.dynamicActors.push_back( pAct->GetText() );
							}
						}
					} // done with all exclusive Actors
				} else {
					cout << "[Task Bind] Unknown Actor Type" << endl;
				}
			}
		}
	} // done with all Actors in Swarm
	aSwarmInfo.state = ACTOR_STATE_INIT;
}

void TaskBinder::processActors(TiXmlElement* aPActors, ACB& anACB, TaskInfo& aTaskInfo)
{
	TiXmlElement* pActors = aPActors;

	for (TiXmlElement *pAct = pActors->FirstChildElement(); pAct != NULL; pAct = pAct->NextSiblingElement())
	{
		string actorTagName = pAct->Value();
		if (actorTagName == "_P_ID")
		{
			anACB.actorID = atoll(pAct->GetText());
		}
		else if (actorTagName == "_P_Name")
		{
			anACB.name = pAct->GetText();
			aTaskInfo.actorName = anACB.name;
		}
		else if (actorTagName == "_P_Num")
		{
			//continue;
			anACB.actorNum = atoi(pAct->GetText());
		}
		else if (actorTagName == "_P_Prio")
		{
// old version: scheduling by adjusting the priority of actors
/*
			// anACB.prio = atol(pAct->GetText());
			bool assignFlag = false;
			int tempNum = _localTaskAssignment.actorName.size();
			for (int ii = 0; ii < tempNum; ii++)
			{
				string tempActorName = anACB.name;
				string tempStr = _localTaskAssignment.actorName[ii];
				int64_t tempID = _localTaskAssignment.platformID[ii];
				if ((tempStr.find(tempActorName) != std::string::npos) && (tempID == _platformID))
				{
					assignFlag = true;
					break;
				}
			}
			if (assignFlag)
			{
				anACB.prio = initPrior;
			}
			else
			{
				anACB.prio = initPrior + 1;
			}
*/
			anACB.prio = atol(pAct->GetText());
		}
		else if (actorTagName == "SensorsConfig")
		{ // sensors
			for (TiXmlElement *pSensor = pAct->FirstChildElement(); pSensor != NULL; pSensor = pSensor->NextSiblingElement())
			{
				SensorActuatorInfo saInfo;
				processSensorActuatorInfo(pSensor, saInfo);
				anACB.sensors.push_back(saInfo);
			}
		}
		else if (actorTagName == "ActuatorsConfig")
		{ // actuators
			for (TiXmlElement *pActuator = pAct->FirstChildElement(); pActuator != NULL; pActuator = pActuator->NextSiblingElement())
			{
				SensorActuatorInfo saInfo;
				processSensorActuatorInfo(pActuator, saInfo);
				anACB.actuators.push_back(saInfo);
			}
		}
		else if (actorTagName == "OODAConfig")
		{
			processTaskInfo(pAct, aTaskInfo);
			anACB.taskInfo = aTaskInfo;
		}
		else
		{
			// ROS_ERROR("[Task Bind] Unknown Tag Name in Actor: %s!", actorTagName.c_str());
			cout << "[Task Bind] Unknown Tag Name in Actor: " << actorTagName << endl;
		}
	}
	anACB.pParent = NULL;			// ToDo: Just for test!!!
	anACB.pChild = NULL;			// ToDo: Just for test!!!
	anACB.pSibling = NULL;			// ToDo: Just for test!!!
	// anACB.state = ACTOR_STATE_INIT; // ToDo: Just for test!!!
	bool assignFlag = false;
	int tempNum = _localTaskAssignment.actorName.size();
	for (int ii = 0; ii < tempNum; ii++)
	{
		string tempActorName = anACB.name;
		string tempStr = _localTaskAssignment.actorName[ii];
		int64_t tempID = _localTaskAssignment.platformID[ii];
		if ((tempStr.find(tempActorName) != std::string::npos) && (tempID == _platformID))
		{
			assignFlag = true;
			break;
		}
	}
	if (assignFlag)
	{
		anACB.state = ACTOR_STATE_INIT;
	}
	else
	{
		anACB.state = ACTOR_STATE_INIT_DELAY;
	}
	anACB.pPlatformInfo = NULL;		// ToDo: Just for test!!!
	anACB.pSwarmInfo = NULL;

	//---------------------*** Error Indicator ***---------------------//
	// TODO
}

void TaskBinder::processSensorActuatorInfo(TiXmlElement* aPSensorActuator, SensorActuatorInfo& aSAInfo)
{
	TiXmlElement *pSensorActuator = aPSensorActuator;
	for (TiXmlElement *pSATag = pSensorActuator->FirstChildElement(); pSATag != NULL; pSATag = pSATag->NextSiblingElement())
	{
		string actuatorTagName = pSATag->Value();
		if (actuatorTagName == "_P_ID")
		{
			aSAInfo.id = atoll(pSATag->GetText());
		}
		else if (actuatorTagName == "_P_Type")
		{
			aSAInfo.type = pSATag->GetText();
		}
		else if (actuatorTagName == "Property")
		{
			for (TiXmlElement *pProperty = pSATag->FirstChildElement(); pProperty != NULL; pProperty = pProperty->NextSiblingElement())
			{
				aSAInfo.properties.insert(std::pair<std::string, std::string>(pProperty->Value(), pProperty->GetText()));
			}
		}
		else if (actuatorTagName == "Param")
		{
			for (TiXmlElement *pParam = pSATag->FirstChildElement(); pParam != NULL; pParam = pParam->NextSiblingElement())
			{
				aSAInfo.properties.insert(std::pair<std::string, std::string>(pParam->Value(), pParam->GetText()));
			}
		}
		else
		{
			// ROS_ERROR("[Task Bind] Unknown Tag Name in ActuatorsConfig: %s!", actuatorTagName.c_str());
			cout << "[Task Bind] Unknown Tag Name in ActuatorsConfig: " << actuatorTagName << endl;
		}
	}
}

void TaskBinder::processTaskInfo(TiXmlElement* aPAct, TaskInfo& aTaskInfo)
{
	TiXmlElement *pAct = aPAct;
	string busName;
	string pluginName;
	for (TiXmlElement *pBus = pAct->FirstChildElement(); pBus != NULL; pBus = pBus->NextSiblingElement())
	{ // Bus
		for (TiXmlElement *pBusTag = pBus->FirstChildElement(); pBusTag != NULL; pBusTag = pBusTag->NextSiblingElement())
		{
			string oodaTagName = pBusTag->Value();
			if (oodaTagName == "_P_Name")
			{
				busName = pBusTag->GetText();
			}
			else if (oodaTagName == "Plugin")
			{ // Plugins
				for (TiXmlElement *pPlugin = pBusTag->FirstChildElement(); pPlugin != NULL; pPlugin = pPlugin->NextSiblingElement())
				{
					pluginName = pPlugin->GetText();
					PluginInfo plugin(pluginName, busName);
					aTaskInfo.plugins.push_back(plugin);
				}
			}
			else
			{
				// ROS_ERROR("[Task Bind] Unknown Tag Name in OODAConfig: %s!", oodaTagName.c_str());
				cout << "[Task Bind] Unknown Tag Name in OODAConfig: " << oodaTagName << endl;
			}
		}
	} // end of all busses
}

bool TaskBinder::pushSwarmTaskAcbs(const std::string aTaskXMLStr)
{
	std::vector<ACB> acbList;
	acbList.clear();
	TiXmlDocument doc;
	// receive XML file as string
	if (aTaskXMLStr.size()==0) {
		// ROS_FATAL("[Task Bind] Input XML string is empty!");
		cout << "[Task Bind] Input XML string is empty!" << endl;
		return false;
	}
	doc.Parse(aTaskXMLStr.c_str());
	if (doc.Error()!=0) {
		// ROS_FATAL("[Task Bind] Input XML file is not in correct format!");
		cout << "[Task Bind] Input XML file is not in correct format!" << endl;
		// cout << aTaskXMLStr << endl;
		return false;
	}
	TiXmlElement *pEle;
	TiXmlHandle hDoc(&doc);
	
	//---------------------********* process SwarmInfo *********---------------------//
	pEle = hDoc.FirstChildElement().Element();	// FirstChildElement of XML is discarded
	SwarmInfo mySwarmInfo;
	processSwarmInfo(pEle, mySwarmInfo);

	//---------------------********* process acbList *********---------------------//
	// ROS_INFO("[Task Bind] start processing acbList!");
	cout << "[Task Bind] start processing acbList!" << endl;
	int64_t taskID;
	string taskName;
	pEle = hDoc.FirstChildElement().Element();	// FirstChildElement of XML is discarded
	for (pEle = pEle->FirstChildElement(); pEle != NULL; pEle = pEle->NextSiblingElement()) {
		string tagName = pEle->Value();
		if (tagName == "_P_ID") {
			taskID = atoll(pEle->GetText());
		} else if (tagName == "_P_SwarmName") {
			taskName = pEle->GetText();
		} else if (tagName == "_ActorsConfig") {
			// ROS_INFO("[Task Bind] start processing ActorsConfig!");
			cout << "[Task Bind] start processing ActorsConfig!" << endl;
			for (TiXmlElement *pActorType = pEle->FirstChildElement(); pActorType != NULL; pActorType = pActorType->NextSiblingElement()) {
				string actorType = pActorType->Value();
				if (actorType=="_GeneralActors") {
					// ROS_INFO("[Task Bind] start processing GeneralActors!");
					cout << "[Task Bind] start processing GeneralActors!" << endl;
					// Iteration among actors
					for (TiXmlElement *pActors = pActorType->FirstChildElement(); pActors != NULL; pActors = pActors->NextSiblingElement())
					{
						ACB myACB;
						TaskInfo myTaskInfo;
						myACB.formationType = "Unset";		// formation type is initialized as "Unset"
						myACB.formationPos = 0;				// formation pos is initialized as 0
						myACB.taskID = taskID;
						myACB.taskName = taskName;
						myTaskInfo.taskID = myACB.taskID;
						myTaskInfo.taskXMLStr = aTaskXMLStr;
						processActors(pActors, myACB, myTaskInfo);
						acbList.push_back(myACB);
						// add by gjl
						ROS_INFO("[TaskBinder] setSwarmTaskActorInfo:%s,%s,%d,%d", taskName.c_str(), myACB.name.c_str(), myACB.actorNum, myACB.prio);
						SwarmDataStorage::instance()->setSwarmTaskActorInfo(taskName, myACB.name, GENERAL, myACB.actorNum, myACB.prio);
					} // end of Actors
					
				} else if (actorType=="_ExcluActors") {
					// ROS_INFO("[Task Bind] start processing ExcluActors!");
					cout << "[Task Bind] start processing ExcluActors!" << endl;
					for (TiXmlElement *pExcluTag = pActorType->FirstChildElement(); pExcluTag != NULL; pExcluTag = pExcluTag->NextSiblingElement()) {
						string excluTag = pExcluTag->Value();
						string excluKey;
						if (excluTag == "_P_Name") {
							excluKey = pExcluTag->GetText();
						} else if (excluTag == "Actor") {
							ACB myACB;
							TaskInfo myTaskInfo;
							myACB.formationType = "Unset";		// formation type is initialized as "Unset"
							myACB.formationPos = 0;				// formation pos is initialized as 0
							myACB.taskID = taskID;
							myACB.taskName = taskName;
							myTaskInfo.taskID = myACB.taskID;
							myTaskInfo.taskXMLStr = aTaskXMLStr;
							processActors(pExcluTag, myACB, myTaskInfo);
							acbList.push_back(myACB);
							// add by gjl
							ROS_INFO("[TaskBinder] setSwarmTaskActorInfo:%s,%s,%d,%d", taskName.c_str(), myACB.name.c_str(), myACB.actorNum, myACB.prio);
							SwarmDataStorage::instance()->setSwarmTaskActorInfo(taskName, myACB.name, EXCLUDE, myACB.actorNum, myACB.prio);
						} else {
							// ROS_ERROR("[Task Bind] Unknown Tag Name in ExcluActors: %s!", excluTag.c_str());
							cout << "[Task Bind] Unknown Tag Name in ExcluActors: " << excluTag << endl;
						}
					}
				} else if (actorType=="_DynamicActors") {
					// ROS_INFO("[Task Bind] start processing DynamicActors!");
					cout << "[Task Bind] start processing DynamicActors!" << endl;
					// Iteration among actors
					for (TiXmlElement *pActors = pActorType->FirstChildElement(); pActors != NULL; pActors = pActors->NextSiblingElement())
					{
						ACB myACB;
						TaskInfo myTaskInfo;
						myACB.formationType = "Unset";		// formation type is initialized as "Unset"
						myACB.formationPos = 0;				// formation pos is initialized as 0
						myACB.taskID = taskID;
						myACB.taskName = taskName;
						myTaskInfo.taskID = myACB.taskID;
						myTaskInfo.taskXMLStr = aTaskXMLStr;
						processActors(pActors, myACB, myTaskInfo);
						acbList.push_back(myACB);
						// add by gjl
						ROS_INFO("[TaskBinder] setSwarmTaskActorInfo:%s,%s,%d,%d", taskName.c_str(), myACB.name.c_str(), myACB.actorNum, myACB.prio);
						SwarmDataStorage::instance()->setSwarmTaskActorInfo(taskName, myACB.name, DYNAMIC, myACB.actorNum, myACB.prio);
					} // end of Actors
				} else {
					// ROS_ERROR("[Task Bind] Unknown Tag Name in ActorType: %s!", actorType.c_str());
					cout << "[Task Bind] Unknown Tag Name in ActorType: " << actorType << endl;
				}
			} 
		} else {
			if (tagName!="_P_SwarmName" && tagName!="_P_SwarmTaskPrio") {
				// ROS_ERROR("[Task Bind] Unknown Tag Name in SwarmTaskConfig: %s!", tagName.c_str());
				cout << "[Task Bind] Unknown Tag Name in SwarmTaskConfig: " << tagName << endl;
			}
		}
	} // done with all actors
	// ROS_INFO("[Task Bind] Start appending acbList!");
	cout << "[Task Bind] Start appending acbList!" << endl;
	_pActorScheduler->appendSwarmACBs(mySwarmInfo, acbList);

	return true;
}

// add by gjl, analyze task xml 
void TaskBinder::splitString(const std::string& aStr, const char aSep, std::vector<std::string>& aRet){
	aRet.clear();
	std::string::size_type pos1, pos2;
	pos2 = aStr.find(aSep);
	pos1 = 0;
	while(std::string::npos != pos2) {
		aRet.push_back(aStr.substr(pos1, pos2-pos1));
		pos1 = pos2 + 1;
		pos2 = aStr.find(aSep, pos1);
	}
	if(pos1 != aStr.length())
		aRet.push_back(aStr.substr(pos1));
}

void TaskBinder::processTaskXml(const std::string& aTaskXml) {

	// check params
	if (aTaskXml.size()==0) {
		cout << "[Task Bind] Input task xml file name is empty!" << endl;
		return;
	}

	TiXmlDocument doc;
	// doc.Parse(msg.data().c_str());
	// if (doc.Error() !=0 ) {
	// 	cout << "[Task Bind] Input XML file is not in correct format!" << endl;
	// 	return;
	// }
    std::string taskFile = _task_path + "/" + aTaskXml;
    ROS_INFO("============= task xml : %s ===========", taskFile.c_str());
    if (!doc.LoadFile(taskFile)) {
        cout << "[Task Bind] file ["<<taskFile<<"] is not in correct format!" << endl;
        return;
    }

	SwarmInfo swarmInfo;
	std::vector<ACB> acbList;
	std::vector<UTOActorNode> utoList;
	std::set<std::string> initialRobotIDs;
	std::vector<std::pair<int, std::string> > initialActors;

	TiXmlHandle hDoc(&doc);
	TiXmlElement *pEle = hDoc.FirstChildElement().Element();
	// if received the same swarm task, then return
	swarmInfo.swarmName = pEle->FirstChildElement("_P_Name")->GetText();
	if(std::find(_receivedSwarmTask.begin(), _receivedSwarmTask.end(), swarmInfo.swarmName) != _receivedSwarmTask.end()){
		return;
	}

	// analyze task xml
	for (pEle = pEle->FirstChildElement(); pEle != NULL; pEle = pEle->NextSiblingElement()) {
		string tagName = pEle->Value();
		if (tagName == "_P_ID") {
			swarmInfo.swarmID = atoll(pEle->GetText());
			if(_currentSwarmID >= 0 && swarmInfo.swarmID == _currentSwarmID) {
				ROS_WARN("[Task Bind] Input XML file with swarmID %ld is duplicate!", _currentSwarmID);
				return;
			}
			_currentSwarmID = swarmInfo.swarmID;
		} else if (tagName == "_P_Name") {
			swarmInfo.swarmName = pEle->GetText();
		} else if (tagName == "_P_TaskPrio") {
			swarmInfo.swarmPrio = atoi(pEle->GetText());
		} else if (tagName == "_ActorsConfig") {
			processActorConfigInXml(pEle, swarmInfo, acbList);
		} else if (tagName == "_UTOConfig") {
			processUTOConfigInXml(pEle, utoList);
		} else if (tagName == "_PlatformActorConfig") {
			processPlatformActorConfigInxml(pEle, initialActors, initialRobotIDs);
		} else {
			ROS_INFO("[TaskBind]unkown tag1:%s", tagName.c_str());
		}
	}
	if(!acbList.empty()){
		//gActorTagStore.updateActorTagStore(swarmInfo, acbList);
		if(_pActorScheduler)
			_pActorScheduler->appendSwarmACBs(swarmInfo, acbList);
	}

    // // //for test
	// cout<<"ACBList:"<<acbList.size()<<endl;
	// for(auto it = acbList.begin(); it != acbList.end(); it++){
	// 	cout<<it->actorID<<":"<<it->name<<"\t"<<it->actorNum<<"\t"<<it->prio<<endl;
	// }
	// cout<<"UTOList:"<<utoList.size()<<endl;
	// for(auto it = utoList.begin(); it!= utoList.end(); it++){
	// 	cout<<it->_actorName<<"\tparams:"<<endl;
	// 	for(auto it2 = it->_paramList.begin(); it2 != it->_paramList.end(); it2++){
	// 		cout<<it2->_key<<"\t"<<it2->_value<<"\t"<<it2->_type<<endl;
	// 	}
	// 	cout<<it->_actorName<<"\ttransitions:"<<endl;
	// 	for(auto it2 = it->_eventList.begin(); it2 != it->_eventList.end(); it2++){
	// 		cout<<it2->_eventName<<":"<<it2->_nextActor<<"\t"<<it2->_sysNum<<"\t"<<it2->_barrierKey<<endl;
	// 	}
	// }
	// cout<<"initialActors:"<<initialActors.size()<<endl;
	// for(auto it = initialActors.begin(); it != initialActors.end(); it++){
	// 	cout<<it->first<<"\t"<<it->second<<endl;
	// }processTaskXml
	// cout<<endl;
		
	if(_pActorStateMachine){
		_pActorStateMachine->setUTOList(utoList);
		_pActorStateMachine->setInitialActors(swarmInfo.swarmName, initialActors);
	}
    
	SwarmDataStorage::instance()->updateInitialRobotIDs(initialRobotIDs);

#ifdef AUTO_RUN_TASK
    ///< Parse task auto process setting xml
    parseTaskAutoProcessXml(utoList);
#endif
}

void TaskBinder::processActorConfigInXml(TiXmlElement* pEle, const SwarmInfo& aSwarmInfo, std::vector<ACB>& aACBList){
	aACBList.clear();
	cout<<"pEle:"<<pEle->Value()<<endl;
	for (TiXmlElement *pActorType = pEle->FirstChildElement(); pActorType != NULL; pActorType = pActorType->NextSiblingElement()) {
		string actorType = pActorType->Value();
		cout<<"pEle child:"<<actorType<<endl;
		if (actorType == "_GeneralActors" || actorType == "_ExcluActors" || actorType == "_DynamicActors") {
			for (TiXmlElement *pActors = pActorType->FirstChildElement("Actor"); pActors != NULL; pActors = pActors->NextSiblingElement("Actor")) {
				ACB myACB;
				TaskInfo myTaskInfo;
				myACB.formationType = "Unset"; // formation type is initialized as "Unset"
				myACB.formationPos = 0;		   // formation pos is initialized as 0
				myACB.taskID = aSwarmInfo.swarmID;
				myACB.taskName = aSwarmInfo.swarmName;
				myTaskInfo.taskID = myACB.taskID;
				myTaskInfo.taskXMLStr = "";
				processActors(pActors, myACB, myTaskInfo);
				myACB.state = ACTOR_STATE_INIT_DELAY;
				aACBList.push_back(myACB);

				ROS_INFO("[TaskBinder] setSwarmTaskActorInfo:%s,%s,%d,%d", myACB.taskName.c_str(), myACB.name.c_str(), myACB.actorNum, myACB.prio);
				ESwarmActorType type = GENERAL;
				if(actorType == "_ExcluActors") type = EXCLUDE;
				else if(actorType == "_DynamicActors") type = DYNAMIC;
				SwarmDataStorage::instance()->setSwarmTaskActorInfo(myACB.taskName, myACB.name, type, myACB.actorNum, myACB.prio);
			}
		} else {
			ROS_INFO("[TaskBind]unkown tag:%s", actorType.c_str());
		}
	}
}

void TaskBinder::processUTOConfigInXml(TiXmlElement* pEle, std::vector<UTOActorNode>& aUTOList){
	aUTOList.clear();
    UTOActorNode aUnit;
    UTOTransition event;
	ActorParam param;
	std::string tagName = "";
	std::string tmp;
    for (TiXmlElement *pActor = pEle->FirstChildElement("actor"); pActor != NULL; pActor = pActor->NextSiblingElement("actor")) {
        aUnit.clear();
        aUnit._actorName = pActor->Attribute("name");
		cout<<"actorname:"<<aUnit._actorName <<endl;
        //Get param element and transition element
        for (TiXmlElement *pParamEle = pActor->FirstChildElement(); pParamEle != NULL; pParamEle = pParamEle->NextSiblingElement()) {
            tagName = pParamEle->Value();
            if(tagName == "param") {
                param.clear();
                param._key = pParamEle->Attribute("name");
                param._value = pParamEle->Attribute("value");
				tmp.clear();
				pParamEle->QueryStringAttribute("type", &tmp);  ///< Cannot use pParamEle->Attribute, cus the returned value may be NULL
				if("distribute" == tmp) {
					param._type = PARAM_TYPE_DISTRIBUTE;
				} else {
					param._type = PARAM_TYPE_NORMAL;
				}
                if(!param._key.empty() && !param._value.empty()) {
					aUnit._paramList.push_back(param);
                }
            } else if(tagName == "transition") {
                event.clear();
				int barrierKey = -1;
				int timeout = DEFAULT_SUSPEND_DURATION;
                pParamEle->QueryIntAttribute("sysNum", &event._sysNum);
				pParamEle->QueryIntAttribute("barrierKey", &barrierKey);
				pParamEle->QueryIntAttribute("timeout", &timeout);
				event._timeout = (short)timeout;
				event._barrierKey = (short)barrierKey;
				event._eventName = pParamEle->Attribute("name");
				event._nextActor = pParamEle->FirstChildElement("successor")->GetText();
                aUnit._eventList.push_back(event);
            } else {
				ROS_INFO("[TaskBind]unkown tag2:%s", tagName.c_str());
			}
        }
        aUTOList.push_back(aUnit);
    }
}

void TaskBinder::processPlatformActorConfigInxml(TiXmlElement* pEle, std::vector<std::pair<int, std::string> >& aInitialActors, std::set<std::string>& aInitialRobotIDs){
	aInitialActors.clear();
	std::vector<std::string> splitList;
	int robotID = SwarmDataStorage::instance()->getLocalRobotID();
	int pcnt = 0;
	for (TiXmlElement *pPlatform = pEle->FirstChildElement("_InitialActor"); pPlatform != NULL; pPlatform = pPlatform->NextSiblingElement("_InitialActor")) {
		// read actors that will running on this platform
		std::string str = pPlatform->Attribute("platformlist");
		splitString(str, ',', splitList);
		aInitialRobotIDs.insert(splitList.begin(), splitList.end());
		pcnt = splitList.size();
		if(std::find(splitList.begin(), splitList.end(), std::to_string(robotID)) != splitList.end()){
			str = pPlatform->Attribute("name");
			splitString(str, ',', splitList);
			for(std::vector<std::string>::iterator it=splitList.begin(); it!=splitList.end(); it++){
				aInitialActors.push_back(std::make_pair(pcnt, *it));
			}
		}
	}
}

void TaskBinder::parseTaskAutoProcessXml(const std::vector<UTOActorNode>& anUtoList) {
    ///< Get config XML storage path
    ros::NodeHandle nodeH;
    std::string autoConfig;
    if(nodeH.hasParam("/task_auto_process_config")){
        nodeH.getParam("/task_auto_process_config", autoConfig);
    } else {
        ROS_ERROR("cannot find /task_auto_process_config in the parameter server!!!");
    }

    ///< load config xml
    TiXmlDocument doc;
    if (!doc.LoadFile(autoConfig)) {
        cout << "[Task Bind] task auto process config file ["<<autoConfig<<"] is not in correct format!" << endl;
        return;
    }

    ///< parse and save config date
    std::map<std::string, int> actor2Interval;

    TiXmlHandle hDoc(&doc);
	TiXmlElement *pEle = hDoc.FirstChildElement("actor_auto_switch").Element();
    const char *dInterval = pEle->Attribute("defaultInterval");
    int defaultInterval = 120;  ///< Default 120s
    if(dInterval) {
        defaultInterval = atoi(dInterval);
    }

    for(auto it = anUtoList.begin(); it != anUtoList.end(); it++) {
        actor2Interval[it->_actorName] = defaultInterval;
    }

    int interval = defaultInterval;
    std::string actor;
	for (pEle = pEle->FirstChildElement("actor"); pEle != NULL; pEle = pEle->NextSiblingElement()) {
        actor = pEle->Attribute("name");
        pEle->QueryIntAttribute("interval", &interval);
        actor2Interval[actor] = interval;
    }

    if(_pActorStateMachine) {
        _pActorStateMachine->setTaskAutoProcessConfig(actor2Interval);
    }
}

void TaskBinder::gStationSwarmTaskCallback(StringMsg& msg){
#ifndef AUTO_RUN_TASK
	int robotID = SwarmDataStorage::instance()->getLocalRobotID();
	if(msg.destID() != robotID && msg.destID() != -1) return;
	if(msg.reply() == 1) return;
	// response 
	StringMsg respMsg;
	respMsg.sendID(robotID);
	respMsg.destID(msg.sendID());
	respMsg.reply(1);
	respMsg.cmdNo(msg.cmdNo());
	respMsg.data("");
	_pPubTaskXmlResponse->publish(respMsg);
	ROS_INFO("[TaskBinder] receive task from gstation, and reply, msg size=%d", (int)msg.getCdrSerializedSize(msg));
#endif

    processTaskXml(msg.data());
}

void TaskBinder::swarmTaskXmlCallback(const actor_msgs::TaskBits &msg) {
    if(msg.xmlStr.empty()) return;
	processTaskXml(msg.xmlStr);
}