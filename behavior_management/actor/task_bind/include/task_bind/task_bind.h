/* ==================================================================
*
* Copyright (c) 2016-2019, micROS Group, NIIDT, TAIIC, HPCL.
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions 
* are met: 
*
* 1. Redistributions of source code must retain the above copyright 
*     notice, this list of conditions and the following disclaimer. 
* 2. Redistributions in binary form must reproduce the above copyright 
*     notice, this list of conditions and the following disclaimer in the 
*     documentation and/or other materials provided with the 
*     distribution. 
* 3. All advertising materials mentioning features or use of this software 
*     must display the following acknowledgement: 
*     This product includes software developed by the micROS Group. and 
*     its contributors. 
* 4. Neither the name of the Group nor the names of its contributors may 
*     be used to endorse or promote products derived from this software 
*     without specific prior written permission. 
*
* THIS SOFTWARE IS PROVIDED BY MICROS,GROUP AND CONTRIBUTORS
* ''AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT
* NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
* FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL 
* THE MICROS,GROUP OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS 
* OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) 
* HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
* OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN 
* IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
* ===================================================================
*
* Author: Bin Di, Chunlian Fu, Junling Gao, Ruihao Li, Wei Yi.
*         (in alphabetical order)
*
*/

#ifndef __TASK_BINDER__
#define __TASK_BINDER__

#include <tinyxml.h>
#include <stdlib.h>
#include <string>
#include <sstream>
#include "actor_match/actor_match.h"
#include "actor_schedule/actor_schedule.h"
#include "actor_state_machine/actor_state_machine.h"
#include "actor_msgs/swarm_task.h"
#include "ros/ros.h"
#include "actor_msgs/task_bind.h"
#include "actor_core/ACB.h"
#include "actor_core/actor_types.h"
#include "actor_core/actor_constant.h"

#include "task_bind/SwarmTask.h"
#include "RTPSPublisher.h"
#include "RTPSSubscriber.h"
#include "micROSRTPSExt.h"
#include "actor_msgs/TaskBits.h"

using namespace std;

const int8_t MODE_GSTATION = 0;
const int8_t MODE_PLATFORM = 1;

class TaskBinder {
public:
	TaskBinder(ActorMatcher* aPActorMatcher, ActorScheduler* aPActorScheduler);	
	TaskBinder(ActorMatcher *aPActorMatcher, ActorScheduler *aPActorScheduler, int64_t aPlatformID);
	TaskBinder(ActorMatcher *aPActorMatcher, ActorScheduler *aPActorScheduler, ActorStateMachine* anActorStateMachine, int64_t aPlatformID);
	TaskBinder(int64_t aPlatformID);
	~TaskBinder();
	void startService();

    /**
     * @brief Start auto process task, called in daemon node
     */
    void startAutoTask();

	bool initTaskFromXML(const std::string aTaskXMLStr,TaskInfo* pTask);	//deprecated
	bool decodeXML(const std::string aTaskXMLStr);							//deprecated
	
	ActorMatcher* _pActorMatcher;
	ActorScheduler* _pActorScheduler;
	ActorStateMachine* _pActorStateMachine;
	// only for testing ground station
	int initPrior = 10;
	struct LocalTaskAssignment {
		std::vector<string> actorName;
		std::vector<int64_t> platformID;
	};
	struct LocalTaskAssignment _localTaskAssignment;
	int64_t _platformID;
	std::string _curSwarmMatchXmlStr;
	std::string _curSwarmTaskXmlStr;
	
	void start();
	void swarmMatchCallback(const actor_msgs::swarm_task& msg);
	void swarmTaskCallback(const actor_msgs::swarm_task& msg);
	bool pushSwarmTaskAcbs(const std::string aTaskXMLStr);	
	// Based on FAST-RTPS
	void start0();
	void subscribeRequirement();
	void subscriberAssignment();
	void publishResponse();
	void publishConfirmation();
	void swarmMatchCallback0(SwarmTask& msg);
	void swarmTaskCallback0(SwarmTask& msg);
	// 20180821
	void processActors(TiXmlElement* aPActors, ACB& anACB, TaskInfo& aTaskInfo);
	void processSensorActuatorInfo(TiXmlElement* aPSensorActuator, SensorActuatorInfo& aSAInfo);
	void processTaskInfo(TiXmlElement* aPAct, TaskInfo& aTaskInfo);
	void processSwarmInfo(TiXmlElement* aPEle, SwarmInfo& aSwarmInfo);
	// 20181113 for GStation Daemon Node
	TaskBinder(ActorScheduler* aPActorScheduler, string aGStationInitXmlFilePath);
	string _gstationInitXmlFilePath;
	bool startGStation();

	// --------------add by gjl
private:
	void splitString(const std::string& aStr, const char aSep, std::vector<std::string>& aRet);
    void processTaskXml(const std::string& aTaskXml);
	void processActorConfigInXml(TiXmlElement* pEle, const SwarmInfo& aSwarmInfo, std::vector<ACB>& aACBList);
	void processUTOConfigInXml(TiXmlElement* pEle, std::vector<UTOActorNode>& aUTOList);
	void processPlatformActorConfigInxml(TiXmlElement* pEle, std::vector<std::pair<int, std::string> >& aInitialActors, std::set<std::string>& aInitialRobotIDs);
	void gStationSwarmTaskCallback(StringMsg& msg);

	/**
	 * @brief ROS callback of '/swarm_task_bits' topic which is used to receive task bits and XML msg from ground station.
	 * 
	 * @param[in] msg ROS msgs with task bits and XML
	 */
	void swarmTaskBitsCallback(const actor_msgs::TaskBits &msg);
    void swarmTaskXmlCallback(const actor_msgs::TaskBits &msg);
    ros::Subscriber _subRosTaskXml;

	int64_t _currentSwarmID;
private:
	RTPSSubscriber<StringMsg>* _pSubTaskXml; // 
	RTPSPublisher<StringMsg>* _pPubTaskXmlResponse;
	std::vector<std::string> _receivedSwarmTask;// <swarmname> 
    std::string _task_path;
	//-- add end

    void parseTaskAutoProcessXml(const std::vector<UTOActorNode>& anUtoList);
private:
	ros::NodeHandle _nh;
	ros::ServiceServer _task_bind_server;
	bool bindCallback(actor_msgs::task_bind::Request  &aReq, actor_msgs::task_bind::Response &aRes);
	bool _newTaskArrived;
	RTPSSubscriber<SwarmTask> *_pSubReq;
	RTPSSubscriber<SwarmTask> *_pSubAssign;
	RTPSPublisher<SwarmTask> *_pPubResp;
	RTPSPublisher<SwarmTask> *_pPubConfirm;
	int8_t _workMode;
};

#endif

