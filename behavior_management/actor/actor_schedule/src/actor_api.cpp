/* ==================================================================
*
* Copyright (c) 2016-2019, micROS Group, NIIDT, TAIIC, HPCL.
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions 
* are met: 
*
* 1. Redistributions of source code must retain the above copyright 
*     notice, this list of conditions and the following disclaimer. 
* 2. Redistributions in binary form must reproduce the above copyright 
*     notice, this list of conditions and the following disclaimer in the 
*     documentation and/or other materials provided with the 
*     distribution. 
* 3. All advertising materials mentioning features or use of this software 
*     must display the following acknowledgement: 
*     This product includes software developed by the micROS Group. and 
*     its contributors. 
* 4. Neither the name of the Group nor the names of its contributors may 
*     be used to endorse or promote products derived from this software 
*     without specific prior written permission. 
*
* THIS SOFTWARE IS PROVIDED BY MICROS,GROUP AND CONTRIBUTORS
* ''AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT
* NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
* FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL 
* THE MICROS,GROUP OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS 
* OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) 
* HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
* OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN 
* IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
* ===================================================================
*
* Author: Bin Di, Chunlian Fu, Junling Gao, Ruihao Li, Wei Yi.
*         (in alphabetical order)
*
*/

#include "actor_schedule/actor_schedule.h"
#include "actor_core/actor_api.h"
#include <string.h>

char* gPSharedData  = NULL;
ActorScheduler *gPActorScheduler = NULL;
int SHARED_DATA_SIZE = 32768;

//get actor name by actor ID
bool getActorName(int64_t anActorID,std::string &anActorName){
    if((gPActorScheduler==NULL)||(!gPActorScheduler)) return false;
    return gPActorScheduler->getActorNameByID(anActorID,anActorName);

}
//get plugin pointer by pluginname
bool getActorPlugin(int64_t anActorID,std::string aPluginName,boost::shared_ptr<general_bus::GeneralPlugin> &aPluginStr){
    if((gPActorScheduler==NULL)||(!gPActorScheduler)) return false;
    return gPActorScheduler->getActorPlugin(anActorID,aPluginName,aPluginStr);
}

extern int getActorPauseCount(int64_t anActorID){
    if ((gPActorScheduler == NULL) || (!gPActorScheduler))
        return 0;
    return gPActorScheduler->getActorPauseCount(anActorID);
}

int getSharedData(char* aPData,int aSize) {
    if (aSize>SHARED_DATA_SIZE) {
        aSize=SHARED_DATA_SIZE;
    }
    memcpy(aPData,gPSharedData,aSize);
    return aSize;
}

int setSharedData(char* aPData,int aSize) {
    if (aSize>SHARED_DATA_SIZE) {
        aSize=SHARED_DATA_SIZE;
    }
    memcpy(gPSharedData,aPData,aSize);
    return aSize;
}

void switchToActor(int64_t anActorID, std::string anActorName){
    ScheduleMsg scheMsg;
    scheMsg._type = SCHEDULE_CMD_SWITCH_PRIO;
    scheMsg._param1 = anActorID;
    assert(anActorName.size() < SCHE_MSGS_CHAR_SIZE);
    strcpy(scheMsg._param3, anActorName.c_str());
    scheMsg._param3[anActorName.size()] = 0;
    gPActorScheduler->appendScheduleMsg(scheMsg);
}

void activateActor(std::string anActorName){
    ScheduleMsg scheMsg;
    scheMsg._type = SCHEDULE_CMD_ACTIVATE_ACTOR;
    assert(anActorName.size() < SCHE_MSGS_CHAR_SIZE);
    strcpy(scheMsg._param3, anActorName.c_str());
    scheMsg._param3[anActorName.size()] = 0;
    gPActorScheduler->appendScheduleMsg(scheMsg);
}

void pauseActorApi(int64_t anActorID){
    ScheduleMsg scheMsg;
    scheMsg._type = SCHEDULE_CMD_YIELD_ACTOR;
    scheMsg._param1 = anActorID;
    gPActorScheduler->appendScheduleMsg(scheMsg);
}

//quit actor
void quitActor(int64_t anActorID){
    ScheduleMsg scheMsg;
    scheMsg._type = SCHEDULE_CMD_QUIT_ACTOR;
    scheMsg._param1 = anActorID;
    gPActorScheduler->appendScheduleMsg(scheMsg);
}

int32_t getActorNumbers() {
    //TODO: return the number of single actors in the swarm actor
    return 0;
}

bool getFormation(int64_t anActorID, std::string &aFormationType, std::string &aFormationPos) 
{
    if( ( gPActorScheduler == NULL ) || ( !gPActorScheduler ) ) {
        return false;
    }
    return gPActorScheduler->getFormation(anActorID, aFormationType, aFormationPos);
}
