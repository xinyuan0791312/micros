/* ==================================================================
*
* Copyright (c) 2016-2019, micROS Group, NIIDT, TAIIC, HPCL.
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions 
* are met: 
*
* 1. Redistributions of source code must retain the above copyright 
*     notice, this list of conditions and the following disclaimer. 
* 2. Redistributions in binary form must reproduce the above copyright 
*     notice, this list of conditions and the following disclaimer in the 
*     documentation and/or other materials provided with the 
*     distribution. 
* 3. All advertising materials mentioning features or use of this software 
*     must display the following acknowledgement: 
*     This product includes software developed by the micROS Group. and 
*     its contributors. 
* 4. Neither the name of the Group nor the names of its contributors may 
*     be used to endorse or promote products derived from this software 
*     without specific prior written permission. 
*
* THIS SOFTWARE IS PROVIDED BY MICROS,GROUP AND CONTRIBUTORS
* ''AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT
* NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
* FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL 
* THE MICROS,GROUP OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS 
* OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) 
* HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
* OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN 
* IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
* ===================================================================
*
* Author: Bin Di, Chunlian Fu, Junling Gao, Ruihao Li, Wei Yi.
*         (in alphabetical order)
*
*/

#include "actor_schedule/actor_schedule.h"


//#define SHARED_DATA_SIZE 32768
#define MAX_RUNNING_ACTOR_NUM 200

extern ActorScheduler *gPActorScheduler;
extern char* gPSharedData;
extern int SHARED_DATA_SIZE;

ActorScheduler::ActorScheduler(std::vector<boost::shared_ptr<general_bus::GeneralBus> > aRegisteredBus) {
	_acbList.clear();
	_registeredBus = aRegisteredBus;
	_actorNo = 0;
	_nSwarm = 0;
	_pCurrentSwarm = NULL;
}


void ActorScheduler::start() {
	//set the global scheduler pointer
	gPActorScheduler = this;
	gPSharedData = new char[SHARED_DATA_SIZE];
	//launch a thread to run the schedule loop
	boost::thread scheduleThread(boost::bind(&ActorScheduler::scheduleLoop,this));
	scheduleThread.detach();
	//wait for the thread to start
	sleep(1);
}

void ActorScheduler::scheduleLoop(){
	/* while(true){
		boost::mutex::scoped_lock lock(_mutex);
		_cond.wait(lock);
		schedule();
	} */
	ScheduleMsg scheMsg;
	bool queueFlag = false;
	while(true){
		queueFlag = false;
		{
			boost::unique_lock<boost::shared_mutex> writeLock(_scheMsgQueueMutex);
			if(!_scheMsgQueue.empty()){
				scheMsg = _scheMsgQueue.front();
				_scheMsgQueue.pop();
				
				queueFlag = true;
			}
		}
	
		if(queueFlag){
			switch(scheMsg._type){
				case SCHEDULE_CMD_RAW:
					schedule(true);
					break;
				case SCHEDULE_CMD_SWITCH_PRIO:
					switchToActor(scheMsg._param1,std::string(scheMsg._param3));
					break;
				case SCHEDULE_CMD_ACTIVATE_ACTOR:
					activateActor(std::string(scheMsg._param3));
					break;
				case SCHEDULE_CMD_YIELD_ACTOR:
					pauseActor(scheMsg._param1);
					break;
				case SCHEDULE_CMD_QUIT_ACTOR:
					quitActor(scheMsg._param1);
					break;
				default:
					ROS_FATAL("unsupported SCHEDULE REQUEST");
			} 
			if(scheMsg._type != SCHEDULE_CMD_RAW) schedule(false);
		}else{
			usleep(10000);
		}
	}

}

bool ActorScheduler::schedule(bool needCheckSwarm){
	boost::shared_lock<boost::shared_mutex> acbReadLock(_acbListMutex);
	assert(!_acbList.empty());
	acbReadLock.unlock();

	//Geting the min prio swarm
	boost::shared_lock<boost::shared_mutex> swarmReadLock(_swarmListMutex);
	int minSwarmPrio = _pCurrentSwarm?_pCurrentSwarm->swarmPrio:MAX_PRIORITY+1;
	SwarmInfo* minSwarmPtr = _pCurrentSwarm;
	if(needCheckSwarm)
	{
		for(int i=0; i<_nSwarm; i++){
			if(_swarmList[i].swarmPrio < minSwarmPrio){
				minSwarmPrio = _swarmList[i].swarmPrio;
				minSwarmPtr = &_swarmList[i];
			}
		}
	} 
	else if(_runningActorsMap.size() >= MAX_RUNNING_ACTOR_NUM) {
		//The count of running actors reach the limit
		#ifdef ACTOR_SCHE_DEBUG
			ROS_INFO("[Actor Scheduler] schedule method : the count of running actors reach the limit");
		#endif
		return true;
	}
    SwarmInfo* pCurrentSwarm = _pCurrentSwarm;
    swarmReadLock.unlock();

	assert(minSwarmPtr != NULL);
    
	//Switching swarm or updating _runningActorsMap
	//Pausing the actors in running swarm
	if(pCurrentSwarm && (pCurrentSwarm != minSwarmPtr) && !pauseAllActorsInRunningSwarm())
	{
		return false;
	}
	//Saving actors which prepare for running in the top priority swarm
	std::multimap<int32_t, ACB*> minSwarmActorsMap;
	acbReadLock.lock();
	for (int i=0; i<_acbList.size(); i++) 
		if((_acbList[i].pSwarmInfo->swarmID == minSwarmPtr->swarmID) && ((_acbList[i].state == ACTOR_STATE_INIT) || (_acbList[i].state == ACTOR_STATE_READY))) 
		{
			minSwarmActorsMap.insert({_acbList[i].prio, &_acbList[i]});
		}
	acbReadLock.unlock();
	//Running actors in minSwarmActorsMap untill _runningActorsMap is equal to MAX_RUNNING_ACTOR_NUM
	for(std::multimap<int32_t, ACB*>::iterator it = minSwarmActorsMap.begin(); (it != minSwarmActorsMap.end()) && (_runningActorsMap.size() < MAX_RUNNING_ACTOR_NUM); it++)
	{
		ActorPluginInfo temp;
		acbReadLock.lock();
		temp._actorID = it->second->actorID;
		temp._actorName = it->second->name;
		temp._state = it->second->state;
		temp._taskInfo = it->second->taskInfo;
		it->second->getActorParams(temp._paramsMap);
        int32_t acbPrio = it->second->prio;
		//create callback queue
		if(_mapActorQueueInfo.find(temp._actorID) == _mapActorQueueInfo.end()){
			_mapActorQueueInfo[temp._actorID] = new ActorCallbackQueueInfo(temp._actorName);
		}
		temp._callbackQueue = _mapActorQueueInfo[temp._actorID]->_callbackQueue;
		acbReadLock.unlock();
		if(updateActorPluginState(temp, ACTOR_STATE_RUNNING))
		{
			//start callback queue
			_mapActorQueueInfo[temp._actorID]->startCallbackQueue();

			//Adding the ActorPluginInfo to runnning actors' map
			_runningActorsMap.insert({acbPrio, temp});
			boost::unique_lock<boost::shared_mutex> writeLock(_acbListMutex);
			it->second->state = ACTOR_STATE_RUNNING;
		} else {
			ROS_INFO("[Actor Scheduler] schedule : running plugins of actor by actorID %ld error", temp._actorID);
		}
	}

    {
        boost::lock_guard<boost::shared_mutex> swarmWriteLock(_swarmListMutex);
        if(_pCurrentSwarm != minSwarmPtr) _pCurrentSwarm = minSwarmPtr;
	    _pCurrentSwarm->state = ACTOR_STATE_PAUSE;
    }
	
	//Updating _pCurrentSwarm
	if(!_runningActorsMap.empty())
	{
		boost::unique_lock<boost::shared_mutex> swarmWriteLock(_swarmListMutex);
		_pCurrentSwarm->state = ACTOR_STATE_RUNNING;
		#ifdef ACTOR_SCHE_DEBUG
			ROS_INFO("[Actor Scheduler] schedule method success");
		#endif
		return true;
	}
	
	ROS_INFO("[Actor Scheduler] schedule method : _runningActorsMap is empty");
	return false;
}


bool ActorScheduler::getSwarm(std::string anSwarmName,SwarmInfo* &anSwarm){
	boost::shared_lock<boost::shared_mutex> readLock(_swarmListMutex);
	for(int i=0;i<_nSwarm;i++){
		if(_swarmList[i].swarmName.compare(anSwarmName)==0){
			anSwarm = &(_swarmList[i]);
			return true;
		}
	}
	return false;
}


int ActorScheduler::appendACBs(std::vector<ACB> &anACBs,SwarmInfo* anPSwarm){
	//check the ACB is legal or not, for example, if the buses have been registered
	for(std::vector<ACB>::iterator it = anACBs.begin();it!=anACBs.end();it++){
		std::string unknownBus;
		for(int i=0;i<it->taskInfo.plugins.size();i++){
			int j = 0;
			for(;j<_registeredBus.size();j++){
				std::string tempStr = _registeredBus[j]->getBusName();
				unknownBus = it->taskInfo.plugins[i].busName;
				if(tempStr.compare(unknownBus)==0) break;
			}
			if(j==_registeredBus.size()){
				ROS_ERROR("[Actor Scheduler] appendACBs: unknown bus %s, deleting actor %s...",unknownBus.c_str(),it->name.c_str());
				//if the bus has not been registered, return 1
				return 1;
			}
		}
		
	}
	//push back ACBs
	{
		//boost::unique_lock<boost::shared_mutex> writeLock(_acbListMutex);
		for(int i=0;i<anACBs.size();i++){
			_actorNo++;
			anACBs[i].actorID = _actorNo;
			anACBs[i].pSwarmInfo = anPSwarm;
			_acbList.push_back(anACBs[i]);
			#ifdef ACTOR_SCHE_DEBUG
				ROS_INFO("[Actor Scheduler] appendACBs: push back ACB actorID %ld , actor prio %d",anACBs[i].actorID,anACBs[i].prio);
			#endif
		}
		
	}
	return 0;
	
}

void ActorScheduler::appendSwarmACBs(SwarmInfo& anSwarm,std::vector<ACB> &anACBs){
	// [DMY] Debug
	// std::cout << "[Debug] Swarm name is " << anSwarm.swarmName << ", and actor names are: " << std::endl;
	// for (int i=0; i<anACBs.size(); i++) {
	// 	std::cout << anACBs[i].name << std::endl;
	// }
	//lock swarmlist and acblist
	boost::unique_lock<boost::shared_mutex> writeLock(_acbListMutex);
	//append swarm
	_nSwarm++;
	anSwarm.swarmID = _nSwarm;
	anSwarm.state = ACTOR_STATE_INIT;
	//boost::unique_lock<boost::shared_mutex> writeLock(_swarmListMutex);
	assert(_nSwarm <= SWARM_MAX_SIZE);
	_swarmList[_nSwarm-1] = anSwarm;
	SwarmInfo* pSwarm = &_swarmList[_nSwarm-1];
	ROS_INFO("[Actor Scheduler] swarm is empty? %d", pSwarm==NULL);
	//append ACBs
	if(appendACBs(anACBs,pSwarm)==0){
		//push back a msg to queue
		ScheduleMsg appendMsg;
		appendMsg._type = SCHEDULE_CMD_RAW;
		appendMsg._param1 = 0;
		appendMsg._param2 = 0;
		appendScheduleMsg(appendMsg);	
	}	
}

bool ActorScheduler::updateActorPluginState(ActorPluginInfo &anActor,const int anState){
	//may be parallel
	bool setResult = true;
	for(int i=0; i<_registeredBus.size(); i++){
		setResult = setResult && _registeredBus[i]->updateActorPluginState(anActor,anState);
		if(!setResult) return setResult;
	}
	return setResult;
}


/*deprecated*/
void ActorScheduler::appendSwarm(SwarmInfo& anSwarm){
	_nSwarm++;
	anSwarm.swarmID = _nSwarm;
	anSwarm.state = ACTOR_STATE_INIT;
//	boost::unique_lock<boost::shared_mutex> writeLock(_swarmListMutex);
	assert(_nSwarm<=SWARM_MAX_SIZE);
	_swarmList[_nSwarm-1] = anSwarm;
}

/*deprecated*/
void ActorScheduler::appendACB(ACB& anACB) {
	//check the ACB is legal or not, for example, if the buses have been registered
	for(int i=0;i<anACB.taskInfo.plugins.size();i++){
		int j = 0;
		for(;j<_registeredBus.size();j++){
			std::string tempStr = _registeredBus[j]->getBusName();
			if(tempStr.compare(anACB.taskInfo.plugins[i].busName)==0) break;
		}
		if(j==_registeredBus.size()) return;
	}
	
	//lock the _acbList
	{
		boost::shared_lock<boost::shared_mutex> writeLock(_acbListMutex);
		_actorNo++;
		anACB.actorID = _actorNo;
		_acbList.push_back(anACB);
		#ifdef ACTOR_SCHE_DEBUG
			ROS_INFO("[Actor Scheduler] Call appendACB function: push back ACB actorID %ld , actor prio %d",anACB.actorID,anACB.prio);
		#endif
	}
	//notify the schedule
	/* boost::mutex::scoped_lock lock(_mutex);
	_cond.notify_one();
	#ifdef ACTOR_SCHE_DEBUG
		ROS_INFO("[Actor Scheduler] Call appendACB function: cond notify one");
	#endif */
	//push back a msg to queue
	ScheduleMsg appendMsg;
	appendMsg._type = SCHEDULE_CMD_RAW;
	appendMsg._param1 = 0;
	appendMsg._param2 = 0;
	appendScheduleMsg(appendMsg);	
}

bool ActorScheduler::getActorNameByID(int64_t anActorID,std::string &anActorName){
	//lock the _acbList
	boost::shared_lock<boost::shared_mutex> writeLock(_acbListMutex);
	
	for(int i=0;i<_acbList.size();i++){
		if(_acbList[i].actorID==anActorID){
			anActorName = _acbList[i].name;
			return true;
		}
	}

	return false;
} 
	
bool ActorScheduler::getActorPlugin(int64_t anActorID,std::string aPluginName,boost::shared_ptr<general_bus::GeneralPlugin> &aPluginStr){
	boost::shared_lock<boost::shared_mutex> readLock(_acbListMutex);
	for(int i=0;i<_acbList.size();i++){
		if(_acbList[i].actorID==anActorID){
			TaskInfo* ptrTaskInfo = &_acbList[i].taskInfo;
			for(int j=0;j<ptrTaskInfo->plugins.size();j++){
				std::string busName = ptrTaskInfo->plugins[j].busName;
				std::string pluginName = ptrTaskInfo->plugins[j].name;
				if(pluginName.compare(aPluginName)==0){
					//find the bus
					for(int k=0;k<_registeredBus.size();k++){
						std::string tempStr = _registeredBus[k]->getBusName();
						if(busName.compare(tempStr)==0){
							return _registeredBus[k]->getActorPlugin(anActorID,aPluginName,aPluginStr);
						}
					}
					//if cannot find the bus return false
					return false;

				}
			}

			//if cannot find the plugin, return false
			return false;
		}
	}
	//if cannot find the actorID
	return false;
}	

int ActorScheduler::getActorPauseCount(int64_t anActorID){
	boost::shared_lock<boost::shared_mutex> readLock(_acbListMutex);
	for (int i = 0; i < _acbList.size(); i++) {
		if (_acbList[i].actorID == anActorID)
			return _acbList[i].pauseCount;
	}
	return 0;
}

//Pausing all the actors of current running swarm
bool ActorScheduler::pauseAllActorsInRunningSwarm(){
	for(std::multimap<int32_t, ActorPluginInfo>::iterator it=_runningActorsMap.begin(); it!=_runningActorsMap.end(); it++)
	{
		ActorPluginInfo temp = it->second;
		if(updateActorPluginState(temp, ACTOR_STATE_PAUSE))
		{
			//stop callback queue
			_mapActorQueueInfo[temp._actorID]->stopCallbackQueue();

			//Seting actors' state as ACTOR_STATE_PAUSE
			boost::unique_lock<boost::shared_mutex> acbWriteLock(_acbListMutex);
			for(int i=0; i<_acbList.size(); i++)
			{
				if(temp._actorID == _acbList[i].actorID)
				{
					_acbList[i].state = ACTOR_STATE_PAUSE;
					_acbList[i].pauseCount++;
					break;
				}
			}
		} else {
			ROS_INFO("[Actor Scheduler] pauseAllActorsInRunningSwarm error: updateActorPluginState error");
			return false;
		}
	}
	_runningActorsMap.clear();

	boost::unique_lock<boost::shared_mutex> swarmWriteLock(_swarmListMutex);
	#ifdef ACTOR_SCHE_DEBUG
		ROS_INFO("[Actor Scheduler] pauseAllActorsInRunningSwarm method : pause all actors in running swarm by swarmID %ld success",_pCurrentSwarm->swarmID);
	#endif
	_pCurrentSwarm->state = ACTOR_STATE_PAUSE;
	return true;
}

bool ActorScheduler::quitActor(int64_t anActorID){
	boost::shared_lock<boost::shared_mutex> acbReadLock(_acbListMutex);
	for(int i=0; i<_acbList.size(); i++)
	{
		if(_acbList[i].actorID == anActorID)
		{
			ActorPluginInfo temp;
			temp._state = _acbList[i].state;
			if((_acbList[i].state != ACTOR_STATE_INIT) && (_acbList[i].state != ACTOR_STATE_INIT_DELAY))
			{
				temp._actorID = _acbList[i].actorID;
				temp._actorName = _acbList[i].name;
				temp._taskInfo = _acbList[i].taskInfo;
			}
			int32_t acbPrio = _acbList[i].prio;
			acbReadLock.unlock();

			//When actor state is ACTOR_STATE_INIT, it's plugins have not been loaded
			//Seems impossible: the actor stops only  when its plugin calls terminate
			if((temp._state != ACTOR_STATE_INIT) && (temp._state != ACTOR_STATE_INIT_DELAY))
			{
				if(!updateActorPluginState(temp, ACTOR_STATE_FINISH))
				{
					ROS_INFO("[Actor Scheduler] quitActor : updateActorPluginState error");
					return false;
				}
				//Updating _runningActorsMap and _pCurrentSwarm
				if(temp._state == ACTOR_STATE_RUNNING)
				{
					//A key may has more than one value
					int valueCount = _runningActorsMap.count(acbPrio);
					//Return an iterator of the first value
					std::multimap<int32_t, ActorPluginInfo>::iterator mit = _runningActorsMap.find(acbPrio);
					while(valueCount > 0)
					{
						if(mit->second._actorID == anActorID)
						{
							_runningActorsMap.erase(mit);
							break;
						}
						++mit;
						valueCount--;
					}
					
					//Checking whether the actors in the running swarm are all paused
					if(_runningActorsMap.empty())
					{
						boost::unique_lock<boost::shared_mutex> swarmWriteLock(_swarmListMutex);
						_pCurrentSwarm->state = ACTOR_STATE_PAUSE;
					}
				}
			}
			
			// release actorCallbackQueue
			delete _mapActorQueueInfo[temp._actorID];
			_mapActorQueueInfo.erase(temp._actorID);

			boost::unique_lock<boost::shared_mutex> acbWriteLock(_acbListMutex);
			_acbList.erase(_acbList.begin()+i);
			#ifdef ACTOR_SCHE_DEBUG
				ROS_INFO("[Actor Scheduler] quit actor by actorID %ld success", anActorID);
			#endif
			return true;
		}
	}
	
	ROS_INFO("[Actor Scheduler] quit actor by actorID %ld which is not in acbList", anActorID);
	return false;
}

bool ActorScheduler::switchToActor(int64_t anActorID, std::string anActorName){
	//Checking whether the anActorName is vaild
	ACB *actor = NULL;
	boost::shared_lock<boost::shared_mutex> acbReadLock(_acbListMutex);
	for(int i=0; i<_acbList.size(); i++)
	{
		if(_acbList[i].name.compare(anActorName) == 0)
		{
			actor = &_acbList[i];
			break;
		}
	}
	acbReadLock.unlock();
	//Pausing anActorID and activating anActorName
	if(actor)
	{
		if(pauseActor(anActorID))
		{
			if(activateActor(anActorName))
			{
				#ifdef ACTOR_SCHE_DEBUG
					ROS_INFO("[Actor Scheduler] switchToActor method success");
				#endif
				return true;
			}
		}
		else {
			//Reseting actor state in order to start in schedule()
			boost::unique_lock<boost::shared_mutex> acbWriteLock(_acbListMutex);
			if(actor->state == ACTOR_STATE_PAUSE) actor->state = ACTOR_STATE_READY;
			else if(actor->state == ACTOR_STATE_INIT_DELAY) actor->state = ACTOR_STATE_INIT;
			ROS_INFO("[Actor Scheduler] switchToActor method error : pause actor by actorID %ld error", anActorID);
		}
	} else {
		ROS_INFO("[Actor Scheduler] switchToActor method error : can't find actor by name %s", anActorName.c_str());
	}
	return false;
}

bool ActorScheduler::activateActor(std::string anActorName){
	//Creating local variate to ensure safety
	boost::shared_lock<boost::shared_mutex> swarmReadLock(_swarmListMutex);
	int64_t runnningSwarmId = _pCurrentSwarm->swarmID;
	swarmReadLock.unlock();

	boost::shared_lock<boost::shared_mutex> acbReadLock(_acbListMutex);
	for(int i=0; i<_acbList.size(); i++)
	{
		//If the actor doesnot belong to the running warm, its swarm priority should be higher than the ones
		if((_acbList[i].name.compare(anActorName) == 0) && (runnningSwarmId == _acbList[i].pSwarmInfo->swarmID))
		{
			if(_acbList[i].state == ACTOR_STATE_RUNNING) 
			{
				#ifdef ACTOR_SCHE_DEBUG
					ROS_INFO("[Actor Scheduler] activateActor method : the actor by name %s needn't to activate", anActorName.c_str());
				#endif
				return true;
			}
			int32_t acbPrio = _acbList[i].prio;
			acbReadLock.unlock();
			if(_runningActorsMap.size() >= MAX_RUNNING_ACTOR_NUM)
			{
				//Pausing the actor which has lower priority before artivating the new actor
				bool hasLower = false;
				for(std::multimap<int32_t, ActorPluginInfo>::reverse_iterator it=_runningActorsMap.rbegin(); it!=_runningActorsMap.rend(); it++)
				{
					if(acbPrio < it->first)
					{
						ActorPluginInfo temp = it->second;
						if(!updateActorPluginState(temp, ACTOR_STATE_PAUSE))
						{
							//Reseting actor state in order to start in schedule()
							boost::unique_lock<boost::shared_mutex> acbWriteLock(_acbListMutex);
							if(_acbList[i].state == ACTOR_STATE_PAUSE) _acbList[i].state = ACTOR_STATE_READY;
							else if(_acbList[i].state == ACTOR_STATE_INIT_DELAY) _acbList[i].state = ACTOR_STATE_INIT;
							ROS_INFO("[Actor Scheduler] activateActor method : pause actor of lower priority error");
							return false;
						}
						//stop callback queue
						_mapActorQueueInfo[temp._actorID]->stopCallbackQueue();

						//Seting the actor's state as ACTOR_STATE_PAUSE
						boost::unique_lock<boost::shared_mutex> acbWriteLock(_acbListMutex);
						for(int j=0; j<_acbList.size(); j++)
						{
							if(temp._actorID == _acbList[j].actorID) 
							{
								_acbList[j].state = ACTOR_STATE_PAUSE;
								_acbList[j].pauseCount++;
								break;
							}
						}
						acbWriteLock.unlock();
						hasLower = true;
						//Converting reverse iterator to iterator: (++it)).base()
						_runningActorsMap.erase((++it).base());
						break;
					}
				}
				//there is not lower priority actor, so the new actor cannot be activated
				if(!hasLower)
				{
					ROS_INFO("[Actor Scheduler] activateActor method : the allowed limit of running actors reached and there is not lower priority actor, the actor by name %s cannot be activated",anActorName.c_str());
					//Reseting actor state in order to start in schedule()
					boost::unique_lock<boost::shared_mutex> acbWriteLock(_acbListMutex);
					if(_acbList[i].state == ACTOR_STATE_PAUSE) _acbList[i].state = ACTOR_STATE_READY;
					else if(_acbList[i].state == ACTOR_STATE_INIT_DELAY) _acbList[i].state = ACTOR_STATE_INIT;
					return false;
				}
			}
			acbReadLock.lock();
			//Activating the actor
			ActorPluginInfo temp;
			temp._actorID = _acbList[i].actorID;
			temp._actorName = _acbList[i].name;
			temp._state = _acbList[i].state;
			temp._taskInfo = _acbList[i].taskInfo;
			_acbList[i].getActorParams(temp._paramsMap);
			//create callback queue
			if(_mapActorQueueInfo.find(temp._actorID) == _mapActorQueueInfo.end()){
				_mapActorQueueInfo[temp._actorID] = new ActorCallbackQueueInfo(temp._actorName);
			}
			temp._callbackQueue = _mapActorQueueInfo[temp._actorID]->_callbackQueue;
			acbReadLock.unlock();
			if(updateActorPluginState(temp, ACTOR_STATE_RUNNING)){
				//start callback queue
				_mapActorQueueInfo[temp._actorID]->startCallbackQueue();

				// Adding the ActorPluginInfo to runnning actors' map
				_runningActorsMap.insert({acbPrio, temp});
				boost::unique_lock<boost::shared_mutex> acbWriteLock(_acbListMutex);
				_acbList[i].state = ACTOR_STATE_RUNNING;
				acbWriteLock.unlock();
				#ifdef ACTOR_SCHE_DEBUG
					ROS_INFO("[Actor Scheduler] activateActor method : activate actor by name %s success", anActorName.c_str());
				#endif
				boost::unique_lock<boost::shared_mutex> swarmWriteLock(_swarmListMutex);
				if(_pCurrentSwarm->state != ACTOR_STATE_RUNNING) _pCurrentSwarm->state = ACTOR_STATE_RUNNING;
				return true;
			}
			ROS_INFO("[Actor Scheduler] activateActor method : updateActorPluginState error");
			//Reseting actor state in order to start in schedule()
			boost::unique_lock<boost::shared_mutex> acbWriteLock(_acbListMutex);
			if(_acbList[i].state == ACTOR_STATE_PAUSE) _acbList[i].state = ACTOR_STATE_READY;
			else if(_acbList[i].state == ACTOR_STATE_INIT_DELAY) _acbList[i].state = ACTOR_STATE_INIT;
			return false;
		}
	}
	ROS_INFO("[Actor Scheduler] activateActor method : the actor by name %s is not in acbList", anActorName.c_str());
	return false;
}

bool ActorScheduler::pauseActor(int64_t anActorID){
	boost::shared_lock<boost::shared_mutex> acbReadLock(_acbListMutex);
	for(int i=0; i<_acbList.size(); i++)
	{
		if(_acbList[i].actorID == anActorID)
		{
			if(_acbList[i].state != ACTOR_STATE_RUNNING)
			{
				#ifdef ACTOR_SCHE_DEBUG
					ROS_INFO("[Actor Scheduler] pauseActor : actor by actorID %ld need not to pause", anActorID);
				#endif
				acbReadLock.unlock();
				//Preventing to be activated in schedule()
				boost::unique_lock<boost::shared_mutex> acbWriteLock(_acbListMutex);
				if(_acbList[i].state == ACTOR_STATE_INIT) _acbList[i].state = ACTOR_STATE_INIT_DELAY;
				else if(_acbList[i].state == ACTOR_STATE_READY) _acbList[i].state = ACTOR_STATE_PAUSE;
				return true;
			}
			ActorPluginInfo temp;
			temp._actorID = _acbList[i].actorID;
			temp._actorName = _acbList[i].name;
			temp._state = _acbList[i].state;
			temp._taskInfo = _acbList[i].taskInfo;
			acbReadLock.unlock();
			if(updateActorPluginState(temp, ACTOR_STATE_PAUSE))
			{
				//stop callback queue
				_mapActorQueueInfo[temp._actorID]->stopCallbackQueue();

				boost::unique_lock<boost::shared_mutex> acbWriteLock(_acbListMutex);
				_acbList[i].state = ACTOR_STATE_PAUSE;
				_acbList[i].pauseCount++;
				int32_t acbPrio = _acbList[i].prio;
				acbWriteLock.unlock();

				//Removing the actor in _runningActorsMap
				//A key may has more than one value
				int valueCount = _runningActorsMap.count(acbPrio);
				//Return an iterator of the first value
				std::multimap<int32_t, ActorPluginInfo>::iterator mit = _runningActorsMap.find(acbPrio);
				while(valueCount > 0)
				{
					if(mit->second._actorID == anActorID)
					{
						_runningActorsMap.erase(mit);
						break;
					}
					++mit;
					valueCount--;
				}

				//Checking whether the actors in the running swarm are all paused
				if(_runningActorsMap.empty())
				{
					boost::unique_lock<boost::shared_mutex> swarmWriteLock(_swarmListMutex);
					_pCurrentSwarm->state = ACTOR_STATE_PAUSE;
					#ifdef ACTOR_SCHE_DEBUG
						ROS_INFO("[Actor Scheduler] pauseActor method : all actors paused in swarm by swarmID %ld",_pCurrentSwarm->swarmID);
					#endif
				}
				#ifdef ACTOR_SCHE_DEBUG
					ROS_INFO("[Actor Scheduler] pauseActor method : pause actor by actorID %ld success",anActorID);
				#endif
				return true;
			}
			ROS_INFO("[Actor Scheduler] pauseActor : updateActorPluginState error");
			return false;
		}
	}
	ROS_INFO("[Actor Scheduler] pauseActor : actor by actorID %ld is not in acbList", anActorID);
	return false;
}

//activate actor in swarm from outside
bool ActorScheduler::activateActor(std::string aSwarmName, std::string anActorName){
	#ifdef ACTOR_SCHE_DEBUG
		ROS_INFO("TEST: activate swarm actor %s in %s", anActorName.c_str(), aSwarmName.c_str());
	#endif
	
	//if the anSwarmName is running
	boost::shared_lock<boost::shared_mutex> readLock(_swarmListMutex);
	if(!_pCurrentSwarm || (_pCurrentSwarm->swarmName.compare(aSwarmName) != 0)){
		ROS_INFO("[Actor Scheduler] extern activateActor: swarm task %s is not running", aSwarmName.c_str());
		return false;
	}
	readLock.unlock();

	ScheduleMsg scheMsg;
	scheMsg._type = SCHEDULE_CMD_ACTIVATE_ACTOR;
	assert(anActorName.size() < SCHE_MSGS_CHAR_SIZE);
    strcpy(scheMsg._param3, anActorName.c_str());
    scheMsg._param3[anActorName.size()] = 0;
	appendScheduleMsg(scheMsg);

	return true;
}

//pause actor in swarm from outside
bool ActorScheduler::pauseActor(std::string aSwarmName, std::string anActorName){
	#ifdef ACTOR_SCHE_DEBUG
		ROS_INFO("TEST: pause swarm actor %s in %s", anActorName.c_str(), aSwarmName.c_str());
	#endif
	
	//if the aSwarmName is running
	boost::shared_lock<boost::shared_mutex> readLock(_swarmListMutex);
	if(!_pCurrentSwarm || (_pCurrentSwarm->swarmName.compare(aSwarmName) != 0)){
		ROS_INFO("[Actor Scheduler] extern pauseActor: swarm task %s is not running", aSwarmName.c_str());
		return false;
	}
	readLock.unlock();

	//get the actorID by anActorName
	int32_t actorID = -1;
	for(int i=0; i<_acbList.size(); i++){
		if((_acbList[i].name.compare(anActorName) == 0) && (_acbList[i].pSwarmInfo->swarmName.compare(aSwarmName) == 0)){
			actorID = _acbList[i].actorID;
			break;
		}
	}

	if(actorID >= 0){
		ScheduleMsg scheMsg;
		scheMsg._type = SCHEDULE_CMD_YIELD_ACTOR;
		scheMsg._param1 = actorID;
		appendScheduleMsg(scheMsg);
	}

	return true;
}

//pause actor in swarm from outside
bool ActorScheduler::switchToActor(std::string aSwarmName, std::string anActorName, std::string aDestActorName){
	#ifdef ACTOR_SCHE_DEBUG
		ROS_INFO("TEST: switch swarm actor %s to actor %s in %s", anActorName.c_str(), aDestActorName.c_str(), aSwarmName.c_str());
	#endif
	
	//if the aSwarmName is running
	boost::shared_lock<boost::shared_mutex> readLock(_swarmListMutex);
	if(!_pCurrentSwarm || (_pCurrentSwarm->swarmName.compare(aSwarmName) != 0)){
		ROS_INFO("[Actor Scheduler] extern pauseActor: swarm task %s is not running", aSwarmName.c_str());
		return false;
	}
	readLock.unlock();

	//get the actorID by anActorName
	int32_t actorID = -1;
	for(int i=0; i<_acbList.size(); i++){
		if((_acbList[i].name.compare(anActorName) == 0) && (_acbList[i].pSwarmInfo->swarmName.compare(aSwarmName) == 0)){
			actorID = _acbList[i].actorID;
			break;
		}
	}

	if(actorID >= 0){
		ScheduleMsg scheMsg;
		scheMsg._type = SCHEDULE_CMD_SWITCH_PRIO;
		scheMsg._param1 = actorID;
		assert(aDestActorName.size() < SCHE_MSGS_CHAR_SIZE);
    	strcpy(scheMsg._param3, aDestActorName.c_str());
    	scheMsg._param3[aDestActorName.size()] = 0;
		appendScheduleMsg(scheMsg);
	}

	return true;
}


void ActorScheduler::getRunningSwarmActors(std::string &aSwarmName, std::vector<std::string> &anActorsVec){
	boost::shared_lock<boost::shared_mutex> readLock(_swarmListMutex);
	if(!_pCurrentSwarm){
		//ROS_INFO("[Actor Scheduler] getRunningSwarmActors: no swarm task is running");
		return;
	}
    aSwarmName = _pCurrentSwarm->swarmName;
	readLock.unlock();

	//Getting running actors
	boost::shared_lock<boost::shared_mutex> acbReadLock(_acbListMutex);
	for(int i=0; i<_acbList.size(); i++){
		if((_acbList[i].state == ACTOR_STATE_RUNNING) && (_acbList[i].pSwarmInfo->swarmName.compare(aSwarmName) == 0)){
			anActorsVec.push_back(_acbList[i].name);
		}
	}
}

ActorScheduler::~ActorScheduler() {
	if(!_mapActorQueueInfo.empty()){
		std::map<int64_t,ActorCallbackQueueInfo*>::iterator it = _mapActorQueueInfo.begin();
		for(; it != _mapActorQueueInfo.end(); it++){
			delete it->second;
		}
		_mapActorQueueInfo.clear();
	}
	for(std::vector<boost::shared_ptr<general_bus::GeneralBus> >::iterator it =_registeredBus.begin(); it != _registeredBus.end();it++){
		(*it).reset();
	}
	_registeredBus.clear();
	delete gPActorScheduler;
	
	gPActorScheduler = NULL;
	delete []gPSharedData;
}

void ActorScheduler::appendScheduleMsg(ScheduleMsg& aMsg){
	boost::unique_lock<boost::shared_mutex> writeLock(_scheMsgQueueMutex);
	_scheMsgQueue.push(aMsg);
}

//Set formation type and position in the formation, 20181101
bool ActorScheduler::setFormation(std::string aSwarmName, std::string anActorName, std::string aFormationType, std::string aFormationPos)
{
	//lock swarmlist and acblist
	boost::unique_lock<boost::shared_mutex> writeLock(_acbListMutex);
	for (int i=0; i<_acbList.size(); i++) {
		// // [DMY] Debug
		// std::cout << "[Debug] SwarmName is " << _acbList[i].pSwarmInfo->swarmName << ", and ActorName is " << _acbList[i].name << std::endl;
		if ( 
			(_acbList[i].pSwarmInfo->swarmName.compare(aSwarmName) == 0) &&
			(_acbList[i].name.compare(anActorName) == 0)
		)
		{
			// // [DMY] Debug
			// std::cout << "[Debug] Set formation matched!" << std::endl;
			std::stringstream ss;
			int8_t tempFormationPos;
			ss.clear();
			ss << aFormationPos;
			ss >> tempFormationPos;
			_acbList[i].formationPos = tempFormationPos;
			_acbList[i].formationType = aFormationType;
			return true;
		}
	}
	return false;
}

//Get formation by aSwarmName and anActorName, 20181101
bool ActorScheduler::getFormation(std::string aSwarmName, std::string anActorName, std::string &aFormationType, std::string &aFormationPos)
{
	//lock the _acbList
	boost::shared_lock<boost::shared_mutex> readLock(_acbListMutex);
	for (int i=0; i<_acbList.size(); i++) {
		// // [DMY] Debug
		// std::cout << "[Debug] SwarmName is " << _acbList[i].pSwarmInfo->swarmName << ", and ActorName is " << _acbList[i].name << std::endl;
		if ( 
			(_acbList[i].pSwarmInfo->swarmName.compare(aSwarmName) == 0) &&
			(_acbList[i].name.compare(anActorName) == 0)
		)
		{
			// // [DMY] Debug
			// std::cout << "[Debug] Get formation matched!" << std::endl;
			std::stringstream ss;
			std::string tempFormationPos;
			ss.clear();
			ss << _acbList[i].formationPos;
			ss >> tempFormationPos;
			aFormationPos = tempFormationPos;
			aFormationType = _acbList[i].formationType;
			return true;
		}
	}
	return false;
}

//Get formation by actorID. This function is open to plugins.
bool ActorScheduler::getFormation(ACTOR_ID_TYPE anActorID, std::string &aFormationType, std::string &aFormationPos)
{
	//lock the _acbList
	boost::shared_lock<boost::shared_mutex> readLock(_acbListMutex);
	for (int i=0; i<_acbList.size(); i++) {
		if ( _acbList[i].actorID == anActorID )
		{
			std::stringstream ss;
			std::string tempFormationPos;
			ss.clear();
			ss << _acbList[i].formationPos;
			ss >> tempFormationPos;
			aFormationPos = tempFormationPos;
			aFormationType = _acbList[i].formationType;
			return true;
		}
	}
	return false;
}

void ActorScheduler::setActorParams(const std::string &aSwarmName, const std::string &anActorName, const std::map<std::string, std::string> &aMap){
	//Check if the aSwarmName is running
	{
		boost::shared_lock<boost::shared_mutex> readLock(_swarmListMutex);
		if(!_pCurrentSwarm || (_pCurrentSwarm->swarmName.compare(aSwarmName) != 0)){
			ROS_INFO("[Actor Scheduler] setActorParams: swarm task %s is not running", aSwarmName.c_str());
			return;
		}
	}

	boost::lock_guard<boost::shared_mutex> acbReadLock(_acbListMutex);
	for(int i=0; i<_acbList.size(); i++)
	{
		if(_acbList[i].name.compare(anActorName) == 0)
		{
			_acbList[i].setActorParams(aMap);
			break;
		}
	}
}

