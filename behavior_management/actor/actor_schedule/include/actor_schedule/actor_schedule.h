/* ==================================================================
*
* Copyright (c) 2016-2019, micROS Group, NIIDT, TAIIC, HPCL.
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions 
* are met: 
*
* 1. Redistributions of source code must retain the above copyright 
*     notice, this list of conditions and the following disclaimer. 
* 2. Redistributions in binary form must reproduce the above copyright 
*     notice, this list of conditions and the following disclaimer in the 
*     documentation and/or other materials provided with the 
*     distribution. 
* 3. All advertising materials mentioning features or use of this software 
*     must display the following acknowledgement: 
*     This product includes software developed by the micROS Group. and 
*     its contributors. 
* 4. Neither the name of the Group nor the names of its contributors may 
*     be used to endorse or promote products derived from this software 
*     without specific prior written permission. 
*
* THIS SOFTWARE IS PROVIDED BY MICROS,GROUP AND CONTRIBUTORS
* ''AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT
* NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
* FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL 
* THE MICROS,GROUP OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS 
* OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) 
* HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
* OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN 
* IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
* ===================================================================
*
* Author: Bin Di, Chunlian Fu, Junling Gao, Ruihao Li, Wei Yi.
*         (in alphabetical order)
*
*/

#ifndef __ACTOR_SCHEDULER__
#define __ACTOR_SCHEDULER__

#include <vector>
#include <iterator>
#include <queue>
#include <map>
#include <list>
#include <sstream>
#include "actor_core/ACB.h"
#include "actor_core/actor_constant.h"
#include "general_bus/general_bus.h"
#include "ros/ros.h"
#include <boost/thread/thread.hpp>
#include <boost/thread/condition.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/bind.hpp>
#include <pluginlib/class_loader.h>

#define ACTOR_SCHE_DEBUG
#define SCHE_MSGS_CHAR_SIZE 30
#define SWARM_MAX_SIZE 1024

#define ACTOR_ID_TYPE int64_t

const int32_t SCHEDULE_CMD_RAW = 1;
const int32_t SCHEDULE_CMD_SWITCH_PRIO = 2;
const int32_t SCHEDULE_CMD_ACTIVATE_ACTOR = 3;
const int32_t SCHEDULE_CMD_YIELD_ACTOR = 4;
const int32_t SCHEDULE_CMD_QUIT_ACTOR = 5;

struct ScheduleMsg{
	int32_t _type;
	int64_t _param1;
	int64_t _param2;
	char _param3[SCHE_MSGS_CHAR_SIZE];
};

class ActorScheduler {

public:
	ActorScheduler(std::vector<boost::shared_ptr<general_bus::GeneralBus> > aRegisteredBus);
	~ActorScheduler();

	//start the main thread for the scheduler
	void start();

	//processing scheduler msgs and call schedule iteratively
	void scheduleLoop();

	//interface for task_binder 
	void appendSwarmACBs(SwarmInfo& anSwarm,std::vector<ACB> &anACBs);
	int appendACBs(std::vector<ACB> &anACBs,SwarmInfo* anPSwarm);

	//deprecated
	void appendACB(ACB& anACB);
	void appendSwarm(SwarmInfo& anSwarm);

	//get the SwarmInfo according to the swarm name
	bool getSwarm(std::string anSwarmName,SwarmInfo* &anSwarm);

	//update plugins' state according to anState
	bool updateActorPluginState(ActorPluginInfo &anActor,const int anState);
	
	// helpers for actor api 
	bool getActorPlugin(int64_t anActorID,std::string aPluginName,boost::shared_ptr<general_bus::GeneralPlugin> &aPluginStr);
	bool getActorNameByID(int64_t anActorID,std::string &anActorName);
	int getActorPauseCount(int64_t anActorID);
	void appendScheduleMsg(ScheduleMsg &aMsg);

	std::vector<ACB> _acbList;
	SwarmInfo _swarmList[SWARM_MAX_SIZE];

	//mutex for _acbList
	boost::shared_mutex _acbListMutex; 
	//mutex for swarm list
	boost::shared_mutex _swarmListMutex;
	
	//actorNum registered before, used for actorID temporialy
	int64_t _actorNo;
	int64_t _nSwarm;
	
	std::vector<boost::shared_ptr<general_bus::GeneralBus> > _registeredBus;
	
	//queue for schedule msgs
	std::queue<ScheduleMsg> _scheMsgQueue;
	boost::shared_mutex _scheMsgQueueMutex;

	//activate or pause actors from outside, such as ground station commands
	bool activateActor(std::string aSwarmName, std::string anActorName);
	bool pauseActor(std::string aSwarmName, std::string anActorName);
	bool switchToActor(std::string aSwarmName, std::string anActorName, std::string aDestActorName);

	//get running swarm name and actors
	void getRunningSwarmActors(std::string &aSwarmName, std::vector<std::string> &anActorsVec);

	//set formation type and position in the formation, 20181101
	bool setFormation(std::string aSwarmName, std::string anActorName, std::string aFormationType, std::string aFormationPos);

	//get formation, 20181101
	bool getFormation(std::string aSwarmName, std::string anActorName, std::string &aFormationType, std::string &aFormationPos);
	bool getFormation(ACTOR_ID_TYPE anActorID, std::string &aFormationType, std::string &aFormationPos);

	// set actor plugins' params
	void setActorParams(const std::string &aSwarmName, const std::string &anActorName, const std::map<std::string, std::string> &aMap);
    // TODO...get actor params after ACB getting params from plugins
    bool getActorParams(const std::string &aSwarmName, const std::string &anActorName, std::map<std::string, std::string> &aMap) const{}
	// TODO...wait to be extracted into an actor configure module
	void setActorParamsRuntime() {}

private:
	std::map<int64_t, ActorCallbackQueueInfo*> _mapActorQueueInfo;
	SwarmInfo *_pCurrentSwarm;
	/*
	* map is used to restore running actors currently
	* map key is the actor's priority, map value is the pointer to the ACB of actor,
	* tips: multimap allows repetitive keys
	*/
	std::multimap<int32_t, ActorPluginInfo> _runningActorsMap;

	bool switchToActor(int64_t anActorID, std::string anActorName);
	bool activateActor(std::string anActorName);
	bool pauseActor(int64_t anActorID);
	bool quitActor(int64_t anActorID);
	bool pauseAllActorsInRunningSwarm();
	bool schedule(bool needCheckSwarm);
};
#endif
