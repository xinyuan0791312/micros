/* ==================================================================
*
* Copyright (c) 2016-2019, micROS Group, NIIDT, TAIIC, HPCL.
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions 
* are met: 
*
* 1. Redistributions of source code must retain the above copyright 
*     notice, this list of conditions and the following disclaimer. 
* 2. Redistributions in binary form must reproduce the above copyright 
*     notice, this list of conditions and the following disclaimer in the 
*     documentation and/or other materials provided with the 
*     distribution. 
* 3. All advertising materials mentioning features or use of this software 
*     must display the following acknowledgement: 
*     This product includes software developed by the micROS Group. and 
*     its contributors. 
* 4. Neither the name of the Group nor the names of its contributors may 
*     be used to endorse or promote products derived from this software 
*     without specific prior written permission. 
*
* THIS SOFTWARE IS PROVIDED BY MICROS,GROUP AND CONTRIBUTORS
* ''AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT
* NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
* FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL 
* THE MICROS,GROUP OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS 
* OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) 
* HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
* OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN 
* IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
* ===================================================================
*
* Author: Bin Di, Chunlian Fu, Junling Gao, Ruihao Li, Wei Yi.
*         (in alphabetical order)
*
*/

#include "actor_manager/actor_manager.h"
#include "swarm_data_storage/swarm_data_storage.h"
//---------------------************************ split line ************************---------------------//
//---------------------******************** based on FAST-RTPS ********************---------------------//
//---------------------************************* 20181031 *************************---------------------//
// ActorManager::ActorManager(ActorScheduler *aPActorScheduler, ActorElection *aPActorElection, int64_t aPlatformID)
// {
// 	this->_pActorScheduler = aPActorScheduler;
// 	this->_pActorElection = aPActorElection;
// 	this->_platformID = aPlatformID;
// 	this->_swarmName = "";
// 	this->_actorName = "";
// 	this->_platformIdListStr = "";
// 	this->_dataStr = "";
// 	this->_workMode = MODE_ELECTION_ON;

// 	_pSubCommand = new RTPSSubscriber<CtrlMsg>("/swarm_commands", &ActorManager::handleMasterDataCmdCallback, this);
// }

ActorManager::ActorManager(ActorScheduler *aPActorScheduler)
{
	// this->_nh = ros::NodeHandle("~");
	// string paramStr;
	// if (_nh.hasParam("platformID")) {
    //     _nh.getParam("platformID",paramStr); 
    // } else {
    //    cout << "[INFO] Cannot find PlatformID in the parameter server" << endl;
	//    return;
    // }
	// stringstream ss;
	// ss.clear();
	// ss << paramStr;
	// ss >> (this->_platformID);
	this->_platformID = SwarmDataStorage::instance()->getLocalRobotID();
	this->_pActorScheduler = aPActorScheduler;
	this->_swarmName = "";
	this->_actorName = "";
	this->_platformIdListStr = "";
	this->_dataStr = "";
	this->_workMode = MODE_ELECTION_ON;

	_pSubCommand = new RTPSSubscriber<CtrlMsg>("/swarm_commands", &ActorManager::handleMasterDataCmdCallback, this);
}

// ActorManager::ActorManager(ActorScheduler *aPActorScheduler, int64_t aPlatformID)
// {
// 	this->_pActorScheduler = aPActorScheduler;
// 	this->_platformID = aPlatformID;
// 	this->_swarmName = "";
// 	this->_actorName = "";
// 	this->_platformIdListStr = "";
// 	this->_dataStr = "";
// 	this->_workMode = MODE_ELECTION_OFF;

// 	_pSubCommand = new RTPSSubscriber<CtrlMsg>("/swarm_commands", &ActorManager::handleMasterDataCmdCallback, this);
// }

ActorManager::~ActorManager() 
{
	if (_pSubCommand != NULL)
	{
		delete _pSubCommand;
		_pSubCommand = NULL;
	}
}

void ActorManager::start()
{
	if (!_pSubCommand->init()) {
		cout << "[Actor Manager] Failed to initialized RTPSSubscriber for /swarm_commands!" << endl;
		return;
	}

	cout << "[Actor Manager] Actor Commander constructed!" << endl;
}

void ActorManager::handleMasterDataCmdCallback(CtrlMsg &aMsg)
{
	cout << "[Actor Manager] In Swarm Command Callback!" << endl;

	stringstream ss;
	string platformIDStr;
	ss.clear();
	ss << _platformID;
	ss >> platformIDStr;

	// only process new messages
	if (
		( _swarmName != aMsg.swarmName() ) || 
		( _actorName != aMsg.actorName() ) || 
		( _platformIdListStr != aMsg.platformIdList() ) || 
		( _dataStr != aMsg.data() )
	)
	{
		_swarmName = aMsg.swarmName();
		_actorName = aMsg.actorName();
		_platformIdListStr = aMsg.platformIdList();
		_dataStr = aMsg.data();
		bool decomposeFlag = false;
		if (decomposeCtrlMsg()) {	// actor_election requires all the info of the swarm
			decomposeFlag = true;
			for (int i=0; i<_platformIdList.size(); i++) {
				int32_t aPlatID = (int32_t)atoi(_platformIdList[i].c_str());
				string aSwarmName = _swarmName;
				string anActorName = _actorName;
				string aFormationType = _formationTypeList[i];
				int8_t aFormationPos = (int8_t)atoi(_formationPosList[i].c_str());
				// set this data iff MODE_ELECTION_ON
				if (_workMode == MODE_ELECTION_ON) {
					//_pActorElection->setPlatFormationPos(aPlatID, aSwarmName, anActorName, aFormationType, aFormationPos);
					SwarmDataStorage::instance()->setRobotPos(aPlatID, aFormationPos, aSwarmName, anActorName, aFormationType);
					SwarmDataStorage::instance()->printInformation();
				} 
			}
		}
		if (_platformIdListStr.find(platformIDStr) != std::string::npos) {
			cout << "[Actor Manager] Platform " << _platformID << " received /swarm_comands!" << endl;
			cout << "swarmName is " << aMsg.swarmName() << endl;
			cout << "actorName is " << aMsg.actorName() << endl;
			cout << "platformIdList is " << aMsg.platformIdList() << endl;
			cout << "data is " << aMsg.data() << endl;
			if (decomposeFlag) {
				for (int i=0; i<_platformIdList.size(); i++) {
					if (_platformIdList[i] == platformIDStr) {
						if ( _pActorScheduler->setFormation(_swarmName, _actorName, _formationTypeList[i], _formationPosList[i]) ) {
							cout << "[Actor Manager] Set formation successfully! Formation type is " << _formationTypeList[i] 
									<< ", and formation position is " << _formationPosList[i] << endl;
						}
						else {
							cout << "[Actor Manager] ERROR: Set formation failed!" << endl;
						}
						break;
					}
				}
				// only for debug
#ifdef ACTOR_MANAGER_DEBUG
				sleep(5);
				string str1, str2;
				if (_pActorScheduler->getFormation(_swarmName, _actorName, str1, str2))
				{
					cout << "[Actor Manager] Test the function of getFormation: formation type is " << str1 << ", and formation position is " << str2 << endl;
				}
				else
				{
					cout << "[Actor Manager] SwarmName or ActorName not matched!" << endl;
				}
#endif
			}
		}
		else {
			cout << "[Actor Manager] /swarm_commands not aimed for platform " << _platformID << endl;
		}
	}
	else
	{
		cout << "[Actor Manager] /swarm_comands received before!" << endl;
	}
}

bool ActorManager::decomposeCtrlMsg()
{
	// clean the space of vector variables
	std::vector<string>().swap(_platformIdList);
	std::vector<string>().swap(_formationTypeList);
	std::vector<string>().swap(_formationPosList);
	// decompose /swarm_commands and save in the previous vectors
	string tempStr1, tempStr2;
	tempStr1 = _platformIdListStr;
	while (tempStr1.find(";") != std::string::npos) {
		size_t pos1 = tempStr1.find(";");
		tempStr2 = tempStr1.substr(0, pos1);
		tempStr1 = tempStr1.substr(pos1+1);
		_platformIdList.push_back(tempStr2);
	}
	tempStr1 = _dataStr;
	while (tempStr1.find(";") != std::string::npos) {
		size_t pos1 = tempStr1.find(";");
		tempStr2 = tempStr1.substr(0, pos1);
		tempStr1 = tempStr1.substr(pos1+1);
		string tempStr3, tempStr4;
		tempStr3 = tempStr2;
		size_t pos2 = tempStr3.find(",");
		tempStr4 = tempStr3.substr(0, pos2);
		tempStr3 = tempStr3.substr(pos2+1);
		_formationTypeList.push_back(tempStr4);
		_formationPosList.push_back(tempStr3);
	}
	// assert that all vectors have the same size
	std::vector<size_t> msgSizes;
	msgSizes.push_back(_platformIdList.size());
	msgSizes.push_back(_formationTypeList.size());
	msgSizes.push_back(_formationPosList.size());
	sort(msgSizes.begin(), msgSizes.end());
	if (msgSizes.front() != msgSizes.back()) {
		cout << "[Actor Manager] Error: Inconsistancy in /swarm_commands!" << endl;
		return false;
	}
	cout << "[Actor Manager] Master Command Data Decomposed Successfully!" << endl;
	return true;
}