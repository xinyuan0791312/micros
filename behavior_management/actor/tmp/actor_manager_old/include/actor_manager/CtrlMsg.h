// Copyright 2016 Proyectos y Sistemas de Mantenimiento SL (eProsima).
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/*!
 * @file CtrlMsg.h
 * This header file contains the declaration of the described types in the IDL file.
 *
 * This file was generated by the tool gen.
 */

#ifndef _CTRLMSG_H_
#define _CTRLMSG_H_

// TODO Poner en el contexto.

#include <stdint.h>
#include <array>
#include <string>
#include <vector>
#include <map>
#include <bitset>

#if defined(_WIN32)
#if defined(EPROSIMA_USER_DLL_EXPORT)
#define eProsima_user_DllExport __declspec( dllexport )
#else
#define eProsima_user_DllExport
#endif
#else
#define eProsima_user_DllExport
#endif

#if defined(_WIN32)
#if defined(EPROSIMA_USER_DLL_EXPORT)
#if defined(CtrlMsg_SOURCE)
#define CtrlMsg_DllAPI __declspec( dllexport )
#else
#define CtrlMsg_DllAPI __declspec( dllimport )
#endif // CtrlMsg_SOURCE
#else
#define CtrlMsg_DllAPI
#endif
#else
#define CtrlMsg_DllAPI
#endif // _WIN32

namespace eprosima
{
    namespace fastcdr
    {
        class Cdr;
    }
}

/*!
 * @brief This class represents the structure CtrlMsg defined by the user in the IDL file.
 * @ingroup CTRLMSG
 */
class CtrlMsg
{
public:

    /*!
     * @brief Default constructor.
     */
    eProsima_user_DllExport CtrlMsg();

    /*!
     * @brief Default destructor.
     */
    eProsima_user_DllExport virtual ~CtrlMsg();

    /*!
     * @brief Copy constructor.
     * @param x Reference to the object CtrlMsg that will be copied.
     */
    eProsima_user_DllExport CtrlMsg(const CtrlMsg &x);

    /*!
     * @brief Move constructor.
     * @param x Reference to the object CtrlMsg that will be copied.
     */
    eProsima_user_DllExport CtrlMsg(CtrlMsg &&x);

    /*!
     * @brief Copy assignment.
     * @param x Reference to the object CtrlMsg that will be copied.
     */
    eProsima_user_DllExport CtrlMsg& operator=(const CtrlMsg &x);

    /*!
     * @brief Move assignment.
     * @param x Reference to the object CtrlMsg that will be copied.
     */
    eProsima_user_DllExport CtrlMsg& operator=(CtrlMsg &&x);

    /*!
     * @brief This function copies the value in member timeStamp
     * @param _timeStamp New value to be copied in member timeStamp
     */
    eProsima_user_DllExport void timeStamp(const std::string &_timeStamp);

    /*!
     * @brief This function moves the value in member timeStamp
     * @param _timeStamp New value to be moved in member timeStamp
     */
    eProsima_user_DllExport void timeStamp(std::string &&_timeStamp);

    /*!
     * @brief This function returns a constant reference to member timeStamp
     * @return Constant reference to member timeStamp
     */
    eProsima_user_DllExport const std::string& timeStamp() const;

    /*!
     * @brief This function returns a reference to member timeStamp
     * @return Reference to member timeStamp
     */
    eProsima_user_DllExport std::string& timeStamp();
    /*!
     * @brief This function copies the value in member swarmName
     * @param _swarmName New value to be copied in member swarmName
     */
    eProsima_user_DllExport void swarmName(const std::string &_swarmName);

    /*!
     * @brief This function moves the value in member swarmName
     * @param _swarmName New value to be moved in member swarmName
     */
    eProsima_user_DllExport void swarmName(std::string &&_swarmName);

    /*!
     * @brief This function returns a constant reference to member swarmName
     * @return Constant reference to member swarmName
     */
    eProsima_user_DllExport const std::string& swarmName() const;

    /*!
     * @brief This function returns a reference to member swarmName
     * @return Reference to member swarmName
     */
    eProsima_user_DllExport std::string& swarmName();
    /*!
     * @brief This function copies the value in member actorName
     * @param _actorName New value to be copied in member actorName
     */
    eProsima_user_DllExport void actorName(const std::string &_actorName);

    /*!
     * @brief This function moves the value in member actorName
     * @param _actorName New value to be moved in member actorName
     */
    eProsima_user_DllExport void actorName(std::string &&_actorName);

    /*!
     * @brief This function returns a constant reference to member actorName
     * @return Constant reference to member actorName
     */
    eProsima_user_DllExport const std::string& actorName() const;

    /*!
     * @brief This function returns a reference to member actorName
     * @return Reference to member actorName
     */
    eProsima_user_DllExport std::string& actorName();
    /*!
     * @brief This function copies the value in member platformIdList
     * @param _platformIdList New value to be copied in member platformIdList
     */
    eProsima_user_DllExport void platformIdList(const std::string &_platformIdList);

    /*!
     * @brief This function moves the value in member platformIdList
     * @param _platformIdList New value to be moved in member platformIdList
     */
    eProsima_user_DllExport void platformIdList(std::string &&_platformIdList);

    /*!
     * @brief This function returns a constant reference to member platformIdList
     * @return Constant reference to member platformIdList
     */
    eProsima_user_DllExport const std::string& platformIdList() const;

    /*!
     * @brief This function returns a reference to member platformIdList
     * @return Reference to member platformIdList
     */
    eProsima_user_DllExport std::string& platformIdList();
    /*!
     * @brief This function copies the value in member data
     * @param _data New value to be copied in member data
     */
    eProsima_user_DllExport void data(const std::string &_data);

    /*!
     * @brief This function moves the value in member data
     * @param _data New value to be moved in member data
     */
    eProsima_user_DllExport void data(std::string &&_data);

    /*!
     * @brief This function returns a constant reference to member data
     * @return Constant reference to member data
     */
    eProsima_user_DllExport const std::string& data() const;

    /*!
     * @brief This function returns a reference to member data
     * @return Reference to member data
     */
    eProsima_user_DllExport std::string& data();

    /*!
     * @brief This function returns the maximum serialized size of an object
     * depending on the buffer alignment.
     * @param current_alignment Buffer alignment.
     * @return Maximum serialized size.
     */
    eProsima_user_DllExport static size_t getMaxCdrSerializedSize(size_t current_alignment = 0);

    /*!
     * @brief This function returns the serialized size of a data depending on the buffer alignment.
     * @param data Data which is calculated its serialized size.
     * @param current_alignment Buffer alignment.
     * @return Serialized size.
     */
    eProsima_user_DllExport static size_t getCdrSerializedSize(const CtrlMsg& data, size_t current_alignment = 0);


    /*!
     * @brief This function serializes an object using CDR serialization.
     * @param cdr CDR serialization object.
     */
    eProsima_user_DllExport void serialize(eprosima::fastcdr::Cdr &cdr) const;

    /*!
     * @brief This function deserializes an object using CDR serialization.
     * @param cdr CDR serialization object.
     */
    eProsima_user_DllExport void deserialize(eprosima::fastcdr::Cdr &cdr);



    /*!
     * @brief This function returns the maximum serialized size of the Key of an object
     * depending on the buffer alignment.
     * @param current_alignment Buffer alignment.
     * @return Maximum serialized size.
     */
    eProsima_user_DllExport static size_t getKeyMaxCdrSerializedSize(size_t current_alignment = 0);

    /*!
     * @brief This function tells you if the Key has been defined for this type
     */
    eProsima_user_DllExport static bool isKeyDefined();

    /*!
     * @brief This function serializes the key members of an object using CDR serialization.
     * @param cdr CDR serialization object.
     */
    eProsima_user_DllExport void serializeKey(eprosima::fastcdr::Cdr &cdr) const;

 eProsima_user_DllExport static std::string getName() { return "CtrlMsg"; } 
private:
    std::string m_timeStamp;
    std::string m_swarmName;
    std::string m_actorName;
    std::string m_platformIdList;
    std::string m_data;
};

#endif // _CTRLMSG_H_