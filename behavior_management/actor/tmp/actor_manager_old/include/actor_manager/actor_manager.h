/* ==================================================================
*
* Copyright (c) 2016-2019, micROS Group, NIIDT, TAIIC, HPCL.
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions 
* are met: 
*
* 1. Redistributions of source code must retain the above copyright 
*     notice, this list of conditions and the following disclaimer. 
* 2. Redistributions in binary form must reproduce the above copyright 
*     notice, this list of conditions and the following disclaimer in the 
*     documentation and/or other materials provided with the 
*     distribution. 
* 3. All advertising materials mentioning features or use of this software 
*     must display the following acknowledgement: 
*     This product includes software developed by the micROS Group. and 
*     its contributors. 
* 4. Neither the name of the Group nor the names of its contributors may 
*     be used to endorse or promote products derived from this software 
*     without specific prior written permission. 
*
* THIS SOFTWARE IS PROVIDED BY MICROS,GROUP AND CONTRIBUTORS
* ''AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT
* NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
* FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL 
* THE MICROS,GROUP OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS 
* OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) 
* HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
* OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN 
* IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
* ===================================================================
*
* Author: Bin Di, Chunlian Fu, Junling Gao, Ruihao Li, Wei Yi.
*         (in alphabetical order)
*
*/

#ifndef __ACTOR_MANAGER__
#define __ACTOR_MANAGER__

#include <tinyxml.h>
#include <stdlib.h>
#include <string>
#include <sstream>
#include <algorithm>
#include "actor_schedule/actor_schedule.h"
//#include "actor_election/actor_election.h"
#include "ros/ros.h"
#include "actor_core/ACB.h"
#include "actor_core/actor_types.h"
#include "actor_core/actor_constant.h"
// //consensus
// #include "micros_swarm/micros_swarm.h"//vs only needed
// #include "micros_swarm/serialize.h"
// #include "micros_swarm/singleton.h"
// #include "micros_swarm/runtime_handle.h"//rth 
// #include "micros_swarm/runtime_core.h"

#include "actor_manager/CtrlMsg.h"

#include "RTPSPublisher.h"
#include "RTPSSubscriber.h"
#include "micROSRTPSExt.h"

#define ACTOR_MANAGER_DEBUG

using namespace std;

const int8_t MODE_ELECTION_ON = 1;
const int8_t MODE_ELECTION_OFF = 2;

class ActorManager {
public:
	ActorManager(ActorScheduler *aPActorScheduler);
	// ActorManager(ActorScheduler *aPActorScheduler, int64_t aPlatformID);
	// ActorManager(ActorScheduler *aPActorScheduler, ActorElection *aPActorElection, int64_t aPlatformID);	
	~ActorManager();
	
	// Based on FAST-RTPS
	std::string _curSwarmTaskXmlStr;
	std::string _curSwarmCommandStr;
	int64_t _platformID;
	string _swarmName, _actorName, _platformIdListStr, _dataStr;
	std::vector<string> _platformIdList;
	std::vector<string> _formationTypeList;
	std::vector<string> _formationPosList;
	void start();
	void handleMasterDataCmdCallback(CtrlMsg &aMsg);
	bool decomposeCtrlMsg();

private:
	ros::NodeHandle _nh;
	RTPSSubscriber<CtrlMsg> *_pSubCommand;
	ActorScheduler *_pActorScheduler;
	//ActorElection *_pActorElection;
	int8_t _workMode;
};

#endif

