/* ==================================================================
*
* Copyright (c) 2016-2019, micROS Group, NIIDT, TAIIC, HPCL.
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions 
* are met: 
*
* 1. Redistributions of source code must retain the above copyright 
*     notice, this list of conditions and the following disclaimer. 
* 2. Redistributions in binary form must reproduce the above copyright 
*     notice, this list of conditions and the following disclaimer in the 
*     documentation and/or other materials provided with the 
*     distribution. 
* 3. All advertising materials mentioning features or use of this software 
*     must display the following acknowledgement: 
*     This product includes software developed by the micROS Group. and 
*     its contributors. 
* 4. Neither the name of the Group nor the names of its contributors may 
*     be used to endorse or promote products derived from this software 
*     without specific prior written permission. 
*
* THIS SOFTWARE IS PROVIDED BY MICROS,GROUP AND CONTRIBUTORS
* ''AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT
* NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
* FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL 
* THE MICROS,GROUP OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS 
* OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) 
* HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
* OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN 
* IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
* ===================================================================
*
* Author: Bin Di, Chunlian Fu, Junling Gao, Ruihao Li, Wei Yi.
*         (in alphabetical order)
*
*/

// ros node for actor match test
#include "task_bind/task_bind.h"
#include "tinyxml.h"
#include "actor_core/actor_core.h"
#include <iostream>

#define DEBUG_FLAG

using namespace std;

int main(int argc, char **argv)
{
	ros::init(argc, argv, "pseudo_client");

	if (argc!=2) {
		ROS_INFO("usage: rosrun pseudo_client_node pseudo_client_node YOUR_TASK_FILE_PATH");
		return 0;
	}

	ros::NodeHandle nh_;
	#ifndef DEBUG_FLAG
	ros::service::waitForService("/actor_main/actor_match");//
	ros::ServiceClient client1 = nh_.serviceClient<actor_msgs::actor_match>("/actor_main/actor_match");
	#endif
	ros::service::waitForService("/actor_main/task_bind_service");//"/actor_main/task_bind_service"
	ros::ServiceClient client2 = nh_.serviceClient<actor_msgs::task_bind>("/actor_main/task_bind_service");

	// TEST 0
	{	// task bind srv
		actor_msgs::task_bind srv2;
		TiXmlDocument doc2;
		string XmlStr2;
		if ( !doc2.LoadFile(argv[1]) )
		{
			ROS_ERROR("[Pseudo Client Node] Error while open XML file!");
		}
		else
		{
			TiXmlPrinter printer2;
			doc2.Accept(&printer2);
			XmlStr2 = printer2.CStr();
		}
		srv2.request.str = XmlStr2;
		if (client2.call(srv2))
		{
			ROS_INFO("[Pseudo Client Node] Call task_bind client request");
			if (srv2.response.bind)
			{
				ROS_INFO("[Pseudo Client Node] Bind and generate ACB!");
			}
		}
		else
		{
			ROS_ERROR("[Pseudo Client Node] Failed to call service task_bind");
			return 1;
		}
	}

	// TEST condition 1
	{
		;
	}

	return 0;
}
