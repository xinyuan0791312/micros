/* ==================================================================
*
* Copyright (c) 2016-2019, micROS Group, NIIDT, TAIIC, HPCL.
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions 
* are met: 
*
* 1. Redistributions of source code must retain the above copyright 
*     notice, this list of conditions and the following disclaimer. 
* 2. Redistributions in binary form must reproduce the above copyright 
*     notice, this list of conditions and the following disclaimer in the 
*     documentation and/or other materials provided with the 
*     distribution. 
* 3. All advertising materials mentioning features or use of this software 
*     must display the following acknowledgement: 
*     This product includes software developed by the micROS Group. and 
*     its contributors. 
* 4. Neither the name of the Group nor the names of its contributors may 
*     be used to endorse or promote products derived from this software 
*     without specific prior written permission. 
*
* THIS SOFTWARE IS PROVIDED BY MICROS,GROUP AND CONTRIBUTORS
* ''AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT
* NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
* FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL 
* THE MICROS,GROUP OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS 
* OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) 
* HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
* OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN 
* IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
* ===================================================================
*
* Author: Bin Di, Chunlian Fu, Junling Gao, Ruihao Li, Wei Yi.
*         (in alphabetical order)
*
*/

#include "task_arrange/task_arrange.h"
#include "actor_match/hungary_match.h"

#include "debug_tools/debug_constant.h"

TaskArranger::TaskArranger(ActorMatcher* aPActorMatcher) {
	this->pActorMatcher = aPActorMatcher;
	this->nh_ = ros::NodeHandle("~/");
}

void TaskArranger::startService() {
	task_arrange_server = nh_.advertiseService("task_arrange_service", &TaskArranger::callArrangeService, this);
	ROS_INFO("[Task Arrange] Service initialized.");
}

bool TaskArranger::callArrangeService(actor_msgs::task_arrange::Request  &req, actor_msgs::task_arrange::Response &res) {
	ROS_INFO("[Task Arrange] Service comfirmed.");
	std::vector<std::string> ActorPathVec;
	std::vector<std::string> PlatformPathVec;
	// decode xml params in the request, get the pathes of all the platforms and actors
	TiXmlDocument doc;
	doc.Parse(req.str.c_str());
	TiXmlHandle hDoc(&doc);
	TiXmlElement *pEle = hDoc.FirstChildElement().Element();		// the first element is discarted, <FilePath>
	pEle = pEle->FirstChildElement();				// <Actors>
	for (TiXmlElement *pActor=pEle->FirstChildElement(); pActor!=NULL; pActor=pActor->NextSiblingElement()) {
		string temp_path = pActor->GetText();
		ActorPathVec.push_back(temp_path);
		cout << temp_path << endl;
	}
	pEle = pEle->NextSiblingElement();				// <Platforms>
	for (TiXmlElement *pPlatform=pEle->FirstChildElement(); pPlatform!=NULL; pPlatform=pPlatform->NextSiblingElement()) {
		string temp_path = pPlatform->GetText();
		PlatformPathVec.push_back(temp_path);
		cout << temp_path << endl;
	}
	// iterate among all platforms, iterate among all the actors
	int ActorNum = ActorPathVec.size();
	int PlatformNum = PlatformPathVec.size();
	int** matrix = new int* [PlatformNum];			// Hungary match init param of bipartite graph
	for (int i = 0; i < PlatformNum; i++)
		matrix[i] = new int[ActorNum];
	for (int i = 0; i < PlatformNum; i++)
	{
		for (int j = 0; j < ActorNum; j++)
		{
			matrix[i][j] = 0;
		}
	}
//	ros::service::waitForService("/actor_main/actor_match");
//	ros::ServiceClient client0 = nh_.serviceClient<actor_msgs::actor_match>("/actor_main/actor_match");
	for (int i=0; i<PlatformNum; i++) {
		if (pActorMatcher->resetTempPlatform(PlatformPathVec[i]))
		{ // reset actor_match local platform data
			ROS_INFO("[Task Arrange] Reset temp platform info in actor_matcher");
			for (int j=0; j<ActorNum; j++)
			{
				TiXmlDocument doc2;		// 1 stands for platform, 2 stands for actor
				TiXmlPrinter printer2;
				std::string xmlstr2;
				actor_msgs::actor_match srv;
				if ( !doc2.LoadFile(ActorPathVec[j]) )
				{
					ROS_ERROR("[Task Arrange] Error while open description xml file for Actor %d!", j + 1);
				}
				else
				{
					doc2.Accept(&printer2);
					xmlstr2 = printer2.CStr();
					srv.request.str = xmlstr2;
					ROS_INFO("[Task Arrange] Call actor_match.");
					pActorMatcher->tempActorMatch(srv.request, srv.response);
//					if (client0.call(srv))
					if (srv.response.match)
					{ // call actor_match service returns successfully
						if (srv.response.match)
						{ // if match
							matrix[i][j] = 1;
							#ifndef DEBUG_FLAG
							ROS_INFO("[Task Arrange] Platform %d and Actor %d match!", i + 1, j + 1);
							#endif
						}
//						sleep(1);
					}
					else
					{
						#ifndef DEBUG_FLAG
						ROS_ERROR("[Task Arrange] Failed to call actor_match service for Platform %d and Actor %d!", i + 1, j + 1);
						#endif
					}
				}
			}
		}
		else
		{
			ROS_ERROR("[Task Arrange] Failed to reset actor_match for Platform %d!", i + 1);
		}
	}
	// do Hungary match
	HungaryMatch a(PlatformNum, ActorNum, matrix);
	int *hungary_match_result;
	int match_num = a.doHungaryMatch(hungary_match_result);
	// save the result
	for (int i=0; i<PlatformNum; i++) {
		cout << "Task Arrangement Result:" << endl;
		cout << i+1 << ":" << hungary_match_result[i]+1 << endl;
	}
	// delete
	for (int i = 0; i < PlatformNum; i++)	{
		delete[] matrix[i];
	}
	delete[] matrix;
	delete[] hungary_match_result;
/*
	// test Hungary Match
	int myResultData[4];
	int *myResult = myResultData;
	int **myTestMatrix = new int*[4];
	int myData[4][4] = {{1,1,1,1},{1,1,1,1},{1,1,1,1},{1,1,1,1}};
	for (int i=0; i<4; i++) {
		myTestMatrix[i] = myData[i];
	}
	HungaryMatch b(4, 4, myTestMatrix);
	int Cnt = b.doHungaryMatch(myResult);
	cout << "Hungary Match Test Result:" << endl;
	for (int i=0; i<4; i++) {
		cout << i+1 <<":" << myResult[i]+1 << endl;
	}
*/
}