/* ==================================================================
*
* Copyright (c) 2016-2019, micROS Group, NIIDT, TAIIC, HPCL.
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions 
* are met: 
*
* 1. Redistributions of source code must retain the above copyright 
*     notice, this list of conditions and the following disclaimer. 
* 2. Redistributions in binary form must reproduce the above copyright 
*     notice, this list of conditions and the following disclaimer in the 
*     documentation and/or other materials provided with the 
*     distribution. 
* 3. All advertising materials mentioning features or use of this software 
*     must display the following acknowledgement: 
*     This product includes software developed by the micROS Group. and 
*     its contributors. 
* 4. Neither the name of the Group nor the names of its contributors may 
*     be used to endorse or promote products derived from this software 
*     without specific prior written permission. 
*
* THIS SOFTWARE IS PROVIDED BY MICROS,GROUP AND CONTRIBUTORS
* ''AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT
* NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
* FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL 
* THE MICROS,GROUP OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS 
* OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) 
* HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
* OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN 
* IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
* ===================================================================
*
* Author: Bin Di, Chunlian Fu, Junling Gao, Ruihao Li, Wei Yi.
*         (in alphabetical order)
*
*/

#include "micROSRTPS.h"
#include <fastrtps/utils/IPFinder.h>
#include <fastrtps/xmlparser/XMLProfileManager.h>
#include <iostream>
#include <string>
#include <fstream>
#include "MyPubSubTypes.h"
using namespace eprosima::fastrtps;

namespace micROS {

	Participant *gPParticipant=NULL;
	int gDomainId = 0;
	std::map<std::string,TopicDataType*> gTopicDataTypeDict;
	boost::mutex gTopicDataTypeMutex;

#ifdef DDS_STATIC_DISCOVERY
	bool init(const std::string& aStaticXmlFilePath){
		if(aStaticXmlFilePath.empty()){
			std::cout<<"error: there is no dds static discovery xml file!"<<std::endl;
			return false;
		}
		// Load an XML file with predefined profiles for publisher and subscriber
		xmlparser::XMLProfileManager::loadXMLFile(aStaticXmlFilePath);
	#ifdef ONE_PARTICIPANT
		if(gPParticipant == NULL){
			gTopicDataTypeDict.clear();
			// Create RTPSParticipant
			gPParticipant = Domain::createParticipant(DDS_PARTICIPANT_PROFILE);
			if(gPParticipant == nullptr)
				return false;
		}
	#endif
	}
#else 

	bool init(const int aDomainId, const std::string& aRobotListFilePath){
		gDomainId = aDomainId;
#ifdef ONE_PARTICIPANT
		if(gPParticipant == NULL){
			gTopicDataTypeDict.clear();

			// Create RTPSParticipant
			ParticipantAttributes PParam;
			PParam.rtps.builtin.domainId = gDomainId;
			PParam.rtps.builtin.discovery_config.leaseDuration = c_TimeInfinite;
			//PParam.rtps.builtin.leaseDuration.seconds = 10;
			PParam.rtps.builtin.discovery_config.leaseDuration_announcementperiod.seconds = 10;
			PParam.rtps.setName("Participant_publisher");  //You can put here the name you want

			// discovery participant by user defined locator list
			if (!aRobotListFilePath.empty()) {
				std::fstream fs;
				fs.open(aRobotListFilePath.c_str(),std::fstream::in);
				if(fs.is_open()){
					// get local IPv4 addresses
					LocatorList_t loclist;
        			IPFinder::getIP4Address(&loclist);

					// read locator from file, and init initialPeersList.
					std::string addr;
					while(getline(fs, addr)){
						if(addr.empty() || addr[0] == '#') continue;
						
						eprosima::fastrtps::rtps::Locator_t locator;
						int pos = addr.find(":");
						//locator.set_IP4_address(addr.substr(0, pos));
						locator.port = atoi(addr.substr(pos+1).c_str());
						PParam.rtps.builtin.initialPeersList.push_back(locator);

						locator.port = 0;
						if(loclist.contains(locator) && PParam.rtps.builtin.metatrafficUnicastLocatorList.size() == 0){
							locator.port = atoi(addr.substr(pos+1).c_str());
							PParam.rtps.builtin.metatrafficUnicastLocatorList.push_back(locator);
						}
					}
				}
				fs.close();

				std::cout<<"Use Static Participant Discovery:initialPeersList:"<<PParam.rtps.builtin.initialPeersList<<std::endl
				<<"metatrafficUnicastLocatorList:"<<PParam.rtps.builtin.metatrafficUnicastLocatorList<<std::endl;
			} 
			
			gPParticipant = Domain::createParticipant(PParam);
			if(gPParticipant == nullptr)
				return false;
		}
		return true;
#endif
	}
#endif

	void finish() {
#ifdef ONE_PARTICIPANT
		if (gPParticipant != nullptr)
			Domain::removeParticipant(gPParticipant);
		std::map<std::string,TopicDataType*>::iterator it;
		it=gTopicDataTypeDict.begin();
		while (it!=gTopicDataTypeDict.end()) {
			delete it->second;
			it++;
		}
#endif
	}
}

