// Copyright 2016 Proyectos y Sistemas de Mantenimiento SL (eProsima).
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/**
 * @file RTPSClient.h
 *
 */


#ifndef __RTPS_CLIENT__
#define __RTPS_CLIENT__

#include "fastrtps/fastrtps_fwd.h"
#include "fastrtps/subscriber/SampleInfo.h"

#include "fastrtps/publisher/PublisherListener.h"
#include "fastrtps/subscriber/SubscriberListener.h"

#include "RequestMetaData.h"
#include "ReplyMetaData.h"

#include "micROSRTPSExt.h"

using namespace eprosima;
using namespace fastrtps;

template<class RequestType,class ReplyType>
class RTPSClient {
public:
	RTPSClient(std::string aTopicName);
	virtual ~RTPSClient();
	Publisher* mp_operation_pub;
	Subscriber* mp_result_sub;
	Participant* mp_participant;
	bool init();
	bool call(RequestType& aRequest,ReplyType& aReply);
	bool isReady();
private:
	SampleInfo_t m_sampleInfo;

	void resetResult();

	std::string _topicName;

	class OperationListener:public PublisherListener
	{
	public:
		OperationListener(RTPSClient* up):mp_up(up){}
		~OperationListener(){}
		RTPSClient* mp_up;
		void onPublicationMatched(Publisher* pub,MatchingInfo& info);
	}m_operationsListener;
	class ResultListener:public SubscriberListener
	{
	public:
		ResultListener(RTPSClient* up):mp_up(up){}
		~ResultListener(){}
		RTPSClient* mp_up;
		void onSubscriptionMatched(Subscriber* sub,MatchingInfo& info);
		void onNewDataMessage(Subscriber* sub);
	}m_resultsListener;
	bool m_isReady;
	int m_operationMatched;
	int m_resultMatched;
	uint32_t m_operationId;

};

#include "RTPSClient.hpp"

#endif /* __RTPS_CLIENT__ */
