// Copyright 2016 Proyectos y Sistemas de Mantenimiento SL (eProsima).
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/**
 * @file RTPSClient.cpp
 *
 */

#include "fastrtps/fastrtps_all.h"

template<class RequestType,class ReplyType>
RTPSClient<RequestType,ReplyType>::RTPSClient(std::string aTopicName):
mp_operation_pub(nullptr),
mp_result_sub(nullptr),
mp_participant(micROS::gPParticipant),
m_operationsListener(nullptr),
m_resultsListener(nullptr),
m_isReady(false),
m_operationMatched(0),
m_resultMatched(0),
_topicName(aTopicName)
{
	m_operationsListener.mp_up = this;
	m_resultsListener.mp_up = this;
	m_operationId=0;
}

template<class RequestType,class ReplyType>
RTPSClient<RequestType,ReplyType>::~RTPSClient()
{
}

template<class RequestType,class ReplyType>
bool RTPSClient<RequestType,ReplyType>::init()
{

/*
	//CREATE RTPSParticipant
	ParticipantAttributes PParam;
	PParam.rtps.defaultSendPort = 10042;
	PParam.rtps.builtin.domainId = 80;
	PParam.rtps.builtin.use_SIMPLE_EndpointDiscoveryProtocol = true;
	PParam.rtps.builtin.use_SIMPLE_RTPSParticipantDiscoveryProtocol = true;
	PParam.rtps.builtin.m_simpleEDP.use_PublicationReaderANDSubscriptionWriter = true;
	PParam.rtps.builtin.m_simpleEDP.use_PublicationWriterANDSubscriptionReader = true;
	PParam.rtps.builtin.leaseDuration = c_TimeInfinite;
	PParam.rtps.setName( "client_RTPSParticipant");
	mp_participant = Domain::createParticipant(PParam);
	if(mp_participant == nullptr)
		return false;
*/
/*
	//REGISTER TYPES
	mp_resultdatatype = new ResultDataType();
	mp_operationdatatype = new OperationDataType();
	Domain::registerType(mp_participant,mp_resultdatatype);
	Domain::registerType(mp_participant,mp_operationdatatype);
*/
	micROS::registerType<RequestMetaData<RequestType> >();
	micROS::registerType<ReplyMetaData<ReplyType> >();

	// DATA PUBLISHER
	PublisherAttributes PubDataparam;
	PubDataparam.topic.topicDataType = RequestMetaData<RequestType>::getName();
	PubDataparam.topic.topicKind = NO_KEY;
	PubDataparam.topic.topicName = _topicName+"Request";
/*
	PubDataparam.topic.historyQos.kind = KEEP_LAST_HISTORY_QOS;
	PubDataparam.topic.historyQos.depth = 2;
	PubDataparam.topic.resourceLimitsQos.max_samples = 50;
	PubDataparam.topic.resourceLimitsQos.allocated_samples = 50;
	PubDataparam.qos.m_reliability.kind = RELIABLE_RELIABILITY_QOS;
	mp_operation_pub = Domain::createPublisher(mp_participant,PubDataparam,(PublisherListener*)&this->m_operationsListener);
	if(mp_operation_pub == nullptr)
		return false;
*/

	mp_operation_pub = Domain::createPublisher(mp_participant,PubDataparam,(PublisherListener*)&this->m_operationsListener);
	if(mp_operation_pub == nullptr)
		return false;	
/*
	//DATA SUBSCRIBER
	SubscriberAttributes SubDataparam;
	Locator_t loc;
	loc.port = 7555;
	PubDataparam.unicastLocatorList.push_back(loc);
	SubDataparam.topic.topicDataType = "Result";
	SubDataparam.topic.topicKind = NO_KEY;
	SubDataparam.topic.topicName = "Results";
	SubDataparam.topic.historyQos.kind = KEEP_LAST_HISTORY_QOS;
	SubDataparam.topic.historyQos.depth = 100;
	SubDataparam.topic.resourceLimitsQos.max_samples = 100;
	SubDataparam.topic.resourceLimitsQos.allocated_samples = 100;
	mp_result_sub = Domain::createSubscriber(mp_participant,SubDataparam,(SubscriberListener*)&this->m_resultsListener);
	if(mp_result_sub == nullptr)
		return false;
*/
	SubscriberAttributes SubDataparam;
	SubDataparam.topic.topicKind = NO_KEY;
	SubDataparam.topic.topicDataType = ReplyMetaData<ReplyType>::getName(); //Must be registered before the creation of the subscriber
	SubDataparam.topic.topicName = _topicName+"Reply";
	mp_result_sub = Domain::createSubscriber(mp_participant,SubDataparam,(SubscriberListener*)&this->m_resultsListener);
	if(mp_result_sub == nullptr)
		return false;
	return true;
}

template<class RequestType,class ReplyType>
bool RTPSClient<RequestType,ReplyType>::call(RequestType& aRequest,ReplyType& aReply) {
	if(!m_isReady)
		return false;
		//return Result::SERVER_NOT_READY;
	RequestMetaData<RequestType> requestMeta;
	ReplyMetaData<ReplyType> replyMeta;

	requestMeta.guid(mp_operation_pub->getGuid());
	requestMeta.operationId(m_operationId++);
	requestMeta.data(aRequest);

	replyMeta.guid(c_Guid_Unknown);
	replyMeta.operationId(0);

	mp_operation_pub->write((void*)&requestMeta);
	do{
		//resetResult();
		mp_result_sub->waitForUnreadMessage();
		mp_result_sub->takeNextData((void*)&replyMeta,&m_sampleInfo);
	}while(m_sampleInfo.sampleKind !=ALIVE ||
			replyMeta.guid() != requestMeta.guid() ||
			replyMeta.operationId() != requestMeta.operationId());
	if(replyMeta.resultType() == RESULTTYPE::GOOD_RESULT)
	{
		aReply = replyMeta.data();
	}
	else 
		return false;
	return true;//m_result.m_resultType;
}


template<class RequestType,class ReplyType>
void RTPSClient<RequestType,ReplyType>::resetResult()
{
	/*
	m_result.m_guid = c_Guid_Unknown;
	m_result.m_operationId = 0;
	m_result.m_result = 0;
	*/
}

template<class RequestType,class ReplyType>
void RTPSClient<RequestType,ReplyType>::OperationListener::onPublicationMatched(Publisher* pub,MatchingInfo& info)
{
	if(info.status == MATCHED_MATCHING)
	{
		mp_up->m_operationMatched++;
	}
	else
		mp_up->m_operationMatched--;
	mp_up->isReady();
}

template<class RequestType,class ReplyType>
void RTPSClient<RequestType,ReplyType>::ResultListener::onSubscriptionMatched(Subscriber* sub,MatchingInfo& info)
{
	if(info.status == MATCHED_MATCHING)
	{
		mp_up->m_resultMatched++;
	}
	else
		mp_up->m_resultMatched--;
	mp_up->isReady();
}

template<class RequestType,class ReplyType>
void RTPSClient<RequestType,ReplyType>::ResultListener::onNewDataMessage(Subscriber* sub)
{
}

template<class RequestType,class ReplyType>
bool RTPSClient<RequestType,ReplyType>::isReady()
{
	if(m_operationMatched == 1 && m_resultMatched == 1)
		m_isReady = true;
	else
		m_isReady = false;
	return m_isReady;
}
