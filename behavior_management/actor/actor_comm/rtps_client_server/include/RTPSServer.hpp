// Copyright 2016 Proyectos y Sistemas de Mantenimiento SL (eProsima).
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/**
 * @file RTPSServer.cpp
 *
 */

#include "fastrtps/fastrtps_all.h"

template<class RequestType, class ReplyType>
RTPSServer<RequestType,ReplyType>::RTPSServer(std::string aTopicName,void(* aPFunc)(RequestType&, ReplyType&)):
mp_operation_sub(nullptr),
mp_result_pub(nullptr),
mp_participant(micROS::gPParticipant),
m_n_served(0),
m_operationsListener(nullptr),
m_resultsListener(nullptr),
_topicName(aTopicName)
{
	m_operationsListener.mp_up = this;
	m_resultsListener.mp_up = this;
	_pFunc=boost::bind(aPFunc,_1,_2);
}

template<class RequestType, class ReplyType>
RTPSServer<RequestType,ReplyType>::~RTPSServer()
{
}

template<class RequestType, class ReplyType>
void RTPSServer<RequestType,ReplyType>::serve()
{
	cout << "Enter a number to stop the server: ";
	int aux;
	std::cin >> aux;
}

template<class RequestType, class ReplyType>
void RTPSServer<RequestType,ReplyType>::serve(uint32_t samples)
{
	while(m_n_served < samples)
		eClock::my_sleep(100);
}

template<class RequestType, class ReplyType>
bool RTPSServer<RequestType,ReplyType>::init()
{
	/*
	//CREATE RTPSParticipant
	ParticipantAttributes PParam;
	PParam.rtps.defaultSendPort = 10042;
	PParam.rtps.builtin.domainId = 80;
	PParam.rtps.builtin.use_SIMPLE_EndpointDiscoveryProtocol = true;
	PParam.rtps.builtin.use_SIMPLE_RTPSParticipantDiscoveryProtocol = true;
	PParam.rtps.builtin.m_simpleEDP.use_PublicationReaderANDSubscriptionWriter = true;
	PParam.rtps.builtin.m_simpleEDP.use_PublicationWriterANDSubscriptionReader = true;
	PParam.rtps.builtin.leaseDuration = c_TimeInfinite;
	PParam.rtps.setName("server_RTPSParticipant");
	mp_participant = Domain::createParticipant(PParam);
	if(mp_participant == nullptr)
		return false;
	*/

	//REGISTER TYPES
	/*
	mp_resultdatatype = new ResultDataType();
	mp_operationdatatype = new OperationDataType();
	Domain::registerType(mp_participant,mp_resultdatatype);
	Domain::registerType(mp_participant,mp_operationdatatype);
	*/
	micROS::registerType<RequestMetaData<RequestType> >();
	micROS::registerType<ReplyMetaData<ReplyType> >();

	// DATA PUBLISHER
	
	PublisherAttributes PubDataparam;
	PubDataparam.topic.topicDataType = ReplyMetaData<ReplyType>::getName();
	PubDataparam.topic.topicKind = NO_KEY;
	PubDataparam.topic.topicName = _topicName+"Reply";
	/*	
	PubDataparam.topic.historyQos.kind = KEEP_LAST_HISTORY_QOS;
	PubDataparam.topic.historyQos.depth = 1000;
	PubDataparam.topic.resourceLimitsQos.max_samples = 1500;
	PubDataparam.topic.resourceLimitsQos.allocated_samples = 1000;
	PubDataparam.qos.m_reliability.kind = RELIABLE_RELIABILITY_QOS;
	mp_result_pub = Domain::createPublisher(mp_participant,PubDataparam,(PublisherListener*)&this->m_resultsListener);
	if(mp_result_pub == nullptr)
		return false;
	*/
	mp_result_pub = Domain::createPublisher(mp_participant,PubDataparam,(PublisherListener*)&this->m_resultsListener);
	if(mp_result_pub == nullptr)
		return false;

	//DATA SUBSCRIBER
	/*
	SubscriberAttributes SubDataparam;
	Locator_t loc;
	loc.port = 7555;
	PubDataparam.unicastLocatorList.push_back(loc);
	SubDataparam.topic.topicDataType = "Operation";
	SubDataparam.topic.topicKind = NO_KEY;
	SubDataparam.topic.topicName = "Operations";
	SubDataparam.topic.historyQos.kind = KEEP_LAST_HISTORY_QOS;
	SubDataparam.topic.historyQos.depth = 1000;
	SubDataparam.topic.resourceLimitsQos.max_samples = 1500;
	SubDataparam.topic.resourceLimitsQos.allocated_samples = 1000;
	mp_operation_sub = Domain::createSubscriber(mp_participant,SubDataparam,(SubscriberListener*)&this->m_operationsListener);
	if(mp_operation_sub == nullptr)
		return false;
	*/

	// Create Subscriber
	SubscriberAttributes SubDataparam;
	SubDataparam.topic.topicKind = NO_KEY;
	SubDataparam.topic.topicDataType = RequestMetaData<RequestType>::getName(); //Must be registered before the creation of the subscriber
	SubDataparam.topic.topicName = _topicName+"Request";
	mp_operation_sub = Domain::createSubscriber(mp_participant,SubDataparam,(SubscriberListener*)&this->m_operationsListener);
	if(mp_operation_sub == nullptr)
		return false;

	return true;
}

/*
template<class RequestType, class ReplyType>
Result::RESULTTYPE RTPSServer<RequestType,ReplyType>::calculate(Operation::OPERATIONTYPE type,
		int32_t num1, int32_t num2, int32_t* result)
{
	switch(type)
	{
	case Operation::SUBTRACTION:
	{
		*result = num1-num2;
		break;
	}
	case Operation::ADDITION:
	{
		*result = num1+num2;
		break;
	}

	case Operation::MULTIPLICATION:
	{
		*result = num1*num2;
		break;
	}
	case Operation::DIVISION:
	{
		if(num2 == 0)
			return Result::ERROR_RESULT;
		break;
	}
	}
	return Result::GOOD_RESULT;
}
*/

template<class RequestType, class ReplyType>
void RTPSServer<RequestType,ReplyType>::OperationListener::onNewDataMessage(Subscriber* sub)
{
	RequestMetaData<RequestType> requestMeta;
	ReplyMetaData<ReplyType> replyMeta;

	mp_up->mp_operation_sub->takeNextData((void*)&requestMeta,&m_sampleInfo);
	
	if(m_sampleInfo.sampleKind == ALIVE)
	{
		++mp_up->m_n_served;
		//replyMeta.guid(m_operation.m_guid);
		replyMeta.guid(requestMeta.guid());
		replyMeta.operationId(requestMeta.operationId());
		mp_up->_pFunc(requestMeta.data(),replyMeta.data());
		std::cout<<"Server receives request GUID="<<requestMeta.guid()<<" REQUEST_ID="<<requestMeta.operationId()<<std::endl;
		/*
		m_result.m_guid = m_operation.m_guid;
		m_result.m_operationId = m_operation.m_operationId;
		m_result.m_result = 0;
		m_result.m_resultType = mp_up->calculate(m_operation.m_operationType,
		m_operation.m_num1,m_operation.m_num2,&m_result.m_result);
		mp_up->mp_result_pub->write((void*)&m_result);
		*/
		mp_up->mp_result_pub->write((void*)&replyMeta);
	}
}

template<class RequestType, class ReplyType>
void RTPSServer<RequestType,ReplyType>::OperationListener::onSubscriptionMatched(Subscriber* sub,MatchingInfo& info)
{

}


template<class RequestType, class ReplyType>
void RTPSServer<RequestType,ReplyType>::ResultListener::onPublicationMatched(Publisher* pub,MatchingInfo& info)
{

}
