// Copyright 2016 Proyectos y Sistemas de Mantenimiento SL (eProsima).
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/**
 * @file RTPSServer.h
 *
 */

#ifndef __RTPS_SERVER__
#define __RTPS_SERVER__

#include "fastrtps/fastrtps_fwd.h"
#include "fastrtps/subscriber/SampleInfo.h"

#include "fastrtps/publisher/PublisherListener.h"
#include "fastrtps/subscriber/SubscriberListener.h"

#include <boost/bind.hpp>
#include <boost/function.hpp>

#include "micROSRTPSExt.h"

#include "RequestMetaData.h"
#include "ReplyMetaData.h"

using namespace eprosima;
using namespace fastrtps;

template<class RequestType, class ReplyType>
class RTPSServer {
	friend class OperationListener;
	friend class ResultListener;
public:
	RTPSServer(std::string aTopicName,void(* aPFunc)(RequestType&, ReplyType&));
	virtual ~RTPSServer();
	bool init();
	//Serve indefinitely.
	void serve();
	//Serve for samples operations.
	void serve(uint32_t samples);
private:

	Subscriber* mp_operation_sub;
	Publisher* mp_result_pub;

	Participant* mp_participant;

	/*
	Result::RESULTTYPE calculate(Operation::OPERATIONTYPE type, int32_t num1,int32_t num2,int32_t* result);
	*/

	std::string _topicName;
	//the main serve function
	boost::function<void (RequestType&,ReplyType&)> _pFunc;
public:
	uint32_t m_n_served;
	class OperationListener:public SubscriberListener
	{
	public:
		OperationListener(RTPSServer* up):mp_up(up){}
		~OperationListener(){}
		RTPSServer* mp_up;
		void onSubscriptionMatched(Subscriber* sub,MatchingInfo& info);
		void onNewDataMessage(Subscriber*sub);
		SampleInfo_t m_sampleInfo;
	}m_operationsListener;
	class ResultListener:public PublisherListener
	{
	public:
		ResultListener(RTPSServer* up):mp_up(up){}
		~ResultListener(){}
		RTPSServer* mp_up;
		void onPublicationMatched(Publisher* pub,MatchingInfo& info);
	}m_resultsListener;
};

#include "RTPSServer.hpp"

#endif /* __RTPS_SERVER__ */
