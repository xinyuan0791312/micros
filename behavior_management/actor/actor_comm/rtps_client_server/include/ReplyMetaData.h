// Copyright 2016 Proyectos y Sistemas de Mantenimiento SL (eProsima).
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/*! 
 * @file ReplyMetaData.h
 * This header file contains the declaration of the described types in the IDL file.
 *
 * This file was generated by the tool gen.
 */

#ifndef _ReplyMetaData_H_
#define _ReplyMetaData_H_

// TODO Poner en el contexto.

#include <stdint.h>
#include <array>
#include <string>
#include <vector>
#include "fastrtps/rtps/common/all_common.h"
#if defined(_WIN32)
#if defined(EPROSIMA_USER_DLL_EXPORT)
#define eProsima_user_DllExport __declspec( dllexport )
#else
#define eProsima_user_DllExport
#endif
#else
#define eProsima_user_DllExport
#endif

#if defined(_WIN32)
#if defined(EPROSIMA_USER_DLL_EXPORT)
#if defined(ReplyMetaData_SOURCE)
#define ReplyMetaData_DllAPI __declspec( dllexport )
#else
#define ReplyMetaData_DllAPI __declspec( dllimport )
#endif // ReplyMetaData_SOURCE
#else
#define ReplyMetaData_DllAPI
#endif
#else
#define ReplyMetaData_DllAPI
#endif // _WIN32

namespace eprosima
{
    namespace fastcdr
    {
        class Cdr;
    }
}

typedef eprosima::fastrtps::rtps::GUID_t GUID_t;


/*!
 * @brief This class represents the enumeration RESULTTYPE defined by the user in the IDL file.
 * @ingroup REPLYMETADATA
 */
enum RESULTTYPE : uint32_t
{
    GOOD_RESULT,
    ERROR_RESULT,
    SERVER_NOT_READY
};
/*!
 * @brief This class represents the structure ReplyMetaData defined by the user in the IDL file.
 * @ingroup REPLYMETADATA
 */
template<class ReplyDataType>
class ReplyMetaData
{
public:

    /*!
     * @brief Default constructor.
     */
    eProsima_user_DllExport ReplyMetaData();
    
    /*!
     * @brief Default destructor.
     */
    eProsima_user_DllExport ~ReplyMetaData();
    
    /*!
     * @brief Copy constructor.
     * @param x Reference to the object ReplyMetaData that will be copied.
     */
    eProsima_user_DllExport ReplyMetaData(const ReplyMetaData &x);
    
    /*!
     * @brief Move constructor.
     * @param x Reference to the object ReplyMetaData that will be copied.
     */
    eProsima_user_DllExport ReplyMetaData(ReplyMetaData &&x);
    
    /*!
     * @brief Copy assignment.
     * @param x Reference to the object ReplyMetaData that will be copied.
     */
    eProsima_user_DllExport ReplyMetaData& operator=(const ReplyMetaData &x);
    
    /*!
     * @brief Move assignment.
     * @param x Reference to the object ReplyMetaData that will be copied.
     */
    eProsima_user_DllExport ReplyMetaData& operator=(ReplyMetaData &&x);
    
    /*!
     * @brief This function sets a value in member guid
     * @param _guid New value for member guid
     */
    inline eProsima_user_DllExport void guid(GUID_t _guid)
    {
        m_guid = _guid;
    }

    /*!
     * @brief This function returns the value of member guid
     * @return Value of member guid
     */
    inline eProsima_user_DllExport GUID_t guid() const
    {
        return m_guid;
    }

    /*!
     * @brief This function returns a reference to member guid
     * @return Reference to member guid
     */
    inline eProsima_user_DllExport GUID_t& guid()
    {
        return m_guid;
    }
    /*!
     * @brief This function sets a value in member operationId
     * @param _operationId New value for member operationId
     */
    inline eProsima_user_DllExport void operationId(uint32_t _operationId)
    {
        m_operationId = _operationId;
    }

    /*!
     * @brief This function returns the value of member operationId
     * @return Value of member operationId
     */
    inline eProsima_user_DllExport uint32_t operationId() const
    {
        return m_operationId;
    }

    /*!
     * @brief This function returns a reference to member operationId
     * @return Reference to member operationId
     */
    inline eProsima_user_DllExport uint32_t& operationId()
    {
        return m_operationId;
    }
    /*!
     * @brief This function sets a value in member resultType
     * @param _resultType New value for member resultType
     */
    inline eProsima_user_DllExport void resultType(RESULTTYPE _resultType)
    {
        m_resultType = _resultType;
    }

    /*!
     * @brief This function returns the value of member resultType
     * @return Value of member resultType
     */
    inline eProsima_user_DllExport RESULTTYPE resultType() const
    {
        return m_resultType;
    }

    /*!
     * @brief This function returns a reference to member resultType
     * @return Reference to member resultType
     */
    inline eProsima_user_DllExport RESULTTYPE& resultType()
    {
        return m_resultType;
    }
    /*!
     * @brief This function copies the value in member data
     * @param _data New value to be copied in member data
     */
    inline eProsima_user_DllExport void data(const ReplyDataType &_data)
    {
        m_data = _data;
    }

    /*!
     * @brief This function moves the value in member data
     * @param _data New value to be moved in member data
     */
    inline eProsima_user_DllExport void data(ReplyDataType &&_data)
    {
        m_data = std::move(_data);
    }

    /*!
     * @brief This function returns a constant reference to member data
     * @return Constant reference to member data
     */
    inline eProsima_user_DllExport const ReplyDataType& data() const
    {
        return m_data;
    }

    /*!
     * @brief This function returns a reference to member data
     * @return Reference to member data
     */
    inline eProsima_user_DllExport ReplyDataType& data()
    {
        return m_data;
    }
    
    /*!
     * @brief This function returns the maximum serialized size of an object
     * depending on the buffer alignment.
     * @param current_alignment Buffer alignment.
     * @return Maximum serialized size.
     */
    eProsima_user_DllExport static size_t getMaxCdrSerializedSize(size_t current_alignment = 0);

    /*!
     * @brief This function returns the serialized size of a data depending on the buffer alignment.
     * @param data Data which is calculated its serialized size.
     * @param current_alignment Buffer alignment.
     * @return Serialized size.
     */
    eProsima_user_DllExport static size_t getCdrSerializedSize(const ReplyMetaData& data, size_t current_alignment = 0);


    /*!
     * @brief This function serializes an object using CDR serialization.
     * @param cdr CDR serialization object.
     */
    eProsima_user_DllExport void serialize(eprosima::fastcdr::Cdr &cdr) const;

    /*!
     * @brief This function deserializes an object using CDR serialization.
     * @param cdr CDR serialization object.
     */
    eProsima_user_DllExport void deserialize(eprosima::fastcdr::Cdr &cdr);



    /*!
     * @brief This function returns the maximum serialized size of the Key of an object
     * depending on the buffer alignment.
     * @param current_alignment Buffer alignment.
     * @return Maximum serialized size.
     */
    eProsima_user_DllExport static size_t getKeyMaxCdrSerializedSize(size_t current_alignment = 0);

    /*!
     * @brief This function tells you if the Key has been defined for this type
     */
    eProsima_user_DllExport static bool isKeyDefined();

    /*!
     * @brief This function serializes the key members of an object using CDR serialization.
     * @param cdr CDR serialization object.
     */
    eProsima_user_DllExport void serializeKey(eprosima::fastcdr::Cdr &cdr) const;
    
    eProsima_user_DllExport static std::string getName() { return ReplyDataType::getName()+"MetaData"; } 
private:
    GUID_t m_guid;
    uint32_t m_operationId;
    RESULTTYPE m_resultType;
    ReplyDataType m_data;
};

#include "ReplyMetaData.hpp"

#endif // _ReplyMetaData_H_
