// Copyright 2016 Proyectos y Sistemas de Mantenimiento SL (eProsima).
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/*! 
 * @file RequestMetaData.h
 * This header file contains the declaration of the described types in the IDL file.
 *
 * This file was generated by the tool gen.
 */

#ifndef _RequestMetaData_H_
#define _RequestMetaData_H_

// TODO Poner en el contexto.

#include <stdint.h>
#include <array>
#include <string>
#include <vector>
#include "fastrtps/rtps/common/all_common.h"

#if defined(_WIN32)
#if defined(EPROSIMA_USER_DLL_EXPORT)
#define eProsima_user_DllExport __declspec( dllexport )
#else
#define eProsima_user_DllExport
#endif
#else
#define eProsima_user_DllExport
#endif

#if defined(_WIN32)
#if defined(EPROSIMA_USER_DLL_EXPORT)
#if defined(RequestMetaData_SOURCE)
#define RequestMetaData_DllAPI __declspec( dllexport )
#else
#define RequestMetaData_DllAPI __declspec( dllimport )
#endif // RequestMetaData_SOURCE
#else
#define RequestMetaData_DllAPI
#endif
#else
#define RequestMetaData_DllAPI
#endif // _WIN32

namespace eprosima
{
    namespace fastcdr
    {
        class Cdr;
    }
}

typedef eprosima::fastrtps::rtps::GUID_t GUID_t;


/*!
 * @brief This class represents the structure RequestMetaData defined by the user in the IDL file.
 * @ingroup REQUESTMETADATA
 */
template<class RequestDataType>
class RequestMetaData
{
public:

    /*!
     * @brief Default constructor.
     */
    eProsima_user_DllExport RequestMetaData();
    
    /*!
     * @brief Default destructor.
     */
    eProsima_user_DllExport ~RequestMetaData();
    
    /*!
     * @brief Copy constructor.
     * @param x Reference to the object RequestMetaData that will be copied.
     */
    eProsima_user_DllExport RequestMetaData(const RequestMetaData<RequestDataType> &x);
    
    /*!
     * @brief Move constructor.
     * @param x Reference to the object RequestMetaData that will be copied.
     */
    eProsima_user_DllExport RequestMetaData(RequestMetaData<RequestDataType> &&x);
    
    /*!
     * @brief Copy assignment.
     * @param x Reference to the object RequestMetaData that will be copied.
     */
    eProsima_user_DllExport RequestMetaData& operator=(const RequestMetaData<RequestDataType> &x);
    
    /*!
     * @brief Move assignment.
     * @param x Reference to the object RequestMetaData that will be copied.
     */
    eProsima_user_DllExport RequestMetaData& operator=(RequestMetaData<RequestDataType> &&x);
    
    /*!
     * @brief This function sets a value in member guid
     * @param _guid New value for member guid
     */
    inline eProsima_user_DllExport void guid(GUID_t _guid)
    {
        m_guid = _guid;
    }

    /*!
     * @brief This function returns the value of member guid
     * @return Value of member guid
     */
    inline eProsima_user_DllExport GUID_t guid() const
    {
        return m_guid;
    }

    /*!
     * @brief This function returns a reference to member guid
     * @return Reference to member guid
     */
    inline eProsima_user_DllExport GUID_t& guid()
    {
        return m_guid;
    }
    /*!
     * @brief This function sets a value in member operationId
     * @param _operationId New value for member operationId
     */
    inline eProsima_user_DllExport void operationId(uint32_t _operationId)
    {
        m_operationId = _operationId;
    }

    /*!
     * @brief This function returns the value of member operationId
     * @return Value of member operationId
     */
    inline eProsima_user_DllExport uint32_t operationId() const
    {
        return m_operationId;
    }

    /*!
     * @brief This function returns a reference to member operationId
     * @return Reference to member operationId
     */
    inline eProsima_user_DllExport uint32_t& operationId()
    {
        return m_operationId;
    }
    /*!
     * @brief This function copies the value in member data
     * @param _data New value to be copied in member data
     */
    inline eProsima_user_DllExport void data(const RequestDataType &_data)
    {
        m_data = _data;
    }

    /*!
     * @brief This function moves the value in member data
     * @param _data New value to be moved in member data
     */
    inline eProsima_user_DllExport void data(RequestDataType &&_data)
    {
        m_data = std::move(_data);
    }

    /*!
     * @brief This function returns a constant reference to member data
     * @return Constant reference to member data
     */
    inline eProsima_user_DllExport const RequestDataType& data() const
    {
        return m_data;
    }

    /*!
     * @brief This function returns a reference to member data
     * @return Reference to member data
     */
    inline eProsima_user_DllExport RequestDataType& data()
    {
        return m_data;
    }
    
    /*!
     * @brief This function returns the maximum serialized size of an object
     * depending on the buffer alignment.
     * @param current_alignment Buffer alignment.
     * @return Maximum serialized size.
     */
    eProsima_user_DllExport static size_t getMaxCdrSerializedSize(size_t current_alignment = 0);

    /*!
     * @brief This function returns the serialized size of a data depending on the buffer alignment.
     * @param data Data which is calculated its serialized size.
     * @param current_alignment Buffer alignment.
     * @return Serialized size.
     */
    eProsima_user_DllExport static size_t getCdrSerializedSize(const RequestMetaData<RequestDataType>& data, size_t current_alignment = 0);


    /*!
     * @brief This function serializes an object using CDR serialization.
     * @param cdr CDR serialization object.
     */
    eProsima_user_DllExport void serialize(eprosima::fastcdr::Cdr &cdr) const;

    /*!
     * @brief This function deserializes an object using CDR serialization.
     * @param cdr CDR serialization object.
     */
    eProsima_user_DllExport void deserialize(eprosima::fastcdr::Cdr &cdr);



    /*!
     * @brief This function returns the maximum serialized size of the Key of an object
     * depending on the buffer alignment.
     * @param current_alignment Buffer alignment.
     * @return Maximum serialized size.
     */
    eProsima_user_DllExport static size_t getKeyMaxCdrSerializedSize(size_t current_alignment = 0);

    /*!
     * @brief This function tells you if the Key has been defined for this type
     */
    eProsima_user_DllExport static bool isKeyDefined();

    /*!
     * @brief This function serializes the key members of an object using CDR serialization.
     * @param cdr CDR serialization object.
     */
    eProsima_user_DllExport void serializeKey(eprosima::fastcdr::Cdr &cdr) const;
    
    eProsima_user_DllExport static std::string getName() { return RequestDataType::getName()+"MetaData"; } 
private:
    GUID_t m_guid;
    uint32_t m_operationId;
    RequestDataType m_data;
};

#include "RequestMetaData.hpp"

#endif // _RequestMetaData_H_
