HOW TO USE

1. revise swarm_task*.xml provided in the test/data/ directory
Set the value of tag <_P_Num> under each type of actors, which is the number of platforms required to run the test.
Set the value of tag <Plugin> under each type of actors, which is the name of the plugin you want to test. You may append multiple plugins for a perticular type of actor.

2. revise ground_station_test.launch provided in the g_station/launch directory
Set the value of ROS param /swarm_match_file_path and /swarm_task_file_path as the PATH of your own swarm_match*.xml and swarm_task*.xml.

3. run the test
$ roslaunch g_station ground_station_test.launch






Further Info About the Package

1. Intro
<!--                           --> 
<!-- USERS MAY SKIP THIS PART. --> 
<!--                           --> 
1.1 Procedure of g_station_node
(1) publish swarm task requirements defined in test/data/swarm_match*.xml and swarm task defined in test/data/swarm_task*.xml to all the platforms
(2) collect responses from platforms
(3) iff get enough responses, then assign swarm task to each platform
(4) set FormationType and FormationPos to each platform
1.2 About Actor Types in swarm_task*.xml
(1) GeneralActors
GeneralActors define the main actor each platform plays during the task, e.g Observer. The total num of GeneralActors should be equal to platforms.
(2) ExcluActors
ExcluActors are designed to manage the swarm structure, e.g Leader and Follower. The total num of ExcluActors should be equal to platforms.
(3) DynamicActors
DynamicActors are actors started dynamiclly during the task, e.g when Observer discovers the target it should switch into Transmitter. During swarm task initialization, g_station does consider DynamicActors.

2. Usage 
2.1 Ways to run the test
There are two ways to use g_station.
(1) roslaunch g_station ground_station_test.launch
In the launch file, SWARM_MATCH_FILE_PATH is provided in ROS param /swarm_match_file_path, and SWARM_TASK_FILE_PATH is provided in ROS param /swarm_task_file_path.
It's recommended to use the test xml files provided in the test/data/ directory. For example, if you want to run the test in 1 platform (local computer), it's recommended to use the swarm_match_1.xml and swarm_task_1.xml; if you want to run the test in 3 platform, it's recommended to use the swarm_match_3.xml and swarm_task_3.xml. 
NOTICE: You can change the plugins defined by tag name <Plugin> in the swarm_task*.xml in order to test your own plugins.
(2) rosrun g_station g_station_node YOUR_MATCH_FILE_PATH YOUR_TASK_FILE_PATH
YOUR_MATCH_FILE_PATH is the absolute PATH of swarm_match*.xml, and YOUR_TASK_FILE_PATH is the absolute PATH of swarm_task*.xml.
2.2 Ways to change the number of platforms in the test 
Find tag name <_P_Num> in the swarm_task*.xml file. This tag defines the number of platforms required in the test. Feel free to edit the value as you wish. In the same way, You can change the plugins defined by tag name <Plugin> in the swarm_task*.xml in order to test your own plugins.
   

