/* ==================================================================
*
* Copyright (c) 2016-2019, micROS Group, NIIDT, TAIIC, HPCL.
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions 
* are met: 
*
* 1. Redistributions of source code must retain the above copyright 
*     notice, this list of conditions and the following disclaimer. 
* 2. Redistributions in binary form must reproduce the above copyright 
*     notice, this list of conditions and the following disclaimer in the 
*     documentation and/or other materials provided with the 
*     distribution. 
* 3. All advertising materials mentioning features or use of this software 
*     must display the following acknowledgement: 
*     This product includes software developed by the micROS Group. and 
*     its contributors. 
* 4. Neither the name of the Group nor the names of its contributors may 
*     be used to endorse or promote products derived from this software 
*     without specific prior written permission. 
*
* THIS SOFTWARE IS PROVIDED BY MICROS,GROUP AND CONTRIBUTORS
* ''AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT
* NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
* FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL 
* THE MICROS,GROUP OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS 
* OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) 
* HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
* OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN 
* IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
* ===================================================================
*
* Author: Bin Di, Chunlian Fu, Junling Gao, Ruihao Li, Wei Yi.
*         (in alphabetical order)
*
*/

#include "g_station/g_station.h"
#include "std_msgs/String.h"
#include "actor_tag_store/ActorTagStore.h"
using namespace std;

// #define GSTATION_DEBUG

GStation::GStation(string aMatchFilePath, string aTaskFilePath, string anUtoFilePath, int aTimeDurationSingle, int aTimeDurationSwarm)
:_curCmdNo(0), _robotNum(0)
{
	this->_nh = ros::NodeHandle("~/");
	_matchFilePath = aMatchFilePath;
	_taskFilePath = aTaskFilePath;
	_utoFilePath = anUtoFilePath;
	_timeDurationSingle = aTimeDurationSingle;
	_timeDurationSwarm = aTimeDurationSwarm;
	_swarmMatchXmlStr = "";
	_swarmTaskXmlStr = "";
	// swarmTaskActorList.clear();					// NOTICE: this will clear the vector, however the data saved in the vector is not cleared
	std::vector<string>().swap(swarmTaskActorList);
	std::vector<struct SwarmTaskResponse>().swap(responseVec);
	std::vector<int64_t>().swap(confirmedPlatformID);
	// ROS_INFO("[Ground Station] Initialized!");

	// modify by gjl
	// _pPubReq = new RTPSPublisher<SwarmTask>("/swarm_task_requirements");
	// _pSubResp = new RTPSSubscriber<SwarmTask>("/swarm_task_response", &GStation::subResponseCallback, this);
	// _pPubAssign = new RTPSPublisher<SwarmTask>("/swarm_task_assignments");
	// _pSubConfirm = new RTPSSubscriber<SwarmTask>("/task_assignment_confirmation", &GStation::subConfirmationCallback, this);
	// _pPubCommand = new RTPSPublisher<CtrlMsg>("/swarm_commands");
	// _subRemoteCmds = _nh.subscribe("/gstation_remote_cmds", 10000, &GStation::subRemoteCmdsCallback0, this);
    _pPubReq = NULL;
    _pSubResp = NULL;
    _pPubAssign = NULL;
    _pSubConfirm = NULL;
    _pPubCommand = NULL;
    
	// if(!anUtoFilePath.empty()){
	// 	_utoPub = new RTPSPublisher<StringMsg>("/uto_task_xml");
	// 	_utoSyncPub = new RTPSPublisher<UTOEventTransition>("/uto_event_sync");
	 	_utoSyncSub = new RTPSSubscriber<UTOEventTransition>("/uto_event_transition", &GStation::subUTOEventCallback, this);
	// }
	
	// add by gjl
	_pPubTaskXml = new RTPSPublisher<StringMsg>("/swarm_task_xml");
	_pSubTaskXmlResponse = new RTPSSubscriber<StringMsg>("/swarm_task_xml_resp", &GStation::subSwarmTaskXmlResponseCallback, this);
	_pPubUTOEvent = new RTPSPublisher<UTOEventWithGStation>("/uto_event_msg");
	_pPubCmd = new RTPSPublisher<MasterCmd>("/gs_master_cmd");
	_pSubCmd = new RTPSSubscriber<MasterCmd>("/gs_master_cmd_resp", &GStation::subPingMsgCallback, this);
	//_pSubWarnMsg = new RTPSSubscriber<StringMsg>("/swarm_warn_msg", &GStation::subWarnMsgCallback, this);
    //_pPubWarnMsg = this->_nh.advertise<std_msgs::String>("/swarm_warn_msg", 10000);

	// set flags
	_taskAssignFlag = false;
	_confirmAssignFlag = false;
	_subRespOvertimeFlag = false;
	_subConfOvertimeFlag = false;

	// if (!_pPubReq->init()) {
	// 	cout << "[Ground Station] Failed to initialize RTPSPublisher for /swarm_task_requirements!" << endl;
	// 	return;
	// }
	// if (!_pSubResp->init()) {
	// 	cout << "[Ground Station] Failed to initialized RTPSSubscriber for /swarm_task_response!" << endl;
	// 	return;
	// }
	// if (!_pPubAssign->init()) {
	// 	cout << "[Ground Station] Failed to initialized RTPSPublisher for /swarm_task_assignments!" << endl;
	// 	return;
	// }
	// if (!_pSubConfirm->init()) {
	// 	cout << "[Ground Station] Failed to initialized RTPSSubscriber for /task_assignment_confirmation!" << endl;
	// 	return;
	// }
	// if (!_pPubCommand->init()) {
	// 	cout << "[Ground Station] Failed to initialized RTPSPublisher for /swarm_commands!" << endl;
	// 	return;
	// }

	cout << "[Ground Station] Initialized!" << endl;
}

GStation::~GStation()
{
	if (_pPubReq != NULL) {
		delete _pPubReq;
		_pPubReq = NULL;
	}
	if (_pSubResp != NULL) {
		delete _pSubResp;
		_pSubResp = NULL;
	}
	if (_pPubAssign != NULL) {
		delete _pPubAssign;
		_pPubAssign = NULL;
	}
	if (_pSubConfirm != NULL) {
		delete _pSubConfirm;
		_pSubConfirm = NULL;
	}
	if (_pPubCommand != NULL) {
		delete _pPubCommand;
		_pPubCommand = NULL;
	}
	// add by gjl
	if (_pPubTaskXml != NULL) {
		delete _pPubTaskXml;
		_pPubTaskXml = NULL;
	}
	if (_pSubTaskXmlResponse != NULL) {
		delete _pSubTaskXmlResponse;
		_pSubTaskXmlResponse = NULL;
	}
	if (_pPubUTOEvent != NULL) {
		delete _pPubUTOEvent;
		_pPubUTOEvent = NULL;
	}
	if (_pPubCmd != NULL) {
		delete _pPubCmd;
		_pPubCmd = NULL;
	}
	if (_pSubCmd != NULL){
		delete _pSubCmd;
		_pSubCmd = NULL;
	}
	if (_pSubWarnMsg != NULL){
		delete _pSubWarnMsg;
		_pSubWarnMsg = NULL;
	}
}

bool GStation::init()
{
	TiXmlDocument doc1, doc2, utoDoc;
	string xmlStr1, xmlStr2;
	if ( !doc1.LoadFile(_matchFilePath) )
	{
		// ROS_ERROR("[Ground Station] Error while open swarm match XML file!");
		cout << "[Ground Station] Error while open swarm match XML file!" << endl;
		return false;
	}
	else
	{
		TiXmlPrinter printer1;
		doc1.Accept(&printer1);
		xmlStr1 = printer1.CStr();
	}
	if ( !doc2.LoadFile(_taskFilePath) )
	{
		// ROS_ERROR("[Ground Station] Error while open swarm task XML file!");
		cout << "[Ground Station] Error while open swarm task XML file!" << endl;
		return false;
	}
	else
	{
		TiXmlPrinter printer2;
		doc2.Accept(&printer2);
		xmlStr2 = printer2.CStr();
	}
	if ( !initSwarmArchitecture(xmlStr2) ) {
		// ROS_FATAL("[Ground Station] Failed to initialize Swarm Architecture!");
		cout << "[Ground Station] Failed to initialize Swarm Architecture!" << endl;
		return false;
	}

	// Uto
	if (_utoFilePath.empty() || !utoDoc.LoadFile(_utoFilePath)) {
		cout << "[Ground Station] Error while open uto XML file! empty:"<<_utoFilePath.empty()<<", _utoFilePath:"<<_utoFilePath<< endl;
		return false;
	} else {
		TiXmlPrinter printer;
		utoDoc.Accept(&printer);
		_utoTaskXmlStr = printer.CStr();
	}

	_swarmMatchXmlStr = xmlStr1;
	_swarmTaskXmlStr = xmlStr2;

#ifdef GSTATION_DEBUG
	cout << "Debug: size of SwarmMatch: " << xmlStr1.size() << ", size of SwarmTask: " << xmlStr2.size() << "." << endl;
	if(!_utoTaskXmlStr.empty()) ROS_INFO("Debug: size of UtoTask: ", _utoTaskXmlStr.size());
#endif

	return true;
}

void GStation::pubRequirements()
{
	// actor_msgs::swarm_task msg;
	SwarmTask msg;

	// ros::Publisher pub = _nh.advertise<actor_msgs::swarm_task>("/swarm_task_requirements", 10000);
	// RTPSPublisher<SwarmTask>  pub("/swarm_task_requirements");

	msg.swarmMatchXmlStr(_swarmMatchXmlStr);
	msg.swarmTaskXmlStr(_swarmTaskXmlStr);

	// UTO task xml
	StringMsg strMsg;
	if(!_utoTaskXmlStr.empty()) {
		ROS_WARN("===========================[Ground Station] _utoTaskXmlStr size:%lu, sizeof(string):%lu, sizeof(_utoxml):%lu ==============================", _utoTaskXmlStr.size(), sizeof(string), sizeof(_utoTaskXmlStr));
		strMsg.data(_utoTaskXmlStr);
	}
	
	// ROS_INFO("[Ground Station] About to publish /swarm_task_requirements.");
	// cout << "[Ground Station] About to publish /swarm_task_requirements." << endl;

	for (int i = 0; i < _timeDurationSingle; i++)
	{
		cout << "[Ground Station] Publishing /swarm_task_requirements." << endl;
		if(!_utoTaskXmlStr.empty()) {
			cout << "[Ground Station] Publishing uto_task_xml." << endl;
			_utoPub->publish(strMsg);
		}
		sleep(1);
		_pPubReq->publish(msg);
		sleep(1);
	}

	// ROS_INFO("[Ground Station] Finish Publishing /swarm_task_requirements.");
	// cout << "[Ground Station] Finish Publishing /swarm_task_requirements." << endl;
}

void GStation::pubRequirements0()
{
	actor_msgs::swarm_task msg;

	ros::Publisher pub = _nh.advertise<actor_msgs::swarm_task>("/swarm_task_requirements", 10000);

	msg.swarmMatchXmlStr = _swarmMatchXmlStr;
	msg.swarmTaskXmlStr = _swarmTaskXmlStr;
	
	// ROS_INFO("[Ground Station] About to publish /swarm_task_requirements.");
	// cout << "[Ground Station] About to publish /swarm_task_requirements." << endl;
	
	for (int i=0; i<_timeDurationSingle; i++) {
		// ROS_INFO("[Ground Station] Publishing /swarm_task_requirements.");
		cout << "[Ground Station] Publishing /swarm_task_requirements." << endl;
		pub.publish(msg);
		sleep(1);
	}
	
	// ROS_INFO("[Ground Station] Publish /swarm_task_requirements.");
	// cout << "[Ground Station] Publish /swarm_task_requirements." << endl;
}

bool GStation::initSwarmArchitecture(string aXmlStr)
// everytime the _swarmTaskXmlStr is updated the struct of swarmArchitectureInfo must be updated!!!
{
	struct SwarmArchitecture tempSwarmArchitectureInfo;
	vector<string> tempSwarmTaskActorList;
	TiXmlDocument doc;
	// receive XML file as string
	if (aXmlStr.size()==0) {
		// ROS_FATAL("[Ground Station] Input XML string is empty!");
		cout << "[Ground Station] Input XML string is empty!" << endl;
		return false;
	}
	doc.Parse(aXmlStr.c_str());
	if (doc.Error()!=0) {
		// ROS_FATAL("[Ground Station] Input XML file is not in correct format!");
		cout << "[Ground Station] Input XML file is not in correct format!" << endl;
		return false;
	}
	TiXmlElement *pEle;
	TiXmlHandle hDoc(&doc);
	pEle = hDoc.FirstChildElement().Element();
	for (pEle = pEle->FirstChildElement(); pEle != NULL; pEle = pEle->NextSiblingElement()) {
		string tagName = pEle->Value();
		if (tagName == "_P_SwarmName") {
			_swarmName = pEle->GetText();
		}
		if (tagName == "_ActorsConfig") {
			for (TiXmlElement* pActorType = pEle->FirstChildElement(); pActorType != NULL; pActorType = pActorType->NextSiblingElement()) {
				string actorTypeTag = pActorType->Value();
				if (actorTypeTag == "_GeneralActors") {
					for (TiXmlElement* pActors = pActorType->FirstChildElement(); pActors != NULL; pActors = pActors->NextSiblingElement()) {
						string actorName;
						int actorNum = 0;
						bool foundName = false;
						bool foundNum = false;
						for (TiXmlElement* pActorTag = pActors->FirstChildElement(); pActorTag != NULL; pActorTag = pActorTag->NextSiblingElement() ) {
							string actorTagName = pActorTag->Value();
							if (actorTagName == "_P_Name") {
								actorName = pActorTag->GetText();
								foundName = true;
							}
							if (actorTagName == "_P_Num") {
								string strNum = pActorTag->GetText();
								actorNum = atoi( strNum.c_str() );
								if (actorNum >= 0) {
									foundNum = true;;
								}								
							}							
						}
						if (!foundName) {
							cout << "[Ground Station] Warning: Actor Name missing in _GeneralActors!" << endl;
						}
						if (!foundNum) {
							cout << "[Ground Station] Warning: Actor Num missing in _GeneralActors!" << endl;
						}
						tempSwarmTaskActorList.push_back(actorName);
						for (int i = 1; i <= actorNum; i++) {
							stringstream ss;
							ss << i;
							string str, temp;
							ss >> str;
							temp = actorName + str;
							tempSwarmArchitectureInfo.actors.generalActors.push_back(temp);
						}
					}
				}
				else if (actorTypeTag == "_ExcluActors") {
					for (TiXmlElement* pActors = pActorType->FirstChildElement(); pActors != NULL; pActors = pActors->NextSiblingElement()) {
						string actorName;
						int actorNum = 0;
						bool foundName = false;
						bool foundNum = false;
						for (TiXmlElement* pActorTag = pActors->FirstChildElement(); pActorTag != NULL; pActorTag = pActorTag->NextSiblingElement() ) {
							string actorTagName = pActorTag->Value();
							if (actorTagName == "_P_Name") {
								actorName = pActorTag->GetText();
								foundName = true;
							}
							if (actorTagName == "_P_Num") {
								string strNum = pActorTag->GetText();
								actorNum = atoi( strNum.c_str() );
								if (actorNum >= 0) {
									foundNum = true;;
								}
							}							
						}
						if (!foundName) {
							cout << "[Ground Station] Warning: Actor Name missing in _ExcluActors!" << endl;
						}
						if (!foundNum) {
							cout << "[Ground Station] Warning: Actor Num missing in _ExcluActors!" << endl;
						}
						tempSwarmTaskActorList.push_back(actorName);
						for (int i = 1; i <= actorNum; i++) {
							stringstream ss;
							ss << i;
							string str, temp;
							ss >> str;
							temp = actorName + str;
							tempSwarmArchitectureInfo.actors.exclusiveActors.push_back(temp);
						}
					}
				}
				else if (actorTypeTag == "_DynamicActors") {
					for (TiXmlElement* pActors = pActorType->FirstChildElement(); pActors != NULL; pActors = pActors->NextSiblingElement()) {
						string actorName;
						int actorNum = 0;
						bool foundName = false;
						bool foundNum = false;
						for (TiXmlElement* pActorTag = pActors->FirstChildElement(); pActorTag != NULL; pActorTag = pActorTag->NextSiblingElement() ) {
							string actorTagName = pActorTag->Value();
							if (actorTagName == "_P_Name") {
								actorName = pActorTag->GetText();
								foundName = true;
							}
							if (actorTagName == "_P_Num") {
								string strNum = pActorTag->GetText();
								actorNum = atoi( strNum.c_str() );
								if (actorNum >= 0) {
									foundNum = true;;
								}
							}							
						}
						if (!foundName) {
							cout << "[Ground Station] Warning: Actor Name missing in _DynamicActors!" << endl;
						}
						if (!foundNum) {
							cout << "[Ground Station] Warning: Actor Num missing in _DynamicActors!" << endl;
						}
						tempSwarmTaskActorList.push_back(actorName);
						for (int i = 1; i <= actorNum; i++) {
							stringstream ss;
							ss << i;
							string str, temp;
							ss >> str;
							temp = actorName + str;
							tempSwarmArchitectureInfo.actors.dynamicActors.push_back(temp);
						}
					}
				}
				else {
					// ROS_ERROR("[Ground Station] Unknown Actor Type: %s!", actorTypeTag.c_str());
					cout << "[Ground Station] Unknown Actor Type: " << actorTypeTag << endl;
					return false;
				}
			}
		}
	}
	swarmArchitectureInfo = tempSwarmArchitectureInfo;
	swarmTaskActorList = tempSwarmTaskActorList;
	return true;
}

void GStation::subUTOEventCallback(UTOEventTransition &msg) {
	cout<<"[Ground Station] Receive uto event transition from ["<<msg.robotId()<<"], actorName:"<<msg.currentActor()<<", eventName:"<<msg.eventName()<<", timestamp:"<<msg.timestamp()<<endl;
	// msg.syncStatus(1);
	// _utoSyncPub->publish(msg);
	// sleep(2);
	// msg.syncStatus(2);
	// _utoSyncPub->publish(msg);
}

void GStation::subscribeResponses()
{
	// ros::Subscriber sub = _nh.subscribe("/swarm_task_response", 10000, &GStation::subResponseCallback, this);
	// RTPSSubscriber<SwarmTask> sub("/swarm_task_response", &GStation::subResponseCallback, this);
	// ROS_INFO("[Ground Station] Start subscribing /swarm_task_response.");
	cout << "[Ground Station] Wait for subscribing /swarm_task_response." << endl;

	for (int i = 0; i < _timeDurationSwarm; i++)
	{
		sleep(1);
		if (_taskAssignFlag) {
			cout << "[Ground Station] OK to assign tasks, quit waiting." << endl;
			break;
		}
	}

	if (_taskAssignFlag) {
		pubAssignments();
	}
	else {
		cout << "[Ground Station] Overtime while subscribing responses." << endl;
		_subRespOvertimeFlag = true;
	}
}

void GStation::subscribeResponses0()
{
	ros::Subscriber sub = _nh.subscribe("/swarm_task_response", 10000, &GStation::subResponseCallback0, this);
	// ROS_INFO("[Ground Station] Start subscribing /swarm_task_response.");
	cout << "[Ground Station] Start subscribing /swarm_task_response." << endl;

	for (int i=0; i<_timeDurationSwarm; i++) {
		ros::spinOnce();
		sleep(1);
		if (_taskAssignFlag) {
			cout << "[Ground Station] OK to assign tasks, quit waiting." << endl;
			break;
		}
	}

	if (_taskAssignFlag) {
		pubAssignments0();
	}
	else {
		cout << "[Ground Station] Overtime while subscribing responses." << endl;
		_subRespOvertimeFlag = true;
	}
}

void GStation::subResponseCallback(SwarmTask& msg)
{
	if (_subRespOvertimeFlag) {	// Overtime?
		cout << "[Ground Station] Subscribe swarm matching responses overtime, please modify overtime params." << endl;
		return;
	} 
	else {
		if (_taskAssignFlag) { 	// Task assigned?
			cout << "[Ground Station] Assigning swarm tasks, not open to new responses." << endl;
			return;
		}
		else {	// subscribe new responses and try to assign tasks
			for (int i = 0; i < responseVec.size(); i++) {
				if (msg.platformID() == responseVec[i].platformID) {	// received before?
					cout << "[Ground Station] Response received before!" << endl;
					return;
				}
			}
			// // TODO: check whether the response is legal or not, commented for debug, remember to uncomment after debug
			// int actorNum = swarmArchitectureInfo.actors.generalActors.size() + swarmArchitectureInfo.actors.exclusiveActors.size() + swarmArchitectureInfo.actors.dynamicActors.size();
			// std::vector<string> tempActorName = msg.actorName();
			// if (tempActorName.size() != actorNum) {
			// 	// ROS_WARN("[Ground Station] Actor number may be wrong in response message from platform %ld!", msg.platformID());
			// 	cout << "[Ground Station] Actor number may be wrong in response message from platform " << msg.platformID() << endl;
			// }

			// // TODO: assign swarm tasks based on BID from platforms
			struct SwarmTaskResponse resp;
			resp.platformID = msg.platformID();
			// resp.actorNameVec = msg.actorName();
			// resp.bidActorVec = msg.bidActor();

			responseVec.push_back(resp);

			// 20181129 group discussion confirmed that G_Station should skip assigning dynamicActos 
			if (
				// (responseVec.size() >= swarmArchitectureInfo.actors.generalActors.size()) &&
				// (responseVec.size() >= swarmArchitectureInfo.actors.exclusiveActors.size()) &&
				// (responseVec.size() >= swarmArchitectureInfo.actors.dynamicActors.size()) )
				(responseVec.size() >= swarmArchitectureInfo.actors.generalActors.size()) &&
				(responseVec.size() >= swarmArchitectureInfo.actors.exclusiveActors.size()) )
			{
#ifdef GSTATION_DEBUG
				cout << "[Debug] General Actors Num: " << swarmArchitectureInfo.actors.generalActors.size()
					<< ", Exclusive Actors Num: " << swarmArchitectureInfo.actors.exclusiveActors.size() 
					<< ", Dynamic Actors Num: " << swarmArchitectureInfo.actors.dynamicActors.size()
					<< endl;
#endif
				cout << "[Ground Station] Start task assignment!" << endl;
				_taskAssignFlag = true;
			}
			else {
				cout << "[Ground Station] Not enough platform. Continue..." << endl;
			}
		}
	}
}

void GStation::subResponseCallback0(const actor_msgs::swarm_task& msg)
{
	if (_subRespOvertimeFlag) {	// Overtime?
		cout << "[Ground Station] Subscribe swarm matching responses overtime, please modify overtime params." << endl;
		return;
	} 
	else {
		if (_taskAssignFlag) { 	// Task assigned?
			cout << "[Ground Station] Assigning swarm tasks, not open to new responses." << endl;
			return;
		}
		else {	// subscribe new responses and try to assign tasks
			for (int i = 0; i < responseVec.size(); i++) {
				if (msg.platformID == responseVec[i].platformID) {
					cout << "[Ground Station] Response received before!" << endl;
					return;
				}
			}
			// int actorNum = swarmArchitectureInfo.actors.generalActors.size() + swarmArchitectureInfo.actors.exclusiveActors.size() + swarmArchitectureInfo.actors.dynamicActors.size();
			// std::vector<string> tempActorName = msg.actorName;
			// if (tempActorName.size() != actorNum) {
			// 	// ROS_WARN("[Ground Station] Actor number may be wrong in response message from platform %ld!", msg.platformID());
			// 	cout << "[Ground Station] Actor number may be wrong in response message from platform " << msg.platformID << endl;
			// }
			struct SwarmTaskResponse resp;
			resp.platformID = msg.platformID;

			// resp.actorNameVec = msg.actorName;
			// resp.bidActorVec = msg.bidActor;

			responseVec.push_back(resp);

			// 20181129 group discussion confirmed that G_Station should skip assigning dynamicActos 
			if (
				// (responseVec.size() >= swarmArchitectureInfo.actors.generalActors.size()) &&
				// (responseVec.size() >= swarmArchitectureInfo.actors.exclusiveActors.size()) &&
				// (responseVec.size() >= swarmArchitectureInfo.actors.dynamicActors.size()) )
				(responseVec.size() >= swarmArchitectureInfo.actors.generalActors.size()) &&
				(responseVec.size() >= swarmArchitectureInfo.actors.exclusiveActors.size()) )
			{
				cout << "[Ground Station] Start task assignment!" << endl;
				_taskAssignFlag = true;
			}
			else {
				cout << "[Ground Station] Not enough platform! Continue..." << endl;
			}
		}
	}
}

void GStation::pubAssignments()
// TODO: This algorithm is not completed yet!!!
// Suppose all platforms are available for all the tasks
{
#ifdef GSTATION_DEBUG
	int responseNum = responseVec.size();
	for (int i=0; i<responseNum; i++) {
		// ROS_INFO("[Ground Station] responseNum:%d, responser ID: %ld", responseNum, responseVec[i].platformID);
		cout << "[Ground Station] Responser count: " << responseNum << ", responser ID: " << responseVec[i].platformID << endl;
	}
#endif
	// Swarm Task Message
	// actor_msgs::swarm_task msg;
	SwarmTask msg;
	msg.swarmMatchXmlStr(_swarmMatchXmlStr);
	msg.swarmTaskXmlStr(_swarmTaskXmlStr);
	// ros::Publisher pub = _nh.advertise<actor_msgs::swarm_task>("/swarm_task_assignments", 10000);
	// RTPSPublisher<SwarmTask>  pub("/swarm_task_assignments");
	
	int actorNum = swarmArchitectureInfo.actors.generalActors.size();
	std::vector<string> tempActorName;			// idl messages based on sequence<string>
	std::vector<int64_t> tempTaskAssignment;	// idl messages based on sequence<int64_t>
	string tempActorNameListStr, tempTaskAssignmentListStr;	// 20181026: Swarm Task Message, change sequence<string> into string<_size>
	tempActorNameListStr.clear();
	tempTaskAssignmentListStr.clear();
	for (int i=0; i<actorNum; i++) {
		tempActorName.push_back(swarmArchitectureInfo.actors.generalActors[i]);
		tempTaskAssignment.push_back(responseVec[i].platformID);
		// 20181026: change sequence<string> into string<_size>
		tempActorNameListStr += swarmArchitectureInfo.actors.generalActors[i];
		tempActorNameListStr += ";";
		stringstream ss;
		ss.clear();
		string tempStr1;
		ss << responseVec[i].platformID;
		ss >> tempStr1;
		tempTaskAssignmentListStr += tempStr1;
		tempTaskAssignmentListStr += ";";
	}
	actorNum = swarmArchitectureInfo.actors.exclusiveActors.size();
	for (int i=0; i<actorNum; i++) {
		tempActorName.push_back(swarmArchitectureInfo.actors.exclusiveActors[i]);
		tempTaskAssignment.push_back(responseVec[i].platformID);
		// 20181026: change sequence<string> into string<_size>
		// task assignment
		tempActorNameListStr += swarmArchitectureInfo.actors.exclusiveActors[i];
		tempActorNameListStr += ";";
		stringstream ss;
		ss.clear();
		string tempStr1;
		ss << responseVec[i].platformID;
		ss >> tempStr1;
		tempTaskAssignmentListStr += tempStr1;
		tempTaskAssignmentListStr += ";";
	}
	// 20181129 group discussion confirmed that G_Station should skip assigning dynamicActos
	// actorNum = swarmArchitectureInfo.actors.dynamicActors.size();
	// for (int i=0; i<actorNum; i++) {
	// 	tempActorName.push_back(swarmArchitectureInfo.actors.dynamicActors[i]);
	// 	tempTaskAssignment.push_back(responseVec[i].platformID);
	// 	// 20181026: change sequence<string> into string<_size>
	// 	tempActorNameListStr += swarmArchitectureInfo.actors.dynamicActors[i];
	// 	tempActorNameListStr += ";";
	// 	stringstream ss;
	// 	ss.clear();
	// 	string tempStr1;
	// 	ss << responseVec[i].platformID;
	// 	ss >> tempStr1;
	// 	tempTaskAssignmentListStr += tempStr1;
	// 	tempTaskAssignmentListStr += ";";
	// }
	msg.actorNameList(tempActorNameListStr);
	msg.taskAssignmentList(tempTaskAssignmentListStr);

	for (int i=0; i<tempTaskAssignment.size(); i++) {
		// ROS_INFO("[Ground Station] debug assignment: %s: platform %ld", tempActorName[i].c_str(), tempTaskAssignment[i]);
		cout << "[Ground Station] Print assignments: " << tempActorName[i] << ", assigned to platformID " << tempTaskAssignment[i] << endl;
	}
	
	// clear the vector of confirmedPlatformID, and prepare for the message of task assignment confirmation from platforms
	std::vector<int64_t>().swap(confirmedPlatformID);

	// ROS_INFO("[Ground Station] About to publish /swarm_task_assignments.");
	cout << "[Ground Station] About to publish /swarm_task_assignments." << endl;

	for (int i = 0; i < _timeDurationSingle; i++)
	{
		GET_CUR_SEC;
		msg.timeStamp(curSecStr);
		_pPubAssign->publish(msg);
		cout << "[Ground Station] Publishing /swarm_task_assignments!" << endl;
		sleep(1);
	}

	cout << "[Ground Station] Finish publishing /swarm_task_assignments." << endl;
}

void GStation::pubAssignments0()
// TODO: This algorithm is not completed yet!!!
// Suppose all platforms are available for all the tasks
{
#ifdef GSTATION_DEBUG
	int responseNum = responseVec.size();
	for (int i=0; i<responseNum; i++) {
		// ROS_INFO("[Ground Station] responseNum:%d, responser ID: %ld", responseNum, responseVec[i].platformID);
		cout << "[Ground Station] responseNum: " << responseNum << ", responser ID: " << responseVec[i].platformID << endl;
	}
#endif

	actor_msgs::swarm_task msg;
	msg.swarmMatchXmlStr = _swarmMatchXmlStr;
	msg.swarmTaskXmlStr = _swarmTaskXmlStr;
	ros::Publisher pub = _nh.advertise<actor_msgs::swarm_task>("/swarm_task_assignments", 10000);
	int actorNum = swarmArchitectureInfo.actors.generalActors.size();
	std::vector<string> tempActorName;
	std::vector<int64_t> tempTaskAssignment;
	for (int i=0; i<actorNum; i++) {
		tempActorName.push_back(swarmArchitectureInfo.actors.generalActors[i]);
		tempTaskAssignment.push_back(responseVec[i].platformID);
	}
	actorNum = swarmArchitectureInfo.actors.exclusiveActors.size();
	for (int i=0; i<actorNum; i++) {
		tempActorName.push_back(swarmArchitectureInfo.actors.exclusiveActors[i]);
		tempTaskAssignment.push_back(responseVec[i].platformID);
	}
	// 20181129 group discussion confirmed that G_Station should skip assigning dynamicActos 
	// actorNum = swarmArchitectureInfo.actors.dynamicActors.size();
	// for (int i=0; i<actorNum; i++) {
	// 	tempActorName.push_back(swarmArchitectureInfo.actors.dynamicActors[i]);
	// 	tempTaskAssignment.push_back(responseVec[i].platformID);
	// }
	msg.actorName = tempActorName;
	msg.taskAssignment = tempTaskAssignment;

	for (int i=0; i<tempTaskAssignment.size(); i++) {
		// ROS_INFO("[Ground Station] debug assignment: %s: platform %ld", tempActorName[i].c_str(), tempTaskAssignment[i]);
		cout << "[Ground Station] Print assignments: " << tempActorName[i] << ", assigned to platformID " << tempTaskAssignment[i] << endl;
	}

	// ROS_INFO("[Ground Station] About to publish /swarm_task_assignments.");
	cout << "[Ground Station] About to publish /swarm_task_assignments." << endl;
	for (int i=0; i<_timeDurationSingle; i++) {
		pub.publish(msg);
		sleep(1);
	}

	cout << "[Ground Station] Finish publishing /swarm_task_assignments." << endl;
}

void GStation::pubFormationCtrlMsg() 
{
	string keyWord1 = "Leader";
	string keyWord2 = "Follower";

	// Swarm Command Message
	CtrlMsg msg1, msg2;

	string tempPlatformIdListStr1, tempDataStr1;	// debug only: Swarm Command Message for keyWord1, change sequence<string> into string<_size>
	string tempPlatformIdListStr2, tempDataStr2;	// debug only: Swarm Command Message for keyWord2, change sequence<string> into string<_size>
	tempPlatformIdListStr1.clear();
	tempDataStr1.clear();
	tempPlatformIdListStr2.clear();
	tempDataStr2.clear();

	int actorNum = swarmArchitectureInfo.actors.exclusiveActors.size();
	int temp1FormationID = 0;
	int temp2FormationID = 0;
	for (int i=0; i<actorNum; i++) {
		// notice: change sequence<string> into string<_size>
		// swarm formation control message
		string tempActorName = swarmArchitectureInfo.actors.exclusiveActors[i];
		stringstream ss;
		ss.clear();
		string tempStr1;
		ss << responseVec[i].platformID;
		ss >> tempStr1;
		if (tempActorName.find(keyWord1) != std::string::npos) {	
			tempPlatformIdListStr1 += tempStr1;
			tempPlatformIdListStr1 += ";";
			temp1FormationID++;
			tempDataStr1 += "TYPE_UNKNOWN,0;";		// formation type
			if (temp1FormationID > 1) {
				cout << "[Ground Station] Warning: More than 1 " << keyWord1 << "!" << endl;
			}
		}
		if (tempActorName.find(keyWord2) != std::string::npos) {	
			tempPlatformIdListStr2 += tempStr1;
			tempPlatformIdListStr2 += ";";
			temp2FormationID++;
			string tempStr2;
			ss.clear();
			ss << temp2FormationID;
			ss >> tempStr2;
			tempDataStr2 += "TYPE_UNKNOWN,";		// formation type
			tempDataStr2 += tempStr2;				// formation position
			tempDataStr2 += ";";
		}
	}
	
	msg1.swarmName(_swarmName);
	msg1.actorName(keyWord1);
	msg1.platformIdList(tempPlatformIdListStr1);
	msg1.data(tempDataStr1);

	msg2.swarmName(_swarmName);
	msg2.actorName(keyWord2);
	msg2.platformIdList(tempPlatformIdListStr2);
	msg2.data(tempDataStr2);

	cout << "[Ground Station] Publish swarm formation control message:" << endl;
	cout << "swarmName is " << _swarmName << endl;
	cout << "actorName is " << keyWord1 << endl;
	cout << "platformIdList is " << tempPlatformIdListStr1 << endl;
	cout << "data is " << tempDataStr1 << endl;

	cout << "swarmName is " << _swarmName << endl;
	cout << "actorName is " << keyWord2 << endl;
	cout << "platformIdList is " << tempPlatformIdListStr2 << endl;
	cout << "data is " << tempDataStr2 << endl;
	
	cout << "[Ground Station] About to publish /swarm_commands." << endl;

	for (int i = 0; i < _timeDurationSingle; i++)
	{
		GET_CUR_TIME;
		string curTimeStr(szTime);
		msg1.timeStamp(curTimeStr);
		_pPubCommand->publish(msg1);
		cout << "[Ground Station] Publishing /swarm_commands for " << keyWord1 << endl;
		sleep(1);
	}
	sleep(1);
	for (int i = 0; i < _timeDurationSingle; i++)
	{
		GET_CUR_TIME;
		string curTimeStr(szTime);
		msg2.timeStamp(curTimeStr);
		_pPubCommand->publish(msg2);
		cout << "[Ground Station] Publishing /swarm_commands for " << keyWord2 << endl;
		sleep(1);
	}

	cout << "[Ground Station] Finish publishing /swarm_commands." << endl;
}

void GStation::subscribeConfirmation()
{
	// ros::Subscriber sub = _nh.subscribe("/task_assignment_confirmation", 10000, &GStation::subConfirmationCallback, this);
	// RTPSSubscriber<SwarmTask> sub("/task_assignment_confirmation", &GStation::subConfirmationCallback, this);
	// ROS_INFO("[Ground Station] Start subscribing /task_assignment_confirmation.");
	cout << "[Ground Station] Wait for subscribing /task_assignment_confirmation." << endl;

	for (int i = 0; i < _timeDurationSwarm; i++)
	{
		sleep(1);
		if (_confirmAssignFlag) {
			cout << "[Ground Station] Task assignment confirmed, quit waiting." << endl;
			break;
		}
	}

	if (_confirmAssignFlag) {
		return;
	}
	else {
		// cout << "[Ground Station] Overtime while subscribing confirmations." << endl;
		_subConfOvertimeFlag = true;
	}
}

void GStation::subscribeConfirmation0()
{
	ros::Subscriber sub = _nh.subscribe("/task_assignment_confirmation", 10000, &GStation::subConfirmationCallback0, this);
	cout << "[Ground Station] Start subscribing /task_assignment_confirmation." << endl;

	for (int i=0; i<_timeDurationSwarm; i++) {
		ros::spinOnce();
		sleep(1);
		if (_confirmAssignFlag) {
			cout << "[Ground Station] Task assignment confirmed, quit waiting." << endl;
			break;
		}
	}

	if (_confirmAssignFlag) {
		return;
	}
	else {
		// cout << "[Ground Station] Overtime while subscribing confirmations." << endl;
		_subConfOvertimeFlag = true;
	}
}

void GStation::subConfirmationCallback(SwarmTask& msg)
{
	if (_subConfOvertimeFlag) {	// Overtime?
		cout << "[Ground Station] Subscribe task assignment confirmation overtime, please modify overtime params." << endl;
		return;
	} 
	else {
		if (_confirmAssignFlag) { // Task assignment confirmed?
			cout << "[Ground Station] Task assignment already confirmed, no need to keep subscribing." << endl;
			return;
		}
		else {	// subscribing new confirmations
			for (int i=0; i<confirmedPlatformID.size(); i++) {
				if (msg.platformID() == confirmedPlatformID[i]) {
					cout << "[Ground Station] Confirmation received before!" << endl;
					return;
				}
			}
			if (msg.confirmAssignment() == 1) {
				confirmedPlatformID.push_back(msg.platformID());
			}
			_confirmAssignFlag = confirmSwarmTaskAssignment();
		}
	}
}

void GStation::subConfirmationCallback0(const actor_msgs::swarm_task& msg)
{
	if (_subConfOvertimeFlag) {	// Overtime?
		cout << "[Ground Station] Subscribe task assignment confirmation overtime, please modify overtime params." << endl;
		return;
	} 
	else {
		if (_confirmAssignFlag) { // Task assignment confirmed?
			cout << "[Ground Station] Task assignment already confirmed, no need to keep subscribing." << endl;
			return;
		}
		else {	// subscribing new confirmations
			for (int i=0; i<confirmedPlatformID.size(); i++) {
				if (msg.platformID == confirmedPlatformID[i]) {
					cout << "[Ground Station] Confirmation received before!" << endl;
					return;
				}
			}
			if (msg.confirmAssignment == 1) {
				confirmedPlatformID.push_back(msg.platformID);
			}
			_confirmAssignFlag = confirmSwarmTaskAssignment();
		}
	}
}

void GStation::subRemoteCmdsCallback0(const actor_msgs::gstation_remote_cmds& msg)
{
	cout << "[Ground Station] GStation Remote Cmds received!" << endl;
	CtrlMsg msg1;
	msg1.timeStamp(msg.timeStamp);
	msg1.swarmName(msg.swarmName);
	msg1.actorName(msg.actorName);
	msg1.platformIdList(msg.platformIdList);
	msg1.data(msg.data);

	cout << "[Ground Station] About to publish /swarm_commands." << endl;

	for (int i = 0; i < _timeDurationSingle; i++)
	{
		GET_CUR_TIME;
		string curTimeStr(szTime);
		msg1.timeStamp(curTimeStr);
		_pPubCommand->publish(msg1);
		cout << "[Ground Station] Publishing /swarm_commands!" << endl;
		sleep(1);
	}

	cout << "[Ground Station] Finish publishing /swarm_commands." << endl;
}

bool GStation::confirmSwarmTaskAssignment()
// NOTICE: This only works when the size of responseVec is equal to the minimum number of platforms which the Swarm Task requires.
{
	if (responseVec.size() == 0) {
		cout << "[Ground Station] Error: Response vector is empty" << endl;
		return false;
	}
	if (confirmedPlatformID.size() == 0) {
		cout << "[Ground Station] Error: ConfirmedPlatformID vector is empty" << endl;
		return false;
	}

	for (int i = 0; i < responseVec.size(); i++) {
		bool matchFlag = false;
		for (int j = 0; j < confirmedPlatformID.size(); j++) {
			if ( responseVec[i].platformID == confirmedPlatformID[j] ) {
				matchFlag = true;
				break;
			}
		}
		if (!matchFlag) {
			return false;
		}
	}
	return true;
}

void GStation::start()
{
	if (init()) {
		cout << "[Ground Station] About to publish /swarm_task_requirements." << endl;
		int cnt = 0;
		while (!_taskAssignFlag) {
			pubRequirements();
			++cnt;
			if (cnt > _timeDurationSwarm) {
				cout << "[Ground Station] Timeout and return!" << endl;
				return;
			}
		}
		subscribeResponses();
		subscribeConfirmation();
		pubFormationCtrlMsg();
	} 
	else {
		cout << "[Ground Station] Failed to initialize, quit g_station!" << endl;
		return;
	}
}

void GStation::start0()
{
	if (init()) {
		cout << "[Ground Station] About to publish /swarm_task_requirements." << endl;
		int cnt = 0;
		while (!_taskAssignFlag) {
			pubRequirements0();
			cnt++;
			if (cnt > _timeDurationSwarm) {
				cout << "[Ground Station] Timeout and return!" << endl;
				return;
			}
		}
		subscribeResponses0();
		subscribeConfirmation0();
	} 
	else {
		cout << "[Ground Station] Failed to initialize, quit g_station!" << endl;
		return;
	}
}

bool GStation::pubSwarmTask(const string& aTaskFileName, const int aRobotNum){
    if(aTaskFileName.substr(aTaskFileName.rfind(".")+1).compare("xml") != 0) {
        cout << "[Ground Station] failed to publish Task, task file name should be xxx.xml"<<endl;
        return false;
    }


    StringMsg strMsg;
    strMsg.sendID(0);
    strMsg.destID(-1);
    strMsg.cmdNo(_curCmdNo++);
    strMsg.reply(0);
    strMsg.data(aTaskFileName);

    boost::unique_lock<boost::mutex> lock(_pPubTaskXmlMutex);
    _pPubTaskXml->publish(strMsg);

    // waiting for reply
    if(aRobotNum > 0){
        _robotNum = aRobotNum;
        ROS_INFO("cmdNo [%ld] waiting reply", strMsg.cmdNo());
        while(true){
            if(_pubCond.timed_wait(lock, boost::get_system_time() + boost::posix_time::seconds(10))){
                boost::shared_lock<boost::shared_mutex> rlock(_cmdNoToResponseListMutex);
                if(_cmdNoToResponseList[strMsg.cmdNo()].size() >= aRobotNum) {
                    ROS_INFO("cmdNo [%ld] received all reply, reply count %lu", strMsg.cmdNo(), _cmdNoToResponseList[strMsg.cmdNo()].size());
                    break;
                }
            } else {
                ROS_INFO("cmdNo [%ld] waiting timeout(10s), reply count %lu, retry?(y/n):", strMsg.cmdNo(), _cmdNoToResponseList[strMsg.cmdNo()].size());
                std::string tmpStr = "";
                cin>>tmpStr;
                cin.clear();
                cin.ignore(INT_MAX, '\n');
                if(tmpStr.size() > 0 && std::toupper(tmpStr[0]) == 'Y'){
                    _pPubTaskXml->publish(strMsg);
                } else break;
            }
        }
    }
    
    return true;
	
}

void GStation::subSwarmTaskXmlResponseCallback(StringMsg& msg){
	if(msg.destID() != 0 && msg.destID() != -1) return;
	if(msg.reply() == 1){
		ROS_INFO("cmdNo [%ld] received robot [%d] reply.", msg.cmdNo(), msg.sendID());
		boost::unique_lock<boost::shared_mutex> wlock(_cmdNoToResponseListMutex);
		_cmdNoToResponseList[msg.cmdNo()].insert(msg.sendID()); 
		wlock.unlock();
		_pubCond.notify_one();
	}
}

bool GStation::pubUTOEventToStartupTask(const string& aSwarmName){
    SWARM_TAG_TYPE swarmTag = gActorTagStore.swarmName2Tag(aSwarmName);
    if(swarmTag == 255) {
        ROS_WARN("invalid task name, please recheck it!!!");
        return false;
    }

    MasterCmd cmd;
	cmd.sendID(0);
	cmd.receID(-1);
    cmd.cmdNo(_curCmdNo++);
	cmd.type(19);//CMD_TYPE_START_TASK_WITH_GS
    cmd.param1(0);//request
	cmd.param2(gActorTagStore.swarmName2Tag(aSwarmName)); // task tag

	boost::unique_lock<boost::mutex> lock(_pPubCmdMutex);
	_pPubCmd->publish(cmd);

    if(_robotNum > 0){
        ROS_INFO("cmdNo [%d] waiting reply", cmd.cmdNo());
        while(true){
            if(_pubCond.timed_wait(lock, boost::get_system_time() + boost::posix_time::seconds(10))){
                boost::shared_lock<boost::shared_mutex> rlock(_cmdNoToResponseListMutex);
                if(_cmdNoToResponseList[cmd.cmdNo()].size() >= _robotNum) {
                    ROS_INFO("cmdNo [%d] received all reply, reply count %lu", cmd.cmdNo(), _cmdNoToResponseList[cmd.cmdNo()].size());
                    break;
                }
            } else {
                ROS_INFO("cmdNo [%d] waiting timeout(10s), reply count %lu, retry?(y/n):", cmd.cmdNo(), _cmdNoToResponseList[cmd.cmdNo()].size());
                std::string tmpStr = "";
                cin>>tmpStr;
                cin.clear();
                cin.ignore(INT_MAX, '\n');
                if(tmpStr.size() > 0 && std::toupper(tmpStr[0]) == 'Y'){
                    _pPubCmd->publish(cmd);
                } else break;
            }
        }
    }
    return true;
}

bool GStation::pubUTOEventToSwitchActor(const string& aCurrentActorName, const string& aEventName){
	UTOEventWithGStation event;
	event.sendID(0);
	event.destID(-1);
	event.cmdNo(_curCmdNo++);
	event.reply(0);
	event.type(SWITCH_EVENT_MSG);
	event.currentActor(aCurrentActorName);
	event.eventName(aEventName);
	pubUTOEvent(event);
}

bool GStation::pubUTOEvent(UTOEventWithGStation& aEvent){
	boost::unique_lock<boost::mutex> lock(_pPubUTOEventMutex);
	_pPubUTOEvent->publish(aEvent);

	// todo waiting for reply
	// while(true){
	// 	_pubCond.wait(lock);
	// 	boost::shared_lock<boost::shared_mutex> rlock(_cmdNoToResponseListMutex);
	// 	if(_cmdNoToResponseList[aEvent.cmdNo()].size() >= 0) break;
	// }
}

void GStation::subWarnMsgCallback(StringMsg& msg){
	if(msg.destID() != 0 && msg.destID() != -1) return;
	//ROS_WARN("[GStation] warnmsg from [%d],%s", msg.sendID(), msg.data().c_str());

	// forward this msg to ros
	std_msgs::String rosmsg;
	std::stringstream ss;
	ss << "warn [" << msg.sendID() <<"]: "<< msg.data();
	rosmsg.data = ss.str();
	_pPubWarnMsg.publish(rosmsg);
}

void GStation::pubPingMsg(){
	MasterCmd cmd;
	cmd.sendID(0);
	cmd.receID(-1);
    cmd.cmdNo(_curCmdNo++);
	cmd.type(14);//CMD_TYPE_PING_WITH_GS
	cmd.param1(0); // request

	boost::unique_lock<boost::mutex> lock(_pPubCmdMutex);
	_pPubCmd->publish(cmd);
}

void GStation::subPingMsgCallback(MasterCmd& cmd){
	if(cmd.receID() != 0 && cmd.receID() != -1) return; 
    if(cmd.type() == 14 && cmd.param1() != 1) return;

    boost::unique_lock<boost::shared_mutex> wlock(_cmdNoToResponseListMutex);
    _cmdNoToResponseList[cmd.cmdNo()].insert(cmd.sendID()); 
    int cmdReplyCount  = _cmdNoToResponseList[cmd.cmdNo()].size();
    wlock.unlock();
    
    switch(cmd.type()) {
        case 14: { // ping reply
            std::string state = cmd.param2() == 0? "KEEPING":"VOTING";
            std::string type = "INVALID";
            switch(cmd.param4()){
                case 0: type = "AUTO"; break;
                case 1: type = "SEMIAUTO"; break;
                case 2: type="FORCE"; break;
                default:break;
            }
            if(cmd.param5() == 1){
                std::cout<<"SetMasterID Result: Robot [";
            } else {
                std::cout<<"CheckState Result: Robot [";
            } 
            std::cout<<cmd.sendID()<<"], State:"<<state<<", Masterid:"<<cmd.param3()<<", Type:"<<type<<", CmdNo:["<<cmd.cmdNo()<<"], ReplyCount:"<<cmdReplyCount<<std::endl;
            break;
        }
        case 20: {
            ROS_INFO("cmdNo [%d] received robot [%d] reply.", cmd.cmdNo(), cmd.sendID());
            _pubCond.notify_one();
            //std::cout<<"Reply CmdNo:"<<cmd.cmdNo()<<", Robot ["<<cmd.sendID()<<"], ReplyCount:"<<cmdReplyCount<<std::endl;
        }
    }
}

void GStation::pubForceMasterCmd(const int aMasterID, const int aForceType){
	MasterCmd cmd;
	cmd.sendID(0);
	cmd.receID(-1);
    cmd.cmdNo(_curCmdNo++);
	cmd.type(15);//CMD_TYPE_FORCE_MASTER_WITH_GS
	cmd.param1(aMasterID); 	// master id
	cmd.param2(aForceType);	// force set
	boost::unique_lock<boost::mutex> lock(_pPubCmdMutex);
	_pPubCmd->publish(cmd);
}

void GStation::pubActivateActorMasterCmd(const int aDestID, const string& aSwarmName, const string& anActorName){
	int swarmTag = gActorTagStore.swarmName2Tag(aSwarmName);
	int actorTag = gActorTagStore.actorName2Tag(aSwarmName, anActorName);
	if(swarmTag == -1 || actorTag == -1) return;

	MasterCmd cmd;
	cmd.sendID(0);
	cmd.receID(aDestID);
	cmd.type(16);//CMD_TYPE_ACTIVATE_ACTOR_WITH_GS
	cmd.param1(swarmTag);
	cmd.param2(actorTag);
	boost::unique_lock<boost::mutex> lock(_pPubCmdMutex);
	_pPubCmd->publish(cmd);
}

void GStation::pubYieldActorMasterCmd(const int aDestID, const string& aSwarmName, const string& anActorName){
	int swarmTag = gActorTagStore.swarmName2Tag(aSwarmName);
	int actorTag = gActorTagStore.actorName2Tag(aSwarmName, anActorName);
	if(swarmTag == -1 || actorTag == -1) return;
	
	MasterCmd cmd;
	cmd.sendID(0);
	cmd.receID(aDestID);
	cmd.type(17);//CMD_TYPE_YIELD_ACTOR_WITH_GS
	cmd.param1(swarmTag);
	cmd.param2(actorTag);
	boost::unique_lock<boost::mutex> lock(_pPubCmdMutex);
	_pPubCmd->publish(cmd);
}

void GStation::pubSwitchActorMasterCmd(const int aDestID, const string& aSwarmName, const string& aYieldActorName, const string& anActivateActorName){
	int swarmTag = gActorTagStore.swarmName2Tag(aSwarmName);
	int yieldActorTag = gActorTagStore.actorName2Tag(aSwarmName, aYieldActorName);
	int activeActorTag = gActorTagStore.actorName2Tag(aSwarmName, anActivateActorName);
	if(swarmTag == -1 || yieldActorTag == -1 || activeActorTag == -1) return;
	
	MasterCmd cmd;
	cmd.sendID(0);
	cmd.receID(aDestID);
	cmd.type(18);//CMD_TYPE_SWITCH_ACTOR_WITH_GS
	cmd.param1(swarmTag);
	cmd.param2(yieldActorTag);
	cmd.param3(activeActorTag);
	boost::unique_lock<boost::mutex> lock(_pPubCmdMutex);
	_pPubCmd->publish(cmd);
}
