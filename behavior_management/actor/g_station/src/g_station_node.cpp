/* ==================================================================
*
* Copyright (c) 2016-2019, micROS Group, NIIDT, TAIIC, HPCL.
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions 
* are met: 
*
* 1. Redistributions of source code must retain the above copyright 
*     notice, this list of conditions and the following disclaimer. 
* 2. Redistributions in binary form must reproduce the above copyright 
*     notice, this list of conditions and the following disclaimer in the 
*     documentation and/or other materials provided with the 
*     distribution. 
* 3. All advertising materials mentioning features or use of this software 
*     must display the following acknowledgement: 
*     This product includes software developed by the micROS Group. and 
*     its contributors. 
* 4. Neither the name of the Group nor the names of its contributors may 
*     be used to endorse or promote products derived from this software 
*     without specific prior written permission. 
*
* THIS SOFTWARE IS PROVIDED BY MICROS,GROUP AND CONTRIBUTORS
* ''AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT
* NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
* FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL 
* THE MICROS,GROUP OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS 
* OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) 
* HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
* OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN 
* IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
* ===================================================================
*
* Author: Bin Di, Chunlian Fu, Junling Gao, Ruihao Li, Wei Yi.
*         (in alphabetical order)
*
*/

#include "g_station/g_station.h"
#include "ros/ros.h"
#include <string>
#include "actor_tag_store/ActorTagStore.h"
using namespace std;

void cinString(string& data){
    data.clear();
    cin>>data;
    std::cin.clear();
    std::cin.ignore(INT_MAX, '\n');
}

bool cinInt(int& data){
    std::string tmp = "";
    cinString(tmp);

    int i = 0;
    while(i < tmp.size() && tmp[i] != '\0' && isdigit(tmp[i])){
         i++;
    }

    if (i<tmp.size()) {
        cout<<"invalid input!"<<endl;
        return false;
    } else {
        data = atoi(tmp.c_str());
        return true;
    }
}

int main(int argc, char** argv) {

    //micROS::init();

    string swarmMatchFilePath;
    string swarmTaskFilePath;
    string utoTaskFilePath;
    bool getArgvFlag = true;
    if (argc != 3 || argc != 4)
    {
        //ROS_INFO("usage: rosrun g_station g_station_node YOUR_MATCH_FILE_PATH YOUR_TASK_FILE_PATH");
        cout << "usage: rosrun g_station g_station_node YOUR_MATCH_FILE_PATH YOUR_TASK_FILE_PATH" << endl;
        getArgvFlag = false;
    }
    else {
        swarmMatchFilePath = argv[1];
        swarmTaskFilePath = argv[2];
        utoTaskFilePath = argv[3];
    }

    ros::init(argc, argv, "ground_station");
    ros::NodeHandle nh;

    bool getParamFlag = false;
    // get swarm match file path from param server
    if (nh.hasParam("/swarm_match_file_path")) {
        nh.getParam("/swarm_match_file_path",swarmMatchFilePath); 
        getParamFlag = true;
    } else {
        // ROS_WARN("Cannot find swarm_match_file_path in the parameter server!");
        cout << "[Ground Station] Warning: Cannot find swarm_match_file_path in the parameter server!" << endl;
        getParamFlag = false;
    }
    // get swarm task file path from param server
    if (nh.hasParam("/swarm_task_file_path")) {
        nh.getParam("/swarm_task_file_path",swarmTaskFilePath); 
        getParamFlag = getParamFlag && true;
    } else {
        // ROS_WARN("Cannot find swarm_task_file_path in the parameter server!");
        cout << "[Ground Station] Warning: Cannot find swarm_task_file_path in the parameter server!" << endl;
        getParamFlag = false;
    }

    // Get uto task xml from param server
    if(nh.hasParam("/uto_task_file_path") && getParamFlag) {
        nh.getParam("/uto_task_file_path", utoTaskFilePath);
    }

    if (!getArgvFlag && !getParamFlag)
    {
        //ROS_ERROR("Could not get init file path from param server or command line! Please revise launch file or command line!");
        cout << "[Ground Station] ERROR: Could not get init file path from param server or command line! Please revise launch file or command line!" << endl;
        return 0;
    }

    cout << "swarm_match_file_path is " << swarmMatchFilePath << ", and swarm_task_file_path is " << swarmTaskFilePath << endl;
    cout << "uto_task_file_path is " << utoTaskFilePath << endl;

#ifdef DDS_STATIC_DISCOVERY
    std::string strDDSXmlProfile = "";
    if (nh.hasParam("/dds_static_profile_xml")) {
        nh.getParam("/dds_static_profile_xml",strDDSXmlProfile); 
    } else {
        ROS_WARN("cannot find /dds_static_profile_xml in the parameter server");
    }
    micROS::init(strDDSXmlProfile);
#else
    // fastrtps participant discovery list 
    std::string strRobotListFile="";
    if (nh.hasParam("/robotListFile")) {
        nh.getParam("/robotListFile",strRobotListFile); 
    } else {
        ROS_WARN("cannot find /robotListFile in the parameter server");
    }
    
    int domainId = 0;
    if(nh.hasParam("/dds_domainid")){
        nh.getParam("/dds_domainid", domainId);
    } else {
        ROS_WARN("cannot find /dds_domainid in the parameter server, default 0");
    }
	ROS_WARN("DDS DomainId is %d", domainId);
    
    micROS::init(domainId, strRobotListFile);
#endif

    // tag list directory
    std::string strTagListFile = "";
    if (nh.hasParam("/taglistdict")) {
        nh.getParam("/taglistdict",strTagListFile); 
    } else {
        ROS_WARN("cannot find /taglistdict in the parameter server");
    }
    if (!gActorTagStore.init(strTagListFile)) {
		std::cout<<"FATAL: unable to get the actorName to tag mapping!"<<std::endl;
		return 1;
	}    

    GStation* pStation = new GStation(swarmMatchFilePath, swarmTaskFilePath, utoTaskFilePath, 5, 5);
    //pStation->start();
    int sel = -1;
    while(sel != 0){
        cout<<"#########################################################"<<endl;
        cout<<"# 1. publish task \t\t 2. start task "<<endl;
        cout<<"# 3. publish uto event \t\t 4. check swarm state "<<endl;
        cout<<"# 5. force set master \t\t 6. force activate actor "<<endl;
        cout<<"# 7. force yield actor \t\t 8. force switch actor "<<endl;
        cout<<"# 0. exit "<<endl;
        cout<<"#########################################################"<<endl;
        cout<<"choose your operation:";
        if(!cinInt(sel) || sel < 0 || sel > 8){
            cout<<"invalid input, please choose in [0-8]!"<<endl;
            continue;
        }

        switch(sel){
            case 1:{
                cout<<"please input task xml file name:";
                string fileName = "";
                cinString(fileName);

                cout<<"please input robot number:";
                int robotNum = 0;
                if(cinInt(robotNum)){
                    pStation->pubSwarmTask(fileName, robotNum);
                }
                break;
            }
            case 2:{
                cout<<"please input task name to start:";
                string taskName = "";
                cinString(taskName);
                pStation->pubUTOEventToStartupTask(taskName);
                break;
            }
            case 3:{
                cout<<"please input actor name and event name, split by enter:";
                string actorName = "", eventName = "";
                cinString(actorName);
                cinString(eventName);
                pStation->pubUTOEventToSwitchActor(actorName, eventName);
                break;
            }
            case 4:{
                pStation->pubPingMsg();
                break;
            }
            case 5:{
                cout<<"please input master robotID:";
                int masterID = 0;
                if(cinInt(masterID)){ //todo check whether masterID is in robotList or not
                    cout<<"please input force type (0-AUTO, 1-SENIAUTO, 2-FORCE):";
                    int type = 0;
                    if(!cinInt(type) || type < 0 || type > 2)
                        cout<<"force set master node failed, invalid force type!"<<endl;
                    else pStation->pubForceMasterCmd(masterID, type);
                }
                break;
            }
            case 6:{
                cout<<"please input target robotID, swarm name and activate actor name, split by enter:";
                int robotID = -1;
                if(cinInt(robotID)){
                    string swarmName = "", actorName = "";
                    cinString(swarmName);
                    cinString(actorName);
                    pStation->pubActivateActorMasterCmd(robotID, swarmName, actorName);
                }
                break;
            }
            case 7:{
                cout<<"please input target robotID, swarm name and yield actor name, split by enter:";
                int robotID = -1;
                if(cinInt(robotID)){
                    string swarmName = "", actorName = "";
                    cinString(swarmName);
                    cinString(actorName);
                    pStation->pubYieldActorMasterCmd(robotID, swarmName, actorName);
                }
                break;
            }
            case 8:{
                cout<<"please input target robotID, swarm name, yield actor name and activate actor name, split by enter:";
                int robotID = -1;
                if(cinInt(robotID)){
                    string swarmName = "", yieldActorName = "", activateActorName = "";
                    cinString(swarmName);
                    cinString(yieldActorName);
                    cinString(activateActorName);
                    pStation->pubSwitchActorMasterCmd(robotID, swarmName, yieldActorName, activateActorName);
                }
                break;
            }
            default: break;
        }
        sleep(1);
    }
    
    delete pStation;
    pStation = NULL;
    
   // ros::spin();

    micROS::finish();

    return 1;
}
