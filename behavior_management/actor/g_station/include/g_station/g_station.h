/* ==================================================================
*
* Copyright (c) 2016-2019, micROS Group, NIIDT, TAIIC, HPCL.
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions 
* are met: 
*
* 1. Redistributions of source code must retain the above copyright 
*     notice, this list of conditions and the following disclaimer. 
* 2. Redistributions in binary form must reproduce the above copyright 
*     notice, this list of conditions and the following disclaimer in the 
*     documentation and/or other materials provided with the 
*     distribution. 
* 3. All advertising materials mentioning features or use of this software 
*     must display the following acknowledgement: 
*     This product includes software developed by the micROS Group. and 
*     its contributors. 
* 4. Neither the name of the Group nor the names of its contributors may 
*     be used to endorse or promote products derived from this software 
*     without specific prior written permission. 
*
* THIS SOFTWARE IS PROVIDED BY MICROS,GROUP AND CONTRIBUTORS
* ''AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT
* NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
* FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL 
* THE MICROS,GROUP OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS 
* OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) 
* HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
* OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN 
* IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
* ===================================================================
*
* Author: Bin Di, Chunlian Fu, Junling Gao, Ruihao Li, Wei Yi.
*         (in alphabetical order)
*
*/

#ifndef __G_STATION__
#define __G_STATION__

#include "ros/ros.h"
#include <string>
#include <iostream>
#include <tinyxml.h>
#include <stdlib.h>
#include <vector>
#include <sstream>
#include <sys/time.h>
#include <time.h>

#include "actor_core/actor_core.h"
#include "actor_msgs/swarm_task.h"
#include "actor_msgs/gstation_remote_cmds.h"
#include "g_station/SwarmTask.h"
#include "g_station/CtrlMsg.h"

#include "RTPSPublisher.h"
#include "RTPSSubscriber.h"
#include "micROSRTPSExt.h"
#include "UTOEventWithGStation.h"
#include "UTOEventTransition.h"
#include "StringMsg.h"
#include "MasterCmd.h"

using namespace std;

#define GET_CUR_SEC  \
	timeval temp_timeV;\
	gettimeofday(&temp_timeV,NULL);\
	string curSecStr;\
	stringstream temp_ss;\
	temp_ss.clear();\
	temp_ss << temp_timeV.tv_sec;\
	temp_ss >> curSecStr;

class GStation {
public:
	GStation(string aMatchFilePath, string aTaskFilePath, string anUtoFilePath, int aTimeDurationSingle, int aTimeDurationSwarm);
	~GStation();
	struct SwarmTaskResponse {
		int64_t platformID;
		std::vector<string> actorNameVec;
		std::vector<int> bidActorVec;
	};
	struct ActorArchitecture {
		std::vector<string> generalActors;
		std::vector<string> exclusiveActors;
		std::vector<string> dynamicActors;
	};
	struct SwarmArchitecture {
		struct ActorArchitecture actors;
		std::vector<int64_t> platformID;
	};
	struct SwarmArchitecture swarmArchitectureInfo;		// swarm size (or actor numbers) is considered, e.g. Observer1, Obsserver2, ...
	vector<string> swarmTaskActorList;					// compared to swarmArchitectureInfo, this variable does not consider swarm size, e.g. the vector may be like Observer, Leader, Follower, Transmitter, ...
	std::vector<struct SwarmTaskResponse> responseVec;
	std::vector<int64_t> confirmedPlatformID;
	ros::NodeHandle _nh;
	string _matchFilePath;
	string _taskFilePath;
	int _timeDurationSingle;
	int _timeDurationSwarm;
	string _swarmMatchXmlStr;
	string _swarmTaskXmlStr;
	string _swarmName;

	bool _taskAssignFlag;		// true: OK to assign task; false: not OK to assign task
	bool _confirmAssignFlag;		// true: task assignment confirmed; false: not confirmed
	bool _subRespOvertimeFlag;	// true: overtime while subscribing match responses; false: not overtime
	bool _subConfOvertimeFlag;	// true: overtime while subscribing task assignment confirmation; false: not overtime

	bool init();
	bool confirmSwarmTaskAssignment();

	//----------------------- Based on FAST-RTPS -----------------------//
	void pubRequirements();
	void pubAssignments();
	void pubFormationCtrlMsg();
	bool initSwarmArchitecture(string aXmlStr);
	void subscribeResponses();
	void subResponseCallback(SwarmTask& msg);
	void subscribeConfirmation();
	void subConfirmationCallback(SwarmTask& msg);
	void start();

	//-------------------------- Based on ROS --------------------------//
	void pubRequirements0();
	void pubAssignments0();
	void subscribeResponses0();
	void subResponseCallback0(const actor_msgs::swarm_task& msg);
	void subscribeConfirmation0();
	void subConfirmationCallback0(const actor_msgs::swarm_task& msg);
	void start0();

	void subRemoteCmdsCallback0(const actor_msgs::gstation_remote_cmds& msg);

	//------------------------------begin gstation new version---------------//
	// publish swarm task to all robots in the swarm, and wait for reply
	// @param taskFilePath taskxml file
	bool pubSwarmTask(const string& aTaskFileName, const int aRobotNum);
	void subSwarmTaskXmlResponseCallback(StringMsg& msg);
	// notify all robots to switch state
	// @param currentActorName current actor which is running
	// @param eventName defined in task xml, this event will lead to actor switch
	bool pubUTOEventToStartupTask(const string& aSwarmName);
	bool pubUTOEventToSwitchActor(const string& aCurrentActorName, const string& aEventName);

	void subWarnMsgCallback(StringMsg& msg);

	void pubPingMsg();
	void subPingMsgCallback(MasterCmd& cmd);

	void pubForceMasterCmd(const int aMasterID, const int aForceType);

	void pubActivateActorMasterCmd(const int aDestID, const string& aSwarmName, const string& anActorName);
	void pubYieldActorMasterCmd(const int aDestID, const string& aSwarmName, const string& anActorName);
	void pubSwitchActorMasterCmd(const int aDestID, const string& aSwarmName, const string& aYieldActorName, const string& anActivateActorName);

private:
	bool pubUTOEvent(UTOEventWithGStation& aEvent);
	
private:
	RTPSPublisher<StringMsg> *_pPubTaskXml;	
	boost::mutex _pPubTaskXmlMutex;
	boost::condition_variable _pubCond;
	RTPSSubscriber<StringMsg> *_pSubTaskXmlResponse;

	RTPSPublisher<UTOEventWithGStation> *_pPubUTOEvent;
	boost::mutex _pPubUTOEventMutex;

	RTPSPublisher<MasterCmd> *_pPubCmd;
	boost::mutex _pPubCmdMutex;
	RTPSSubscriber<MasterCmd> *_pSubCmd;

	RTPSSubscriber<StringMsg> *_pSubWarnMsg;
	ros::Publisher _pPubWarnMsg;

	std::map<int64_t, std::set<int32_t> > _cmdNoToResponseList;
	boost::shared_mutex _cmdNoToResponseListMutex;

	int64_t _curCmdNo;
    int32_t _robotNum;
	//------------------------------end gstation new version---------------//
private:
	RTPSPublisher<SwarmTask>  *_pPubReq;
	RTPSSubscriber<SwarmTask> *_pSubResp;
	RTPSPublisher<SwarmTask>  *_pPubAssign;
	RTPSSubscriber<SwarmTask> *_pSubConfirm;
	RTPSPublisher<CtrlMsg>  *_pPubCommand;
	ros::Subscriber _subRemoteCmds;

	// Uto
	string _utoFilePath;
	string _utoTaskXmlStr;
	RTPSPublisher<StringMsg>  *_utoPub;
	RTPSPublisher<UTOEventTransition>  *_utoSyncPub;
	RTPSSubscriber<UTOEventTransition> *_utoSyncSub;
	void subUTOEventCallback(UTOEventTransition &msg);
};

#endif
