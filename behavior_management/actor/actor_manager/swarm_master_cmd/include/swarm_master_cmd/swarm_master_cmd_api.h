/* ==================================================================
*
* Copyright (c) 2016-2019, micROS Group, NIIDT, TAIIC, HPCL.
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions 
* are met: 
*
* 1. Redistributions of source code must retain the above copyright 
*     notice, this list of conditions and the following disclaimer. 
* 2. Redistributions in binary form must reproduce the above copyright 
*     notice, this list of conditions and the following disclaimer in the 
*     documentation and/or other materials provided with the 
*     distribution. 
* 3. All advertising materials mentioning features or use of this software 
*     must display the following acknowledgement: 
*     This product includes software developed by the micROS Group. and 
*     its contributors. 
* 4. Neither the name of the Group nor the names of its contributors may 
*     be used to endorse or promote products derived from this software 
*     without specific prior written permission. 
*
* THIS SOFTWARE IS PROVIDED BY MICROS,GROUP AND CONTRIBUTORS
* ''AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT
* NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
* FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL 
* THE MICROS,GROUP OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS 
* OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) 
* HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
* OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN 
* IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
* ===================================================================
*
* Author: Bin Di, Chunlian Fu, Junling Gao, Ruihao Li, Wei Yi.
*         (in alphabetical order)
*
*/

#ifndef __SWARM_MASTER_CMD_API_H__
#define __SWARM_MASTER_CMD_API_H__
#include <string>
#include "actor_core/barrier.h"
//class SwarmMasterCmd;
//extern SwarmMasterCmd *gSwarmMasterCmd;

extern void requestActivateActorFromMaster(const std::string& aSendActorName,const std::string& anActorName);
extern void requestSwitchActorFromMaster(const std::string& aSendActorName,const std::string& anActorName);

/**
 * @brief actor barrier api
 * 
 * @param aBarrierKey   barrierKey, should be unique in the actor control process
 * @param aWaitCount    when the number of robots reached to the barrier >= aWaitCount, the caller will go on.
 * @param aTimeout      seconds,default 5s
 * @return BarrierResult 
 */
extern BarrierResult actorBarrierApi(GlobalBarrierKey& aBarrierKey, int aWaitCount,  const short aTimeout = 5);

/**
 * @brief plugin barrier api
 * 
 * @param aBarrierKey   barrierKey, should be unique
 * @param aWaitCount    when the number of robots reached to the barrier >= aWaitCount, the caller will go on. 
 * @param aTimeout      seconds,default 5s
 * @return BarrierResult 
 */
extern BarrierResult pluginBarrierApi(GlobalBarrierKey& aBarrierKey, int aWaitCount,  const short aTimeout = 5);

/**
 * @brief data distribute api
 * 
 * @param aBarrierKey   barrierKey, should be unique
 * @param anActorName   the actor which call this method, diffirent actor name means diffirent request client.
 * @param aWaitCount    when the number of robots reached to the barrier >= aWaitCount, the caller will go on. 
 * @param aTimeout      seconds
 * @param[out] anIndex  data index, start with 1, avaliable when BarrierResult is YES.
 * @return BarrierResult 
 */
extern BarrierResult actorDataDistributeBarrierApi(GlobalBarrierKey& aBarrierKey, std::string anActorName, int aWaitCount, const short aTimeout, int &anIndex);


//TODO temp add formation content for DA
extern short getFormationPos(const int aRobotID);

#endif