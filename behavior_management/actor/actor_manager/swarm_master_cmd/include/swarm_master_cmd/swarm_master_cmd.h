/* ==================================================================
*
* Copyright (c) 2016-2019, micROS Group, NIIDT, TAIIC, HPCL.
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions 
* are met: 
*
* 1. Redistributions of source code must retain the above copyright 
*     notice, this list of conditions and the following disclaimer. 
* 2. Redistributions in binary form must reproduce the above copyright 
*     notice, this list of conditions and the following disclaimer in the 
*     documentation and/or other materials provided with the 
*     distribution. 
* 3. All advertising materials mentioning features or use of this software 
*     must display the following acknowledgement: 
*     This product includes software developed by the micROS Group. and 
*     its contributors. 
* 4. Neither the name of the Group nor the names of its contributors may 
*     be used to endorse or promote products derived from this software 
*     without specific prior written permission. 
*
* THIS SOFTWARE IS PROVIDED BY MICROS,GROUP AND CONTRIBUTORS
* ''AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT
* NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
* FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL 
* THE MICROS,GROUP OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS 
* OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) 
* HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
* OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN 
* IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
* ===================================================================
*
* Author: Bin Di, Chunlian Fu, Junling Gao, Ruihao Li, Wei Yi.
*         (in alphabetical order)
*
*/

#ifndef __SWARM_MASTER_CMD_H__
#define __SWARM_MASTER_CMD_H__
#include <map>
#include <vector>
#include "RTPSPublisher.h"
#include "RTPSSubscriber.h"
#include "MasterCmd.h"
#include "CmdReply.h"
#include "ros/time.h"
#include "ros/ros.h"
#include "actor_core/barrier.h"
#include <boost/thread/thread.hpp>
#include <boost/thread/condition.hpp>
#include <boost/thread/mutex.hpp>

//receID received by all members, used by destRobotID
const int32_t RECE_ID_ALL_MEMBER = -1;

//request actor from master type defination
const int32_t REQUEST_ACTOR_TYPE_ACTIVATE = 1;  // just need activate the request actor
const int32_t REQUEST_ACTOR_TYPE_SWITCH = 2;    // stop current actor and activate the request actor

//swarm master cmd msg channel, send/receive master cmd to/from swarm members and g_statioin through fastrtps
class SwarmMasterCmd{
public:
    SwarmMasterCmd();
    ~SwarmMasterCmd();

    // this method will be called when robot join, the master will add it to swarm formation
    void addRobotToFormation(const int32_t aRobotID);

    // this method will be called when robot join or leave, the master should reassign swarm formation
    void reassignFormation();

    void sendFormationSwitchCmd(const int32_t aDestRobotID, const int32_t aPos);

    void sendFormationSaveCmd(const int32_t aDestRobotID, const int32_t aPos);

    // called when local robot becomes the master, to notify other member robots
    // or when a robot join, the master should notify it.
    void sendIamMasterCmd(const int32_t aOldMasterID, const int32_t aDestRobotID = RECE_ID_ALL_MEMBER);
    
    // send ping cmd
    void sendPingCmd(const int32_t aDestRobotID, const bool bReply = false);

    // send ping response cmd
    void sendPingResponseToGStation(const int32_t aStationID, const int32_t aCmdNo, bool aIsInitiative = false);

    // called when a robot join, the master should share all robots information to it.
    // currently just share the swarm formation infomation.
    void sendInfo2RobotNewJoin(const int32_t aRootID);
    
    // called when the robot need a actor, this method will send a request to the master.
    // @param aSendActorName the actor which call this method
    // @param aActorName request actor
    // @param aType REQUEST_ACTOR_TYPE_ACTIVATE / REQUEST_ACTOR_TYPE_SWITCH
    void sendSwarmActorRequestCmd(const std::string& aSendActorName, const std::string& aActorName, int aType);

    // called when the master receive actor request
    // @note if multi robots request the same actor within 10s, the master will handle it as a same command
    bool handleSwarmActorRequest(const int32_t aSendID, const int aType,  const std::string& aSwarmName, const std::string& aSendActorName, const std::string& aActorName);

    // called when plugin reached to the barrier point, it will send barrier request to the master
    BarrierResult sendBarrierRequestCmd(GlobalBarrierKey& aBarrierKey, const std::string& anActorName, int aWaitCount, const short aTimeout, int &aIndex);

    // called when the master received barrier request, when received robots count >= aWaitCount, the master will reply to these robots with yes result
    // @return barrier result
    BarrierResult handleBarrierRequest(GlobalBarrierKey& aBarrierKey, const std::string& anActorName, const int aWaitCount, const int aRobotID, int &aIndex);

    // called when master notify robots barrier result
    void sendBarrierResponse(GlobalBarrierKey& aBarrierKey, const int aDestID, const std::string& anActorName, const int aResultDataIndex);

    void sendActivateActorCmd(const int aDestID, const std::string& aSwarmName, const std::string& anActorName);

    void sendSwitchActorCmd(const int aDestID, const std::string& aSwarmName, const std::string& anActorName, const std::string& aTargetActorName);

    // called when a robot leave,then clear all master cmds that will send to it
    void clearMasterCmdsForRobot(const int32_t aRobotID);
    
    // called when switch master, and this robot change to member, then clearAllMasterCmds
    void clearAllMasterCmds();
private:
    //send master cmd and master cmd call back
    bool sendMasterCmdAndGetReply(MasterCmd* pCmd);
    void sendMasterCmd(MasterCmd* pCmd);
    void masterCmdCallback(MasterCmd& aCmd);
    void gsMasterCmdCallBack(MasterCmd& aCmd);
    void masterCmdReplyCallback(CmdReply& aCmd);
    
    //resend master cmd until recv reply, config periodic time by RESEND_DURATION_CMD_RTPS
    void resendTimeoutMasterCmdLoop();
    void resendTimeoutMasterCmd();

    //send reply 
    void sendMasterCmdReply(const int aDestID, const int cmdNo);
    void sendReplyToGStation(const int32_t aStationID, const int32_t aCmdNo);


    //UTO event
    void sendActiveActorEvent(const std::string& anActorName);
    void sendPauseActorEvent(const std::string& anActorName);
    void sendSwitchActorEvent(const std::string& anActorName, const std::string& aTargetActorName);

private:
    int _robotID;

    // lost actor
    std::string _lostActorName;
	std::string _candidateActorName;

    //TODO temp add formation content for DA
    bool _reassignSwitch;

    // UTO event
    ros::Publisher _pubUTOEvent;
    boost::mutex _pubUTOEventMutex;

    //rtps sub pub master cmd from ground station
    RTPSPublisher<MasterCmd>* _pPubGSMasterCmd;
    RTPSSubscriber<MasterCmd>* _pSubGSMasterCmd;
    boost::mutex _pPubGSMasterCmdMutex;

    //rtps sub pub master cmd with swarm members
    RTPSPublisher<MasterCmd>* _pPubMasterCmd;
    RTPSSubscriber<MasterCmd>* _pSubMasterCmd;
    boost::mutex _pubMutex;

    //rtps reply msg
    RTPSPublisher<CmdReply>* _pPubCmdReply;
    RTPSSubscriber<CmdReply>* _pSubCmdReply;
    boost::mutex _pPubCmdReplyMutex;

    //send cmdNo map, cmdNo-->map(robotID-->reply), 
    //reply init RESEND_NUM_CMD_RTPS, and decrease it when receive reply
    std::map<int32_t,std::map<int32_t, bool> > _cmdNo2RobotReply;
    boost::shared_mutex _cmdNo2RobotReplyMutex;

    // increased by 1, when create a new cmd
    int32_t _masterCmdNo;

    // contains received master cmdNo, robotID-->cmdNo (used by member robots)
    std::map<int32_t,int32_t> _robotID2ReceivedCmdNo; 

     // contains msg which is timeout and should be resend by _pthResendTimeoutMsgs thread
    struct MasterCmdQueue{
        std::map<int, MasterCmd*> _msgQueue;
        short _resendCount;     // resend count, linear increase send duration when there's no reply.increase after send cmd.
        short _leftWaitCount;   // after _leftWaitCount loop count(_leftWaitCount=0), this cmd will be send again. decrease in every loop.
        MasterCmdQueue():_resendCount(0), _leftWaitCount(0){}
        void clearQueue();
        void popMsg();
        void pushMsg(MasterCmd* cmd);
        int size();
        bool isEmpty();
    };

    std::map<int, MasterCmdQueue> _robotID2MasterCmdQueue; // receID-->(cmdNo->cmd)
    boost::shared_mutex _robotID2MasterCmdQueueMutex;
    boost::shared_ptr<boost::thread> _pthResendTimeoutMsgs;

    // store actor request cmd result, actorName->(time, allowed robotIDs)
    // only for master
    std::map<std::string, std::pair<ros::Time, std::vector<int> > >_actorName2RequestResult;
    boost::shared_mutex _actorName2RequestResultMutex;

    // barrier result and data index
    struct BarrierResultDataIndex{
        BarrierResult _result;
        int _index;
        BarrierResultDataIndex():_result(BarrierResult::NO),_index(0){}
    };
    // robot ID and actor name, used as key of _requestRobots 
    struct RobotIDandActorName{
        int _robotID;
        std::string _actorName;

        RobotIDandActorName():_robotID(-1),_actorName(""){}
        RobotIDandActorName(int aRobotID, const std::string& anActorName):_robotID(aRobotID),_actorName(anActorName){}
        friend bool operator <(const RobotIDandActorName& key1, const RobotIDandActorName& key2){
            if(key1._robotID < key2._robotID) return true;
            if(key1._robotID == key2._robotID){
                if(key1._actorName < key2._actorName) return true;
            }
            return false;
        }
    };

    // store barrier list,<barrier key, received robotsIDs>
    // only for master
    struct BarrierRequestList{
        bool _isReplyed;
        ros::Time _firstRequestTime;
        ros::Time _firstReplyTime;
        std::map<RobotIDandActorName, BarrierResultDataIndex> _requestRobots;
        BarrierRequestList():_isReplyed(false){}
    };
    std::map<GlobalBarrierKey, BarrierRequestList> _barrierKey2BarrierRequestList;
    boost::shared_mutex _barrierKey2BarrierRequestListMutex;

    // barrier result data index and condition signal
    struct BarrierResultDataIndexCond{
        boost::shared_ptr<boost::condition_variable_any> _cond;
        BarrierResultDataIndex _barrierResult;
    };
    // store barrier condition signal,when receive barrier response from master, it will send this signal
    // only for member robot
    std::map<GlobalBarrierKey, std::map<std::string, BarrierResultDataIndexCond> > _barrierKey2ConditionAndResult;
    boost::shared_mutex _barrierKey2ConditionAndResultMutex;
};
#endif