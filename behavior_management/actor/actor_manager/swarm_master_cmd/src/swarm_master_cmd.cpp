/* ==================================================================
*
* Copyright (c) 2016-2019, micROS Group, NIIDT, TAIIC, HPCL.
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions 
* are met: 
*
* 1. Redistributions of source code must retain the above copyright 
*     notice, this list of conditions and the following disclaimer. 
* 2. Redistributions in binary form must reproduce the above copyright 
*     notice, this list of conditions and the following disclaimer in the 
*     documentation and/or other materials provided with the 
*     distribution. 
* 3. All advertising materials mentioning features or use of this software 
*     must display the following acknowledgement: 
*     This product includes software developed by the micROS Group. and 
*     its contributors. 
* 4. Neither the name of the Group nor the names of its contributors may 
*     be used to endorse or promote products derived from this software 
*     without specific prior written permission. 
*
* THIS SOFTWARE IS PROVIDED BY MICROS,GROUP AND CONTRIBUTORS
* ''AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT
* NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
* FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL 
* THE MICROS,GROUP OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS 
* OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) 
* HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
* OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN 
* IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
* ===================================================================
*
* Author: Bin Di, Chunlian Fu, Junling Gao, Ruihao Li, Wei Yi.
*         (in alphabetical order)
*
*/

#include "swarm_master_cmd/swarm_master_cmd.h"
#include "swarm_data_storage/swarm_data_storage.h"
#include "actor_tag_store/ActorTagStore.h"
#include "actor_msgs/UTOEvent.h"
#include "ros/ros.h"
#include <vector>
#include <algorithm>
#include "actor_core/actor_constant.h"

#define BARRIER_WAIT_COUNT_ALL_ROBOTS 32767

//resend num and duration for master cmd with rtps
#define RESEND_DURATION_CMD_RTPS 3*100      // ms
#define RESEND_MAX_DURATION_CMD_RTPS 10000   // ms

//cmd type defination
const int32_t CMD_TYPE_REPLY = 0;               // reply msg
const int32_t CMD_TYPE_ACTIVATE_ACTOR = 1;      // master->member
const int32_t CMD_TYPE_YIELD_ACTOR = 2;         // master->member
const int32_t CMD_TYPE_SWITCH_ACTOR = 3;        // master->member
const int32_t CMD_TYPE_SET_FORMATION = 4;       // master->member
const int32_t CMD_TYPE_GET_FORMATION = 5;       // master->member
const int32_t CMD_TYPE_SET_SAVE_FORMATION = 6;  // master->member,to notify all member robots save reassigned position
const int32_t CMD_TYPE_SET_FORMATION_SWITCH = 7;// master->member,to notify member robot change position
const int32_t CMD_TYPE_SWITCH_MASTER = 8;       // master->member,used for switch master,new master send this cmd

const int32_t CMD_TYPE_REQUEST_ACTOR = 9;       // member->master,request actor
const int32_t CMD_TYPE_REQUEST_ACTOR_RSP = 10;  // master->member,response actor

const int32_t CMD_TYPE_BARRIER_REQUEST = 11;    // member->master
const int32_t CMD_TYPE_BARRIER_RESPONSE = 12;   // master->member

const int32_t CMD_TYPE_PING = 13;       // ping 
const int32_t CMD_TYPE_PING_WITH_GS = 14;

const int32_t CMD_TYPE_FORCE_MASTER_WITH_GS = 15; // force master from gstation
const int32_t CMD_TYPE_ACTIVATE_ACTOR_WITH_GS = 16;
const int32_t CMD_TYPE_YIELD_ACTOR_WITH_GS = 17;
const int32_t CMD_TYPE_SWITCH_ACTOR_WITH_GS = 18;
const int32_t CMD_TYPE_START_TASK_WITH_GS = 19;
const int32_t CMD_TYPE_REPLY_WITH_GS = 20;

SwarmMasterCmd::SwarmMasterCmd():_masterCmdNo(0), _reassignSwitch(false){
    _robotID = SwarmDataStorage::instance()->getLocalRobotID();

    ros::NodeHandle nh;
    if (nh.hasParam("/lost_actor_switch")) {
        nh.getParam("/lost_actor_name", _lostActorName);
        nh.getParam("/candidate_actor_name", _candidateActorName);
    } 
    _pubUTOEvent = nh.advertise<actor_msgs::UTOEvent>("uto_event_msg", 10000);

#ifdef DEBUG_V1
    _pPubMasterCmd = NULL;
    _pSubMasterCmd = NULL;
    _pPubCmdReply = NULL;
    _pSubCmdReply = NULL;
#else
    _pPubMasterCmd = new RTPSPublisher<MasterCmd>("/master_cmd");
    _pSubMasterCmd = new RTPSSubscriber<MasterCmd>("/master_cmd",&SwarmMasterCmd::masterCmdCallback,this); 

    _pPubCmdReply = new RTPSPublisher<CmdReply>("/master_cmd_reply");
    _pSubCmdReply = new RTPSSubscriber<CmdReply>("/master_cmd_reply", &SwarmMasterCmd::masterCmdReplyCallback,this);
#endif

    _pPubGSMasterCmd = new RTPSPublisher<MasterCmd>("/gs_master_cmd_resp");
    _pSubGSMasterCmd = new RTPSSubscriber<MasterCmd>("/gs_master_cmd",&SwarmMasterCmd::gsMasterCmdCallBack,this);

    _pthResendTimeoutMsgs = boost::shared_ptr<boost::thread>(new boost::thread(boost::bind(&SwarmMasterCmd::resendTimeoutMasterCmdLoop,this)));
}

SwarmMasterCmd::~SwarmMasterCmd(){

    if(_pPubMasterCmd){
        delete _pPubMasterCmd;
        _pPubMasterCmd = NULL;
    }

    if(_pSubMasterCmd){
        delete _pSubMasterCmd;
        _pSubMasterCmd = NULL;
    }

    if(_pPubGSMasterCmd){
        delete _pPubGSMasterCmd;
        _pPubGSMasterCmd = NULL;
    }

    if(_pSubGSMasterCmd){
        delete _pSubGSMasterCmd;
        _pSubGSMasterCmd = NULL;
    }

    if(_pPubCmdReply){
        delete _pPubCmdReply;
        _pPubCmdReply = NULL;
    }

    if(_pSubCmdReply){
        delete _pSubCmdReply;
        _pSubCmdReply = NULL;
    }

    if(_pthResendTimeoutMsgs){
        _pthResendTimeoutMsgs->interrupt();
        _pthResendTimeoutMsgs->join();
        _pthResendTimeoutMsgs.reset();
    }

   for(auto it = _robotID2MasterCmdQueue.begin(); it != _robotID2MasterCmdQueue.end(); it++){
        it->second.clearQueue();
    }
    _robotID2MasterCmdQueue.clear(); // or swap

    _barrierKey2BarrierRequestList.clear();

    _barrierKey2ConditionAndResult.clear();
}

void SwarmMasterCmd::gsMasterCmdCallBack(MasterCmd& aCmd){
    int32_t receID = aCmd.receID();
    int32_t sendID = aCmd.sendID();
    int32_t cmdNo = aCmd.cmdNo();
    if(sendID == _robotID) return;

#ifdef ACTOR_SWARM_LOG_OUT
    std::string logStr = std::to_string(MasterCmd::getCdrSerializedSize(aCmd)) + ",GSMasterCmd";
    SwarmDataStorage::instance()->logOut2File("netreceive",logStr);
 #endif  

	if(receID != _robotID && receID != RECE_ID_ALL_MEMBER) return;

	switch(aCmd.type()){
    case CMD_TYPE_PING_WITH_GS:{    //ping msg from gstation
        ROS_INFO("[SwarmMasterCmd] receive ping from g_station");
        if (aCmd.param1() == 0){// request
            sendPingResponseToGStation(sendID, cmdNo);
        }
        break;
    }
    case CMD_TYPE_FORCE_MASTER_WITH_GS:{ // force set master id
        ROS_INFO("[SwarmMasterCmd] receive force set masterID = %d", aCmd.param1());
#ifdef ACTOR_SWARM_LOG_OUT
        std::string logStr = "gstation force set master:" + std::to_string(aCmd.param1()) + ", type:" + std::to_string(aCmd.param2());
        SwarmDataStorage::instance()->logOut2File("event", logStr);
#endif
        // clear msg to old master
        int oldMasterID = SwarmDataStorage::instance()->getMasterID();
        if ( oldMasterID != aCmd.param1()){
            if(oldMasterID == _robotID)
                clearAllMasterCmds();
            else
                clearMasterCmdsForRobot(oldMasterID);
        }

        if(aCmd.param2() == FORCE){
            // set local state to KEEPING when force set
            SwarmDataStorage::instance()->setLocalRobotState(KEEPING);
            SwarmDataStorage::instance()->setMasterID(aCmd.param1(), FORCE);
        } else if(oldMasterID != aCmd.param1() || SwarmDataStorage::instance()->getMasterGenerateType() != aCmd.param2()){
            // when masterid changed or master generate type changed, then set local masterid
            if(aCmd.param1() == _robotID || SwarmDataStorage::instance()->getRobotStateByID(aCmd.param1()) == KEEPING){
                SwarmDataStorage::instance()->setLocalRobotState(KEEPING);
            } else {
                SwarmDataStorage::instance()->setLocalRobotState(VOTING);
            }
            SwarmDataStorage::instance()->setMasterID(aCmd.param1(), (EMasterGenerateType)aCmd.param2());
        }
        sendPingResponseToGStation(sendID, cmdNo, true);
        break;
    }
    case CMD_TYPE_ACTIVATE_ACTOR_WITH_GS:{
        std::string swarmName = gActorTagStore.swarmTag2Name(aCmd.param1());
        std::string actorName = gActorTagStore.actorTag2Name(swarmName, aCmd.param2());

        ROS_INFO("[SwarmMasterCmd] receive force activate actor %s", actorName.c_str());
#ifdef ACTOR_SWARM_LOG_OUT
        std::string logStr = "gstation force activate actor:" + actorName;
        SwarmDataStorage::instance()->logOut2File("event", logStr);
#endif
        sendActiveActorEvent(actorName);
        break;
    }
    case CMD_TYPE_YIELD_ACTOR_WITH_GS:{
        std::string swarmName = gActorTagStore.swarmTag2Name(aCmd.param1());
        std::string actorName = gActorTagStore.actorTag2Name(swarmName, aCmd.param2());

        ROS_INFO("[SwarmMasterCmd] receive force yield actor %s", actorName.c_str());
#ifdef ACTOR_SWARM_LOG_OUT
        std::string logStr = "gstation force yield actor:" + actorName;
        SwarmDataStorage::instance()->logOut2File("event", logStr);
#endif
        sendPauseActorEvent(actorName);
        break;
    }
    case CMD_TYPE_SWITCH_ACTOR_WITH_GS:{
        std::string swarmName = gActorTagStore.swarmTag2Name(aCmd.param1());
        std::string yieldActorName = gActorTagStore.actorTag2Name(swarmName, aCmd.param2());
        std::string activateActorName = gActorTagStore.actorTag2Name(swarmName, aCmd.param3());

        ROS_INFO("[SwarmMasterCmd] receive force switch actor %s->%s", yieldActorName.c_str(), activateActorName.c_str());
#ifdef ACTOR_SWARM_LOG_OUT
        std::string logStr = "gstation force switch actor:" + yieldActorName + " to " + activateActorName;
        SwarmDataStorage::instance()->logOut2File("event", logStr);
#endif
        sendSwitchActorEvent(yieldActorName, activateActorName);
        break;
    }
    case CMD_TYPE_START_TASK_WITH_GS:{
        std::string swarmName = gActorTagStore.swarmTag2Name(aCmd.param2());
#ifdef ACTOR_SWARM_LOG_OUT
        std::string logStr = "gstation start task:" + swarmName;
        SwarmDataStorage::instance()->logOut2File("event", logStr);
#endif
        actor_msgs::UTOEvent event;
        event.eventName = "startup";
        event.type = START_TASK_MSG;
        event.currentActor = swarmName;

        boost::unique_lock<boost::mutex> lock(_pubUTOEventMutex);
        _pubUTOEvent.publish(event);
        lock.unlock();

        sendReplyToGStation(sendID, cmdNo);
    }
    default: break;
    }
}

void SwarmMasterCmd::masterCmdReplyCallback(CmdReply& aCmd){
    int32_t receID = aCmd.receID();
    int32_t sendID = aCmd.sendID();
    int32_t cmdNo = aCmd.cmdNo();
    if(sendID == _robotID) return;

#ifdef ACTOR_SWARM_LOG_OUT
    std::string logStr = std::to_string(CmdReply::getCdrSerializedSize(aCmd)) + ",CmdReply";
    SwarmDataStorage::instance()->logOut2File("netreceive",logStr);
 #endif  

	if(receID != _robotID && receID != RECE_ID_ALL_MEMBER) return;

    boost::unique_lock<boost::shared_mutex> cmdNoWriteLock(_cmdNo2RobotReplyMutex);
    if (_cmdNo2RobotReply.count(cmdNo) > 0 && _cmdNo2RobotReply[cmdNo].count(sendID) > 0)
        _cmdNo2RobotReply[cmdNo][sendID] = true;
}

// when robot join, assign a larger formation pos
void SwarmMasterCmd::addRobotToFormation(const int32_t aRobotID){
    if (!_reassignSwitch) return;

    if(INVALID_ROBOTPOS == SwarmDataStorage::instance()->getRobotPosByID(aRobotID)){
        int nextPos = SwarmDataStorage::instance()->getRobotCount();
        SwarmDataStorage::instance()->setRobotPos(aRobotID, nextPos);
        ROS_INFO("[SwarmMasterCmd] add robot [%d],pos [%d]", aRobotID, nextPos);
    }
}

//spread formation pos when needed
//set formation pos when Leader/Follower loses, DA required
//broadcast the formation pos info, saved by all members and set by target member
//when leader loses, send the formation pos data firstly and then send the tranfer actor cmd
void SwarmMasterCmd::reassignFormation(){
    if(!_reassignSwitch) return;
    
    std::map<int, int> mapRobotPos; // pos->robotID
    SwarmDataStorage::instance()->getAllRobotPos(mapRobotPos);

    //if invalid_pos exists, then set a larger pos for these robots whose pos is -1
    if (mapRobotPos.count(INVALID_ROBOTPOS) > 0) {
    #ifdef ACTOR_SWARM_LOG_OUT
        SwarmDataStorage::instance()->logOut2File("event", "INVALID POS exists when reassign");
    #endif

        mapRobotPos.erase(INVALID_ROBOTPOS);
        std::vector<int> robots;
        SwarmDataStorage::instance()->getRobotIDs(robots);
        for (int i = 0; i < robots.size(); i++) {
            //if not find, set a larger pos
            std::map<int, int>::iterator it = mapRobotPos.begin();
            for (; it != mapRobotPos.end(); it++)
            {
                if (it->second == robots[i])
                    break;
            }
            if (it == mapRobotPos.end())
                mapRobotPos[robots.size() + i] = robots[i];
        }
    }

    // reassign robot position in formation
    for (int pos = 0; pos < mapRobotPos.size(); pos++) {
        //if pos not exist, reassign and send switch cmd
        if (mapRobotPos.count(pos) != 0) continue; 

        // if this position has no robot,then find a robot which is in the same side and its pos > pos
        std::map<int32_t, int32_t>::iterator nextRobot = mapRobotPos.upper_bound(pos);
        //IF NOT FOUND, SOMETHING WROING
        if (nextRobot == mapRobotPos.end()) {
            ROS_FATAL("[SwarmMasterCmd] no larger pos for %d", pos);
            return;
        }

        // if the robot in the same side ,if has none ,then use the nearest one
        for (; nextRobot != mapRobotPos.end(); nextRobot++) {
            if (pos%2 == nextRobot->first%2) {
                break;
            }
        } 

        //if not find a plat with the same side, find the min pos 
        if (nextRobot == mapRobotPos.end()) {
            nextRobot = mapRobotPos.upper_bound(pos);
        }

        // replace
        mapRobotPos[pos] = nextRobot->second;
        mapRobotPos.erase(nextRobot);  

         //log to file
        #ifdef ACTOR_SWARM_LOG_OUT
            std::string logStr = "reassign pos->id:";
            logStr.append(std::to_string(pos))
            .append(std::to_string(mapRobotPos[pos]))
            .append(", islocal=").append(std::to_string(mapRobotPos[pos] == _robotID));
            SwarmDataStorage::instance()->logOut2File("event",logStr);
        #endif          
        
        sendFormationSwitchCmd(mapRobotPos[pos], pos);
        sendFormationSaveCmd(mapRobotPos[pos], pos);
    }

    SwarmDataStorage::instance()->resetRobotPos(mapRobotPos);
}

void SwarmMasterCmd::sendFormationSwitchCmd(const int32_t aDestRobotID, const int32_t aPos){
    std::string startActorName = aPos == 0 ? _lostActorName : _candidateActorName;
    std::string stopActorName = aPos == 0 ? _candidateActorName : _lostActorName;
    std::string swarmName = SwarmDataStorage::instance()->getSwarmNameByID(_robotID);

    // if local, switch lcoal actor
    if (aDestRobotID == _robotID) {
        sendPauseActorEvent(stopActorName);
        sendActiveActorEvent(startActorName);
    } else { // else send switch msg
        MasterCmd *cmd = new MasterCmd();
        cmd->sendID(_robotID);
        cmd->receID(aDestRobotID);
        cmd->type(CMD_TYPE_SET_FORMATION_SWITCH);
        cmd->param1(gActorTagStore.swarmName2Tag(swarmName));
        cmd->param2(gActorTagStore.actorName2Tag(swarmName, startActorName));
        cmd->param3(0);
        cmd->param4(aPos);
        cmd->param5(gActorTagStore.actorName2Tag(swarmName, stopActorName));

        sendMasterCmd(cmd);
    }
}

void SwarmMasterCmd::sendFormationSaveCmd(const int32_t aDestRobotID, const int32_t aPos){
    std::string startActorName = aPos == 0 ? _lostActorName : _candidateActorName;
    std::string swarmName = SwarmDataStorage::instance()->getSwarmNameByID(_robotID);

    // broadcast the cmd to inform pos info
    MasterCmd *cmd = new MasterCmd();
    cmd->sendID(_robotID);
    cmd->receID(RECE_ID_ALL_MEMBER);
    cmd->type(CMD_TYPE_SET_SAVE_FORMATION);
    cmd->param1(gActorTagStore.swarmName2Tag(swarmName));
    cmd->param2(gActorTagStore.actorName2Tag(swarmName, startActorName));
    cmd->param3(0);
    cmd->param4(aPos);
    cmd->param5(aDestRobotID);

    sendMasterCmd(cmd);
}

// send by master robot
void SwarmMasterCmd::sendIamMasterCmd(const int32_t aOldMasterID, const int32_t aDestRobotID){
    
    ROS_INFO("[SwarmMasterCmd] sendIamMasterCmd old masterID is [%d]", aOldMasterID);
        MasterCmd *cmd = new MasterCmd();
        cmd->sendID(_robotID);
        cmd->receID(aDestRobotID);
        cmd->type(CMD_TYPE_SWITCH_MASTER);
        cmd->param1(aOldMasterID);
        sendMasterCmd(cmd);
    }

void SwarmMasterCmd::sendPingCmd(const int32_t aDestRobotID, const bool bReply){
    ROS_INFO("[SwarmMasterCmd] sendPingCmd to [%d]", aDestRobotID);
        MasterCmd *cmd = new MasterCmd();
        cmd->sendID(_robotID);
        cmd->receID(aDestRobotID);
        cmd->type(CMD_TYPE_PING);
        cmd->param1(bReply);
        sendMasterCmd(cmd);
    }

void SwarmMasterCmd::sendPingResponseToGStation(const int32_t aStationID, const int32_t aCmdNo, bool aIsInitiative){
    MasterCmd cmd;
    cmd.sendID(_robotID);
    cmd.receID(aStationID);
    cmd.cmdNo(aCmdNo);
    cmd.type(CMD_TYPE_PING_WITH_GS);
    cmd.param1(1);
    cmd.param2(SwarmDataStorage::instance()->getLocalRobotState());
    cmd.param3(SwarmDataStorage::instance()->getMasterID());
    cmd.param4(SwarmDataStorage::instance()->getMasterGenerateType());
    cmd.param5(aIsInitiative ? 1 : 0);

    boost::unique_lock<boost::mutex> lock(_pPubGSMasterCmdMutex);
    _pPubGSMasterCmd->publish(cmd);
    lock.unlock();

#ifdef ACTOR_SWARM_LOG_OUT
    std::string logStr = std::to_string(MasterCmd::getCdrSerializedSize(cmd)) + ",MasterCmd2GS";
    SwarmDataStorage::instance()->logOut2File("netsend", logStr);
#endif
}


//send swarm info to member new joined, called by master
//only send the information which is necessary, formation pos for now
void SwarmMasterCmd::sendInfo2RobotNewJoin(const int32_t aRootID){ 
    if(!_reassignSwitch) return;

    if(SwarmDataStorage::instance()->getSwarmNameByID(_robotID).empty())return;
    
    ROS_INFO("[SwarmMasterCmd] sendInfo2RobotNewJoin to robot[%d]", aRootID);
    std::vector<int> robotIDs;
    SwarmDataStorage::instance()->getRobotIDs(robotIDs);

    std::string swarmName = "", actorName = "", formationType = "";
    int formationPos = INVALID_ROBOTPOS;
    for (int i = 0; i < robotIDs.size(); i++) {
        //only send formation pos of other robots
        if (aRootID == robotIDs[i])
            continue;
        swarmName.clear();
        actorName.clear();
        formationType.clear();
        formationPos = INVALID_ROBOTPOS;
        if (SwarmDataStorage::instance()->getRobotPosByID(robotIDs[i], swarmName, actorName, formationType, formationPos)) {
            MasterCmd *cmd = new MasterCmd();
            cmd->sendID(_robotID);
            cmd->receID(aRootID);
            cmd->type(CMD_TYPE_SET_SAVE_FORMATION);
            cmd->param1(gActorTagStore.swarmName2Tag(swarmName));
            cmd->param2(gActorTagStore.actorName2Tag(swarmName, actorName));
            cmd->param3(0);
            cmd->param4(formationPos);
            cmd->param5(robotIDs[i]);
            sendMasterCmd(cmd);
        }
    }
}

// send by member robot, also handle from master itself
void SwarmMasterCmd::sendSwarmActorRequestCmd(const std::string& aSendActorName, const std::string& aActorName, int aType){
    if(SwarmDataStorage::instance()->getLocalRobotState() == VOTING){
        ROS_INFO("[SwarmMasterCmd] sendSwarmActorRequestCmd failed, msg: VOTING state");
#ifdef ACTOR_SWARM_LOG_OUT
        std::string logStr = "sendSwarmActorRequestCmd";
        logStr.append(" actorName=").append(aActorName).append(" failed, msg: VOTING state");
        SwarmDataStorage::instance()->logOut2File("warn", logStr);
#endif
        return;
    }

    ROS_INFO("[SwarmMasterCmd] sendSwarmActorRequestCmd, aSendActorName=%s, aActorName=%s, type=%d", aSendActorName.c_str(), aActorName.c_str(), aType);
    int masterID = SwarmDataStorage::instance()->getMasterID();
    std::string swarmName = SwarmDataStorage::instance()->getSwarmNameByID(_robotID);
    //if receive request from master itself
    if(_robotID == masterID){
        ROS_WARN("[SwarmMasterCmd] receive swarm actor request: local is master.");
        if(handleSwarmActorRequest(_robotID, aType, swarmName, aSendActorName, aActorName)){
            switch (aType) {
                case REQUEST_ACTOR_TYPE_ACTIVATE:
                    sendActiveActorEvent(aActorName);
                    ROS_INFO("[SwarmMasterCmd] recvRequestActor answer, master allowd activate actor [%s].", aActorName.c_str());
                    break;
                case REQUEST_ACTOR_TYPE_SWITCH:
                    sendSwitchActorEvent(aSendActorName, aActorName);
                    ROS_INFO("[SwarmMasterCmd] recvRequestActor answer, master allowd switch to actor [%s].", aActorName.c_str());
                    break;
                default:
                    break;
            } 
        }
    }else{
        //send request cmd
        MasterCmd *cmd = new MasterCmd();
        cmd->sendID(_robotID);
        cmd->receID(masterID);
        cmd->type(CMD_TYPE_REQUEST_ACTOR);
        cmd->param1(aType);
        cmd->param2(gActorTagStore.swarmName2Tag(swarmName));
        cmd->param3(gActorTagStore.actorName2Tag(swarmName, aSendActorName));
        cmd->param4(gActorTagStore.actorName2Tag(swarmName, aActorName));

        sendMasterCmd(cmd);
    }
}

//handle swarm actor request cmd
bool SwarmMasterCmd::handleSwarmActorRequest(const int32_t aSendID, const int aType,  const std::string& aSwarmName, const std::string& aSendActorName, const std::string& aActorName){
    ROS_INFO("[SwarmMasterCmd] recvRequestActor request,robot[%d] need actor[%s]", aSendID, aActorName.c_str());
    
    // request actor cmd in 10s, be process as the same cmd
    //compensate the time (send cmd + activate/switch actor + broadcast running actors through stigmergy)
    boost::shared_lock<boost::shared_mutex> rlock(_actorName2RequestResultMutex);
    if(_actorName2RequestResult.count(aActorName) == 0){
        _actorName2RequestResult[aActorName].first = ros::Time::now();
    }else{
        ros::Time now = ros::Time::now();
        int seconds = (now - _actorName2RequestResult[aActorName].first).toSec();
        if(seconds >= 10){
            _actorName2RequestResult[aActorName].first = now;
            _actorName2RequestResult[aActorName].second.clear();
        }
    }
    
    // store robotID who runs the actor, and reject other robot if actor reached on max Count
    bool reqRet = true;
    std::string curSwarmName = SwarmDataStorage::instance()->getSwarmNameByID(_robotID);
    if(curSwarmName.empty() || curSwarmName.compare(aSwarmName) != 0 || !SwarmDataStorage::instance()->isActorExist(aSwarmName, aActorName)){
        reqRet = false;
    } else if(std::find(_actorName2RequestResult[aActorName].second.begin(), _actorName2RequestResult[aActorName].second.end(), aSendID) == _actorName2RequestResult[aActorName].second.end()) {
        int runActorCount = SwarmDataStorage::instance()->getSwarmActorCount(aSwarmName, aActorName);
        int maxActorCount = SwarmDataStorage::instance()->getSwarmActorMaxCount(aSwarmName, aActorName);
        if (runActorCount < _actorName2RequestResult[aActorName].second.size())
            runActorCount = _actorName2RequestResult[aActorName].second.size();
        if(runActorCount < maxActorCount){
            _actorName2RequestResult[aActorName].second.push_back(aSendID);
        } else {
            reqRet = false;
        }
    }

    return reqRet;
}

BarrierResult SwarmMasterCmd::sendBarrierRequestCmd(GlobalBarrierKey& aBarrierKey, const std::string& anActorName, int aWaitCount, const short aTimeout, int &anIndex){
    // check barrierKey
    if(aBarrierKey.getSwarmName().empty() && (!anActorName.empty() || !aBarrierKey.getActorName().empty())){
        aBarrierKey.setSwarmName(SwarmDataStorage::instance()->getSwarmNameByID(_robotID));
    }

    if(!aBarrierKey.getSwarmName().empty() && gActorTagStore.swarmName2Tag(aBarrierKey.getSwarmName()) == -1){
        ROS_WARN("[SwarmMasterCmd] sendBarrierRequestCmd failed, invalid swarmName %s", aBarrierKey.getSwarmName().c_str());
        return NO;
    }

    if(!aBarrierKey.getActorName().empty() && gActorTagStore.actorName2Tag(aBarrierKey.getSwarmName(), aBarrierKey.getActorName()) == -1){
        ROS_WARN("[SwarmMasterCmd] sendBarrierRequestCmd failed, invalid actorName %s", aBarrierKey.getActorName().c_str());
        return NO;
    }

    if(!aBarrierKey.getPluginName().empty() && gActorTagStore.pluginName2Tag(aBarrierKey.getPluginName()) == -1){
        ROS_WARN("[SwarmMasterCmd] sendBarrierRequestCmd failed, invalid pluginName %s", aBarrierKey.getPluginName().c_str());
        return NO;
    }

    // calculate left wait time when in voting state
    int waitTime = aTimeout;
    if(SwarmDataStorage::instance()->getLocalRobotState() == VOTING){
        ROS_INFO("[SwarmMasterCmd] sendBarrierRequestCmd will be put off, msg: VOTING state");
        boost::system_time startTime = boost::get_system_time();
        if(!SwarmDataStorage::instance()->waitForLocalKeepingState(aTimeout)){
            ROS_INFO("[SwarmMasterCmd] sendBarrierRequestCmd has timed out, msg: VOTING state");
            return TIMEOUT;
        }
        boost::system_time endTime = boost::get_system_time();
        waitTime = waitTime - (endTime - startTime).seconds();

#ifdef ACTOR_SWARM_LOG_OUT
        std::string logStr = "sendBarrierRequestCmd";
        logStr.append(" barrierKey=").append(std::to_string(aBarrierKey.getBarrierKey())).append(" actorName=").append(anActorName).append(" left time= ").append(std::to_string(waitTime)).append(", msg: VOTING state");
        SwarmDataStorage::instance()->logOut2File("warn", logStr);
#endif
    }

    // if barrier key and actor name already exists
    boost::unique_lock<boost::shared_mutex> wlock(_barrierKey2ConditionAndResultMutex);
    boost::shared_ptr<boost::condition_variable_any> cond(new boost::condition_variable_any());
    if((_barrierKey2ConditionAndResult.count(aBarrierKey)>0) && (_barrierKey2ConditionAndResult[aBarrierKey].count(anActorName)>0)){
        ROS_INFO("A barrier with the same barrier key and actorname already exists, return NO");
        cond.reset();
        return BarrierResult::NO;
    }else{   
        // create condition_variable for aBarrierKey
        BarrierResultDataIndexCond resultCond;
        resultCond._cond = cond;
        _barrierKey2ConditionAndResult[aBarrierKey].insert(std::pair<std::string, BarrierResultDataIndexCond>(anActorName, resultCond));
    }
    wlock.unlock();

    // if the barrier is on master node, then process it 
    int masterID = SwarmDataStorage::instance()->getMasterID();
    if(_robotID == masterID){
        // when process local barrier, all barriers may reach to synchronous state, this method should return immediately
        BarrierResult localRet = handleBarrierRequest(aBarrierKey, anActorName, aWaitCount, _robotID, anIndex);
        if(localRet == YES || localRet == OPTIONALYES){
            boost::unique_lock<boost::shared_mutex> wlock(_barrierKey2ConditionAndResultMutex);
            _barrierKey2ConditionAndResult[aBarrierKey].at(anActorName)._cond.reset();
            _barrierKey2ConditionAndResult[aBarrierKey].erase(anActorName);
            if(_barrierKey2ConditionAndResult[aBarrierKey].empty()) _barrierKey2ConditionAndResult.erase(aBarrierKey);
            return localRet;
        }
    } else { // if the barrier is on member node, then send barrier cmd to the master node
        MasterCmd *cmd = new MasterCmd();
        cmd->sendID(_robotID);
        cmd->receID(masterID);
        cmd->type(CMD_TYPE_BARRIER_REQUEST);
        cmd->param1(aBarrierKey.getSwarmName().empty() ? -1 : gActorTagStore.swarmName2Tag(aBarrierKey.getSwarmName()));
        cmd->param2(aBarrierKey.getActorName().empty() ? -1 : gActorTagStore.actorName2Tag(aBarrierKey.getSwarmName(), aBarrierKey.getActorName()));
        cmd->param3(aBarrierKey.getPluginName().empty() ? -1 : gActorTagStore.pluginName2Tag(aBarrierKey.getPluginName()));
        cmd->param4(aBarrierKey.getBarrierKey());
        cmd->param5(aWaitCount);
        cmd->param6().push_back(anActorName.empty() ? -1 : gActorTagStore.actorName2Tag(aBarrierKey.getSwarmName(), anActorName));
        sendMasterCmd(cmd);
    }

    // start wait signal
    boost::shared_lock<boost::shared_mutex> rlock(_barrierKey2ConditionAndResultMutex);
    BarrierResult ret = NO;
    if(cond->timed_wait(rlock, boost::get_system_time() + boost::posix_time::seconds(waitTime))){
        ret = _barrierKey2ConditionAndResult[aBarrierKey][anActorName]._barrierResult._result;
        anIndex = _barrierKey2ConditionAndResult[aBarrierKey][anActorName]._barrierResult._index;
    } else {
        ret = TIMEOUT;
    }
    rlock.unlock();

    //Q: erase condition and result?
    wlock.lock();
    _barrierKey2ConditionAndResult[aBarrierKey].at(anActorName)._cond.reset();
    _barrierKey2ConditionAndResult[aBarrierKey].erase(anActorName);
    if(_barrierKey2ConditionAndResult[aBarrierKey].empty()) _barrierKey2ConditionAndResult.erase(aBarrierKey);
    return ret;
}

BarrierResult SwarmMasterCmd::handleBarrierRequest(GlobalBarrierKey& aBarrierKey, const std::string& anActorName, const int aWaitCount, const int aRobotID, int &anIndex){
    int waitCount = aWaitCount;
    if(waitCount == BARRIER_WAIT_COUNT_ALL_ROBOTS) {
        waitCount = SwarmDataStorage::instance()->getRobotCount();
    }

    boost::unique_lock<boost::shared_mutex> wlock(_barrierKey2BarrierRequestListMutex);
    // check barrier key
    if(_barrierKey2BarrierRequestList.count(aBarrierKey) == 0){
        _barrierKey2BarrierRequestList[aBarrierKey]._firstRequestTime = ros::Time::now();
        if(_barrierKey2BarrierRequestList.size() > 100){
            _barrierKey2BarrierRequestList.erase(_barrierKey2BarrierRequestList.begin());
        }
    }

    // if robot and actor repeated request the same barrier, then reply it
    RobotIDandActorName robotActor(aRobotID,anActorName);
    BarrierResult ret = NO;
    if(_barrierKey2BarrierRequestList[aBarrierKey]._requestRobots.count(robotActor) > 0){
        ret = _barrierKey2BarrierRequestList[aBarrierKey]._requestRobots[robotActor]._result;        
        if(ret != NO){
            anIndex = _barrierKey2BarrierRequestList[aBarrierKey]._requestRobots[robotActor]._index;
            sendBarrierResponse(aBarrierKey, aRobotID, anActorName, anIndex*10+ret);
        }
           
    } else {
        _barrierKey2BarrierRequestList[aBarrierKey]._requestRobots[robotActor]._result = NO; // init ret with NO value
        if(_barrierKey2BarrierRequestList[aBarrierKey]._isReplyed)
            ret = OPTIONALYES;
        else if(_barrierKey2BarrierRequestList[aBarrierKey]._requestRobots.size() >= waitCount) {
            ret = YES;
        } 
    
        // needn't notify those robots who request this barrier
        int dataIndex = 0;
        switch(ret){
            case YES: { // nodify all robots in list
                for(auto it = _barrierKey2BarrierRequestList[aBarrierKey]._requestRobots.begin(); it !=  _barrierKey2BarrierRequestList[aBarrierKey]._requestRobots.end(); it++){
                    dataIndex++;
                    it->second._result = ret;
                    it->second._index = dataIndex;
                    if((aRobotID==it->first._robotID)&&(anActorName.compare(it->first._actorName)==0)) anIndex = dataIndex;
                    sendBarrierResponse(aBarrierKey, it->first._robotID, it->first._actorName, dataIndex*10+ret);
                    
                }
                _barrierKey2BarrierRequestList[aBarrierKey]._isReplyed = true;
            }
            break;
            case OPTIONALYES: { // notify the robot only
                _barrierKey2BarrierRequestList[aBarrierKey]._requestRobots[robotActor]._result = ret;
                sendBarrierResponse(aBarrierKey, aRobotID, anActorName, ret);   
            }
            break;
            default: break;
        }
    }

    //TODO temp add formation content for DA
    if (anActorName.compare(_candidateActorName) == 0 && aWaitCount == 1 && ret == YES) {
        _reassignSwitch = true;
        SwarmDataStorage::instance()->setRobotPos(aRobotID, 0);
        sendFormationSwitchCmd(aRobotID, 0);
        sendFormationSaveCmd(aRobotID, 0);
        reassignFormation();
    } else if (anActorName.compare(_lostActorName) == 0 ) {
        _reassignSwitch = false;
        std::map<int,int> tmp;
        SwarmDataStorage::instance()->resetRobotPos(tmp);

        // close reassignSwitch
        MasterCmd *cmd = new MasterCmd();
        cmd->sendID(_robotID);
        cmd->receID(RECE_ID_ALL_MEMBER);
        cmd->type(CMD_TYPE_SET_SAVE_FORMATION);
        cmd->param1(0);
        cmd->param2(0);
        cmd->param3(0);
        cmd->param4(INVALID_ROBOTPOS);
        cmd->param5(INVALID_ROBOTID);

        sendMasterCmd(cmd);
    }

    return ret;
}

void SwarmMasterCmd::sendBarrierResponse(GlobalBarrierKey& aBarrierKey, const int aDestID, const std::string& anActorName, const int aResultDataIndex){
    ROS_INFO("[SwarmMasterCmd] notify barrier result, barrierkey=%d, robotid=%d, result and dataIndex=%d", aBarrierKey.getBarrierKey(), aDestID, aResultDataIndex);
    if(aDestID != _robotID){
        MasterCmd *cmd = new MasterCmd();
        cmd->sendID(_robotID);
        cmd->receID(aDestID);
        cmd->type(CMD_TYPE_BARRIER_RESPONSE);
        cmd->param1(aBarrierKey.getSwarmName().empty() ? -1 : gActorTagStore.swarmName2Tag(aBarrierKey.getSwarmName()));
        cmd->param2(aBarrierKey.getActorName().empty() ? -1 : gActorTagStore.actorName2Tag(aBarrierKey.getSwarmName(), aBarrierKey.getActorName()));
        cmd->param3(aBarrierKey.getPluginName().empty() ? -1 : gActorTagStore.pluginName2Tag(aBarrierKey.getPluginName()));
        cmd->param4(aBarrierKey.getBarrierKey());
        cmd->param5(aResultDataIndex);
        cmd->param6().push_back(anActorName.empty() ? -1 : gActorTagStore.actorName2Tag(aBarrierKey.getSwarmName(), anActorName));
        sendMasterCmd(cmd);
    } else {
        boost::unique_lock<boost::shared_mutex> wlock(_barrierKey2ConditionAndResultMutex);
        if((_barrierKey2ConditionAndResult.count(aBarrierKey) != 0)&&(_barrierKey2ConditionAndResult[aBarrierKey].count(anActorName)!=0)){
            _barrierKey2ConditionAndResult[aBarrierKey].at(anActorName)._barrierResult._result = (BarrierResult)(aResultDataIndex%10);
            _barrierKey2ConditionAndResult[aBarrierKey].at(anActorName)._barrierResult._index = aResultDataIndex/10;
            _barrierKey2ConditionAndResult[aBarrierKey].at(anActorName)._cond->notify_one();
        }
    }
#ifdef ACTOR_SWARM_LOG_OUT
    std::string logStr = "sendBarrierResponse ";
    logStr.append(std::to_string(_robotID)).append("-->").append(std::to_string(aDestID)).append(" barrierKey:").append(std::to_string(aBarrierKey.getBarrierKey())).append(" actorName:").append(anActorName).append(" requestResult:").append(std::to_string(aResultDataIndex));
    SwarmDataStorage::instance()->logOut2File("event", logStr);
#endif
}

void SwarmMasterCmd::sendActivateActorCmd(const int aDestID, const std::string& aSwarmName, const std::string& anActorName){
    ROS_INFO("[SwarmMasterCmd] sendActivateActorCmd, robotid=%d, activate %s", aDestID, anActorName.c_str());
    if(aDestID != _robotID){
        MasterCmd *cmd = new MasterCmd();
        cmd->sendID(_robotID);
        cmd->receID(aDestID);
        cmd->type(CMD_TYPE_ACTIVATE_ACTOR);
        cmd->param1(aSwarmName.empty() ? -1 : gActorTagStore.swarmName2Tag(aSwarmName));
        cmd->param2(anActorName.empty() ? -1 : gActorTagStore.actorName2Tag(aSwarmName, anActorName));
        sendMasterCmd(cmd);
    } else {
        sendActiveActorEvent(anActorName);
    }
#ifdef ACTOR_SWARM_LOG_OUT
    std::string logStr = "sendActivateActorCmd ";
    logStr.append(std::to_string(_robotID)).append("-->").append(std::to_string(aDestID)).append(" actorName:").append(anActorName);
    SwarmDataStorage::instance()->logOut2File("event", logStr);
#endif
}

void SwarmMasterCmd::sendSwitchActorCmd(const int aDestID, const std::string& aSwarmName, const std::string& anActorName, const std::string& aTargetActorName){
    ROS_INFO("[SwarmMasterCmd] sendSwitchActorCmd, robotid=%d, %s switch to %s", aDestID, anActorName.c_str(), aTargetActorName.c_str());
    if(aDestID != _robotID){
        MasterCmd *cmd = new MasterCmd();
        cmd->sendID(_robotID);
        cmd->receID(aDestID);
        cmd->type(CMD_TYPE_SWITCH_ACTOR);
        cmd->param1(aSwarmName.empty() ? -1 : gActorTagStore.swarmName2Tag(aSwarmName));
        cmd->param2(anActorName.empty() ? -1 : gActorTagStore.actorName2Tag(aSwarmName, anActorName));
        cmd->param3(aTargetActorName.empty() ? -1 : gActorTagStore.actorName2Tag(aSwarmName, aTargetActorName));
        sendMasterCmd(cmd);
    } else {
        sendSwitchActorEvent(anActorName, aTargetActorName);
    }
#ifdef ACTOR_SWARM_LOG_OUT
    std::string logStr = "sendSwitchActorCmd ";
    logStr.append(std::to_string(_robotID)).append("-->").append(std::to_string(aDestID)).append(" actorName:").append(anActorName).append(" to ").append(aTargetActorName);
    SwarmDataStorage::instance()->logOut2File("event", logStr);
#endif
}

// return if the cmd replyed
bool SwarmMasterCmd::sendMasterCmdAndGetReply(MasterCmd* pCmd){
    bool reply = true;
    int32_t cmdNo = pCmd->cmdNo();
    boost::shared_lock<boost::shared_mutex> cmdNoReadLock(_cmdNo2RobotReplyMutex);
    for (auto it = _cmdNo2RobotReply[cmdNo].begin(); it != _cmdNo2RobotReply[cmdNo].end(); it++) {
        if (!it->second) {
            reply = false;
            break;
        }
    }
    cmdNoReadLock.unlock();

    if (!reply) {
        //ROS_INFO("[SwarmMasterCmd] sendMasterCmdAndGetReply, cmdNo=%d, type=%d, dest=%d", cmdNo, pCmd->type(), pCmd->receID());
        #ifdef ACTOR_SWARM_LOG_OUT
            std::string logStr = "sendMasterCmd";
            logStr.append(std::to_string(pCmd->sendID())).append("-->").append(std::to_string(pCmd->receID()))
            .append(" cmdNo:").append(std::to_string(pCmd->cmdNo()))
            .append(" cmdType:").append(std::to_string(pCmd->type()));
            SwarmDataStorage::instance()->logOut2File("event",logStr);

            logStr = std::to_string(MasterCmd::getCdrSerializedSize(*pCmd)) + ",MasterCmd";
            SwarmDataStorage::instance()->logOut2File("netsend", logStr);
        #endif
        boost::unique_lock<boost::mutex> pubCmdLock(_pubMutex);
        _pPubMasterCmd->publish(*pCmd);
        pubCmdLock.unlock();
    }

    return reply;
}

void SwarmMasterCmd::sendMasterCmd(MasterCmd* pCmd){
#ifdef DEBUG_V1
    delete pCmd;
    pCmd = NULL;
    return;
#else
#ifdef DEBUG_V2
    if (pCmd->type() != CMD_TYPE_BARRIER_REQUEST && pCmd->type() != CMD_TYPE_BARRIER_RESPONSE) {
        delete pCmd;
        pCmd = NULL;
        return;
    }
#endif
    boost::unique_lock<boost::shared_mutex> cmdNoWriteLock(_cmdNo2RobotReplyMutex);
    _masterCmdNo++;
    pCmd->cmdNo(_masterCmdNo);
    if (pCmd->receID() == RECE_ID_ALL_MEMBER) {
        std::vector<int> vRobotIDs;
        SwarmDataStorage::instance()->getRobotIDs(vRobotIDs);
        for (std::vector<int>::iterator it = vRobotIDs.begin(); it != vRobotIDs.end(); it++) {
            if (_robotID != *it)
                _cmdNo2RobotReply[_masterCmdNo].insert(std::pair<int32_t, bool>(*it, false));
        }
    } else {
        _cmdNo2RobotReply[_masterCmdNo].insert(std::pair<int32_t, bool>(pCmd->receID(), false));
    }
    cmdNoWriteLock.unlock();

    //push back to the cmd msg queue
    boost::unique_lock<boost::shared_mutex> wlock(_robotID2MasterCmdQueueMutex);
    ROS_INFO("[SwarmMasterCmd] Add a cmd to msg queue to send cmdNo %d, receId %d, cmdType %d", pCmd->cmdNo(),pCmd->receID(),pCmd->type());
    if(_robotID != pCmd->receID())
        _robotID2MasterCmdQueue[pCmd->receID()].pushMsg(pCmd);
#endif
}

void SwarmMasterCmd::sendMasterCmdReply(const int aDestID, const int cmdNo){
    CmdReply reply;
    reply.sendID(_robotID);
    reply.receID(aDestID);
    reply.cmdNo(cmdNo);
    
    boost::unique_lock<boost::mutex> lock(_pPubCmdReplyMutex);
    _pPubCmdReply->publish(reply);
    lock.unlock();

#ifdef ACTOR_SWARM_LOG_OUT
        std::string logStr = std::to_string(CmdReply::getCdrSerializedSize(reply)) + ",MasterCmd_Reply";
        SwarmDataStorage::instance()->logOut2File("netsend", logStr);
        
        logStr.clear();
        logStr.append("sendcmdCmdReply ").append(std::to_string(_robotID)).append("-->").append(std::to_string(aDestID)).append(" cmdNo:").append(std::to_string(cmdNo));
        SwarmDataStorage::instance()->logOut2File("event", logStr);
#endif
}

void SwarmMasterCmd::sendReplyToGStation(const int32_t aStationID, const int32_t aCmdNo) {
    MasterCmd cmd;
    cmd.sendID(_robotID);
    cmd.receID(aStationID);
    cmd.cmdNo(aCmdNo);
    cmd.type(CMD_TYPE_REPLY_WITH_GS);

    boost::unique_lock<boost::mutex> lock(_pPubGSMasterCmdMutex);
    _pPubGSMasterCmd->publish(cmd);
    lock.unlock();

#ifdef ACTOR_SWARM_LOG_OUT
    std::string logStr = std::to_string(MasterCmd::getCdrSerializedSize(cmd)) + ",MasterCmd2GS";
    SwarmDataStorage::instance()->logOut2File("netsend", logStr);
#endif
}

void SwarmMasterCmd::masterCmdCallback(MasterCmd& aCmd){
    int32_t receID = aCmd.receID();
    int32_t sendID = aCmd.sendID();
    int32_t cmdNo = aCmd.cmdNo();
    int32_t masterID = SwarmDataStorage::instance()->getMasterID();

    if(sendID == _robotID) return;

 #ifdef ACTOR_SWARM_LOG_OUT
    std::string logStr = std::to_string(MasterCmd::getCdrSerializedSize(aCmd)) + ",MasterCmd";
    SwarmDataStorage::instance()->logOut2File("netreceive",logStr);
 #endif  

    if(receID != _robotID && receID != RECE_ID_ALL_MEMBER) // this robot is not target
        return;

    //log to file
    #ifdef ACTOR_SWARM_LOG_OUT
        logStr = "recvMasterCmd";
        logStr.append(std::to_string(sendID)).append("-->").append(std::to_string(receID))
        .append(" cmdNo:").append(std::to_string(cmdNo))
        .append(" cmdType:").append(std::to_string(aCmd.type()));
        if((aCmd.type() == CMD_TYPE_BARRIER_REQUEST || aCmd.type() == CMD_TYPE_BARRIER_RESPONSE) && aCmd.param6().size() > 0) {
            logStr.append(" srcActorID:").append(std::to_string(aCmd.param6().at(0)));
        }
        SwarmDataStorage::instance()->logOut2File("event",logStr);
    #endif

    if(aCmd.type() == CMD_TYPE_PING){ // ping msg
        sendMasterCmdReply(sendID, cmdNo);

        if (aCmd.param1() == 0){ // request 
            ROS_INFO("[SwarmMasterCmd] receive ping from %d, cmdno=%d", sendID, cmdNo);
            if ((_robotID2ReceivedCmdNo.count(sendID) == 0) || (_robotID2ReceivedCmdNo[sendID] < cmdNo) ||(receID == RECE_ID_ALL_MEMBER)) {
                if(receID != RECE_ID_ALL_MEMBER) _robotID2ReceivedCmdNo[sendID] = cmdNo;
                
                //reply
                sendPingCmd(sendID, true);

                // master will send position and actor to the robot
                if(_robotID == masterID){
                    int pos = SwarmDataStorage::instance()->getRobotPosByID(sendID);
                    if(INVALID_ROBOTPOS != pos)
                        sendFormationSwitchCmd(sendID, pos);
                }
            }
        } else if (sendID == masterID){ // reply from master
            ROS_INFO("[SwarmMasterCmd] receive ping from master %d, cmdno=%d", sendID, cmdNo);
            SwarmDataStorage::instance()->setLocalRobotState(KEEPING);
        }
        return;
    }

    //handle the cmd msg
    // cmd from master
    if (sendID == masterID) {
        // answer from master,so cmdNo may be smaller than _robotID2ReceivedCmdNo[sendID]
        switch(aCmd.type()){
        case CMD_TYPE_REQUEST_ACTOR_RSP:{
            std::string swarmName = gActorTagStore.swarmTag2Name(aCmd.param2());
            std::string sendActorName = gActorTagStore.actorTag2Name(swarmName, aCmd.param3());
            std::string actorName = gActorTagStore.actorTag2Name(swarmName, aCmd.param4());
            if (aCmd.param5() == 1){//  param5==1 means allowed
                switch (aCmd.param1()) {
                    case REQUEST_ACTOR_TYPE_ACTIVATE:
                        sendActiveActorEvent(actorName); 
                        ROS_INFO("[SwarmMasterCmd] recvRequestActor answer, master allowd activate actor [%s].", actorName.c_str());
                        break;
                    case REQUEST_ACTOR_TYPE_SWITCH:
                        sendSwitchActorEvent(sendActorName, actorName); 
                        ROS_INFO("[SwarmMasterCmd] recvRequestActor answer, master allowd switch to actor [%s].", actorName.c_str());
                        break;
                    default:
                        break;
                }                
            }else{
                ROS_INFO("[SwarmMasterCmd] recvRequestActor answer, master denied actor [%s].", actorName.c_str());
            }
            //reply to sender
            sendMasterCmdReply(sendID, cmdNo);
            return;
        }
        case CMD_TYPE_BARRIER_RESPONSE:{
            std::string swarmName =  aCmd.param1() == -1 ? "" : gActorTagStore.swarmTag2Name(aCmd.param1());
            std::string actorName = aCmd.param2() == -1 ? "" : gActorTagStore.actorTag2Name(swarmName, aCmd.param2());
            std::string pluginName = aCmd.param3() == -1 ? "" : gActorTagStore.pluginTag2Name(aCmd.param3());

            GlobalBarrierKey barrierKey(swarmName, actorName, pluginName, aCmd.param4());
            int result = aCmd.param5()%10;
            int dataIndex = aCmd.param5()/10;
            actorName = (aCmd.param6().at(0) == -1 ? "" : gActorTagStore.actorTag2Name(swarmName, aCmd.param6().at(0)));
            ROS_WARN("receive barrier response:key=%d, actor=%s, ret=%d,index=%d", aCmd.param4(), actorName.c_str(), result, dataIndex);

            if((result == BarrierResult::YES || result == BarrierResult::OPTIONALYES) && (!aCmd.param6().empty())) {
                boost::unique_lock<boost::shared_mutex> rlock(_barrierKey2ConditionAndResultMutex);
                if((_barrierKey2ConditionAndResult.count(barrierKey) != 0)&&(_barrierKey2ConditionAndResult[barrierKey].count(actorName)!=0)){
                    _barrierKey2ConditionAndResult[barrierKey].at(actorName)._barrierResult._result = (BarrierResult)result;
                    _barrierKey2ConditionAndResult[barrierKey].at(actorName)._barrierResult._index = dataIndex;
                    _barrierKey2ConditionAndResult[barrierKey].at(actorName)._cond->notify_one(); 
                }
            }

            //reply to sender
            sendMasterCmdReply(sendID, cmdNo);
            return;
        }
        default:break;
        }

        // master cmd sent by master actively
        //new cmd or broadcast data RECE_ID_ALL_MEMBER
        if ((_robotID2ReceivedCmdNo.count(sendID) == 0) || (_robotID2ReceivedCmdNo[sendID] < cmdNo) ||(receID == RECE_ID_ALL_MEMBER)) {
            if(receID != RECE_ID_ALL_MEMBER) _robotID2ReceivedCmdNo[sendID] = cmdNo;
            // if (aCmd.param1() > 127 || aCmd.param2() > 127 || aCmd.param3() > 127 || aCmd.param4() > 127 || aCmd.param5() > 127){
            //     ROS_ERROR("[SwarmMasterCmd] receive a master cmd type [%d] with invalid params, return", aCmd.type());
            //     ROS_ERROR("data:%d,%d,%d,%d,%d", aCmd.param1(), aCmd.param2(), aCmd.param3(), aCmd.param4(), aCmd.param5());
            //     return; 
            // }
        
            std::string swarmName = "", actorName = "", actorName1 = "", formationType = "", formationPosStr = "";
            int32_t formationPos = -1, targetID = -1;
            switch (aCmd.type()) {
            case CMD_TYPE_ACTIVATE_ACTOR:
                swarmName = gActorTagStore.swarmTag2Name(aCmd.param1());
                actorName = gActorTagStore.actorTag2Name(swarmName, aCmd.param2());
                sendActiveActorEvent(actorName);
                break;

            case CMD_TYPE_YIELD_ACTOR:
                swarmName = gActorTagStore.swarmTag2Name(aCmd.param1());
                actorName = gActorTagStore.actorTag2Name(swarmName, aCmd.param2());
                sendPauseActorEvent(actorName);
                break;

            case CMD_TYPE_SWITCH_ACTOR:
                swarmName = gActorTagStore.swarmTag2Name(aCmd.param1());
                actorName = gActorTagStore.actorTag2Name(swarmName, aCmd.param2());
                actorName1 = gActorTagStore.actorTag2Name(swarmName, aCmd.param3());
                sendSwitchActorEvent(actorName, actorName1);
                break;

            case CMD_TYPE_SET_FORMATION: //not used currently, replaced by CMD_TYPE_SET_SAVE_FORMATION
                break;
            case CMD_TYPE_GET_FORMATION:
                break;
            case CMD_TYPE_SET_SAVE_FORMATION:
                formationType = std::to_string(aCmd.param3());
                formationPos = aCmd.param4();
                targetID = aCmd.param5();
                if(formationPos == INVALID_ROBOTPOS && targetID == INVALID_ROBOTID) {
                    _reassignSwitch = false;
                    std::map<int,int> tmp;
                    SwarmDataStorage::instance()->resetRobotPos(tmp);
                } else {
                    _reassignSwitch = true;
                    swarmName = gActorTagStore.swarmTag2Name(aCmd.param1());
                    actorName = gActorTagStore.actorTag2Name(swarmName, aCmd.param2());
                    SwarmDataStorage::instance()->setRobotPos(targetID, formationPos,swarmName,actorName,formationType);
                }
                
                // if (targetID == _robotID)
                //     _pActorScheduler->setFormation(swarmName, actorName, formationType, std::to_string(formationPos));
                break;

            case CMD_TYPE_SET_FORMATION_SWITCH:
                ROS_INFO("receive master cmd 7, pos=%d", aCmd.param4());
                _reassignSwitch = true; 
                swarmName = gActorTagStore.swarmTag2Name(aCmd.param1());
                actorName = gActorTagStore.actorTag2Name(swarmName, aCmd.param2());
                actorName1 = gActorTagStore.actorTag2Name(swarmName, aCmd.param5());
                // formationType = std::to_string(aCmd.param3());
                // formationPosStr = std::to_string(aCmd.param4());
                //_pActorScheduler->setFormation(swarmName, actorName, formationType, formationPosStr);
                sendSwitchActorEvent(actorName1, actorName);
                //may not need,SwarmDataStorage::instance()->setRobotPos(_robotID), formationPos);
                break;

            case CMD_TYPE_SWITCH_MASTER:
                SwarmDataStorage::instance()->setLocalRobotState(KEEPING);
                ROS_INFO("[SwarmMasterCmd] recvIamMasterCmd, newMasterID is [%d], oldMasterID is [%d]", masterID, aCmd.param1());
                break;
            default:
                break;
            }
        }

        //reply to sender
        sendMasterCmdReply(sendID, cmdNo);

    } else if(masterID == _robotID){ // sender is not master
        if(aCmd.type() != CMD_TYPE_REQUEST_ACTOR && aCmd.type() != CMD_TYPE_BARRIER_REQUEST){
            return;
        }

        //reply to sender
        sendMasterCmdReply(sendID, cmdNo);

        switch(aCmd.type()){
        case CMD_TYPE_REQUEST_ACTOR:{
            std::string swarmName = gActorTagStore.swarmTag2Name(aCmd.param2());
            std::string sendActorName = gActorTagStore.actorTag2Name(swarmName, aCmd.param3());
            std::string actorName = gActorTagStore.actorTag2Name(swarmName, aCmd.param4());
            ROS_INFO("[SwarmMasterCmd] master recvRequestActor request,robot[%d] need actor[%s]", sendID, actorName.c_str());

            MasterCmd* answerMsg = new MasterCmd(aCmd);
            answerMsg->type(CMD_TYPE_REQUEST_ACTOR_RSP);
            answerMsg->sendID(_robotID);
            answerMsg->receID(sendID);

            if(SwarmDataStorage::instance()->getLocalRobotState() == VOTING){
                ROS_WARN("[SwarmMasterCmd] master receive actor request, but master is in voting state!");
                answerMsg->param5(0);

#ifdef ACTOR_SWARM_LOG_OUT  
                logStr.clear();
                logStr.append("VOTING master receive actor request from ").append(std::to_string(sendID).append(" actorName=").append(actorName));
                SwarmDataStorage::instance()->logOut2File("warn", logStr);
#endif                  
            } else{
                int requestResult = handleSwarmActorRequest(sendID,aCmd.param1(), swarmName, sendActorName,actorName);
                answerMsg->param5(requestResult);
                ROS_INFO("[SwarmMasterCmd] robot[%d] need actor[%s],result:%d.", sendID, actorName.c_str(), answerMsg->param5());
            }
            
            sendMasterCmd(answerMsg);

#ifdef ACTOR_SWARM_LOG_OUT
            logStr.clear();
            logStr.append("answerActorRequest ").append(std::to_string(_robotID)).append("-->").append(std::to_string(sendID)).append(" cmdNo:").append(std::to_string(cmdNo)).append(" cmdType:").append(std::to_string(answerMsg->param1())).append(" requestResult:").append(std::to_string(answerMsg->param5()));
            SwarmDataStorage::instance()->logOut2File("event", logStr);

            logStr = std::to_string(MasterCmd::getCdrSerializedSize(*answerMsg)) + ",MasterCmd_ReplyActorReq";
            SwarmDataStorage::instance()->logOut2File("netsend", logStr);
#endif
        }
        break;
        case CMD_TYPE_BARRIER_REQUEST: { 
            std::string swarmName =  aCmd.param1() == -1 ? "" : gActorTagStore.swarmTag2Name(aCmd.param1());
            std::string actorName = aCmd.param2() == -1 ? "" : gActorTagStore.actorTag2Name(swarmName, aCmd.param2());
            std::string pluginName = aCmd.param3() == -1 ? "" : gActorTagStore.pluginTag2Name(aCmd.param3());

            GlobalBarrierKey barrierKey(swarmName, actorName, pluginName, aCmd.param4());
            if(!aCmd.param6().empty()){actorName = (aCmd.param6().at(0) == -1 ? "" : gActorTagStore.actorTag2Name(swarmName, aCmd.param6().at(0)));}
            ROS_WARN("[SwarmMasterCmd] receive barrier request from [%d],key=%d, actor=%s", sendID, aCmd.param4(), actorName.c_str());

            if(SwarmDataStorage::instance()->getLocalRobotState() == VOTING){
                ROS_WARN("[SwarmMasterCmd] receive barrier request, but master is in voting state, return NO!");
                sendBarrierResponse(barrierKey, sendID, actorName, 0);
#ifdef ACTOR_SWARM_LOG_OUT  
                logStr.clear();
                logStr.append("VOTING master receive barrier request from ").append(std::to_string(sendID)).append(" actorName=").append(actorName).append(" barrierKey=").append(std::to_string(aCmd.param4()));
                SwarmDataStorage::instance()->logOut2File("warn", logStr);
#endif                 
            } else{
                int tempIndex = 0;
                handleBarrierRequest(barrierKey, actorName, aCmd.param5(), sendID, tempIndex);
            }           
        }
        break;
        default: break;
        }
    }
}

// clear from _robotID2TimeoutMsg by robotID
void SwarmMasterCmd::clearMasterCmdsForRobot(const int32_t aRobotID){
        ROS_INFO("[SwarmMasterCmd] clearMasterCmdsForRobot [%d]", aRobotID);
#ifdef ACTOR_SWARM_LOG_OUT 
    std::string logStr = "clearMasterCmdsForRobot:" + std::to_string(aRobotID);
    SwarmDataStorage::instance()->logOut2File("event", logStr);
#endif

    boost::unique_lock<boost::shared_mutex> wlock(_robotID2MasterCmdQueueMutex);
    if(_robotID2MasterCmdQueue.count(aRobotID)>0) {
        _robotID2MasterCmdQueue[aRobotID].clearQueue();
        _robotID2MasterCmdQueue.erase(aRobotID);
    }
    wlock.unlock();

    boost::unique_lock<boost::shared_mutex> cmdNoWriteLock(_cmdNo2RobotReplyMutex);
    for(auto it=_cmdNo2RobotReply.begin();it!=_cmdNo2RobotReply.end();){
        if(it->second.count(aRobotID)>0) it->second.erase(aRobotID);
        if(it->second.empty()) it = _cmdNo2RobotReply.erase(it);
        else it++;    
    } 
}

// clear _robotID2MasterCmdQueue
void SwarmMasterCmd::clearAllMasterCmds(){
    ROS_INFO("[SwarmMasterCmd] clearAllMasterCmds");
#ifdef ACTOR_SWARM_LOG_OUT 
    SwarmDataStorage::instance()->logOut2File("event", "clearAllMasterCmds");
#endif    
    {
        boost::unique_lock<boost::shared_mutex> wlock(_robotID2MasterCmdQueueMutex);
        for(auto it = _robotID2MasterCmdQueue.begin(); it != _robotID2MasterCmdQueue.end(); it++){
            it->second.clearQueue();
        }
        _robotID2MasterCmdQueue.clear(); // or swap
    }

    {
        boost::unique_lock<boost::shared_mutex> wlock(_barrierKey2BarrierRequestListMutex);
        _barrierKey2BarrierRequestList.clear();// the master should not response to barrier request
    }
    
    // NOTICE: can not clear it!!!
    //{
    //    boost::unique_lock<boost::shared_mutex> wlock(_barrierKey2ConditionAndResultMutex);
    //    _barrierKey2ConditionAndResult.clear();
    //}
}

// resend master cmd until recv reply
void SwarmMasterCmd::resendTimeoutMasterCmdLoop(){
    ROS_INFO("[SwarmMasterCmd] start resendTimeoutMasterCmdLoop");
    while(1){
        resendTimeoutMasterCmd();
        usleep((RESEND_DURATION_CMD_RTPS+rand()%50)*1000);
    }
}

void SwarmMasterCmd::resendTimeoutMasterCmd(){
    // todo there is msg will be send to robot which is in leave state, then master will try to send this msgs, and nerver clear msgqueue
    // send the front master cmd
    boost::unique_lock<boost::shared_mutex> wlock(_robotID2MasterCmdQueueMutex);
    for(auto it = _robotID2MasterCmdQueue.begin();it!=_robotID2MasterCmdQueue.end();it++){
        if(it->second.isEmpty()) continue;

        // check if the front cmd has been replyed， if replyed ,then erase it
        bool reply = true;
        int32_t cmdNo = it->second._msgQueue.begin()->first;
        boost::shared_lock<boost::shared_mutex> rlock(_cmdNo2RobotReplyMutex);
        for (auto it2 = _cmdNo2RobotReply[cmdNo].begin(); it2 != _cmdNo2RobotReply[cmdNo].end(); it2++) {
            if (!it2->second) {
                reply = false;
                break;
            }
        }
        rlock.unlock();

        if(reply){
            it->second.popMsg();
            if(it->second.isEmpty()) continue;
        }
        
        // check whether the front cmd can be sent or not
        //ROS_ERROR("destid=%d, queuesize=%d, resendcount=%d, leftwaitcount=%d", it->first, it->second.size(), it->second._resendCount, it->second._leftWaitCount);
        if(it->second._leftWaitCount > 0) {
            it->second._leftWaitCount--;
            continue;
        }

        // send the front cmd when _leftWaitCount=0
        it->second._resendCount++;
        it->second._leftWaitCount = std::min(it->second._resendCount, (short)(RESEND_MAX_DURATION_CMD_RTPS/RESEND_DURATION_CMD_RTPS));
        MasterCmd* pCmd = it->second._msgQueue.begin()->second;
        boost::unique_lock<boost::mutex> pubCmdLock(_pubMutex);
        _pPubMasterCmd->publish(*pCmd);
        pubCmdLock.unlock();

#ifdef ACTOR_SWARM_LOG_OUT
        std::string logStr = "sendMasterCmd";
        logStr.append(std::to_string(pCmd->sendID())).append("-->").append(std::to_string(pCmd->receID()))
        .append(" cmdNo:").append(std::to_string(pCmd->cmdNo()))
        .append(" cmdType:").append(std::to_string(pCmd->type()));
        SwarmDataStorage::instance()->logOut2File("event",logStr);

        logStr = std::to_string(MasterCmd::getCdrSerializedSize(*pCmd)) + ",MasterCmd";
        SwarmDataStorage::instance()->logOut2File("netsend", logStr);

        if(it->second._resendCount > 3){
            std::string eventStr = "send message to robot ";
            eventStr.append(std::to_string(it->first)).append(" abnormal, sendcount=").append(std::to_string(it->second._resendCount)).append(", queuesize=").append(std::to_string(it->second.size()));
            SwarmDataStorage::instance()->logOut2File("warn", eventStr);
        }
#endif
    }  
}

void SwarmMasterCmd::sendActiveActorEvent(const std::string& anActorName){
    ROS_INFO("[SwarmMasterCmd] sendActiveActorEvent, %s", anActorName.c_str());
    actor_msgs::UTOEvent event;
    event.eventName = "";
    event.currentActor = "";
    event.targetActor = anActorName;
    event.type = ACTIVATE_ACTOR_MSG;

    boost::unique_lock<boost::mutex> lock(_pubUTOEventMutex);
    _pubUTOEvent.publish(event);
    lock.unlock();
}

void SwarmMasterCmd::sendPauseActorEvent(const std::string& anActorName){
    ROS_INFO("[SwarmMasterCmd] sendPauseActorEvent, %s", anActorName.c_str());
    actor_msgs::UTOEvent event;
    event.eventName = "";
    event.targetActor = "";
    event.type = PAUSE_ACTOR_MSG;
    event.currentActor = anActorName;

    boost::unique_lock<boost::mutex> lock(_pubUTOEventMutex);
    _pubUTOEvent.publish(event);
    lock.unlock();
}

void SwarmMasterCmd::sendSwitchActorEvent(const std::string& anActorName, const std::string& aTargetActorName){
    ROS_INFO("[SwarmMasterCmd] sendSwitchActorEvent, %s,%s", anActorName.c_str(), aTargetActorName.c_str());
    actor_msgs::UTOEvent event;
    event.eventName = "";
    event.type = SWITCH_ACTOR_MSG;
    event.currentActor = anActorName;
    event.targetActor = aTargetActorName;

    boost::unique_lock<boost::mutex> lock(_pubUTOEventMutex);
    _pubUTOEvent.publish(event);
    lock.unlock();
}


void SwarmMasterCmd::MasterCmdQueue::clearQueue(){
    for(auto it = _msgQueue.begin(); it != _msgQueue.end(); it++){
        delete it->second;
        it->second = NULL;
    }
    _msgQueue.clear();
}

void SwarmMasterCmd::MasterCmdQueue::popMsg(){
    // erase from cmd
    delete _msgQueue.begin()->second;
    _msgQueue.erase(_msgQueue.begin());

    // reset state
    _resendCount = 0;
    _leftWaitCount = 0;
}

void SwarmMasterCmd::MasterCmdQueue::pushMsg(MasterCmd* cmd){
    if(_msgQueue.count(cmd->cmdNo()) > 0){
        delete _msgQueue[cmd->cmdNo()];
    }
    _msgQueue[cmd->cmdNo()] = cmd;
}

int SwarmMasterCmd::MasterCmdQueue::size(){
    return _msgQueue.size();
}

bool SwarmMasterCmd::MasterCmdQueue::isEmpty(){
    return _msgQueue.size() == 0;
}