/* ==================================================================
*
* Copyright (c) 2016-2019, micROS Group, NIIDT, TAIIC, HPCL.
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions 
* are met: 
*
* 1. Redistributions of source code must retain the above copyright 
*     notice, this list of conditions and the following disclaimer. 
* 2. Redistributions in binary form must reproduce the above copyright 
*     notice, this list of conditions and the following disclaimer in the 
*     documentation and/or other materials provided with the 
*     distribution. 
* 3. All advertising materials mentioning features or use of this software 
*     must display the following acknowledgement: 
*     This product includes software developed by the micROS Group. and 
*     its contributors. 
* 4. Neither the name of the Group nor the names of its contributors may 
*     be used to endorse or promote products derived from this software 
*     without specific prior written permission. 
*
* THIS SOFTWARE IS PROVIDED BY MICROS,GROUP AND CONTRIBUTORS
* ''AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT
* NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
* FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL 
* THE MICROS,GROUP OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS 
* OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) 
* HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
* OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN 
* IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
* ===================================================================
*
* Author: Bin Di, Chunlian Fu, Junling Gao, Ruihao Li, Wei Yi.
*         (in alphabetical order)
*
*/

#include "swarm_master_cmd/swarm_master_cmd_api.h"
#include "swarm_master_cmd/swarm_master_cmd.h"
#include "swarm_data_storage/swarm_data_storage.h"

SwarmMasterCmd* gSwarmMasterCmd = NULL;

void requestActivateActorFromMaster(const std::string& aSendActorName,const std::string& anActorName){
    ROS_INFO("request activate actor from master api called");
    if(gSwarmMasterCmd){
        gSwarmMasterCmd->sendSwarmActorRequestCmd(aSendActorName,anActorName,REQUEST_ACTOR_TYPE_ACTIVATE);
    }
}

void requestSwitchActorFromMaster(const std::string& aSendActorName,const std::string& anActorName){
    ROS_INFO("request switch actor from master api called");
    if(gSwarmMasterCmd){
        gSwarmMasterCmd->sendSwarmActorRequestCmd(aSendActorName,anActorName,REQUEST_ACTOR_TYPE_SWITCH);
    }
}

BarrierResult actorBarrierApi(GlobalBarrierKey& aBarrierKey, int aWaitCount,  const short aTimeout){
    if(gSwarmMasterCmd){
        //if(aBarrierKey._actorName.empty() || aBarrierKey._barrierKey == -1){
        if(aWaitCount < 0 || aBarrierKey.getBarrierKey() == -1){
            ROS_WARN("actorBarrierApi: invalid param!");
            return BarrierResult::NO;
        }
        //aBarrierKey._pluginName.clear();
        int tempIndex = 0;
        std::string actorName = "";
        return gSwarmMasterCmd->sendBarrierRequestCmd(aBarrierKey, actorName, aWaitCount, aTimeout, tempIndex);
    }
    return BarrierResult::NO;
}

BarrierResult pluginBarrierApi(GlobalBarrierKey& aBarrierKey, int aWaitCount, const short aTimeout){
    if(gSwarmMasterCmd){
        //if(aBarrierKey._actorName.empty() || aBarrierKey._pluginName.empty() || aBarrierKey._barrierKey == -1){
        if(aWaitCount < 0 || aBarrierKey.getPluginName().empty() || aBarrierKey.getBarrierKey() < 0){
            ROS_WARN("pluginBarrierApi: invalid param!");
            return BarrierResult::NO;
        }
        int tempIndex = 0;
        std::string actorName = "";
        return gSwarmMasterCmd->sendBarrierRequestCmd(aBarrierKey, actorName, aWaitCount, aTimeout, tempIndex);
    }
    return BarrierResult::NO;
}

BarrierResult actorDataDistributeBarrierApi(GlobalBarrierKey& aBarrierKey, std::string anActorName, int aWaitCount, const short aTimeout, int &anIndex){
    if(gSwarmMasterCmd){
        //if(aBarrierKey._actorName.empty() || aBarrierKey._barrierKey == -1){
        if(aWaitCount < 0 || aBarrierKey.getBarrierKey() == -1){
            ROS_WARN("actorDataDistributeBarrierApi: invalid param!");
            return BarrierResult::NO;
        }
        if((!aBarrierKey.getActorName().empty())&&(aBarrierKey.getActorName().compare(anActorName)!=0)){
            ROS_WARN("actorDataDistributeBarrierApi: actorName inconsistent");
            return BarrierResult::NO;
        }
        //aBarrierKey._pluginName.clear();
        //std::string actorName = "";
        return gSwarmMasterCmd->sendBarrierRequestCmd(aBarrierKey, anActorName, aWaitCount, aTimeout, anIndex);
    }
    return BarrierResult::NO;
}

//TODO temp add formation content for DA
short getFormationPos(const int aRobotID) {
    int robotID = aRobotID == -1 ? SwarmDataStorage::instance()->getLocalRobotID() : aRobotID;
    return SwarmDataStorage::instance()->getRobotPosByID(robotID);

}