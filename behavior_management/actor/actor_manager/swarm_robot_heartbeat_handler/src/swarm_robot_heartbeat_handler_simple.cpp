/* ==================================================================
*
* Copyright (c) 2016-2019, micROS Group, NIIDT, TAIIC, HPCL.
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions 
* are met: 
*
* 1. Redistributions of source code must retain the above copyright 
*     notice, this list of conditions and the following disclaimer. 
* 2. Redistributions in binary form must reproduce the above copyright 
*     notice, this list of conditions and the following disclaimer in the 
*     documentation and/or other materials provided with the 
*     distribution. 
* 3. All advertising materials mentioning features or use of this software 
*     must display the following acknowledgement: 
*     This product includes software developed by the micROS Group. and 
*     its contributors. 
* 4. Neither the name of the Group nor the names of its contributors may 
*     be used to endorse or promote products derived from this software 
*     without specific prior written permission. 
*
* THIS SOFTWARE IS PROVIDED BY MICROS,GROUP AND CONTRIBUTORS
* ''AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT
* NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
* FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL 
* THE MICROS,GROUP OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS 
* OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) 
* HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
* OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN 
* IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
* ===================================================================
*
* Author: Bin Di, Chunlian Fu, Junling Gao, Ruihao Li, Wei Yi.
*         (in alphabetical order)
*
*/

#include "swarm_robot_heartbeat_handler/swarm_robot_heartbeat_handler_simple.h"
#include "swarm_data_storage/swarm_data_storage.h"
#include "ros/ros.h"

SwarmRobotHeartbeatHandlerSimple::SwarmRobotHeartbeatHandlerSimple(boost::shared_ptr<ActorScheduler> pScheduler)
: SwarmRobotHeartbeatHandler(pScheduler){
    _pMasterElection = new SwarmMasterElectionSimple; // will be released by base class
}

SwarmRobotHeartbeatHandlerSimple::~SwarmRobotHeartbeatHandlerSimple(){
    if(_pMasterElection){
        delete _pMasterElection;
        _pMasterElection = NULL;
    }
}

void SwarmRobotHeartbeatHandlerSimple::processRobotJoinMsg(const int aJoinRobotID){
    // if force set master, then do nothing. 
    // note: include lost actor function
    if(SwarmDataStorage::instance()->getMasterGenerateType() == FORCE) return;

    //if already join, return
    if(SwarmDataStorage::instance()->getRobotStateByID(aJoinRobotID) == KEEPING) return;

    //add to local swarm
    SwarmDataStorage::instance()->addRobotToLocalSwarm(aJoinRobotID);

    // if master id is force set, if master join,then set KEEPING state, else if local is master, then send IamMaster to the robot.
    if(SwarmDataStorage::instance()->getMasterGenerateType() == SEMIAUTO){
        int masterID = SwarmDataStorage::instance()->getMasterID();
        if(masterID == aJoinRobotID){
            SwarmDataStorage::instance()->setLocalRobotState(KEEPING);
            SwarmDataStorage::instance()->setMasterID(aJoinRobotID, AUTO);
        } else if (masterID == _robotID){
            processSwitchMaster(masterID, aJoinRobotID);
        }
    } else if (SwarmDataStorage::instance()->getMasterGenerateType() == AUTO){ // if master id is auto set
        // reset local masterID,because aJoinRobotID maybe the min ID
        int oldMasterID = SwarmDataStorage::instance()->getMasterID();
        _pMasterElection->electMasterID();
        int newMasterID = SwarmDataStorage::instance()->getMasterID();
        
        if (newMasterID != oldMasterID){// master changed
            ROS_INFO("[SwarmRobotHeartbeatHandlerSimple] after [%d] join, current master is [%d]", aJoinRobotID, newMasterID);
            if(_robotID == newMasterID){   // local is new master
                processSwitchMaster(oldMasterID, RECE_ID_ALL_MEMBER);
            } else {
                if(_robotID == oldMasterID)// local is old master
                    _pMasterCmd->clearAllMasterCmds();
                else                         // local is member
                    _pMasterCmd->clearMasterCmdsForRobot(oldMasterID);

                SwarmDataStorage::instance()->setLocalRobotState(VOTING);   
                _pMasterCmd->sendPingCmd(newMasterID);
            }
        } else if (newMasterID == _robotID){ // master is not changed and local is master
            processSwitchMaster(oldMasterID, aJoinRobotID);
        }
    }
   
    // switch to candidateActor if exits multi lostActor, just keep one
    /*if(_lostActorSwitch && SwarmDataStorage::instance()->getMasterID() == _robotID){
        std::vector<int> robotIDs;
        if(SwarmDataStorage::instance()->getRobotsWithActorRunning(_lostActorName, robotIDs)){
            std::string swarmName = SwarmDataStorage::instance()->getSwarmNameByID(_robotID);
            for(auto it = robotIDs.begin()+1; it != robotIDs.end(); it++)
                _pMasterCmd->sendSwitchActorCmd(*it, swarmName, _lostActorName, _candidateActorName);
        }
    }*/

    // if a swarm task is running, reassign formation pos on master node
    // condition is temporary, may need to judge running actors and other states
    if(_lostActorSwitch 
    && SwarmDataStorage::instance()->getMasterID() == _robotID 
    && !SwarmDataStorage::instance()->getSwarmNameByID(_robotID).empty()) {
        _pMasterCmd->addRobotToFormation(aJoinRobotID);
        _pMasterCmd->reassignFormation();
        //SwarmDataStorage::instance()->printInformation();
        
        //send swarm info to member new joined, called after reassign because reassign may change the formation pos info
        _pMasterCmd->sendInfo2RobotNewJoin(aJoinRobotID);
    }

    // notify local actors to swarm members
    _pRobotActors->pubRunningActors(aJoinRobotID);
}

void SwarmRobotHeartbeatHandlerSimple::processRobotLeaveMsg(const int aLeaveRobotID){
    // if force set master, then do nothing. 
    // note: include lost actor function
    if(SwarmDataStorage::instance()->getMasterGenerateType() == FORCE) return;

    //if already leave, return
    if(SwarmDataStorage::instance()->getRobotStateByID(aLeaveRobotID) == LEAVE) return;

    //remove from swarm, set formation pos to INVALID
    SwarmDataStorage::instance()->removeRobotFromLocalSwarm(aLeaveRobotID);

    //when robot leave, clear cmd msg sent to it
    _pMasterCmd->clearMasterCmdsForRobot(aLeaveRobotID);
    
    // if master leave,then elect master
    if(SwarmDataStorage::instance()->getMasterGenerateType() != FORCE){
        int oldMasterID = SwarmDataStorage::instance()->getMasterID();
        if(aLeaveRobotID == oldMasterID) {
            // elect master
            _pMasterElection->electMasterID();
            int newMasterID = SwarmDataStorage::instance()->getMasterID();
            ROS_INFO("[SwarmRobotHeartbeatHandlerSimple] after [%d] leave, current master is [%d]", aLeaveRobotID, newMasterID);
            
            // notify all members "i'm the master"
            if (newMasterID == _robotID ){
                processSwitchMaster(oldMasterID, RECE_ID_ALL_MEMBER);
            } else {
                SwarmDataStorage::instance()->setLocalRobotState(VOTING);
                _pMasterCmd->sendPingCmd(newMasterID);
            }
        }
    }
    
    //if Actor lost, then choose one among candidateActors
    /*if(_lostActorSwitch && SwarmDataStorage::instance()->getMasterID() == _robotID) {
        std::vector<int> robotIDs;
        if(!SwarmDataStorage::instance()->getRobotsWithActorRunning(_lostActorName, robotIDs)
        && SwarmDataStorage::instance()->getRobotsWithActorRunning(_candidateActorName, robotIDs)){
            std::string swarmName = SwarmDataStorage::instance()->getSwarmNameByID(_robotID);
            _pMasterCmd->sendSwitchActorCmd(robotIDs[0], swarmName, _candidateActorName, _lostActorName);
        }
    }*/

    //if a swarm task is running, reassign formation pos
    //condition is temporary, may need to judge running actors and other states
    if(_lostActorSwitch
    && SwarmDataStorage::instance()->getMasterID() == _robotID
    && !SwarmDataStorage::instance()->getSwarmNameByID(_robotID).empty()) { 
        _pMasterCmd->reassignFormation();
        //SwarmDataStorage::instance()->printInformation();
    }
}

//deprecated
void SwarmRobotHeartbeatHandlerSimple::processRobotJoinForMaster(const int anOldMasterID, const int aDestMasterID, const int aJoinRobotID){
    //set robot state, wait for members to reply
    SwarmDataStorage::instance()->setLocalRobotState(VOTING);
    // notify dest members
    _pMasterCmd->sendIamMasterCmd(anOldMasterID, aDestMasterID);

    //TODO: may need to change state to keeping when receive all reply msgs
    SwarmDataStorage::instance()->setLocalRobotState(KEEPING);

    //send swarm info to member new joined
    _pMasterCmd->sendInfo2RobotNewJoin(aJoinRobotID);
    
    //if a swarm task is running, reassign formation pos
    //condition is temporary, may need to judge running actors and other states
    // std::string swarmName = SwarmDataStorage::instance()->getSwarmNameByID(_robotID);
    // if(!swarmName.empty()) {
    //     _pMasterCmd->addRobotToFormation(aJoinRobotID);
    //     _pMasterCmd->reassignFormation();
    //     SwarmDataStorage::instance()->printInformation();
    // }
}

void SwarmRobotHeartbeatHandlerSimple::processSwitchMaster(const int anOldMasterID, const int aDestRobotID){
    //set robot state, wait for members to reply
    //SwarmDataStorage::instance()->setLocalRobotState(VOTING);
    // notify dest members
    _pMasterCmd->sendIamMasterCmd(anOldMasterID, aDestRobotID);
    SwarmDataStorage::instance()->setLocalRobotState(KEEPING);   
    //TODO: may need to change state to keeping when receive all reply msgs
    //SwarmDataStorage::instance()->setLocalRobotState(KEEPING);
}

