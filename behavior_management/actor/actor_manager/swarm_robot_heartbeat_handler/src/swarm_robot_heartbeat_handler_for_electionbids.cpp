/* ==================================================================
*
* Copyright (c) 2016-2019, micROS Group, NIIDT, TAIIC, HPCL.
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions 
* are met: 
*
* 1. Redistributions of source code must retain the above copyright 
*     notice, this list of conditions and the following disclaimer. 
* 2. Redistributions in binary form must reproduce the above copyright 
*     notice, this list of conditions and the following disclaimer in the 
*     documentation and/or other materials provided with the 
*     distribution. 
* 3. All advertising materials mentioning features or use of this software 
*     must display the following acknowledgement: 
*     This product includes software developed by the micROS Group. and 
*     its contributors. 
* 4. Neither the name of the Group nor the names of its contributors may 
*     be used to endorse or promote products derived from this software 
*     without specific prior written permission. 
*
* THIS SOFTWARE IS PROVIDED BY MICROS,GROUP AND CONTRIBUTORS
* ''AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT
* NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
* FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL 
* THE MICROS,GROUP OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS 
* OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) 
* HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
* OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN 
* IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
* ===================================================================
*
* Author: Bin Di, Chunlian Fu, Junling Gao, Ruihao Li, Wei Yi.
*         (in alphabetical order)
*
*/

//#include "swarm_master_election/test_bids.h"
#include "swarm_robot_heartbeat_handler/swarm_robot_heartbeat_handler_for_electionbids.h"
#include "swarm_data_storage/swarm_data_storage.h"
#include "ros/ros.h"

SwarmRobotHeartbeatHandler_ForElectionBids::SwarmRobotHeartbeatHandler_ForElectionBids(boost::shared_ptr<ActorScheduler> pScheduler)
: SwarmRobotHeartbeatHandler(pScheduler) {
    _pMasterElection = new SwarmMasterElectionBids2; // will be released by base class
    _pMasterElection->init(-1, "/home/gjl/log/bid/");
    _pthCheckVotingState = boost::shared_ptr<boost::thread>(new boost::thread(boost::bind(&SwarmRobotHeartbeatHandler_ForElectionBids::checkVotingStateLoop,this)));
    _pthCheckVotingState->detach();
}

SwarmRobotHeartbeatHandler_ForElectionBids::~SwarmRobotHeartbeatHandler_ForElectionBids(){
    if(_pMasterElection){
        delete _pMasterElection;
        _pMasterElection = NULL;
    }
    if(_pthCheckVotingState){
        _pthCheckVotingState->join();
        _pthCheckVotingState->interrupt();
        _pthCheckVotingState.reset();
    }
}

void SwarmRobotHeartbeatHandler_ForElectionBids::processRobotJoinMsg(const int aJoinRobotID){
    //if already join, return
    if(SwarmDataStorage::instance()->getRobotStateByID(aJoinRobotID) == KEEPING) return;

    //add to local swarm
    SwarmDataStorage::instance()->addRobotToLocalSwarm(aJoinRobotID);

    //for master
    int32_t curMasterID = SwarmDataStorage::instance()->getMasterID();
    if(curMasterID == _robotID){
        //notify aJoinRobotID that i am the master to aJoinRobotID
        _pMasterCmd->sendIamMasterCmd(_robotID, aJoinRobotID);

        //if a swarm task is running, reassign formation pos
        //condition is temporary, may need to judge running actors and other states
        std::string swarmName = SwarmDataStorage::instance()->getSwarmNameByID(_robotID);
        if(!swarmName.empty()) {
            _pMasterCmd->addRobotToFormation(aJoinRobotID);
            _pMasterCmd->reassignFormation();
            SwarmDataStorage::instance()->printInformation();
        }

        //send swarm info to member new joined, called after reassign because reassign may change the formation pos info
        _pMasterCmd->sendInfo2RobotNewJoin(aJoinRobotID);  
    }

    // notify local actors to swarm members
    _pRobotActors->pubRunningActors(aJoinRobotID);
}

void SwarmRobotHeartbeatHandler_ForElectionBids::processRobotLeaveMsg(const int aLeaveRobotID){
    //if already leave, return
    if(SwarmDataStorage::instance()->getRobotStateByID(aLeaveRobotID) == LEAVE) return;

    //remove from swarm, set formation pos to INVALID
    SwarmDataStorage::instance()->removeRobotFromLocalSwarm(aLeaveRobotID);

    //when robot leave, clear cmd msg sent to it
    _pMasterCmd->clearMasterCmdsForRobot(aLeaveRobotID);
    
    // if master leave,then elect master
    int oldMasterID = SwarmDataStorage::instance()->getMasterID();
    if(aLeaveRobotID == oldMasterID) {
        // elect master
        _pMasterElection->activateMasterVote();
        ROS_INFO("[SwarmRobotHeartbeatHandler_ForElectionBids] after [%d] leave, revote new master", aLeaveRobotID);
    }
}


void SwarmRobotHeartbeatHandler_ForElectionBids::checkVotingStateLoop(){
    while(1){
        int oldMasterID = SwarmDataStorage::instance()->getMasterID();

        boost::unique_lock<boost::mutex> lock(_mutex);
        _pMasterElection->getVoteCompletedCond().wait(lock);
        if(SwarmDataStorage::instance()->getMasterID() == _robotID
        && SwarmDataStorage::instance()->getLocalRobotState() == KEEPING){
            _pMasterCmd->sendIamMasterCmd(oldMasterID, RECE_ID_ALL_MEMBER);
             if(!SwarmDataStorage::instance()->getSwarmNameByID(_robotID).empty()) 
                _pMasterCmd->reassignFormation();
        }
        
    }
}