/* ==================================================================
*
* Copyright (c) 2016-2019, micROS Group, NIIDT, TAIIC, HPCL.
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions 
* are met: 
*
* 1. Redistributions of source code must retain the above copyright 
*     notice, this list of conditions and the following disclaimer. 
* 2. Redistributions in binary form must reproduce the above copyright 
*     notice, this list of conditions and the following disclaimer in the 
*     documentation and/or other materials provided with the 
*     distribution. 
* 3. All advertising materials mentioning features or use of this software 
*     must display the following acknowledgement: 
*     This product includes software developed by the micROS Group. and 
*     its contributors. 
* 4. Neither the name of the Group nor the names of its contributors may 
*     be used to endorse or promote products derived from this software 
*     without specific prior written permission. 
*
* THIS SOFTWARE IS PROVIDED BY MICROS,GROUP AND CONTRIBUTORS
* ''AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT
* NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
* FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL 
* THE MICROS,GROUP OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS 
* OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) 
* HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
* OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN 
* IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
* ===================================================================
*
* Author: Bin Di, Chunlian Fu, Junling Gao, Ruihao Li, Wei Yi.
*         (in alphabetical order)
*
*/

#ifndef __SWARM_ACTOR_SHARING_STIGMERGY_H__
#define __SWARM_ACTOR_SHARING_STIGMERGY_H__

#include "std_msgs/Int8.h"
#include "micros_swarm/micros_swarm.h"
#include "actor_schedule/actor_schedule.h"
#include "swarm_actor_sharing/swarm_actor_sharing.h"

//robot actors info sharing through stigmergy
class SwarmActorSharingStigmergy : public SwarmActorSharing{
public:
    SwarmActorSharingStigmergy(boost::shared_ptr<ActorScheduler> pScheduler);
    ~SwarmActorSharingStigmergy();
    virtual void pubRunningActors(const int aDestID = -1);

protected:
    void putSwarmActors(const int32_t aNum, const int32_t aDuration);
    bool getSwarmActors(const int32_t aRobotID, std::string& anSwarmName, std::vector<std::string>& anActors);

    // if local actor changed, then save and put to stigmergy
    void updateLocalSwarmActors();
    void updateLocalSwarmActorsLoop();

    // read others' actors info from stigmergy
    void checkSwarmActors();
    void checkSwarmActorsLoop();

    std::string getCmdKey(const int32_t aRobotID);

private:
    int _robotID;

    micros_swarm::VirtualStigmergy _vs;
    boost::mutex _vsMutex;

    // map for save robot actors lamport_clock
	std::map<int32_t, int32_t> _robotID2Clock;
	boost::mutex _rcMutex;

    boost::shared_ptr<ActorScheduler> _pActorScheduler;

    // thread for put actors and check actors
	boost::shared_ptr<boost::thread> _pthCheckActors;
	boost::shared_ptr<boost::thread> _pthUpdateActors;
};
#endif