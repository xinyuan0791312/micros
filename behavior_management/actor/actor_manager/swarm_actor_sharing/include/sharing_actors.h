// Copyright 2016 Proyectos y Sistemas de Mantenimiento SL (eProsima).
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/*!
 * @file sharing_actors.h
 * This header file contains the declaration of the described types in the IDL file.
 *
 * This file was generated by the tool gen.
 */

#ifndef _SHARING_ACTORS_H_
#define _SHARING_ACTORS_H_

// TODO Poner en el contexto.

#include <stdint.h>
#include <array>
#include <string>
#include <vector>
#include <map>
#include <bitset>

#if defined(_WIN32)
#if defined(EPROSIMA_USER_DLL_EXPORT)
#define eProsima_user_DllExport __declspec( dllexport )
#else
#define eProsima_user_DllExport
#endif
#else
#define eProsima_user_DllExport
#endif

#if defined(_WIN32)
#if defined(EPROSIMA_USER_DLL_EXPORT)
#if defined(sharing_actors_SOURCE)
#define sharing_actors_DllAPI __declspec( dllexport )
#else
#define sharing_actors_DllAPI __declspec( dllimport )
#endif // sharing_actors_SOURCE
#else
#define sharing_actors_DllAPI
#endif
#else
#define sharing_actors_DllAPI
#endif // _WIN32

namespace eprosima
{
    namespace fastcdr
    {
        class Cdr;
    }
}

/*!
 * @brief This class represents the structure SharingActors defined by the user in the IDL file.
 * @ingroup SHARING_ACTORS
 */
class SharingActors
{
public:

    /*!
     * @brief Default constructor.
     */
    eProsima_user_DllExport SharingActors();

    /*!
     * @brief Default destructor.
     */
    eProsima_user_DllExport virtual ~SharingActors();

    /*!
     * @brief Copy constructor.
     * @param x Reference to the object SharingActors that will be copied.
     */
    eProsima_user_DllExport SharingActors(const SharingActors &x);

    /*!
     * @brief Move constructor.
     * @param x Reference to the object SharingActors that will be copied.
     */
    eProsima_user_DllExport SharingActors(SharingActors &&x);

    /*!
     * @brief Copy assignment.
     * @param x Reference to the object SharingActors that will be copied.
     */
    eProsima_user_DllExport SharingActors& operator=(const SharingActors &x);

    /*!
     * @brief Move assignment.
     * @param x Reference to the object SharingActors that will be copied.
     */
    eProsima_user_DllExport SharingActors& operator=(SharingActors &&x);

    /*!
     * @brief This function sets a value in member sendId
     * @param _sendId New value for member sendId
     */
    eProsima_user_DllExport void sendId(int16_t _sendId);

    /*!
     * @brief This function returns the value of member sendId
     * @return Value of member sendId
     */
    eProsima_user_DllExport int16_t sendId() const;

    /*!
     * @brief This function returns a reference to member sendId
     * @return Reference to member sendId
     */
    eProsima_user_DllExport int16_t& sendId();
    /*!
     * @brief This function sets a value in member receId
     * @param _receId New value for member receId
     */
    eProsima_user_DllExport void receId(int16_t _receId);

    /*!
     * @brief This function returns the value of member receId
     * @return Value of member receId
     */
    eProsima_user_DllExport int16_t receId() const;

    /*!
     * @brief This function returns a reference to member receId
     * @return Reference to member receId
     */
    eProsima_user_DllExport int16_t& receId();
    /*!
     * @brief This function sets a value in member swarmTag
     * @param _swarmTag New value for member swarmTag
     */
    eProsima_user_DllExport void swarmTag(int16_t _swarmTag);

    /*!
     * @brief This function returns the value of member swarmTag
     * @return Value of member swarmTag
     */
    eProsima_user_DllExport int16_t swarmTag() const;

    /*!
     * @brief This function returns a reference to member swarmTag
     * @return Reference to member swarmTag
     */
    eProsima_user_DllExport int16_t& swarmTag();
    /*!
     * @brief This function copies the value in member actorTags
     * @param _actorTags New value to be copied in member actorTags
     */
    eProsima_user_DllExport void actorTags(const std::vector<int16_t> &_actorTags);

    /*!
     * @brief This function moves the value in member actorTags
     * @param _actorTags New value to be moved in member actorTags
     */
    eProsima_user_DllExport void actorTags(std::vector<int16_t> &&_actorTags);

    /*!
     * @brief This function returns a constant reference to member actorTags
     * @return Constant reference to member actorTags
     */
    eProsima_user_DllExport const std::vector<int16_t>& actorTags() const;

    /*!
     * @brief This function returns a reference to member actorTags
     * @return Reference to member actorTags
     */
    eProsima_user_DllExport std::vector<int16_t>& actorTags();

    /*!
     * @brief This function returns the maximum serialized size of an object
     * depending on the buffer alignment.
     * @param current_alignment Buffer alignment.
     * @return Maximum serialized size.
     */
    eProsima_user_DllExport static size_t getMaxCdrSerializedSize(size_t current_alignment = 0);

    /*!
     * @brief This function returns the serialized size of a data depending on the buffer alignment.
     * @param data Data which is calculated its serialized size.
     * @param current_alignment Buffer alignment.
     * @return Serialized size.
     */
    eProsima_user_DllExport static size_t getCdrSerializedSize(const SharingActors& data, size_t current_alignment = 0);


    /*!
     * @brief This function serializes an object using CDR serialization.
     * @param cdr CDR serialization object.
     */
    eProsima_user_DllExport void serialize(eprosima::fastcdr::Cdr &cdr) const;

    /*!
     * @brief This function deserializes an object using CDR serialization.
     * @param cdr CDR serialization object.
     */
    eProsima_user_DllExport void deserialize(eprosima::fastcdr::Cdr &cdr);



    /*!
     * @brief This function returns the maximum serialized size of the Key of an object
     * depending on the buffer alignment.
     * @param current_alignment Buffer alignment.
     * @return Maximum serialized size.
     */
    eProsima_user_DllExport static size_t getKeyMaxCdrSerializedSize(size_t current_alignment = 0);

    /*!
     * @brief This function tells you if the Key has been defined for this type
     */
    eProsima_user_DllExport static bool isKeyDefined();

    /*!
     * @brief This function serializes the key members of an object using CDR serialization.
     * @param cdr CDR serialization object.
     */
    eProsima_user_DllExport void serializeKey(eprosima::fastcdr::Cdr &cdr) const;

 eProsima_user_DllExport static std::string getName() { return "SharingActors"; } 
private:
    int16_t m_sendId;
    int16_t m_receId;
    int16_t m_swarmTag;
    std::vector<int16_t> m_actorTags;
};

#endif // _SHARING_ACTORS_H_