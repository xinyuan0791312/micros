/* ==================================================================
*
* Copyright (c) 2016-2019, micROS Group, NIIDT, TAIIC, HPCL.
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions 
* are met: 
*
* 1. Redistributions of source code must retain the above copyright 
*     notice, this list of conditions and the following disclaimer. 
* 2. Redistributions in binary form must reproduce the above copyright 
*     notice, this list of conditions and the following disclaimer in the 
*     documentation and/or other materials provided with the 
*     distribution. 
* 3. All advertising materials mentioning features or use of this software 
*     must display the following acknowledgement: 
*     This product includes software developed by the micROS Group. and 
*     its contributors. 
* 4. Neither the name of the Group nor the names of its contributors may 
*     be used to endorse or promote products derived from this software 
*     without specific prior written permission. 
*
* THIS SOFTWARE IS PROVIDED BY MICROS,GROUP AND CONTRIBUTORS
* ''AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT
* NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
* FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL 
* THE MICROS,GROUP OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS 
* OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) 
* HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
* OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN 
* IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
* ===================================================================
*
* Author: Bin Di, Chunlian Fu, Junling Gao, Ruihao Li, Wei Yi.
*         (in alphabetical order)
*
*/

#include "swarm_actor_sharing/swarm_actor_sharing_rtps.h"
#include "swarm_data_storage/swarm_data_storage.h"
#include "actor_tag_store/ActorTagStore.h"

SwarmActorSharing_RTPS::SwarmActorSharing_RTPS(boost::shared_ptr<ActorScheduler> pScheduler){
    _pActorScheduler = pScheduler;
    _robotID = SwarmDataStorage::instance()->getLocalRobotID();
    _pPubRunningActors = new RTPSPublisher<SharingActors>("/sharing_actors");
    _pSubRunningActors = new RTPSSubscriber<SharingActors>("/sharing_actors", &SwarmActorSharing_RTPS::runningActorsCallback, this);
    _pthRunningActors = boost::shared_ptr<boost::thread>(new boost::thread(boost::bind(&SwarmActorSharing_RTPS::sendRunningActorsLoop,this)));
    _pthRunningActors->detach();
}

SwarmActorSharing_RTPS::~SwarmActorSharing_RTPS(){
    _pActorScheduler.reset();

    if(_pthRunningActors){
        _pthRunningActors->join();
        _pthRunningActors->interrupt();
        _pthRunningActors.reset();
    }
    if(_pPubRunningActors){
        delete _pPubRunningActors;
        _pPubRunningActors = NULL;
    }
    if(_pSubRunningActors){
        delete _pSubRunningActors;
        _pSubRunningActors = NULL;
    }
}

void SwarmActorSharing_RTPS::pubRunningActors(const int aDestID){
    std::string swarmName = "";
    std::vector<std::string> vActors;
    SwarmDataStorage::instance()->getRobotActorsByID(_robotID, swarmName, vActors);
    if(swarmName.empty()) return;

    SharingActors msg;
    msg.sendId(_robotID);
    msg.receId(aDestID);
    msg.swarmTag(gActorTagStore.swarmName2Tag(swarmName));
    for(auto it = vActors.begin(); it != vActors.end(); it++){
        msg.actorTags().push_back(gActorTagStore.actorName2Tag(swarmName, *it));
    }

    boost::unique_lock<boost::mutex> lock(_pPubRunningActorsMutex);
    _pPubRunningActors->publish(msg);
    lock.unlock();

#ifdef ACTOR_SWARM_LOG_OUT
    std::string logStr = std::to_string(SharingActors::getCdrSerializedSize(msg)) + ",SharingActors";
    SwarmDataStorage::instance()->logOut2File("netsend", logStr);
#endif
}

void SwarmActorSharing_RTPS::sendRunningActorsLoop(){
    while(1){
        // read running actors
        std::string runningSwarmName = "";
        std::vector<std::string> vRActors;
        _pActorScheduler->getRunningSwarmActors(runningSwarmName, vRActors);

         // read local robot actors
        std::string sLSwarmName = "";
        std::vector<std::string> vLActors;
        SwarmDataStorage::instance()->getRobotActorsByID(_robotID, sLSwarmName, vLActors);

        //compare with the local actors info
        bool bChanged = false;
        if ((sLSwarmName.compare(runningSwarmName) != 0) || (vLActors.size() != vRActors.size())) {
            bChanged = true;
        } else{
            for (std::vector<std::string>::iterator it = vLActors.begin(); it != vLActors.end(); it++){
                if(std::find(vRActors.begin(), vRActors.end(), *it) == vRActors.end()){
                    bChanged = true;
                    break;
                }
            }
        }

        //if actors info changed, save and put
        if (bChanged) {
            ROS_INFO("[SwarmActorSharing_RTPS::sendRunningActorsLoop] changed, swarmName=%s, vRActors.size=%lu", runningSwarmName.c_str(), vRActors.size());
            SwarmDataStorage::instance()->setRobotActors(_robotID, runningSwarmName, vRActors);
            
            pubRunningActors();
        }
        sleep(1);
    }

}

void SwarmActorSharing_RTPS::runningActorsCallback(SharingActors& msg){
    if(msg.sendId() == _robotID) return;
#ifdef ACTOR_SWARM_LOG_OUT
    std::string logStr = std::to_string(SharingActors::getCdrSerializedSize(msg)) + ",SharingActors";
    SwarmDataStorage::instance()->logOut2File("netreceive", logStr);
#endif

    if(msg.receId() != -1 && msg.receId() != _robotID) return;

    std::string swarmName = gActorTagStore.swarmTag2Name(msg.swarmTag());
    std::vector<std::string> vActors;
    for(auto it = msg.actorTags().begin(); it != msg.actorTags().end(); it++){
        vActors.push_back(gActorTagStore.actorTag2Name(swarmName, *it));
    }
    SwarmDataStorage::instance()->setRobotActors(msg.sendId(), swarmName, vActors);
}