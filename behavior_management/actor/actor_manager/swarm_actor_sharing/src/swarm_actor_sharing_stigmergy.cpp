/* ==================================================================
*
* Copyright (c) 2016-2019, micROS Group, NIIDT, TAIIC, HPCL.
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions 
* are met: 
*
* 1. Redistributions of source code must retain the above copyright 
*     notice, this list of conditions and the following disclaimer. 
* 2. Redistributions in binary form must reproduce the above copyright 
*     notice, this list of conditions and the following disclaimer in the 
*     documentation and/or other materials provided with the 
*     distribution. 
* 3. All advertising materials mentioning features or use of this software 
*     must display the following acknowledgement: 
*     This product includes software developed by the micROS Group. and 
*     its contributors. 
* 4. Neither the name of the Group nor the names of its contributors may 
*     be used to endorse or promote products derived from this software 
*     without specific prior written permission. 
*
* THIS SOFTWARE IS PROVIDED BY MICROS,GROUP AND CONTRIBUTORS
* ''AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT
* NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
* FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL 
* THE MICROS,GROUP OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS 
* OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) 
* HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
* OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN 
* IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
* ===================================================================
*
* Author: Bin Di, Chunlian Fu, Junling Gao, Ruihao Li, Wei Yi.
*         (in alphabetical order)
*
*/

#include "swarm_actor_sharing/swarm_actor_sharing_stigmergy.h"
#include "swarm_data_storage/swarm_data_storage.h"
#include "actor_msgs/SwarmActors.h"
#include "actor_tag_store/ActorTagStore.h"

SwarmActorSharingStigmergy::SwarmActorSharingStigmergy(boost::shared_ptr<ActorScheduler> pScheduler){
    _pActorScheduler = pScheduler;
    _robotID = SwarmDataStorage::instance()->getLocalRobotID();
    _vs = micros_swarm::VirtualStigmergy(4);

    _pthCheckActors = boost::shared_ptr<boost::thread>(new boost::thread(boost::bind(&SwarmActorSharingStigmergy::checkSwarmActorsLoop,this)));
    _pthUpdateActors = boost::shared_ptr<boost::thread>(new boost::thread(boost::bind(&SwarmActorSharingStigmergy::updateLocalSwarmActorsLoop,this)));
}

SwarmActorSharingStigmergy::~SwarmActorSharingStigmergy(){
    _pActorScheduler.reset();

    if(_pthCheckActors){
        _pthCheckActors->join();
        _pthCheckActors->interrupt();
        _pthCheckActors.reset();
    }

    if(_pthUpdateActors){
        _pthUpdateActors->join();
        _pthUpdateActors->interrupt();
        _pthUpdateActors.reset();
    }
}

void SwarmActorSharingStigmergy::pubRunningActors(const int aDestID){
    //put swarm actors on stigmergy '1' time in every '10' ms, '10' makes on sense if the first param is '1'
    putSwarmActors(1, 10);
}

// put local robot actors to stigmergy blackboard
// need to start a thread if aNum>1
void SwarmActorSharingStigmergy::putSwarmActors(const int32_t aNum, const int32_t aDuration){
    // read local robot actors
    std::string swarmName = "";
    std::vector<std::string> actors;
    SwarmDataStorage::instance()->getRobotActorsByID(_robotID, swarmName, actors);
    if(swarmName.empty()) {
        return;
    }

    // construct msg
    actor_msgs::SwarmActors msgSwarmActors;
    msgSwarmActors.robotID = _robotID;
    msgSwarmActors.swarmNameTag = gActorTagStore.swarmName2Tag(swarmName);
    for (int i = 0; i < actors.size(); i++) {
        msgSwarmActors.actorsTag.push_back(gActorTagStore.actorName2Tag(swarmName, actors[i]));
    }

    // put msg to stigmergy
    std::string cmdKey = getCmdKey(_robotID);
    for (int i = 0; i < aNum; i++) {
        {
            boost::unique_lock<boost::mutex> lock(_vsMutex);
            _vs.put<actor_msgs::SwarmActors>(cmdKey, msgSwarmActors);
        }
        if (i < (aNum - 1))
            usleep(aDuration * 1000);
    }
}

// get robot actors from stigmergy blackboard
bool SwarmActorSharingStigmergy::getSwarmActors(const int32_t aRobotID, std::string& anSwarmName, std::vector<std::string>& anActors) {
    // read from stigmergy
    std::string cmdKey = getCmdKey(aRobotID);
    actor_msgs::SwarmActors msgSwarmActors;
    boost::unique_lock<boost::mutex> lock(_vsMutex);
    bool bSuccess = _vs.get_or_query<actor_msgs::SwarmActors>(cmdKey, msgSwarmActors);
    lock.unlock();
 
    //convert to string
    if (bSuccess) {
        if (msgSwarmActors.swarmNameTag == -1) {
            anSwarmName.clear();
            anActors.clear();
        } else {
            anSwarmName = gActorTagStore.swarmTag2Name(msgSwarmActors.swarmNameTag);
            anActors.clear();
            for (int i = 0; i < msgSwarmActors.actorsTag.size(); i++) {
                anActors.push_back(gActorTagStore.actorTag2Name(anSwarmName, msgSwarmActors.actorsTag[i]));
            }
        }
    }
    return bSuccess;
}

// read actors running on this robot,and if change, put to stigmergy
void SwarmActorSharingStigmergy::updateLocalSwarmActors() {
    // read running actors
    std::string runningSwarmName = "";
    std::vector<std::string> vRActors;
    _pActorScheduler->getRunningSwarmActors(runningSwarmName, vRActors);

    // read local robot actors
    std::string sLSwarmName = "";
    std::vector<std::string> vLActors;
    SwarmDataStorage::instance()->getRobotActorsByID(_robotID, sLSwarmName, vLActors);

    //compare with the local actors info
    bool bChanged = false;
    if ((sLSwarmName.compare(runningSwarmName) != 0) || (vLActors.size() != vRActors.size())) {
        bChanged = true;
    } else{
        for (std::vector<std::string>::iterator it = vLActors.begin(); it != vLActors.end(); it++){
            if(std::find(vRActors.begin(), vRActors.end(), *it) == vRActors.end()){
                bChanged = true;
                break;
            }
        }
    }

    //if actors info changed, save and put
    if (bChanged) {
        ROS_INFO("[SwarmActorSharingStigmergy::putSwarmActors] changed, swarmName=%s, vRActors.size=%u", runningSwarmName.c_str(), vRActors.size());
        SwarmDataStorage::instance()->setRobotActors(_robotID, runningSwarmName, vRActors);
        pubRunningActors();
    }
}

void SwarmActorSharingStigmergy::updateLocalSwarmActorsLoop() {
    while (true) {
        updateLocalSwarmActors();
        usleep(1000 * 1000); //1s
    }
}

// read actors from stigmergy, and if change, update local swarminformation
void SwarmActorSharingStigmergy::checkSwarmActors(){
    // get stigmergy data
    std::map<std::string, micros_swarm::VirtualStigmergyTuple> vs_map;
    boost::unique_lock<boost::mutex> vsLock(_vsMutex);
    _vs.get_vs(vs_map);
    vsLock.unlock();

    if(vs_map.empty()){
        //ROS_WARN("[SwarmActorSharingStigmergy] vs map is empty, no members in the swarm, return..");
		return;
    }

    int8_t robotID = 0;
    std::string swarmName = "";
    std::vector<std::string> actors;

    boost::unique_lock<boost::mutex> rcLock(_rcMutex);
    for(std::map<std::string, micros_swarm::VirtualStigmergyTuple>::iterator it = vs_map.begin(); it!=vs_map.end(); it++){
        
        // deserialize
        actor_msgs::SwarmActors msgSwarmActors = micros_swarm::deserialize_ros<actor_msgs::SwarmActors>(it->second.vstig_value);
        robotID = msgSwarmActors.robotID; 

        // if actors has been changed, then update local SwarmDataStorage
        if(_robotID2Clock.find(robotID) == _robotID2Clock.end() || _robotID2Clock[robotID] != it->second.lamport_clock){
            swarmName = gActorTagStore.swarmTag2Name(msgSwarmActors.swarmNameTag);
            actors.clear();
            for (int i = 0; i < msgSwarmActors.actorsTag.size(); i++) {
                actors.push_back(gActorTagStore.actorTag2Name(swarmName, msgSwarmActors.actorsTag[i]));
            }
            SwarmDataStorage::instance()->setRobotActors(robotID, swarmName, actors);
        }

        // update local lamport clock
        _robotID2Clock[robotID] = it->second.lamport_clock;
    }
}

void SwarmActorSharingStigmergy::checkSwarmActorsLoop(){
    while (true) {
        checkSwarmActors();
        usleep(1000 * 1000); //1s
    }
}


std::string SwarmActorSharingStigmergy::getCmdKey(const int32_t aRobotID){
    return "robot_" + std::to_string(aRobotID) + "_swarm_actors";
}