/* ==================================================================
*
* Copyright (c) 2016-2019, micROS Group, NIIDT, TAIIC, HPCL.
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions 
* are met: 
*
* 1. Redistributions of source code must retain the above copyright 
*     notice, this list of conditions and the following disclaimer. 
* 2. Redistributions in binary form must reproduce the above copyright 
*     notice, this list of conditions and the following disclaimer in the 
*     documentation and/or other materials provided with the 
*     distribution. 
* 3. All advertising materials mentioning features or use of this software 
*     must display the following acknowledgement: 
*     This product includes software developed by the micROS Group. and 
*     its contributors. 
* 4. Neither the name of the Group nor the names of its contributors may 
*     be used to endorse or promote products derived from this software 
*     without specific prior written permission. 
*
* THIS SOFTWARE IS PROVIDED BY MICROS,GROUP AND CONTRIBUTORS
* ''AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT
* NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
* FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL 
* THE MICROS,GROUP OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS 
* OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) 
* HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
* OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN 
* IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
* ===================================================================
*
* Author: Bin Di, Chunlian Fu, Junling Gao, Ruihao Li, Wei Yi.
*         (in alphabetical order)
*
*/

#ifndef __SWARM_ROBOT_HEARTBEAT_STIGMERGY_LATENCY_H__
#define __SWARM_ROBOT_HEARTBEAT_STIGMERGY_LATENCY_H__
#include "swarm_robot_heartbeat_strategy.h"
#include <boost/accumulators/accumulators.hpp>
#include <boost/accumulators/statistics/stats.hpp>
#include <boost/accumulators/statistics/rolling_window.hpp>
#include <boost/accumulators/statistics/rolling_variance.hpp>
#include <boost/accumulators/statistics/rolling_mean.hpp>
#include <boost/math/distributions/normal.hpp>
#include <boost/math/distributions/exponential.hpp>
#include <boost/date_time/posix_time/posix_time.hpp> 
using namespace boost::accumulators;

//  heart beat stragety,compute latency data by rolling window
class SwarmRobotHeartbeatStrategyLatency: public SwarmRobotHeartbeatStrategy{
public:
	SwarmRobotHeartbeatStrategyLatency();
	~SwarmRobotHeartbeatStrategyLatency();
	virtual void processHeartbeatMsg(const int aRobotID, const bool aIsFromMaster);

protected:
	void checkRobotStateLoop();

private:
    // thread to check robot state
	boost::shared_ptr<boost::thread> _pthCheckRobotState;

	// struct contains heartbeat data and statistic objects
	typedef accumulator_set<double, stats<tag::rolling_mean, tag::rolling_variance, tag::rolling_count, tag::rolling_sum > > ROLLINGSET;
	struct HBLatencyDistribution{
		int32_t _state;					
		boost::posix_time::ptime _lastHBTime; 
		ROLLINGSET* _acc = NULL;
		boost::math::normal_distribution<>* _normal = NULL;
		boost::math::exponential_distribution<>* _exponential = NULL;
		void release(){
			if(_acc){
				delete _acc;
				_acc = NULL;
			}
			if(_normal){
				delete _normal;
				_normal = NULL;
			}
			if(_exponential){
				delete _exponential;
				_exponential = NULL;
			}
		}
	};

	// contains all robots hearbeat latency data in the swarm
    std::map<int32_t, HBLatencyDistribution> _robotID2HBLatency;
    boost::mutex _robotID2HBLatencyMutex;
};
#endif
