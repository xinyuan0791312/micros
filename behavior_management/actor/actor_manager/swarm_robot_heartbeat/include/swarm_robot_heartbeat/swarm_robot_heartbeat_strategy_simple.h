/* ==================================================================
*
* Copyright (c) 2016-2019, micROS Group, NIIDT, TAIIC, HPCL.
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions 
* are met: 
*
* 1. Redistributions of source code must retain the above copyright 
*     notice, this list of conditions and the following disclaimer. 
* 2. Redistributions in binary form must reproduce the above copyright 
*     notice, this list of conditions and the following disclaimer in the 
*     documentation and/or other materials provided with the 
*     distribution. 
* 3. All advertising materials mentioning features or use of this software 
*     must display the following acknowledgement: 
*     This product includes software developed by the micROS Group. and 
*     its contributors. 
* 4. Neither the name of the Group nor the names of its contributors may 
*     be used to endorse or promote products derived from this software 
*     without specific prior written permission. 
*
* THIS SOFTWARE IS PROVIDED BY MICROS,GROUP AND CONTRIBUTORS
* ''AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT
* NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
* FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL 
* THE MICROS,GROUP OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS 
* OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) 
* HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
* OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN 
* IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
* ===================================================================
*
* Author: Bin Di, Chunlian Fu, Junling Gao, Ruihao Li, Wei Yi.
*         (in alphabetical order)
*
*/

#ifndef __SWARM_ROBOT_HEARTBEAT_STIGMERGY_SIMPLE_H__
#define __SWARM_ROBOT_HEARTBEAT_STIGMERGY_SIMPLE_H__
#include "swarm_robot_heartbeat_strategy.h"
#include "swarm_data_storage/swarm_data_storage.h"
#include <map>
#include <boost/thread/thread.hpp>
#include <boost/thread/mutex.hpp>

struct RobotStateCount{
	int32_t _state;//ROBOT_STATE_JOIN or ROBOT_STATE_STAY
	int32_t _count;
	int32_t _timeOut;
};
//a simple heart beat stragety
//a robot is considered to leave the group if cannot receive its heart beat msg in _hbTimeOut(s) 
//a robot is consider to join the group if _robotJoinHBCount heart beats were received in _robotJoinTimeOut (s)
class SwarmRobotHeartbeatStrategySimple: public SwarmRobotHeartbeatStrategy{
public:
	SwarmRobotHeartbeatStrategySimple();
	~SwarmRobotHeartbeatStrategySimple();
	virtual void processHeartbeatMsg(const int aRobotID, const bool aIsFromMaster);

protected:
	void checkRobotStateLoop();

private:
    int32_t _hbTimeOutCount; //counter for robot leave time out
	int32_t _robotJoinTimeOutCount;//counter for robot join time out
	boost::shared_ptr<boost::thread> _pthCheckRobotState;

	//map stores robot state count info, robotID-->RobotStateCount
	std::map<int32_t, RobotStateCount> _robotID2StateCount;
	boost::mutex _robotID2StateCountMutex;

#ifdef ACTOR_SWARM_LOG_OUT	
	std::map<int32_t, std::vector<ros::Time> > _robotID2HBTime;// for debug
	std::map<int32_t, std::vector<ros::Time> > _robotID2JoinTime;// for debug
#endif
};
#endif
