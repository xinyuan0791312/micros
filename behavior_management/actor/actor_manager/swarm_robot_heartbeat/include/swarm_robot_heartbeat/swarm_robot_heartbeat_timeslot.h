/* ==================================================================
*
* Copyright (c) 2016-2019, micROS Group, NIIDT, TAIIC, HPCL.
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions 
* are met: 
*
* 1. Redistributions of source code must retain the above copyright 
*     notice, this list of conditions and the following disclaimer. 
* 2. Redistributions in binary form must reproduce the above copyright 
*     notice, this list of conditions and the following disclaimer in the 
*     documentation and/or other materials provided with the 
*     distribution. 
* 3. All advertising materials mentioning features or use of this software 
*     must display the following acknowledgement: 
*     This product includes software developed by the micROS Group. and 
*     its contributors. 
* 4. Neither the name of the Group nor the names of its contributors may 
*     be used to endorse or promote products derived from this software 
*     without specific prior written permission. 
*
* THIS SOFTWARE IS PROVIDED BY MICROS,GROUP AND CONTRIBUTORS
* ''AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT
* NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
* FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL 
* THE MICROS,GROUP OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS 
* OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) 
* HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
* OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN 
* IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
* ===================================================================
*
* Author: Bin Di, Chunlian Fu, Junling Gao, Ruihao Li, Wei Yi.
*         (in alphabetical order)
*
*/

#ifndef __SWARM_ROBOT_HEARTBEAT_TIMESLOT_H__
#define __SWARM_ROBOT_HEARTBEAT_TIMESLOT_H__
#include "swarm_robot_heartbeat.h"
#include "Heartbeat.h"
#include "ros/timer.h"
class SwarmRobotHeartbeat_TimeSlot:public SwarmRobotHeartbeat{
public:
	SwarmRobotHeartbeat_TimeSlot();
	virtual ~SwarmRobotHeartbeat_TimeSlot();
	virtual void run();
	void updateSlot(const int aSlotIndex, const int aSlotCount);

private:
	void heartbeatCallback(Heartbeat& aHB);
	void timerCallback(const ros::TimerEvent& aEvent);

	void checkInitialRobotIDs();

private:
	RTPSPublisher<Heartbeat>* _pPubHeartbeat;
    RTPSSubscriber<Heartbeat>* _pSubHeartbeat;

	// thread for check intialRobotIDs
	boost::shared_ptr<boost::thread> _pthCheckInitialRobotIDs;

	ros::Timer _timer;
	int _slot;
	boost::mutex _slotMutex;
};

#endif