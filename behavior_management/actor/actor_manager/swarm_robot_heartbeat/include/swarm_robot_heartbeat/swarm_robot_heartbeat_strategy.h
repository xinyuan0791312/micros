/* ==================================================================
*
* Copyright (c) 2016-2019, micROS Group, NIIDT, TAIIC, HPCL.
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions 
* are met: 
*
* 1. Redistributions of source code must retain the above copyright 
*     notice, this list of conditions and the following disclaimer. 
* 2. Redistributions in binary form must reproduce the above copyright 
*     notice, this list of conditions and the following disclaimer in the 
*     documentation and/or other materials provided with the 
*     distribution. 
* 3. All advertising materials mentioning features or use of this software 
*     must display the following acknowledgement: 
*     This product includes software developed by the micROS Group. and 
*     its contributors. 
* 4. Neither the name of the Group nor the names of its contributors may 
*     be used to endorse or promote products derived from this software 
*     without specific prior written permission. 
*
* THIS SOFTWARE IS PROVIDED BY MICROS,GROUP AND CONTRIBUTORS
* ''AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT
* NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
* FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL 
* THE MICROS,GROUP OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS 
* OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) 
* HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
* OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN 
* IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
* ===================================================================
*
* Author: Bin Di, Chunlian Fu, Junling Gao, Ruihao Li, Wei Yi.
*         (in alphabetical order)
*
*/

#ifndef __SWARM_ROBOT_HEARTBEAT_STRATEGY_H__
#define __SWARM_ROBOT_HEARTBEAT_STRATEGY_H__
#include "swarm_robot_heartbeat_handler/swarm_robot_heartbeat_handler.h"

#define HB_CHECK_PERIOD 100   //check the robot state in HB_CHECK_PERIOD (ms)
//robot is JOINing the group
const int32_t ROBOT_STATE_JOIN = 1;
//robot STAY in the group
const int32_t ROBOT_STATE_STAY = 2;

//base class for heart beat handling strategy
class SwarmRobotHeartbeatStrategy{
public:
	SwarmRobotHeartbeatStrategy();
    virtual ~SwarmRobotHeartbeatStrategy();
	virtual void processHeartbeatMsg(const int aRobotID, const bool aIsFromMaster)=0;
	void initHeatbeatHandler(SwarmRobotHeartbeatHandler* aHandler){
		_pHandler = aHandler;
	}

protected:
	//strategy judge if a robot is join/leave, and send a join/leave msg to handler
	void sendRobotJoinMsg(const int aRobotID);
	void sendRobotLeaveMsg(const int aRobotID);

protected:
	//variables which may be useful in most strategies
	//heartbeat time out(robot leave), 
	//a robot is considered to leave the group if cannot receive its heart beat msg in _hbTimeOut(s) 
	int32_t _hbTimeOut;
	//heart beat counting when robot joining the group
	//a robot is consider to join the group if _robotJoinHBCount heart beats were received in _robotJoinTimeOut (s)
	int32_t _robotJoinHBCount;
	int32_t _robotJoinTimeOut;
private:
	//handler to process robot join/leave msg
	SwarmRobotHeartbeatHandler* _pHandler;
};
#endif
