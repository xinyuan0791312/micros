/* ==================================================================
*
* Copyright (c) 2016-2019, micROS Group, NIIDT, TAIIC, HPCL.
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions 
* are met: 
*
* 1. Redistributions of source code must retain the above copyright 
*     notice, this list of conditions and the following disclaimer. 
* 2. Redistributions in binary form must reproduce the above copyright 
*     notice, this list of conditions and the following disclaimer in the 
*     documentation and/or other materials provided with the 
*     distribution. 
* 3. All advertising materials mentioning features or use of this software 
*     must display the following acknowledgement: 
*     This product includes software developed by the micROS Group. and 
*     its contributors. 
* 4. Neither the name of the Group nor the names of its contributors may 
*     be used to endorse or promote products derived from this software 
*     without specific prior written permission. 
*
* THIS SOFTWARE IS PROVIDED BY MICROS,GROUP AND CONTRIBUTORS
* ''AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT
* NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
* FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL 
* THE MICROS,GROUP OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS 
* OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) 
* HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
* OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN 
* IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
* ===================================================================
*
* Author: Bin Di, Chunlian Fu, Junling Gao, Ruihao Li, Wei Yi.
*         (in alphabetical order)
*
*/

#ifndef __HEARTBEAT_H__
#define __HEARTBEAT_H__

#include <stdint.h>
#include <string>
#include <fastcdr/Cdr.h>
namespace eprosima
{
    namespace fastcdr
    {
        class Cdr;
    }
}
// fastrtps heartbeat msg
class Heartbeat{
public:
    Heartbeat(){
        _sendID = 0;
        _isMaster = 0; 
    }

    ~Heartbeat(){}

    void sendID(int16_t aSendID){
        _sendID = aSendID; 
    }

    int16_t sendID(){ 
        return _sendID; 
    }

    void isMaster(uint8_t aIsMaster){ 
        _isMaster = aIsMaster; 
    } 

    uint64_t isMaster(){
        return _isMaster;
    }

    static size_t getMaxCdrSerializedSize(size_t current_alignment = 0){
        size_t initial_alignment = current_alignment;
        current_alignment += 2 + eprosima::fastcdr::Cdr::alignment(current_alignment, 2);
        current_alignment += 1 + eprosima::fastcdr::Cdr::alignment(current_alignment, 1);
        return current_alignment - initial_alignment;
    }

    static size_t getCdrSerializedSize(const Heartbeat& data, size_t current_alignment = 0){
        size_t initial_alignment = current_alignment;    
        current_alignment += 2 + eprosima::fastcdr::Cdr::alignment(current_alignment, 2);
        current_alignment += 1 + eprosima::fastcdr::Cdr::alignment(current_alignment, 1);
        return current_alignment - initial_alignment;
    }

    void serialize(eprosima::fastcdr::Cdr &cdr) const{
        cdr<<_sendID;
        cdr<<_isMaster;
    }

    void deserialize(eprosima::fastcdr::Cdr &cdr){
        cdr>>_sendID;
        cdr>>_isMaster;
    }

    static size_t getKeyMaxCdrSerializedSize(size_t current_alignment = 0){
        return current_alignment;
    }

    static bool isKeyDefined(){
        return false;
    }

    void serializeKey(eprosima::fastcdr::Cdr &cdr) const{}

    static std::string getName() { return "Heartbeat"; } 
private:
    int16_t _sendID;
    uint8_t _isMaster;
};

#endif