/* ==================================================================
*
* Copyright (c) 2016-2019, micROS Group, NIIDT, TAIIC, HPCL.
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions 
* are met: 
*
* 1. Redistributions of source code must retain the above copyright 
*     notice, this list of conditions and the following disclaimer. 
* 2. Redistributions in binary form must reproduce the above copyright 
*     notice, this list of conditions and the following disclaimer in the 
*     documentation and/or other materials provided with the 
*     distribution. 
* 3. All advertising materials mentioning features or use of this software 
*     must display the following acknowledgement: 
*     This product includes software developed by the micROS Group. and 
*     its contributors. 
* 4. Neither the name of the Group nor the names of its contributors may 
*     be used to endorse or promote products derived from this software 
*     without specific prior written permission. 
*
* THIS SOFTWARE IS PROVIDED BY MICROS,GROUP AND CONTRIBUTORS
* ''AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT
* NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
* FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL 
* THE MICROS,GROUP OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS 
* OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) 
* HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
* OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN 
* IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
* ===================================================================
*
* Author: Bin Di, Chunlian Fu, Junling Gao, Ruihao Li, Wei Yi.
*         (in alphabetical order)
*
*/

#include "swarm_robot_heartbeat/swarm_robot_heartbeat_strategy_latency.h"
#include "swarm_data_storage/swarm_data_storage.h"

const int WINDOWSIZE = 10;
const double NORMAL_CONFIDENCE = 0.998;
SwarmRobotHeartbeatStrategyLatency::SwarmRobotHeartbeatStrategyLatency(){
    //start the robot state check loop
    _pthCheckRobotState = boost::shared_ptr<boost::thread>(new boost::thread(boost::bind(&SwarmRobotHeartbeatStrategyLatency::checkRobotStateLoop, this)));    
}

SwarmRobotHeartbeatStrategyLatency::~SwarmRobotHeartbeatStrategyLatency(){
    if(_pthCheckRobotState){
        _pthCheckRobotState->join();
        _pthCheckRobotState->interrupt();
        _pthCheckRobotState.reset();
    }
}

void SwarmRobotHeartbeatStrategyLatency::processHeartbeatMsg(const int aRobotID, const bool aIsFromMaster){
    boost::unique_lock<boost::mutex> lock(_robotID2HBLatencyMutex);
    boost::posix_time::ptime time_now = boost::posix_time::microsec_clock::universal_time();  

    // construct a HBLatencyDistribution object for the aRobotID
    if(_robotID2HBLatency.count(aRobotID) == 0 || _robotID2HBLatency[aRobotID]._lastHBTime.is_special()){
        _robotID2HBLatency[aRobotID]._state = ROBOT_STATE_JOIN;
        _robotID2HBLatency[aRobotID]._acc = new ROLLINGSET(tag::rolling_window::window_size = WINDOWSIZE);
        SwarmDataStorage::instance()->setRobotHBLatencyStatisticConfidence(aRobotID, NORMAL_CONFIDENCE);

        ROS_INFO("[SwarmRobotHeartbeatStrategyLatency] new heartbeat[%d]", aRobotID);
        #ifdef ACTOR_SWARM_LOG_OUT
        std::string eventStr = "newHB:" + std::to_string(aRobotID);
        SwarmDataStorage::instance()->logOut2File("event", eventStr);
        #endif

    } else {
        // add heartbeat latency data to rolling set
        int ticks = (time_now - _robotID2HBLatency[aRobotID]._lastHBTime).ticks(); //us
        (*(_robotID2HBLatency[aRobotID]._acc))(ticks);

        // caculate mean value and variance value
        double meanVal = rolling_mean((*(_robotID2HBLatency[aRobotID]._acc)));
        double varianceVal = rolling_variance((*(_robotID2HBLatency[aRobotID]._acc)));

        //TODO set params dynamically
        if(varianceVal > 0){
            // update distribution params
            double stddev = sqrtf(varianceVal);
            if(_robotID2HBLatency[aRobotID]._normal){
                delete _robotID2HBLatency[aRobotID]._normal;
            }
            if(_robotID2HBLatency[aRobotID]._exponential){
                delete _robotID2HBLatency[aRobotID]._exponential;
            }
            _robotID2HBLatency[aRobotID]._normal = new boost::math::normal_distribution<>(meanVal, stddev);
            _robotID2HBLatency[aRobotID]._exponential = new boost::math::exponential_distribution<>(1/meanVal);

            // save statistic params
            double confidence = SwarmDataStorage::instance()->getRobotHBLatencyStatisticConfidence(aRobotID);
            double quantile = boost::math::quantile(*(_robotID2HBLatency[aRobotID]._normal), confidence);
            SwarmDataStorage::instance()->setRobotHBLatencyStatistic(aRobotID, meanVal, stddev, quantile);
        }
    }

    // update last heartbeat time
    _robotID2HBLatency[aRobotID]._lastHBTime = time_now;
    lock.unlock();

    // log
    #ifdef ACTOR_SWARM_LOG_OUT
    SwarmDataStorage::instance()->logOutHB2File(aRobotID,1);
    #endif
}

void SwarmRobotHeartbeatStrategyLatency::checkRobotStateLoop(){
    while (true){
        boost::unique_lock<boost::mutex> lock(_robotID2HBLatencyMutex);
        boost::posix_time::ptime time_now = boost::posix_time::microsec_clock::universal_time(); 
        for (std::map<int32_t, HBLatencyDistribution>::iterator it = _robotID2HBLatency.begin(); it != _robotID2HBLatency.end();) { 
            bool bRemove = false;
            double timediff = (time_now - it->second._lastHBTime).ticks(); //us
            
            switch(it->second._state){
                case ROBOT_STATE_JOIN:{ // if JOIN STATE, check if received enough heartbeat count in _robotJoinTimeOut seconds
                    double timeSinceFirstHB = rolling_sum(*(it->second._acc)) + timediff; //us
                    if(timeSinceFirstHB <= _robotJoinTimeOut *1000*1000){
                        if((rolling_count(*(it->second._acc)) + 1) >= _robotJoinHBCount){
                            sendRobotJoinMsg(it->first);
                            it->second._state = ROBOT_STATE_STAY;
                        }
                    }else{ // robot join failed, erase this record from _robotID2HBLatency
                        #ifdef ACTOR_SWARM_LOG_OUT
                        std::string eventStr = "newHB failed:" + std::to_string(it->first);
                        SwarmDataStorage::instance()->logOut2File("event", eventStr);
                        #endif

                        it->second.release();
                        it = _robotID2HBLatency.erase(it);
                        bRemove = true;
                    }
                }
                break;
                case ROBOT_STATE_STAY:{// if STAY STATE, check whether the robot has left or not
                    bool bLeave = false;
                    if (rolling_count(*(it->second._acc)) < WINDOWSIZE) {
                        // if there's no heartbeat in _hbTimeOut seconds, then the robot has left
                        bLeave = timediff > _hbTimeOut * 1000 * 1000;
                     }  else if (it->second._normal && timediff > it->second._normal->mean()) {
                        // if the normal distribution probability lager than confidence, then the robot has left
                        double confidence = SwarmDataStorage::instance()->getRobotHBLatencyStatisticConfidence(it->first);
                        bLeave = boost::math::cdf(*(it->second._normal), timediff) > confidence;
                    } 
                    if(bLeave){
                        sendRobotLeaveMsg(it->first);
                        it->second.release();
                        it = _robotID2HBLatency.erase(it);
                        bRemove = true;
                    }
                }
                break;
                default:break;
            }

            if(!bRemove)
                it++;
        }
        lock.unlock();

        usleep(HB_CHECK_PERIOD * 1000);
    }
}