/* ==================================================================
*
* Copyright (c) 2016-2019, micROS Group, NIIDT, TAIIC, HPCL.
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions 
* are met: 
*
* 1. Redistributions of source code must retain the above copyright 
*     notice, this list of conditions and the following disclaimer. 
* 2. Redistributions in binary form must reproduce the above copyright 
*     notice, this list of conditions and the following disclaimer in the 
*     documentation and/or other materials provided with the 
*     distribution. 
* 3. All advertising materials mentioning features or use of this software 
*     must display the following acknowledgement: 
*     This product includes software developed by the micROS Group. and 
*     its contributors. 
* 4. Neither the name of the Group nor the names of its contributors may 
*     be used to endorse or promote products derived from this software 
*     without specific prior written permission. 
*
* THIS SOFTWARE IS PROVIDED BY MICROS,GROUP AND CONTRIBUTORS
* ''AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT
* NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
* FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL 
* THE MICROS,GROUP OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS 
* OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) 
* HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
* OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN 
* IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
* ===================================================================
*
* Author: Bin Di, Chunlian Fu, Junling Gao, Ruihao Li, Wei Yi.
*         (in alphabetical order)
*
*/

#include "swarm_robot_heartbeat/swarm_robot_heartbeat_stigmergy.h"
#include "swarm_data_storage/swarm_data_storage.h"
#include "ros/ros.h"
#include "std_msgs/Int8.h"


SwarmRobotHeartbeat_Stigmergy::SwarmRobotHeartbeat_Stigmergy(){
    _vs = micros_swarm::VirtualStigmergy(3);
}

SwarmRobotHeartbeat_Stigmergy::~SwarmRobotHeartbeat_Stigmergy(){
    if(_pthUpdateHeartBeat){
        _pthUpdateHeartBeat->join();
        _pthUpdateHeartBeat->interrupt();
        _pthUpdateHeartBeat.reset();
    }
    if(_pthCheckHeartBeat){
        _pthCheckHeartBeat->join();
        _pthCheckHeartBeat->interrupt();
        _pthCheckHeartBeat.reset();
    }
}

void SwarmRobotHeartbeat_Stigmergy::run(){
    if(!_pthUpdateHeartBeat){
        _pthUpdateHeartBeat = boost::shared_ptr<boost::thread>(new boost::thread(boost::bind(&SwarmRobotHeartbeat_Stigmergy::updateHeartBeatLoop,this)));
        _pthUpdateHeartBeat->detach();
    }

    if(!_pthCheckHeartBeat){
        _pthCheckHeartBeat = boost::shared_ptr<boost::thread>(new boost::thread(boost::bind(&SwarmRobotHeartbeat_Stigmergy::checkHeartBeatLoop,this)));
        _pthCheckHeartBeat->detach();
    }
}

void SwarmRobotHeartbeat_Stigmergy::checkHeartBeatLoop(){
    while(1){
        checkHeartBeat();
        usleep(_hbPeriod*1000);
    }
}

void SwarmRobotHeartbeat_Stigmergy::checkHeartBeat(){
    // get stigmergy data
    std::map<std::string, micros_swarm::VirtualStigmergyTuple> vsMap;
    boost::unique_lock<boost::mutex> vsLock(_vsMutex);
    _vs.get_vs(vsMap);
    vsLock.unlock();

    if(vsMap.empty()){
        ROS_WARN("[SwarmRobotHeartbeat_Stigmergy] vs map is empty, no members in the swarm, return..");
		return;
    }
    
    boost::unique_lock<boost::mutex> rcLock(_rcMutex);
    for(std::map<std::string, micros_swarm::VirtualStigmergyTuple>::iterator it = vsMap.begin(); it!=vsMap.end(); it++){
        int32_t lamportClock = it->second.lamport_clock;
        std_msgs::Int8 robotIDMsg = micros_swarm::deserialize_ros<std_msgs::Int8>(it->second.vstig_value);
        int32_t robotID = robotIDMsg.data; 
        // if has heartbeat,process this msg and update clock
        if(robotID != _robotID && (_robotID2Clock.find(robotID) == _robotID2Clock.end()
         ||  _robotID2Clock[robotID] != lamportClock)){
            processHeartbeatMsg(robotID);
        }
        _robotID2Clock[robotID] = lamportClock;
    }
}

// periodic write visual stigmergy value to implement heartbeat
void SwarmRobotHeartbeat_Stigmergy::updateHeartBeatLoop(){
    while(1){
        updateHeartBeat();
        usleep(_hbPeriod*1000);
    }
}

void SwarmRobotHeartbeat_Stigmergy::updateHeartBeat(){
    std::string robotIDStr = "robot_" + std::to_string(_robotID);
    std_msgs::Int8 pval;
    pval.data = (int8_t)_robotID;

    boost::unique_lock<boost::mutex> vsLock(_vsMutex);
    _vs.put<std_msgs::Int8>(robotIDStr, pval);
}