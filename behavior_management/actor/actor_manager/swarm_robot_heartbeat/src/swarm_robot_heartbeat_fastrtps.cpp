/* ==================================================================
*
* Copyright (c) 2016-2019, micROS Group, NIIDT, TAIIC, HPCL.
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions 
* are met: 
*
* 1. Redistributions of source code must retain the above copyright 
*     notice, this list of conditions and the following disclaimer. 
* 2. Redistributions in binary form must reproduce the above copyright 
*     notice, this list of conditions and the following disclaimer in the 
*     documentation and/or other materials provided with the 
*     distribution. 
* 3. All advertising materials mentioning features or use of this software 
*     must display the following acknowledgement: 
*     This product includes software developed by the micROS Group. and 
*     its contributors. 
* 4. Neither the name of the Group nor the names of its contributors may 
*     be used to endorse or promote products derived from this software 
*     without specific prior written permission. 
*
* THIS SOFTWARE IS PROVIDED BY MICROS,GROUP AND CONTRIBUTORS
* ''AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT
* NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
* FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL 
* THE MICROS,GROUP OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS 
* OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) 
* HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
* OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN 
* IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
* ===================================================================
*
* Author: Bin Di, Chunlian Fu, Junling Gao, Ruihao Li, Wei Yi.
*         (in alphabetical order)
*
*/

#include "swarm_robot_heartbeat/swarm_robot_heartbeat_fastrtps.h"
#include "swarm_data_storage/swarm_data_storage.h"
SwarmRobotHeartbeat_RTPS::SwarmRobotHeartbeat_RTPS(){
    _hbCount = 0;
    _pPubHeartbeat = new RTPSPublisher<Heartbeat>("/heartbeat");
    _pSubHeartbeat = new RTPSSubscriber<Heartbeat>("/heartbeat", &SwarmRobotHeartbeat_RTPS::heartbeatCallback, this);
}

SwarmRobotHeartbeat_RTPS::~SwarmRobotHeartbeat_RTPS(){
    if(_pthSendHeartBeat){
        _pthSendHeartBeat->join();
        _pthSendHeartBeat->interrupt();
        _pthSendHeartBeat.reset();
    }
    if(_pPubHeartbeat){
        delete _pPubHeartbeat;
        _pPubHeartbeat = NULL;
    }
    if(_pSubHeartbeat){
        delete _pSubHeartbeat;
        _pSubHeartbeat = NULL;
    }
}

void SwarmRobotHeartbeat_RTPS::run(){
    if(!_pthSendHeartBeat){
        _pthSendHeartBeat = boost::shared_ptr<boost::thread>(new boost::thread(boost::bind(&SwarmRobotHeartbeat_RTPS::sendHeartbeatLoop,this)));
        _pthSendHeartBeat->detach();
    }
}

void SwarmRobotHeartbeat_RTPS::sendHeartbeatLoop(){
    Heartbeat hb;
    hb.sendID(_robotID);
    while(1){
        hb.isMaster(SwarmDataStorage::instance()->getMasterID() == _robotID ? 1 : 0);
        _pPubHeartbeat->publish(hb);

#ifdef ACTOR_SWARM_LOG_OUT
        std::string logStr = std::to_string(Heartbeat::getCdrSerializedSize(hb)) + ",Heartbeat";
        SwarmDataStorage::instance()->logOut2File("netsend", logStr);
#endif

        usleep(_hbPeriod*1000);
    }
}

void SwarmRobotHeartbeat_RTPS::heartbeatCallback(Heartbeat& aHB){
    if(_robotID == aHB.sendID()) return;
 
#ifdef ACTOR_SWARM_LOG_OUT
    std::string logStr = std::to_string(Heartbeat::getCdrSerializedSize(aHB)) + ",Heartbeat";
    SwarmDataStorage::instance()->logOut2File("netreceive", logStr);
#endif

    processHeartbeatMsg(aHB.sendID(), aHB.isMaster());
}

