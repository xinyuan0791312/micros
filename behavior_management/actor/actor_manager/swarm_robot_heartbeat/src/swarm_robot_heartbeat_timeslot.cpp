/* ==================================================================
*
* Copyright (c) 2016-2019, micROS Group, NIIDT, TAIIC, HPCL.
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions 
* are met: 
*
* 1. Redistributions of source code must retain the above copyright 
*     notice, this list of conditions and the following disclaimer. 
* 2. Redistributions in binary form must reproduce the above copyright 
*     notice, this list of conditions and the following disclaimer in the 
*     documentation and/or other materials provided with the 
*     distribution. 
* 3. All advertising materials mentioning features or use of this software 
*     must display the following acknowledgement: 
*     This product includes software developed by the micROS Group. and 
*     its contributors. 
* 4. Neither the name of the Group nor the names of its contributors may 
*     be used to endorse or promote products derived from this software 
*     without specific prior written permission. 
*
* THIS SOFTWARE IS PROVIDED BY MICROS,GROUP AND CONTRIBUTORS
* ''AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT
* NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
* FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL 
* THE MICROS,GROUP OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS 
* OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) 
* HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
* OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN 
* IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
* ===================================================================
*
* Author: Bin Di, Chunlian Fu, Junling Gao, Ruihao Li, Wei Yi.
*         (in alphabetical order)
*
*/

#include "swarm_robot_heartbeat/swarm_robot_heartbeat_timeslot.h"
#include "swarm_data_storage/swarm_data_storage.h"
#include "ros/ros.h"
SwarmRobotHeartbeat_TimeSlot::SwarmRobotHeartbeat_TimeSlot():_slot(-1){
    _pPubHeartbeat = new RTPSPublisher<Heartbeat>("/heartbeat");
    _pSubHeartbeat = new RTPSSubscriber<Heartbeat>("/heartbeat", &SwarmRobotHeartbeat_TimeSlot::heartbeatCallback, this);
}

SwarmRobotHeartbeat_TimeSlot::~SwarmRobotHeartbeat_TimeSlot(){
    if(_pPubHeartbeat){
        delete _pPubHeartbeat;
        _pPubHeartbeat = NULL;
    }
    if(_pSubHeartbeat){
        delete _pSubHeartbeat;
        _pSubHeartbeat = NULL;
    }
    if(_pthCheckInitialRobotIDs){
        _pthCheckInitialRobotIDs->join();
        _pthCheckInitialRobotIDs->interrupt();
        _pthCheckInitialRobotIDs.reset();
    }
}

void SwarmRobotHeartbeat_TimeSlot::run(){
    ros::NodeHandle nh;
    _timer = nh.createTimer(ros::Duration(_hbPeriod/1000), &SwarmRobotHeartbeat_TimeSlot::timerCallback, this);
    if(!_pthCheckInitialRobotIDs){
        _pthCheckInitialRobotIDs = boost::shared_ptr<boost::thread>(new boost::thread(boost::bind(&SwarmRobotHeartbeat_TimeSlot::checkInitialRobotIDs,this)));
        _pthCheckInitialRobotIDs->detach();
    }
}

void SwarmRobotHeartbeat_TimeSlot::updateSlot(const int aSlotIndex, const int aSlotCount){
    boost::unique_lock<boost::mutex> lock(_slotMutex);
    _slot = aSlotIndex;
#ifdef ACTOR_SWARM_LOG_OUT
    std::string logStr = "update slot:" + std::to_string(_slot);
    SwarmDataStorage::instance()->logOut2File("event", logStr);
#endif    
    ROS_INFO("[SwarmRobotHeartbeat_TimeSlot] updateSlot slot=%d, slotcount=%d", aSlotIndex, aSlotCount);
    // stop timer
    _timer.stop();

    // compute timer start delay time
    uint64_t hbPeroid = _hbPeriod * 1e6;
    uint64_t slotPeriod = hbPeroid / aSlotCount; // ns
    ros::Time start = ros::Time::now();

    uint64_t curHBDiff = start.toNSec() % hbPeroid;
    uint64_t curSlot = curHBDiff / slotPeriod;
    uint64_t curSlotDiff = curHBDiff % slotPeriod;

    uint64_t delay = curSlotDiff >= slotPeriod/2 ? slotPeriod/2 : 0;
    delay = (((_slot - curSlot + aSlotCount) % aSlotCount) * slotPeriod - delay + hbPeroid) % hbPeroid;
    if(delay > 0){
        ros::Duration(0, delay).sleep();
    }

    _timer.start();
    ROS_INFO("[SwarmRobotHeartbeat_TimeSlot] updateSlot restart timer. delay=%lu", delay);
}

void SwarmRobotHeartbeat_TimeSlot::heartbeatCallback(Heartbeat& aHB){
    if(_robotID == aHB.sendID()) return;
 
#ifdef ACTOR_SWARM_LOG_OUT
    std::string logStr = std::to_string(Heartbeat::getCdrSerializedSize(aHB)) + ",Heartbeat";
    SwarmDataStorage::instance()->logOut2File("netreceive", logStr);
#endif

    processHeartbeatMsg(aHB.sendID(), aHB.isMaster());
}

void SwarmRobotHeartbeat_TimeSlot::timerCallback(const ros::TimerEvent& aEvent){
    Heartbeat hb;
    hb.sendID(_robotID);
    hb.isMaster(SwarmDataStorage::instance()->getMasterID() == _robotID ? 1 : 0);
    _pPubHeartbeat->publish(hb);

#ifdef ACTOR_SWARM_LOG_OUT
    std::string logStr = std::to_string(Heartbeat::getCdrSerializedSize(hb)) + ",Heartbeat";
    SwarmDataStorage::instance()->logOut2File("netsend", logStr);
#endif
}

void SwarmRobotHeartbeat_TimeSlot::checkInitialRobotIDs(){
    std::set<int> initialRobotIDs;
    while(_slot == -1){
        initialRobotIDs = SwarmDataStorage::instance()->getInitialRobotIDs();
        if(initialRobotIDs.size() > 0){
            int tmpSlot = -1;
            for(auto it = initialRobotIDs.begin(); it != initialRobotIDs.end(); it++){
                tmpSlot++;
                if(*it == _robotID){
                    updateSlot(tmpSlot, initialRobotIDs.size());
                    break;
                }
            }
            break;
        }
        
        sleep(1);
    }
}