/* ==================================================================
*
* Copyright (c) 2016-2019, micROS Group, NIIDT, TAIIC, HPCL.
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions 
* are met: 
*
* 1. Redistributions of source code must retain the above copyright 
*     notice, this list of conditions and the following disclaimer. 
* 2. Redistributions in binary form must reproduce the above copyright 
*     notice, this list of conditions and the following disclaimer in the 
*     documentation and/or other materials provided with the 
*     distribution. 
* 3. All advertising materials mentioning features or use of this software 
*     must display the following acknowledgement: 
*     This product includes software developed by the micROS Group. and 
*     its contributors. 
* 4. Neither the name of the Group nor the names of its contributors may 
*     be used to endorse or promote products derived from this software 
*     without specific prior written permission. 
*
* THIS SOFTWARE IS PROVIDED BY MICROS,GROUP AND CONTRIBUTORS
* ''AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT
* NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
* FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL 
* THE MICROS,GROUP OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS 
* OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) 
* HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
* OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN 
* IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
* ===================================================================
*
* Author: Bin Di, Chunlian Fu, Junling Gao, Ruihao Li, Wei Yi.
*         (in alphabetical order)
*
*/

#include "swarm_robot_heartbeat/swarm_robot_heartbeat_strategy.h"
#include "ros/ros.h"

//default parameters for robot heart beat
#define HB_TIME_OUT 5           // heart beat time out, (s)
#define ROBOT_JOIN_HB_COUNT 3   // heartbeat count before robot join
#define ROBOT_JOIN_TIME_OUT 3   // robot join time out, (s)

SwarmRobotHeartbeatStrategy::SwarmRobotHeartbeatStrategy(){
    ros::NodeHandle nh;
    if (nh.hasParam("/HeartBeatTimeOut")) {
        nh.getParam("/HeartBeatTimeOut", _hbTimeOut); 
    } else {
        _hbTimeOut = HB_TIME_OUT;
    }

    if (nh.hasParam("/RobotJoinHBCount")) {
        nh.getParam("/RobotJoinHBCount", _robotJoinHBCount); 
    } else {
        _robotJoinHBCount = ROBOT_JOIN_HB_COUNT;
    }

    if (nh.hasParam("/RobotJoinTimeout")) {
        nh.getParam("/RobotJoinTimeout", _robotJoinTimeOut); 
    } else {
        _robotJoinTimeOut = ROBOT_JOIN_TIME_OUT;
    }
    ROS_INFO("[SwarmRobotHeartbeatStrategy] _hbTimeOut=%d, _robotJoinHBCount=%d, _robotJoinTimeOut=%d", _hbTimeOut, _robotJoinHBCount, _robotJoinTimeOut);
}

SwarmRobotHeartbeatStrategy::~SwarmRobotHeartbeatStrategy(){
    if(_pHandler){
        delete _pHandler;
        _pHandler = NULL;
    }
}

void SwarmRobotHeartbeatStrategy::sendRobotJoinMsg(const int aRobotID){
    ROS_INFO("[SwarmRobotHeartbeatStrategy] sendRobotJoinMsg,%d", aRobotID);
    _pHandler->processRobotJoinMsg(aRobotID);
}

void SwarmRobotHeartbeatStrategy::sendRobotLeaveMsg(const int aRobotID){
    ROS_INFO("[SwarmRobotHeartbeatStrategy] sendRobotLeaveMsg,%d", aRobotID);
    _pHandler->processRobotLeaveMsg(aRobotID);
}