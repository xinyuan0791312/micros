/* ==================================================================
*
* Copyright (c) 2016-2019, micROS Group, NIIDT, TAIIC, HPCL.
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions 
* are met: 
*
* 1. Redistributions of source code must retain the above copyright 
*     notice, this list of conditions and the following disclaimer. 
* 2. Redistributions in binary form must reproduce the above copyright 
*     notice, this list of conditions and the following disclaimer in the 
*     documentation and/or other materials provided with the 
*     distribution. 
* 3. All advertising materials mentioning features or use of this software 
*     must display the following acknowledgement: 
*     This product includes software developed by the micROS Group. and 
*     its contributors. 
* 4. Neither the name of the Group nor the names of its contributors may 
*     be used to endorse or promote products derived from this software 
*     without specific prior written permission. 
*
* THIS SOFTWARE IS PROVIDED BY MICROS,GROUP AND CONTRIBUTORS
* ''AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT
* NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
* FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL 
* THE MICROS,GROUP OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS 
* OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) 
* HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
* OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN 
* IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
* ===================================================================
*
* Author: Bin Di, Chunlian Fu, Junling Gao, Ruihao Li, Wei Yi.
*         (in alphabetical order)
*
*/

#include "swarm_robot_heartbeat/swarm_robot_heartbeat_strategy_simple.h"
#include <ros/duration.h>
SwarmRobotHeartbeatStrategySimple::SwarmRobotHeartbeatStrategySimple(){
    //calculate heart beat time out count
    _hbTimeOutCount = _hbTimeOut*1000/HB_CHECK_PERIOD;
    if(_hbTimeOutCount <= 0){
        ROS_FATAL("[SwarmRobotHeartbeatStrategySimple] heart beat time out <=0");
        return;
    }
    
    _robotJoinTimeOutCount = _robotJoinTimeOut*1000/HB_CHECK_PERIOD;
    if(_robotJoinTimeOutCount <= 0){
        ROS_FATAL("[SwarmRobotHeartbeatStrategySimple] robot join heart beat time out <=0");
        return;
    }
    //start the robot state check loop
    _pthCheckRobotState = boost::shared_ptr<boost::thread>(new boost::thread(boost::bind(&SwarmRobotHeartbeatStrategySimple::checkRobotStateLoop, this)));    
}

SwarmRobotHeartbeatStrategySimple::~SwarmRobotHeartbeatStrategySimple(){
    if(_pthCheckRobotState){
        _pthCheckRobotState->join();
        _pthCheckRobotState->interrupt();
        _pthCheckRobotState.reset();
    }
}

void SwarmRobotHeartbeatStrategySimple::processHeartbeatMsg(const int32_t aRobotID, const bool aIsFromMaster){
#ifdef ACTOR_SWARM_LOG_OUT
    if (_robotID2HBTime[aRobotID].size() >= 10){
        _robotID2HBTime[aRobotID].erase(_robotID2HBTime[aRobotID].begin());

        if (aIsFromMaster && aRobotID != SwarmDataStorage::instance()->getMasterID()){
            std::string eventStr = "receive robot " + std::to_string(aRobotID) + " heartbeat as master, cur master is:" + std::to_string(SwarmDataStorage::instance()->getMasterID());
            SwarmDataStorage::instance()->logOut2File("warn", eventStr);
        }
    }
    _robotID2HBTime[aRobotID].push_back(ros::Time::now());
    
    if( _robotID2HBTime[aRobotID].size() > 2){
        ros::Duration duration = _robotID2HBTime[aRobotID].back() - _robotID2HBTime[aRobotID].front();
        float avghbtime= duration.toSec() / (_robotID2HBTime[aRobotID].size() - 1);
        if(avghbtime >= _hbTimeOut/2){
            // variance
            float acc  = 0.0;
            for(auto it = _robotID2HBTime[aRobotID].begin() +1; it != _robotID2HBTime[aRobotID].end(); it++){
                acc += pow((*it - *(it-1)).toSec() - avghbtime, 2);
            }
            double stdev = sqrt(acc/(_robotID2HBTime[aRobotID].size()-2)); 

            std::string eventStr = "receive robot " + std::to_string(aRobotID) + " heartbeat abnormal, avg_hb_time:" + std::to_string(avghbtime) + ", stddev_hb_time:" + std::to_string(stdev);
            SwarmDataStorage::instance()->logOut2File("warn", eventStr);
        }
    }
#endif

    boost::unique_lock<boost::mutex> rscLock(_robotID2StateCountMutex);
    //if receive heart beat from new robot, set its state to ROBOT_STATE_JOIN
    if (_robotID2StateCount.count(aRobotID) == 0) {
        RobotStateCount joinCount;
        joinCount._state = ROBOT_STATE_JOIN;
        joinCount._count = 1;
        joinCount._timeOut = _robotJoinTimeOutCount;
        _robotID2StateCount[aRobotID] = joinCount;

        ROS_INFO("[SwarmRobotHeartbeatStrategySimple] new heartbeat[%d]", aRobotID);
        #ifdef ACTOR_SWARM_LOG_OUT
        std::string eventStr = "newHB:" + std::to_string(aRobotID);
        SwarmDataStorage::instance()->logOut2File("event", eventStr);
        #endif
    } else {
        switch (_robotID2StateCount[aRobotID]._state) {
        case ROBOT_STATE_JOIN:
            _robotID2StateCount[aRobotID]._count++;
            break;
        case ROBOT_STATE_STAY:
            _robotID2StateCount[aRobotID]._count = 0;
            break;
        default:
            break;
        }
    }
    rscLock.unlock();
    #ifdef ACTOR_SWARM_LOG_OUT
    SwarmDataStorage::instance()->logOutHB2File(aRobotID,1);
    #endif
}

void SwarmRobotHeartbeatStrategySimple::checkRobotStateLoop(){
    
    while (true){
        {
        boost::unique_lock<boost::mutex> rscLock(_robotID2StateCountMutex);
        for (std::map<int32_t, RobotStateCount>::iterator it = _robotID2StateCount.begin(); it != _robotID2StateCount.end();) {
            bool eraseRobot = false;
            //hbValue indicates the degree/probability a robot is in the group, [0,1]
            float hbValue = -1;
            switch (it->second._state) {
            case ROBOT_STATE_JOIN:
                hbValue = float(it->second._count)/float(_robotJoinHBCount);
                if(hbValue>1) hbValue = 1;
                SwarmDataStorage::instance()->setRobotHeartbeatValue(it->first, hbValue);
                it->second._timeOut--;

                //if receive more than _robotJoinHBCount hb in _robotJoinTimeOut, set the state to ROBOT_STATE_STAY
                if(it->second._timeOut >= 0){
                    if(it->second._count >= _robotJoinHBCount) {
                        it->second._state = ROBOT_STATE_STAY;
                        it->second._count = 0;
                        //send robot join msg
                        sendRobotJoinMsg(it->first);

#ifdef ACTOR_SWARM_LOG_OUT
                        _robotID2JoinTime[it->first].push_back(ros::Time::now());
                        if (_robotID2JoinTime[it->first].size() > 3){
                            _robotID2JoinTime[it->first].erase(_robotID2JoinTime[it->first].begin());
                        
                            ros::Duration duration = _robotID2JoinTime[it->first].back() - _robotID2JoinTime[it->first].front();
                            if(duration.toSec() < _hbTimeOut * 10){
                                std::string eventStr = "robot ";
                                eventStr.append(std::to_string(it->first)).append(" join abnormal, join 3 times in ").append(std::to_string(duration.toSec())).append(" seconds");
                                SwarmDataStorage::instance()->logOut2File("warn", eventStr);
                            }
                        }     
#endif                       
                    }
                } else {// else remove
                    #ifdef ACTOR_SWARM_LOG_OUT
                    std::string eventStr = "newHB failed:" + std::to_string(it->first);
                    SwarmDataStorage::instance()->logOut2File("event", eventStr);
                    #endif
                    
                    eraseRobot = true;
                    SwarmDataStorage::instance()->setRobotHeartbeatValue(it->first, 0);
                    it = _robotID2StateCount.erase(it);
                }
                break;
            case ROBOT_STATE_STAY:
                hbValue = 1.0 - float(it->second._count)/float(_hbTimeOutCount);
                if(hbValue<0) hbValue = 0;
                SwarmDataStorage::instance()->setRobotHeartbeatValue(it->first, hbValue);
                it->second._count++; //lose count++

                //if time out,erase the robot
                if (it->second._count >= _hbTimeOutCount) {
                    eraseRobot = true;
                    sendRobotLeaveMsg(it->first);
                    it = _robotID2StateCount.erase(it);
                }
                break;
            default:
                break;
            }
            //if didnot erase robot, it++
            if (!eraseRobot)
                it++;
        }
        }
        usleep(HB_CHECK_PERIOD * 1000);
    }
}	