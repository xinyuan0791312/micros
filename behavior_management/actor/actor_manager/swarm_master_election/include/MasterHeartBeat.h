// Copyright 2016 Proyectos y Sistemas de Mantenimiento SL (eProsima).
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/*!
 * @file MasterHeartBeat.h
 * This header file contains the declaration of the described types in the IDL file.
 *
 * This file was generated by the tool gen.
 */

#ifndef _MASTERHEARTBEAT_H_
#define _MASTERHEARTBEAT_H_

// TODO Poner en el contexto.

#include <stdint.h>
#include <array>
#include <string>
#include <vector>
#include <map>
#include <bitset>

#if defined(_WIN32)
#if defined(EPROSIMA_USER_DLL_EXPORT)
#define eProsima_user_DllExport __declspec( dllexport )
#else
#define eProsima_user_DllExport
#endif
#else
#define eProsima_user_DllExport
#endif

#if defined(_WIN32)
#if defined(EPROSIMA_USER_DLL_EXPORT)
#if defined(MasterHeartBeat_SOURCE)
#define MasterHeartBeat_DllAPI __declspec( dllexport )
#else
#define MasterHeartBeat_DllAPI __declspec( dllimport )
#endif // MasterHeartBeat_SOURCE
#else
#define MasterHeartBeat_DllAPI
#endif
#else
#define MasterHeartBeat_DllAPI
#endif // _WIN32

namespace eprosima
{
    namespace fastcdr
    {
        class Cdr;
    }
}

/*!
 * @brief This class represents the structure MasterHeartBeat defined by the user in the IDL file.
 * @ingroup MASTERHEARTBEAT
 */
class MasterHeartBeat
{
public:

    /*!
     * @brief Default constructor.
     */
    eProsima_user_DllExport MasterHeartBeat();

    /*!
     * @brief Default destructor.
     */
    eProsima_user_DllExport virtual ~MasterHeartBeat();

    /*!
     * @brief Copy constructor.
     * @param x Reference to the object MasterHeartBeat that will be copied.
     */
    eProsima_user_DllExport MasterHeartBeat(const MasterHeartBeat &x);

    /*!
     * @brief Move constructor.
     * @param x Reference to the object MasterHeartBeat that will be copied.
     */
    eProsima_user_DllExport MasterHeartBeat(MasterHeartBeat &&x);

    /*!
     * @brief Copy assignment.
     * @param x Reference to the object MasterHeartBeat that will be copied.
     */
    eProsima_user_DllExport MasterHeartBeat& operator=(const MasterHeartBeat &x);

    /*!
     * @brief Move assignment.
     * @param x Reference to the object MasterHeartBeat that will be copied.
     */
    eProsima_user_DllExport MasterHeartBeat& operator=(MasterHeartBeat &&x);

    /*!
     * @brief This function sets a value in member voteNo
     * @param _voteNo New value for member voteNo
     */
    eProsima_user_DllExport void voteNo(int16_t _voteNo);

    /*!
     * @brief This function returns the value of member voteNo
     * @return Value of member voteNo
     */
    eProsima_user_DllExport int16_t voteNo() const;

    /*!
     * @brief This function returns a reference to member voteNo
     * @return Reference to member voteNo
     */
    eProsima_user_DllExport int16_t& voteNo();
    /*!
     * @brief This function sets a value in member masterID
     * @param _masterID New value for member masterID
     */
    eProsima_user_DllExport void masterID(int16_t _masterID);

    /*!
     * @brief This function returns the value of member masterID
     * @return Value of member masterID
     */
    eProsima_user_DllExport int16_t masterID() const;

    /*!
     * @brief This function returns a reference to member masterID
     * @return Reference to member masterID
     */
    eProsima_user_DllExport int16_t& masterID();

    /*!
     * @brief This function returns the maximum serialized size of an object
     * depending on the buffer alignment.
     * @param current_alignment Buffer alignment.
     * @return Maximum serialized size.
     */
    eProsima_user_DllExport static size_t getMaxCdrSerializedSize(size_t current_alignment = 0);

    /*!
     * @brief This function returns the serialized size of a data depending on the buffer alignment.
     * @param data Data which is calculated its serialized size.
     * @param current_alignment Buffer alignment.
     * @return Serialized size.
     */
    eProsima_user_DllExport static size_t getCdrSerializedSize(const MasterHeartBeat& data, size_t current_alignment = 0);


    /*!
     * @brief This function serializes an object using CDR serialization.
     * @param cdr CDR serialization object.
     */
    eProsima_user_DllExport void serialize(eprosima::fastcdr::Cdr &cdr) const;

    /*!
     * @brief This function deserializes an object using CDR serialization.
     * @param cdr CDR serialization object.
     */
    eProsima_user_DllExport void deserialize(eprosima::fastcdr::Cdr &cdr);



    /*!
     * @brief This function returns the maximum serialized size of the Key of an object
     * depending on the buffer alignment.
     * @param current_alignment Buffer alignment.
     * @return Maximum serialized size.
     */
    eProsima_user_DllExport static size_t getKeyMaxCdrSerializedSize(size_t current_alignment = 0);

    /*!
     * @brief This function tells you if the Key has been defined for this type
     */
    eProsima_user_DllExport static bool isKeyDefined();

    /*!
     * @brief This function serializes the key members of an object using CDR serialization.
     * @param cdr CDR serialization object.
     */
    eProsima_user_DllExport void serializeKey(eprosima::fastcdr::Cdr &cdr) const;

 eProsima_user_DllExport static std::string getName() { return "MasterHeartBeat"; } 
private:
    int16_t m_voteNo;
    int16_t m_masterID;
};

#endif // _MASTERHEARTBEAT_H_