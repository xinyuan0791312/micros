/* ==================================================================
*
* Copyright (c) 2016-2019, micROS Group, NIIDT, TAIIC, HPCL.
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions 
* are met: 
*
* 1. Redistributions of source code must retain the above copyright 
*     notice, this list of conditions and the following disclaimer. 
* 2. Redistributions in binary form must reproduce the above copyright 
*     notice, this list of conditions and the following disclaimer in the 
*     documentation and/or other materials provided with the 
*     distribution. 
* 3. All advertising materials mentioning features or use of this software 
*     must display the following acknowledgement: 
*     This product includes software developed by the micROS Group. and 
*     its contributors. 
* 4. Neither the name of the Group nor the names of its contributors may 
*     be used to endorse or promote products derived from this software 
*     without specific prior written permission. 
*
* THIS SOFTWARE IS PROVIDED BY MICROS,GROUP AND CONTRIBUTORS
* ''AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT
* NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
* FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL 
* THE MICROS,GROUP OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS 
* OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) 
* HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
* OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN 
* IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
* ===================================================================
*
* Author: Bin Di, Chunlian Fu, Junling Gao, Ruihao Li, Wei Yi.
*         (in alphabetical order)
*
*/

#ifndef __SWARM_MASTER_ELECTION_BIDS_H__
#define __SWARM_MASTER_ELECTION_BIDS_H__

#include <string>
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <map>
#include <sys/time.h>
#include <time.h>
#include <boost/thread/thread.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/thread/recursive_mutex.hpp>
#include <boost/bind.hpp>
#include <ros/ros.h>
#include "RTPSPublisher.h"
#include "RTPSSubscriber.h"
#include "MasterHeartBeat.h"
#include "MasterElectionBid.h"
#include "swarm_master_election.h"
//define average communication delay and max communication delay of all members
//#define AVERAGE_COMM_LATENCY 10
//#define MAX_COMM_LATENCY 2*AVERAGE_COMM_LATENCY

//vote state
const int32_t VOTE_PROCESSING = 0;
const int32_t VOTE_CONSENSUS = 1;
//
const int32_t MAX_ROBOTID = 32768;

//master heart beat period 
const int32_t MASTER_HB_PERIOD = 1000;//heart beat send period 1s
const int32_t COUNT_DURATION = 100;//100ms

//deprecated
const int32_t TIME_OUT_COUNT = 10*MASTER_HB_PERIOD/COUNT_DURATION;//100*100ms = 10s;10 hb send period
//const int32_t INITIAL_TIME_OUT_COUNT = 60*MASTER_HB_PERIOD/COUNT_DURATION;//600*100ms = 1min;60 hb send period

//inconsistent msg count 
const int32_t INCONSISTENT_MSG_COUNT = 2;

//struct for vote info
struct VoteInfo{
    int32_t _voteNo;
    int32_t _robotID;
    int32_t _robotIDWithMaxBid;
    int32_t _maxBid;
    int32_t _state;
    std::vector<int32_t> _voteMembers;
    std::set<int32_t> _receivedRobotIDs;
    std::vector<int32_t> _robotIDs;
    std::vector<int32_t> _bids;
};

//struct, for test diff leader msg 
struct MsgReceTimeInfo{
    int32_t _voteNo;
    std::vector<timeval> _msgReceTimeList;
};

class SwarmMasterElectionBids : public SwarmMasterElection {
public:
    SwarmMasterElectionBids();
    void init(int32_t aLocalBid, std::string aLogFilePath);
    virtual ~SwarmMasterElectionBids();   

    //master heart beat count
    void masterHBCountLoop();
    void masterHBCount();

    //send master heart beat msg loop
    void sendMasterHBLoop();
    void sendMasterHB();

    //sub function
    void masterElectionCallback(MasterElectionBid &aElecBidMsg);
    void masterHBCallback(MasterHeartBeat &aHeartBeat);

    //receive heart beat from matched/unmatched master
    void receiveHBfromMatchedMaster(int32_t aMasterID);
    void receiveHBfromUnMatchedMaster(int32_t aMasterID);

    //activate a new vote
    void activateMasterVote();

    //delay vote msg
    void voteMsgDelay(int32_t aDelayTime,int32_t aVoteNo,int32_t aMsgIndex,MasterElectionBid *aPVoteMsg);
    void voteConsensusDelay(int32_t aDelayTime,int32_t aVoteNo,int32_t aMaxRobotID);

    //set local bid
    void setLocalBid(int32_t aLocalBid);   

    //write masterID to file
    void writeMasterIDtoFile();
    //write master voteNo and voteState to file
    void writeVoteNoStatetoFile();
    //write vote activate or receive msg,aFlag=0--receive;aFlag>0--activate
    void writeVoteActiOrRecetoFile(int aFlag);
    //write swarm size to file
    void writeSwarmSizetoFile(int32_t aSize);
    //write event string to file
    void writeEventtoFile(std::string aEventStr);
    //write vote msg count to file
    void writeVoteCounttoFile();
    //write vote msg source robot ID
    void writeVoteMsgSourcetoFile(int32_t aRobotID);
   
    boost::condition_variable& getVoteCompletedCond(){
        return _voteCompletedCond;
    }

private:

    int32_t _robotID;
    //local bid
    int32_t _localBid;

    //rtps sub pub master election msg
    RTPSPublisher<MasterElectionBid>* _pPubMasterElection;
    RTPSSubscriber<MasterElectionBid>* _pSubMasterElection;
    boost::mutex _pubMasterElecMutex;

    //rtps sub pub master heartbeat msg
    RTPSPublisher<MasterHeartBeat>* _pPubMasterHeartBeat;
    RTPSSubscriber<MasterHeartBeat>* _pSubMasterHeartBeat;
    //boost::mutex _pubMasterHBMutex;
    
    //master heart beat loop thread
    boost::shared_ptr<boost::thread> _pthMasterHBLoop;
    //master heart beat count thread
    boost::shared_ptr<boost::thread> _pthMasterHBCountLoop;

    //master heart beat period
    int32_t _masterHBPeriod;

    //recursive mutex 
    boost::recursive_mutex _recurMutex;

    //current vote state and voteNo
    int32_t _masterVoteNo;
    //time out for each vote
    int32_t _masterVoteTimeOut;

    //map voteNo-->voteinfo
    std::map<int32_t, VoteInfo> _voteInfoMap;

    //heart beat time count
    int32_t _timerCount;
    //vote processing time count
    int32_t _voteTimerCount;

    //mutex for time count,replaced by _recurMutex
    boost::mutex _timerCountMutex;

    //inconsistent HB msg received count
    //voteNo-->count
    std::map<int32_t, int32_t> _largerVoteNoMsgCount;
    //different leader HB msg receive time
    std::map<int32_t,MsgReceTimeInfo> _diffMasterMsgInfo;

    //fstream for test data
    std::fstream _fs;
    boost::mutex _fsMutex;
    
    //test vote msg count
    int32_t _voteCount;
    //thread group for delay vote msg
    boost::thread_group _voteMsgDelayTG;
    //voteNo-->send or not
    std::map<int32_t,std::vector<int32_t> > _voteMsgDelayMap;
    //mutex for voteMsgDelayMap
    boost::shared_mutex _voteMsgDelayMapMutex;

    // notify other threads that master vote completed
    boost::condition_variable _voteCompletedCond;

};

#endif
