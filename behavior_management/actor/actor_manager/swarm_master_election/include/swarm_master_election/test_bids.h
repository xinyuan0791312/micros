/* ==================================================================
*
* Copyright (c) 2016-2019, micROS Group, NIIDT, TAIIC, HPCL.
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions 
* are met: 
*
* 1. Redistributions of source code must retain the above copyright 
*     notice, this list of conditions and the following disclaimer. 
* 2. Redistributions in binary form must reproduce the above copyright 
*     notice, this list of conditions and the following disclaimer in the 
*     documentation and/or other materials provided with the 
*     distribution. 
* 3. All advertising materials mentioning features or use of this software 
*     must display the following acknowledgement: 
*     This product includes software developed by the micROS Group. and 
*     its contributors. 
* 4. Neither the name of the Group nor the names of its contributors may 
*     be used to endorse or promote products derived from this software 
*     without specific prior written permission. 
*
* THIS SOFTWARE IS PROVIDED BY MICROS,GROUP AND CONTRIBUTORS
* ''AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT
* NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
* FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL 
* THE MICROS,GROUP OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS 
* OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) 
* HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
* OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN 
* IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
* ===================================================================
*
* Author: Bin Di, Chunlian Fu, Junling Gao, Ruihao Li, Wei Yi.
*         (in alphabetical order)
*
*/

#ifndef __SWARM_MASTER_ELECTION_BIDS2_H__
#define __SWARM_MASTER_ELECTION_BIDS2_H__

#include "RTPSPublisher.h"
#include "RTPSSubscriber.h"
#include "MasterHeartBeat.h"
#include "MasterElectionBid.h"
#include "swarm_master_election.h"
#include <boost/thread/thread.hpp>
#include <boost/thread/mutex.hpp>
#include <fstream>

class SwarmMasterElectionBids2 : public SwarmMasterElection {
public:
    SwarmMasterElectionBids2();
    ~SwarmMasterElectionBids2();
    void init(const int32_t aLocalBid, const std::string& aLogFilePath);
    void setLocalBid(const int32_t aLocalBid);
    // called by handleHBfromUnMatchedMaster method when detect multi master 
    // or master leave
    void activateMasterVote();

    boost::condition_variable& getVoteCompletedCond(){ return _voteCompletedCond;}

private:
    // thread loops
    void sendMasterHBLoop();
    void checkVoteTimeoutLoop();
    void forwardVoteMsgLoop();

    // callback methods
    void masterHBCallback(MasterHeartBeat &aHeartBeat);
    void masterElectionCallback(MasterElectionBid &aElecBidMsg);

    // without lock, called when receive different master id
    void handleHBfromUnMatchedMaster(const int32_t aMasterID);
    // without lock, us
    void voteConsensusDelay(const int aDelayTime);

    void writeLocalTime();
    //write masterID to file
    void writeMasterIDtoFile();
    //write master voteNo and voteState to file
    void writeVoteNoStatetoFile();
    //write vote activate or receive msg,aFlag=0--receive;aFlag>0--activate
    void writeVoteActiOrRecetoFile(const int aFlag);
    //write swarm size to file
    void writeSwarmSizetoFile(const int32_t aSize);
    //write event string to file
    void writeEventtoFile(const std::string& aEventStr);
    //write vote msg count to file
    void writeVoteCounttoFile();
    //write vote msg source robot ID
    void writeVoteMsgSourcetoFile(const int32_t aRobotID);    

private:
    // rtps sub pub master election msg
    RTPSPublisher<MasterElectionBid>* _pPubMasterElection;
    RTPSSubscriber<MasterElectionBid>* _pSubMasterElection;
    boost::mutex _pubMasterElecMutex;

    // rtps sub pub master heartbeat msg
    RTPSPublisher<MasterHeartBeat>* _pPubMasterHeartBeat;
    RTPSSubscriber<MasterHeartBeat>* _pSubMasterHeartBeat;

    // thread to send periodic Master Heartbeat 
    boost::shared_ptr<boost::thread> _pthMasterHBLoop;

    // thread to check if voting process timeout
    boost::shared_ptr<boost::thread> _pthCheckVoteTimeout;
    boost::condition_variable _voteStartCond;     // notify when voting process start
    boost::condition_variable _voteCompletedCond; // notify when voting process completed

    // thread to delay forward vote msg, when condition notify timeout, forward vote msg
    boost::shared_ptr<boost::thread> _pthForwardVoteMsgDelay;
    boost::condition_variable _forwardVoteMsgCond; // notify when need forward vote msg

    // these member params will be initialized by construct
    int32_t _robotID;
    int32_t _masterHBPeriod;

    // these member params will be dynamic set
    int32_t _localBid;
    boost::shared_mutex _localBidMutex;

    // fstream for log data
    std::fstream _fs;
    boost::mutex _fsMutex;

    //struct for vote info
    struct VoteInfo{
        int32_t _voteNo;            // current voteNo
        int32_t _robotIDWithMaxBid; //
        int32_t _maxBid;
        int32_t _state;             // KEEPING or VOTING
        int32_t _forwardDelay;      // us, delay time to forward vote msg when in VOTING state
        timeval _startTime;         //time when vote started
        int32_t _voteConsensusTimeout;// us
        std::vector<int32_t> _voteMembers;
        std::set<int32_t> _receivedRobotIDs;
        std::vector<int32_t> _robotIDs;
        std::vector<int32_t> _bids;
        VoteInfo():_voteNo(0){}
        void reset(const int aMasterID, const int aMaxBid);
    }_voteInfo;
    boost::mutex _voteInfoMutex;

    //inconsistent HB msg received count
    std::map<int32_t, int32_t> _largerVoteNoMsgCount; // voteno->count

    //different leader HB msg receive time
    std::map<int32_t, std::vector<timeval> > _diffMasterMsgInfo; // robotid->receiveTime
};
#endif