/* ==================================================================
*
* Copyright (c) 2016-2019, micROS Group, NIIDT, TAIIC, HPCL.
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions 
* are met: 
*
* 1. Redistributions of source code must retain the above copyright 
*     notice, this list of conditions and the following disclaimer. 
* 2. Redistributions in binary form must reproduce the above copyright 
*     notice, this list of conditions and the following disclaimer in the 
*     documentation and/or other materials provided with the 
*     distribution. 
* 3. All advertising materials mentioning features or use of this software 
*     must display the following acknowledgement: 
*     This product includes software developed by the micROS Group. and 
*     its contributors. 
* 4. Neither the name of the Group nor the names of its contributors may 
*     be used to endorse or promote products derived from this software 
*     without specific prior written permission. 
*
* THIS SOFTWARE IS PROVIDED BY MICROS,GROUP AND CONTRIBUTORS
* ''AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT
* NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
* FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL 
* THE MICROS,GROUP OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS 
* OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) 
* HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
* OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN 
* IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
* ===================================================================
*
* Author: Bin Di, Chunlian Fu, Junling Gao, Ruihao Li, Wei Yi.
*         (in alphabetical order)
*
*/

#include "swarm_master_election/test_bids.h"
#include "swarm_data_storage/swarm_data_storage.h"
#include <ros/ros.h>

//master heart beat period 
const int32_t MASTER_HB_PERIOD = 1000;  // 1s
const int32_t INCONSISTENT_MSG_COUNT = 2;
const int32_t MAX_ROBOTID = 32768;
#define ACTOR_VOTE_TEST

SwarmMasterElectionBids2::SwarmMasterElectionBids2(){
    _robotID = SwarmDataStorage::instance()->getLocalRobotID();
    ros::NodeHandle nh;
    if (nh.hasParam("/HeartBeatPeroid")) {
        nh.getParam("/HeartBeatPeroid", _masterHBPeriod); 
    } else {
        _masterHBPeriod = MASTER_HB_PERIOD;
    }
}

SwarmMasterElectionBids2::~SwarmMasterElectionBids2(){
    if(_fs && _fs.is_open())
        _fs.close();
        
    if(_pPubMasterElection){
        delete _pPubMasterElection;
        _pPubMasterElection = NULL;
    }

    if(_pSubMasterElection){
        delete _pSubMasterElection;
        _pSubMasterElection = NULL;
    }

    if(_pPubMasterHeartBeat){
        delete _pPubMasterHeartBeat;
        _pPubMasterHeartBeat = NULL;
    }

    if(_pSubMasterHeartBeat){
        delete _pSubMasterHeartBeat;
        _pSubMasterHeartBeat = NULL;
    }

    if(_pthMasterHBLoop){
        _pthMasterHBLoop->join();
        _pthMasterHBLoop->interrupt();
        _pthMasterHBLoop.reset();
    }

    if(_pthCheckVoteTimeout){
        _pthCheckVoteTimeout->join();
        _pthCheckVoteTimeout->interrupt();
        _pthCheckVoteTimeout.reset();
    }

    if(_pthForwardVoteMsgDelay){
        _pthForwardVoteMsgDelay->join();
        _pthForwardVoteMsgDelay->interrupt();
        _pthForwardVoteMsgDelay.reset();
    }
}

void SwarmMasterElectionBids2::init(const int32_t aLocalBid, const std::string& aLogFilePath){
    setLocalBid(aLocalBid);
    _voteInfo.reset(_robotID, _localBid);

    #ifdef ACTOR_VOTE_TEST
    ROS_INFO("ROBOTID %d LOACL BID INITIAL %d",_robotID,_localBid);
    #endif

    //open data file
    if(!_fs){
        ROS_ERROR("Test data file cannot be constructed");
    }else{
        if(_fs.is_open()){
            _fs.close();
        }
        //get current time
        timeval timeV;
        gettimeofday(&timeV,NULL);
        time_t timeT = timeV.tv_sec;
        tm *pTm = localtime(&timeT);
        char szTime[16];
        sprintf(szTime,"%04d%02d%02d:%02d%02d%02d",pTm->tm_year+1900,pTm->tm_mon+1, pTm->tm_mday, pTm->tm_hour, pTm->tm_min, pTm->tm_sec);
        std::string fileNameStr = aLogFilePath;
        fileNameStr.append("actor_vote@ID").append(std::to_string(_robotID)).append("-").append(szTime).append(".txt");
        _fs.open(fileNameStr.c_str(),std::fstream::out | std::fstream::app);
        if(!_fs.is_open()){
            ROS_ERROR("open %s failed", fileNameStr.c_str());
        } else {
            _fs<<"sec"<<" "<<"usec"<<" "<<"item"<<" "<<"value"<<std::endl;
            _fs<<timeV.tv_sec<<" "<<timeV.tv_usec<<" "<<"robotID"<<" "<<_robotID<<std::endl;
        }  
    }

    //write masterID
    writeMasterIDtoFile();
    //write voteNo and State
    writeVoteNoStatetoFile();

    //rtps pub sub
    _pPubMasterElection = new RTPSPublisher<MasterElectionBid>("/master_election_bid");
    _pSubMasterElection = new RTPSSubscriber<MasterElectionBid>("/master_election_bid",&SwarmMasterElectionBids2::masterElectionCallback,this); 

    _pPubMasterHeartBeat = new RTPSPublisher<MasterHeartBeat>("/master_heart_beat");
    _pSubMasterHeartBeat = new RTPSSubscriber<MasterHeartBeat>("/master_heart_beat",&SwarmMasterElectionBids2::masterHBCallback,this);

    _pthMasterHBLoop = boost::shared_ptr<boost::thread>(new boost::thread(boost::bind(&SwarmMasterElectionBids2::sendMasterHBLoop,this)));
    _pthMasterHBLoop->detach();

    _pthCheckVoteTimeout = boost::shared_ptr<boost::thread>(new boost::thread(boost::bind(&SwarmMasterElectionBids2::checkVoteTimeoutLoop,this)));
    _pthCheckVoteTimeout->detach();

    _pthForwardVoteMsgDelay = boost::shared_ptr<boost::thread>(new boost::thread(boost::bind(&SwarmMasterElectionBids2::forwardVoteMsgLoop,this)));
    _pthForwardVoteMsgDelay->detach();
}

void SwarmMasterElectionBids2::setLocalBid(const int32_t aLocalBid){
    boost::unique_lock<boost::shared_mutex> wlock(_localBidMutex);
    if(aLocalBid<0){
        srand(_robotID);
        _localBid = rand()%100;
        ROS_ERROR("LocalBid<0, would generate bid randomly, bid=%d", _localBid);
    }else{
        _localBid = aLocalBid;
    }
    ROS_INFO("LocalBid value=%d", _localBid);
}


void SwarmMasterElectionBids2::activateMasterVote(){
    boost::unique_lock<boost::mutex> lock(_voteInfoMutex);

    //write activate vote flag to file
    writeVoteActiOrRecetoFile(1);
    _voteInfo._voteNo++;
    //change the vote state
    SwarmDataStorage::instance()->setLocalRobotState(VOTING);
    //write voteNo and State when changes
    writeVoteNoStatetoFile();

    //save local voteinfo
    _voteInfo.reset(_robotID, _localBid);
    _voteInfo._robotIDs.push_back(_robotID);
    _voteInfo._bids.push_back(_localBid);
    _voteInfo._receivedRobotIDs.insert(_robotID);
    SwarmDataStorage::instance()->getRobotIDs(_voteInfo._voteMembers);
    for(auto it = _diffMasterMsgInfo.begin(); it != _diffMasterMsgInfo.end(); it++){
        if(std::find(_voteInfo._voteMembers.begin(), _voteInfo._voteMembers.end(), it->first) == _voteInfo._voteMembers.end())
            _voteInfo._voteMembers.push_back(it->first);
    }

    _voteInfo._forwardDelay = 0;
    _forwardVoteMsgCond.notify_one();

    srand(_robotID);
    int maxCommLatency = std::max(500000, (int)SwarmDataStorage::instance()->getMaxRobotHBLatency());
    int meanCommLatency = std::max(500000, (int)SwarmDataStorage::instance()->getMeanRobotHBLatency());
    _voteInfo._voteConsensusTimeout = ceil(2 * maxCommLatency + (rand() % meanCommLatency));
    
    //_voteStartCond.notify_one();
    //set start time
    timeval startTime;
    gettimeofday(&startTime,NULL);
    _voteInfo._startTime = startTime;

    //write swarm size to file
    writeSwarmSizetoFile(_voteInfo._voteMembers.size());
    ROS_WARN("activateMasterVote, voteNo=%d", _voteInfo._voteNo);
}

// thread loops
void SwarmMasterElectionBids2::sendMasterHBLoop(){
    while(true){
        writeMasterIDtoFile();
        writeVoteNoStatetoFile();
        //while local robot is the swarm master and consensus achieved, broadcast heart beat msgs
        if(SwarmDataStorage::instance()->getLocalRobotState() == KEEPING && _robotID == SwarmDataStorage::instance()->getMasterID()){          
            boost::unique_lock<boost::mutex> lock(_voteInfoMutex);
            MasterHeartBeat hbMsg;
            hbMsg.voteNo(_voteInfo._voteNo);
            hbMsg.masterID(_robotID);
            _pPubMasterHeartBeat->publish(hbMsg);
        }
        usleep(_masterHBPeriod*1000);//1s
    }
}

void SwarmMasterElectionBids2::checkVoteTimeoutLoop(){
    while(true){
        // boost::mutex::scoped_lock lock(_voteInfoMutex);
        // _voteStartCond.wait(lock);
        // ROS_WARN("checkVoteTimeoutLoop: vote start");
        // if(SwarmDataStorage::instance()->getLocalRobotState() == VOTING && _voteInfo._voteConsensusTimeout > 0){
        //     if(!_voteCompletedCond.timed_wait(lock, boost::get_system_time() + boost::posix_time::time_duration(0, 0, 0, _voteInfo._voteConsensusTimeout))){
        //         // wait timeout, then choose robot with the max bid as the master
        //         ROS_WARN("checkVoteTimeoutLoop:Vote processing cannot achive consensus for a long time, voteNo: %d",_voteInfo._voteNo);
        //         writeEventtoFile("vote donot converge " + std::to_string(_voteInfo._voteNo));
        //         voteConsensusDelay(0);
        //     } else {
        //         ROS_WARN("checkVoteTimeoutLoop: vote completed");
        //     }
        // }
        if(SwarmDataStorage::instance()->getLocalRobotState() == VOTING){
            boost::mutex::scoped_lock lock(_voteInfoMutex);
            timeval now;
            gettimeofday(&now,NULL);
            int32_t diffTime = (now.tv_sec - _voteInfo._startTime.tv_sec) * 1000000 + (now.tv_usec - _voteInfo._startTime.tv_usec);
            if(diffTime>=_voteInfo._voteConsensusTimeout){
                // wait timeout, then choose robot with the max bid as the master
                ROS_WARN("checkVoteTimeoutLoop:Vote processing cannot achive consensus for a long time, voteNo: %d",_voteInfo._voteNo);
                writeEventtoFile("vote donot converge " + std::to_string(_voteInfo._voteNo));
                voteConsensusDelay(0);
            }
        }
        usleep(100*1000);
    }
}

void SwarmMasterElectionBids2::forwardVoteMsgLoop(){
    while(true){
        boost::mutex::scoped_lock lock(_voteInfoMutex);
        _forwardVoteMsgCond.wait(lock);
        ROS_INFO("forwardVoteMsgLoop: get notify");
        if(SwarmDataStorage::instance()->getLocalRobotState() == VOTING ){
            if(_voteInfo._forwardDelay <= 0 || !_forwardVoteMsgCond.timed_wait(lock, boost::get_system_time() + boost::posix_time::time_duration(0, 0, 0, _voteInfo._forwardDelay))){ 
                if(SwarmDataStorage::instance()->getLocalRobotState() == VOTING ){ // check again
                    ROS_INFO("forwardVoteMsgLoop: send msg");
                    MasterElectionBid voteMsg;
                    voteMsg.voteNo(_voteInfo._voteNo);
                    voteMsg.robotID(_robotID);
                    voteMsg.state(_voteInfo._state);
                    voteMsg.robots().assign(_voteInfo._robotIDs.begin(), _voteInfo._robotIDs.end());
                    voteMsg.bids().assign(_voteInfo._bids.begin(), _voteInfo._bids.end());
                    voteMsg.members().assign(_voteInfo._voteMembers.begin(), _voteInfo._voteMembers.end());

                    boost::unique_lock<boost::mutex> _pubMasterElecMutex;
                    _pPubMasterElection->publish(voteMsg);
                } else {
                    ROS_INFO("forwardVoteMsgLoop: send msg cancle");
                }
            } else {
                ROS_INFO("forwardVoteMsgLoop: send msg cancle");
            }
        }
    }
}

// subscribe callback methods
void SwarmMasterElectionBids2::masterHBCallback(MasterHeartBeat &aHeartBeat){
    boost::unique_lock<boost::mutex> lock(_voteInfoMutex);
    int32_t receMasterID = aHeartBeat.masterID();
    int32_t receVoteNo = aHeartBeat.voteNo();
    if(receMasterID == _robotID || receVoteNo < _voteInfo._voteNo) 
        return;

    if(receVoteNo == _voteInfo._voteNo) {
        if(SwarmDataStorage::instance()->getLocalRobotState() == VOTING) {
            // when in voting process, then set the masterID
            ROS_WARN("Robot %d receive master HB msg with voteNo %d while vote processing, change master", _robotID, receVoteNo);
            writeEventtoFile("receive master HB while vote processing");
            // for members, vote ends when receive new master HB, change state and notify 
            _voteInfo._robotIDWithMaxBid = receMasterID;
            voteConsensusDelay(0);
            SwarmDataStorage::instance()->setLocalRobotState(KEEPING);
            writeVoteNoStatetoFile();
            _voteCompletedCond.notify_all();

        } else if (receMasterID != SwarmDataStorage::instance()->getMasterID()) {
            // when in consensus state, handle inconsensus master ids
            lock.unlock();
            handleHBfromUnMatchedMaster(receMasterID);
        }
    } else { // new voteNo, and HBcount enough, then set the masterID
        ROS_WARN("Larger voteNo %d msg received", receVoteNo);
        writeEventtoFile("detect master with larger voteNo " + std::to_string(receMasterID));

        _largerVoteNoMsgCount[receVoteNo]++; // todo clear this
        if (_largerVoteNoMsgCount[receVoteNo] > INCONSISTENT_MSG_COUNT) {
            //write event detect unmatched master HB msg
            writeEventtoFile("changer to master with larger voteNo " + std::to_string(receMasterID));
            
            //change local master vote information
            _voteInfo._voteNo = receVoteNo;
            _voteInfo._robotIDWithMaxBid = receMasterID;
            //change state in voteInfo and masterID in data storage
            voteConsensusDelay(0);
            //change state in data storage, donot notify 
            SwarmDataStorage::instance()->setLocalRobotState(KEEPING);
            writeVoteNoStatetoFile();
        }
    }
}

void SwarmMasterElectionBids2::masterElectionCallback(MasterElectionBid &aElecBidMsg){
    int32_t voteNo = aElecBidMsg.voteNo();
    int32_t receRobotID = aElecBidMsg.robotID();
    if (receRobotID == _robotID)
        return;

    boost::unique_lock<boost::mutex> lock(_voteInfoMutex);
    ROS_WARN("[SwarmMasterElectionBids2] masterElectionCallback voteNO=%d, robotID=%d", voteNo, receRobotID);
    //judge if the robots size == bids size
    if (aElecBidMsg.robots().size() != aElecBidMsg.bids().size()) {
        ROS_ERROR("Robot size != bids size in received msg, return");
        return;
    }

    //judge if the vote msg is from unknown member
    // if(SwarmDataStorage::instance()->getRobotStateByID(receRobotID) == LEAVE){
    //     ROS_ERROR("Robot %d receive vote msg from unknown member %d, return",_robotID,receRobotID);
    //     return;
    // }

    //judge if the platform included in the swarmMembers, if not inclued, return
    if (std::find(aElecBidMsg.members().begin(), aElecBidMsg.members().end(), _robotID) == aElecBidMsg.members().end()) {
        ROS_WARN("Robot %d receive disrelated vote msg from %d, return", _robotID, receRobotID);
        return;
    }

    // judge if this message is out of date
    if (voteNo < _voteInfo._voteNo) {
        ROS_WARN("Robot %d receive invalid voteNo %d from %d, return", _robotID, voteNo, receRobotID);
        return;
    }

    int maxCommLatency = std::max(500000, (int)SwarmDataStorage::instance()->getMaxRobotHBLatency());
    int meanCommLatency = std::max(500000, (int)SwarmDataStorage::instance()->getMeanRobotHBLatency());
    // new vote process
    if (voteNo > _voteInfo._voteNo) {
        writeVoteActiOrRecetoFile(0); //write receive vote msg flag to file
        writeVoteMsgSourcetoFile(receRobotID);

        _voteInfo._voteNo = voteNo;

        SwarmDataStorage::instance()->setLocalRobotState(VOTING);
        writeVoteNoStatetoFile();

        //update the vote info
        _voteInfo.reset(_robotID, _localBid);
        _voteInfo._robotIDs.assign(aElecBidMsg.robots().begin(), aElecBidMsg.robots().end());
        _voteInfo._robotIDs.push_back(_robotID);
        _voteInfo._bids.assign(aElecBidMsg.bids().begin(), aElecBidMsg.bids().end());
        _voteInfo._bids.push_back(_localBid);
        _voteInfo._voteMembers.assign(aElecBidMsg.members().begin(), aElecBidMsg.members().end());
        _voteInfo._receivedRobotIDs.insert(receRobotID);
        _voteInfo._receivedRobotIDs.insert(_robotID);

        _voteInfo._maxBid = *std::max_element(_voteInfo._bids.begin(), _voteInfo._bids.end());
        _voteInfo._robotIDWithMaxBid = MAX_ROBOTID;
        for (int i = 0; i < _voteInfo._bids.size(); i++) {
            if (_voteInfo._maxBid == _voteInfo._bids.at(i) && _voteInfo._robotIDWithMaxBid > _voteInfo._robotIDs.at(i)) {
                _voteInfo._robotIDWithMaxBid = _voteInfo._robotIDs.at(i);
            }
        }
        _voteInfo._state = VOTING;
        _voteInfo._forwardDelay = 0;
        _forwardVoteMsgCond.notify_one();

        _voteInfo._voteConsensusTimeout = ceil(2 * maxCommLatency + (rand() % meanCommLatency));
        //_voteStartCond.notify_one();
        //set start time
        timeval startTime;
        gettimeofday(&startTime,NULL);
        _voteInfo._startTime = startTime;

        ROS_WARN("[SwarmMasterElectionBids2] masterElectionCallback maxBid=%d, maxBidRobotID=%d", _voteInfo._maxBid, _voteInfo._robotIDWithMaxBid);
    } else if (SwarmDataStorage::instance()->getLocalRobotState() == VOTING){ 
        // this robot is in voting process
        // merge with local voteinfo and publish
        _voteInfo._receivedRobotIDs.insert(receRobotID);
        for (int i = 0; i < aElecBidMsg.robots().size(); i++) {
            int16_t tmpRobotID = aElecBidMsg.robots()[i];
            if (std::find(_voteInfo._robotIDs.begin(), _voteInfo._robotIDs.end(), tmpRobotID) == _voteInfo._robotIDs.end()) {
                int16_t tmpBid = aElecBidMsg.bids()[i];
                _voteInfo._robotIDs.push_back(tmpRobotID);
                _voteInfo._bids.push_back(tmpBid);
                if ((_voteInfo._maxBid < tmpBid) || (_voteInfo._maxBid == tmpBid && _voteInfo._robotIDWithMaxBid > tmpRobotID)) {
                    _voteInfo._maxBid = tmpBid;
                    _voteInfo._robotIDWithMaxBid = tmpRobotID;
                }
            }
        }
        _voteInfo._forwardDelay = maxCommLatency / _voteInfo._robotIDs.size();
        _forwardVoteMsgCond.notify_one();
    }

    //judge if consensus, judge if received more than half(2/3) of the swarm or local voteinfo contain all members bid
    if (SwarmDataStorage::instance()->getLocalRobotState() == VOTING)
    if (_voteInfo._robotIDs.size() >= _voteInfo._voteMembers.size()) {
        lock.unlock();
        voteConsensusDelay(0);
    } else {
        if (_voteInfo._robotIDs.size() > (_voteInfo._voteMembers.size() * 2.0 / 3.0)) {
            lock.unlock();
            boost::thread voteConsensusDelayThread(boost::bind(&SwarmMasterElectionBids2::voteConsensusDelay, this, meanCommLatency));
            voteConsensusDelayThread.detach();
        }
    }
}

void SwarmMasterElectionBids2::handleHBfromUnMatchedMaster(const int32_t aMasterID){
    ROS_WARN("Robot [%d] receive unmatched master HB msg with masterID [%d]", _robotID, aMasterID);
    writeEventtoFile("detect unmatched master " + std::to_string(aMasterID));

    boost::unique_lock<boost::mutex> lock(_voteInfoMutex);
    //get time of the msg comes
    timeval msgReceTime;
    gettimeofday(&msgReceTime,NULL);
    _diffMasterMsgInfo[aMasterID].push_back(msgReceTime);

    //if receive inconsistent leaderHB msg more than one master, which means there are at least three masters, start a new vote
    ROS_WARN("[SwarmMasterElectionBids2]receiveHBfromUnMatchedMaster :diff count=%d", (int)_diffMasterMsgInfo.size());
    if(_diffMasterMsgInfo.size()>1){
        lock.unlock();
        activateMasterVote();
        _diffMasterMsgInfo.clear();

    } else if (_diffMasterMsgInfo[aMasterID].size() > INCONSISTENT_MSG_COUNT){
        int masterID = SwarmDataStorage::instance()->getMasterID();

        // local is master, then elect master by compare robot id
        if(_robotID == masterID){
            ROS_WARN("Master [%d] receive unmatched master HB msg with masterID [%d], compare ID", _robotID, aMasterID);
            if(masterID > aMasterID) {
                writeEventtoFile("master change to another master " + std::to_string(aMasterID));
                _voteInfo._robotIDWithMaxBid = aMasterID;
                //set masterID in data storage
                SwarmDataStorage::instance()->setMasterID(_voteInfo._robotIDWithMaxBid);
                writeMasterIDtoFile();
                _diffMasterMsgInfo.clear();
            } else {
                //for the platform who has smaller ID, remains to be master, ought to do nothing
                //filter the msgs comes simultanously
                for(auto it = _diffMasterMsgInfo[aMasterID].begin(); it != _diffMasterMsgInfo[aMasterID].end();){
                    auto itNext = it + 1;
                    if (itNext == _diffMasterMsgInfo[aMasterID].end()) break;
                    double diffTime = (itNext->tv_sec - it->tv_sec) * 1000 + (itNext->tv_usec - it->tv_usec) / 1000.0;
                    if (diffTime < (_masterHBPeriod / 5)) {
                        it = _diffMasterMsgInfo[aMasterID].erase(it);
                    } else {
                        it++;
                    }
                }
                //if receives much more hb, two times of INCONSISTENT_MSG_COUNT
                if(_diffMasterMsgInfo[aMasterID].size() > 2*INCONSISTENT_MSG_COUNT){
                    ROS_ERROR("THERE MAY EXISTS ANOTHER MASTER IN THE SWARM WHO CANNOT RECEIVE MSG...");
                    writeEventtoFile("ANOTHER MASTER REMAINS: " + std::to_string(aMasterID));
                }
            }
        } else { // local is member robot
            //for swarm members,if it receive more new master HB and do not receive former master HB for some time,change masterID
            if((_diffMasterMsgInfo[aMasterID].size() > 2*INCONSISTENT_MSG_COUNT)){//&&(_timerCount>TIME_OUT_COUNT/3)){
                ROS_WARN("Robot %d change masterID from %d to %d", _robotID, masterID, aMasterID);
                writeEventtoFile("member change to another masterID " + std::to_string(aMasterID));
                _voteInfo._robotIDWithMaxBid = aMasterID;
                //set masterID in data storage
                SwarmDataStorage::instance()->setMasterID(_voteInfo._robotIDWithMaxBid);
                writeMasterIDtoFile();         
            }
        }
    }
}

//called under _voteInfoMutex
void SwarmMasterElectionBids2::voteConsensusDelay(const int aDelayTime){//todo lock when in new thread
    usleep(aDelayTime * 1000);

    //boost::unique_lock<boost::mutex> lock(_voteInfoMutex);
    ROS_WARN("CONSENSUS: cur master=%d", _voteInfo._robotIDWithMaxBid);
    //reset voteInfo
    _voteInfo._state = KEEPING;
    _voteInfo._forwardDelay = 0;
    _voteInfo._voteConsensusTimeout = 0;
    //set masterID in data storage
    SwarmDataStorage::instance()->setMasterID(_voteInfo._robotIDWithMaxBid);
    writeMasterIDtoFile();
    //if master, change state, notify and send master HB immediately
    if(_robotID==_voteInfo._robotIDWithMaxBid){
        SwarmDataStorage::instance()->setLocalRobotState(KEEPING);
        writeVoteNoStatetoFile();
        _voteCompletedCond.notify_all();
        MasterHeartBeat hbMsg;
        hbMsg.voteNo(_voteInfo._voteNo);
        hbMsg.masterID(_robotID);
        _pPubMasterHeartBeat->publish(hbMsg);
    }
}

// write log methods
void SwarmMasterElectionBids2::writeLocalTime(){
    boost::unique_lock<boost::mutex> lock(_fsMutex);
    if(!_fs) return;
    if(!_fs.is_open()) return;
    timeval timeV;
	gettimeofday(&timeV,NULL);
     _fs<<timeV.tv_sec<<" "<<timeV.tv_usec<<" ";
}

void SwarmMasterElectionBids2::writeMasterIDtoFile(){
    writeLocalTime();
    boost::unique_lock<boost::mutex> lock(_fsMutex);
    _fs<<"masterID "<<SwarmDataStorage::instance()->getMasterID()<<std::endl;
}

void SwarmMasterElectionBids2::writeVoteNoStatetoFile(){
    writeLocalTime();
    boost::unique_lock<boost::mutex> lock(_fsMutex);
    _fs<<"voteNo "<<_voteInfo._voteNo<<" voteState "<<SwarmDataStorage::instance()->getLocalRobotState()<<std::endl;
}

void SwarmMasterElectionBids2::writeVoteActiOrRecetoFile(const int aFlag){
    writeLocalTime();
    boost::unique_lock<boost::mutex> lock(_fsMutex);
    _fs<<"voteActiOrRece "<<aFlag<<std::endl;
}

void SwarmMasterElectionBids2::writeSwarmSizetoFile(const int32_t aSize){
    writeLocalTime();
    boost::unique_lock<boost::mutex> lock(_fsMutex);
    _fs<<"swarmSize "<<aSize<<std::endl;
}

void SwarmMasterElectionBids2::writeEventtoFile(const std::string& aEventStr){
    writeLocalTime();
    boost::unique_lock<boost::mutex> lock(_fsMutex);
    _fs<<"event "<<aEventStr<<std::endl;
}

void SwarmMasterElectionBids2::writeVoteCounttoFile(){
    writeLocalTime();
    boost::unique_lock<boost::mutex> lock(_fsMutex);
    // _fs<<"voteCount "<<_voteCount<<std::endl;
}

void SwarmMasterElectionBids2::writeVoteMsgSourcetoFile(const int32_t aRobotID){
    writeLocalTime();
    boost::unique_lock<boost::mutex> lock(_fsMutex);
    _fs<<"voteMsgFrom "<<aRobotID<<std::endl;
}

void SwarmMasterElectionBids2::VoteInfo::reset(const int aMasterID, const int aMaxBid) {
    _robotIDWithMaxBid = aMasterID;
    _maxBid = aMaxBid;
    _state = VOTING;
    _forwardDelay = 0;
    _voteConsensusTimeout = 0;
    _voteMembers.clear();
    _receivedRobotIDs.clear();
    _robotIDs.clear();
    _bids.clear();
}