/* ==================================================================
*
* Copyright (c) 2016-2019, micROS Group, NIIDT, TAIIC, HPCL.
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions 
* are met: 
*
* 1. Redistributions of source code must retain the above copyright 
*     notice, this list of conditions and the following disclaimer. 
* 2. Redistributions in binary form must reproduce the above copyright 
*     notice, this list of conditions and the following disclaimer in the 
*     documentation and/or other materials provided with the 
*     distribution. 
* 3. All advertising materials mentioning features or use of this software 
*     must display the following acknowledgement: 
*     This product includes software developed by the micROS Group. and 
*     its contributors. 
* 4. Neither the name of the Group nor the names of its contributors may 
*     be used to endorse or promote products derived from this software 
*     without specific prior written permission. 
*
* THIS SOFTWARE IS PROVIDED BY MICROS,GROUP AND CONTRIBUTORS
* ''AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT
* NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
* FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL 
* THE MICROS,GROUP OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS 
* OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) 
* HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
* OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN 
* IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
* ===================================================================
*
* Author: Bin Di, Chunlian Fu, Junling Gao, Ruihao Li, Wei Yi.
*         (in alphabetical order)
*
*/

#include <unistd.h>
#include "swarm_master_election/swarm_master_election_bids.h"
#include "swarm_data_storage/swarm_data_storage.h"
#define ACTOR_VOTE_TEST

SwarmMasterElectionBids::SwarmMasterElectionBids(){
    _robotID = SwarmDataStorage::instance()->getLocalRobotID();
}

void SwarmMasterElectionBids::init(int32_t aLocalBid, std::string aLogFilePath) {
    //local bid
    if(aLocalBid<0){
        srand(_robotID);
        _localBid = rand()%100;
        ROS_ERROR("LocalBid<0, would generate bid randomly, bid=%d", _localBid);
    }else{
        _localBid = aLocalBid;
    }
    
    #ifdef ACTOR_VOTE_TEST
    ROS_INFO("ROBOTID %d LOACL BID INITIAL %d",_robotID,_localBid);
    #endif

    ros::NodeHandle nh;
    if (nh.hasParam("/HeartBeatPeroid")) {
        nh.getParam("/HeartBeatPeroid", _masterHBPeriod); 
    } else {
        _masterHBPeriod = MASTER_HB_PERIOD;
    }
   
    //timer count
    _timerCount = 0;
    _voteTimerCount = 0;
    //vote No and State
    _masterVoteNo = 0;
    //vote msg count, for test
    _voteCount = 0;

    //open data file
    if(!_fs){
        ROS_ERROR("Test data file cannot be constructed");
    }else{
        if(_fs.is_open()){
            _fs.close();
        }
        //get current time
        timeval timeV;
        gettimeofday(&timeV,NULL);
        time_t timeT = timeV.tv_sec;
        tm *pTm = localtime(&timeT);
        char szTime[16];
        sprintf(szTime,"%04d%02d%02d:%02d%02d%02d",pTm->tm_year+1900,pTm->tm_mon+1, pTm->tm_mday, pTm->tm_hour, pTm->tm_min, pTm->tm_sec);
        std::string curTimeStr(szTime);
        //get robotID
        std::string robotIDStr;
        std::stringstream ss;
        ss.str();
        ss<<_robotID;
        ss>>robotIDStr;
        std::string fileNameStr = aLogFilePath +"actor_vote@ID" + robotIDStr;
        fileNameStr += "-";
        fileNameStr += curTimeStr;
        fileNameStr += ".txt";
        _fs.open(fileNameStr.c_str(),std::fstream::out | std::fstream::app);
        if(!_fs.is_open()){
            ROS_ERROR("open %s failed", fileNameStr.c_str());
        }
        _fs<<"sec"<<" "<<"usec"<<" "<<"item"<<" "<<"value"<<std::endl;
    }
    //save robotID and initial masterID
    timeval timeV;
	gettimeofday(&timeV,NULL);
    _fs<<timeV.tv_sec<<" "<<timeV.tv_usec<<" "<<"robotID"<<" "<<_robotID<<std::endl;
    //write masterID
    writeMasterIDtoFile();
    //write voteNo and State
    writeVoteNoStatetoFile();

    //rtps pub sub
    _pPubMasterElection = new RTPSPublisher<MasterElectionBid>("/master_election_bid");
    _pSubMasterElection = new RTPSSubscriber<MasterElectionBid>("/master_election_bid",&SwarmMasterElectionBids::masterElectionCallback,this); 

    _pPubMasterHeartBeat = new RTPSPublisher<MasterHeartBeat>("/master_heart_beat");
    _pSubMasterHeartBeat = new RTPSSubscriber<MasterHeartBeat>("/master_heart_beat",&SwarmMasterElectionBids::masterHBCallback,this);

    //master heart beat loop thread
    _pthMasterHBLoop = boost::shared_ptr<boost::thread>(new boost::thread(boost::bind(&SwarmMasterElectionBids::sendMasterHBLoop,this)));
    _pthMasterHBLoop->detach();

    //master heart beat count thread
    _pthMasterHBCountLoop = boost::shared_ptr<boost::thread>(new boost::thread(boost::bind(&SwarmMasterElectionBids::masterHBCountLoop,this)));
    _pthMasterHBCountLoop->detach();

}

SwarmMasterElectionBids::~SwarmMasterElectionBids(){
    if(_pPubMasterElection){
        delete _pPubMasterElection;
        _pPubMasterElection = NULL;
    }

    if(_pSubMasterElection){
        delete _pSubMasterElection;
        _pSubMasterElection = NULL;
    }

    if(_pPubMasterHeartBeat){
        delete _pPubMasterHeartBeat;
        _pPubMasterHeartBeat = NULL;
    }

    if(_pSubMasterHeartBeat){
        delete _pSubMasterHeartBeat;
        _pSubMasterHeartBeat = NULL;
    }

    if(_pthMasterHBLoop){
        _pthMasterHBLoop->join();
        _pthMasterHBLoop->interrupt();
        _pthMasterHBLoop.reset();
    }

    if(_pthMasterHBCountLoop){
        _pthMasterHBCountLoop->join();
        _pthMasterHBCountLoop->interrupt();
        _pthMasterHBCountLoop.reset();
    }

    if(_fs && _fs.is_open())
        _fs.close();
}

void SwarmMasterElectionBids::writeMasterIDtoFile(){
    boost::unique_lock<boost::mutex> lock(_fsMutex);
    if(!_fs) return;
    if(!_fs.is_open()) return;
    timeval timeV;
	gettimeofday(&timeV,NULL);
    _fs<<timeV.tv_sec<<" "<<timeV.tv_usec<<" "<<"masterID"<<" "<<SwarmDataStorage::instance()->getMasterID()<<std::endl;
}

void SwarmMasterElectionBids::writeVoteNoStatetoFile(){
    boost::unique_lock<boost::mutex> lock(_fsMutex);
    if(!_fs) return;
    if(!_fs.is_open()) return;
    timeval timeV;
	gettimeofday(&timeV,NULL);
    _fs<<timeV.tv_sec<<" "<<timeV.tv_usec<<" "<<"voteNo"<<" "<<_masterVoteNo<<std::endl;
    _fs<<timeV.tv_sec<<" "<<timeV.tv_usec<<" "<<"voteState"<<" "<<(SwarmDataStorage::instance()->getLocalRobotState() == VOTING ? VOTE_PROCESSING : VOTE_CONSENSUS)<<std::endl;
}

void SwarmMasterElectionBids::writeVoteActiOrRecetoFile(int aFlag){
    boost::unique_lock<boost::mutex> lock(_fsMutex);
    if(!_fs) return;
    if(!_fs.is_open()) return;
    timeval timeV;
	gettimeofday(&timeV,NULL);
    _fs<<timeV.tv_sec<<" "<<timeV.tv_usec<<" "<<"voteActiOrRece"<<" "<<aFlag<<std::endl;
}

void SwarmMasterElectionBids::writeSwarmSizetoFile(int32_t aSize){
    boost::unique_lock<boost::mutex> lock(_fsMutex);
    if(!_fs) return;
    if(!_fs.is_open()) return;
    timeval timeV;
	gettimeofday(&timeV,NULL);
    _fs<<timeV.tv_sec<<" "<<timeV.tv_usec<<" "<<"swarmSize"<<" "<<aSize<<std::endl;
}

void SwarmMasterElectionBids::writeEventtoFile(std::string aEventStr){
    boost::unique_lock<boost::mutex> lock(_fsMutex);
    if(!_fs) return;
    if(!_fs.is_open()) return;
    timeval timeV;
	gettimeofday(&timeV,NULL);
    _fs<<timeV.tv_sec<<" "<<timeV.tv_usec<<" "<<"event"<<" "<<aEventStr<<std::endl;
}

void SwarmMasterElectionBids::writeVoteCounttoFile(){
    boost::unique_lock<boost::mutex> lock(_fsMutex);
    if(!_fs) return;
    if(!_fs.is_open()) return;
    timeval timeV;
	gettimeofday(&timeV,NULL);
    _fs<<timeV.tv_sec<<" "<<timeV.tv_usec<<" "<<"voteCount"<<" "<<_voteCount<<std::endl;
}

void SwarmMasterElectionBids::writeVoteMsgSourcetoFile(int32_t aRobotID){
    boost::unique_lock<boost::mutex> lock(_fsMutex);
    if(!_fs) return;
    if(!_fs.is_open()) return;
    timeval timeV;
	gettimeofday(&timeV,NULL);
    _fs<<timeV.tv_sec<<" "<<timeV.tv_usec<<" "<<"voteMsgFrom"<<" "<<aRobotID<<std::endl;
}


void SwarmMasterElectionBids::setLocalBid(int32_t aLocalBid){
    _localBid = aLocalBid;
    #ifdef ACTOR_VOTE_TEST
    ROS_INFO("SET LOCAL BID %d",_localBid);
    #endif
}

//delay vote msg, for test, TODO: only one thread
void SwarmMasterElectionBids::voteMsgDelay(int32_t aDelayTime,int32_t aVoteNo,int32_t aMsgIndex,MasterElectionBid *aPVoteMsg){
    //delay
    ROS_WARN("Vote msg delay %d ms", aDelayTime);
    usleep(aDelayTime * 1000);
    boost::shared_lock<boost::shared_mutex> msgDelayReadLock(_voteMsgDelayMapMutex);
    if (_voteMsgDelayMap.count(aVoteNo) > 0 && _voteMsgDelayMap[aVoteNo].at(aMsgIndex) > 0){// TODO just keep the newest vote information
        _pPubMasterElection->publish(*aPVoteMsg);
        ROS_WARN("Vote msg resend after delayed");
    } else {
        ROS_WARN("Vote msg resend cancelled");
    }
    _voteMsgDelayMap.erase(aVoteNo);
    delete aPVoteMsg;
}

//delay consensus for the case of received more than half(2/3) of the swarm members, for test
void SwarmMasterElectionBids::voteConsensusDelay(int32_t aDelayTime,int32_t aVoteNo,int32_t aMaxRobotID){
    //delay or not
    ROS_WARN("Vote consensus delay %d ms", aDelayTime);
    usleep(aDelayTime * 1000);
    //mutex
    boost::recursive_mutex::scoped_lock lock(_recurMutex);
    if ((aVoteNo == _masterVoteNo) && (_voteInfoMap[aVoteNo]._state != VOTE_CONSENSUS)) {
        _voteInfoMap[aVoteNo]._state = VOTE_CONSENSUS;
        //write voteNo and State when changes
        writeVoteNoStatetoFile();

        SwarmDataStorage::instance()->setMasterID(aMaxRobotID);
        //write leaderID
        writeMasterIDtoFile();

        _timerCount = 0;
        //if local platform is chosen master,master start publish HB msg,
        //followers recevied master HB msg then consider the vote is consensus
        if (_robotID == aMaxRobotID) {
            //change to CONSENSUS and send master hb
            _voteTimerCount = 0;
            SwarmDataStorage::instance()->setLocalRobotState(KEEPING);
            _voteCompletedCond.notify_one();
            sendMasterHB();
        }

        // erase vote msg ready to publish
        boost::unique_lock<boost::shared_mutex> msgDelayWriteLock(_voteMsgDelayMapMutex);
        int32_t msgIndex = 0;
        for(;msgIndex<_voteMsgDelayMap[_masterVoteNo].size();msgIndex++){
            _voteMsgDelayMap[_masterVoteNo].at(msgIndex) = 0;
        }
        msgDelayWriteLock.unlock();
    }
}

void SwarmMasterElectionBids::masterElectionCallback(MasterElectionBid &aElecBidMsg){
    int32_t voteNo = aElecBidMsg.voteNo();
    int32_t receRobotID = aElecBidMsg.robotID();
    //receive msg from itself, return
    if(receRobotID==_robotID) return;
    ROS_WARN("[SwarmMasterElectionBids] masterElectionCallback voteNO=%d, robotID=%d", voteNo, receRobotID);
    
    boost::recursive_mutex::scoped_lock lock(_recurMutex);

    //judge if the robots size == bids size
    if(aElecBidMsg.robots().size()!=aElecBidMsg.bids().size()){
        ROS_ERROR("Robot size != bids size in received msg, return");
        return;
    }

    //judge if the vote msg is from unknown member
    // if(SwarmDataStorage::instance()->getRobotStateByID(receRobotID) == LEAVE){
    //     ROS_ERROR("Robot %d receive vote msg from unknown member %d, return",_robotID,receRobotID);
    //     return;
    // }

    //judge if the platform included in the swarmMembers, if not inclued, return
    if(std::find(aElecBidMsg.members().begin(), aElecBidMsg.members().end(), _robotID) == aElecBidMsg.members().end()){
        ROS_WARN("Robot %d receive disrelated vote msg from %d, return",_robotID,receRobotID);
        return;
    }

    if(voteNo < _masterVoteNo){
        ROS_WARN("Robot %d receive invalid voteNo %d from %d, return", _robotID, voteNo, receRobotID);
        return;
    }

    int maxCommLatency = std::max(500000, (int)SwarmDataStorage::instance()->getMaxRobotHBLatency());
    int meanCommLatency = std::max(500000, (int)SwarmDataStorage::instance()->getMeanRobotHBLatency());
    ROS_WARN("maxCommLatency=%d, meanCommLatency=%d", maxCommLatency, meanCommLatency);
    // new vote process
    if(voteNo > _masterVoteNo){
        writeVoteActiOrRecetoFile(0); //write receive vote msg flag to file
        _masterVoteNo = voteNo;
        _voteCount = 0;         //vote msg count, for test
        _voteTimerCount = 0;    //reset vote timer
        _masterVoteTimeOut = ceil(2*maxCommLatency+(rand()%meanCommLatency));
        //_masterVoteTimeOut = std::max(1, (int)ceil((2*maxCommLatency+(rand()%meanCommLatency))/COUNT_DURATION));
        SwarmDataStorage::instance()->setLocalRobotState(VOTING);

        //write to file
        writeVoteCounttoFile();
        //vote msg source plat
        writeVoteMsgSourcetoFile(receRobotID);
        //write voteNo and State when changes
        writeVoteNoStatetoFile();

        MasterElectionBid voteMsg(aElecBidMsg);
        voteMsg.robotID(_robotID);
        voteMsg.state(VOTE_PROCESSING);
        voteMsg.robots().push_back(_robotID);
        voteMsg.bids().push_back(_localBid);

        //get the vote info
        VoteInfo voteInfo;
        voteInfo._voteNo = voteNo;
        voteInfo._robotID = _robotID;
        voteInfo._robotIDs.assign(voteMsg.robots().begin(), voteMsg.robots().end());
        voteInfo._bids.assign(voteMsg.bids().begin(), voteMsg.bids().end());
        voteInfo._voteMembers.assign(voteMsg.members().begin(), voteMsg.members().end());
        voteInfo._receivedRobotIDs.insert(receRobotID);
        voteInfo._receivedRobotIDs.insert(_robotID);

        voteInfo._maxBid = *std::max_element(voteInfo._bids.begin(), voteInfo._bids.end());
        voteInfo._robotIDWithMaxBid = MAX_ROBOTID;
        for(int i = 0; i < voteInfo._bids.size(); i++){
            if(voteInfo._maxBid == voteInfo._bids.at(i) && voteInfo._robotIDWithMaxBid > voteInfo._robotIDs.at(i)){
                voteInfo._robotIDWithMaxBid = voteInfo._robotIDs.at(i);
            }
        }
        voteInfo._state = voteMsg.state();
        _voteInfoMap[voteNo] = voteInfo;
        ROS_WARN("[SwarmMasterElectionBids] masterElectionCallback maxBid=%d, maxBidRobotID=%d", voteInfo._maxBid, voteInfo._robotIDWithMaxBid);
        
        //pub the vote msg
        _pPubMasterElection->publish(voteMsg);
    } else {// this robot is in voting process 
        _voteCount++;
        if(SwarmDataStorage::instance()->getLocalRobotState() != VOTING) return;

        //merge with local voteinfo and publish
        //get received platformID
        VoteInfo &voteInfo = _voteInfoMap[voteNo];
        voteInfo._receivedRobotIDs.insert(receRobotID);

        //merge the received bids
        for(int i=0;i<aElecBidMsg.robots().size();i++){
            int16_t tmpRobotID = aElecBidMsg.robots()[i];
            if(std::find(voteInfo._robotIDs.begin(), voteInfo._robotIDs.end(), tmpRobotID) == voteInfo._robotIDs.end()){
                int16_t tmpBid = aElecBidMsg.bids()[i];
                voteInfo._robotIDs.push_back(tmpRobotID);
                voteInfo._bids.push_back(tmpBid);
                if((voteInfo._maxBid < tmpBid) || (voteInfo._maxBid == tmpBid && voteInfo._robotIDWithMaxBid > tmpRobotID)){
                    voteInfo._maxBid = tmpBid;
                    voteInfo._robotIDWithMaxBid = tmpRobotID;
                }
            }
        }
        ROS_WARN("[SwarmMasterElectionBids] masterElectionCallback maxBid=%d, maxBidRobotID=%d", voteInfo._maxBid, voteInfo._robotIDWithMaxBid);

        //cancel former msgs
        boost::unique_lock<boost::shared_mutex> msgDelayWriteLock(_voteMsgDelayMapMutex);
        int32_t msgIndex = 0;
        for(;msgIndex<_voteMsgDelayMap[_masterVoteNo].size();msgIndex++){
            _voteMsgDelayMap[_masterVoteNo].at(msgIndex) = 0;
        }
       
        _voteMsgDelayMap[_masterVoteNo].push_back(1);
        msgIndex = _voteMsgDelayMap[_masterVoteNo].size()-1;
        msgDelayWriteLock.unlock();

        //compute delay duration, delay duration decreases
        int32_t delayDur = maxCommLatency/(msgIndex+1);
        
        //create thread to delay vote msg
        MasterElectionBid *pVoteMsg = new MasterElectionBid();
        pVoteMsg->voteNo(voteNo);
        pVoteMsg->robotID(_robotID);
        pVoteMsg->state(voteInfo._state);
        pVoteMsg->robots().assign(voteInfo._robotIDs.begin(), voteInfo._robotIDs.end());
        pVoteMsg->bids().assign(voteInfo._bids.begin(), voteInfo._bids.end());
        pVoteMsg->members().assign(voteInfo._voteMembers.begin(), voteInfo._voteMembers.end());

        _voteMsgDelayTG.create_thread(boost::bind(&SwarmMasterElectionBids::voteMsgDelay,this,delayDur,_masterVoteNo,msgIndex,pVoteMsg));
    }

    //judge if consensus, judge if received more than half(2/3) of the swarm or local voteinfo contain all members bid
    VoteInfo &voteInfo = _voteInfoMap[voteNo];
    ROS_WARN("robotIDs.size=%lu, voteMembers.size=%lu", voteInfo._robotIDs.size(), voteInfo._voteMembers.size());
    if(voteInfo._robotIDs.size()>=voteInfo._voteMembers.size()){
        voteConsensusDelay(0,_masterVoteNo,voteInfo._robotIDWithMaxBid);
    }else{ 
        if(voteInfo._robotIDs.size()>(voteInfo._voteMembers.size()*2.0/3.0)){//TODO merge thread
            boost::thread voteConsensusDelayThread(boost::bind(&SwarmMasterElectionBids::voteConsensusDelay,this,meanCommLatency,_masterVoteNo,voteInfo._robotIDWithMaxBid));
            voteConsensusDelayThread.detach();
        }
    }
}

//receive matched master HB
void SwarmMasterElectionBids::receiveHBfromMatchedMaster(int32_t aMasterID){
    //ROS_WARN("Robot [%d] receive master HB  with masterID [%d]", _robotID, aMasterID);
    _timerCount = 0;
}

//receive unmatched master HB
void SwarmMasterElectionBids::receiveHBfromUnMatchedMaster(int32_t aMasterID){
    //HB msg from another master with the same voteNo
    ROS_WARN("Robot [%d] receive unmatched master HB msg with masterID [%d]", _robotID, aMasterID);
    //write event detect unmatched master HB msg
    std::string eventStr = "detect unmatched master " + std::to_string(aMasterID);
    writeEventtoFile(eventStr);

    //rewrite this function using msg receive time list 180919
    //get time of the msg comes
    timeval msgReceTime;
    gettimeofday(&msgReceTime,NULL);

    if(_diffMasterMsgInfo.count(aMasterID)==0){
        _diffMasterMsgInfo[aMasterID]._voteNo = _masterVoteNo;
        _diffMasterMsgInfo[aMasterID]._msgReceTimeList.push_back(msgReceTime);
    }else{
        if(_diffMasterMsgInfo[aMasterID]._voteNo==_masterVoteNo)
        _diffMasterMsgInfo[aMasterID]._msgReceTimeList.push_back(msgReceTime);
    }   
    
    //erase the info with former _voteNo
    for(std::map<int32_t,MsgReceTimeInfo>::iterator it=_diffMasterMsgInfo.begin();it!=_diffMasterMsgInfo.end();){
        if(it->second._voteNo!=_masterVoteNo) it = _diffMasterMsgInfo.erase(it);
        else it++;
    }

    //if receive inconsistent leaderHB msg more than one master, which means there are at least three masters, start a new vote
    ROS_WARN("[SwarmMasterElectionBids]receiveHBfromUnMatchedMaster :diff count=%lu", _diffMasterMsgInfo.size());
    if(_diffMasterMsgInfo.size()>1){
        activateMasterVote();
        _diffMasterMsgInfo.clear();
    }else{
        //only two masters,master compare the robotID
        if(_diffMasterMsgInfo[aMasterID]._msgReceTimeList.size()>INCONSISTENT_MSG_COUNT){
            int masterID = SwarmDataStorage::instance()->getMasterID();
            if(_robotID == masterID){
                ROS_WARN("Master [%d] receive unmatched master HB msg with masterID [%d], compare ID",_robotID,aMasterID);
                //compare the robotID
                if(masterID>aMasterID) {//for the robot who has larger ID, change its masterID
                    SwarmDataStorage::instance()->setMasterID(aMasterID);
                    //write event detect unmatched master HB msg
                    std::string eventStr = "master change to another master " + std::to_string(aMasterID);
                    writeEventtoFile(eventStr);
                    //write leaderID to file when _masterID changes
                    writeMasterIDtoFile();
                    //clear
                    _diffMasterMsgInfo.clear();
                }else{//for the platform who has smaller ID, remains to be master, ought to do nothing
                    //filter the msgs comes simultanously
                    for(std::vector<timeval>::iterator it=_diffMasterMsgInfo[aMasterID]._msgReceTimeList.begin();it!=_diffMasterMsgInfo[aMasterID]._msgReceTimeList.end();){
                        std::vector<timeval>::iterator itNext = it + 1;
                        if(itNext==_diffMasterMsgInfo[aMasterID]._msgReceTimeList.end()) break;
                        double diffTime = (itNext->tv_sec-it->tv_sec)*1000+(itNext->tv_usec-it->tv_usec)/1000.0;
                        if(diffTime<(_masterHBPeriod/5)){
                            it = _diffMasterMsgInfo[aMasterID]._msgReceTimeList.erase(it);
                        }else{
                            it++;
                        }
                    }
                    //if receives much more hb, two times of INCONSISTENT_MSG_COUNT
                    if(_diffMasterMsgInfo[aMasterID]._msgReceTimeList.size()>2*INCONSISTENT_MSG_COUNT){
                        ROS_ERROR("THERE MAY EXISTS ANOTHER MASTER IN THE SWARM WHO CANNOT RECEIVE MSG...");
                        std::string eventStr = "ANOTHER MASTER REMAINS: " + std::to_string(aMasterID);
                        writeEventtoFile(eventStr);
                    }
                }
                
            }else{
                //for swarm members,if it receive more new master HB and do not receive former master HB for some time,change masterID
                if((_diffMasterMsgInfo[aMasterID]._msgReceTimeList.size()>2*INCONSISTENT_MSG_COUNT)&&(_timerCount>TIME_OUT_COUNT/3)){
                    ROS_WARN("Robot %d change masterID from %d to %d",_robotID,masterID,aMasterID);
                    SwarmDataStorage::instance()->setMasterID(aMasterID);
                    //write event detect unmatched master HB msg
                    std::string eventStr = "member change to another masterID " + std::to_string(aMasterID);
                    writeEventtoFile(eventStr);
                    //write masterID to file when _masterID changes
                    writeMasterIDtoFile();
                }
            }
        }
    }

}

//master HB callback
void SwarmMasterElectionBids::masterHBCallback(MasterHeartBeat &aHeartBeat){
    
    boost::recursive_mutex::scoped_lock lock(_recurMutex);
    int32_t receMasterID = aHeartBeat.masterID();
    int32_t receVoteNo = aHeartBeat.voteNo();
    if(receMasterID==_robotID) return;
    //ROS_WARN("[SwarmMasterElectionBids] masterHBCallback receive voteNO=%d, masterID=%d", aHeartBeat.voteNo(), aHeartBeat.masterID());
    //if voteNo matches
    if(receVoteNo==_masterVoteNo){        

        if(SwarmDataStorage::instance()->getLocalRobotState() == VOTING){
            ROS_WARN("Robot %d receive master HB msg with voteNo %d while vote processing, change master"\
            ,_robotID,receVoteNo);
            //write event to file
            writeEventtoFile("receive master HB while vote processing");
            SwarmDataStorage::instance()->setMasterID(receMasterID);
            writeMasterIDtoFile();
            SwarmDataStorage::instance()->setLocalRobotState(KEEPING);
            _voteCompletedCond.notify_one();

            //write voteNo and State when changes
            writeVoteNoStatetoFile();
            _voteTimerCount = 0;


            //write leaderID to file when _masterID changes
            _voteInfoMap[_masterVoteNo]._robotIDWithMaxBid = receMasterID;
            _voteInfoMap[_masterVoteNo]._state = VOTE_CONSENSUS;

            //cancel vote msg
            boost::unique_lock<boost::shared_mutex> msgDelayWriteLock(_voteMsgDelayMapMutex);
            for(int32_t msgIndex = 0;msgIndex<_voteMsgDelayMap[_masterVoteNo].size();msgIndex++){
                _voteMsgDelayMap[_masterVoteNo].at(msgIndex) = 0;
            }
        }else{
            if(receMasterID==SwarmDataStorage::instance()->getMasterID()){
                //if the masterID matches,do nothing for now
                receiveHBfromMatchedMaster(receMasterID);                
            }else{
                //HB msg from another master with the same voteNo
                receiveHBfromUnMatchedMaster(receMasterID);
            }
        }

    }else if(receVoteNo>_masterVoteNo){
        
        ROS_WARN("Larger voteNo %d msg received",receVoteNo);
        //write event detect master HB msg with larger voteNo
        std::string eventStr = "detect master with larger voteNo " + std::to_string(receMasterID);
        writeEventtoFile(eventStr);
        if(_largerVoteNoMsgCount.count(receVoteNo)==0){
            _largerVoteNoMsgCount[receVoteNo] = 1;
        }else{
            _largerVoteNoMsgCount[receVoteNo]++;
        }
        //if count>COUNT
        if(_largerVoteNoMsgCount[receVoteNo]>INCONSISTENT_MSG_COUNT){
            //change local master vote information
            _masterVoteNo = receVoteNo;
            SwarmDataStorage::instance()->setMasterID(receMasterID);
            SwarmDataStorage::instance()->setLocalRobotState(KEEPING);
            _voteCompletedCond.notify_one();

            //write event detect unmatched master HB msg
            std::string eventStr = "changer to master with larger voteNo " + std::to_string(receMasterID);
            writeEventtoFile(eventStr);
            //write leaderID to file when _masterID changes
            writeMasterIDtoFile();
            //write voteNo and State when changes
            writeVoteNoStatetoFile();

            _timerCount = 0;
            _voteTimerCount = 0;
            //update local voteInfo
            if(_voteInfoMap.count(_masterVoteNo)==0){
                VoteInfo voteInfo;
                voteInfo._voteNo = _masterVoteNo;
                voteInfo._robotID = _robotID;
                voteInfo._robotIDWithMaxBid = receMasterID;
                voteInfo._state = VOTE_CONSENSUS;
                _voteInfoMap[_masterVoteNo] = voteInfo;

            }else{
                _voteInfoMap[_masterVoteNo]._robotIDWithMaxBid = receMasterID;
                _voteInfoMap[_masterVoteNo]._state = VOTE_CONSENSUS;

            }
        }    
    }
}

void SwarmMasterElectionBids::sendMasterHBLoop(){
    while(true){
        sendMasterHB();
        usleep(_masterHBPeriod*1000);//1s
    }
}

void SwarmMasterElectionBids::sendMasterHB(){
    boost::recursive_mutex::scoped_lock lock(_recurMutex);
    //write data to file
    writeMasterIDtoFile();
    writeVoteNoStatetoFile();
    //while local robot is the swarm master and consensus achieved, broadcast heart beat msgs
    if((SwarmDataStorage::instance()->getLocalRobotState() == KEEPING)
        &&(_robotID == SwarmDataStorage::instance()->getMasterID())){          
        MasterHeartBeat hbMsg;
        hbMsg.voteNo(_masterVoteNo);
        hbMsg.masterID(_robotID);
        _pPubMasterHeartBeat->publish(hbMsg);
        //ROS_WARN("[SwarmMasterElectionBids] sendMasterHB:voteNo=%d, masterID=%d", _masterVoteNo, _robotID);
    }
}

void SwarmMasterElectionBids::masterHBCountLoop(){
    // while(true){
    //     masterHBCount();
    //     usleep(COUNT_DURATION*1000);//100ms
    // }

    boost::mutex mutex;
   
    while(true){
        if( SwarmDataStorage::instance()->getLocalRobotState() == VOTING){
            boost::unique_lock<boost::mutex> lock(mutex);
            ROS_WARN("waiting voting, timeout=%d", _masterVoteTimeOut);
            if(!_voteCompletedCond.timed_wait(lock, boost::get_system_time() + boost::posix_time::seconds(_masterVoteTimeOut))){
                // wait timeout, then choose robot with the max bid as the master
                ROS_WARN("Vote processing cannot achive consensus for a long time, voteNo: %d",_masterVoteNo);
                writeEventtoFile("vote donot converge " + std::to_string(_masterVoteNo));

                VoteInfo& voteInfo = _voteInfoMap[_masterVoteNo];
                SwarmDataStorage::instance()->setMasterID(voteInfo._robotIDWithMaxBid);
                writeMasterIDtoFile();
                SwarmDataStorage::instance()->setLocalRobotState(KEEPING);
                writeVoteNoStatetoFile();
                _voteCompletedCond.notify_one();
            } else{
                ROS_WARN("Vote processing achive consensus ");
            }
        }
        usleep(COUNT_DURATION*1000);//100ms
    }
}

void SwarmMasterElectionBids::masterHBCount(){
    boost::recursive_mutex::scoped_lock lock(_recurMutex);
    int masterID = SwarmDataStorage::instance()->getMasterID();
    //count hb msg when vote consensus
    if(SwarmDataStorage::instance()->getLocalRobotState() == KEEPING){
        //members receive master heart beat
        if(_robotID!=masterID){
            _timerCount++;//count for members receive unmatched master heart beat
        }

    }else{//count vote time when vote processing
        _voteTimerCount++;
        if(_voteTimerCount>_masterVoteTimeOut){
            ROS_ERROR("[Actor Vote] Vote processing cannot achive consensus for a long time, voteNo: %d",_masterVoteNo);
            //write event lose master
            std::string eventStr = "vote donot converge " + std::to_string(_masterVoteNo);
            writeEventtoFile(eventStr);
            //set itself to be master and increase voteNo
            SwarmDataStorage::instance()->setMasterID(_robotID);
            //write leaderID to file when _masterID changes
            writeMasterIDtoFile();
            _masterVoteNo++;
            SwarmDataStorage::instance()->setLocalRobotState(KEEPING);
            _voteCompletedCond.notify_one();

            //write voteNo and State when changes
            writeVoteNoStatetoFile();
            _timerCount = 0;
            _voteTimerCount = 0;
            
            //send master hb
            sendMasterHB();
            
        }
    }
}

//activate a new vote
//receive master vote msg from itself, write voteInfo and send master vote msg again
void SwarmMasterElectionBids::activateMasterVote(){
    boost::recursive_mutex::scoped_lock lock(_recurMutex);
    // get members who send master heartbeat
    std::set<int32_t> diffMasterRobotIDs;
    for (std::map<int32_t, MsgReceTimeInfo>::iterator it = _diffMasterMsgInfo.begin(); it != _diffMasterMsgInfo.end(); it++) {
        if(it->second._voteNo == _masterVoteNo)
            diffMasterRobotIDs.insert(it->first);
    }

    //write activate vote flag to file
    writeVoteActiOrRecetoFile(1);
    _masterVoteNo++;
    //change the vote state
    SwarmDataStorage::instance()->setLocalRobotState(VOTING);
    //write voteNo and State when changes
    writeVoteNoStatetoFile();

    //reset vote timer
    _voteTimerCount = 0;
    //set vote time out randomly
    srand(_robotID);

    int maxCommLatency = std::max(500000, (int)SwarmDataStorage::instance()->getMaxRobotHBLatency());
    int meanCommLatency = std::max(500000, (int)SwarmDataStorage::instance()->getMeanRobotHBLatency());
    _masterVoteTimeOut = ceil(2*maxCommLatency+(rand()%meanCommLatency));
    //_masterVoteTimeOut = ceil((2*maxCommLatency+(rand()%meanCommLatency))/COUNT_DURATION);//VOTE_TIME_OUT_COUNT/2 + (rand()%(VOTE_TIME_OUT_COUNT/2));
    //if(_masterVoteTimeOut==0) _masterVoteTimeOut = 1;
    //save local voteinfo
    VoteInfo voteInfo;
    voteInfo._voteNo = _masterVoteNo;
    voteInfo._robotID = _robotID;
    voteInfo._robotIDs.push_back(_robotID);
    voteInfo._bids.push_back(_localBid);
    
    //received platformID
    voteInfo._receivedRobotIDs.clear();
    voteInfo._receivedRobotIDs.insert(_robotID);

    //get maxBid etc
    voteInfo._maxBid = _localBid;
    voteInfo._robotIDWithMaxBid = _robotID;
    voteInfo._state = VOTE_PROCESSING;
    SwarmDataStorage::instance()->getRobotIDs(voteInfo._voteMembers);
    for(auto it = diffMasterRobotIDs.begin(); it != diffMasterRobotIDs.end(); it++){
        if(std::find(voteInfo._voteMembers.begin(), voteInfo._voteMembers.end(), *it) == voteInfo._voteMembers.end())
            voteInfo._voteMembers.push_back(*it);
    }

    //prepare vote master msg
    MasterElectionBid voteMsg;
    voteMsg.voteNo(_masterVoteNo);
    voteMsg.robotID(_robotID);
    voteMsg.state(VOTE_PROCESSING);
    voteMsg.robots().push_back(_robotID);
    voteMsg.bids().push_back(_localBid);
    voteMsg.members().assign(voteInfo._voteMembers.begin(), voteInfo._voteMembers.end());

    //write swarm size to file
    writeSwarmSizetoFile(voteInfo._voteMembers.size());
    
    //save the vote info
    _voteInfoMap[voteInfo._voteNo] = voteInfo;//TODO remove old votes

    //pub the vote msg
    _pPubMasterElection->publish(voteMsg);    

    ROS_WARN("[SwarmMasterElectionBids] activateMasterVote, voteNO=%d, membersCount=%lu", _masterVoteNo, voteInfo._voteMembers.size());
}
