/* ==================================================================
*
* Copyright (c) 2016-2019, micROS Group, NIIDT, TAIIC, HPCL.
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions 
* are met: 
*
* 1. Redistributions of source code must retain the above copyright 
*     notice, this list of conditions and the following disclaimer. 
* 2. Redistributions in binary form must reproduce the above copyright 
*     notice, this list of conditions and the following disclaimer in the 
*     documentation and/or other materials provided with the 
*     distribution. 
* 3. All advertising materials mentioning features or use of this software 
*     must display the following acknowledgement: 
*     This product includes software developed by the micROS Group. and 
*     its contributors. 
* 4. Neither the name of the Group nor the names of its contributors may 
*     be used to endorse or promote products derived from this software 
*     without specific prior written permission. 
*
* THIS SOFTWARE IS PROVIDED BY MICROS,GROUP AND CONTRIBUTORS
* ''AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT
* NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
* FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL 
* THE MICROS,GROUP OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS 
* OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) 
* HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
* OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN 
* IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
* ===================================================================
*
* Author: Bin Di, Chunlian Fu, Junling Gao, Ruihao Li, Wei Yi.
*         (in alphabetical order)
*
*/

#include "swarm_data_storage/swarm_data_storage.h"
#include <iostream>
SwarmDataStorage* SwarmDataStorage::_instance = new SwarmDataStorage();
SwarmDataStorage* SwarmDataStorage::instance(){
    return _instance;
}

SwarmDataStorage::SwarmDataStorage()
:_localRobotID(INVALID_ROBOTID),_localMasterID(INVALID_ROBOTID),_localMasterGenerateType(AUTO),_pubWarnMsg(NULL){}

SwarmDataStorage::~SwarmDataStorage(){
    for(ROBOTINFOMAP::iterator it = _robotID2Info.begin(); it != _robotID2Info.end(); it++){
        it->second.reset();
    }
    _robotID2Info.clear();
    for(SWARMACTORGROUPMAP::iterator it = _swarmName2InfoFromTask.begin(); it != _swarmName2InfoFromTask.end(); it++){
        for(ACTORGROUPMAP::iterator it2 = it->second.begin(); it2 != it->second.end(); it2++)
            it2->second.reset();
    }
    _swarmName2InfoFromTask.clear();
    if(_fs){
        _fs.close();
    }
    if(_pubWarnMsg){
        delete _pubWarnMsg;
        _pubWarnMsg = NULL;
    }
}

void SwarmDataStorage::init(const int aRobotID, std::string aLogFilePath){
    if(_localRobotID == INVALID_ROBOTID){
        boost::unique_lock<boost::shared_mutex> wlock(_localRobotIDMutex);
        _localRobotID = aRobotID;
        addRobotToLocalSwarm(aRobotID);
    }

    if(_localMasterID == INVALID_ROBOTID){
        boost::unique_lock<boost::shared_mutex> wlock(_localMasterIDMutex);
        _localMasterID = aRobotID;
    }

    #ifdef ACTOR_SWARM_LOG_OUT
    if(aLogFilePath.empty()){
        return;
    }
    //open data file
    if(!_fs){
        std::cout<<"WARNING: Test data file cannot be constructed"<<std::endl;
    }else{
        if(_fs.is_open()){
            _fs.close();
        }
        //get current dir
        std::string curDir = aLogFilePath;
  
        //get current time
        timeval timeV;
	    gettimeofday(&timeV,NULL);
	    time_t timeT = timeV.tv_sec;
	    tm *pTm = localtime(&timeT);
	    char szTime[16];
	    sprintf(szTime,"%04d%02d%02d:%02d%02d%02d",pTm->tm_year+1900,pTm->tm_mon+1, pTm->tm_mday, pTm->tm_hour, pTm->tm_min, pTm->tm_sec);
        std::string curTimeStr(szTime);
        
        //get robotID
        curDir += "actor_log@ID";
        std::string fileNameStr = curDir + std::to_string(aRobotID);
        fileNameStr += "-";
        fileNameStr += curTimeStr;
        fileNameStr += ".log";
        std::cout<<"TEST: file name: "<<fileNameStr<<std::endl;
        _fs.open(fileNameStr.c_str(),std::fstream::out | std::fstream::app);
        _fs<<"sec"<<" "<<"usec"<<" "<<"item"<<" "<<"value"<<std::endl;
        _fs<<timeV.tv_sec<<" "<<timeV.tv_usec<<" "<<"platformID"<<" "<<aRobotID<<std::endl;
    }

    _pubWarnMsg = NULL;
    //_pubWarnMsg = new RTPSPublisher<StringMsg>("/swarm_warn_msg");
    #endif
}

int SwarmDataStorage::getLocalRobotID() {
    boost::shared_lock<boost::shared_mutex> rlock(_localRobotIDMutex);
    return _localRobotID;
}

void SwarmDataStorage::setLocalRobotState(const ESwarmRobotState aState){
    boost::unique_lock<boost::shared_mutex> wlock(_robotID2InfoMutex);
    if( _robotID2Info[_localRobotID]->_state == aState) return;

#ifdef DEBUG_V3
    _robotID2Info[_localRobotID]->_state = aState;
#else 
    _robotID2Info[_localRobotID]->_state = KEEPING;
#endif
    if(aState == KEEPING){
        _localStateCondition.notify_all();
    }

    wlock.unlock();

    #ifdef ACTOR_SWARM_LOG_OUT
    logOut2File("masterState",std::to_string(aState));
    std::string eventStr = "set_master_state_" + std::to_string(aState);
    logOut2File("event",eventStr);
    #endif
}

ESwarmRobotState SwarmDataStorage::getLocalRobotState(){
    boost::shared_lock<boost::shared_mutex> rlock(_robotID2InfoMutex);
    return _robotID2Info[_localRobotID]->_state;
}

ESwarmRobotState SwarmDataStorage::getRobotStateByID(const int aRobotID){
    boost::shared_lock<boost::shared_mutex> rlock(_robotID2InfoMutex);
    if(_robotID2Info.count(aRobotID) > 0)
        return _robotID2Info[aRobotID]->_state;
    return LEAVE;
}

bool SwarmDataStorage::waitForLocalKeepingState(const int aTimeout){
    boost::unique_lock<boost::shared_mutex> rlock(_robotID2InfoMutex);
    return _localStateCondition.timed_wait(rlock, boost::get_system_time() + boost::posix_time::seconds(aTimeout));
}


void SwarmDataStorage::setMasterID(const int aMasterID, const EMasterGenerateType aGenerateType){
    boost::unique_lock<boost::shared_mutex> wlock(_localMasterIDMutex);
    if(_localMasterID == aMasterID && _localMasterGenerateType == aGenerateType)return;

    _localMasterID = aMasterID;
    _localMasterGenerateType = aGenerateType;
    wlock.unlock();

    #ifdef ACTOR_SWARM_LOG_OUT
    logOut2File("masterID",std::to_string(aMasterID));
    std::string eventStr = "set_masterID_" + std::to_string(aMasterID) + ",set by " + std::to_string(aGenerateType);
    logOut2File("event",eventStr);
    #endif
}

int SwarmDataStorage::getMasterID(){
    boost::shared_lock<boost::shared_mutex> rlock(_localMasterIDMutex);
    return _localMasterID;
}

EMasterGenerateType SwarmDataStorage::getMasterGenerateType(){
    boost::shared_lock<boost::shared_mutex> rlock(_localMasterIDMutex);
    return _localMasterGenerateType;
}

// get all robotIDs not leave in swarm
void SwarmDataStorage::getRobotIDs(std::vector<int>& vRobotIDs){
    vRobotIDs.clear();
    boost::shared_lock<boost::shared_mutex> rlock(_robotID2InfoMutex);
    for(ROBOTINFOMAP::iterator it = _robotID2Info.begin(); it != _robotID2Info.end(); it++)
        if(it->second->_state != LEAVE)
            vRobotIDs.push_back(it->first);
}

// get robot count not leave in swarm
int SwarmDataStorage::getRobotCount(){
    int count = 0;
    boost::shared_lock<boost::shared_mutex> rlock(_robotID2InfoMutex);
    for(ROBOTINFOMAP::iterator it = _robotID2Info.begin(); it != _robotID2Info.end(); it++)
        if(it->second->_state != LEAVE)
            count++;
    return count;
}

// add a robot to local swarm
void SwarmDataStorage::addRobotToLocalSwarm(const int aRobotID){
    boost::unique_lock<boost::shared_mutex> wlock(_robotID2InfoMutex);
    if(_robotID2Info.count(aRobotID) == 0){
        _robotID2Info[aRobotID].reset(new RobotInfoDyn());
    }
    _robotID2Info[aRobotID]->_state = KEEPING;
    setRobotHeartbeatValue(aRobotID, 1);
    wlock.unlock();

    #ifdef ACTOR_SWARM_LOG_OUT
    logOut2File("swarmSize",std::to_string(getRobotCount()));
    std::string eventStr = "add_robot_" + std::to_string(aRobotID);
    logOut2File("event",eventStr);
    #endif
}

// remove a robot from local swarm
void SwarmDataStorage::removeRobotFromLocalSwarm(const int aRobotID){
    if(_robotID2Info.count(aRobotID) > 0){
        boost::unique_lock<boost::shared_mutex> wlock(_robotID2InfoMutex);
        _robotID2Info[aRobotID]->_state = LEAVE;
        _robotID2Info[aRobotID]->_iFormationPos = INVALID_ROBOTPOS;
        // _robotID2Info[aRobotID].reset();
        // _robotID2Info.erase(aRobotID);
    }
    setRobotHeartbeatValue(aRobotID, 0);

    #ifdef ACTOR_SWARM_LOG_OUT
    logOut2File("swarmSize",std::to_string(getRobotCount()));
    std::string eventStr = "remove_robot_" + std::to_string(aRobotID);
    logOut2File("event",eventStr);
    #endif
}

// return swarmName
std::string SwarmDataStorage::getSwarmNameByID(const int aRobotID){
    boost::shared_lock<boost::shared_mutex> rlock(_robotID2InfoMutex);
    if(_robotID2Info.count(aRobotID) > 0){
        return _robotID2Info[aRobotID]->_sSwarmName; 
    }
    return "";
}

// get actors running on robot
void SwarmDataStorage::getRobotActorsByID(const int aRobotID, std::string& aSwarmName, std::vector<std::string>& aSwarmActors){
    boost::shared_lock<boost::shared_mutex> rlock(_robotID2InfoMutex);
    if(_robotID2Info.count(aRobotID) > 0){
        aSwarmActors.clear();
        aSwarmName = _robotID2Info[aRobotID]->_sSwarmName;
        aSwarmActors.assign(_robotID2Info[aRobotID]->_vSwarmActors.begin(),  _robotID2Info[aRobotID]->_vSwarmActors.end());
    }
}
    
bool SwarmDataStorage::isActorRunningOnRobot(const int aRobotID, const std::string& anActorName){
    boost::shared_lock<boost::shared_mutex> rlock(_robotID2InfoMutex);
    if(_robotID2Info.count(aRobotID) == 0 || _robotID2Info[aRobotID]->_state == LEAVE || _robotID2Info[aRobotID]->_vSwarmActors.size() == 0){
        return false;
    }

    return std::find(_robotID2Info[aRobotID]->_vSwarmActors.begin(), _robotID2Info[aRobotID]->_vSwarmActors.end(), anActorName) != _robotID2Info[aRobotID]->_vSwarmActors.end();
}

bool SwarmDataStorage::getRobotsWithActorRunning(const std::string& anActorName, std::vector<int> aRobots){
    aRobots.clear();

    boost::shared_lock<boost::shared_mutex> rlock(_robotID2InfoMutex);
    for(auto it = _robotID2Info.begin(); it != _robotID2Info.end(); it++){
        if(it->second->_state != LEAVE
        && std::find(it->second->_vSwarmActors.begin(), it->second->_vSwarmActors.end(), anActorName) != it->second->_vSwarmActors.end())
            aRobots.push_back(it->first);
    }
    return aRobots.size() > 0;
}

// set actors running on robot
void SwarmDataStorage::setRobotActors(const int aRobotID, const std::string aSwarmName, const std::vector<std::string>& aSwarmActors){
    boost::unique_lock<boost::shared_mutex> wlock(_robotID2InfoMutex);
    if(_robotID2Info.count(aRobotID) == 0)
        return;

    _robotID2Info[aRobotID]->_sSwarmName = aSwarmName;
    _robotID2Info[aRobotID]->_vSwarmActors.clear();
    _robotID2Info[aRobotID]->_vSwarmActors.assign(aSwarmActors.begin(), aSwarmActors.end());

    #ifdef ACTOR_SWARM_LOG_OUT
    std::string logStr = std::to_string(aRobotID) + "-->";
    for(std::vector<std::string>::const_iterator it = aSwarmActors.begin(); it != aSwarmActors.end(); it++){
        logStr.append(*it).append(",");
    }
 
    if((!aSwarmName.empty())&&(!aSwarmActors.empty()))
    logOut2File("setRobotActors",logStr);
    #endif
}

int SwarmDataStorage::getRobotPosByID(const int aRobotID){
    boost::shared_lock<boost::shared_mutex> rlock(_robotID2InfoMutex);
    if(_robotID2Info.count(aRobotID) > 0 && _robotID2Info[aRobotID]->_state != LEAVE){
        return _robotID2Info[aRobotID]->_iFormationPos;
    }
    return INVALID_ROBOTPOS;
}

//get robot pos info by ID
bool SwarmDataStorage::getRobotPosByID(const int aRobotID,std::string &aSwarmName,std::string &aActorName,std::string &aFormationType,int &aFormationPos){
    boost::shared_lock<boost::shared_mutex> rlock(_robotID2InfoMutex);
    if(_robotID2Info.count(aRobotID) > 0 && _robotID2Info[aRobotID]->_state != LEAVE){
        aSwarmName = _robotID2Info[aRobotID]->_sSwarmName;
        aActorName = _robotID2Info[aRobotID]->_sActorName;
        aFormationType = _robotID2Info[aRobotID]->_sFormationType;
        aFormationPos = _robotID2Info[aRobotID]->_iFormationPos;
        return true;
    }
    return false;    
}

int SwarmDataStorage::getRobotIDByPos(const int aRobotPos){
    boost::shared_lock<boost::shared_mutex> rlock(_robotID2InfoMutex);
    ROBOTINFOMAP::iterator it = std::find_if(_robotID2Info.begin(), _robotID2Info.end(), MapValueFinder(aRobotPos));
    if(it != _robotID2Info.end() && it->second->_state != LEAVE)
        return it->first;
    return INVALID_ROBOTID;
}
// aMapRobotPos:pos->robotID
void SwarmDataStorage::getAllRobotPos(std::map<int, int>& aMapRobotPos){
    aMapRobotPos.clear();
    boost::shared_lock<boost::shared_mutex> rlock(_robotID2InfoMutex);
    for(ROBOTINFOMAP::iterator it = _robotID2Info.begin(); it != _robotID2Info.end(); it++)
        if(it->second->_state != LEAVE)
            aMapRobotPos[it->second->_iFormationPos] = it->first;
}

void SwarmDataStorage::setRobotPos(const int aRobotID, const int aRobotPos){
    boost::unique_lock<boost::shared_mutex> wlock(_robotID2InfoMutex);
    if(_robotID2Info.count(aRobotID) == 0){
        _robotID2Info[aRobotID].reset(new RobotInfoDyn()); 
    }
    //_robotID2Info[aRobotID]->_sFormationType = formationType;
    _robotID2Info[aRobotID]->_iFormationPos = aRobotPos;
    _robotID2Info[aRobotID]->_state = KEEPING;
    wlock.unlock();

    #ifdef ACTOR_SWARM_LOG_OUT
    std::string logStr = std::to_string(aRobotID) + "-->";
    logStr += std::to_string(aRobotPos);
    logOut2File("resetFormationPos",logStr);
    #endif
}

void SwarmDataStorage::setRobotPos(const int aRobotID, const int aRobotPos, const std::string aSwarmName, const std::string aActorName, const std::string aFormationType){
    boost::unique_lock<boost::shared_mutex> wlock(_robotID2InfoMutex);
    if(_robotID2Info.count(aRobotID) == 0){
        _robotID2Info[aRobotID].reset(new RobotInfoDyn()); 
    }
    _robotID2Info[aRobotID]->_sSwarmName = aSwarmName;
    _robotID2Info[aRobotID]->_sActorName = aActorName;
    _robotID2Info[aRobotID]->_sFormationType = aFormationType;
    _robotID2Info[aRobotID]->_iFormationPos = aRobotPos;
    _robotID2Info[aRobotID]->_state = KEEPING;
    wlock.unlock();

    #ifdef ACTOR_SWARM_LOG_OUT
    std::string logStr = std::to_string(aRobotID) + "-->";
    logStr += std::to_string(aRobotPos);
    logOut2File("resetFormationPos",logStr);
    #endif
}

// aMapRobotPos:pos->robotID
void SwarmDataStorage::resetRobotPos(std::map<int,int>& aMapRobotPos){
    boost::unique_lock<boost::shared_mutex> wlock(_robotID2InfoMutex);
    if(aMapRobotPos.empty()) {
        for(ROBOTINFOMAP::iterator it = _robotID2Info.begin(); it != _robotID2Info.end(); it++) {
            it->second->_iFormationPos = INVALID_ROBOTPOS;
        }
        #ifdef ACTOR_SWARM_LOG_OUT
            logOut2File("resetFormationPos", "reset all to -1");
        #endif
    } else {
        for(std::map<int,int>::iterator it = aMapRobotPos.begin(); it != aMapRobotPos.end(); it++) {
            _robotID2Info[it->second]->_iFormationPos = it->first;

            #ifdef ACTOR_SWARM_LOG_OUT
            std::string logStr = std::to_string(it->second) + "-->";
            logStr += std::to_string(it->first);
            logOut2File("resetFormationPos",logStr);
            #endif
        }
    }
}

// heartbeat value
float SwarmDataStorage::getRobotHeartbeatValueByID(const int aRobotID){
    boost::shared_lock<boost::shared_mutex> rlock(_robotID2HeartbeatValueMutex);
    if(_robotID2HeartbeatValue.count(aRobotID) > 0)
        return _robotID2HeartbeatValue[aRobotID];
    return 0.0F;    
}

void SwarmDataStorage::setRobotHeartbeatValue(const int aRobotID, float aValue){
    boost::unique_lock<boost::shared_mutex> wlock(_robotID2HeartbeatValueMutex);
    _robotID2HeartbeatValue[aRobotID] = aValue;
}

// heartbeat latency statistic
const boost::shared_ptr<RobotHBLatencyStatistic> SwarmDataStorage::getRobotHBLatencyStatistic(const int aRobotID){
    if(getRobotStateByID(aRobotID) != LEAVE){
        boost::shared_lock<boost::shared_mutex> rlock(_robotID2HBLatencyStatisticMutex);
        if(_robotID2HBLatencyStatistic.count(aRobotID) != 0)
            return _robotID2HBLatencyStatistic[aRobotID];
        return boost::shared_ptr<RobotHBLatencyStatistic>();
    }
    return boost::shared_ptr<RobotHBLatencyStatistic>();
}

void SwarmDataStorage::setRobotHBLatencyStatistic(const int aRobotID, const double aMean, const double aStddev, const double aQuantile){
    if(getRobotStateByID(aRobotID) == LEAVE)return;

    boost::unique_lock<boost::shared_mutex> wlock(_robotID2HBLatencyStatisticMutex);
    if(_robotID2HBLatencyStatistic.count(aRobotID) == 0)
        _robotID2HBLatencyStatistic[aRobotID] = boost::shared_ptr<RobotHBLatencyStatistic>(new RobotHBLatencyStatistic);
    _robotID2HBLatencyStatistic[aRobotID]->_mean = aMean;
    _robotID2HBLatencyStatistic[aRobotID]->_stddev = aStddev;
    _robotID2HBLatencyStatistic[aRobotID]->_quantile = aQuantile;
}

double SwarmDataStorage::getRobotHBLatencyStatisticConfidence(const int aRobotID){
    boost::shared_lock<boost::shared_mutex> rlock(_robotID2HBLatencyStatisticMutex);
    if(_robotID2HBLatencyStatistic.count(aRobotID) != 0)
        return _robotID2HBLatencyStatistic[aRobotID]->_confidence;
    return 0.0f;  
}

void SwarmDataStorage::setRobotHBLatencyStatisticConfidence(const int aRobotID, const double aConfidence){
    boost::unique_lock<boost::shared_mutex> wlock(_robotID2HBLatencyStatisticMutex);
    if(_robotID2HBLatencyStatistic.count(aRobotID) == 0)
        _robotID2HBLatencyStatistic[aRobotID] = boost::shared_ptr<RobotHBLatencyStatistic>(new RobotHBLatencyStatistic);
    _robotID2HBLatencyStatistic[aRobotID]->_confidence = aConfidence;
}

double SwarmDataStorage::getMaxRobotHBLatency(const int aRobotID){
    boost::shared_lock<boost::shared_mutex> rlock(_robotID2HBLatencyStatisticMutex);
    if(aRobotID != INVALID_ROBOTID && _robotID2HBLatencyStatistic.count(aRobotID) > 0){
        return _robotID2HBLatencyStatistic[aRobotID]->_quantile;
    }
    
    double quantile = 0.0f;
    for(auto it = _robotID2HBLatencyStatistic.begin(); it != _robotID2HBLatencyStatistic.end(); it++){
        quantile = std::max(it->second->_quantile, quantile);
    }
    return quantile;
}

double SwarmDataStorage::getMeanRobotHBLatency(const int aRobotID){
    boost::shared_lock<boost::shared_mutex> rlock(_robotID2HBLatencyStatisticMutex);
    if(aRobotID != INVALID_ROBOTID && _robotID2HBLatencyStatistic.count(aRobotID) > 0){
        return _robotID2HBLatencyStatistic[aRobotID]->_mean;
    }

    if(_robotID2HBLatencyStatistic.size() > 0){
        double sum = 0.0f;
        for(auto it = _robotID2HBLatencyStatistic.begin(); it != _robotID2HBLatencyStatistic.end(); it++){
            sum += it->second->_mean;
        }
        return sum/_robotID2HBLatencyStatistic.size();
    }
    return 0.0f;
}

// when bind swarm task, set by this method
void SwarmDataStorage::setSwarmTaskActorInfo(const std::string aSwarmName, const std::string aActorName, const ESwarmActorType aType, const int aNum, const int aPriority){
    boost::unique_lock<boost::shared_mutex> wlock(_swarmName2InfoFromTaskMutex);
    if(_swarmName2InfoFromTask.count(aSwarmName) ==0 ||_swarmName2InfoFromTask[aSwarmName].count(aActorName) == 0){
        _swarmName2InfoFromTask[aSwarmName][aActorName].reset(new ActorGroupInfo());
    }
    _swarmName2InfoFromTask[aSwarmName][aActorName]->_sActorName = aActorName;
    _swarmName2InfoFromTask[aSwarmName][aActorName]->_type = aType;
    _swarmName2InfoFromTask[aSwarmName][aActorName]->_iActorMaxCount = aNum;
    _swarmName2InfoFromTask[aSwarmName][aActorName]->_iActorPriority = aPriority;
}

bool SwarmDataStorage::isActorExist(const std::string aSwarmName, const std::string aActorName){
    boost::shared_lock<boost::shared_mutex> rlock(_swarmName2InfoFromTaskMutex);
    if(_swarmName2InfoFromTask.count(aSwarmName) == 0 || _swarmName2InfoFromTask[aSwarmName].count(aActorName) == 0)
        return false;
    return true;
}

int SwarmDataStorage::getSwarmActorCount(const std::string aSwarmName, const std::string aActorName){
    int runActorCount = 0;
    boost::shared_lock<boost::shared_mutex> rlock(_robotID2InfoMutex);
    for(ROBOTINFOMAP::iterator it = _robotID2Info.begin(); it != _robotID2Info.end(); it++) {
        if( it->second->_state != LEAVE
        && it->second->_sSwarmName.compare(aSwarmName) == 0
        && std::find(it->second->_vSwarmActors.begin(), it->second->_vSwarmActors.end(), aActorName) != it->second->_vSwarmActors.end()){
            runActorCount++;
        }
    }
    return runActorCount;
}

int SwarmDataStorage::getSwarmActorMaxCount(const std::string aSwarmName, const std::string aActorName){
    boost::shared_lock<boost::shared_mutex> rlock(_swarmName2InfoFromTaskMutex);
    if(_swarmName2InfoFromTask.count(aSwarmName) > 0 && _swarmName2InfoFromTask[aSwarmName].count(aActorName) > 0)
        return _swarmName2InfoFromTask[aSwarmName][aActorName]->_iActorMaxCount;
    return 0;
}

void SwarmDataStorage::updateInitialRobotIDs(std::set<std::string>& aRobotList){
    boost::unique_lock<boost::shared_mutex> wlock(_robotIDsFromTaskMutex);
    for(auto it = aRobotList.begin(); it != aRobotList.end(); it++){
        _robotIDsFromTask.insert(atoi(it->c_str()));
    }
}

std::set<int> SwarmDataStorage::getInitialRobotIDs(){
    boost::shared_lock<boost::shared_mutex> rlock(_robotIDsFromTaskMutex);
    return _robotIDsFromTask;
}

int SwarmDataStorage::getInitialRobotIDsSize(){
    boost::shared_lock<boost::shared_mutex> rlock(_robotIDsFromTaskMutex);
    return _robotIDsFromTask.size();
}

void SwarmDataStorage::printInformation(){
    boost::shared_lock<boost::shared_mutex> rlock(_robotID2InfoMutex);
    std::cout<<"====ROBOT ["<<_localRobotID<<"]===="<<std::endl;
    std::cout<<"SwarmMasterID:"<<_localMasterID<<std::endl;
    std::cout<<"SwarmRobots:"<<std::endl;
    for(ROBOTINFOMAP::iterator it = _robotID2Info.begin(); it != _robotID2Info.end(); it++){
        std::cout<<"RobotID:"<<it->first
        <<"\t\tState:"<<it->second->_state
        <<"\t\tSwarmName:"<<it->second->_sSwarmName
        <<"\t\tFormationType:"<<it->second->_sFormationType
        <<"\t\tFormationPos:"<<it->second->_iFormationPos<<std::endl;
    }
    std::cout<<"========"<<std::endl;
}

//write masterID/swarmsize/votestate/events to log file
//masterID-->setMasterID
//swarmsize-->add/removeRobotToLocalSwarm
//votestate-->setLocalRobotState
//formation pos-->resetRobotPos
void SwarmDataStorage::logOut2File(std::string aItem, std::string aData){
    boost::unique_lock<boost::mutex> lock(_fsMutex);
    if(!_fs) return;
    if(!_fs.is_open()) return;
    timeval timeV;
	gettimeofday(&timeV,NULL);
    _fs<<timeV.tv_sec<<" "<<timeV.tv_usec<<" "<<aItem<<" "<<aData<<std::endl;

    // pub warn msg,lock with _fsMutex
    // if(aItem.compare("warn") == 0){
    //     StringMsg msg;
    //     msg.sendID(_localRobotID);
    //     msg.destID(-1);
    //     msg.data(aData);
    //     _pubWarnMsg->publish(msg);

    //     std::string logStr = std::to_string(StringMsg::getCdrSerializedSize(msg)) + ",Warn";
    //     _fs<<timeV.tv_sec<<" "<<timeV.tv_usec<<" netsend "<<logStr<<std::endl;
    // }
}

//write member heart beat to log file
//called by heartbeat
void SwarmDataStorage::logOutHB2File(int32_t aMemberID,int32_t aData){
    boost::unique_lock<boost::mutex> lock(_fsMutex);
    if(!_fs) return;
    if(!_fs.is_open()) return;
    timeval timeV;
	gettimeofday(&timeV,NULL);
    std::string memberID = "IDHB_"+std::to_string(aMemberID);
    _fs<<timeV.tv_sec<<" "<<timeV.tv_usec<<" "<<memberID<<" "<<aData<<std::endl;
}
