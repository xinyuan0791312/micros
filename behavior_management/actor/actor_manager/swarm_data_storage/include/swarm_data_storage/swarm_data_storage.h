/* ==================================================================
*
* Copyright (c) 2016-2019, micROS Group, NIIDT, TAIIC, HPCL.
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions 
* are met: 
*
* 1. Redistributions of source code must retain the above copyright 
*     notice, this list of conditions and the following disclaimer. 
* 2. Redistributions in binary form must reproduce the above copyright 
*     notice, this list of conditions and the following disclaimer in the 
*     documentation and/or other materials provided with the 
*     distribution. 
* 3. All advertising materials mentioning features or use of this software 
*     must display the following acknowledgement: 
*     This product includes software developed by the micROS Group. and 
*     its contributors. 
* 4. Neither the name of the Group nor the names of its contributors may 
*     be used to endorse or promote products derived from this software 
*     without specific prior written permission. 
*
* THIS SOFTWARE IS PROVIDED BY MICROS,GROUP AND CONTRIBUTORS
* ''AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT
* NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
* FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL 
* THE MICROS,GROUP OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS 
* OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) 
* HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
* OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN 
* IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
* ===================================================================
*
* Author: Bin Di, Chunlian Fu, Junling Gao, Ruihao Li, Wei Yi.
*         (in alphabetical order)
*
*/

#ifndef __SWARM_INFO_H__
#define __SWARM_INFO_H__
#include <boost/thread/shared_mutex.hpp>
#include <string>
#include <vector>
#include <map>
#include <fstream>
#include "RTPSPublisher.h"
#include "StringMsg.h"

#define ACTOR_SWARM_LOG_OUT
#define INVALID_ROBOTID -1
#define INVALID_ROBOTPOS -1

// enums generate master
enum EMasterGenerateType{
    AUTO,       // auto elect 
    SEMIAUTO,   // manual set temporary
    FORCE       // manual set forever  
};

// Enums Rotot State
enum ESwarmRobotState{
    KEEPING,    // robot is liveliness 
    LEAVE,      // robot has left the swarm
    VOTING     // robot is waiting for master election(used for local robot)
};

struct RobotInfoDyn{
    ESwarmRobotState _state;
    std::string _sSwarmName;
    std::string _sActorName;
    std::string _sFormationType;
    int _iFormationPos;
    std::vector<std::string> _vSwarmActors;
    RobotInfoDyn():_state(KEEPING), _sSwarmName(""), _sActorName(""), _sFormationType(""), _iFormationPos(INVALID_ROBOTPOS){}
};

struct RobotHBLatencyStatistic{
    double _mean;            // mean value, us
    double _stddev;          // standard deviation
    double _confidence;      // confidence intervals of the standard deviation for normal distribution
    double _quantile;        // quantile for normal distribution computed by _confidence
    RobotHBLatencyStatistic():_mean(0.0f), _stddev(0.0f), _confidence(0.0f), _quantile(0.0f){}
};

// compare with task.xml
enum ESwarmActorType{
    GENERAL,
    EXCLUDE,
    DYNAMIC
};

struct ActorGroupInfo{
    ESwarmActorType _type;
    std::string _sActorName;
    int _iActorMaxCount;
    int _iActorPriority; 
    ActorGroupInfo():_type(GENERAL), _sActorName(""), _iActorMaxCount(0), _iActorPriority(0){}
};

// Class SwarmDataStorage, this is a singleton class which is used for save robots information in the same swarm,
// and provides interfaces to get these information.
class SwarmDataStorage{
public:
    ~SwarmDataStorage();
    static SwarmDataStorage* instance();

     // this method should be called once the application start up
     // @param aRobotID  the local robot id
     // @param logFilePath this file will be used to log robot operations,you can set ACTOR_SWARM_LOG_OUT to disable it
     // @note  this method will add local robot to the swarm and set self as the master,this will be useful when the robot start up alone.
    void init(const int aRobotID, std::string aLogFilePath="");

    int getLocalRobotID();

    // robotstate
    void setLocalRobotState(const ESwarmRobotState aState);
    ESwarmRobotState getLocalRobotState();
    ESwarmRobotState getRobotStateByID(const int aRobotID);
    bool waitForLocalKeepingState(const int aTimeout);

    // masterid
    // the master in the swarm is used to control all the robots. 
    void setMasterID(const int aMasterID, const EMasterGenerateType aGenerateType = AUTO);
    int getMasterID();
    EMasterGenerateType getMasterGenerateType();

    // get all liveliness robots id in the swarm
    void getRobotIDs(std::vector<int>& vRobotIDs);

    // get liveliness robots count in the swarm 
    int getRobotCount();

    // this method will be called when a new robot join in the swarm
    // @note when robot joinin, it's state will be set to KEEPING
    void addRobotToLocalSwarm(const int aRobotID);

    // this method will be called when a robot leave the swarm
    // @note when robot left, it's state will be set to LEAVE
    void removeRobotFromLocalSwarm(const int aRobotID);

    // running actors
    // @return if not exists, return empty string
    std::string getSwarmNameByID(const int aRobotID);
    void getRobotActorsByID(const int aRobotID, std::string& aSwarmName, std::vector<std::string>& aSwarmActors);

    // judge if actor with anActorName running on aRobotID 
    bool isActorRunningOnRobot(const int aRobotID, const std::string& anActorName);
    bool getRobotsWithActorRunning(const std::string& anActorName, std::vector<int> aRobots);

    // this method will be called when actors running on the robot is changed, or deceted other robot's actors changed.
    void setRobotActors(const int aRobotID, const std::string aSwarmName, const std::vector<std::string>& aSwarmActors);

    // formation
    // @return if not exists, then return INVALID_ROBOTPOS
    int getRobotPosByID(const int aRobotID);
    bool getRobotPosByID(const int aRobotID,std::string &aSwarmName,std::string &aActorName,std::string &aFormationType,int &aFormationPos);
    
    // @return if not exists, then return INVALID_ROBOTID
    int getRobotIDByPos(const int aRobotPos);

    // get all liveliness robot pos with vector 
    // @param aMapRobotPos pos->robotID
    void getAllRobotPos(std::map<int, int>& aMapRobotPos);
    void setRobotPos(const int aRobotID, const int aRobotPos);
    void setRobotPos(const int aRobotID, const int aRobotPos, const std::string aSwarmName, const std::string aActorName, const std::string aFormationType);
    
    // this method will be called when the swarm formation reassiged
    // @param aMapRobotPos pos->robotID
    void resetRobotPos(std::map<int,int>& aMapRobotPos);

    // heartbeat value, 0~1, means the weight of liveliness
    float getRobotHeartbeatValueByID(const int aRobotID);
    void setRobotHeartbeatValue(const int aRobotID, float aValue);

    // heartbeat latency statistic, us
    const boost::shared_ptr<RobotHBLatencyStatistic> getRobotHBLatencyStatistic(const int aRobotID);
    void setRobotHBLatencyStatistic(const int aRobotID, const double aMean, const double aStddev, const double aQuantile);
    double getRobotHBLatencyStatisticConfidence(const int aRobotID);
    void setRobotHBLatencyStatisticConfidence(const int aRobotID, const double aConfidence);
    double getMaxRobotHBLatency(const int aRobotID = INVALID_ROBOTID);
    double getMeanRobotHBLatency(const int aRobotID = INVALID_ROBOTID);

    // task actors,set when task bind
    void setSwarmTaskActorInfo(const std::string aSwarmName, const std::string aActorName, const ESwarmActorType aType, const int aNum, const int aPriority);

    // check if has robot running the actor
    bool isActorExist(const std::string aSwarmName, const std::string aActorName);

    // get the robot count, which is running the actor
    int getSwarmActorCount(const std::string aSwarmName, const std::string aActorName);

    // get the max actor count allowed by the swarm task.
    int getSwarmActorMaxCount(const std::string aSwarmName, const std::string aActorName);
        
    // initial robot list, set when task bind
    void updateInitialRobotIDs(std::set<std::string>& aRobotList);
    std::set<int> getInitialRobotIDs();
    int getInitialRobotIDsSize();

    void printInformation();

    //write masterID/swarmsize/votestate/events to log file
    void logOut2File(std::string aItem, std::string aData);
    //write member heart beat to log file
    void logOutHB2File(int32_t aMemberID,int32_t aData);

private:
    SwarmDataStorage();
 
private:
    class MapValueFinder{
    public:
        MapValueFinder(const int &val):_val(val){}
        bool operator ()(const std::map<int, boost::shared_ptr<RobotInfoDyn> >::value_type &pair){
            return pair.second->_iFormationPos == _val;
        }
    private:
        const int &_val;                    
    };
    
    // instance and gc
    static SwarmDataStorage* _instance;
    class GC{
    public: ~GC(){
            if(_instance){
                delete _instance;
                _instance = NULL;
            }
        }
    }_gc;

    // robotID
    int _localRobotID;
    boost::shared_mutex _localRobotIDMutex;

    // masterID
    int _localMasterID;
    EMasterGenerateType _localMasterGenerateType;
    boost::shared_mutex _localMasterIDMutex;
    boost::condition_variable_any _localStateCondition;

    // swarm actor infos,set when gstation pub swarm task.
    typedef std::map<std::string, boost::shared_ptr<ActorGroupInfo> > ACTORGROUPMAP;
    typedef std::map<std::string, ACTORGROUPMAP> SWARMACTORGROUPMAP;
    SWARMACTORGROUPMAP _swarmName2InfoFromTask;
    boost::shared_mutex _swarmName2InfoFromTaskMutex;

    std::set<int> _robotIDsFromTask;
    boost::shared_mutex _robotIDsFromTaskMutex;
    
    // swarm robot infos, contains all robot information in the swarm
    typedef std::map<int, boost::shared_ptr<RobotInfoDyn> > ROBOTINFOMAP;
    ROBOTINFOMAP _robotID2Info;//robotID--SwarmActors
    boost::shared_mutex _robotID2InfoMutex;

    // calculate based on heartbeat count before robot join. 0 means robot is leave, 1 means robot is keeping
    // saved by SwarmRobotHeartbeatStrategySimple object
    std::map<int, float> _robotID2HeartbeatValue;
    boost::shared_mutex _robotID2HeartbeatValueMutex;

    // calculate based on heartbeat latency data, and saved by SwarmRobotHeartbeatStrategyLatency object.
    std::map<int, boost::shared_ptr<RobotHBLatencyStatistic> > _robotID2HBLatencyStatistic;
    boost::shared_mutex _robotID2HBLatencyStatisticMutex;

    //dibin add, log file 
    //fstream for test data log
    std::fstream _fs;
    boost::mutex _fsMutex;

    // send warn msg
    RTPSPublisher<StringMsg>* _pubWarnMsg;
};
#endif
