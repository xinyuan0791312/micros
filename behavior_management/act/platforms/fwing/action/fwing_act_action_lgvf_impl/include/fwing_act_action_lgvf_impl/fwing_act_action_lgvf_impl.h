/* ==================================================================
* Copyright (c) 2018, micROS Group, TAIIC, NIIDT & HPCL.
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions
* are met:
*
* 1. Redistributions of source code must retain the above copyright
* notice, this list of conditions and the following disclaimer.
* 2. Redistributions in binary form must reproduce the above copyright
* notice, this list of conditions and the following disclaimer in the
* documentation and/or other materials provided with the
* distribution.
* 3. All advertising materials mentioning features or use of this software
* must display the following acknowledgement:
* This product includes software developed by the micROS Group. and
* its contributors.
* 4. Neither the name of the Group nor the names of its contributors may
* be used to endorse or promote products derived from this software
* without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY MICROS,GROUP AND CONTRIBUTORS
* ''AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT
* NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
* FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
* THE MICROS,GROUP OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
* OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
* HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
* OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
* IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
* ===================================================================
* Author: Ren Xiaoguang, Wu Yunlong, Li Jinghua, micROS-DA Team, NIIDT, TAIIC.
*/

#ifndef FWING_ACT_ACTION_LGVF_IMPL_H
#define FWING_ACT_ACTION_LGVF_IMPL_H

#include "message_types.h"
#include <pluginlib/class_list_macros.h>
#include "act_softbus/act_action_interface.h"

#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#include "common/param_precision.h"
#include "common/act/action/act_action_cmd_out.h"
#include "common/act/action/act_action_patrol_leader.h"

/**
 * @brief Fwing Act Action LGVF Impl Namespace.
 * 
 */
namespace fwing_act_action_lgvf_impl
{
  /**
   * @brief This is a class to implement the algorithm plugin fwing_act_action_lgvf_impl.
   */
  class FwingActActionLgvfImpl : public act_action_interface::ActActionInterface
  {
    public:

      FwingActActionLgvfImpl();

      ~FwingActActionLgvfImpl();

      /**
       * @brief This is the main function for the algorithms equipped in the action node of BT in the act stage.
       * 
       * @param[in] pInputBuffer pointer to the input buffer
       * @param[in] pOutputBuffer pointer to the output buffer
       */
      virtual void calculateCommand( unified_msg::unified_message_t *pInputBuffer, 
                                     unified_msg::unified_message_t *pOutputBuffer);

      /** @brief This is the function for setting the parameters for the algorithms.
       * 
       * @param[in] pParams pointer to the parameter setting buffer
       */
      virtual void initializeParam(unified_msg::unified_message_t *pParams);

    private:

      act_action_patrol_leader_t _payload;  ///< This is the struct saving input data  

      param_precision_t _paramPrecision;  ///< This is the struct saving parameters

      act_action_cmd_out_t _mavCmd;  ///< This is the struct saving output data 
      
      /**
       * @brief Get desired course angle according to LGVF formula
       * 
       * @param[in] aStateNorth specifies real positon of MAV along Northern direction
       * @param[in] aStateEast specifies real positon of MAV along Eastern direction
       * @param[in] aGoalNorth specifies real positon of MAV along Northern direction
       * @param[in] aGoalEast specifies real positon of MAV along Eastern direction
       * @param[in] aDesiredDistance specifies desired distance between MAV and virtual agent
       * @param[in] aCurrentCourse specifies real course angle of MAV 
       * @param[out] aRealDistance obtains real distance from virtual agent
       * 
       * @return float obtains desired course angle
       */
      float getLgvfChi(const float& aStateNorth, 
                       const float& aStateEast, 
                       const float& aGoalNorth, 
                       const float& aGoalEast, 
                       const float& aDesiredDistance, 
                       const float& aCurrentCourse, 
                             float& aRealDistance);
      
      /**
       * @brief Get the course angle adaptive parameter according to velcity and radius of trajectory
       * 
       * @param[in] aDesiredVelocity specifies flight velocity
       * @param[in] aDesiredDistance specifies radius of trajectory
       * @return float adaptive parameter of course angle
       */
      float getChiParam(const float& aDesiredVelocity, const float& aDesiredDistance);
      
      /**
       * @brief Keep course angle in range function
       * 
       * @param[in] aCurrentCourse specifies real course angle of MAV
       * @param[out] aDesiredCourse obtains desired course angle of MAV 
       */
      void keepChiInRange(const float& aCurrentCourse, float& aDesiredCourse);

  };
}//end namespace
#endif