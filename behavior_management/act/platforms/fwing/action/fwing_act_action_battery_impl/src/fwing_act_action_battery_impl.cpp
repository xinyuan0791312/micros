/* ==================================================================
* Copyright (c) 2018, micROS Group, TAIIC, NIIDT & HPCL.
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions
* are met:
*
* 1. Redistributions of source code must retain the above copyright
* notice, this list of conditions and the following disclaimer.
* 2. Redistributions in binary form must reproduce the above copyright
* notice, this list of conditions and the following disclaimer in the
* documentation and/or other materials provided with the
* distribution.
* 3. All advertising materials mentioning features or use of this software
* must display the following acknowledgement:
* This product includes software developed by the micROS Group. and
* its contributors.
* 4. Neither the name of the Group nor the names of its contributors may
* be used to endorse or promote products derived from this software
* without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY MICROS,GROUP AND CONTRIBUTORS
* ''AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT
* NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
* FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
* THE MICROS,GROUP OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
* OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
* HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
* OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
* IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
* ===================================================================
* Author: Ren Xiaoguang, Wu Yunlong, Li Jinghua, micROS-DA Team, NIIDT, TAIIC.
*/

#include "fwing_act_action_battery_impl/fwing_act_action_battery_impl.h"

namespace fwing_act_action_battery_impl
{	
  FwingActActionBatteryImpl::FwingActActionBatteryImpl()
  {
    std::cout << "********** [act_action_BATTERY_impl] FwingActActionBatteryImpl Loaded! **********" << std::endl;
  }
  
  void FwingActActionBatteryImpl::initializeParam(unified_msg::unified_message_t *pParams)
  {
    if(pParams->type[0] == 0x05 && pParams->type[1] == 0x00 && pParams->type[2] == 0x00)
    {
      memcpy(&_paramPrecision, &pParams->payload[0], sizeof(_paramPrecision));
    }
  }
  
  void FwingActActionBatteryImpl::calculateCommand(unified_msg::unified_message_t *pInputBuffer, 
                                                   unified_msg::unified_message_t *pOutputBuffer)
  {
    if((pInputBuffer->type[0] == 0x01 || pInputBuffer->type[0] == 0x02) && pInputBuffer->type[1] == 0x00 && pInputBuffer->type[2] == 0x00)
    {
      memcpy(&_payload, &(pInputBuffer->payload[0]), sizeof(_payload));
      
      float r = 0;
      float chi_d = getLgvfChi(_payload.mav_pos_N, _payload.mav_pos_E, _payload.virtual_agent_y, _payload.virtual_agent_x-300, _payload.rd, _payload.mav_chi, r);
      chi_d += getChiParam(_payload.vd, _payload.rd);
      keepChiInRange(_payload.mav_chi, chi_d);
      
      _mavCmd.Va_c = _payload.mav_Va;
      _mavCmd.h_c = _payload.hd;
      _mavCmd.chi_c = chi_d;

      if(fabs(r-_payload.rd) < 20)
      {
        _mavCmd.Va_c = 0;
        _mavCmd.h_c =  0;
        _mavCmd.chi_c = 0;
      }
      pOutputBuffer->setPayload(_mavCmd);
      memcpy(pInputBuffer, pOutputBuffer, sizeof(*pOutputBuffer));
      
    }else
    { 
      pOutputBuffer->error_code[0] = 0x01;
      std::cout << " [act_action_BATTERY_impl] action_input_buffer_ type is Wrong !" << std::endl;
    }
    std::cout << "********** [act_action_BATTERY_impl] calculateCommand is executed! **********" << std::endl;
  }

  FwingActActionBatteryImpl::~FwingActActionBatteryImpl(){}

  float FwingActActionBatteryImpl::getLgvfChi(const float& aStateNorth, 
                                              const float& aStateEast, 
                                              const float& aGoalNorth, 
                                              const float& aGoalEast, 
                                              const float& aDesiredDistance, 
                                              const float& aCurrentCourse, 
                                                    float& aRealDistance)
  {
    float diffNorth = aStateNorth - aGoalNorth;
    float diffEast = aStateEast - aGoalEast;
    aRealDistance = sqrt(pow(diffNorth,2) +  pow(diffEast,2));
    float B = aRealDistance*aRealDistance - aDesiredDistance*aDesiredDistance;
    float C = aRealDistance*aDesiredDistance;
		
    if(fabs(diffNorth*B + 2*diffEast*C) > _paramPrecision.precision)
    {
      return (atan2(-((diffEast)*B - 2*(diffNorth)*C), -((diffNorth)*B + 2*(diffEast)*C)));
    }else
    {
      return aCurrentCourse;
    }
  }

  float FwingActActionBatteryImpl::getChiParam(const float& aDesiredVelocity, const float& aDesiredDistance)
  {
    return( - 0.1569 + 0.0862*aDesiredVelocity - 0.006063*aDesiredDistance 
            - 0.0009796*aDesiredVelocity*aDesiredDistance + 0.0009238*aDesiredVelocity*aDesiredVelocity 
            + 0.0001031*aDesiredDistance*aDesiredDistance);
  }

  void FwingActActionBatteryImpl::keepChiInRange(const float& aCurrentCourse, float& aDesiredCourse)
  {
    if(aDesiredCourse - aCurrentCourse > M_PI)
      aDesiredCourse -= 2*M_PI;
    else if(aDesiredCourse - aCurrentCourse < - M_PI)
      aDesiredCourse += 2*M_PI;
    else
      return;	
  }

} //end namespace

PLUGINLIB_EXPORT_CLASS(fwing_act_action_battery_impl::FwingActActionBatteryImpl, act_action_interface::ActActionInterface)






