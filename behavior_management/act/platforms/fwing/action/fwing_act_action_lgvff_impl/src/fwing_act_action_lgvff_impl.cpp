/* ==================================================================
* Copyright (c) 2018, micROS Group, TAIIC, NIIDT & HPCL.
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions
* are met:
*
* 1. Redistributions of source code must retain the above copyright
* notice, this list of conditions and the following disclaimer.
* 2. Redistributions in binary form must reproduce the above copyright
* notice, this list of conditions and the following disclaimer in the
* documentation and/or other materials provided with the
* distribution.
* 3. All advertising materials mentioning features or use of this software
* must display the following acknowledgement:
* This product includes software developed by the micROS Group. and
* its contributors.
* 4. Neither the name of the Group nor the names of its contributors may
* be used to endorse or promote products derived from this software
* without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY MICROS,GROUP AND CONTRIBUTORS
* ''AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT
* NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
* FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
* THE MICROS,GROUP OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
* OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
* HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
* OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
* IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
* ===================================================================
* Author: Ren Xiaoguang, Wu Yunlong, Li Jinghua, micROS-DA Team, NIIDT, TAIIC.
*/

#include "fwing_act_action_lgvff_impl/fwing_act_action_lgvff_impl.h"
#include "ros/ros.h"

namespace fwing_act_action_lgvff_impl
{	
  FwingActActionLgvffImpl::FwingActActionLgvffImpl()
  {
    std::cout << "********** [act_action_LGVFF_impl] FwingActActionLgvffImpl Loaded! **********" << std::endl;
  }
  
  void FwingActActionLgvffImpl::initializeParam(unified_msg::unified_message_t *pParams)
  {
    if(pParams->type[0] == 0x05 && pParams->type[1] == 0x00 && pParams->type[2] == 0x00)
    {
      memcpy(&_paramPrecision, &pParams->payload[0], sizeof(_paramPrecision));
    }
  }
  
  void FwingActActionLgvffImpl::calculateCommand(unified_msg::unified_message_t *pInputBuffer, 
                                                 unified_msg::unified_message_t *pOutputBuffer)
  {
    if(pInputBuffer->type[0] == 0x02 && pInputBuffer->type[1] == 0x00 && pInputBuffer->type[2] == 0x00)
    {
      memcpy(&_payload, &(pInputBuffer->payload[0]), sizeof(_payload));
		
      float r = 0;
      float cmdVel = _payload.vd;
      float desiredCourse = getLgvfChi(_payload.mav_pos_N, _payload.mav_pos_E, _payload.mav_chi, _payload.virtual_agent_y, _payload.virtual_agent_x, _payload.rd, r);
      if(_payload.leader_state_init)
      {
        float phase = getPhase(_payload.mav_pos_N, _payload.mav_pos_E, _payload.virtual_agent_y, _payload.virtual_agent_x); 
        float leaderPhase = getPhase(_payload.leader_pos_N, _payload.leader_pos_E, _payload.leader_virtual_agent_y, _payload.leader_virtual_agent_x); 
        // float realLeaderPhase = getPhase(_payload.real_leader_pos_N, _payload.real_leader_pos_E, _payload.leader_virtual_agent_y, _payload.leader_virtual_agent_x);
        float tmpLeaderPhase = leaderPhase;
        if( _lastLeaderStateInitialized && fabs( leaderPhase - _lastLeaderPhase ) < _paramPrecision.precision ) 
        {
          struct timeval tv_duration_end;
          gettimeofday(&tv_duration_end, NULL);
          long int deltaInterval = tv_duration_end.tv_sec*1e6 + tv_duration_end.tv_usec - (_commEnd.tv_sec*1e6 + _commEnd.tv_usec);
          tmpLeaderPhase = _lastLeaderPhase + _lastestLeaderPalstance * (deltaInterval/1000.0);	
          while(tmpLeaderPhase > M_PI)
            tmpLeaderPhase -= 2*M_PI;
          while(tmpLeaderPhase < -M_PI)
            tmpLeaderPhase += 2*M_PI;
          if(!_payload.lost_leader_state)
          {
            _payload.lost_leader_state_start_cnt = 0;
            _payload.lost_leader_event_pubed = false;
            _payload.lost_leader_state = true;
          }
          if(_payload.lost_leader_state_start_cnt ++ > (1e6 / _payload.sleep_duration) * _payload.lost_leader_find_duration)
          {
            _payload.lost_leader_state_start_cnt = (1e6 / _payload.sleep_duration) * _payload.lost_leader_find_duration;
          }
        }
        else 
        {
          COMM_TIME_PROBE;
          if(_commInterval > 0)
          {
            double tmpPalstance = (leaderPhase - _lastLeaderPhase) / (_commInterval/1000.0);
            _lastestLeaderPalstance = tmpPalstance * _lastestLeaderPalstance >= 0 ? tmpPalstance : _lastestLeaderPalstance;    
          }
          _lastLeaderPhase = leaderPhase;	
          _lastLeaderStateInitialized = true;
          _payload.lost_leader_state = false;
        }
        float diffPhase = standAngleDiff(phase, tmpLeaderPhase);
        synchronizePhase(_payload.vd, _payload.rd, diffPhase, _payload.mav_chi, _payload.v_max, _payload.v_min, desiredCourse, cmdVel);
        _lastCourseAngle =  _payload.mav_chi;
        _lastCourseInitialized = true;
      }else
      {
        desiredCourse += getChiParam(_payload.vd, _payload.rd);
        keepChiInRange(_payload.mav_chi, desiredCourse);
      }
      _mavCmd.chi_c = desiredCourse;
      _mavCmd.Va_c = cmdVel;
      _mavCmd.h_c = _payload.hd;
      
      pOutputBuffer->setPayload(_mavCmd);
      memcpy(pInputBuffer, pOutputBuffer, sizeof(*pOutputBuffer));
    }else
    { 
      pOutputBuffer->error_code[0] = 0x01;
      std::cout << " [act_action_LGVFF_impl] action_input_buffer_ type is Wrong !" << std::endl;
    }
    
    std::cout << "********** [act_action_LGVFF_impl] calculateCommand is executed! **********" << std::endl;
  }

  FwingActActionLgvffImpl::~FwingActActionLgvffImpl()  {}

  float FwingActActionLgvffImpl::getPhase(const float& aStateNorth, 
                                          const float& aStateEast, 
								                          const float& aGoalNorth, 
								                          const float& aGoalEast )
  {
    float diffNorth = aStateNorth - aGoalNorth;
    float diffEast = aStateEast - aGoalEast;
		
    return ( atan2(diffNorth, diffEast) );
  }

  void FwingActActionLgvffImpl::synchronizePhase(const float& aDesiredFinalVelocity, 
                                                 const float& aDesiredDistance, 
                                                 const float& aPhaseDifference, 
                                                 const float& aCurrentCourse, 
                                                 const float& aMaximumVelocity, 
                                                 const float& aMinmumVelocity, 
                                                       float& aDesiredCourse, 
                                                       float& aDesiredVelocity)
  {
    aDesiredVelocity = aDesiredFinalVelocity + aPhaseDifference*4;
    keepVelInRange(aMinmumVelocity, aMaximumVelocity, aDesiredVelocity);
    // aDesiredCourse = (aCurrentCourse - _lastCourseAngle) * _lastestLeaderPalstance > 0 ? (aDesiredCourse - M_PI) : aDesiredCourse;
    aDesiredCourse += getChiParam(aDesiredVelocity, aDesiredDistance);
    keepChiInRange(aCurrentCourse, aDesiredCourse);	 
  }

  float FwingActActionLgvffImpl::getLgvfChi(const float& aStateNorth, 
                                            const float& aStateEast, 
                                            const float& aCurrentCourse, 
                                            const float& aGoalNorth, 
                                            const float& aGoalEast, 
                                            const float& aDesiredDistance, 
                                                  float& aRealDistance)
  {
    float diffNorth = aStateNorth - aGoalNorth;
    float diffEast = aStateEast - aGoalEast;
    aRealDistance = sqrt(pow(diffNorth,2) +  pow(diffEast,2));
    float B = aRealDistance*aRealDistance - aDesiredDistance*aDesiredDistance;
    float C = aRealDistance*aDesiredDistance;
			
    if(fabs(diffNorth*B + 2*diffEast*C) > _paramPrecision.precision)
    {  
      return (atan2(-((diffEast)*B - 2*(diffNorth)*C), -((diffNorth)*B + 2*(diffEast)*C)));
    }else
    {
      return aCurrentCourse;
    }
  }


  float FwingActActionLgvffImpl::getChiParam(const float& aDesiredVelocity, const float& aDesiredDistance)
  {
    return( - 0.1569 + 0.0862*aDesiredVelocity - 0.006063*aDesiredDistance 
            - 0.0009796*aDesiredVelocity*aDesiredDistance + 0.0009238*aDesiredVelocity*aDesiredVelocity 
            + 0.0001031*aDesiredDistance*aDesiredDistance);
  }

  void FwingActActionLgvffImpl::keepChiInRange(const float& aCurrentCourse, float& aDesiredCourse)
  {
    if(aDesiredCourse - aCurrentCourse > M_PI)
      aDesiredCourse -= 2*M_PI;
    else if(aDesiredCourse - aCurrentCourse < - M_PI)
      aDesiredCourse += 2*M_PI;
    else
      return;	
  }

  void FwingActActionLgvffImpl::keepVelInRange(const float& aMinmumVelocity, const float& aMaximumVelocity, float& aDesiredVelocity )
  {
    if( aDesiredVelocity < aMinmumVelocity )
      aDesiredVelocity = aMinmumVelocity;
    else if( aDesiredVelocity > aMaximumVelocity )
      aDesiredVelocity = aMaximumVelocity;
    else
      return;	
  }

  float FwingActActionLgvffImpl::standAngleDiff(const float& aMAVPhase, const float& aTargetPhase)
  {
    float diff = aMAVPhase - aTargetPhase;
    if ( diff < -M_PI && diff >= -2*M_PI )  
      return (diff + 2*M_PI);
    else if ( diff <= 2*M_PI && diff > M_PI ) 
      return ( diff - 2*M_PI );
    else 
      return diff;
  }

  void FwingActActionLgvffImpl::getCommTimeInterval()
  {
    if( _cycleCounts == 0 )
    {
      gettimeofday(&_commStart,NULL);
      _commInterval = -1;
      _cycleCounts = 1;
    }else
    {
      gettimeofday(&_commEnd,NULL);
      _commInterval = _commEnd.tv_sec*1e6 + _commEnd.tv_usec - _commStart.tv_sec*1e6 - _commStart.tv_usec;   
      _commStart.tv_sec = _commEnd.tv_sec;
      _commStart.tv_usec = _commEnd.tv_usec;
    }
  }

} //end namespace

PLUGINLIB_EXPORT_CLASS(fwing_act_action_lgvff_impl::FwingActActionLgvffImpl, act_action_interface::ActActionInterface)