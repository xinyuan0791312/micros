/* ==================================================================
* Copyright (c) 2018, micROS Group, TAIIC, NIIDT & HPCL.
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions
* are met:
*
* 1. Redistributions of source code must retain the above copyright
* notice, this list of conditions and the following disclaimer.
* 2. Redistributions in binary form must reproduce the above copyright
* notice, this list of conditions and the following disclaimer in the
* documentation and/or other materials provided with the
* distribution.
* 3. All advertising materials mentioning features or use of this software
* must display the following acknowledgement:
* This product includes software developed by the micROS Group. and
* its contributors.
* 4. Neither the name of the Group nor the names of its contributors may
* be used to endorse or promote products derived from this software
* without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY MICROS,GROUP AND CONTRIBUTORS
* ''AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT
* NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
* FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
* THE MICROS,GROUP OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
* OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
* HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
* OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
* IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
* ===================================================================
* Author: Ren Xiaoguang, Wu Yunlong, Li Jinghua, micROS-DA Team, NIIDT, TAIIC.
*/

#ifndef FWING_ACT_CONDITION_BATTERY_IMPL_H
#define FWING_ACT_CONDITION_BATTERY_IMPL_H

#include "message_types.h"
#include <pluginlib/class_list_macros.h>
#include "act_softbus/act_condition_interface.h"

#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#include "common/act/param/act_param_battery.h"
#include "common/act/condition/act_condition_battery.h"

#define PRECISION 1e-5

/**
 * @brief Fwing Act Condition Battery Impl Namespace.
 * 
 */
namespace fwing_act_condition_battery_impl
{
  /**
   * @brief This is a class to implement the algorithm plugin fwing_act_condition_battery_impl.
   */
  class FwingActConditionBatteryImpl : public act_condition_interface::ActConditionInterface
  {
    public:

      FwingActConditionBatteryImpl();

      ~FwingActConditionBatteryImpl();
      
      /**
       * @brief This is the main function for the algorithms equipped in the condition node of BT in the act stage.
       * 
       * @param[in] pBuffer pointer to the condition buffer
       */
      virtual bool judgeCondition(unified_msg::unified_message_t *pBuffer);

      /** @brief This is the function for setting the parameters for the algorithms.
       * 
       * @param[in] pParams pointer to the parameter setting buffer
       */
      virtual void initializeParam(unified_msg::unified_message_t *pParams);


    private:

      act_condition_battery_t _payload;  ///< This is the struct saving input data  

      float _minBattery = 2.0;  ///< This is the minmunum battery to trigger low battery algorithm
  };
} //end namespace
#endif