/* ==================================================================
* Copyright (c) 2018, micROS Group, TAIIC, NIIDT & HPCL.
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions
* are met:
*
* 1. Redistributions of source code must retain the above copyright
* notice, this list of conditions and the following disclaimer.
* 2. Redistributions in binary form must reproduce the above copyright
* notice, this list of conditions and the following disclaimer in the
* documentation and/or other materials provided with the
* distribution.
* 3. All advertising materials mentioning features or use of this software
* must display the following acknowledgement:
* This product includes software developed by the micROS Group. and
* its contributors.
* 4. Neither the name of the Group nor the names of its contributors may
* be used to endorse or promote products derived from this software
* without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY MICROS,GROUP AND CONTRIBUTORS
* ''AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT
* NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
* FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
* THE MICROS,GROUP OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
* OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
* HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
* OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
* IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
* ===================================================================
* Author: Ren Xiaoguang, Wu Yunlong, Li Jinghua, micROS-DA Team, NIIDT, TAIIC.
*/

#include "fwing_act_condition_battery_impl/fwing_act_condition_battery_impl.h"

namespace fwing_act_condition_battery_impl
{	
  FwingActConditionBatteryImpl::FwingActConditionBatteryImpl()
  {
    std::cout << "********** [act_CONDITION_BATTERY_impl] FwingActConditionBatteryImpl Loaded! **********" << std::endl;
  }
  
  bool FwingActConditionBatteryImpl::judgeCondition(unified_msg::unified_message_t *pBuffer)
  {
    if(pBuffer->type[0] == 0x03 && pBuffer->type[1] == 0x00 && pBuffer->type[2] == 0x00)
    {
      memcpy(&_payload, &(pBuffer->payload[0]), sizeof(_payload));
      // Condition checking and state update
      if(_payload.battery < _minBattery)
      {
        return true;
      }else
      {
        return false;
      }  
    }else
    {
      std::cout << " [act_CONDITION_BATTERY_impl] condition_input_buffer_ type is Wrong !" << std::endl;
      return true;
    }
  }

  void FwingActConditionBatteryImpl::initializeParam(unified_msg::unified_message_t *pParams)
  {
    if(pParams->type[0] == 0x04 && pParams->type[1] == 0x00 && pParams->type[2] == 0x00)
    {
      act_param_battery_t payload;
      memcpy(&payload, &(pParams->payload[0]), sizeof(_payload));
      _minBattery = payload.min_battery;
    }else
    {
      std::cout << " [act_CONDITION_BATTERY_impl] param_input_buffer_ type is Wrong !" << std::endl;
    }
  }

  FwingActConditionBatteryImpl::~FwingActConditionBatteryImpl(){}
} //end namespace

PLUGINLIB_EXPORT_CLASS(fwing_act_condition_battery_impl::FwingActConditionBatteryImpl, act_condition_interface::ActConditionInterface)






