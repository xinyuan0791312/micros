###    DESCRIPTION    ### 
  This is a global path planning test script for fixed-wing plane.

  The details are as follows.

###  EXPECTED RESULTS ###
  1. All UAVs take off and fly to a predefined area for executing coverage task. 
     The area coverage algorithm plans the virutal points for all UAVs, then, each
     UAV may fly around its specified virtual points.
  2. In this scenario, when the battery energy of UAVs exhaust, the behavior tree
     model in each plugin will trigger it to switch to a landing state. 
  3. Finally, all UAVs will land on the ground.

###  OPERATING STEPS  ###
#****************************************************************#
***                        [RUN with micROS]                   ***

# Source the workspace
  source XXX/devel/setup.bash
 
# Launch the Gazebo simulation environment:
  roslaunch rosplane_sim fixedwingfor5.launch

# Run the daemon nodes:    
  roslaunch decide_patrol_leader actor_daemon_fixedwing.launch
  roslaunch decide_patrol_leader actor_daemon_fixedwing1.launch
  roslaunch decide_patrol_leader actor_daemon_fixedwing2.launch
  roslaunch decide_patrol_leader actor_daemon_fixedwing3.launch
  roslaunch decide_patrol_leader actor_daemon_fixedwing4.launch

# Run the g_station:    
  roslaunch decide_patrol_leader bt_demo_ground_station.launch

## Input name of decription file of task:
                                         da_demo_testfor5.xml
## Input task name to start:
                                         BT                                         
#****************************************************************#

