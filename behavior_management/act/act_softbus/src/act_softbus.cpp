/* ==================================================================
* Copyright (c) 2018, micROS Group, TAIIC, NIIDT & HPCL.
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions
* are met:
*
* 1. Redistributions of source code must retain the above copyright
* notice, this list of conditions and the following disclaimer.
* 2. Redistributions in binary form must reproduce the above copyright
* notice, this list of conditions and the following disclaimer in the
* documentation and/or other materials provided with the
* distribution.
* 3. All advertising materials mentioning features or use of this software
* must display the following acknowledgement:
* This product includes software developed by the micROS Group. and
* its contributors.
* 4. Neither the name of the Group nor the names of its contributors may
* be used to endorse or promote products derived from this software
* without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY MICROS,GROUP AND CONTRIBUTORS
* ''AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT
* NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
* FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
* THE MICROS,GROUP OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
* OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
* HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
* OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
* IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
* ===================================================================
* Author: Ren Xiaoguang, Wu Yunlong, Li Jinghua, micROS-DA Team, NIIDT, TAIIC.
*/

#include "act_softbus/act_softbus.h"
#include "assert.h"

namespace act_softbus
{
  ActSoftbus::ActSoftbus()
  {
    _inputGlobalBuffer = new unified_msg::unified_message_t();
    _outputGlobalBuffer = new unified_msg::unified_message_t();
    
    _root = new BT::SequenceNode("root");
  }

  ActSoftbus::~ActSoftbus()
  {
    for(auto &c:_paramBuffer)
      delete c;
    std::vector<unified_msg::unified_message_t* >().swap(_paramBuffer);

    for(auto &c:_conditionBuffer)
      delete c;
    std::vector<unified_msg::unified_message_t* >().swap(_conditionBuffer);

    delete _inputGlobalBuffer;
    delete _outputGlobalBuffer;
    
    delete _root;
  }

  unified_msg::unified_message_t * ActSoftbus::addParamBuffer()  
  {
    unified_msg::unified_message_t *tmp = new unified_msg::unified_message_t();
    _paramBuffer.push_back(tmp);
  }

  unified_msg::unified_message_t * ActSoftbus::addConditionBuffer()  
  {
    unified_msg::unified_message_t *tmp = new unified_msg::unified_message_t();
    _conditionBuffer.push_back(tmp);
  }

}

