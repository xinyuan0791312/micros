/* ==================================================================
* Copyright (c) 2018, micROS Group, TAIIC, NIIDT & HPCL.
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions
* are met:
*
* 1. Redistributions of source code must retain the above copyright
* notice, this list of conditions and the following disclaimer.
* 2. Redistributions in binary form must reproduce the above copyright
* notice, this list of conditions and the following disclaimer in the
* documentation and/or other materials provided with the
* distribution.
* 3. All advertising materials mentioning features or use of this software
* must display the following acknowledgement:
* This product includes software developed by the micROS Group. and
* its contributors.
* 4. Neither the name of the Group nor the names of its contributors may
* be used to endorse or promote products derived from this software
* without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY MICROS,GROUP AND CONTRIBUTORS
* ''AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT
* NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
* FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
* THE MICROS,GROUP OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
* OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
* HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
* OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
* IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
* ===================================================================
* Author: Ren Xiaoguang, Wu Yunlong, Li Jinghua, micROS-DA Team, NIIDT, TAIIC.
*/

#ifndef ACT_ACTION_NODE_H
#define ACT_ACTION_NODE_H

#include <vector>
#include <string.h>
#include <pluginlib/class_loader.h>

#include "action_node.h"
#include "message_types.h"
#include "act_softbus/act_action_interface.h"

/**
 * @brief Behavior Tree Namespace.
 */
namespace BT
{
  template <class T>
  /**
   * @brief This is a class for Act Action Node.
   */
  class ActActionNode : public ActionNode
  {
    public:
    
      explicit ActActionNode(std::string aName);
      ~ActActionNode();
  
      void WaitForTick();
      void set_time(int aTime);
      void Halt();
      void set_boolean_value(bool aBooleanValue);
      
      /** 
       * @brief Set Method
       * 
       * @param[in] aBaseClassPkgName demonstrates name of package of interface base class 
       * @param[in] anInterfaceClassName demonstrates name of interface base class 
       * @param[in] anInterfaceImplName demonstrates name of interface subclass(allgorithm) 
       */
      void setMethod(const std::string aBaseClassPkgName,
                     const std::string anInterfaceClassName,
                     const std::string anInterfaceImplName);
      
      /** 
       * @brief Set Buffer
       * 
       * @param[in] pInputBuffer demonstrates structure pointer containing input parameters 
       * @param[in] pOutputBuffer demonstrates structure pointer containing output parameters 
       */
      void setBuffer(unified_msg::unified_message_t *pInputBuffer,
                     unified_msg::unified_message_t *pOutputBuffer);
      
      /** @brief Set Param
       * 
       * @param[in] pParams demonstrates structure pointer containing parameters to be initialized
       */
      void setParam(unified_msg::unified_message_t *pParams);
      
    private:
    
      int _time;
      bool _booleanValue;
      bool _paramSet;

      pluginlib::ClassLoader<T> *_testLoader;
      boost::shared_ptr<T> _testCal; 

      unified_msg::unified_message_t *_inputBuffer;
      unified_msg::unified_message_t *_outputBuffer;
      unified_msg::unified_message_t *_inputParamBuffer;
  };

  template <class T>
  ActActionNode<T>::ActActionNode(std::string name) : ActionNode::ActionNode(name)
  {
    type_ = BT::ACTION_NODE;
    _booleanValue = true;
    _paramSet = false;
    _time = 1;
    thread_ = std::thread(&ActActionNode::WaitForTick, this);
  }

  template <class T>
  ActActionNode<T>::~ActActionNode() 
  {
    _testCal.reset();
    delete _testLoader;
    
    delete _inputBuffer;
    delete _outputBuffer;
    delete _inputParamBuffer;
  } 

  template <class T>
  void ActActionNode<T>::WaitForTick()
  { 
    while (true)
    {
      // Waiting for the first tick to come
      DEBUG_STDOUT(get_name() << " WAIT FOR TICK");

      tick_engine.Wait();
      DEBUG_STDOUT(get_name() << " TICK RECEIVED");

      // Running state
      set_status(BT::RUNNING);
          
      // Perform action...
      // std::cout << " Action to be executed! " << std::endl;
      
      if(_paramSet)
      {
        _testCal->initializeParam(_inputParamBuffer); 
        _paramSet = false;
      }

      _testCal->calculateCommand(_inputBuffer, _outputBuffer);       

      set_status(BT::SUCCESS);
    }
  }

  template <class T>
  void ActActionNode<T>::Halt()
  {
    set_status(BT::HALTED);
    DEBUG_STDOUT("HALTED state set!");
  }

  template <class T>
  void ActActionNode<T>::set_time(int aTime)
  {
    _time = aTime;
  }

  template <class T>
  void ActActionNode<T>::set_boolean_value(bool aBooleanValue)
  {
    _booleanValue = aBooleanValue;
  }

  template <class T>
  void ActActionNode<T>::setMethod(const std::string aBaseClassPkgName,
                                   const std::string anInterfaceClassName,
                                   const std::string anInterfaceImplName)
  {
    _testLoader = new pluginlib::ClassLoader<T>(aBaseClassPkgName, anInterfaceClassName);
    _testCal = _testLoader->createInstance(anInterfaceImplName);
  }

  template <class T>
  void ActActionNode<T>::setBuffer(unified_msg::unified_message_t *pInputBuffer,
                                   unified_msg::unified_message_t *pOutputBuffer)
  {
    _inputBuffer = pInputBuffer;
    _outputBuffer = pOutputBuffer;
  }

  template <class T>
  void ActActionNode<T>::setParam(unified_msg::unified_message_t *pParams)
  {
    _inputParamBuffer = pParams;
    _paramSet = true;
  }

} //end namespace

#endif
