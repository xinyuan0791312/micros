/* ==================================================================
* Copyright (c) 2018, micROS Group, TAIIC, NIIDT & HPCL.
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions
* are met:
*
* 1. Redistributions of source code must retain the above copyright
* notice, this list of conditions and the following disclaimer.
* 2. Redistributions in binary form must reproduce the above copyright
* notice, this list of conditions and the following disclaimer in the
* documentation and/or other materials provided with the
* distribution.
* 3. All advertising materials mentioning features or use of this software
* must display the following acknowledgement:
* This product includes software developed by the micROS Group. and
* its contributors.
* 4. Neither the name of the Group nor the names of its contributors may
* be used to endorse or promote products derived from this software
* without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY MICROS,GROUP AND CONTRIBUTORS
* ''AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT
* NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
* FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
* THE MICROS,GROUP OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
* OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
* HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
* OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
* IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
* ===================================================================
* Author: Ren Xiaoguang, Wu Yunlong, Li Jinghua, micROS-DA Team, NIIDT, TAIIC.
*/

#ifndef ACT_ACTION_INTERFACE_H
#define ACT_ACTION_INTERFACE_H

#include "message_types.h"

/**
 * @brief Act Action Interface Namespace.
 */
namespace act_action_interface
{
  /**
   * @brief This is a class for Act Action Interface.
   */
  class ActActionInterface
  {
    public:
    
      /**
       * @brief Implement interface function for ActActionNode
       * 
       * @param[in] pInputBuffer demonstrates structure pointer containing input parameters 
       * @param[in] pOutputBuffer demonstrates structure pointer containing output parameters 
       */
      virtual void calculateCommand(unified_msg::unified_message_t *pInputBuffer, 
                                    unified_msg::unified_message_t *pOutputBuffer){}
      
      /** @brief Initialize parameters
       * 
       * @param[in] pParams demonstrates structure pointer containing parameters to be initialized
      */
      virtual void initializeParam(unified_msg::unified_message_t *pParams){}
      
      /**
       * @brief Destroy the ActActionInterface object
       */
      virtual ~ActActionInterface(){}

    protected:

      /**
       * @brief Construct a new ActActionInterface object
       */
      ActActionInterface(){}
  };
} //end namespace
#endif
