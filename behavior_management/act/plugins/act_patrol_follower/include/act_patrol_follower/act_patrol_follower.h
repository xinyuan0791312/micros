/* ==================================================================
* Copyright (c) 2018, micROS Group, TAIIC, NIIDT & HPCL.
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions
* are met:
*
* 1. Redistributions of source code must retain the above copyright
* notice, this list of conditions and the following disclaimer.
* 2. Redistributions in binary form must reproduce the above copyright
* notice, this list of conditions and the following disclaimer in the
* documentation and/or other materials provided with the
* distribution.
* 3. All advertising materials mentioning features or use of this software
* must display the following acknowledgement:
* This product includes software developed by the micROS Group. and
* its contributors.
* 4. Neither the name of the Group nor the names of its contributors may
* be used to endorse or promote products derived from this software
* without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY MICROS,GROUP AND CONTRIBUTORS
* ''AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT
* NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
* FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
* THE MICROS,GROUP OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
* OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
* HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
* OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
* IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
* ===================================================================
* Author: Ren Xiaoguang, Wu Yunlong, Li Jinghua, micROS-DA Team, NIIDT, TAIIC.
*/

#ifndef ACT_PATROL_FOLLOWER_H
#define ACT_PATROL_FOLLOWER_H

#include "message_types.h"
#include "da_adapter/da_adapter.h"
#include "act_softbus/act_softbus.h"
#include <pluginlib/class_list_macros.h>

#include <cmath>
#include <std_msgs/Empty.h>
#include <std_msgs/String.h>
#include <geometry_msgs/Point.h>
#include "rosplane_msgs/State.h"
#include "rosplane_msgs/Controller_Commands.h"

#include "common/param_precision.h"
#include "common/act/param/act_param_battery.h"
#include "common/act/action/act_action_cmd_out.h"
#include "common/act/action/act_action_patrol_follower.h"
#include "common/act/condition/act_condition_battery.h"

/**
 * @brief Plugin Act Patrol Follower Namespace.
 * 
 */
namespace act_patrol_follower
{
  /**
   * @brief Fixed-wing state struct
   */
  #pragma pack (1)
  typedef struct __fwing_state
  {
    float pos_N;  ///< This is the position in north direction (m)
    float pos_E;  ///< This is the position in east direction (m)
    float pos_D;  ///< This is the position in negative height direction (m)
    float Va;  ///< This is the airspeed (m/s)
    float chi;  ///< This is the course angle(rad)
  }fwing_state;
  #pragma pack()

  /**
   * @brief Fixed-wing virtual agent struct
   */
  #pragma pack (1)
  typedef struct __fwing_va
  {
    float x;  ///< This is the position in east direction
    float y;  ///< This is the position in north direction
  }fwing_va;
  #pragma pack()

  /**
   * @brief This is a class to implement the actor plugin for actor act_patrol_follower.
   */
  class ActPatrolFollower : public act_softbus::ActSoftbus, public da_adapter::DaAdapter
  {
    public:

      #ifdef RUN_ALONE
        ActPatrolFollower();
      #endif

      /**
       * @brief Overload start function of General Plugin
       */
      virtual void start();

      /**
       * @brief Initialize arguments 
       * for instnnce, remapping parameters from launch files or desciption files, etc.
       */
      virtual void initArgs();

    private:

      ros::NodeHandle _nh;
      ros::NodeHandle _nhPrivate; 

      ros::Subscriber _goalsSub;  ///< This is the subscriber for position of virtual agents
      ros::Subscriber _mavStateSub;  ///< This is the subscriber for the state of MAV
      ros::Subscriber _leaderStateSub;  ///< This is the subscriber for the state of the leader MAV
      ros::Subscriber _areacoverDoneSub;  ///< This is the subscriber for areacover_done state
      
      ros::Publisher _controllerCommandsPub; ///< This is the publisher for msg to controller

      act_action_patrol_follower_t _payload;  ///< This is the struct saving input data of the loaded algorithm plugin
      act_condition_battery_t _judgePayload = {10.0};  ///< This is the struct saving output data of the loaded algorithm plugin

      fwing_state _mavState = {0, 0, 0};  ///< This is the struct variable saving the state of MAV
      fwing_state _leaderState = {0, 0, 0};  ///< This is the struct variable saving the state of leader MAV
      fwing_va _mavVAPosition = {0.0,0.0};  ///< This is the struct variable saving the position of virtual agent   
      fwing_va _leaderVAPosition = {0.0,0.0};  ///< This is indicates the struct variable saving the state of virtual agent of leader MAV
                           
      rosplane_msgs::Controller_Commands _cmdMsg;  ///< This is the msg to controller_commands 
            
      bool _runAlone;  ///< This specifies whether the plugin is running alone or not
      bool _landing = false;  ///< This specifies whether landing command published or not 
      bool _goalReceived = false;  ///< This specifies whether position of virtual agents is received or not
      bool _areacoverDoneReceived = false;  ///< This specifies whether areacover_done is received or not
      bool _mavStateInitialized = false;  ///< This specifies whether the state of MAV is initialized or not
      bool _leaderStateInitialized = false;  ///< This specifies whether the state of MAV is initialized or not
      bool _enemyEventPublished = false;  ///< This specifies whether enemy_find_event is published or not
      bool _finishEventPublished = false;  ///< This specifies whether finish_event is published or not

      int _mavID;  ///< This is the identification number of MAV
      int _leaderID;  ///< This is the identificationnumber of the leader MAV
      int _beginTime;  ///< This is the starting time of plugin
      int _totalQuantity;  ///< This is the quantity of MAVs
      int _enemyFindDuration = 40;  ///< This is the duration of enemy finded from the start of plugin
             
      float _heightDifference;    ///< This is the difference between MAVs with serial identification number                           
      
      /**
       * @brief Send calclultated commands function
       * 
       * @param[in] pOutBuffer pointer variable demonstrates _outputGlobalBuffer
       */
      void sendCommand(unified_msg::unified_message_t *pOutBuffer);
            
      /**
       * @brief Goal callback function
       * 
       * @param[in] pMsg specifies the goal message
       */
      void goalCallback(const std_msgs::String::ConstPtr& pMsg);

      /**
       * @brief Leader state callback function
       * 
       * @param[in] pMsg specifies leader state information
       */
      void leader_state_callback(const rosplane_msgs::StateConstPtr& pMsg); 

      /**
       * @brief Current MAV state callback function
       * 
       * @param[in] pMsg specifies own state information
       */
      void uav_state_callback(const rosplane_msgs::StateConstPtr& pMsg);

      /**
       * @brief Areacover_done state callback function
       * 
       * @param pMsg specifies areacover_done state information
       */
      void areacoverDoneCallback(const std_msgs::Empty::ConstPtr& pMsg);

      /**
       * @brief Set the Default Virtrual Point object
       *
       * @param[in] aID specifies the identification number of current MAV
       * @param[in] aQuantity specifies total number of cluster of MAV
       * @param[in] aDeisredDistance specifies desired radius of trajectory
       * @param[out] aDefaultX specifies default position along x-axis to be claculated
       * @param[out] aDefaultY specifies default position to be y-axis claculated
       */
      void setDefaultVirtrualPoint(const int& aID, 
                                   const int& aQuantity, 
                                   const float& aDeisredDistance, 
                                         float& aDefaultX, 
                                         float& aDefaultY);

      /**
       * @brief Discriminate events to triger function
       */
      void discriminateEvents(); 
            
      /**
       * @brief Update _payload and intput_global_buffer_
       * 
       */
      void updateBuffers(); 
  };
} //end namespace

#endif
