/* ==================================================================
* Copyright (c) 2018, micROS Group, TAIIC, NIIDT & HPCL.
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions
* are met:
*
* 1. Redistributions of source code must retain the above copyright
* notice, this list of conditions and the following disclaimer.
* 2. Redistributions in binary form must reproduce the above copyright
* notice, this list of conditions and the following disclaimer in the
* documentation and/or other materials provided with the
* distribution.
* 3. All advertising materials mentioning features or use of this software
* must display the following acknowledgement:
* This product includes software developed by the micROS Group. and
* its contributors.
* 4. Neither the name of the Group nor the names of its contributors may
* be used to endorse or promote products derived from this software
* without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY MICROS,GROUP AND CONTRIBUTORS
* ''AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT
* NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
* FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
* THE MICROS,GROUP OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
* OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
* HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
* OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
* IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
* ===================================================================
* Author: Ren Xiaoguang, Wu Yunlong, Li Jinghua, micROS-DA Team, NIIDT, TAIIC.
*/

#include "act_patrol_follower/act_patrol_follower.h"

namespace act_patrol_follower
{
  #ifdef RUN_ALONE
    ActPatrolFollower::ActPatrolFollower()
    {
      start();
    }
  #endif
  
  void ActPatrolFollower::start()
  {
    //  Step 1 : Initialize arguments  
    initArgs();

    //  Step 2 : Create Control Nodes and Leaf Nodes of Behavior Tree 
    BT::FallbackNode* fallbackRoot = new BT::FallbackNode("fallbackRoot");
    BT::SequenceNode* sequenceLowbBattery = new BT::SequenceNode("sequenceLowbBattery");
    BT::ActConditionNode<act_condition_interface::ActConditionInterface> 
      *conditionLowbBattery = new BT::ActConditionNode<act_condition_interface::ActConditionInterface>("conditionLowbBattery");
    BT::ActActionNode<act_action_interface::ActActionInterface>
      *actionLGVFF = new BT::ActActionNode<act_action_interface::ActActionInterface>("actionLGVFF");
    BT::ActActionNode<act_action_interface::ActActionInterface>
      *actionLowbBattery = new BT::ActActionNode<act_action_interface::ActActionInterface>("actionLowbBattery");
    
    //  Step 3 : Set Action Nodes and Condition Nodes  
    conditionLowbBattery->setParam(_paramBuffer[0]);
    conditionLowbBattery->setMethod("act_softbus", "act_condition_interface::ActConditionInterface", "fwing_act_condition_battery_impl::FwingActConditionBatteryImpl");
    conditionLowbBattery->setBuffer(_conditionBuffer[0]);
    actionLGVFF->setParam(_paramBuffer[1]);
    actionLGVFF->setMethod("act_softbus", "act_action_interface::ActActionInterface", "fwing_act_action_lgvff_impl::FwingActActionLgvffImpl");
    actionLGVFF->setBuffer(_inputGlobalBuffer,_outputGlobalBuffer);
    actionLowbBattery->setParam(_paramBuffer[2]);
    actionLowbBattery->setMethod("act_softbus", "act_action_interface::ActActionInterface", "fwing_act_action_battery_impl::FwingActActionBatteryImpl");
    actionLowbBattery->setBuffer(_inputGlobalBuffer,_outputGlobalBuffer);
    
    //  Step 4 : Build Behavior Tree  
    _root->AddChild(fallbackRoot);
    fallbackRoot->AddChild(sequenceLowbBattery);
    fallbackRoot->AddChild(actionLGVFF);
    sequenceLowbBattery->AddChild(conditionLowbBattery);
    sequenceLowbBattery->AddChild(actionLowbBattery);

    ros::Rate loop_rate(1);
    while(ros::ok())
    {
      #ifdef RUN_ALONE
        ros::spinOnce();
      #else
        discriminateEvents();
        GOON_OR_RETURN
      #endif
      
      if(!_landing)
      {
        //  Step 5 : Update Buffers  
        updateBuffers();

        //  Step 6 : Execute Behavior Tree  
        BT::ReturnStatus state = _root->Tick();

        //  Step 7 : Analyze _outputGlobalBuffer and send command  
        sendCommand(_outputGlobalBuffer);
        if (_root->get_status() != BT::RUNNING)
        {
          _root->ResetColorState();
        }
      } 
      loop_rate.sleep();
    }
    //  Step 8 : Release memory of Control Nodes and Leaf Nodes of Behavior Tree  
    delete fallbackRoot;
    delete sequenceLowbBattery;
    delete conditionLowbBattery;
    delete actionLGVFF;
    delete actionLowbBattery;
  }

  void ActPatrolFollower::initArgs()
  {
    _nh = ros::NodeHandle();
    _nhPrivate = ros::NodeHandle("~");

    #ifdef RUN_ALONE
      _runAlone = true;
    #else
      _runAlone = false;  
    #endif   

    _nhPrivate.param<int>("robot_id", _mavID, 2);
  
    if(_runAlone)
    {
      _nhPrivate.param<float>("v0", _payload.vd, 15.0);
      _nhPrivate.param<float>("rd", _payload.rd, 50.0);
      _nhPrivate.param<float>("h_d", _payload.hd, 100.0);
      _nhPrivate.param<int>("total_number", _totalQuantity, 4);
      
      _nhPrivate.param<float>("delta_h", _heightDifference, 2.0);
      _nhPrivate.param<int>("enemy_find_duration", _enemyFindDuration, 40);
      _nhPrivate.param<float>("v_max", _payload.v_max, 25.0);
      _nhPrivate.param<float>("v_min", _payload.v_min, 10.0);
      _nhPrivate.param<int>("lost_leader_find_duration", _payload.lost_leader_find_duration, 20);
      _nhPrivate.param<int>("sleep_duration", _payload.sleep_duration, 10000);
    }
    else
    {
      _payload.vd = atof( getParam("v0").c_str() );
      _payload.rd = atof( getParam("rd").c_str() );
      _payload.hd = atof( getParam("h_d").c_str() );
      _totalQuantity = atoi( getParam("total_number").c_str() );

      _heightDifference = atof( getParam("delta_h").c_str() );
      _enemyFindDuration = atoi( getParam("enemy_find_duration").c_str() );
      _payload.v_max = atof( getParam("v_max").c_str() );
      _payload.v_min = atof( getParam("v_min").c_str() );
      _payload.lost_leader_find_duration = atoi( getParam("lost_leader_find_duration").c_str() );
      _payload.sleep_duration = atoi( getParam("sleep_duration").c_str() );
    }
    
    _judgePayload.battery = 25;
    if(_mavID == 3)
      _judgePayload.battery = 20;
    
    _inputGlobalBuffer->setValue("type", 2);
    _inputGlobalBuffer->setValue("error_code", 0);
    
    _outputGlobalBuffer->setValue("type", 6);
    _outputGlobalBuffer->setValue("error_code", 0);

    addConditionBuffer();
    _conditionBuffer[0]->setValue("type", 3);
    _conditionBuffer[0]->setValue("error_code", 0);
    
    addParamBuffer();
    _paramBuffer[0]->setValue("type", 4);
    _paramBuffer[0]->setValue("error_code", 0);
    act_param_battery_t condition_param = {10.0};
    _paramBuffer[0]->setPayload(condition_param);
    
    addParamBuffer();
    _paramBuffer[1]->setValue("type", 5);
    _paramBuffer[1]->setValue("error_code", 0); 
    param_precision_t tmp = {1e-5};
    _paramBuffer[1]->setPayload(tmp);
    
    addParamBuffer();
    _paramBuffer[2]->setValue("type", 5);
    _paramBuffer[2]->setValue("error_code", 0);
    _paramBuffer[2]->setPayload(tmp);
    
    _beginTime = (int)ros::Time::now().toSec();
    _payload.hd += _mavID * _heightDifference;
    setDefaultVirtrualPoint(_payload.rd, _totalQuantity, _mavID, _mavVAPosition.y, _mavVAPosition.x); 

    _controllerCommandsPub = advertiseDA<rosplane_msgs::Controller_Commands>("controller_commands", 10, _runAlone);
    
    _goalsSub =  subscribeDA<std_msgs::String>("/goal", 10,&ActPatrolFollower::goalCallback,this,_runAlone);
    _leaderStateSub = subscribeDA<rosplane_msgs::State>("/Leader_state", 10,&ActPatrolFollower::leader_state_callback,this,_runAlone);
    _areacoverDoneSub = subscribeDA<std_msgs::Empty>("/areacover_done", 10,&ActPatrolFollower::areacoverDoneCallback,this,_runAlone);
    _mavStateSub = subscribeDA<rosplane_msgs::State>("truth", 10,&ActPatrolFollower::uav_state_callback,this,_runAlone);
    
    ROS_INFO("[Plugin] Args Initialized");
  }
  
  void ActPatrolFollower::goalCallback(const std_msgs::String::ConstPtr& pMsg)
  {
    memcpy(&_leaderID,&pMsg->data[0],4);
    int size_msg = (pMsg->data).size() / 8;
    if(_leaderID <= size_msg && _mavID <= size_msg)
    {
      memcpy(&_leaderVAPosition.x, &pMsg->data[8*(_leaderID-1)+8], 4);
      memcpy(&_leaderVAPosition.y, &pMsg->data[8*(_leaderID-1)+12], 4);
      
      memcpy(&_mavVAPosition.x, &pMsg->data[8*(_mavID-1)+8], 4);
      memcpy(&_mavVAPosition.y, &pMsg->data[8*(_mavID-1)+12], 4);
      
      _goalReceived = true;	
    }
    else
    {
      _goalReceived = false;	
    } 
  }

  void ActPatrolFollower::uav_state_callback(const rosplane_msgs::StateConstPtr& pMsg)
  {
    _mavState.pos_N = pMsg->position[0];
    _mavState.pos_E = pMsg->position[1];
    _mavState.pos_D = - pMsg->position[2];
    _mavState.Va = pMsg->Va;
    _mavState.chi = pMsg->chi;
    
    _mavStateInitialized = true;
  }

  void ActPatrolFollower::leader_state_callback(const rosplane_msgs::StateConstPtr& pMsg)
  {
    _leaderState.pos_N = pMsg->position[0];
    _leaderState.pos_E = pMsg->position[1];
    _leaderState.pos_D = pMsg->position[2];
    _leaderState.Va = pMsg->Va;
    _leaderState.chi = pMsg->chi;

    _leaderStateInitialized = true;
    _payload.leader_state_init = true;
  }

  void ActPatrolFollower::areacoverDoneCallback(const std_msgs::Empty::ConstPtr& pMsg)
  {
    _areacoverDoneReceived = true;
  }

  void ActPatrolFollower::sendCommand(unified_msg::unified_message_t *pOutBuffer)
  {
    if(pOutBuffer->error_code[0] == 0x00 && pOutBuffer->error_code[1] == 0x00 && pOutBuffer->error_code[2] == 0x00)
    {
      act_action_cmd_out_t mav_cmd;
      memcpy(&mav_cmd, &pOutBuffer->payload[0], sizeof(mav_cmd));
      
      _cmdMsg.chi_c = mav_cmd.chi_c;
      _cmdMsg.Va_c = mav_cmd.Va_c;
      _cmdMsg.h_c = mav_cmd.h_c;
      
      if(int(mav_cmd.Va_c) == 0)
      {
        _landing = true;
      }
      if( !std::isnan(_cmdMsg.chi_c) && !std::isnan(_cmdMsg.Va_c) && !std::isnan(_cmdMsg.h_c) )
        _controllerCommandsPub.publish(_cmdMsg);
        
    }else
    {
      ROS_INFO("[ActPatrolFollower] _outputGlobalBuffer type is WRONG !\n");
    }
  }

  void ActPatrolFollower::setDefaultVirtrualPoint(const int& aID, 
                                                  const int& aQuantity, 
                                                  const float& aDeisredDistance, 
                                                        float& aDefaultX, 
                                                        float& aDefaultY)
  {
    int max_index = aQuantity > pow(sqrt(aQuantity), 2) ? (sqrt(aQuantity) + 1) : sqrt(aQuantity);
    aDefaultX = (aDeisredDistance / 2.0) * (float((aID - 1) % max_index + 1) - (max_index+1)/2.0);
    aDefaultY = (aDeisredDistance / 2.0) * (float((aID - 1) / max_index + 1) - (max_index+1)/2.0);
  }

  void ActPatrolFollower::discriminateEvents()
  {
    if( !_finishEventPublished && _areacoverDoneReceived )
    {
      _areacoverDoneReceived = false;
      _finishEventPublished = true;
      ROS_INFO("[Follower stateCallback] Publishing finish event to UTO !");
      pubEventMsg("finish_event");

    }else if( !_payload.lost_leader_event_pubed && (!_payload.leader_state_init && ((int)ros::Time::now().toSec() > _beginTime + _payload.lost_leader_find_duration) 
                                            || _payload.lost_leader_state && _payload.lost_leader_state_start_cnt >= (1e6 / _payload.sleep_duration) * _payload.lost_leader_find_duration ) )
    {
      _payload.lost_leader_event_pubed = true;
      ROS_INFO("[Follower stateCallback] Publishing lost leader event to UTO !");
      pubEventMsg("lost_leader_event"); 

    }else if( !_enemyEventPublished && (int)ros::Time::now().toSec() >  _beginTime + _enemyFindDuration)
    {
      _enemyEventPublished = true;
      ROS_INFO("[Follower calculateCommand] Publishing enemy event to UTO !");
      // pubEventMsg("enemy_event");
    }
  } 

  void ActPatrolFollower::updateBuffers()
  {
    memcpy(&_payload.mav_pos_N, &_mavState, sizeof(_mavState));
    memcpy(&_payload.virtual_agent_x, &_mavVAPosition, sizeof(_mavVAPosition));
    memcpy(&_payload.leader_pos_N, &_leaderState, sizeof(_leaderState)); 
    memcpy(&_payload.leader_virtual_agent_x, &_leaderVAPosition, sizeof(_leaderVAPosition));

    _inputGlobalBuffer->setPayload(_payload);
    _conditionBuffer[0]->setPayload(_judgePayload);
    
    _inputGlobalBuffer->setValue("type", 2);
    _outputGlobalBuffer->setValue("error_code", 0);
    
    _judgePayload.battery -= 0.1;
  }
} //end namespace

#ifdef RUN_ALONE
  int main(int argc, char** argv)
  {
    ros::init(argc, argv, "act_patrol_follower");

    ros::NodeHandle n; 
    
    act_patrol_follower::ActPatrolFollower da_plugin;

    ROS_INFO("Act Test BT F");

    return 0; 
  }
#else
  PLUGINLIB_EXPORT_CLASS(act_patrol_follower::ActPatrolFollower, general_bus::GeneralPlugin)
#endif
