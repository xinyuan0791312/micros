/* ==================================================================
* Copyright (c) 2018, micROS Group, TAIIC, NIIDT & HPCL.
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions
* are met:
*
* 1. Redistributions of source code must retain the above copyright
* notice, this list of conditions and the following disclaimer.
* 2. Redistributions in binary form must reproduce the above copyright
* notice, this list of conditions and the following disclaimer in the
* documentation and/or other materials provided with the
* distribution.
* 3. All advertising materials mentioning features or use of this software
* must display the following acknowledgement:
* This product includes software developed by the micROS Group. and
* its contributors.
* 4. Neither the name of the Group nor the names of its contributors may
* be used to endorse or promote products derived from this software
* without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY MICROS,GROUP AND CONTRIBUTORS
* ''AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT
* NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
* FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
* THE MICROS,GROUP OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
* OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
* HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
* OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
* IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
* ===================================================================
* Author: Ren Xiaoguang, Wu Yunlong, Li Jinghua, micROS-DA Team, NIIDT, TAIIC.
*/

#ifndef ACT_PATROL_LEADER_H
#define ACT_PATROL_LEADER_H

#include "message_types.h"
#include "da_adapter/da_adapter.h"
#include "act_softbus/act_softbus.h"
#include <pluginlib/class_list_macros.h>

#include <cmath>
#include <std_msgs/Empty.h>
#include <std_msgs/String.h>
#include "rosplane_msgs/State.h"
#include "rosplane_msgs/Controller_Commands.h"

#include "common/param_precision.h"
#include "common/act/param/act_param_battery.h"
#include "common/act/action/act_action_cmd_out.h"
#include "common/act/action/act_action_patrol_leader.h"
#include "common/act/condition/act_condition_battery.h"

/**
 * @brief Plugin Act Patrol Leader Namespace.
 * 
 */
namespace act_patrol_leader
{
  /**
   * @brief Fixed-wing state struct
   */
  typedef struct __fwing_state
  {
    float pos_N;  ///< This is the position in north direction (m)
    float pos_E;  ///< This is the position in east direction (m)
    float pos_D;  ///< This is the position in negative height direction (m)
    float Va;  ///< This is the airspeed (m/s)
    float chi;  ///< This is the course angle(rad)
  }fwing_state;

  /**
   * @brief Fixed-wing virtual agent struct
   */
  typedef struct __fwing_va
  {
    float x;  ///< This is the position in east direction
    float y;  ///< This is the position in north direction
  }fwing_va;

  /**
   * @brief This is a class to implement the actor plugin for actor act_patrol_leader.
   */
  class ActPatrolLeader : public act_softbus::ActSoftbus, public da_adapter::DaAdapter
  {
    public:

      #ifdef RUN_ALONE
        ActPatrolLeader();
      #endif

      /**
       * @brief Overload start function of General Plugin
       */
      virtual void start();

      /**
       * @brief Initialize arguments 
       * for instnnce, remapping parameters from launch files or desciption files, etc.
       */
      virtual void initArgs();

    private:

      ros::NodeHandle _nh;
      ros::NodeHandle _nhPrivate;
      
      ros::Subscriber _goalsSub;  ///< This is the subscriber for position of virtual agents
      ros::Subscriber _mavStateSub;  ///< This is the subscriber for current the state of MAV
      
      ros::Publisher _leaderStatePub;  ///< This is the publisher for msg of the state of the leader MAV
      ros::Publisher _areacoverDonePub;  ///< This is the publisher for areacover done signal to followers
      ros::Publisher _controllerCommandsPub;  ///< This is the publisher for msg to controller
            
      act_action_patrol_leader_t _payload;  ///< This is the struct saving input data of the loaded algorithm plugin
      act_condition_battery_t _judgePayload = {10.0};  ///< This is the struct saving output data of the loaded algorithm plugin
     
      fwing_va _mavVAPosition = {43.301270, 25.000000};  ///< This is the position of virtual agent
      fwing_state _mavState = {0, 0, 0};  ///< This is the state of MAV
      
      rosplane_msgs::State _leaderStateMsg;  ///< This is the msg saves the state of the leader MAV to be published                  
      rosplane_msgs::Controller_Commands _cmdMsg;  ///< This is the msg to controller_commands 
            
      bool _runAlone;  ///< This is the whether the plugin is running alone or not
      bool _landing = false;  ///< This is the landing command published or not
      bool _mavStateInitialized = false;  ///< This is the whether the state of MAV is initialized or not
      bool _goalReceived = false;  ///< This is the whether position of virtual agents is received or not  
      bool _finishEventPublished = false;  ///< This is the whether finish_event is published
      
      int _mavID;  ///< This is the identification number of MAV
      int _beginTime;  ///< This is the starting time of plugin
      int _coverFinishDuration;  ///< This is the finish time of plugin
            
      float _heightDifference;    ///< This is the difference between MAVs with serial identification number                           
      
      /**
       * @brief Broadcast the state of leader
       * 
       * @param[in] pub_leader_state_ specifies the struct variable saving the state of leader
       */
      void sendLeaderState(const fwing_state& aLeaderState);

      /**
       * @brief Broadcast the result of the loaded algorithm plugin. 
       * 
       * @param[in] pOutBuffer specifies the pointer variable pointing to _outputGlobalBuffer
       */
      void sendCommand(unified_msg::unified_message_t *pOutBuffer);

      /**
       * @brief Receive positions of virtual agents
       * 
       * @param[in] pMsg specifies the ros message saving goals
       */
      void goalCallback(const std_msgs::String::ConstPtr& pMsg);  

      /**
       * @brief Receive the current state of MAV 
       * 
       * @param[in] pMsg specifies own state information of MAV
       */
      void uav_state_callback(const rosplane_msgs::StateConstPtr& pMsg); 

      /**
       * @brief Discriminate transmission events 
       */
      void discriminateEvents();  

      /**
       * @brief Update buffers
       * 
       */
      void updateBuffers(); 
  };
} //end namespace

#endif
