/* ==================================================================
* Copyright (c) 2018, micROS Group, TAIIC, NIIDT & HPCL.
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions
* are met:
*
* 1. Redistributions of source code must retain the above copyright
* notice, this list of conditions and the following disclaimer.
* 2. Redistributions in binary form must reproduce the above copyright
* notice, this list of conditions and the following disclaimer in the
* documentation and/or other materials provided with the
* distribution.
* 3. All advertising materials mentioning features or use of this software
* must display the following acknowledgement:
* This product includes software developed by the micROS Group. and
* its contributors.
* 4. Neither the name of the Group nor the names of its contributors may
* be used to endorse or promote products derived from this software
* without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY MICROS,GROUP AND CONTRIBUTORS
* ''AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT
* NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
* FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
* THE MICROS,GROUP OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
* OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
* HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
* OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
* IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
* ===================================================================
* Author: Ren Xiaoguang, Wu Yunlong, Li Jinghua, micROS-DA Team, NIIDT, TAIIC.
*/

#include "act_patrol_leader/act_patrol_leader.h"

namespace act_patrol_leader
{
  #ifdef RUN_ALONE
    ActPatrolLeader::ActPatrolLeader()
    {
      start();
    }
  #endif
  
  void ActPatrolLeader::start()
  {
    //  Step 1 : Initialize arguments  
    initArgs();

    //  Step 2 : Create Control Nodes and Leaf Nodes of Behavior Tree  
    BT::FallbackNode* fallbackRoot = new BT::FallbackNode("fallbackRoot");
    BT::SequenceNode* sequenceLowbBattery = new BT::SequenceNode("sequenceLowbBattery");
    BT::ActConditionNode<act_condition_interface::ActConditionInterface>* conditionLowbBattery = new BT::ActConditionNode<act_condition_interface::ActConditionInterface>("conditionLowbBattery");
    BT::ActActionNode<act_action_interface::ActActionInterface>* actionLGVF = new BT::ActActionNode<act_action_interface::ActActionInterface>("actionLGVF");
    BT::ActActionNode<act_action_interface::ActActionInterface>* actionLowbBattery = new BT::ActActionNode<act_action_interface::ActActionInterface>("actionLowbBattery");
    
    //  Step 3 : Set Action Nodes and Condition Nodes 
    conditionLowbBattery->setParam(_paramBuffer[0]);
    conditionLowbBattery->setMethod("act_softbus", "act_condition_interface::ActConditionInterface", "fwing_act_condition_battery_impl::FwingActConditionBatteryImpl");
    conditionLowbBattery->setBuffer(_conditionBuffer[0]);
    actionLGVF->setParam(_paramBuffer[1]);
    actionLGVF->setMethod("act_softbus", "act_action_interface::ActActionInterface", "fwing_act_action_lgvf_impl::FwingActActionLgvfImpl");
    actionLGVF->setBuffer(_inputGlobalBuffer, _outputGlobalBuffer);
    actionLowbBattery->setParam(_paramBuffer[2]);
    actionLowbBattery->setMethod("act_softbus", "act_action_interface::ActActionInterface", "fwing_act_action_battery_impl::FwingActActionBatteryImpl");
    actionLowbBattery->setBuffer(_inputGlobalBuffer, _outputGlobalBuffer);
    
    //  Step 4 : Build Behavior Tree 
    _root->AddChild(fallbackRoot);
    fallbackRoot->AddChild(sequenceLowbBattery);
    fallbackRoot->AddChild(actionLGVF);
    sequenceLowbBattery->AddChild(conditionLowbBattery);
    sequenceLowbBattery->AddChild(actionLowbBattery);

    ros::Rate loop_rate(1);
    while(ros::ok())
    {
      #ifdef RUN_ALONE
        ros::spinOnce();
      #else
        discriminateEvents();
        GOON_OR_RETURN
      #endif
      
      if(!_landing)
      {
        //  Step 5 : Update Buffers  
        updateBuffers();
        
        //  Step 6 : Execute Behavior Tree  
        BT::ReturnStatus state = _root->Tick();

        sendLeaderState(_mavState);
        
        //  Step 7 : Analyze _outputGlobalBuffer and send command  
        sendCommand(_outputGlobalBuffer);
        
        if (_root->get_status() != BT::RUNNING)
        {
          _root->ResetColorState();
        }
      }
      loop_rate.sleep();
    }
    //  Step 8 : Release memory of Control Nodes and Leaf Nodes of Behavior Tree  
    delete fallbackRoot;
    delete sequenceLowbBattery;
    delete conditionLowbBattery;
    delete actionLGVF;
    delete actionLowbBattery;
  }

  void ActPatrolLeader::initArgs()
  {
    _nh = ros::NodeHandle();
    _nhPrivate = ros::NodeHandle("~");

    #ifdef RUN_ALONE
      _runAlone = true;
    #else
      _runAlone = false;
    #endif

    _nhPrivate.param<int>("robot_id", _mavID, 1);
    
    if(_runAlone)
    {
      _nhPrivate.param<float>("v0", _payload.vd, 15.0);
      _nhPrivate.param<float>("rd", _payload.rd, 50.0);
      _nhPrivate.param<float>("h_d", _payload.hd, 100.0);
      _nhPrivate.param<float>("delta_h", _heightDifference, 2.0);
      _nhPrivate.param<int>("sleep_duration", _payload.sleep_duration, 10000);
      _nhPrivate.param<int>("cover_finish_duration", _coverFinishDuration, 300); 
    }
    else
    {
      _payload.vd = atof( getParam("v0").c_str() );
      _payload.rd = atof( getParam("rd").c_str() );
      _payload.hd = atof( getParam("h_d").c_str() );
      _heightDifference = atof( getParam("delta_h").c_str() );
      _payload.sleep_duration = atoi( getParam("sleep_duration").c_str() );
      _coverFinishDuration = atoi( getParam("cover_finish_duration").c_str() );
    }
    
    _beginTime = (int)ros::Time::now().toSec();
    _payload.hd += _mavID*_heightDifference;

    _judgePayload.battery = 25;
    
    _inputGlobalBuffer->setValue("type", 1);
    _inputGlobalBuffer->setValue("error_code", 0);
    
    _outputGlobalBuffer->setValue("type", 6);
    _outputGlobalBuffer->setValue("error_code", 0);
    
    addConditionBuffer();
    _conditionBuffer[0]->setValue("type", 3);
    _conditionBuffer[0]->setValue("error_code", 0);
    
    addParamBuffer();
    _paramBuffer[0]->setValue("type", 4);
    _paramBuffer[0]->setValue("error_code", 0);
    act_param_battery_t condition_param = {10.0};
    _paramBuffer[0]->setPayload(condition_param);
    
    addParamBuffer();   
    _paramBuffer[1]->setValue("type", 5);
    _paramBuffer[1]->setValue("error_code", 0); 
    param_precision_t tmp = {1e-5};
    _paramBuffer[1]->setPayload(tmp);
    
    addParamBuffer();
    _paramBuffer[2]->setValue("type", 5);
    _paramBuffer[2]->setValue("error_code", 0);
    _paramBuffer[2]->setPayload(tmp);
    
    _leaderStatePub = advertiseDA<rosplane_msgs::State>("/Leader_state", 10, _runAlone);
    _controllerCommandsPub = advertiseDA<rosplane_msgs::Controller_Commands>("controller_commands", 10, _runAlone);
    _areacoverDonePub = advertiseDA<std_msgs::Empty>("/areacover_done", 10, _runAlone);

    _goalsSub = subscribeDA<std_msgs::String>("/goal", 10, &ActPatrolLeader::goalCallback, this, _runAlone);
    _mavStateSub = subscribeDA<rosplane_msgs::State>("truth", 10, &ActPatrolLeader::uav_state_callback, this, _runAlone);
  }

  void ActPatrolLeader::goalCallback(const std_msgs::String::ConstPtr& pMsg)
  {
    int size_msg = (pMsg->data).size() / 8;
    if(_mavID < size_msg)
    {
      memcpy(&_mavVAPosition.x, &pMsg->data[8*(_mavID-1)+8], 4);
      memcpy(&_mavVAPosition.y, &pMsg->data[8*(_mavID-1)+12], 4);
      _goalReceived = true;
    }
    else
    {
      _goalReceived = false;	
    }
  }

  void ActPatrolLeader::uav_state_callback(const rosplane_msgs::StateConstPtr& pMsg)
  {
    _mavState.pos_N = pMsg->position[0];
    _mavState.pos_E = pMsg->position[1];
    _mavState.pos_D = - pMsg->position[2];
    _mavState.Va = pMsg->Va;
    _mavState.chi = pMsg->chi;
    
    _mavStateInitialized = true;
  }

  void ActPatrolLeader::sendLeaderState(const fwing_state& aLeaderState)
  {
    _leaderStateMsg.position[0] = aLeaderState.pos_N;
    _leaderStateMsg.position[1] = aLeaderState.pos_E;
    _leaderStateMsg.position[2] = aLeaderState.pos_D;
    _leaderStateMsg.Va = aLeaderState.Va;
    _leaderStateMsg.chi = aLeaderState.chi;

    _leaderStatePub.publish(_leaderStateMsg);
  }

  void ActPatrolLeader::sendCommand(unified_msg::unified_message_t *pOutBuffer)
  {
    if(pOutBuffer->error_code[0] == 0x00 && pOutBuffer->error_code[1] == 0x00 && pOutBuffer->error_code[2] == 0x00)
    {
      act_action_cmd_out_t mav_cmd;
      memcpy(&mav_cmd, &pOutBuffer->payload[0], sizeof(mav_cmd));
          
      _cmdMsg.chi_c = mav_cmd.chi_c;
      _cmdMsg.Va_c = mav_cmd.Va_c;
      _cmdMsg.h_c = mav_cmd.h_c; 
      if(mav_cmd.Va_c == 0)
      {
        _landing = true;
      }
      
      if(!std::isnan(_cmdMsg.chi_c) && !std::isnan(_cmdMsg.Va_c) && !std::isnan(_cmdMsg.h_c))
        _controllerCommandsPub.publish(_cmdMsg);

    }else
    {
      ROS_INFO("[ActPatrolLeader] _outputGlobalBuffer type is WRONG !\n");
    }
  }

  void ActPatrolLeader::discriminateEvents()
  {
    if( (int)ros::Time::now().toSec() > (_beginTime + _coverFinishDuration) )
    {
      std_msgs::Empty msg;
      _areacoverDonePub.publish(msg);

      if(!_finishEventPublished)
      {
        pubEventMsg("finish_event");
        _finishEventPublished = true;
      }
    }
  }

  void ActPatrolLeader::updateBuffers()
  {
    memcpy(&_payload.mav_pos_N, &_mavState, sizeof(_mavState));
    memcpy(&_payload.virtual_agent_x, &_mavVAPosition, sizeof(_mavVAPosition));

    _inputGlobalBuffer->setPayload(_payload);
    _conditionBuffer[0]->setPayload(_judgePayload);
    
    _inputGlobalBuffer->setValue("type", 1);
    _outputGlobalBuffer->setValue("error_code", 0);
    
    _judgePayload.battery -= 0.1;
  }
} //end namespace

#ifdef RUN_ALONE
  int main(int argc, char** argv)
  {
    ros::init(argc, argv, "act_patrol_leader");

    ros::NodeHandle n; 
    
    act_patrol_leader::ActPatrolLeader da_plugin;

    ROS_INFO("BT Test2");

    return 0; 
  }
#else
  PLUGINLIB_EXPORT_CLASS(act_patrol_leader::ActPatrolLeader, general_bus::GeneralPlugin)
#endif

