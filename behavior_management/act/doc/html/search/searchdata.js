var indexSectionsWithContent =
{
  0: "_abcdefghijklmnopqrstuvwxyz~å",
  1: "abcefghijklmopqrsuvw",
  2: "afgmqrt",
  3: "acdfghijklmopqrsuvw",
  4: "abcdefghijklmnopqrstuvwxyz~",
  5: "_abcdefghijklmnopqrstuvwxyz",
  6: "bcgilmnou",
  7: "m",
  8: "k",
  9: "akr",
  10: "moprst",
  11: "p",
  12: "bprtuwå"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "typedefs",
  7: "enums",
  8: "enumvalues",
  9: "related",
  10: "defines",
  11: "groups",
  12: "pages"
};

var indexSectionLabels =
{
  0: "全部",
  1: "类",
  2: "命名空间",
  3: "文件",
  4: "函数",
  5: "变量",
  6: "类型定义",
  7: "枚举",
  8: "枚举值",
  9: "友元",
  10: "宏定义",
  11: "组",
  12: "页"
};

