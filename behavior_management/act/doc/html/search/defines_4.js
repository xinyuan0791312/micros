var searchData=
[
  ['sampling_5fmax_5flat',['SAMPLING_MAX_LAT',['../geo__mag__declination_8cpp.html#ad0f0a2722a6731fb8ee2ea0046875340',1,'geo_mag_declination.cpp']]],
  ['sampling_5fmax_5flon',['SAMPLING_MAX_LON',['../geo__mag__declination_8cpp.html#ad9a605961cc88456796cd03c43ce8873',1,'geo_mag_declination.cpp']]],
  ['sampling_5fmin_5flat',['SAMPLING_MIN_LAT',['../geo__mag__declination_8cpp.html#aa1b435ff25b933498e4f1fc20a5ee741',1,'geo_mag_declination.cpp']]],
  ['sampling_5fmin_5flon',['SAMPLING_MIN_LON',['../geo__mag__declination_8cpp.html#a386e31e8e4474cd3654e0c373c0e50d4',1,'geo_mag_declination.cpp']]],
  ['sampling_5fres',['SAMPLING_RES',['../geo__mag__declination_8cpp.html#a33b606f14ec182aa602e0662fac93918',1,'geo_mag_declination.cpp']]]
];
