var searchData=
[
  ['eigenodometry',['EigenOdometry',['../structrotors__control_1_1EigenOdometry.html#a85068f06571789c00dd5301db7e9f75d',1,'rotors_control::EigenOdometry::EigenOdometry()'],['../structrotors__control_1_1EigenOdometry.html#a97bc3069b39104b3f7e3096888b38e1b',1,'rotors_control::EigenOdometry::EigenOdometry(const Eigen::Vector3d &amp;_position, const Eigen::Quaterniond &amp;_orientation, const Eigen::Vector3d &amp;_velocity, const Eigen::Vector3d &amp;_angular_velocity)']]],
  ['eigenodometryfrommsg',['eigenOdometryFromMsg',['../namespacerotors__control.html#aa9f8255c0362903f2feff121ac3cf599',1,'rotors_control']]],
  ['externalforcecallback',['ExternalForceCallback',['../classgazebo_1_1GazeboBagPlugin.html#a0fbf6f5d43a16a76c282ea5f2b6aaf62',1,'gazebo::GazeboBagPlugin']]]
];
