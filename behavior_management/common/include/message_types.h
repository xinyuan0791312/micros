/* ==================================================================
* Copyright (c) 2018, micROS Group, TAIIC, NIIDT & HPCL.
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions
* are met:
*
* 1. Redistributions of source code must retain the above copyright
* notice, this list of conditions and the following disclaimer.
* 2. Redistributions in binary form must reproduce the above copyright
* notice, this list of conditions and the following disclaimer in the
* documentation and/or other materials provided with the
* distribution.
* 3. All advertising materials mentioning features or use of this software
* must display the following acknowledgement:
* This product includes software developed by the micROS Group. and
* its contributors.
* 4. Neither the name of the Group nor the names of its contributors may
* be used to endorse or promote products derived from this software
* without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY MICROS,GROUP AND CONTRIBUTORS
* ''AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT
* NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
* FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
* THE MICROS,GROUP OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
* OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
* HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
* OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
* IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
* ===================================================================
* Author: Ren Xiaoguang, Wu Yunlong, Li Jinghua, micROS-DA Team, NIIDT, TAIIC.
*/

#ifndef MESSAGE_TYPES_H
#define MESSAGE_TYPES_H

#include <string.h>
#include <iostream>

#define LEN 256

/**
 * @brief This is a unified message namespace.
 * 
 */
namespace unified_msg
{
  /**
   * @brief This is a unified message struct as input & output for universal interface.
   * 
   */
  #pragma pack (1)
  typedef struct __unified_message 
  {

    uint8_t type[3]{0x00};  ///< Indicates type code of payload
    uint8_t error_code[3]{0x00};  ///< Indicates error code of payload
    char payload[LEN];  ///< Indicates core data
    
    /**
     * @brief Set Value of Keywords
     * 
     * @param[in] keyword specifies keyword of value
     * @param[in] value specifies value of keyword to be set 
     */
    template <typename T>
    bool setValue(const std::string& keyword, const T& value);

    /**
     * @brief Set Payload With Data(Fixed Size)
     * The size of data is fixed
     * 
     * @param[in] data specifies data of payload to be set
     */
    template <typename T>
    bool setPayload(const T& data);

    /**
     * @brief Set Payload With Data(Flexible Size)
     * 
     * @param[in] data specifies data of payload to be set
     * @param[in] size specifies size(byte) of data
     */
    template <typename T, typename M>
    bool setPayload(const T& data, const M& size);
    
    /**
     * @brief Set Payload With Data(Flexible Size)
     * 
     * @param[in] data specifies pointer of payload to be set
     */
    template <typename T>
    bool setPayloadPtr(T& data);

  }unified_message_t;
  #pragma pack()

  template <typename T>
  bool unified_message_t::setValue(const std::string& keyword, const T& value)
  {
    if(keyword == "type")
    {
      unsigned int len = sizeof(type) < sizeof(value) ? sizeof(type) : sizeof(value);
      char buffer[len];
      memcpy(buffer, &value, len);
    
      for(int i = 0; i < len; i++)
      {
        memcpy(&type[i], &buffer[i], 1);
      }
      for(int i = len; i < sizeof(type); i++)
        type[i] = 0x00;

      return true;

    }else if(keyword == "error_code")
    {
      unsigned int len = sizeof(error_code) < sizeof(value) ? sizeof(error_code) : sizeof(value);
      char buffer[len];
      memcpy(buffer, &value, len);
    
      for(int i = 0; i < len; i++)
      {
        memcpy(&error_code[i], &buffer[i], 1);
      }
      for(int i = len; i < sizeof(error_code); i++)
        error_code[i] = 0x00;

      return true;

    }
    return false;
  }

  template <typename T>
  bool unified_message_t::setPayload(const T& data)
  {
    if(sizeof(data) <= LEN)
    {
      memcpy(payload, &data, sizeof(data));
      return true;
    }
    return false;      
  }
  
  template <typename T, typename M>
  bool unified_message_t::setPayload(const T& data, const M& size)
  {
    if(size <= LEN)
    {
      memcpy(payload, &size, sizeof(M));
      memcpy(&payload[sizeof(M)], &data, size);

      return true;
    }
    return false;  
  }

  template <typename T>
  bool unified_message_t::setPayloadPtr(T& data)
  {
    int size = sizeof(&data);
    if(size+sizeof(size) <= LEN)
    {
      T *data_ptr = &data;
      memcpy(payload, &size, sizeof(size));
      memcpy(&payload[sizeof(size)], &data_ptr, size);
      return true;
    }
    return false;      
  }

} //end namespace

#endif
