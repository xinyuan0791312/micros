/* ==================================================================
* Copyright (c) 2018, micROS Group, TAIIC, NIIDT & HPCL.
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions
* are met:
*
* 1. Redistributions of source code must retain the above copyright
* notice, this list of conditions and the following disclaimer.
* 2. Redistributions in binary form must reproduce the above copyright
* notice, this list of conditions and the following disclaimer in the
* documentation and/or other materials provided with the
* distribution.
* 3. All advertising materials mentioning features or use of this software
* must display the following acknowledgement:
* This product includes software developed by the micROS Group. and
* its contributors.
* 4. Neither the name of the Group nor the names of its contributors may
* be used to endorse or promote products derived from this software
* without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY MICROS,GROUP AND CONTRIBUTORS
* ''AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT
* NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
* FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
* THE MICROS,GROUP OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
* OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
* HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
* OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
* IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
* ===================================================================
* Author: Ren Xiaoguang, Wu Yunlong, Li Jinghua, micROS-DA Team, NIIDT, TAIIC.
*/

#ifndef ACT_ACTION_PATROL_FOLLOWER_H
#define ACT_ACTION_PATROL_FOLLOWER_H

#define UNIFIED_MSG_ID_PATROL_fOLLOWER 2

/**
 * @brief This is an act action message struct as input for algorithm LGVFF.
 * 
 */
#pragma pack (1)
typedef struct __act_action_patrol_follower 
{

  float mav_pos_N;                        ///< This is the position of MAV in north direction (m)
  float mav_pos_E;                        ///< This is the position of MAV in east direction (m)
  float mav_pos_D;	                      ///< This is the position of MAV in negative height direction (m)
  float mav_Va;	                          ///< This is the airspeed (m/s) of MAV
  float mav_chi;	                        ///< This is the course (rad) of MAV
  float virtual_agent_x;                  ///< This is the position virtual agent (VA) of MAV in east direction
  float virtual_agent_y;                  ///< This is the position virtual agent (VA) of MAV in north direction
  float vd;                               ///< This is the desired velocity of MAV    
  float rd;                               ///< This is the desired radius of MAV 
  float hd;                               ///< This is the desired height of MAV
  int sleep_duration = 10000;             ///< This is the sleep duration per main loop
  float leader_pos_N;                     ///< This is the position of leader MAV in north direction (m)
  float leader_pos_E;                     ///< This is the position of leader MAV in east direction (m)
  float leader_pos_D;	                    ///< This is the position of leader MAV in negative height direction (m)
  float leader_Va;	                      ///< This is the airspeed of leader MAV (m/s)
  float leader_chi;	                      ///< This is the course of leader MAV (rad) 
  float leader_virtual_agent_x;           ///< This is the position of virtual agent (VA) of leader MAV in east direction
  float leader_virtual_agent_y;           ///< This is the position of virtual agent (VA) of leader MAV in north direction
  float v_max;                            ///< This is the maximum velocity of leader MAV
  float v_min;                            ///< This is the minimum velocity of leader MAV
  int lost_leader_find_duration = 20;      ///< This is the the time triger lost_leader_event
  bool leader_state_init = false;         ///< This specifies whether leader state is initialized
  bool lost_leader_state = false;         ///< This specifies whether leader state is lost 
	bool lost_leader_event_pubed = false;   ///< This is the param specifies whether lost_leader_event publsihed
	int lost_leader_state_start_cnt = 0;    ///< This is the quantity of lost of leader state

  float real_leader_pos_N;                        ///< This is the position of MAV in north direction (m)
  float real_leader_pos_E;                        ///< This is the position of MAV in east direction (m)
  
}act_action_patrol_follower_t;
#pragma pack()

#endif