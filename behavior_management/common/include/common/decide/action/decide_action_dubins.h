/* ==================================================================
* Copyright (c) 2018, micROS Group, TAIIC, NIIDT & HPCL.
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions
* are met:
*
* 1. Redistributions of source code must retain the above copyright
* notice, this list of conditions and the following disclaimer.
* 2. Redistributions in binary form must reproduce the above copyright
* notice, this list of conditions and the following disclaimer in the
* documentation and/or other materials provided with the
* distribution.
* 3. All advertising materials mentioning features or use of this software
* must display the following acknowledgement:
* This product includes software developed by the micROS Group. and
* its contributors.
* 4. Neither the name of the Group nor the names of its contributors may
* be used to endorse or promote products derived from this software
* without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY MICROS,GROUP AND CONTRIBUTORS
* ''AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT
* NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
* FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
* THE MICROS,GROUP OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
* OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
* HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
* OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
* IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
* ===================================================================
* Author: Ren Xiaoguang, Wu Yunlong, Li Jinghua, micROS-DA Team, NIIDT, TAIIC.
*/

#ifndef DECIDE_ACTION_DUBINS_H
#define DECIDE_ACTION_DUBINS_H

#define UNIFIED_MSG_ID_DECIDE_ACTION_DUBINS 9

/**
 * @brief This is a decide action message struct as input for algorithm fwing_decide_action_dubins_impl.
 * 
 */
#pragma pack (1)
typedef struct ___decide_action_dubins 
{ 

  float pos_N;  ///< This is the position of MAV in north direction (m)
  float pos_E;  ///< This is the position of MAV in east direction (m)
  float pos_D;  ///< This is the position of MAV in negative height direction (m)
  float Va;  ///< This is the airspeed (m/s) of MAV
  float chi;  ///< This is the course (rad) of MAV

  float w[3];  ///< This is the waypoint in local NED (m)
  float chi_d;  ///< This is the desired course at this waypoint (rad)
  float Va_d;  ///< This is the desired airspeed (m/s)
  bool chi_valid;  ///< This is the desired course valid (dubin or fillet paths)
  bool landing;  ///< This is the flag value used by Landing actor plugin
  bool loiter_point;  ///< This is the flag value used by Landing actor plugin
  bool set_current;  ///< This is the flag value setting this waypoint to be executed now! Starts a new list
  bool clear_wp_list;  ///< This is the flag value removing all waypoints and returns to origin.  The rest of this message will be ignored

  float R_min;  /** minimal R */
  float speed_v;  /** flight speed */
  float h_default;  /** default flight height */
  float h_diff;  /** height difference among flights*/ 
  float delta_diff;  /** X/Y distance among flights*/ 
  float chi_infty; /** maximal change on chi  */
  float k_path;    /** parameter for following straight path  */
  float k_orbit;   /** parameter for following orbit path  */

  bool renew;
  bool done;

}decide_action_dubins_t;
#pragma pack()

#endif